<?php

class Perfect_store_content_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'perfect_store_content';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    public $types = array(
        0 => 'product',
        1 => 'place',
        2 => 'promotion'
    );

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function prepareContent(array $content)
    {
        $output = array(
            0 => array('id' => 'product', 'title' => 'Product'),
            1 => array('id' => 'place', 'title' => 'Place'),
            2 => array('id' => 'promotion', 'title' => 'Promotion')
        );

        foreach ($content as $key => $value) 
        {
            $output[$value['type']]['content'][] = $value;
        }

        return $output;
    }
}

?>