<?php

class Call_records_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'call_records';
    protected $_primary_key = 'armstrong_2_call_records_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_call_records_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getCallRecordListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[0] = $default;
        }

        if ($where != NULL) 
        {
            $records = $this->get_by($where, false, NULL, array('armstrong_2_call_records_id'));
        } 
        else 
        {
            $records = $this->get(NULL, false, NULL, array('armstrong_2_call_records_id'));
        }

        if ($records)
        {
            foreach ($records as $record) 
            {
                $output[$record['armstrong_2_call_records_id']] = $record['armstrong_2_call_records_id'];
            }
        }

        return $output;
    }
}

?>