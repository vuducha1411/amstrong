<?php

class Contact_strategy_daterange_dt extends Contact_strategy_daterange_m implements DatatableModel
{
    public function appendToSelectStr()
    {
        return array(
            'otm_A_visited' => "CONCAT( s.otm_A_customers_visited,'/',s.otm_A_customers_visited_in_weeks,' weeks')",
            'otm_B_visited' => "CONCAT( s.otm_B_customers_visited,'/',s.otm_B_customers_visited_in_weeks,' weeks')",
            'otm_C_visited' => "CONCAT( s.otm_C_customers_visited,'/',s.otm_C_customers_visited_in_weeks,' weeks')",
            'otm_D_visited' => "CONCAT( s.otm_D_customers_visited,'/',s.otm_D_customers_visited_in_weeks,' weeks')"
        );
    }

    public function fromTableStr()
    {
        return 'contact_strategy_daterange s';
    }

    public function joinArray()
    {

    }

}