<?php

class Wholesalers_dt extends Wholesalers_m implements DatatableModel {

    public function appendToSelectStr() {
        return array(
            'salesperson_name' => "CONCAT( s.first_name,SPACE(1),s.last_name )"
        );
    }

    public function fromTableStr() {
        return 'wholesalers c';
    }

    public function joinArray(){
        return array(
            'salespersons s' => 's.armstrong_2_salespersons_id = c.armstrong_2_salespersons_id'
        );
    }
}