 <?php
/* list of tables to fetch from*/


class Cron_monitoring_model extends CI_Model
{
	
	public function getAll(){
		$query = "SELECT * from cronjobs";

		 return $this->db->query($query)->result_array();

	}

	public function getCronData($country, $refTable, $dateRef, $countryRef){

		$query = "SELECT " . $dateRef . " from " . $refTable . " where " . $countryRef . " = " . $country . " order by " . $dateRef . " desc";

		return $this->db->query($query)->row_array();

	}

}

?>
