<?php
/**
 * Created by PhpStorm.
 * User: OPSOLUTIONS PH INC
 * Date: 8/31/2017
 * Time: 1:55 PM
 */

class Webshop_m extends AMI_Model{

    protected $_table_name = 'webshop';

    public function __construct()
    {
        parent::__construct();
    }

    public function getRecord($table = NULL, $data, $where, $group_by = NULL, $limit = FALSE){
        $db_tbl = $table != NULL ? $table : $this->_table_name;
        $group_by != NULL ? $this->db->group_by($group_by) : "";

        $this->db->select($data, FALSE);

        $this->db->from($db_tbl);

        $this->db->where(array('is_draft' => 0));
        if ($where && is_array($where)) {
            foreach ($where as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $k => $v) {
                        $this->db->where($key, $v);
                    }
                } else {
                    $this->db->where($key, $val);
                }
            }
        }

        if($limit){
            $this->db->limit($limit);
        }
        $result = $this->db->get()->result();

        return $result;
    }

    public function updateRecord($table = NULL, $data, $where){
        $db_tbl = $table != NULL ? $table : $this->_table_name;
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($db_tbl);
    }

    public function getJoinRecord($tbl_from, $select = '*', $tbl_to, $joinId, $joinType, $where){
        $db_tbl = $tbl_from != NULL ? $tbl_from.' w' : $this->_table_name.' w';
        $result = $this->db->select($select, false)
            ->from($db_tbl)
            ->join($tbl_to, $joinId, $joinType)
            ->where($where)
            ->get()
            ->result();

        return $result;
    }

    public function addNewRecord($table = NULL, $data){
        $now = date('Y-m-d h:i:s');
        $data['date_created'] = $now;
        $data['last_updated'] = $now;

        $db_tbl = $table != NULL ? $table : $this->_table_name;
        $result = $this->db->insert($db_tbl, $data);
        return $result;
    }

}