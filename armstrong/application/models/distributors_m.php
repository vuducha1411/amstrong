<?php

class Distributors_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'distributors';
    protected $_primary_key = 'armstrong_2_distributors_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_distributors_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getDistributorListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL) 
        {
            $distributors = $this->get_by($where, false, NULL, array('armstrong_2_distributors_id', 'name'));
        } 
        else 
        {
            $distributors = $this->get(NULL, false, NULL, array('armstrong_2_distributors_id', 'name'));
        }

        if ($distributors)
        {
            foreach ($distributors as $distributor) 
            {
                $output[$distributor['armstrong_2_distributors_id']] = $distributor['armstrong_2_distributors_id'] . ' - ' . $distributor['name'];
            }
        }

        return $output;
    }

    public function parse_form_with_sale($cols, $default = NULL, $where = NULL, $single = FALSE)
    {
        $return = array();
        if ($where != NULL) {
            $data = $this->get_by($where, $single, NULL, $cols);
        } else {
            $data = $this->get(NULL, $single, NULL, $cols);
        }

        $default == NULL || $return[''] = $default;

        if (count($data)) {
            foreach ($data as $val) {
                $return[$val[$this->_primary_key]] = $val[$cols[2]] . ' - '. $val[$cols[1]] ;
            }
        }

        return $return;
    }

    public function getDistributorDetails($select = '*', $tblJoinTo, $con, $joinType, $where = NULL){
        if($where != NULL){
            $this->db->where($where);
        }

        $result = $this->db->select($select)
            ->from($this->_table_name)
            ->join($tblJoinTo, $this->_table_name.".".$con, $joinType)
            ->where($where)
            ->get()
            ->result();

        return $result;
    }
}

?>