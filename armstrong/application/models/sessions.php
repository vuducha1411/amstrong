<?php
/**
 * Date: 8/12/14
 * Time: 3:03 PM
 */
class Sessions extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'sessions';
    protected $_primary_key = 'session_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'session_id';
    protected $_order_rule = 'ASC';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }
}

?>