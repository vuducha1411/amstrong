<?php

class Brands_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'brands';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getBrandListOptions($default = null, $where = null, $key = 'id')
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL)
        {
            $brands = $this->get_by($where, false, NULL);
        }
        else
        {
            $brands = $this->get(NULL, false, NULL);
        }

        if ($brands)
        {

            foreach ($brands as $brand)
            {
                $output[$brand[$key]] = $brand['name'];
            }
        }

        return $output;
    }
}

?>