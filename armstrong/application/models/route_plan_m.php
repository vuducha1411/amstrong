<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Route_plan_m extends AMI_Model 
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'route_plan';
    protected $_primary_key = 'armstrong_2_route_plan_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_route_plan_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() 
    {
        parent::__construct();
    }

    public function getRouteStatusListOptions($where = null)
    {
        return array(
            1 => 'Meeting Day',
            2 => 'Leave Day',
            3 => 'MC Day',
            4 => 'Workshop Day',
            5 => 'Training',
            6 => 'Special Event',
            7 => 'Holiday'
        );
    }

    public function getRoutePlanListOptions($where = null) {
        $output = array();

        // if ($where != null) {
        //     $plans = $this->get_by($where, false, null, array(
        //         'armstrong_2_salespersons_id'
        //     ));
        // } else {
        //     $plans = $this->get(null, false, null, array(
        //         'armstrong_2_salespersons_id'
        //     ));
        // }

        $plans = $this->db->select($this->_table_name . '.*')
            ->from($this->_table_name)
            ->join('salespersons', $this->_table_name . '.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id')
            ->where($where)
            ->get()->result_array();
    
        if ($plans) {
            foreach ($plans as $plan) {
                $output[$plan['armstrong_2_route_plan_id']] = $plan['armstrong_2_route_plan_id'];
            }
        }

        return $output;
    }
}