<?php 


class Global_ssd_m extends AMI_Model {
	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'global_ssd';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    private $dateOfTransactionParse = "IFNULL( STR_TO_DATE(DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(DATE_OF_TRANSACTION, '%d/%m/%Y'))";
//    private $dateOfTransactionParse = "STR_TO_DATE(DATE_OF_TRANSACTION, '%d-%b-%y')";

    public function __construct() {
        parent::__construct();
    }

    public function getSSD($limit, $where, $ack = 0) {
        $this->db->select('*')->from($this->_table_name);
        $this->db->like($where["col"], $where["keyword"]);
        $this->db->where("acknowledged", ($ack == "TRUE" ? 1 : 0));
        $this->db->limit($limit[0], $limit[1]);
        return $this->db->get()->result_array();
    }

    public function countSSD($where, $ack = "FALSE") {
        $this->db->select('*')->from($this->_table_name);
        $this->db->like($where["col"], $where["keyword"]);
        $this->db->where("acknowledged", ($ack == "TRUE" ? 1 : 0));
        return count($this->db->get()->result());
    }

    public function getSSDbetween($from, $to, $ack = 0) {
        $this->db->select('*')->from($this->_table_name);
        $this->db->where("$this->dateOfTransactionParse >=", $from);
        $this->db->where("$this->dateOfTransactionParse <=", $to);
	    $this->db->where("acknowledged", ($ack == "TRUE" ? 1 : 0));
        return $this->db->get()->result_array();
    }

	public function getSSDbetweenSA($from, $to, $ack = 0) {
		$this->db->select('*')->from($this->_table_name);
		$this->db->where("$this->dateOfTransactionParse >=", $from);
		$this->db->where("$this->dateOfTransactionParse <=", $to);
		if( $ack == "FALSE" ) {
			$this->db->where("acknowledged", 0);
		}
		return $this->db->get()->result_array();
	}

    public function getSSDbySalesperson($salespersonId, $ack = "FALSE") {
        if (strripos(strtoupper($salespersonId), "DELETE") === false) {
            $sql = "SELECT a.*
                    FROM global_ssd as a
                    JOIN distributors as b ON a.FILE_OWNER_ID = b.armstrong_2_distributors_id
                    WHERE b.armstrong_2_salespersons_id = '{$salespersonId}'
                    AND a.acknowledged IS {$ack}
                    UNION
                    SELECT a.*
                    FROM global_ssd as a
                    JOIN wholesalers as c ON a.FILE_OWNER_ID = c.armstrong_2_wholesalers_id
                    WHERE c.armstrong_2_salespersons_id = '{$salespersonId}'
                    AND a.acknowledged IS {$ack}
                    UNION
                    SELECT a.*
                    FROM global_ssd as a
                    JOIN customers as d ON a.FILE_OWNER_ID = d.armstrong_2_customers_id
                    WHERE d.armstrong_2_salespersons_id = '{$salespersonId}'
                    AND a.acknowledged IS {$ack}";
            return $this->db->query($sql)->result_array();
        } else {
            return [];
        }
    }

	public function getSSDbySalespersonSA($salespersonId, $ack = "FALSE") {
		if (strripos(strtoupper($salespersonId), "DELETE") === false) {
			$dtSql = "SELECT a.*
                FROM global_ssd as a
                JOIN distributors as b ON a.FILE_OWNER_ID = b.armstrong_2_distributors_id
                WHERE b.armstrong_2_salespersons_id = '{$salespersonId}'";
			$whsSql = "SELECT a.*
                FROM global_ssd as a
                JOIN wholesalers as c ON a.FILE_OWNER_ID = c.armstrong_2_wholesalers_id
                WHERE c.armstrong_2_salespersons_id = '{$salespersonId}'";
			$custSql = " SELECT a.*
                FROM global_ssd as a
                JOIN customers as d ON a.FILE_OWNER_ID = d.armstrong_2_customers_id
                WHERE d.armstrong_2_salespersons_id = '{$salespersonId}'";

			if ( $ack == "FALSE" ) {
				$dtSql .= " AND a.acknowledged = 0";
				$whsSql .= " AND a.acknowledged = 0";
				$custSql .= " AND a.acknowledged = 0";
			}
			$sql = implode(" UNION ", [$dtSql, $whsSql, $custSql]);
			return $this->db->query($sql)->result_array();
		} else {
			return [];
		}
	}

	public function getSSDbySalespersonBetween($salespersonId, $date, $ack = "FALSE") {
		if (strripos(strtoupper($salespersonId), "DELETE") === false) {
			$sql = "SELECT a.*
                FROM global_ssd as a
                JOIN distributors as b ON a.FILE_OWNER_ID = b.armstrong_2_distributors_id
                WHERE b.armstrong_2_salespersons_id = '{$salespersonId}'
				AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'
                AND a.acknowledged IS {$ack}

                UNION

                SELECT a.*
                FROM global_ssd as a
                JOIN wholesalers as c ON a.FILE_OWNER_ID = c.armstrong_2_wholesalers_id
                WHERE c.armstrong_2_salespersons_id = '{$salespersonId}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'
                AND a.acknowledged IS {$ack}

                UNION

                SELECT a.*
                FROM global_ssd as a
                JOIN customers as d ON a.FILE_OWNER_ID = d.armstrong_2_customers_id
                WHERE d.armstrong_2_salespersons_id = '{$salespersonId}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'
                AND a.acknowledged IS {$ack}";
			return $this->db->query($sql)->result_array();
		} else {
			return [];
		}
	}

	public function getSSDbySalespersonBetweenSA($salespersonId, $date, $ack = "FALSE") {
		if (strripos(strtoupper($salespersonId), "DELETE") === false) {
			$dtSql = "SELECT a.*
                FROM global_ssd as a
                JOIN distributors as b ON a.FILE_OWNER_ID = b.armstrong_2_distributors_id
                WHERE b.armstrong_2_salespersons_id = '{$salespersonId}'
				AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'";
			$whsSql = "SELECT a.*
                FROM global_ssd as a
                JOIN wholesalers as c ON a.FILE_OWNER_ID = c.armstrong_2_wholesalers_id
                WHERE c.armstrong_2_salespersons_id = '{$salespersonId}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'";
			$custSql = "SELECT a.*
                FROM global_ssd as a
                JOIN customers as d ON a.FILE_OWNER_ID = d.armstrong_2_customers_id
                WHERE d.armstrong_2_salespersons_id = '{$salespersonId}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) >= '{$date["from"]}'
                AND IFNULL( STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d-%b-%y'), STR_TO_DATE(a.DATE_OF_TRANSACTION, '%d/%m/%Y')) <= '{$date["to"]}'";

			if ( $ack == "FALSE" ) {
				$dtSql .= " AND a.acknowledged = 0";
				$whsSql .= " AND a.acknowledged = 0";
				$custSql .= " AND a.acknowledged = 0";
			}
			$sql = implode(" UNION ", [$dtSql, $whsSql, $custSql]);
			return $this->db->query($sql)->result_array();
		} else {
			return [];
		}
	}
}