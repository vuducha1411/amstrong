<?php

class Promotions_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'promotions';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getProductListOptions($default = null, $where = null, $key = 'sku_number')
    {
        $output = array();

        if ($default)
        {
            $output[0] = $default;
        }

        $fields = array('id', 'sku_number', 'sku_name', 'name_alt', 'quantity_case', 'weight_pc');

        if ($where != NULL) 
        {
            $products = $this->get_by($where, false, NULL, $fields);
        } 
        else 
        {
            $products = $this->get(NULL, false, NULL, $fields);
        }

        if ($products)
        {
            foreach ($products as $product) 
            {
                $output[$product[$key]] = "{$product['sku_number']} - {$product['name_alt']} ({$product['quantity_case']}x{$product['weight_pc']})";
            }
        }

        return $output;
    }
}

?>