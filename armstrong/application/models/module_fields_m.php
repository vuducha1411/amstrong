<?php

class Module_fields_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'module_fields';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getListOptions($default = null, $where = null, $where_not_in = null, $restrict = false)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }

        $user = $this->session->userdata('user_data');

        $data = $this->get_by($where, false, 'field', array(
            'id',
            'field',
            'module'
        ));
        if ($data) {
            foreach ($data as $post) {
                $output[$post['id']] = $post['field'];
            }
        }
        return $output;
    }
}

?>