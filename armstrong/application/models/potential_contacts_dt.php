<?php

class Potential_Contacts_dt extends Potential_Contacts_m implements DatatableModel {

    public function appendToSelectStr() {
        return array(
            'armstrong_2_customers_name' => "cus.`armstrong_2_customers_name`"
        );
    }

    public function fromTableStr() {
        return 'potential_contacts c';
    }

    public function joinArray(){
        return array(
            'potential_customers cus|left' => 'c.armstrong_1_customers_id = cus.armstrong_1_customers_id'
        );
    }
}