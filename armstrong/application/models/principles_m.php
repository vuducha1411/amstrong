<?php

class Principles_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'principles';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'name';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getColumnTypeList(){
        $output = [];
        $principle_type = $this->db->from($this->_table_name)->where('column_type != ""', null)->group_by('column_type')->get()->result_array();
        if($principle_type){
            foreach($principle_type as $type){
                $output[$type['column_type']] = ucfirst(str_replace('_', ' ', $type['column_type']));
            }
        }
        return $output;
    }
}

?>