<?php

class Wholesalers_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'wholesalers';
    protected $_primary_key = 'armstrong_2_wholesalers_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_wholesalers_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getSalerListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL) 
        {
            $persons = $this->get_by($where, false, NULL, array('armstrong_2_wholesalers_id', 'name'));
        } 
        else 
        {
            $persons = $this->get(NULL, false, NULL, array('armstrong_2_wholesalers_id', 'name'));
        }

        if ($persons)
        {
            foreach ($persons as $person) 
            {
                $output[$person['armstrong_2_wholesalers_id']] = $person['armstrong_2_wholesalers_id'] . ' - ' . $person['name'];
            }
        }

        return $output;
    }

    public function parse_form_with_sale($cols, $default = NULL, $where = NULL, $single = FALSE)
    {
        $return = array();
        if ($where != NULL) {
            $data = $this->get_by($where, $single, NULL, $cols);
        } else {
            $data = $this->get(NULL, $single, NULL, $cols);
        }

        $default == NULL || $return[''] = $default;

        if (count($data)) {
            foreach ($data as $val) {
                $return[$val[$this->_primary_key]] = $val[$cols[2]] . ' - '. $val[$cols[1]] ;
            }
        }

        return $return;
    }
}

?>