<?php

class Perfect_store_cc_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'perfect_store_cc';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    public $types = array(
        0 => 'product',
        1 => 'price',
        2 => 'promotion',
        3 => 'place',
        4 => 'proposition'
    );

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function prepareContent(array $content)
    {
        $output = array(
            0 => array('id' => 'product', 'title' => 'Product'),
            1 => array('id' => 'price', 'title' => 'Price'),
            2 => array('id' => 'promotion', 'title' => 'Promotion'),
            3 => array('id' => 'place', 'title' => 'Place'),
            4 => array('id' => 'proposition', 'title' => 'Proposition')
        );

        foreach ($content as $key => $value) 
        {
            $output[$value['type']]['content'][] = $value;
        }

        return $output;
    }
}

?>