<?php

class Contacts_dt extends Contacts_m implements DatatableModel {

    public function appendToSelectStr() {
        return array(
            'armstrong_2_customers_name' => "(CASE WHEN c.armstrong_2_customers_id LIKE '%OPD%' THEN cus.armstrong_2_customers_name WHEN c.armstrong_2_customers_id LIKE '%DIS%' THEN dis.`name` WHEN c.armstrong_2_customers_id LIKE '%WHS%' THEN who.`name` END)"
        );
    }

    public function fromTableStr() {
        return 'contacts c';
    }

    public function joinArray(){
        return array(
            'customers cus|left' => 'c.armstrong_2_customers_id = cus.armstrong_2_customers_id',
            'distributors dis|left' => 'c.armstrong_2_customers_id = dis.armstrong_2_distributors_id',
            'wholesalers who|left' => 'c.armstrong_2_customers_id = who.armstrong_2_wholesalers_id'
        );
    }
}