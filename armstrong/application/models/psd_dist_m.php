<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Psd_dist_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'psd_dist';
    protected $_primary_key = 'armstrong_2';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_psd_dist_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }


}

?>