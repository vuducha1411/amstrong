<?php

class tfo_status_m extends AMI_Model{

    function getCountryList(){
        $stmt = "SELECT id, name, code FROM country";
        $query = $this->db->query($stmt)->result_array();
        return $query;
    }

    function getTFOStatus($limit, $country_id){
        if (is_array($limit))
        {
            list($total, $offset) = $limit;
        }
        else
        {
            $total = $limit;
            $offset = 0;
        }

        /*
        $stmt = "SELECT
            t.armstrong_2_tfo_id, 
            c.name,
            date_format(t.date_created, '%d %b %Y %h:%i %p') AS created_date,
            date_format(t.date_sync, '%d %b %Y %h:%i %p') AS sync_date,
            (CASE
                WHEN t.method = 0 THEN 'Email'
                WHEN t.method = 1 THEN 'Fax'
                WHEN t.method = 2 THEN 'Phone'
                ELSE 'Save Record'
            END) AS method_name,
			#t.fax,
			(CASE
				WHEN t.method = 0 THEN t.email
				WHEN t.method = 1 THEN t.fax
				WHEN t.method = 2 THEN t.phone
			END) AS contact_dtl,
			(CASE
                WHEN t.sent = 0 THEN 'Successful'
                WHEN t.sent < 0 THEN 'Failed'
                ELSE 'Sending'
            END) AS status,
            t.fail_message
            FROM tfo t 
            LEFT JOIN country c
            ON c.id = t.country_id
            WHERE t.is_draft = 0
            AND t.country_id = '".$country_id."'
            AND t.method = 1
            #ORDER BY t.country_id
            ORDER BY t.date_sync DESC
            LIMIT 0, 500
            ";
        */
        $stmt = "SELECT
            armstrong_2_tfo_id, 
            date_format(date_created, '%d %b %Y %h:%i %p') AS created_date,
            date_format(date_sync, '%d %b %Y %h:%i %p') AS sync_date,
            (CASE
                WHEN method = 0 THEN 'Email'
                WHEN method = 1 THEN 'Fax'
                WHEN method = 2 THEN 'Phone'
                ELSE 'Save Record'
            END) AS method_name,
                        (CASE
                                WHEN method = 0 THEN email
                                WHEN method = 1 THEN fax
                                WHEN method = 2 THEN phone
                        END) AS contact_dtl,
                        (CASE
                WHEN sent = 0 THEN 'Successful'
                WHEN sent < 0 THEN 'Failed'
                ELSE 'Sending'
            END) AS status,
            fail_message
            FROM tfo
            WHERE is_draft = 0
            AND country_id = '".$country_id."'
            AND method = 1
            ORDER BY date_sync DESC
            LIMIT 0, 500
            ";
        $query = $this->db->query($stmt)->result_array();
        return $query;
    }

    function getTFOCount(){
        $stmt = "SELECT
            count(*) as cnt
            FROM tfo t
        ";
        $query = $this->db->query($stmt)->result_array();
        return $query;
    }

}

?>
