<?php

class Ssd_m extends AMI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'ssd';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
    }


    public function getMethodListOptions()
    {
        return array(
            0 => 'Email',
            1 => 'Fax',
            2 => 'Phone',
            3 => 'Save Record'
        );
    }
    public function approvessd($id)
    {
        $ssd = $this->get($id);
        $sql = "UPDATE ssd SET status = '1', last_updated = '".date('Y-m-d H:i:s')."' WHERE armstrong_2_ssd_id = '".$ssd['armstrong_2_ssd_id']."'";
        return $this->db->query($sql);
    }
    public function rejectssd($id)
    {
        $ssd = $this->get($id);
        $sql = "UPDATE ssd SET status = '-1', last_updated = '".date('Y-m-d H:i:s')."' WHERE armstrong_2_ssd_id = '".$ssd['armstrong_2_ssd_id']."'";
        return $this->db->query($sql);
    }
}

?>