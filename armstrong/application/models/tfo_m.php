<?php

class Tfo_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'tfo';
    protected $_primary_key = 'armstrong_2_tfo_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_tfo_id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getMethodListOptions()
    {
        return array(
            0 => 'Email',
            1 => 'Fax',
            2 => 'Phone',
            3 => 'Save Record'
        );
    }
}

?>