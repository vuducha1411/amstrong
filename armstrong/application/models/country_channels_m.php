<?php

class Country_channels_m extends AMI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'country_channels';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
    }

    public function getCountryChannelsListOptions($default = null, $where = null, $where_not_in = null, $restrict = false)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }

        $user = $this->session->userdata('user_data');

        $data = $this->get_by($where, false, null, array(
            'id',
            'name',
            'country_id'
        ));
        if ($data) {
            foreach ($data as $post) {
                $output[$post['id']] = $post['id'] . ' - ' . $post['name'];
            }
        }
        return $output;
    }

    public function getConvenience_lvl($params){
        $result = '';
        if($params){
            $where = array(
                'country_id' => $params['country'],
                'id' => $params['value']
            );
            $data = $this->get_by($where, false, null, array('convenience_lvl'));
            if($data){
                foreach($data as $_data){
                    $result = $_data['convenience_lvl'];
                }
            }
        }
        return $result;
    }

}

?>