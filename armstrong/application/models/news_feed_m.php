<?php
/**
 * Date: 8/12/14
 * Time: 5:03 PM
 */
class News_feed_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'news_feed';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function prepareDataTables(array $news_feed)
    {
        $data = array();

        foreach ($news_feed as $_news_feed)
        {
            $data[] = array(
                'title' => $_news_feed['title'],
            );
        }

        return $data;
    }
}

?>