<?php 


class Global_ssd_mappings_m extends AMI_Model {
	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'global_ssd_mappings';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */

    public function __construct() {
        parent::__construct();
    }

    public function isSuperAdmin($id = null) {
        if ($id) {
            $this->load->model('admins_m');
            return $this->admins_m->get($id);
        } else {
            $user = $this->session->userdata('user_data');
            $roleId = isset($user['roles_id']) ? $user['roles_id'] : null;

            if ($roleId == Authentication_m::ADMIN_ROLES_ID) {
                return true;
            }
        }

        return false;
    }

    public function getMapDistributors( $salesPersonId = false, $where = [] ) {
        $sql = "SELECT 
                    a.id AS map_id,
                    a.value,
                    a.is_draft,
                    b.id AS dt_id,
                    b.armstrong_2_distributors_id,
                    b.name,
                    c.columns,
                    c.name as map_name,
                    b.id as id,
                    b.name as name
                from global_ssd_mappings AS a
                join distributors AS b ON REPLACE(a.armstrong_fk_id, 'DT:', '') = b.id
                join global_ssd_map_entities AS c ON a.entity_id = c.id";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where), $salesPersonId);
    }

    public function getMapWholesalers( $salesPersonId = false, $where = [] ) {
        $sql = "SELECT 
                    a.id AS map_id,
                    a.value,
                    a.is_draft,
                    b.id AS dt_id,
                    b.armstrong_2_wholesalers_id,
                    b.name,
                    c.columns,
                    c.name as map_name,
                    b.id as id,
                    b.name as name
                from global_ssd_mappings AS a
                join wholesalers AS b ON REPLACE(a.armstrong_fk_id, 'WHS:', '') = b.id
                join global_ssd_map_entities AS c ON a.entity_id = c.id";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where), $salesPersonId);
    }

    public function getMapCustomers( $salesPersonId = false, $where = [] ) {
        $sql = "SELECT 
                    a.id AS map_id,
                    a.value,
                    a.is_draft,
                    b.id AS dt_id,
                    b.armstrong_2_customers_id,
                    b.armstrong_2_customers_name as name,
                    c.columns,
                    c.name as map_name,
                    b.id as id,
                    b.armstrong_2_customers_name as name
                from global_ssd_mappings AS a
                join customers AS b ON REPLACE(a.armstrong_fk_id, 'CUST:', '') = b.id
                join global_ssd_map_entities AS c ON a.entity_id = c.id";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where), $salesPersonId);
    }

    private function executeCustomQuery( $sql, $salesPersonId = false ) {

        if ( !$this->isSuperAdmin($salesPersonId) ) {
            if( stripos($sql, "where") >= 0 ) {
                $sql .= ($salesPersonId) ? " and a.armstrong_2_salesperson_id = '" . $salesPersonId . "'" : "";
            } else {
                $sql .= ($salesPersonId) ? " where a.armstrong_2_salesperson_id = '" . $salesPersonId . "'" : "";
            }
        }
        return $this->db->query($sql)->result_array();
    }

    public function getDistributorsWithMap($salesPersonId) {
        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM distributors AS a
                JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'DT:', '')
                WHERE b.armstrong_2_salesperson_id = '" . $salesPersonId . "'";
        return $this->executeCustomQuery($sql);
    }

    public function getWholesalersWithMap($salesPersonId) {
        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM wholesalers AS a
                JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'WHS:', '')
                WHERE b.armstrong_2_salesperson_id = '" . $salesPersonId . "'";
        return $this->executeCustomQuery($sql);
    }

    public function getCustomersWithMap($salesPersonId) {
        $sql = "SELECT 
                    a.id as id,
                    a.armstrong_2_customers_name as name
                FROM customers AS a
                JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'CUST:', '')
                WHERE b.armstrong_2_salesperson_id = '". $salesPersonId ."'";
        return $this->executeCustomQuery($sql);
    }

    public function getDistributorsWithoutMap($data = []) {
        $entityId = $data["entityId"];
        $salesPersonId = $data["salesPersonId"];
        // list($entityId, $salesPersonId) = $data;
        $isAdmin = $this->isSuperAdmin();
        $where = [
            "cc.id" =>  $entityId
        ];

        if( !$isAdmin ) {
            $where = array_merge($where, [ "aa.armstrong_2_salespersons_id"  => $salesPersonId]);
        }

        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM distributors AS a
                WHERE a.id NOT IN   (
                                        SELECT aa.id FROM distributors AS aa
                                        JOIN global_ssd_mappings AS bb ON aa.id = REPLACE(bb.armstrong_fk_id, 'DT:', '')
                                        JOIN global_ssd_map_entities as cc ON bb.entity_id = cc.id";
        $sql = $this->appendWhereClause($sql, $where);
        $sql .= " GROUP BY aa.id )";
        $sql .= (!$isAdmin) ? " AND a.armstrong_2_salespersons_id = '$salesPersonId'" : "";
        $sql .= " ORDER BY a.name";

        return $this->executeCustomQuery($sql);
    }

    public function getWholesalersWithoutMap($data = []) {
        $entityId = $data["entityId"];
        $salesPersonId = $data["salesPersonId"];
        // list($entityId, $salesPersonId) = $data;
        $isAdmin = $this->isSuperAdmin();
        $where = [
            "cc.id" =>  $entityId
        ];

        if( !$isAdmin ) {
            $where = array_merge($where, [ "aa.armstrong_2_salespersons_id"  => $salesPersonId]);
        }

        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM wholesalers AS a
                WHERE a.id NOT IN   (
                                        SELECT aa.id FROM wholesalers AS aa
                                        JOIN global_ssd_mappings AS bb ON aa.id = REPLACE(bb.armstrong_fk_id, 'WHS:', '')
                                        JOIN global_ssd_map_entities as cc ON bb.entity_id = cc.id";
        $sql = $this->appendWhereClause($sql, $where);
        $sql .= " GROUP BY aa.id )";
        $sql .= (!$isAdmin) ? " AND a.armstrong_2_salespersons_id = '$salesPersonId'" : "";
        $sql .= " ORDER BY a.name";

        return $this->executeCustomQuery($sql);
    }

    public function getCustomersWithoutMap($data = []) {
        $entityId = $data["entityId"];
        $salesPersonId = $data["salesPersonId"];
        // list($entityId, $salesPersonId) = $data;
        $isAdmin = $this->isSuperAdmin();
        $where = [
            "cc.id" =>  $entityId
        ];

        if( !$isAdmin ) {
            $where = array_merge($where, [ "aa.armstrong_2_salespersons_id"  => $salesPersonId]);
        }

        $sql = "SELECT 
                    a.id as id,
                    a.armstrong_2_customers_name as name
                FROM customers AS a
                WHERE a.id NOT IN   (
                                        SELECT aa.id FROM customers AS aa
                                        JOIN global_ssd_mappings AS bb ON aa.id = REPLACE(bb.armstrong_fk_id, 'CUST:', '')
                                        JOIN global_ssd_map_entities as cc ON bb.entity_id = cc.id";
        $sql = $this->appendWhereClause($sql, $where);
        $sql .= " GROUP BY aa.id )";
        $sql .= (!$isAdmin) ? " AND a.armstrong_2_salespersons_id = '$salesPersonId'" : "";
        $sql .= " ORDER BY name";

        return $this->executeCustomQuery($sql);
    }

    /*
    public function getDistributorsWithoutMap($salesPersonId, $where = []) {
        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM distributors AS a
                LEFT JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'DT:', '')
                WHERE b.id IS NULL
                AND a.armstrong_2_salespersons_id = '". $salesPersonId ."'";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where));
    }

    public function getWholesalersWithoutMap($salesPersonId, $where = []) {
        $sql = "SELECT 
                    a.id as id,
                    a.name
                FROM wholesalers AS a
                LEFT JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'WHS:', '')
                WHERE b.id IS NULL
                AND a.armstrong_2_salespersons_id = '". $salesPersonId ."'";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where));
    }

    public function getCustomersWithoutMap($salesPersonId, $where = []) {
        $sql = "SELECT 
                    a.id as id,
                    a.armstrong_2_customers_name as name
                FROM customers AS a
                LEFT JOIN global_ssd_mappings AS b ON a.id = REPLACE(b.armstrong_fk_id, 'CUST:', '')
                WHERE b.id IS NULL
                AND a.armstrong_2_salespersons_id = '" . $salesPersonId . "'";
        return $this->executeCustomQuery($this->appendWhereClause($sql, $where));
    }
    */

    private function appendWhereClause($sql, $where) {
        foreach($where as $key => $val) {
            if( stripos($sql, "where") >= 0 ) {
                // $sql .= " AND " . $key . " = '" . $val  . "'";
                $sql .= " AND " . $key;
            } else {
                // $sql .= " WHERE " . $key . " = '" . $val . "'";
                $sql .= " WHERE " . $key;
            }

            if( $val === "NULL" ) {
                $sql .= " IS NULL";
            } else {
                $sql .= " = '" . $val . "'";
            }
        }

        return $sql;
    }
}