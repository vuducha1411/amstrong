<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Date: 9/8/14
 * Time: 9:10 AM
 */
class Country_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'country';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }
	
	public function getListCountry($default = null, $where = null, $where_not_in = null,  $restrict = false) {
		$output = array();
		if ($default) {
			$output[''] = $default;
		}
        $countries = $this->get_by($where, false, null, array(
                'name',
                'code',
				'id'
            ),
            $where_not_in
        );

        if ($countries) {
            foreach ($countries as $c) {
                $output[ $c['id'] ] = $c['name'] . ' - ' . $c['code'];
            }
        }        
		return $output;
	}

}

?>