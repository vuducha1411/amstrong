<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Date: 9/8/14
 * Time: 9:10 AM
 */
class Salespersons_m extends AMI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'salespersons';
    protected $_primary_key = 'armstrong_2_salespersons_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_salespersons_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;
    public $rules_login = array(
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|xss_clean|valid_email'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|xss_clean|min_length[6]'
        ),
        'country_id' => array(
            'field' => 'country_id',
            'label' => 'Country',
            'rules' => 'required'
        ),
    );

    // protected $_hash = '3fe23f6867398ee9e05d1b3e209ff8b4b8e9fe7ee21d';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_m');
    }

    public function preparePerson(array $person)
    {
        $person['name'] = $person['first_name'] . ' ' . $person['last_name'];

        return $person;
    }

    public function preparePersons(array $persons)
    {
        foreach ($persons as &$person) {
            $person = $this->preparePerson($person);
        }

        return $persons;
    }

    public function getPersonListOptions($default = null, $where = null, $restrict = false, $where_not_in = null)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }
        $where['test_acc'] = 0;
        $user = $this->session->userdata('user_data');

        if ($restrict) {
            if (CI_Controller::get_instance()->hasPermission('manage_all', 'salespersons')) {
                // do nothing
            } else if (CI_Controller::get_instance()->hasPermission('manage_staff', 'salespersons')) {
                $where['salespersons_manager_id'] = $user['id'];
            } else {
                $where['armstrong_2_salespersons_id'] = $user['id'];
            }
        }

        $persons = $this->get_by($where, false, 'first_name', array(
            'armstrong_2_salespersons_id',
            'first_name',
            'last_name',
        ),
            $where_not_in
        );

        if ($persons) {
//            $controllerInstance = & get_instance();
//            $persons = $controllerInstance->filter_dummy_data($persons);
            foreach ($persons as $person) {
                $person = $this->preparePerson($person);
//                $output[$person['armstrong_2_salespersons_id']] = $person['armstrong_2_salespersons_id'] . ' - ' . $person['name'];
                $output[$person['armstrong_2_salespersons_id']] = $person['name'].' ('.$person['armstrong_2_salespersons_id'].')';
            }
        }

        if ($restrict && !isset($output[$user['id']]) && ($user['roles_id'] != Authentication_m::ADMIN_ROLES_ID && $user['sub_type'] != 'md')) {
            $output[$user['id']] = "{$user['id']} - {$user['name']}";
        }
        return $output;
    }

    public function getLeaderListOptions($default = null, $where = null, $where_not_in = null, $restrict = false)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }
        $where['test_acc'] = 0;
        $user = $this->session->userdata('user_data');

        if ($restrict) {

            if (CI_Controller::get_instance()->hasPermission('manage', 'salespersons')) {
                // do nothing
            } else if (CI_Controller::get_instance()->hasPermission('manage_staff', 'salespersons')) {
                $where['armstrong_2_salespersons_id'] = $user['id'];
            } else {

            }
        }

        $persons = $this->get_by($where, false, null, array(
            'armstrong_2_salespersons_id',
            'first_name',
            'last_name',
        ),
            $where_not_in
        );

        if ($persons) {
            foreach ($persons as $person) {
                $person = $this->preparePerson($person);
                $output[$person['armstrong_2_salespersons_id']] = $person['armstrong_2_salespersons_id'] . ' - ' . $person['name'];
            }
        }

        if ($restrict && !isset($output[$user['id']]) && $user['roles_id'] != Authentication_m::ADMIN_ROLES_ID) {
            $output[$user['id']] = "{$user['id']} - {$user['name']}";
        }

        return $output;
    }

    public function saveEntry($data, $id = null, $country_id = '')
    {
        // update existing person
        if ($id != null) {
            // ignore password if empty
            if ($data['password'] == '') {
                unset($data['password']);
            } else {
                $person = $this->get($id);
                $data['password'] = crypt($data['password'], md5($person['hash']));
            }

            $this->save($data, $id);
        } // create new person
        else {
            $now = date('Y-m-d H:i:s');
            $data['date_created'] = $now;
            $data['last_updated'] = $now;
            $data['password'] = crypt($data['password'], md5($this->_hash));
            $data['hash'] = $this->_hash;
            // auto generate armstrong_2_salespersons_id
            $data['armstrong_2_salespersons_id'] = ''; //$this->salespersons_m->generateArmstrongId($this->country_id, 'salespersons');

          //  $this->db->set($data);
           // $this->db->insert($this->_table_name);
            $this->save($data, null, true, 'INSERT', $country_id);
            $id = $this->db->insert_id();
        }

        return $id;
    }

    public function setManager($ids, $managerId)
    {
        if (!$ids) {
            return false;
        }

        $ids = explode(',', preg_replace('/\s+/', '', $ids));

        if (intval($managerId)) {
            $data = array('manager_id' => $managerId);
        } else {
            $data = array('salespersons_manager_id' => $managerId);
        }

        $this->update_in($data, 'armstrong_2_salespersons_id', $ids);

        foreach ($ids as $id) {
            $this->syncManagerId($id, $managerId);
        }
    }

    public function syncManagerId($id, $managerId)
    {
        if (!$id || !$managerId) {
            return false;
        }

        if (intval($id)) {
            $person = $this->get_by(array('id' => $id), true);
        } else {
            $person = $this->get($id, true);
        }

        if (!$person) {
            return false;
        }

        if (intval($managerId)) {
            $manager = $this->get_by(array('id' => $managerId), true);
        } else {
            $manager = $this->get($managerId, true);
        }

        if ($manager) {
            $this->save(array(
                'salespersons_manager_id' => $manager['armstrong_2_salespersons_id'],
                'manager_id' => $manager['id']
            ), $person['armstrong_2_salespersons_id']);
        }
    }

    public function get_by($where, $single = FALSE, $order_by = NULL, $cols = '*', $where_not_in = null)
    {

        if (is_array($where_not_in)) {
            foreach ($where_not_in as $col => $array_values) {
                $this->db->where_not_in($col, $array_values);
            }
        }

        return parent::get_by($where, $single, $order_by, $cols);
    }

    public function getEmail($salesPersonID)
    {
        return $this->db->from('salespersons')->select('email')->where(array('armstrong_2_salespersons_id' => $salesPersonID))->get()->row_array();
    }

    public function getListSales($salesManagerID)
    {
        $list_sales = [];
        $sales = $this->db->from('salespersons')
            ->select(array('armstrong_2_salespersons_id', 'salespersons_manager_id'))
            ->where(array(
                'salespersons_manager_id' => $salesManagerID,
                'is_draft' => 0,
                'active' => 1,
                'test_acc' => 0
            ))->get()->result_array();
        if ($sales) {
            foreach ($sales as $sale) {
                $list_sales[] = $sale['armstrong_2_salespersons_id'];
                $childs = $this->getListSales($sale['armstrong_2_salespersons_id']);

                if ($childs){
                    $childs = array_filter($childs);
                    foreach ($childs as $child){
                        $list_sales[] = $child;
                    }
                }
            }
        }

        return $list_sales;

    }
}

?>