<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kpi_target_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'kpi_target';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }   

    public function getKpiSettings($limit, $conditions)
    {
        if (is_array($limit))
        {
            list($total, $offset) = $limit;
        }
        else
        {
            $total = $limit;
            $offset = 0;
        }

        $kpiSettings = $this->db->select('kpi_target.*, salespersons.first_name, salespersons.last_name')
            ->from($this->_table_name)
            ->where($conditions)
            ->join('salespersons', 'salespersons.armstrong_2_salespersons_id = kpi_target.armstrong_2_salespersons_id')
            ->limit($total, $offset)
            ->order_by($this->_table_name . '.' . $this->_order_by)
            ->get()->result_array();

        return $kpiSettings;
    }
}

?>