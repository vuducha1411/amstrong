<?php
/**
 * User: vietthang
 * Date: 10/20/14
 * Time: 9:51 AM
 */

class Admins_m extends AMI_Model {

	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
	protected $_table_name = 'admins';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	protected $_order_rule = 'DESC';
	protected $_timestamp = TRUE;
	public $_is_draft = 0;

	/*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
	function __construct() {
		parent::__construct();		
	}

	public function save($data, $id = NULL) {
		isset($data['is_draft']) || $data['is_draft'] = $this->_is_draft;
		$return = parent::save($data, $id);

		return $return;
	}
}