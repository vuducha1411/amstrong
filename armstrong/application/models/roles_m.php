<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Date: 9/8/14
 * Time: 9:10 AM
 */
class Roles_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'roles';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function parseRestrictions($restrictions)
    {
        if (empty($restrictions))
        {
            return array();
        }


        if (!is_array($restrictions))
        {
            $restrictions = json_decode($restrictions, true) ? json_decode($restrictions, true) : array();
        }

        foreach ($restrictions as $key => &$value) 
        {
            $restrictions[$key] = explode(',', $value);
        }

        return $restrictions;
    }    

    public function groupListByParentId(array $items)
    {
        $itemHierarchy = array();

        foreach ($items AS $item) 
        {
            if (!$item['parent_id']) 
            {
                $item['parent_id'] = 0;
            }

            $itemHierarchy[$item['parent_id']][$item['id']] = $item;
        }

        return $itemHierarchy;
    }

    public function makeNestedList($hierarchy, $parentNodeId = 0, $level = 0) 
    {
        $items = array();

        if (empty($hierarchy[$parentNodeId])) 
        {
            return array();
        }

        foreach ($hierarchy[$parentNodeId] AS $i => $item) 
        {
            $items[$item['id']] = $item;
            // $items[$item['id']]['level'] = $item;

            $childItems = $this->makeNestedList($hierarchy, $item['id'], $level + 1);

            $items[$item['id']]['children'] = $childItems;
        }

        return $items;
    }

    public function renderNestedTemplate($roles, &$level = 0)
    {
        if ($roles)
        {
            $sortable = ($level == 0) ? 'sortable' : '';

            $template = '<ul class="list-unstyled ' . $sortable . '">';

            $level++;

            foreach ($roles as $role) 
            {                
                $list = '       
                    <li class="category_li" id="list_' . $role['id'] . '">
                        <div class="category_div" data-id="' . $role['id'] . '">
                            <div class="category_row">
                                <i class="handle fa fa-2x fa-arrows ico-droppable"></i>
                                <span class="panel-title">' . $role['name'] . '</span>                                                       
                            </div>
                        </div>
                        ' . $this->renderNestedTemplate($role['children'], $level) . ' 
                    </li>';

                $template .= $list;         
            }

            $template .= '</ul>';

            return $template;
        }
    }

    public function getRoleIdByRolename($country_id = null, $role_name = null) {
        if ($role_name) {
            if($country_id == null){
                $user = $this->session->userdata('user_data');
                if($role_name == "Global Marketing")
                {
                    $this->db->where(array(
                        'name' => $role_name,
                    ));
                }
                else
                {
                    $this->db->where(array(
                        'name' => $role_name,
                        "country_id IN ({$user['country_management']})" => null,
                    ));
                }
                $returns = $this->get(null, false, null, $cols = '*');
                $return = [];
                foreach($returns as $item){
                    $return[] = $item['id'];
                }
            }else{
                $this->db->where(array(
                    'name' => $role_name,
                    'country_id' => $country_id,
                ));
                $return = $this->get(null, true, null, $cols = '*');
                $return = $return['id'];
            }
        } else  {
            return false;
        }

        return $return;
    }

    public function getListRolesGlobal()
    {
        $output = array();
        $this->db->where(array(
            'global' => 1
        ));
        $roles = $this->get(null, false, null, $cols = '*');

        if($roles){
            foreach($roles as $role){
                $output[$role['id']] = $role['name'];
            }
            $default = array(0 => 'Admin');
            $output = $default + $output;
        }

        return $output;
    }

    public function checkReportPermission($roleId = null)
    {
        $result = array();
        if($roleId){
            $this->db->where(array('id' => $roleId));
            $roleRestriction = $this->get(null, false, null, $cols = 'restrictions');
            if($roleRestriction){
                $roleRestriction = json_decode($roleRestriction[0]['restrictions']);
                $roleRestriction = CI_Controller::get_instance()->convertToArray($roleRestriction);
                if(isset($roleRestriction['report_view'])){
                    $result['report_view'] = $roleRestriction['report_view'];
                }
                if(isset($roleRestriction['report_export'])){
                    $result['report_export'] = $roleRestriction['report_export'];
                }
            }
        }
        return $result;
    }
}

?>