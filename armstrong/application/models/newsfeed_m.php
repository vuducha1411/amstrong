<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Date: 3/4/15
 * Time: 3:39 PM
 */class Newsfeed_m  extends AMI_Model {

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'news_feed';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    
    public function __construct() {
        parent::__construct();
    }

    public function prepareDataTables(array $newsfeeds) {

        foreach ($newsfeeds as &$newsfeed )
        {
            $newsfeed['type'] = $newsfeed['newsfeed_type'] == 0 ? 'Local' : 'Country';
        }

        return $newsfeeds;
    }
}