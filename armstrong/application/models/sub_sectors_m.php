<?php

class Sub_sectors_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'sub_sectors';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

}

?>