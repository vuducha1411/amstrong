<?php

class perfect_store_groups_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'perfect_store_groups';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getGroupsListOptions($default = null, $where = null, $key = 'id')
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL)
        {
            $groups = $this->get_by($where, false, NULL);
        }
        else
        {
            $groups = $this->get(NULL, false, NULL);
        }

        if ($groups)
        {

            foreach ($groups as $group)
            {
                $output[$group[$key]] = $group['name'];
            }
        }

        return $output;
    }
}

?>