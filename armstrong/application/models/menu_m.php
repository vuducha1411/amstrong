<?php
/**
 * Created by PhpStorm.
 * User: vietthang
 * Date: 10/14/14
 * Time: 3:18 PM
 */

class Menu_m extends AMI_Model {
	/*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
	protected $_table_name = 'ami_modules';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_rule = 'asc';

	/*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
	public function __construct() {
		parent::__construct();
	}

	public function getMenuItems() {
		// Get all parent nodes
		$menuItems = $this->nested_set->getRootNodes();

		// Foreach parent node, get its child nodes.
		foreach ($menuItems as $key => $menuItem) {
			$childNodes = $this->nested_set->getTreePreorder($menuItem, TRUE, 'desc');

			// Assign child node to root node
			!count($childNodes['result_array']) || $menuItems[$key]['child_nodes'] = $childNodes['result_array'];
		}
		return $menuItems;
	}

}