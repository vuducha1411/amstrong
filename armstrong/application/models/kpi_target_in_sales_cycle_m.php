<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kpi_target_in_sales_cycle_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'kpi_target_in_sales_cycle';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }   

    public function getKpiSettings($limit, $conditions)
    {
        if (is_array($limit))
        {
            list($total, $offset) = $limit;
        }
        else
        {
            $total = $limit;
            $offset = 0;
        }

        $kpiSettings = $this->db->select('kpi_target_in_sales_cycle.*, salespersons.first_name, salespersons.last_name, sales_cycle.from_date, sales_cycle.to_date')
            ->from($this->_table_name)
            ->where($conditions)
            ->join('salespersons', 'salespersons.armstrong_2_salespersons_id = kpi_target_in_sales_cycle.armstrong_2_salespersons_id')
            ->join('sales_cycle', 'sales_cycle.id = kpi_target_in_sales_cycle.sales_cycle_id')
            ->limit($total, $offset)
            ->order_by($this->_table_name . '.' . $this->_order_by)
            ->get()->result_array();

        return $kpiSettings;
    }

}

?>