<?php

class Discount_model_range_m extends AMI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'discount_model_range';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
    }

    public function getRangeListOption($default = null, $where = null)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }

        if ($where != NULL) {
            $chains = $this->get_by($where, false, NULL, array('id', 'title', 'from_date', 'to_date'));
        } else {
            $chains = $this->get(NULL, false, NULL, array('id', 'title', 'from_date', 'to_date'));
        }

        if ($chains) {
            foreach ($chains as $chain) {
                $output[$chain['id']] = $chain['title'] . ' (' . $chain['from_date'] . ' ~ ' . $chain['to_date'] . ')';
            }
        }

        return $output;
    }
}

?>