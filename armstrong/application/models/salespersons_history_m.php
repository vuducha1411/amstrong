<?php

class Salespersons_history_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'salespersons_history';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }
    public function preparePerson(array $person)
    {
        $person['name'] = $person['first_name'] . ' ' . $person['last_name'];

        return $person;
    }
    public function getPersonListOptions($default = null, $where = null, $restrict = false, $where_not_in = null)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }
        $where['test_acc'] = 0;
        $user = $this->session->userdata('user_data');

        if ($restrict) {
            if (CI_Controller::get_instance()->hasPermission('manage_all', 'salespersons')) {
                // do nothing
            } else if (CI_Controller::get_instance()->hasPermission('manage_staff', 'salespersons')) {
                $where['salespersons_manager_id'] = $user['id'];
            } else {
                $where['armstrong_2_salespersons_id'] = $user['id'];
            }
        }

        $persons = $this->get_by($where, false, null, array(
            'armstrong_2_salespersons_id',
            'first_name',
            'last_name',
        ),
            $where_not_in
        );

        if ($persons) {
            foreach ($persons as $person) {
                $person = $this->preparePerson($person);
                $output[$person['armstrong_2_salespersons_id']] = $person['armstrong_2_salespersons_id'] . ' - ' . $person['name'];
            }
        }

        if ($restrict && !isset($output[$user['id']]) && ($user['roles_id'] != Authentication_m::ADMIN_ROLES_ID && $user['sub_type'] != 'md')) {
            $output[$user['id']] = "{$user['id']} - {$user['name']}";
        }
        return $output;
    }
    public function getLeaderListOptions($default = null, $where = null, $where_not_in = null, $restrict = false)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }
        $where['test_acc'] = 0;
        $user = $this->session->userdata('user_data');

        if ($restrict) {

            if (CI_Controller::get_instance()->hasPermission('manage', 'salespersons')) {
                // do nothing
            } else if (CI_Controller::get_instance()->hasPermission('manage_staff', 'salespersons')) {
                $where['armstrong_2_salespersons_id'] = $user['id'];
            } else {

            }
        }

        $persons = $this->get_by($where, false, null, array(
            'armstrong_2_salespersons_id',
            'first_name',
            'last_name',
        ),
            $where_not_in
        );

        if ($persons) {
            foreach ($persons as $person) {
                $person = $this->preparePerson($person);
                $output[$person['armstrong_2_salespersons_id']] = $person['armstrong_2_salespersons_id'] . ' - ' . $person['name'];
            }
        }

        if ($restrict && !isset($output[$user['id']]) && $user['roles_id'] != Authentication_m::ADMIN_ROLES_ID) {
            $output[$user['id']] = "{$user['id']} - {$user['name']}";
        }

        return $output;
    }
}

?>