<?php

class Potential_customers_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'potential_customers';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getCustomerListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL) 
        {
            $customers = $this->get_by($where, false, NULL, array('armstrong_1_customers_id', 'armstrong_2_customers_name'));
        } 
        else 
        {
            $customers = $this->get(NULL, false, NULL, array('armstrong_1_customers_id', 'armstrong_2_customers_name'));
        }

        if ($customers)
        {
            foreach ($customers as $customer) 
            {
                $output[$customer['armstrong_1_customers_id']] = $customer['armstrong_1_customers_id'] . ' - ' . $customer['armstrong_2_customers_name'];
            }
        }

        return $output;
    }
}

?>