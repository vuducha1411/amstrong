<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Questions_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'questions';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getListOptions($default = null, $where = null, $where_not_in = null, $restrict = false)
    {
        $output = array();

        if ($default) {
            $output[''] = $default;
        }

        $user = $this->session->userdata('user_data');

        $data = $this->get_by($where, false, null, array(
            'id',
            'title',
            'country_id'
        ));
        if ($data) {
            foreach ($data as $post) {
                $output[$post['id']] = $post['id'] . ' - ' . $post['title'];
            }
        }
        return $output;
    }
}

?>