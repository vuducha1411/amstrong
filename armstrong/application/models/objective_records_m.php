<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Objective_records_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'objective_records';
    protected $_primary_key = 'armstrong_2_objective_records_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'date_sync';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }
}

?>