<?php

class Customers_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'customers';
    protected $_primary_key = 'armstrong_2_customers_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_customers_id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getCustomerListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL) 
        {
            $customers = $this->get_by($where, false, NULL, array('armstrong_2_customers_id', 'armstrong_2_customers_name'));
        } 
        else 
        {
            $customers = $this->get(NULL, false, NULL, array('armstrong_2_customers_id', 'armstrong_2_customers_name'));
        }

        if ($customers)
        {
            foreach ($customers as $customer) 
            {
                $output[$customer['armstrong_2_customers_id']] = $customer['armstrong_2_customers_id'] . ' - ' . $customer['armstrong_2_customers_name'];
            }
        }

        return $output;
    }

    public function getCustomerDetails($customer_id)
    {
        /* used for webshop integration */
        /*if($where != NULL){
            $this->db->where($where);
        }

        $result = $this->db->select($select)
            ->from($this->_table_name)
            ->join($tblJoinTo, $this->_table_name.".".$con, $joinType)
            ->where($where)
            ->get()
            ->result();*/
        $query = "SELECT 
            c.`armstrong_2_customers_id`,
            c.`armstrong_2_customers_name`,
            c.`channel`,
            ch.`name` AS channel_name,
            c.`customers_types_id`,
            ct.`name` AS customers_type_name,
            c.`business_type`,
            bt.`name` AS business_type_name,
            c.`global_channels_id`,
            gc.`name` AS global_channels_name
            FROM
            customers c 
            LEFT JOIN country_channels ch 
            ON c.`channel` = ch.`id` 
            LEFT JOIN customers_types ct 
            ON c.`customers_types_id` = ct.`id` 
            LEFT JOIN business_types bt 
            ON c.`business_type` = bt.`id` 
            LEFT JOIN global_channels gc 
            ON c.`global_channels_id` = gc.`id` 
            WHERE armstrong_2_customers_id = '" . $customer_id . "'";

        $result = $this->db->query($query)->result();
        return $result;
    }
}

?>