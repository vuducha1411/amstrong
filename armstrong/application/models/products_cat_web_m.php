<?php

class Products_cat_web_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'products_cat_web';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getListOptions($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL)
        {
            $datas = $this->get_by($where, false, NULL, array('id', 'name'));
        }
        else
        {
            $datas = $this->get(NULL, false, NULL, array('id', 'name'));
        }

        if ($datas)
        {
            foreach ($datas as $data)
            {
                $output[$data['id']] = $data['name'];
            }
        }

        return $output;
    }

}

?>