<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Media_m extends AMI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'media';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('media_ref_m');
    }

    public function checkMedia()
    {

    }

    public function addMedia($class, $entry_type, $country_id, $name, $filename_array = null, $filesize = null, $admins_id = 0)
    {
        $data = array(
            'class' => $class,
            'entry_type' => $entry_type,
            'name' => $name,
            'filename_old' => (!empty($filename_array[0])) ? $filename_array[0] : null,
            'path' => (!empty($filename_array[1])) ? $filename_array[1] : null,
            'file_size' => $filesize,
            'country_id' => $country_id,
            'admins_id' => $admins_id,
            'date_created' => date('Y-m-d H:i:s'),
            'last_updated' => date('Y-m-d H:i:s'),
            'date_sync' => date('Y-m-d H:i:s')
        );
        $insert = $this->db->insert('media', $data);
        if ($insert) return $this->db->insert_id();
        return false;
    }

    public function updateMediaRef($data, $where)
    {
        return $this->db->update('media_ref', $data, $where);
    }

    public function getMedia($class, $type, $country_id, $entry_id = null, $group_by = false)
    {
        $mer_country = array(12, 13, 14, 15, 16, 17);
        $this->db->where('class', $class);
        if (empty($entry_id)) {
            if ($country_id != '') {
                // MER country can share media
                if(in_array($country_id, $mer_country) && $class != 'video'){
                    $this->db->where('entry_type', $type)->where("is_draft", 0)->where_in('country_id', $mer_country);
                }else{
                    $this->db->where('entry_type', $type)->where("is_draft", 0)->where('country_id', $country_id);
                }
            } else {
                $this->db->where('entry_type', $type)->where("is_draft", 0);
            }
            $query = $this->db->get('media');
        } else {
            $this->db->select('*');
            $this->db->from('media_ref');
            $this->db->join('media', "media.ID = media_ref.media_ID", 'right');
            if ($group_by) {
                if ($country_id != '') {
                    // MER country can share media
                    if(in_array($country_id, $mer_country)){
                        $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0)->where_in('media.country_id', $mer_country)->group_by("media_ref.media_ID");
                    }else{
                        $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0)->where('media.country_id', $country_id)->group_by("media_ref.media_ID");
                    }
                } else {
                    $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0)->group_by("media_ref.media_ID");
                }
            } else {
                if ($country_id != '') {
                    // MER country can share media
                    if(in_array($country_id, $mer_country)){
                        $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0)->where_in('media.country_id', $mer_country);
                    }else{
                        $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0)->where('media.country_id', $country_id);
                    }
                } else {
                    $this->db->where("media_ref.entry_id", $entry_id)->where("media_ref.entry_type", $type)->where("media_ref.is_draft", 0);
                }
            }
            $query = $this->db->get();
        }
        $ret = $query->result_array();
        $query->free_result();
        return $ret;
    }

    public function addMediaRef($data)
    {

        $data['date_sync'] = date('Y-m-d H:i:s');
        $data['date_created'] = date('Y-m-d H:i:s');
        $data['last_updated'] = date('Y-m-d H:i:s');

        $insert = $this->db->insert('media_ref', $data);
        $this->db->where('id', $data['media_id']);
        $this->db->update($this->_table_name, array('name' => null, 'last_updated' => date('Y-m-d H:i:s')));
        if ($insert) return $this->db->insert_id();
        return false;
    }

    public function deleteMediaRef($entry_type, $entry_id, $media_id = false, $delete = false)
    {
        $this->db->where('entry_type', $entry_type);
        $this->db->where('entry_id', $entry_id);
        if ($media_id) $this->db->where('media_id', $media_id);
        if ($delete == false) {
            $this->db->update('media_ref', array('is_draft' => 1, 'last_updated' => date('Y-m-d H:i:s')));
        } else {
            return $this->db->delete('media_ref');
        }
    }

    public function deleteMedia($country_id, $media_id, $delete = false)
    {
        $this->db->where('media_id', $media_id);
        $this->db->delete('media_ref');
        $this->db->where('id', $media_id)->where('country_id', $country_id);

        if ($delete == false) {
            $this->db->update('media', array('is_draft' => 1, 'last_updated' => date('Y-m-d H:i:s')));
        } else {
            return $this->db->delete('media');
        }
    }

    public function countMediaRefs($media_id)
    {
        $this->db->where('media_id', $media_id);
        return $this->db->count_all_results('media_ref');
    }
}

?>