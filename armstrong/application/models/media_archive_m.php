<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Date: 11/28/16
 */
class Media_archive_m extends AMI_Model {

    protected $_table_name = 'media_archive';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';

    public function __construct() {
        parent::__construct();
    }

    // Return all archived media by country per app_type
    public function getAllArchivePerCountry($app_type) {
        $result_array = $this->db->select
                    ('
                        country.id,
                        country.name,
                        country.code,
                        country.is_draft,
                        country.time_zone,
                        country.gmt,
                        IFNULL(media_archive.size, \'NA\') as size,
                        IFNULL(media_archive.queue, \'NA\') as queue,
                        IFNULL(media_archive.created_at, \'NA\') as created_at,
                        IFNULL(media_archive.updated_at, \'NA\') as updated_at
                    ', FALSE)
                ->from('country')
                ->join($this->_table_name, 'country.id = ' . $this->_table_name .'.country_id AND ' . $this->_table_name . '.app_type = ' . $app_type, 'left')
                ->join('media_ref', 'country.id = media_ref.country_id AND media_ref.app_type = ' . $app_type, 'left')
                ->where('country.is_draft', '0')
                ->not_like('country.name', 'test')
                ->group_by('country.id')
                ->get()
                ->result_array();

        return $result_array;
    }

    // Return all media to be archived based from country and app_type
    public function getAllMediaPerCountryKat($country_id, $app_type = 0) {
        $result_array = $this->db->select
                    ('
                        country.name,
                        media.country_id,
                        media.class as media_type,
                        media.path as filename,
                        media_ref.app_type
                    ')
                ->from('media_ref')
                ->join('media', 'media.id = media_ref.media_id', 'left')
                
                ->join('country', 'country.id = media_ref.country_id', 'left')

                ->where('media_ref.country_id', $country_id)
                ->where('media_ref.app_type', $app_type)
                ->where('media_ref.is_draft', '0')
                ->where('media.is_draft', '0')
                ->order_by('media.class', 'ASC')
                ->group_by('media_ref.media_id')
                ->get()
                ->result_array();

        return $result_array;
    }



// SELECT 
// media.id,
// media.country_id,
// media.class AS media_type,
// media.path AS filename,
// media_ref.app_type,
// media_ref.country_id
// FROM media
// LEFT JOIN media_ref ON media.id = media_ref.media_id AND media.country_id = media_ref.country_id AND media_ref.app_type = 2
// WHERE
// media.country_id = 5
// AND media.is_draft = 0
    public function getAllMediaPerCountry($country_id, $app_type = 0) {
        $result_array = $this->db->select
                    ('
                        media.id,
                        media.country_id,
                        media.class AS media_type,
                        media.path AS filename,
                        IFNULL(media_ref.app_type, \'NULL\') as app_type,
                        IFNULL(media_ref.country_id, \'NULL\') as country_id
                    ', FALSE)
                ->from('media')
                ->join('media_ref', 'media.id = media_ref.media_id AND media.country_id = media_ref.country_id AND media_ref.app_type = '. $app_type, 'left')
                ->where('media.country_id', $country_id)
                ->where('media.is_draft', '0')
                // ->order_by('media.class', 'ASC')
                ->group_by('media.id')
                ->get()
                ->result_array();
                        // IFNULL(media_ref.app_type, \'NA\') as app_type,
                        // IFNULL(media_ref.country_id, \'NA\') as media_ref_country_id

        return $result_array;
    }


    // With Non App specific
    public function getAllNoAppTypePerCountry($country_id) {
        $result_array = $this->db->select
                    ('
                        media.id,
                        media.country_id,
                        media.class AS media_type,
                        media.path AS filename,
                        IFNULL(media_ref.app_type, \'NA\') as app_type,
                        IFNULL(media_ref.counter_reset_value, \'NA\') as media_ref_country_id
                    ')
                ->from('media')
                ->join('media_ref', 'media.id = media_ref.media_id AND media.country_id = media_ref.country_id', 'left')
                ->where('media.country_id', $country_id)
                ->where('media.is_draft', 0)
                ->where('media_ref.country_id', NULL)
                ->get()
                ->result_array();

// $this->output->enable_profiler(TRUE);
        // SELECT 
        // media.id,
        // media.country_id,
        // media.class AS media_type,
        // media.path AS filename,
        // media_ref.app_type,
        // media_ref.country_id
        // FROM  `media` 
        // LEFT JOIN media_ref ON media.id = media_ref.media_id
        // AND media.country_id = media_ref.country_id
        // WHERE media.`country_id` =18
        // AND media.is_draft =0
        // AND media_ref.country_id IS NULL 
        // LIMIT 200

        return $result_array;
    }

    public function update($country_id, $app_type,  $data)
    {
        $isSuccess = $this->db->where('country_id', $country_id)
                ->where('app_type', $app_type)
                ->update($this->_table_name, $data);

        return $this->db->affected_rows() ? TRUE : FALSE;
    }

    public function save($country_id, $app_type, $data)
    {
        $insert = $this->db->insert($this->_table_name, $data);

        return $this->db->affected_rows() ? TRUE : FALSE;
    }

    public function isMediaInDB($country_id, $app_type) {
        $object = $this->db->select('id', FALSE)
            ->from($this->_table_name)
            ->where('country_id', $country_id)
            ->where('app_type', $app_type)
            ->get()
            ->row();

        return (count((array)$object) > 0) ? TRUE : FALSE;
    }
}
?>