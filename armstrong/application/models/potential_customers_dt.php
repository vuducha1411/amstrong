<?php

class Potential_customers_dt extends Potential_customers_m implements DatatableModel {

    public function appendToSelectStr() {
        return array(
            'salesperson_name' => "CONCAT( s.first_name,s.last_name )"
        );
    }

    public function fromTableStr() {
        return 'potential_customers c';
    }

    public function joinArray(){
        return array(
            'salespersons s' => 's.armstrong_2_salespersons_id = c.armstrong_2_salespersons_id'
        );
    }
}