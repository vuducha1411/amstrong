<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pantry_check_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'pantry_check';
    protected $_primary_key = 'armstrong_2_pantry_check_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_pantry_check_id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getPantryCheck($condition, array $limit)
    {
        if (is_array($limit))
        {
            list($total, $offset) = $limit;
        }
        else
        {
            $total = $limit;
            $offset = 0;
        }

        return $this->db->select($this->_table_name . '.*')
            ->from($this->_table_name)
            ->join('call_records', $this->_table_name . '.armstrong_2_call_records_id = call_records.armstrong_2_call_records_id')
            ->where($condition)
            ->limit($total, $offset)
            ->get()->result_array();
    }

    public function countPantryCheck($rule, $condition)
    {
        return $this->db->from($this->_table_name)
            ->join('call_records', $this->_table_name . '.armstrong_2_call_records_id = call_records.armstrong_2_call_records_id')
            ->$rule($condition)
            ->count_all_results();
    }
}