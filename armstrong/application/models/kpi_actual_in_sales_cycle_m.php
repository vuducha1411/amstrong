<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kpi_actual_in_sales_cycle_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    protected $_table_name = 'kpi_actual_in_sales_cycle';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }   

}

?>