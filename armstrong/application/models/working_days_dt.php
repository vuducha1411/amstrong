<?php

class Working_days_dt extends Working_days_m implements DatatableModel {

    public function appendToSelectStr() {
        return array(
            'salesperson_name' => "CONCAT( s.first_name,'&nbsp;',s.last_name )"
        );
    }

    public function fromTableStr() {
        return 'working_days c';
    }

    public function joinArray(){
        return array(
            'salespersons s' => 's.armstrong_2_salespersons_id = c.armstrong_2_salespersons_id'
        );
    }
}