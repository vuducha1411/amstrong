<?php

class Chains_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'chains';
    protected $_primary_key = 'armstrong_2_chains_id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'armstrong_2_chains_id';
    protected $_order_rule = 'DESC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getChainListOption($default = null, $where = null)
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL)
        {
            $chains = $this->get_by($where, false, NULL, array('armstrong_2_chains_id', 'armstrong_2_chains_name'));
        }
        else
        {
            $chains = $this->get(NULL, false, NULL, array('armstrong_2_chains_id', 'armstrong_2_chains_name'));
        }

        if ($chains)
        {
            foreach ($chains as $chain)
            {
                $output[$chain['armstrong_2_chains_id']] = $chain['armstrong_2_chains_id'] . ' - ' . $chain['armstrong_2_chains_name'];
            }
        }

        return $output;
    }
}

?>