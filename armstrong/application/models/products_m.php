<?php

class Products_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'products';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'trim';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function getProductListOptions($default = null, $where = null, $key = 'sku_number')
    {
        $output = array();

        if ($default)
        {
            $output[''] = $default;
        }

        if ($where != NULL) 
        {
            $products = $this->get_by($where, false, 'sku_name');
        } 
        else 
        {
            $products = $this->get(NULL, false, NULL);
        }

        if ($products)
        {
            $title = in_array($this->country_code, array('tw', 'hk')) ? 'name_alt' : 'sku_name';

            foreach ($products as $product) 
            {
                // $output[$product[$key]] = "{$product['sku_number']} - {$product['sku_name']} ({$product['quantity_case']}x{$product['weight_pc']})";
                $output[$product[$key]] = get_product_label($product, $title);
            }
        }
        return $output;
    }
}

?>