<?php

class Infographic_model extends CI_Model
{
    const CALL_RECORD = 'report_call_record';
    const DATA = 'report_data';
    const TYPE = 'report_with_type';
    const GRIP_GRAB = 'report_grip_grab';
    const TOP_SKU = 'top_sku_on_month';

    protected static $_tableType = '';

    public function getReportAllInOne($countryId, $where = array(), $month = null, $year = null)
    {
        $this->db->select('a.armstrong_2_salespersons_id,a.salespersons_manager_id, a.totalGrip, a.totalGrab, a.grip_otm_a, a.grip_otm_b, a.grip_otm_c, a.grip_otm_d, a.grip_otm_u');

        if ($countryId) {
            $this->db->where('a.country_id', intval($countryId));

        }
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('a.month_report', intval($month));
        $this->db->where('a.year_report', intval($year));

        $this->db->where('a.app_type', 0);
        $this->db->where('b.active', 1);
        $this->db->where('a.total_call >', 0);
        $this->db->where('b.test_acc', 0);
        //$this->db->where('((b.email NOT LIKE "%test%" AND (b.first_name NOT LIKE "%test%" OR b.last_name NOT LIKE "%test%")) AND b.first_name NOT LIKE "%duyen%" AND b.first_name NOT LIKE "%dummy%")', NULL);
        $this->db->join('salespersons as b', 'a.armstrong_2_salespersons_id = b.armstrong_2_salespersons_id', 'inner');
        $data = $this->db->get('report_all_in_one as a')->result_array();
        $data_temp = array();
        $results['sale'] = 0;
        $results['leader'] = 0;
        if (count($data) > 0) {
            $data_temp['totalGrip'] = $data_temp['totalGrab'] = $data_temp['customerOtmA'] = $data_temp['customerOtmB'] = $data_temp['customerOtmC'] = $data_temp['customerOtmD'] = $data_temp['customerOtmU'] = 0;
            foreach ($data as $d) {
                $data_temp['sale'][$d['armstrong_2_salespersons_id']] = 1;
                // TODO remove comment to hide group unassigned
                //if($d['salespersons_manager_id'] != ''){
                $data_temp['leader'][$d['salespersons_manager_id']] = 1;
                //}
                $data_temp['totalGrip'] += $d['totalGrip'];
                $data_temp['totalGrab'] += $d['totalGrab'];
                $data_temp['customerOtmA'] += $d['grip_otm_a'];
                $data_temp['customerOtmB'] += $d['grip_otm_b'];
                $data_temp['customerOtmC'] += $d['grip_otm_c'];
                $data_temp['customerOtmD'] += $d['grip_otm_d'];
                $data_temp['customerOtmU'] += $d['grip_otm_u'];
            }
            $results['numberSalesPersonnels'] = count($data_temp['sale']);
            $results['numberSalesLeaders'] = count($data_temp['leader']);
//            $results['totalGrip'] = $data_temp['totalGrip'];
            $results['totalGrip'] = $data_temp['customerOtmA'] + $data_temp['customerOtmB'] + $data_temp['customerOtmC'] + $data_temp['customerOtmD']+$data_temp['customerOtmU'];
            $results['totalGrab'] = $data_temp['totalGrab'];
            $results['customerOtmA'] = $data_temp['customerOtmA'];
            $results['customerOtmB'] = $data_temp['customerOtmB'];
            $results['customerOtmC'] = $data_temp['customerOtmC'];
            $results['customerOtmD'] = $data_temp['customerOtmD'];
            $results['customerOtmU'] = $data_temp['customerOtmU'];
        }
        return $results;
    }

    public function getData($table, $countryId, $where = array(), $month = null, $year = null, $select = null)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $result = 'result_array';
        if ($countryId) {
            if ($select) {
                $this->db->select($select)->where('countryId', intval($countryId));
            } else {
                $this->db->where('countryId', intval($countryId));
            }
            $result = 'row_array';
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('monthReport', intval($month));
        $this->db->where('yearReport', intval($year));

        return $this->db->get($this->_getTableName($table))->$result();
    }

    public function getGripGrab($countryId, $where = array(), $month = null, $year = null)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if ($countryId) {
            $this->db->where('countryId', intval($countryId));
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('monthReport', intval($month));
        $this->db->where('yearReport', intval($year));

        $this->db->order_by('totalGrip', 'desc');

        return $this->db->get($this->_getTableName(static::GRIP_GRAB))->result_array();
    }

    public function getGripByTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());


        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate -1 day" ));
        $enddate = ("'" . $enddate . "'");

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");


        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $sql = "select count(distinct armstrong_2_customers_id) as totalGrip, count(sku_number) as totalGrab, channel as countryChannels, cast(cc.name as CHAR(100)) as countryChannelsName
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number, c.channel  
                    from tfo_details as a
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created between " . $fromdate . " and " . $enddate . ") ".$salesID."

                    union 

                    select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                    FROM pantry_check_details as pc 
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between " . $fromdate . " and " . $enddate . "))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                        ".$salesID."


                    union

                    SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                    FROM ssd as a  
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId} 
                    AND (a.ssd_date between " . $fromdate . " and " . $enddate . ")
                    ".$salesID."
                ) a left join country_channels cc ON a.channel = cc.id
                group by channel
                order by count(distinct armstrong_2_customers_id) desc, count(sku_number) desc";

        return $this->db->query($sql)->result_array();
    }

    public function getOtherGripByTeam(array $ids, $countryId, $month = null, $year = null, $srID, $slID)
    {
        if (!$ids) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate -1 day" ));
        $enddate = ("'" . $enddate . "'");

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");

        $ids = implode(',', $ids);

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
 
        $sql = "select ifnull(sum(totalGrip),0) as totalGrip, ifnull(sum(totalGrab),0) as totalGrab
                from(
                    select count(distinct armstrong_2_customers_id) as totalGrip, count(sku_number) as totalGrab
                    FROM(
                        select a.armstrong_2_customers_id, a.sku_number, c.channel  
                        from tfo_details as a
                        join salespersons as sp
                        ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                        INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                        AND (a.date_created between " . $fromdate . " and " . $enddate . ") ".$salesID."

                        union 

                        select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                        FROM pantry_check_details as pc 
                        INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between " . $fromdate . " and " . $enddate . "))  
                        INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                        INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                        WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                            ".$salesID."


                        union

                        SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                        FROM ssd as a  
                        LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                        INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                        WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                        AND a.country_id={$countryId} 
                        AND (a.ssd_date between " . $fromdate . " and " . $enddate . ")
                        ".$salesID."
                    ) a left join country_channels cc ON a.channel = cc.id
                    where channel not in ($ids)
                    group by channel
                    order by count(distinct armstrong_2_customers_id) desc, count(distinct sku_number) desc
                ) x";

        return $this->db->query($sql)->row_array();
    }

    public function getOtherGripBreakdownForTeam(array $ids, $countryId, $month = null, $year = null, $srID, $slID)
    {
        if (!$ids) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate -1 day" ));
        $enddate = ("'" . $enddate . "'");

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");

        $ids = implode(',', $ids);

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $sql = "select count(distinct armstrong_2_customers_id) as totalGrip, count(sku_number) as totalGrab, channel as countryChannels, cc.name as countryChannelsName
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number, c.channel  
                    from tfo_details as a
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created between " . $fromdate . " and " . $enddate . ") ".$salesID."

                    union 

                    select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                    FROM pantry_check_details as pc 
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between " . $fromdate . " and " . $enddate . "))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                        ".$salesID."


                    union

                    SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                    FROM ssd as a  
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId} 
                    AND (a.ssd_date between " . $fromdate . " and " . $enddate . ")
                    ".$salesID."
                ) a left join country_channels cc ON a.channel = cc.id
                where channel not in ($ids)
                group by channel
                order by count(distinct armstrong_2_customers_id) desc, count(distinct sku_number) desc";

        return $this->db->query($sql)->result_array();
    }

    public function getGripGrabByTeamChannel($countryId, $month, $year, $srID, $slID, $channel){
        

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate -1 day" ));
        $enddate = ("'" . $enddate . "'");

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $qry = "select count(distinct armstrong_2_customers_id) as totalGrip, count(sku_number) as totalGrab, channel as countryChannels, cc.name as countryChannelsName
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number, c.channel  
                    from tfo_details as a
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created between " . $fromdate . " and " . $enddate . ") ".$salesID."

                    union 

                    select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                    FROM pantry_check_details as pc 
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between " . $fromdate . " and " . $enddate . "))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                        ".$salesID."


                    union

                    SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                    FROM ssd as a  
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId} 
                    AND (a.ssd_date between " . $fromdate . " and " . $enddate . ")
                    ".$salesID."
                ) a left join country_channels cc ON a.channel = cc.id
                where a.channel = '".$channel."'
                group by channel
                order by count(distinct armstrong_2_customers_id) desc, count(distinct sku_number) desc";

        return $this->db->query($qry)->result_array();
    }

    public function getOtherGripGrab(array $ids, $countryId, $month = null, $year = null)
    {
        if (!$ids) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(totalGrip) AS totalGrip,
            SUM(newGrip) AS  newGrip,
            SUM(totalGrab) AS  totalGrab,
            SUM(newGrab) AS  newGrab
        FROM " . $this->_getTableName(static::GRIP_GRAB) . "
        WHERE countryId = {$countryId} 
            AND monthReport = {$month} 
            AND yearReport = {$year} 
            AND countryChannels NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getOtherGripGrabBreakdown(array $ids, $countryId, $month = null, $year = null)
    {
        if (!$ids) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());
        
        $ids = implode(',', $ids);

        $sql = "SELECT 
            totalGrip AS totalGrip,
            newGrip AS  newGrip,
            totalGrab AS  totalGrab,
            newGrab AS  newGrab,
            countryChannels,
            countryChannelsName
        FROM " . $this->_getTableName(static::GRIP_GRAB) . "
        WHERE countryId = {$countryId} 
            AND monthReport = {$month} 
            AND yearReport = {$year} 
            AND countryChannels NOT IN ($ids)
            AND id > 0";

        return $this->db->query($sql)->result_array();
    }

    public function getTopSku($countryId, $month = null, $year = null)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());
        $this->db->join('products p', 'p.sku_number = ' . static::TOP_SKU . '.skuNumber and p.country_id = ' . intval($countryId), 'left');
        if ($countryId) {
            $this->db->where('countryId', intval($countryId));
        }
        $this->db->where('monthReport', intval($month));
        $this->db->where('yearReport', intval($year));
      //$this->db->where(array('rangeValue' => NULL));
        $this->db->where('CHAR_LENGTH(skuNname) !=', '');
         $this->db->where('is_draft', 0);
        if (static::$_tableType) {
            $this->db->where('type', 2);
        } else {
            $this->db->where('type', 1);
        }
        $this->db->order_by('(totalCustomerOrder)', 'desc');
        //$this->db->limit(10);
        return $this->db->get(static::TOP_SKU)->result_array();
    }

    public function getTopSkuLastYearOld($countryId, $month = null, $year = null, array $skuNumber)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());
        $this->db->join('products p', 'p.sku_number = ' . static::TOP_SKU . '.skuNumber', 'left');
        if ($countryId) {
            $this->db->where('countryId', intval($countryId));
        }

        $this->db->where('monthReport', intval($month));
        $this->db->where('yearReport', intval($year));
        $this->db->where('CHAR_LENGTH(skuNname) !=', '');
        $this->db->where_in('skuNumber', $skuNumber);
        if (static::$_tableType) {
            $this->db->where('type', 2);
        } else {
            $this->db->where('type', 1);
        }
        $this->db->order_by('(totalCustomerOrder)', 'desc');
        //$this->db->limit(10);
        return $this->db->get(static::TOP_SKU)->result_array();
    }

    public function getTopSkuLastYear($countryId, $month = null, $year = null, array $skuNumber)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());


        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate" ));
        $enddate = ("'" . $enddate . "'");

        

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");

        //debug::printArrDie($fromdate . " " .$enddate);

        $skuNumber = implode(',', $skuNumber);

        $sku = (" AND p.sku_number IN (".$skuNumber.")");

        $sql = "select id, countryId, monthReport, yearReport, skuNumber, mainSku, skuNname, pakingSize, numberCustomerOder, totalCustomer, type, timeUpdated, count(distinct armstrong_2_customers_id) as totalCustomerOrder
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number  
                    from tfo_details as a
                    join products as p
                    on a.sku_number = p.sku_number
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created_tfo >= " . $fromdate . " and a.date_created_tfo < " . $enddate . ") ".$sku."

                    union 

                    select pc.armstrong_2_customers_id, pc.sku_number 
                    FROM pantry_check_details as pc 
                    join products as p
                    on pc.sku_number = p.sku_number
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start >= " . $fromdate . " and a.datetime_call_start <" . $enddate . "))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                        ".$sku."


                    union

                    SELECT a.armstrong_2_customers_id, a.sku_number 
                    FROM ssd as a 
                    join products as p
                    on a.sku_number = p.sku_number
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId} 
                    AND (a.ssd_date >= " . $fromdate . " and a.ssd_date < " . $enddate . ")
                    ".$sku."
                ) a left join top_sku_on_month p on a.sku_number = p.skuNumber
                group by p.mainSku
                order by count(distinct armstrong_2_customers_id) desc, count(p.skuNumber) desc";

        return $this->db->query($sql)->result_array();
    }


    //revised top sku grip fetching for previous year
    public function getTopSkuGripLastYear($countryId, $month = null, $year = null, array $skuNumber)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());


        $enddate = new DateTime();
        $enddate->setDate($year, $month+1, 1);
        $enddate =  $enddate->format('Y-m-d');
        $enddate = date( "Y-m-d", strtotime( "$enddate" ));
        $enddate = ("'" . $enddate . "'");

        //debug::printArrDie($enddate);

        $fromdate = new DateTime();
        $fromdate->setDate($year, $month, 1);
        $fromdate =  $fromdate->format('Y-m-d');
        $fromdate = date( "Y-m-d", strtotime( "$fromdate -5 months" ));
        $fromdate = ("'" . $fromdate . "'");

        $skuNumber = implode(',', $skuNumber);

        $sku = (" AND p.main_sku IN (".$skuNumber.") ");

        $sql = "select main_sku as skuNumber, count(distinct armstrong_2_customers_id) as totalCustomerOrder FROM(
                    select a.armstrong_2_customers_id, a.sku_number, p.main_sku   
                    from tfo_details as a
                    join products_main as p
                    on a.sku_number = p.sku_number
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created_tfo >= " . $fromdate . " and a.date_created_tfo < " . $enddate . ") ".$sku."

                    union 

                    select pc.armstrong_2_customers_id, pc.sku_number, p.main_sku 
                    FROM pantry_check_details as pc 
                    join products_main as p
                    on pc.sku_number = p.sku_number
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start >= " . $fromdate . " and a.datetime_call_start <" . $enddate . "))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                        ".$sku."


                    union

                    SELECT a.armstrong_2_customers_id, a.sku_number, p.main_sku 
                    FROM ssd as a 
                    join products_main as p
                    on a.sku_number = p.sku_number
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId} 
                    AND (a.ssd_date >= " . $fromdate . " and a.ssd_date < " . $enddate . ")
                    ".$sku."
                ) a group by main_sku ";

        return $this->db->query($sql)->result_array();
    }


    public function getTopSkuByTeam($countryId, $month = null, $year = null, $srID, $slID, array $skuNumber){
        if (!$skuNumber) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $skuNumber = implode(',', $skuNumber);

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND p.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        
        $sql = "SELECT sku_number as skuNumber, sum(p) as totalCustomerOrder, (select count(armstrong_2_customers_id) from salespersons s left join customers p on s.`armstrong_2_salespersons_id` = p.`armstrong_2_salespersons_id` where p.active=1 and p.country_id={$countryId} ".$salesID.") as totalCustomer
                from
                ((select sku_number, count(armstrong_2_customers_id) as p
                from salespersons s 
                left join pantry_check_details p on s.`armstrong_2_salespersons_id` = p.`armstrong_2_salespersons_id`
                where p.app_type=0 and sku_number in ({$skuNumber})
                and p.country_id={$countryId}
                and month(p.date_created)={$month}
                and year(p.date_created)={$year}
                ".$salesID."
                group by sku_number)
                union
                (select sku_number, count(armstrong_2_customers_id) as p
                from salespersons s 
                left join tfo_details p on s.`armstrong_2_salespersons_id` = p.`armstrong_2_salespersons_id` and s.is_draft=0 and s.test_acc=0
                where p.app_type=0 and sku_number in ({$skuNumber})
                and p.country_id={$countryId}
                and month(p.date_created)={$month}
                and year(p.date_created)={$year}
                ".$salesID."
                group by sku_number)) as p
                group by sku_number";

        return $this->db->query($sql)->result_array();
    }

    public function getAllSku($countryId, $month = null, $year = null, array $skuNumber){
        if (!$skuNumber) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $skuNumber = implode(',', $skuNumber);
        
        $sql = "SELECT main_sku as mainSku, sku_number as skuNumber, sku_name as skuName, quantity_case, weight_pc
                FROM products
                WHERE main_sku IN ({$skuNumber})
                AND country_id = {$countryId}";

        return $this->db->query($sql)->result_array();
    }

    public function getActiveGripByTeam($countryId, $month = null, $year = null, $srID, $slID){
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        
        $sql = "SELECT sum(totalGrip) as activeGrip,
                sum(totalGrab) as activeGrab
                FROM (report_all_in_one as a)
                WHERE `a`.`country_id` = {$countryId} 
                AND `a`.`app_type` = 0 
                AND `a`.`month_report` = {$month} 
                AND `a`.`year_report` = '".$year."' ".$salesID;
                //AND `a`.`active` = 1
                 //AND `a`.`total_call` > 0 
                 //AND `b`.`test_acc` = 0"

        return $this->db->query($sql)->result_array();
    }

    public function getActiveGrip($countryId, $month = null, $year = null){
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());
        
        $sql = "SELECT sum(totalGrip) as activeGrip,
                sum(totalGrab) as activeGrab
                FROM (report_all_in_one as a)
                WHERE `a`.`country_id` = {$countryId} 
                AND `a`.`app_type` = 0 
                AND `a`.`month_report` = {$month} 
                AND `a`.`year_report` = {$year}";
                //AND `a`.`active` = 1
                 //AND `a`.`total_call` > 0 
                 //AND `b`.`test_acc` = 0"

        return $this->db->query($sql)->result_array();
    }

    public function getCustomerData($countryId, $month = null, $year = null)
    {
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if ($countryId) {
            $this->db->where('countryId', intval($countryId));
        }

        $this->db->where('monthReport', intval($month));
        $this->db->where('yearReport', intval($year));
        $this->db->where('groupsTypes', 2);

        $this->db->order_by('valueData', 'desc');
        $this->db->order_by('typeName', 'asc');

        return $this->db->get($this->_getTableName(static::TYPE))->result_array();
    }

    public function getOtherCustomerData(array $ids, $countryId, $month = null, $year = null)
    {
        if (!$ids) {
            echo $this->load->view('infographic/no_data', '', true);
            die();
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $ids = implode(',', $ids);

        $sql = "SELECT
            SUM(quantity) AS totalTfo,
            SUM(valueData) AS  totalValue
        FROM " . $this->_getTableName(static::TYPE) . "
        WHERE countryId = {$countryId}
            AND monthReport = {$month}
            AND yearReport = {$year}
            AND groupsTypes = 2
            AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallCriteria($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(call_count) AS callCount,
            SUM(perfect_calls) AS perfectCalls,
            SUM(sampling_count) AS  samplingCount,
            SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount
        FROM perfect_call_infographic
        WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = {$year}"; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallCriteriaForTeam($countryId, $month = null, $year = null, $srID, $slID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND p.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(call_count) AS callCount,
            SUM(perfect_calls) AS perfectCalls,
            SUM(sampling_count) AS  samplingCount,
            SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount
        FROM perfect_call_infographic p
        INNER JOIN salespersons_history s ON p.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
        WHERE app_type = 0
            {$sStatus}
            AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
            AND p.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            /*AND id NOT IN ($ids)";*/
        
        return $this->db->query($sql)->row_array();
    }

    public function getSalesTeamPersons($countryId, $month = null, $year = null, $slID, $srID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}
        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        }

        if ($slID == "" && $srID != ""){
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(armstrong_2_salespersons_id) as srCount,
                IFNULL(month_report,{$month}) as monthReport
                FROM report_all_in_one
                WHERE country_id = {$countryId}
                AND app_type = 0
                AND active = 1
                AND total_call > 0
                AND month_report = {$month} 
                AND year_report = '".$year."' ".$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }

    public function getSalesTeamReps($countryId, $month = null, $year = null, $slID, $srID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}
        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        }

        if ($srID != ""){
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';
        
        if ($status != 2){
            $sStatus = (" AND active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(distinct armstrong_2_salespersons_id) as srCount,
                IFNULL(month(from_date),{$month}) as monthReport
                FROM salespersons_history
                WHERE country_id = {$countryId}
                {$sStatus}
                AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
                AND type != 'PUSH'
                AND is_draft=0" .$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }



    public function getSalesTeamLeader($countryId, $month = null, $year = null, $slID, $srID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        
        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        }

        if ($srID != ""){
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(DISTINCT r.salespersons_manager_id) as slCount,
                IFNULL(month_report,{$month}) as monthReport
                FROM report_all_in_one r
                LEFT JOIN salespersons_history s ON r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE r.country_id = {$countryId}
                AND r.app_type =0
                {$sStatus}
                AND total_call > 0
                AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
                AND month_report = {$month} 
                AND year_report = '".$year."' ".$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }



    public function getTotalSalesForTeam($countryId, $month = null, $year = null, $slID, $srID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}
        
        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        }

        if ($srID != ""){
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        }
        
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT SUM(`total_amount`) as totalAmount
                FROM report_all_in_one r
                LEFT JOIN salespersons_history s ON r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE r.country_id = {$countryId}
                AND r.app_type =0
                {$sStatus}
                AND total_call > 0
                AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
                AND month_report = {$month} 
                AND year_report = '".$year."' ".$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }

    public function getWholesalers($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $nextMonth = $month+1;
        $year = $year ? $year : date('Y', time());
        if ($nextMonth<10)
            $day = $year . "-0" . $nextMonth . "-01";
        else
            $day = $year . "-" . $nextMonth . "-01";
        

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(  `armstrong_2_wholesalers_id` ) as wholesalers
                FROM  `wholesalers` w
                LEFT JOIN salespersons s ON w.`armstrong_2_salespersons_id` = s.`armstrong_2_salespersons_id` and s.test_acc=0
                WHERE w.active =1
                AND w.is_draft =0
                AND w.country_id = {$countryId}
                AND w.date_created  < '".$day."' "; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->result_array();
    }

    public function getWholesalersForTeam($countryId, $month = null, $year = null, $slID, $srID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND s.salespersons_manager_id = '".$slID."'");
        }

        if ($slID == "" && $srID != ""){
            $salesID = (" AND w.armstrong_2_salespersons_id = '".$srID."'");
        }

        $month = $month ? $month : date('m', time());
        $nextMonth = $month+1;
        $year = $year ? $year : date('Y', time());
        if ($nextMonth<10)
            $day = $year . "-0" . $nextMonth . "-01";
        else
            $day = $year . "-" . $nextMonth . "-01";

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }
        

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(  `armstrong_2_wholesalers_id` ) as wholesalers
                FROM  `wholesalers` w
                LEFT JOIN salespersons_history s ON w.`armstrong_2_salespersons_id` = s.`armstrong_2_salespersons_id` and s.test_acc=0
                WHERE w.is_draft =0
                {$sStatus}
                AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
                AND w.country_id = {$countryId}
                AND w.date_created  < '".$day."'  ".$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->result_array();
    }

    public function getDistributors($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $nextMonth = $month+1;
        $year = $year ? $year : date('Y', time());
        if ($nextMonth<10)
            $day = $year . "-0" . $nextMonth . "-01";
        else
            $day = $year . "-" . $nextMonth . "-01";
        

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(  `armstrong_2_distributors_id` ) as distributors
                FROM  `distributors` w
                LEFT JOIN salespersons s ON w.`armstrong_2_salespersons_id` = s.`armstrong_2_salespersons_id` and s.test_acc=0
                WHERE w.active =1
                AND w.is_draft =0
                AND w.country_id = {$countryId}
                AND w.date_created  < '".$day."' "; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->result_array();
    }

    public function getDistributorsForTeam($countryId, $month = null, $year = null, $slID, $srID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID == "") {
            $salesID = ("");
        } else {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        }

        if ($slID == "" && $srID != ""){
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        }

        $month = $month ? $month : date('m', time());
        $nextMonth = $month+1;
        $year = $year ? $year : date('Y', time());
        if ($nextMonth<10)
            $day = $year . "-0" . $nextMonth . "-01";
        else
            $day = $year . "-" . $nextMonth . "-01";                

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }
        

        //$ids = implode(',', $ids);

        $sql = "SELECT COUNT(  `armstrong_2_distributors_id` ) as distributors
                FROM  `distributors` w
                WHERE w.active =1
                {$sStatus}
                AND w.is_draft =0
                AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
                AND w.country_id = {$countryId}
                AND w.date_created  < '".$day."'  ".$salesID; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->result_array();
    }

    public function getCallCoverageByCountry($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(totalCall) AS totalCalls,
            SUM(timesCallOtmA) AS otmACalls,
            SUM(timesCallOtmB) AS  otmBCalls,
            SUM(timesCallOtmC) AS  otmCCalls,
            SUM(timesCallOtmD) AS  otmDCalls,
            SUM(timesCallOtmU) AS  otmUCalls,
            SUM(visitedOtmA) AS otmAVisits,
            SUM(visitedOtmB) AS otmBVisits,
            SUM(visitedOtmC) AS otmCVisits,
            SUM(visitedOtmD) AS otmDVisits,
            SUM(visitedOtmU) AS otmUVisits,
            SUM(minuteCallOtmA) AS otmACallMins,
            SUM(minuteCallOtmB) AS otmBCallMins,
            SUM(minuteCallOtmC) AS otmCCallMins,
            SUM(minuteCallOtmD) AS otmDCallMins,
            SUM(minuteCallOtmU) AS otmUCallMins,
            SUM(customerOtmA) AS otmACount,
            SUM(customerOtmB) AS otmBCount,
            SUM(customerOtmC) AS otmCCount,
            SUM(customerOtmD) AS otmDCount,
            SUM(customerOtmU) AS otmUCount,
            workingDay,
            SUM(totalSalespersons) AS totalSalespersons,
            SUM(totalActualWorking) AS totalActualWorking
           /* SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount*/
            FROM report_call_record
        WHERE appType = 0
            AND countryId = {$countryId} 
            AND monthReport = {$month}
            AND yearReport = {$year}
            order by timeUpdated desc
            ";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getHolidays($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT count(`title`) as holidays
            FROM `holiday`
            WHERE country_id = {$countryId} 
            AND month(date) = {$month} 
            AND year(date) = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getStandardWorkingHours($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        $date = new DateTime();
        $date->setDate($year, $month+1, 1);
        $date =  $date->format('Y-m-d');
        $date = date( "Y-m-d", strtotime( "$date -1 day" ));
        $date = ("'" . $date . "'");

        //$ids = implode(',', $ids);

        $sql = "SELECT no_of_working_hours_per_day as numWorkingHours
            FROM `out_of_trade`
            WHERE country_id = {$countryId} 
            AND is_draft=0
            order by last_updated desc
            limit 0,1";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getAvgWorkingHours($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT sum(r.actual_woking_days) as totalWorkingDays, count(a.date) as leaves
            FROM report_all_in_one r 
            LEFT JOIN absent_day a ON r.armstrong_2_salespersons_id=a.armstrong_2_salespersons_id AND (month(a.date) = {$month} AND year(a.date) = {$year}) and a.type = 2
            WHERE app_type = 0
            AND r.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }



    public function getAvgWorkingHoursByTeam($countryId, $month = null, $year = null, $srID, $slID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT sum(r.actual_woking_days) as totalWorkingDays, count(a.date) as leaves
            FROM report_all_in_one r 
            LEFT JOIN salespersons_history s ON r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
            LEFT JOIN absent_day a ON r.armstrong_2_salespersons_id=a.armstrong_2_salespersons_id AND (month(a.date) = {$month} AND year(a.date) = {$year}) and a.type = 2
            WHERE r.app_type = 0 {$sStatus}
            AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
            AND r.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getOtmVisitsAndCount($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(visited_otm_a) AS otmAVisits,
            SUM(visited_otm_b) AS otmBVisits,
            SUM(visited_otm_c) AS otmCVisits,
            SUM(visited_otm_d) AS otmDVisits,
            SUM(visited_otm_na) AS otmUVisits,
            SUM(number_customer_otm_a) AS otmACount,
            SUM(number_customer_otm_b) AS otmBCount,
            SUM(number_customer_otm_c) AS otmCCount,
            SUM(number_customer_otm_d) AS otmDCount,
            SUM(number_customer_otm_na) AS otmUCount
        FROM report_all_in_one 
            WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getOtmVisitsAndCountByTeam($countryId, $month = null, $year = null, $srID, $slID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(visited_otm_a) AS otmAVisits,
            SUM(visited_otm_b) AS otmBVisits,
            SUM(visited_otm_c) AS otmCVisits,
            SUM(visited_otm_d) AS otmDVisits,
            SUM(visited_otm_na) AS otmUVisits,
            SUM(number_customer_otm_a) AS otmACount,
            SUM(number_customer_otm_b) AS otmBCount,
            SUM(number_customer_otm_c) AS otmCCount,
            SUM(number_customer_otm_d) AS otmDCount,
            SUM(number_customer_otm_na) AS otmUCount
        FROM report_all_in_one r
        LEFT JOIN salespersons_history s ON r.armstrong_2_salespersons_id=s.armstrong_2_salespersons_id 
            WHERE r.app_type = 0 {$sStatus}
            AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
            AND r.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getCallCoverageByTeam($countryId, $month = null, $year = null, $srID, $slID, $status)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(total_call) AS totalCalls,
            SUM(times_call_otm_a) AS otmACalls,
            SUM(times_call_otm_b) AS  otmBCalls,
            SUM(times_call_otm_c) AS  otmCCalls,
            SUM(times_call_otm_d) AS  otmDCalls,
            SUM(times_call_otm_na) AS  otmUCalls,
            SUM(visited_otm_a) AS otmAVisits,
            SUM(visited_otm_b) AS otmBVisits,
            SUM(visited_otm_c) AS otmCVisits,
            SUM(visited_otm_d) AS otmDVisits,
            SUM(visited_otm_na) AS otmUVisits,
            SUM(average_time_call_otm_a*times_call_otm_a)  AS otmACallMins,
            SUM(average_time_call_otm_b*times_call_otm_b) AS otmBCallMins,
            SUM(average_time_call_otm_c*times_call_otm_c) AS otmCCallMins,
            SUM(average_time_call_otm_d*times_call_otm_d) AS otmDCallMins,
            SUM(average_time_call_otm_na*times_call_otm_na) AS otmUCallMins,
            SUM(number_customer_otm_a) AS otmACount,
            SUM(number_customer_otm_b) AS otmBCount,
            SUM(number_customer_otm_c) AS otmCCount,
            SUM(number_customer_otm_d) AS otmDCount,
            SUM(number_customer_otm_na) AS otmUCount,
            woking_days as workingDay,
            COUNT(distinct r.armstrong_2_salespersons_id) as totalSalespersons,
            SUM(actual_woking_days) as totalActualWorking
           /* SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount*/
        FROM report_all_in_one r
        LEFT JOIN salespersons_history s ON r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
            WHERE r.app_type = 0
            AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
            AND r.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }


    public function getTotalCallsPerCountry($countryId, $month = null, $year = null){
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            IFNULL(SUM(totalCall),0) AS totalCalls,
            IFNULL(monthReport,{$month}) AS monthReport
           /* SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount*/
            FROM report_call_record
        WHERE appType = 0
            AND countryId = {$countryId} 
            AND monthReport = {$month}
            AND yearReport = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallsPerCountry($countryId, $month = null, $year = null){
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

         $sql = "SELECT 
            IFNULL(((SUM(perfect_calls)/SUM(call_count))*100),0) as perfectCalls,
            IFNULL(SUM(call_count),0) as callCount,
            IFNULL(SUM(perfect_calls),0) as perfectCallInt,
            IFNULL(month_report,{$month}) as monthReport
        FROM perfect_call_infographic 
        WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month}
            AND year_report = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getStrikeRatePerCountry($countryId, $month = null, $year = null){
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "select IFNULL(((tfo/calls)*100),0) as strikeRate, IFNULL(tfo,0) as tfo, 
            IFNULL(calls,0) as calls, IFNULL(month_report,{$month}) as monthReport
            from (select sum(tfo_incall) as tfo, (select sum(total_call) as calls from report_all_in_one where country_id = {$countryId} and month_report = {$month} and year_report = {$year} and app_type=0 and is_draft=0) as calls, month_report
                from report_all_in_one
                where country_id = {$countryId} 
                AND app_type = 0 
                AND month_report = {$month}
                AND year_report = {$year}) a";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getTotalTfoGrowthPerCountry($countryId, $month = null, $year = null){
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        
        //$ids = implode(',', $ids);

        $sql = "SELECT 
                IFNULL(MONTH(date_created),{$month}) as monthReport,
                SUM(IF(new_sku='No',total,0)) AS exi_sku_curr,
                SUM(IF(new_sku='Yes',total,0)) AS new_sku_curr
                FROM
                tfo_details 
                WHERE app_type = 0
                AND country_id = {$countryId} 
                AND MONTH(date_created) = {$month}
                AND (YEAR(`date_created`) = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getTotalCallsPerTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(total_call) AS totalCalls,
            month_report AS monthReport
           /* SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount*/
        FROM report_all_in_one 
            WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallsPerTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

         $sql = "SELECT 
            ((SUM(perfect_calls)/SUM(call_count))*100) as perfectCalls,
            IFNULL(SUM(call_count),0) as callCount,
            IFNULL(SUM(perfect_calls),0) as perfectCallInt,
            IFNULL(month_report,{$month}) as monthReport
        FROM perfect_call_infographic p
        INNER JOIN salespersons s ON p.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
        WHERE app_type = 0
            AND p.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getStrikeRatePerTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

         $sql = "select IFNULL(((tfo/calls)*100),0) as strikeRate, IFNULL(tfo,0) as tfo, 
            IFNULL(calls,0) as calls, IFNULL(month_report,{$month}) as monthReport
            from (select sum(tfo_incall) as tfo, (select sum(total_call) as calls from report_all_in_one where country_id = {$countryId} and month_report = {$month} and year_report = {$year} and app_type=0 and is_draft=0) as calls, month_report
                from report_all_in_one
                where country_id = {$countryId} 
                AND app_type = 0 
                AND month_report = {$month}
                AND year_report = {$year}) a";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallPerOtm($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
             SUM(perfect_calls) AS totalPerfectCalls,
            SUM(otm_a_perfect_calls) AS otmAPerfectCalls,
           SUM(otm_b_perfect_calls) AS otmBPerfectCalls,
            SUM(otm_c_perfect_calls) AS otmCPerfectCalls,
            SUM(otm_d_perfect_calls) AS otmDPerfectCalls
        FROM perfect_call_infographic
        WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = {$year}"; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getPerfectCallPerOtmByTeam($countryId, $month = null, $year = null, $srID, $slID, $status){  
        if ($slID != "" && $srID == "") {
            $salesID = (" AND s.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND p.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        if ($status != 2){
            $sStatus = (" AND s.active = '".$status."' ");
        }else{
            $sStatus = ("");
        }

        //$ids = implode(',', $ids);

        $sql = "SELECT 
             SUM(perfect_calls) AS totalPerfectCalls,
            SUM(otm_a_perfect_calls) AS otmAPerfectCalls,
           SUM(otm_b_perfect_calls) AS otmBPerfectCalls,
            SUM(otm_c_perfect_calls) AS otmCPerfectCalls,
            SUM(otm_d_perfect_calls) AS otmDPerfectCalls
        FROM perfect_call_infographic p
        JOIN salespersons_history s ON p.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
        WHERE p.app_type = 0
            {$sStatus}
            AND (((to_date IS NULL OR to_date >= {$from}) AND from_date <= {$to}) OR ((from_date IS NULL OR from_date <= {$to}) AND to_date >= {$from}) OR (from_date >= {$from} AND from_date <= {$to}) OR (to_date >= {$from} AND to_date <= {$to}) OR (to_date IS NULL AND from_date IS NULL))
            AND p.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID;
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function setTableType($type = '')
    {
        if (!in_array($type, array('ssd'))) {
            $type = '';
        }

        static::$_tableType = $type;

        return $this;
    }

    protected function _getTableName($table)
    {
        return static::$_tableType ? $table . '_' . static::$_tableType : $table;
    }

    public function getTfoGrowthByCountryANZ($countryID, $month, $year){
        
        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No'";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev.") AS new_prev";*/
        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  armstrong_2_tfo_id,
                  SUM(IF(new_sku='No',total,0)) AS exi_sku_curr,
                  SUM(IF(new_sku='Yes',total,0)) AS new_sku_curr
                FROM
                  tfo_details 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                  AND (YEAR(`date_created`) = '".$curr_yr."')
                  AND MONTH(date_created) <= '".$month."' ";

        $qry1 = "SELECT 
                  armstrong_2_tfo_id,
                  SUM(IF(new_sku='No',total,0)) AS exi_sku_prev,
                  SUM(IF(new_sku='Yes',total,0)) AS new_sku_prev
                FROM
                  tfo_details 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                  AND (YEAR(`date_created`) = '".$prev_yr."')
                  AND MONTH(date_created) <= '".$month."' ";

        $result[] = $this->db->query($qry)->result_array();
        $result[] = $this->db->query($qry1)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
    }

    public function getTfoGrowthByCountryID($countryID, $month, $year){
        
        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No'";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev.") AS new_prev";*/
        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  (SUM(report_all_in_one.total_amount) - SUM(report_all_in_one.new_grab_value)) AS exi_sku_curr,
                  SUM(new_grab_value) AS new_sku_curr
                FROM
                  report_all_in_one 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                  AND year_report = '".$curr_yr."'
                  AND month_report <= '".$month."'";

        $qry1 = "SELECT 
                  (SUM(report_all_in_one.total_amount) - SUM(report_all_in_one.new_grab_value)) AS exi_sku_prev,
                 SUM(new_grab_value) AS new_sku_prev
                FROM
                  report_all_in_one
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                  AND year_report = '".$prev_yr."'
                  AND month_report <= '".$month."'";

        $result[] = $this->db->query($qry)->result_array();
        $result[] = $this->db->query($qry1)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
    }

    public function getTotalTfoGrowthByCountryANZ($countryID, $month, $year){
        
        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No'";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev.") AS new_prev";*/
        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  armstrong_2_tfo_id,
                  SUM(IF(new_sku='No',total,0)) AS exi_sku_curr,
                  SUM(IF(new_sku='Yes',total,0)) AS new_sku_curr,
                  month(date_created) as monthReport
                FROM
                  tfo_details 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                AND month(date_created) = '".$month."'
                  AND (YEAR(`date_created`) = '".$curr_yr."')";


        $result[] = $this->db->query($qry)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
    }

    public function getTotalTfoGrowthByCountryID($countryID, $month, $year){
        
        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No'";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev.") AS new_prev";*/
        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  (SUM(report_all_in_one.total_amount) - report_all_in_one.new_grab_value) AS exi_sku_curr,
                  SUM(new_grab_value) AS new_sku_curr,
                  month_report as monthReport
                FROM
                  report_all_in_one 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                AND month_report = '".$month."'
                  AND year_report = '".$curr_yr."'";


        $result[] = $this->db->query($qry)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
    }

    public function getTotalTfoGrowthByTeam($countryID, $month, $year, $srID, $slID){
        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        
        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No'";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev.") AS new_prev";*/
        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  (SUM(report_all_in_one.total_amount) - report_all_in_one.new_grab_value) AS exi_sku_curr,
                  SUM(new_grab_value) AS new_sku_curr,
                  month_report as monthReport
                FROM
                  report_all_in_one 
                WHERE app_type = 0
                AND country_id = '".$countryID."' 
                AND month_report <= '".$month."'
                  AND year_report = '".$curr_yr."' 
                  '".$salesID."'";


        $result[] = $this->db->query($qry)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
    }


    public function getTfoStrikeRateForBarGraph($countryID, $month, $year){        
        $selectFrom = "(SELECT sum(totalCallWithTfo) AS tfo FROM report_call_record ";
        $where = "WHERE countryId = '".$countryID."' ";
        $monthIs = "AND monthReport = '".($month)."'";
        $yearToday = " AND yearReport = '".$year."' AND appType=0"; 

        $qry = "SELECT ";
        $qry .= $selectFrom." ".$where." ".$monthIs." ".$yearToday.") AS helpme"; 
        return $this->db->query($qry)->result_array();  
        
    }

    public function getTfoStrikeRateForBarGraphForTeam($countryID, $month, $year, $srID, $slID, $status){  
        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }
        $from = '"'.$year.'-'.$month.'-'.'01 00:00:00"';
        $to = '"'.$year.'-'.$month.'-'.'31 00:00:00"';

        $selectFrom = "(SELECT sum(`tfo_incall`) AS tfo FROM report_all_in_one r ";
        $where = "WHERE r.app_type = 0 AND r.country_id = '".$countryID."'";
        $monthIs = "AND month_report = '".($month)."'";
        $yearToday = " AND year_report = '".$year."' AND r.is_draft=0"; 

        $qry = "SELECT ";
        $qry .= $selectFrom." ".$where." " .$sStatus. " ".$monthIs." ".$yearToday." ".$salesID.") AS helpme"; 
        return $this->db->query($qry)->result_array();  
        
    }

    public function getCallRecords($countryID, $month, $year){        
        $selectFrom = "(SELECT sum(`total_call`) AS callRecords FROM report_all_in_one ";
        $where = "WHERE app_type = 0 AND country_id = '".$countryID."' ";
        $monthIs = "AND month_report = '".($month)."'";
        $yearToday = " AND year_report = '".$year."' AND is_draft=0"; 

        $qry = "SELECT ";
        $qry .= $selectFrom." ".$where." ".$monthIs." ".$yearToday.") AS callRecords"; 
        return $this->db->query($qry)->result_array();  
        
    }

    public function getCallRecordsForTeam($countryID, $month, $year, $srID, $slID){  
        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
      
        $selectFrom = "(SELECT sum(`total_call`) AS callRecords FROM report_all_in_one ";
        $where = "WHERE app_type = 0 AND country_id = '".$countryID."' ";
        $monthIs = "AND month_report = '".($month)."'";
        $yearToday = " AND year_report = '".$year."' AND is_draft=0"; 

        $qry = "SELECT ";
        $qry .= $selectFrom." ".$where." ".$monthIs." ".$yearToday." ".$salesID.") AS callRecords"; 
        return $this->db->query($qry)->result_array();  
        
    }

    public function getTfoGrowthBySalespersonANZ($countryID, $month, $year, $srID = 0, $slID = 0){

        switch ($slID) { // get sales id for query condition
            case 0: // 0-0, 0-1
                $salesID = ($srID = 0 ? "" : " AND s.salespersons_manager_id = '".$slID."'");
                break;
            
            default: // 1-0, 1-1
                $salesID = ($srID = 0 ? "AND s.salespersons_manager_id = '".$slID."'" : " AND s.salespersons_manager_id = '".$slID."' OR armstrong_2_salespersons_id IN ('".$srID."')");
                break;
        }

        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  t.armstrong_2_tfo_id,
                  SUM(IF(t.new_sku='No',t.total,0)) AS exi_sku_curr,
                  SUM(IF(t.new_sku='Yes',t.total,0)) AS new_sku_curr
                FROM
                  tfo_details as t
                  join salespersons as s
                  on t.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE t.app_type = 0
                AND t.country_id = '".$countryID."' 
               ".$salesID. "
                  AND (YEAR(t.date_created) = '".$curr_yr."')";

        $qry1 = "SELECT 
                  t.armstrong_2_tfo_id,
                  SUM(IF(t.new_sku='No',t.total,0)) AS exi_sku_prev,
                  SUM(IF(t.new_sku='Yes',t.total,0)) AS new_sku_prev
               FROM
                  tfo_details as t
                  join salespersons as s
                  on t.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE t.app_type = 0
                AND t.country_id = '".$countryID."' 
               ".$salesID. "
                  AND (YEAR(t.date_created) = '".$prev_yr."')";

        $result[] = $this->db->query($qry)->result_array();
        $result[] = $this->db->query($qry1)->result_array();
        
        return $result; // $this->db->query($qry)->result_array();  
        
       

        /*$exi = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'No' AND app_type = 0 ";
        $new = "(SELECT SUM(total) AS currAmt FROM tfo_details WHERE new_sku = 'Yes' AND app_type = 0  ";
        $where = "AND country_id = '".$countryID."' ";
        $where .= "AND MONTH(`date_created`) = '".$month."'";
        $con_Curr = "AND YEAR(`date_created`) = '".$year."'"; 
        $con_Prev = "AND YEAR(`date_created`) = '".($year-1)."'";

        $qry = "SELECT ";
        $qry .= $exi." ".$where." ".$con_Curr." ".$salesID.") AS exi_curr,"; 
        $qry .= $new." ".$where." ".$con_Curr." ".$salesID.") AS new_curr,";
        $qry .= $exi." ".$where." ".$con_Prev." ".$salesID.") AS exi_prev,";
        $qry .= $new." ".$where." ".$con_Prev." ".$salesID.") AS new_prev";
        
        return $this->db->query($qry)->result_array();*/
    }

    public function getTfoGrowthBySalesperson($countryID, $month, $year, $srID = 0, $slID = 0){

        switch ($slID) { // get sales id for query condition
            case 0: // 0-0, 0-1
                $salesID = ($srID = 0 ? "" : " AND s.salespersons_manager_id = '".$slID."'");
                break;
            
            default: // 1-0, 1-1
                $salesID = ($srID = 0 ? "AND s.salespersons_manager_id = '".$slID."'" : " AND s.salespersons_manager_id = '".$slID."' OR armstrong_2_salespersons_id IN ('".$srID."')");
                break;
        }

        $curr_yr = $year;
        $prev_yr = $year-1;

        $curr_yr = $year;
        $prev_yr = $year-1;

        $qry = "SELECT 
                  SUM(r.total_amount - r.new_grab_value) AS exi_sku_curr,
                  SUM(new_grab_value) AS new_sku_curr
                FROM
                  report_all_in_one as r
                  join salespersons as s
                  on r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE r.app_type = 0
                AND r.country_id = '".$countryID."' 
                ".$salesID. "
                  AND r.year_report = '".$curr_yr."'";

        $qry1 = "SELECT 
                  SUM(r.total_amount - r.new_grab_value) AS exi_sku_prev,
                 SUM(new_grab_value) AS new_sku_prev
                FROM
                  report_all_in_one as r
                  join salespersons as s
                  on r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE r.app_type = 0
                AND r.country_id = '".$countryID."' 
                ".$salesID. "
                  AND r.year_report = '".$prev_yr."'";

        $result[] = $this->db->query($qry)->result_array();
        $result[] = $this->db->query($qry1)->result_array();
        
        
        return $result; 
        
    }

    public function getGripRecordByCountry($country_id, $month, $year){

        $qry = "SELECT 
                    SUM(number_customer_otm_a+number_customer_otm_b+number_customer_otm_c+number_customer_otm_d+number_customer_otm_na) AS total_active,
                    SUM(totalGrip) AS total_grip, 
                    SUM(new_grip) as total_new_grip,
                    SUM(lost_grip) AS total_lost_grip 
                FROM report_all_in_one as a
                INNER JOIN salespersons as b ON a.armstrong_2_salespersons_id = b.armstrong_2_salespersons_id
                WHERE a.app_type = 0 AND `b`.`is_draft` = 0 AND `b`.`test_acc` = 0 and a.is_draft=0
                AND a.country_id = '".$country_id."'
                AND month_report = '".$month."' 
                AND year_report = '".$year."'";

        return $this->db->query($qry)->result_array();
    }

    public function getGripRecordBySalesperson($country_id, $month, $year, $srID, $slID){

        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
       
        $qry = "SELECT 
                    SUM(total_active) AS total_active,
                    SUM(totalGrip) AS total_grip, 
                    SUM(new_grip) as total_new_grip,
                    SUM(lost_grip) AS total_lost_grip 
                FROM report_all_in_one as r
                INNER JOIN salespersons as b ON r.armstrong_2_salespersons_id = b.armstrong_2_salespersons_id
                /*join salespersons as s
                on r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id*/
                WHERE r.app_type = 0 /*and s.test_acc=0*/
                AND r.country_id = '".$country_id."'
                AND month_report = '".$month."' 
                AND year_report = '".$year."' ".$salesID;

        return $this->db->query($qry)->result_array();
    }

    public function getGrabRecordByCountry($country_id, $month, $year){
        $qry = "SELECT 
                  SUM(totalGrab) AS total_grab,
                  SUM(new_grab) AS grab_target,
                  SUM(new_grab) AS new_grab_target,
                  SUM(totalGrip) AS total_grip,
                  SUM(totalGrab)/SUM(totalGrip) AS ave_grab,
                  SUM(lost_grab) AS lost_grab 
                FROM
                  report_all_in_one 
                WHERE app_type = 0 /*and is_draft=0 */
                AND country_id = '".$country_id."' 
                  AND month_report = '".$month."' 
                  AND year_report = '".$year."'";

        return $this->db->query($qry)->result_array();
    }


     public function getActiveCustomersByCountry($country_id, $month, $year){
        $qry = "SELECT 
                  SUM(customerOtmA+customerOtmB+customerOtmC+customerOtmD+customerOtmU) AS total_active     
                FROM
                  report_call_record
                WHERE appType = 0
                AND countryId = '".$country_id."' 
                  AND monthReport = '".$month."' 
                  AND yearReport = '".$year."'";

        return $this->db->query($qry)->result_array();
    }


     public function getActiveCustomersByTeam($country_id, $month, $year, $slID, $srID){
        
        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $qry = "SELECT 
                  SUM(number_customer_otm_a+number_customer_otm_b+number_customer_otm_c+number_customer_otm_d+number_customer_otm_na) AS total_active     
                FROM
                  report_all_in_one
                WHERE app_type = 0
                AND country_id = '".$country_id."' 
                  AND month_report = '".$month."' 
                  AND year_report = '".$year."'" . $salesID;

        return $this->db->query($qry)->result_array();
    }

    public function getGrabRecordBySalesperson($country_id, $month, $year, $srID, $slID){

        if ($slID != "" && $srID == "") {
            $salesID = (" AND r.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        $qry = "SELECT 
                  SUM(totalGrab) AS total_grab,
                  SUM(new_grab) AS grab_target,
                  SUM(new_grab) AS new_grab_target,
                  SUM(totalGrip) AS total_grip,
                  SUM(totalGrab)/SUM(totalGrip) AS ave_grab,
                  SUM(lost_grab) AS lost_grab 
                FROM
                  report_all_in_one as r
                  join salespersons as s
                  on r.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id
                WHERE r.app_type = 0 and r.is_draft=0 and s.test_acc=0
                  AND r.country_id = '".$country_id."' 
                  AND r.month_report = '".$month."' 
                  AND r.year_report = '".$year."'".$salesID;

        return $this->db->query($qry)->result_array();
    }

    public function getGrowthGripByChannel($country_id, $month, $year, $channel){
        
        $qry = "SELECT 
                  * 
                FROM
                  report_grip_grab 
                WHERE countryId = '".$country_id."' 
                  AND monthReport = '".$month."' 
                  AND yearReport = '".$year."' 
                  AND countryChannels = '".$channel."'";

        return $this->db->query($qry)->result_array();
    }

    //customer profiling per country
     public function getCustomerProfilingPerCountry($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            SUM(customer_count) AS customerCount,
            SUM(profiled) AS profiled,
            SUM(customer_name_count) AS  customerNameCount,
            SUM(channel_count) AS  channelCount,
            SUM(p_contact_count) AS  pContactCount,
            SUM(address_count) AS  addressCount,
            SUM(email_mobile_count) AS  emailMobileCount,
            SUM(signature_count) AS  signatureCount
        FROM report_customer_profiling
        WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = {$year}"; 
            /*AND id NOT IN ($ids)";*/

        return $this->db->query($sql)->row_array();
    }

    public function getCustomerProfilingByTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND r.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);
          $sql = "SELECT 
            SUM(customer_count) AS customerCount,
            SUM(profiled) AS profiled,
            SUM(customer_name_count) AS  customerNameCount,
            SUM(channel_count) AS  channelCount,
            SUM(p_contact_count) AS  pContactCount,
            SUM(address_count) AS  addressCount,
            SUM(email_mobile_count) AS  emailMobileCount,
            SUM(signature_count) AS  signatureCount
        FROM report_customer_profiling r
        JOIN salespersons s ON r.`armstrong_2_salespersons_id` = s.`armstrong_2_salespersons_id` 
        WHERE app_type = 0
            AND r.country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 

    

        return $this->db->query($sql)->row_array();
    }

    public function getGrowthGrip($countryId, $month = null, $year = null, $channel){
        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);
        $sql = "SELECT *
        FROM report_grip_grab
        WHERE countryId = {$countryId} 
            AND monthReport = {$month} 
            AND yearReport = {$year}
            AND countryChannels = {$channel}"; 

    

        return $this->db->query($sql)->row_array();
    }

    public function getTotalOTMCallsPerCountry($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            IFNULL(SUM(timesCallOtmA),0) AS otmACalls,
            IFNULL(SUM(timesCallOtmB),0) AS  otmBCalls,
            IFNULL(SUM(timesCallOtmC),0) AS  otmCCalls,
            IFNULL(SUM(timesCallOtmD),0) AS  otmDCalls,
            IFNULL(SUM(timesCallOtmU),0) AS  otmUCalls,
            IFNULL(monthReport,{$month}) AS monthReport
           /* SUM(pantry_check_count) AS  pantryCheckCount,
            SUM(tfo_count) AS  tfoCount,
            SUM(rich_media_count) AS  richMediaCount,
            SUM(competitor_info_count) AS  competitorInfoCount,
            SUM(personal_objective_count) AS  personalObjectiveCount*/
            FROM report_call_record
        WHERE appType = 0
            AND countryId = {$countryId} 
            AND monthReport = {$month}
            AND yearReport = {$year}";  
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getTotalOTMCallsPerTeam($countryId, $month = null, $year = null, $srID, $slID)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        if ($slID != "" && $srID == "") {
            $salesID = (" AND salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT 
            IFNULL(SUM(timesCallOtmA),0) AS otmACalls,
            IFNULL(SUM(timesCallOtmB),0) AS  otmBCalls,
            IFNULL(SUM(timesCallOtmC),0) AS  otmCCalls,
            IFNULL(SUM(timesCallOtmD),0) AS  otmDCalls,
            IFNULL(SUM(timesCallOtmU),0) AS  otmUCalls,
            IFNULL(monthReport,{$month}) AS monthReport
        FROM report_all_in_one 
            WHERE app_type = 0
            AND country_id = {$countryId} 
            AND month_report = {$month} 
            AND year_report = '".$year."' ".$salesID; 
            //AND id NOT IN ($ids)";

        return $this->db->query($sql)->row_array();
    }

    public function getSlName($slID)
    {
        $sql = "SELECT CONCAT(first_name,' ',last_name) as slName
                FROM salespersons
                WHERE armstrong_2_salespersons_id='{$slID}'";

        return $this->db->query($sql)->row_array();
    }

    public function getKPITargetsPerCountry($countryId, $month = null, $year = null)
    {
        //if (!$ids) {
          //  echo $this->load->view('infographic/no_data', '', true);
            //die();
        //}

        $month = $month ? $month : date('m', time());
        $year = $year ? $year : date('Y', time());

        //$ids = implode(',', $ids);

        $sql = "SELECT
                SUM(kpi_new_grip_target) as newGripTarget, 
                SUM(total_grip_target) as totalGripTarget
                FROM kpi_target
                WHERE
                is_draft=0 AND country_id = {$countryId}
                AND year = {$year} AND month = {$month}";
           

        return $this->db->query($sql)->row_array();
    }

    public function getLostGripPerChannel($countryId, $month, $year, $srID, $slID){
        $nextMonth = $month + 1;

        if($nextMonth < 10)
        {
            $nextMonth = '0'.$nextMonth;
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }

        //date range for the 'from' tag
        $fToDate = $year.'-'.$month.'-'.'01';
        $fFromDate = $year.'-'.($month-1).'-'.'01';
        $fFromDate = date("Y-m-d", strtotime($fFromDate."-5 months"));

        //date range for the nested query
        $nToDate = $year.'-'.$nextMonth.'-'.'01';
        $nFromDate = $year.'-'.$month.'-'.'01';
        $nFromDate = date("Y-m-d", strtotime($nFromDate."-5 months"));

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }
        //echo $salesID;

        //echo $fFromDate . " - " . $fToDate . " <br /> " . $nFromDate . " - " . $nToDate;

        $qry = "select count(distinct armstrong_2_customers_id) as lostGrip, channel, cc.name as countryChannelsName        
                from        
                (       
                    select a.armstrong_2_customers_id, MAX(a.date_created) as date_created, a.armstrong_2_salespersons_id, c.channel    
                    from tfo as a   
                    join salespersons as sp ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0    
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id  AND (a.date_created >= '".$fFromDate."' AND a.date_created < '".$fToDate."')    
                    AND a.country_id={$countryId}  AND a.is_draft=0 ".$salesID."
                    GROUP by a.armstrong_2_customers_id 
                        
                    union   
                        
                    select a.armstrong_2_customers_id, MAX(cr.date_created) as date_created, a.armstrong_2_salespersons_id, a.channel 
                    FROM pantry_check as cr  
                    INNER JOIN call_records_map_route_plan a ON (cr.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND a.datetime_call_start >= '".$fFromDate."' AND a.datetime_call_start < '".$fToDate."') 
                    INNER JOIN salespersons as sp   ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0    
                    WHERE  cr.is_draft = 0 AND cr.app_type = 0  and a.country_id={$countryId}   ".$salesID." 
                    GROUP BY a.armstrong_2_customers_id    
                        
                    union   
                        
                    SELECT a.armstrong_2_customers_id, MAX(ssd_date) as date_created, a.armstrong_2_salespersons_id, c.channel  
                    FROM ssd as a   
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id    
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )   AND (a.ssd_date >= '".$fFromDate."' AND a.ssd_date < '".$fToDate."' )    
                    AND a.country_id={$countryId}  and a.is_draft=0 ".$salesID."
                    group by a.armstrong_2_customers_id 
                ) b left join country_channels cc ON b.channel = cc.id      
                where armstrong_2_customers_id not in (     
                    select distinct armstrong_2_customers_id    
                    from (  
                        select distinct(a.armstrong_2_customers_id)
                        FROM pantry_check as cr
                        INNER JOIN call_records_map_route_plan a   ON (a.armstrong_2_call_records_id = cr.armstrong_2_call_records_id  AND a.datetime_call_start >= '".$nFromDate."' AND a.datetime_call_start < '".$nToDate."')
                        INNER JOIN salespersons as sp   ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0
                        WHERE  cr.is_draft = 0 AND cr.app_type = 0  and a.country_id={$countryId} ".$salesID."
                        
                        union
                        
                        select distinct(a.armstrong_2_customers_id)
                        from tfo as a
                        join salespersons as sp ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0
                        INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id  AND (a.date_created >= '".$nFromDate."' AND a.date_created < '".$nToDate."')  AND a.country_id={$countryId}  AND a.is_draft=0
                        ".$salesID."
                        
                        union
                        
                        SELECT distinct a.armstrong_2_customers_id
                        FROM ssd as a
                        LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id
                        INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0
                        WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                        AND (a.ssd_date >= '".$nFromDate."' AND a.ssd_date < '".$nToDate."' ) AND a.country_id={$countryId}  and a.is_draft=0 group by a.armstrong_2_customers_id ".$salesID."
                    ) a 
                )       
                group by channel
                order by count(distinct armstrong_2_customers_id) desc ";
        //echo $qry;
        return $this->db->query($qry)->result_array();
    }

    public function getLostGrabPerChannel($countryId, $month, $year, $srID, $slID){
        $nextMonth = $month + 1;

        if($nextMonth < 10)
        {
            $nextMonth = '0'.$nextMonth;
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }

        //date range for the 'from' tag
        $fToDate = $year.'-'.$month.'-'.'01';
        $fFromDate = $year.'-'.($month-1).'-'.'01';
        $fFromDate = date("Y-m-d", strtotime($fFromDate."-5 months"));
        $fToDate = date("Y-m-d", strtotime($fToDate."-1 day"));

        //date range for the nested query
        $nToDate = $year.'-'.$nextMonth.'-'.'01';
        $nFromDate = $year.'-'.$month.'-'.'01';
        $nFromDate = date("Y-m-d", strtotime($nFromDate."-5 months"));
        $nToDate = date("Y-m-d", strtotime($nToDate."-1 day"));

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        //echo $fFromDate . " - " . $fToDate . " <br /> " . $nFromDate . " - " . $nToDate;

        $qry = "select count(sku_number) as lostGrab, channel as countryChannels, cc.name as countryChannelsName
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number, c.channel  
                    from tfo_details as a
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created between '".$fFromDate."' and '".$fToDate."') ".$salesID." 
                    
                    union 
                    
                    select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                    FROM pantry_check_details as pc 
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between '".$fFromDate."' and '".$fToDate."'))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId}
                    ".$salesID."
                    
                    union
                    
                    SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                    FROM ssd as a  
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId}
                    AND (a.ssd_date between '".$fFromDate."' and '".$fToDate."') ".$salesID."
                    ) a left join country_channels cc ON a.channel = cc.id
                where (armstrong_2_customers_id, sku_number) not in
                    (
                        select armstrong_2_customers_id, sku_number
                        from 
                        (
                            select a.armstrong_2_customers_id, a.sku_number, c.channel  
                            from tfo_details as a
                            join salespersons as sp
                            ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                            INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                            AND (a.date_created between '".$nFromDate."' and '".$nToDate."') ".$salesID."
                            
                            union 
                            
                            select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                            FROM pantry_check_details as pc 
                            INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between '".$nFromDate."' and '".$nToDate."'))  
                            INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                            INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                            WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId} ".$salesID."
                            
                            union
                            
                            SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                            FROM ssd as a  
                            LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                            INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                            WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                            AND a.country_id={$countryId}
                            AND (a.ssd_date between '".$nFromDate."' and '".$nToDate."') ".$salesID."
                        ) b
                    )
                group by channel
                order by count(sku_number) desc";
                
        return $this->db->query($qry)->result_array();
    }

    public function getNewGripPerChannel($countryId, $month, $year, $srID, $slID){
        $nextMonth = $month + 1;

        if($nextMonth < 10)
        {
            $nextMonth = '0'.$nextMonth;
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }

        //date range for the 'from' tag
        $fToDate = $year.'-'.$nextMonth.'-'.'01';
        $fFromDate = $year.'-'.($month).'-'.'01';
        $fFromDate = date("Y-m-d", strtotime($fFromDate."-5 months"));

        //date range for the nested query
        $nToDate = $year.'-'.($month-1).'-'.'01';
        $nFromDate = $year.'-'.$month.'-'.'01';
        $nFromDate = date("Y-m-d", strtotime($nFromDate."-6 months"));

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        //echo $fFromDate . " - " . $fToDate . " <br /> " . $nFromDate . " - " . $nToDate;

        $qry = "select count(distinct armstrong_2_customers_id) as newGrip, channel, cc.name as countryChannelsName        
                from        
                (       
                    select a.armstrong_2_customers_id, MAX(a.date_created) as date_created, a.armstrong_2_salespersons_id, c.channel    
                    from tfo as a   
                    join salespersons as sp ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0    
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id  AND (a.date_created >= '".$fFromDate."' AND a.date_created < '".$fToDate."')    
                    AND a.country_id={$countryId}  AND a.is_draft=0  ".$salesID." 
                    GROUP by a.armstrong_2_customers_id 
                        
                    union   
                        
                    select a.armstrong_2_customers_id, MAX(cr.date_created) as date_created, a.armstrong_2_salespersons_id, a.channel 
                    FROM pantry_check as cr  
                    INNER JOIN call_records_map_route_plan a ON (cr.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND a.datetime_call_start >= '".$fFromDate."' AND a.datetime_call_start < '".$fToDate."') 
                    INNER JOIN salespersons as sp   ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0    
                    WHERE  cr.is_draft = 0 AND cr.app_type = 0  and a.country_id={$countryId}   ".$salesID." 
                    GROUP BY a.armstrong_2_customers_id 
                        
                    union   
                        
                    SELECT a.armstrong_2_customers_id, MAX(ssd_date) as date_created, a.armstrong_2_salespersons_id, c.channel  
                    FROM ssd as a   
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id    
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )   AND (a.ssd_date >= '".$fFromDate."' AND a.ssd_date < '".$fToDate."' )    
                    AND a.country_id={$countryId}  and a.is_draft=0  ".$salesID." 
                    group by a.armstrong_2_customers_id 
                ) b left join country_channels cc ON b.channel = cc.id      
                where armstrong_2_customers_id not in (     
                    select distinct armstrong_2_customers_id    
                    from (  
                        select distinct(a.armstrong_2_customers_id)
                        FROM pantry_check as cr
                        INNER JOIN call_records_map_route_plan a   ON (a.armstrong_2_call_records_id = cr.armstrong_2_call_records_id  AND a.datetime_call_start >= '".$nFromDate."' AND a.datetime_call_start < '".$nToDate."')
                        INNER JOIN salespersons as sp   ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0
                        WHERE  cr.is_draft = 0 AND cr.app_type = 0  and a.country_id={$countryId} ".$salesID."
                        
                        union
                        
                        select distinct(a.armstrong_2_customers_id)
                        from tfo as a
                        join salespersons as sp ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0
                        INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id  AND (a.date_created >= '".$nFromDate."' AND a.date_created < '".$nToDate."')  AND a.country_id={$countryId}  AND a.is_draft=0
                        ".$salesID." 
                        
                        union
                        
                        SELECT distinct a.armstrong_2_customers_id
                        FROM ssd as a
                        LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id
                        INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0
                        WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                        AND (a.ssd_date >= '".$nFromDate."' AND a.ssd_date < '".$nToDate."' ) AND a.country_id={$countryId}  and a.is_draft=0 group by a.armstrong_2_customers_id ".$salesID." 
                    ) a 
                )       
                group by channel
                order by count(distinct armstrong_2_customers_id) desc ";
                
        return $this->db->query($qry)->result_array();
    }

    public function getNewGrabPerChannel($countryId, $month, $year, $srID, $slID){
       
        $nextMonth = $month + 1;

        if($nextMonth < 10)
        {
            $nextMonth = '0'.$nextMonth;
        }

        if($month < 10)
        {
            $month = '0'.$month;
        }

        //date range for the 'from' tag
        $fToDate = $year.'-'.($month+1).'-'.'01';
        $fFromDate = $year.'-'.($month).'-'.'01';
        $fFromDate = date("Y-m-d", strtotime($fFromDate."-5 months"));
        $fToDate = date("Y-m-d", strtotime($fToDate."-1 day"));

        //date range for the nested query
        $nToDate = $year.'-'.($month).'-'.'01';
        $nFromDate = $year.'-'.($month-1).'-'.'01';
        $nFromDate = date("Y-m-d", strtotime($nFromDate."-5 months"));
        $nToDate = date("Y-m-d", strtotime($nToDate."-1 day"));

        if ($slID != "" && $srID == "") {
            $salesID = (" AND sp.salespersons_manager_id = '".$slID."'");
        } elseif ($srID != "") {
            $salesID = (" AND a.armstrong_2_salespersons_id = '".$srID."'");
        } elseif ($slID == "" && $srID == "") {
            $salesID = "";
        }

        //echo $fFromDate . " - " . $fToDate . " <br /> " . $nFromDate . " - " . $nToDate;

        $qry = "select count(sku_number) as newGrab, channel as countryChannels, cc.name as countryChannelsName
                FROM(
                    select a.armstrong_2_customers_id, a.sku_number, c.channel  
                    from tfo_details as a
                    join salespersons as sp
                    ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                    INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                    AND (a.date_created between '".$fFromDate."' and '".$fToDate."') ".$salesID." 
                    
                    union 
                    
                    select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                    FROM pantry_check_details as pc 
                    INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between '".$fFromDate."' and '".$fToDate."'))  
                    INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                    WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId} ".$salesID." 
                    
                    
                    union
                    
                    SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                    FROM ssd as a  
                    LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                    INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                    WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                    AND a.country_id={$countryId}
                    AND (a.ssd_date between '".$fFromDate."' and '".$fToDate."') ".$salesID." 
                    ) a left join country_channels cc ON a.channel = cc.id
                where (armstrong_2_customers_id, sku_number) not in
                    (
                        select armstrong_2_customers_id, sku_number
                        from 
                        (
                            select a.armstrong_2_customers_id, a.sku_number, c.channel  
                            from tfo_details as a
                            join salespersons as sp
                            ON a.armstrong_2_salespersons_id = sp.armstrong_2_salespersons_id AND sp.test_acc=0  AND sp.is_draft = 0 
                            INNER JOIN customers as c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id AND a.country_id={$countryId} AND a.is_draft=0 
                            AND (a.date_created between '".$nFromDate."' and '".$nToDate."')  ".$salesID." 
                            
                            union 
                            
                            select pc.armstrong_2_customers_id, pc.sku_number, c.channel 
                            FROM pantry_check_details as pc 
                            INNER JOIN call_records_map_route_plan a ON (pc.armstrong_2_call_records_id = a.armstrong_2_call_records_id  AND (a.datetime_call_start between '".$nFromDate."' and '".$nToDate."'))  
                            INNER JOIN salespersons as sp ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                            INNER JOIN customers c ON a.armstrong_2_customers_id = c.armstrong_2_customers_id 
                            WHERE  pc.is_draft = 0 AND pc.app_type = 0  and a.country_id={$countryId} ".$salesID." 
                            
                            union
                            
                            SELECT a.armstrong_2_customers_id, sku_number, c.channel 
                            FROM ssd as a  
                            LEFT JOIN customers AS c ON  c.armstrong_2_customers_id = a.armstrong_2_customers_id 
                            INNER JOIN salespersons as sp  ON sp.armstrong_2_salespersons_id = a.armstrong_2_salespersons_id AND sp.is_draft = 0 AND sp.test_acc=0  
                            WHERE a.is_draft = 0 AND (a.armstrong_2_customers_id LIKE 'OPD%' )
                            AND a.country_id={$countryId}
                            AND (a.ssd_date between '".$nFromDate."' and '".$nToDate."') ".$salesID." 
                        ) b
                    )
                group by channel
                order by count(sku_number) desc";
                
        return $this->db->query($qry)->result_array();
    }

    public function getCountryChannels($countryId){
        $qry = "select id, name
                from country_channels
                where country_id = {$countryId}";

        return $this->db->query($qry)->result_array();
    }
}