<?php
/**
 * Date: 8/12/14
 * Time: 5:03 PM
 */
class News_m extends AMI_Model {
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = 'news';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_order_rule = 'ASC';
    protected $_timestamp = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() {
        parent::__construct();
    }

    public function prepareDataTables(array $news)
    {
        $data = array();

        foreach ($news as $_news) 
        {
            $data[] = array(
                'title' => $_news['title'],
            );
        }

        return $data;
    }
}

?>