<?php
/**
 * Created by PhpStorm.
 * User: Erika Liongco
 * Date: 12/7/2017
 * Time: 6:51 PM
 */

class Shipment_m extends AMI_Model{

    protected $_table_name = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_record($table = null, $where = array()){
        $where['is_draft'] = 0;
        $src = ($table != null ? $table : $this->_table_name);
        $data = $this->getEntry($src,$where);
        return $data;
    }

    public function import_to_db($type, $data, $session)
    {
        if (empty($data)) {
            return false;
        }

        try {
            ini_set('max_execution_time',0);
            $now = date('Y-m-d H:i:s');
            $this->_table_name = $type;

            $fields = $this->db->list_fields($this->_table_name);

            $first = reset($data);
            $arrayKeys = array();
            foreach ($first as $_value) {
                $arrayKeys[] = strtolower(str_replace(' ', '_', $_value));
            }

            unset($data[0]); // Column titles
            
            // TODO : check if data exists on current month/year - set draft
            $is_exist = $this->getEntry($this->_table_name,array('is_draft' => 0, 'month' => date('m'), 'year' => date('Y')));
            if(!empty($is_exist)){
                foreach ($is_exist as $rec){
                    $this->db->set(array('is_draft' => 1));
                    $this->db->where(array('month' => date('m'), 'year'=> date('Y')));
                    $this->db->update($this->_table_name);
                }
            }

            // TODO : Insert new record
            $key = 0;
            $total = count($data);
            $invalid_data = array();
            $valid_data = array();
            /* NOTES:
             * 0 - country_id
             * 1 - country_name
             * 2 - distributor_id
             * 3 - distributor_name
             * 4 - sku_id
             * 5 - sku_desc
             * 6 - forecast
             * 7 - shipment plan
             * */
            foreach ($data as $datum){
                // TODO : validate per column
                if(!is_numeric($datum[0]) || !is_numeric($datum[6]) || !is_numeric($datum[7])){
                    array_push($invalid_data, $datum);
                    continue;
                };

                $distributor = $this->getEntry('distributors',array('armstrong_2_distributors_id' => $datum[2]));
                if(empty($distributor)){
                    array_push($invalid_data, $datum);
                    continue;
                }

                $sku = $this->getEntry('products', array('sku_number' => $datum[4]));
                if (empty($sku)) {
                    array_push($invalid_data, $datum);
                    continue;
                } else {
                    $datum[5] = $sku[0]->sku_name;
                }

                array_push($valid_data, $datum);

                $value = array_combine($arrayKeys, $datum);
                $value = array_only($value, $fields);

                $value['month'] = date('m');
                $value['year'] = date('Y');
                $value['date_created'] = $now;
                $value['sku_name'] = $sku[0]->sku_name;
                if($type != 'git') $value['category_id'] = $sku[0]->products_cat_web_id;

                $result = $this->save($value);
                if($result){
                    $key += 1;
                    $this->saveProgressToJson($key, $total, $session);
                }
            }

            /*foreach ($data as $key => $value) {
                $value = array_combine($arrayKeys, $value);
                $value = array_only($value, $fields);

                $sku = $this->getEntry('products', array('sku_number' => $value['sku_number']));
                if(!empty($sku)){
                    $value['sku_name'] = $sku[0]->sku_name;
                    if($type != 'git') $value['category_id'] = $sku[0]->products_cat_web_id;
                }

                $value['month'] = date('m');
                $value['year'] = date('Y');
                $value['date_created'] = $now;
                $result = $this->save($value);
                if($result){
                    // TODO : Create txt file that shows status of upload
                    $this->saveProgressToJson($key, $total, $session);
                }
            }*/
        } catch(Exemption $e){
            return $e;
        }
    }

    /**
     * @param $data
     * @param $session_id
     * @return bool|Exception|Exemption
     */
    public function import_sap_to_db($data, $session_id)
    {
        if (empty($data)) {
            return false;
        }

        try {
            $now = date('Y-m-d H:i:s');
            $this->_table_name = 'sap_invoicing';
            ini_set('max_execution_time',0);

            $fields = $this->db->list_fields($this->_table_name);
            // $fields = array_flip($fields);

            $first = reset($data);
            $arrayKeys = array();
            foreach ($first as $_value) {
                $arrayKeys[] = strtolower(str_replace(' ', '_', $_value));
            }

            unset($data[0]); // Column titles
            // TODO : check if data exists on current month/year - set draft
            /*$is_exist = $this->getEntry($this->_table_name,array('is_draft' => 0, 'MONTH(`invoice_date`)' => date('m'), 'YEAR(`invoice_date`)' => date('Y')));
            if(!empty($is_exist)){
                foreach ($is_exist as $rec){
                    $this->db->set(array('is_draft' => 1));
                    $this->db->where(array('MONTH(`invoice_date`)' => date('m'), 'YEAR(`invoice_date`)'=> date('Y')));
                    $this->db->update($this->_table_name);
                }
            }*/

            // TODO : Insert new record
            $invalid_data = array();
            $valid_data = array();
            $total = count($data);
            $key = 0;

            foreach ($data as $datum) {
                /**
                 * NOTE:
                 * 0 - country Id
                 * 1 - country name / code
                 * 2 - armstrong 2 distributor id
                 * 3 - distributor name
                 * 4 - sku_number
                 * 5 - sku_desc
                 * 6 - invoice_qty
                 * 7 - invoice_value
                 * 8 - invoice_date
                 */
                $new_data = array(
                    'country_id' => $datum[0],
                    'armstrong_2_distributors_id' => $datum[2],
                    'sku_number' => $datum[4],
                    'invoice_qty' => $datum[6],
                    'invoice_value' => $datum[7],
                    'invoice_date' => $datum[8]
                );

                // TODO: check each columns for correct value
                if (!is_numeric($new_data['country_id'])) {
                    array_push($invalid_data, $datum);
                    continue;
                };

                $distributor = $this->getEntry('distributors', array('armstrong_2_distributors_id' => $new_data['armstrong_2_distributors_id']));
                if (empty($distributor)) {
                    array_push($invalid_data, $datum);
                    continue;
                } else {

                    $datum[3] = $new_data['armstrong_2_distributors_name'] = $distributor[0]->name;
                }

                $sku = $this->getEntry('products', array('sku_number' => $new_data['sku_number']));
                if (empty($sku)) {
                    array_push($invalid_data, $datum);
                    continue;
                } else {
                    $datum[5] = $new_data['sku_name'] = $sku[0]->sku_name;
                }

                if (!$this->checkIfDateValid($new_data['invoice_date'])) {
                    array_push($invalid_data, $datum);
                    continue;
                } else {
                    $new_date = strtotime($new_data['invoice_date']);
                    $datum[8] = $new_data['invoice_date'] = date("Y-m-d H:i:s", $new_date);
                }

                array_push($valid_data, $datum);

                // TODO : update record if exists
                $is_exist = $this->getEntry($this->_table_name,
                    array('is_draft' => 0,
                        'invoice_date' => $new_data['invoice_date'],
                        'armstrong_2_distributors_id' => $new_data['armstrong_2_distributors_id'],
                        'sku_number' => $new_data['sku_number']));

                if (!empty($is_exist)) {
                    $to_update = array();
                    foreach ($new_data as $key => $value) {
                        if ($new_data[$key] != $is_exist[0]->$key) {
                            $to_update[$key] = $value;
                        }
                    }

                    if(!empty($to_update)){
                        $to_update['last_updated'] = $now;
                        $this->db->set($to_update);
                        $this->db->where(array(
                            'invoice_date' => $new_data['invoice_date'],
                            'armstrong_2_distributors_id' => $new_data['armstrong_2_distributors_id'],
                            'sku_number' => $new_data['sku_number']));
                        $this->db->update($this->_table_name);
                    }

                    $key += 1;
                    $this->saveProgressToJson($key, $total, $session_id);
                } else {
                    $value = array_combine($arrayKeys, $datum);
                    $value = array_only($value, $fields);
                    $value['sku_name'] = $value['sku_description'];
                    $value['date_created'] = $now;
                    $value['last_updated'] = $now;

                    $result = $this->save($value);
                    if($result){
                        $key += 1;
                        $this->saveProgressToJson($key, $total, $session_id);
                    }
                }
            }
            return true;
        } catch (Exemption $e) {
            return $e;
        }
    }

    public function checkIfDateValid($date)
    {
        return (bool)strtotime($date);
    }
}