<?php

class DataTables
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function response($data)
    {
        $this->CI->output->set_content_type( 'application/json' )
            ->set_output( json_encode( $data ) );
    }

    public function generate($data, $totalRecords, $displayRecords = null)
    {
        return $this->response(array(
            'aaData' => $data,
            'iTotalRecords' => $totalRecords,
            'iTotalDisplayRecords' => $displayRecords ? $displayRecords : count($data)
        ));
    }
}