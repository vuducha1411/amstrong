<?php

final class debug{

    public static function printArr($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public static function printArrDie($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
}