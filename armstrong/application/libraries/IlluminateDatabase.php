<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

class IlluminateDatabase
{
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();

		$this->_loadDb($this->CI->db);
	}

	public function _loadDb($db)
	{
		$dbConfig = array(
			'driver' => 'mysql',
			'host' => $db->hostname,
			'database' => $db->database,
			'username' => $db->username,
			'password' => $db->password,
			'charset' => $db->char_set,
			'collation' => $db->dbcollat,
			'prefix' => $db->dbprefix
		);

		$this->bootEloquent($dbConfig);
	}

	public function bootEloquent(array $dbConfig)
	{
		$capsule = new Capsule(); 
		$capsule->addConnection(array(
			'driver'    => $dbConfig['driver'],
			'host'      => $dbConfig['host'],
			'database'  => $dbConfig['database'],
			'username'  => $dbConfig['username'],
			'password'  => $dbConfig['password'],
			'charset'   => $dbConfig['charset'],
			'collation' => $dbConfig['collation'],
			'prefix'    => $dbConfig['prefix']
		));
		 
		$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher());
		$capsule->setAsGlobal();
		$capsule->bootEloquent();

		$capsule->getConnection()->enableQueryLog();
		
		return $capsule;
	}
}