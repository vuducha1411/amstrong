<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Email 
{
    protected $CI;

    public function __construct()
    {
        // parent::__construct();
        $this->CI =& get_instance();
        $this->CI->config->load('email');

        parent::__construct($this->getConfig());

        $this->from($this->configItem('from'), $this->configItem('from_name'));
    }

    public function factory(array $options = array(), $clear = true)
    {
        $this->clear($clear);

        if ($options)
        {
            foreach ($options as $function => $value) 
            {
                $this->$function($value);
            }
        }        
    }

    public function getConfig()
    {
        $config = array();

        $config['useragent']        = $this->configItem('useragent');
        $config['protocol']         = $this->configItem('protocol');
        $config['mailpath']         = $this->configItem('mailpath');
        $config['smtp_host']        = $this->configItem('smtp_host');
        $config['smtp_user']        = $this->configItem('smtp_user');
        $config['smtp_pass']        = $this->configItem('smtp_pass');
        $config['smtp_port']        = $this->configItem('smtp_port');
        $config['smtp_timeout']     = $this->configItem('smtp_timeout');
        $config['wordwrap']         = $this->configItem('wordwrap');
        $config['wrapchars']        = $this->configItem('wrapchars');
        $config['mailtype']         = $this->configItem('mailtype');
        $config['charset']          = $this->configItem('charset');
        $config['validate']         = $this->configItem('validate');
        $config['priority']         = $this->configItem('priority');
        $config['crlf']             = $this->configItem('crlf');
        $config['newline']          = $this->configItem('newline');
        $config['bcc_batch_mode']   = $this->configItem('bcc_batch_mode');
        $config['bcc_batch_size']   = $this->configItem('bcc_batch_size');

        return $config;
    }

    public function sendMail($to, $subject, $message, $cc = null, $bcc = null)
    {
        $this->to($to)
            ->subject($subject)
            ->message($message);

        if ($cc)
        {
            $this->cc($cc);
        }

        if ($bcc)
        {
            $this->bcc($bcc);//change bcc
        }

        $this->send();
    }

    public function configItem($item)
    {
        return $this->CI->config->item($item);
    }
}