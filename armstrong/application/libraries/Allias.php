<?php

class Allias
{
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('alias');

		$this->loadDefaultAliases();
	}

	public static function makeAliases(array $aliases)
	{
		foreach ($aliases as $class => $alias) 
		{
			static::makeAlias($class, $alias);
		}
	}

	public static function makeAlias($class, $alias)
	{
		class_alias($class, $alias);
	}

	public function loadDefaultAliases()
	{
		static::makeAliases($this->CI->config->item('aliases'));
	}
}