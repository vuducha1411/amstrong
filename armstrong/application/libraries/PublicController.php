<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PublicController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->data['errors'] = array();
        $this->data['page_title'] = config_item('site_name');
        $this->data['per_page'] = 20;

        Allias::makeAliases(array(
            '\Carbon\Carbon' => 'Carbon'
        ));
        $this->twig->addGlobal('Carbon', new Carbon);

        $this->twig->addGlobal('Router', $this->router);

        $this->twig->addFunction('pagination', function ($config) {
            $this->pagination->initialize($config);

            $link = $this->pagination->create_links();
            $get = $this->input->get();
            if (isset($get['p'])) {
                unset($get['p']);
            }
            if (!$get) {
                $link = str_replace('&amp;' . $this->pagination->query_string_segment, '?' . $this->pagination->query_string_segment, $link);
            }

            return $link;
        }, array('is_safe' => array('html')));
    }

    public function render($template, array $data = array())
    {
        // For CI only
        if (!$data) {
            $data = $this->data;
        }

        return $this->twig->display($template, $data);
    }

    public function renderView($template, array $data = array())
    {
        // For CI only
        if (!$data) {
            $data = $this->data;
        }

        return $this->twig->render($template, $data);
    }

    public function filter_dummy_data($data)
    {
        $filter_dump_datas = array(
            'test', 'duyen', 'dummy'
        );
        foreach ($data as $key => $val) {
            if (in_array($data[$key]['sub_type'], array('md', 'nsm', 'ssm'))) {
                unset($data[$key]);
            }
            foreach ($filter_dump_datas as $filter_dump_data) {
                if (preg_match("/" . $filter_dump_data . "/i", (is_array($val)) ? $val['first_name'] : $val->first_name)) {
                    unset($data[$key]);
                }
                if (preg_match("/" . $filter_dump_data . "/i", (is_array($val)) ? $val['last_name'] : $val->last_name)) {
                    unset($data[$key]);
                }
                if (preg_match("/" . $filter_dump_data . "/i", (is_array($val)) ? $val['email'] : $val->email)) {
                    unset($data[$key]);
                }
            }

        }
        $data = array_values($data);
        return $data;
    }
}