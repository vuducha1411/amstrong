<?
$UserID = "kr/userid"; // PGi ID
$PassWord = "password";      // PGiPW
$SendPath = "./Temp/";   // Temp directory
$TiffFileName = "test.tif"; // tiff file

$NowTime = date("Y-m-d H:i:s");
$InputTime = preg_replace('/[^0-9]/', '', SubStr($NowTime, 2)) . SubStr(MicroTime(), 2, 6);

if (!is_dir($SendPath)) mkdir($SendPath);

if ($_POST[UseBanner] == "Y") $BannerUse = "yes";
else {
    $BannerUse = "no";
    $BannerName = '';
}

if ($_POST[TiffMode]) $FaxMode = StrToLower($_POST[TiffMode]);
else $FaxMode = "standard";
$ReportType = "detail";
if ($_POST[Schedule] == 'scheduled') {
    $SendDate = mktime($_POST[ScheduleHour], $_POST[ScheduleMin], '00', $_POST[ScheduleMonth], $_POST[ScheduleDay], $_POST[ScheduleYear]) - (3600 * 9);
    $SendDate = Date("Y-m-d H:i:s", $SendDate);
    $SendDate = Str_Replace(" ", "T", $SendDate);
}

// Create XML
$MessageID = Str_Replace("/", "_", $UserID) . "_Send_" . $InputTime;
$XML_DATA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$XML_DATA .= "<tns:Envelope xmlns:tns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
$XML_DATA .= "<tns:Header>";
$XML_DATA .= "<Request xmlns=\"http://premconn.premiereconnect.com/2007/02\">";
$XML_DATA .= "<ReceiverKey>http://api.pgiconnect.co.kr/soap/sync</ReceiverKey>";
$XML_DATA .= "<Authentication>";
$XML_DATA .= "<XDDSAuth>";
$XML_DATA .= "<RequesterID>" . $UserID . "</RequesterID>";
$XML_DATA .= "<Password>" . $PassWord . "</Password>";
$XML_DATA .= "</XDDSAuth>";
$XML_DATA .= "</Authentication>";
$XML_DATA .= "</Request>";
$XML_DATA .= "</tns:Header>";
$XML_DATA .= "<tns:Body>";
$XML_DATA .= "<JobSubmitRequest xmlns=\"http://premconn.premiereconnect.com/2007/02\">";
$XML_DATA .= "<Message>";
$XML_DATA .= "<MessageId>" . $MessageID . "</MessageId>";
$XML_DATA .= "<JobOptions>";
if ($_POST[Schedule] == 'scheduled') $XML_DATA .= "<Delivery><Schedule>scheduled</Schedule><StartTime>$SendDate</StartTime></Delivery>";
else $XML_DATA .= "<Delivery><Schedule>express</Schedule></Delivery>";
$XML_DATA .= "<FaxOptions>";
$XML_DATA .= "<BannerFX>";
$XML_DATA .= "<UseBannerFx>" . $BannerUse . "</UseBannerFx>";
$XML_DATA .= "<BannerFxName>" . $BannerName . "</BannerFxName>";
$XML_DATA .= "</BannerFX>";
$XML_DATA .= "<FaxMode>" . $FaxMode . "</FaxMode>";
$XML_DATA .= "</FaxOptions>";
$XML_DATA .= "</JobOptions>";
$XML_DATA .= "<Destinations>";

// input the fax numbers
$DataEA = 0;
for ($i = 0; $i <= Count($DestList) - 1; $i++) {
    if (strpos($DestList[$i], "|")) {
        $IndividualNumber = Explode("|", $DestList[$i]);
        $REF = base64_encode($IndividualNumber[0]);
        $ADDR = $IndividualNumber[1];
        if (StrLen($ADDR) >= 8) {
            $XML_DATA .= "<Fax refb64=\"$REF\"><Phone>$ADDR</Phone></Fax>";
            $DataEA++;
        }
    }
}
$XML_DATA .= "</Destinations>";
if ($DataEA == 0) {
    @Unlink($CsvFile);
    echo "<<script>alert('Not found the list');location.href='about:blank'</script>";
    exit;
}
$XML_DATA .= "<Reports><DeliveryReport><DeliveryReportType>" . $ReportType . "</DeliveryReportType></DeliveryReport></Reports>";
$XML_DATA .= "<Contents><Part><Document>";
$XML_DATA .= "<DocType>TIFF</DocType>";
$XML_DATA .= "<Filename>" . $TiffFileName . "</Filename>";
$TiffBase64 = join("", file($TiffFileName));
$XML_DATA .= "<DocData format=\"base64\">" . base64_encode($TiffBase64);
$XML_DATA .= "</DocData></Document></Part></Contents></Message></JobSubmitRequest></tns:Body></tns:Envelope>";

$RemoteServer = "api.pgiconnect.co.kr";
$Header = "POST /soap/sync HTTP/1.0\r\n";
$Header .= "Host: " . $RemoteServer . "\r\n";
$Header .= "User-agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)\r\n";
$Header .= "Content-type: text/html\r\n";
$Header .= "Content-length: " . strlen($XML_DATA) . "\r\n\r\n";
$Header .= $XML_DATA . "\r\n";

$DataUP = fsockopen($RemoteServer, 80);
fputs($DataUP, $Header);
while (!feof($DataUP)) {
    $ReturnXML = fgets($DataUP, 4096);
    if (strpos($ReturnXML, "xml version")) $FindCode = 1;
    if ($FindCode == 1) $Result_Send .= $ReturnXML;
}
fclose($DataUP);

$Parser = new SimpleXMLElement($Result_Send);
$Code = $Parser->Body->JobSubmitResult->MessageResult;
$StatusMessage = $Code->Status->StatusMessage;
$JobNumber = $Code->JobId->MRN;

$XML_DATA_FILE = fopen($SendPath . $MessageID . ".XML", "w");
fputs($XML_DATA_FILE, $XML_DATA);
fclose($XML_DATA_FILE);

$XML_RESULT_FILE = fopen($SendPath . $MessageID . "_RESULT.XML", "w");
fputs($XML_RESULT_FILE, $Result_Send);
fclose($XML_RESULT_FILE);
if ($StatusMessage == "OK") {
    rename($SendPath . $TiffFileName, $SendPath . $JobNumber . ".tif");
    rename($SendPath . $MessageID . ".XML", $SendPath . $JobNumber . ".XML");
    rename($SendPath . $MessageID . "_RESULT.XML", $SendPath . $JobNumber . "_RESULT_OK.xml");
    echo "<script>alert('OK\\nJobNumber : $JobNumber');</script>";
} else {
    rename($SendPath . $TiffFileName, $SendPath . $InputTime . ".tif");
    rename($SendPath . $MessageID . ".XML", $SendPath . $InputTime . ".XML");
    rename($SendPath . $MessageID . "_RESULT.XML", $SendPath . $InputTime . "_RESULT_ERROR.xml");
    echo "<script>alert('Fail')</script>";
}
?>