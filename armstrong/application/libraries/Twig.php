<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Twig
{
	private $CI;
	private $_twig;
	private $_template_dir;
	private $_cache_dir;
	private $_ext;
	
	function __construct($debug = false)
	{
		$this->CI =& get_instance();
		$this->CI->config->load('twig');

		// Already loaded via composer autoload.php
		/*ini_set('include_path',
		ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'libraries/Twig');
		require_once (string) "Autoloader" . EXT;
		log_message('debug', "Twig Autoloader Loaded");
		Twig_Autoloader::register();*/

		$this->_template_dir = $this->CI->config->item('template_dir');
		$this->_cache_dir = $this->CI->config->item('cache_dir');
		$this->_ext = $this->CI->config->item('extension');
		$loader = new Twig_Loader_Filesystem($this->_template_dir);
		$this->_twig = new Twig_Environment($loader, array(
			'cache' => $this->_cache_dir,
			'debug' => $debug,
		));

		if ($debug)
		{
			$this->_twig->addExtension(new Twig_Extension_Debug());
		}

		$this->_loadTemplateHelpers();

        $this->CI->load->helpers(array('form', 'html', 'string', 'text'));

		$this->addDefaultHelpers($this->_getFormHelpers());
        $this->addDefaultHelpers($this->_getHtmlHelpers());
        $this->addDefaultHelpers($this->_getStringHelpers());
        $this->addDefaultHelpers($this->_getTextHelpers());
	}

	/**
	 * Get Twig instance
	 * 
	 * @return object
	 */ 
	public function instance()
	{
		return $this->_twig;
	}

	/**
	 * Shorthand for twig->addFunction
	 */ 
	public function addFunction($name, $function, $options = array())
	{
		if (!$function instanceof Twig_SimpleFunction)
		{
			$function = new Twig_SimpleFunction($name, $function, $options);
		}

		$this->_twig->addFunction($function);
	}

	/**
	 * Shorthand for twig->addGlobal
	 */ 
	public function addGlobal($name, $value)
	{
		$this->_twig->addGlobal($name, $value);
	}

	/**
	 * Shorthand for twig->addFilter
	 */ 
	public function addFilter($name, $filter, $options = array())
	{
		if (!$filter instanceof Twig_SimpleFilter)
		{
			$filter = new Twig_SimpleFilter($name, $filter, $options);
		}

		$this->_twig->addFilter($filter);
	}

	/**
	 * Render a page with twig
	 * 
	 * @param string|$template template path
	 * @param array|$data
	 */
	public function render($template, $data = array())
	{
		$template = $this->_loadTemplate($template);
		return $template->render($data);
	}

	/**
	 * Render a page with twig with some debug information
	 * 
	 * @param string|$template template path
	 * @param array|$data
	 */
	public function display($template, $data = array())
	{
		$template = $this->_loadTemplate($template);
		/* elapsed_time and memory_usage */
		$data['elapsed_time'] = $this->CI->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
		$memory = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2) . 'MB';
		$data['memory_usage'] = $memory;
		$template->display($data);
	}

	/**
	 * Preload template with twig, auto append template extension
	 * 
	 * @param string|$template template path
	 */
	protected function _loadTemplate($template)
	{
		$ext = pathinfo($template, PATHINFO_EXTENSION);

		if (!$ext)
		{
			$template .= '.' . $this->_ext;
		}

		return $this->_twig->loadTemplate($template);
	}	

	/**
	 * Add html helpers
	 * 
	 * @param array|$helpers
	 */
	public function addDefaultHelpers(array $helpers)
	{
		foreach ($helpers as $function) 
		{
			$this->addFunction($function, $function, array('is_safe' => array('html')));
		}		
	}

	/**
	 * Add default html helpers	 
	 */
	protected function _loadTemplateHelpers()
    {
        // Funtions
        $this->addFunction('anchor', 'anchor', array('is_safe' => array('html')));
        $this->addFunction('base_url', 'base_url');
        $this->addFunction('site_url', 'site_url');
        $this->addFunction('current_url', 'current_url');
        $this->addFunction('uri_string', 'uri_string');

        $this->addFunction('trim_word', 'wordTrim', array('is_safe' => array('html')));
        $this->addFunction('html_btn', 'html_btn', array('is_safe' => array('html')));        
        $this->addFunction('select_year', 'select_year', array('is_safe' => array('html')));
        $this->addFunction('select_month', 'select_month', array('is_safe' => array('html')));
        $this->addFunction('get_country', 'get_country', array('is_safe' => array('html')));
        $this->addFunction('member_type', 'member_type', array('is_safe' => array('html')));

        $this->addFunction('array_only', 'array_only');
        $this->addFunction('array_except', 'array_except');
        $this->addFunction('array_rewrite', 'array_rewrite');
        $this->addFunction('parse_datetime', 'parse_datetime');
        $this->addFunction('get_product_label', 'get_product_label');
        $this->addFunction('get_country_name', 'get_country_name');

        // Global values
        $this->addGlobal('site_name', config_item('site_name'));
        $this->addGlobal('errors', array());        

        // Filters        
    }

    /**
	 * Get CI form helpers	 
	 */
	protected function _getFormHelpers()
	{
		return array(
			'form_open',
			'form_open_multipart',
			'form_hidden',
			'form_input',
			'form_password',
			'form_upload',
			'form_textarea',
			'form_dropdown',
			'form_multiselect',
			'form_fieldset',
			'form_fieldset_close',
			'form_checkbox',
			'form_radio',
			'form_submit',
			'form_label',
			'form_reset',
			'form_button',
			'form_close',
			'form_prep',
			'form_error',

			'set_value',
			'set_select',
			'set_checkbox',
			'set_radio',

			'validation_errors'
		);
	}

    protected function _getHtmlHelpers()
    {
        return array(
            'br',
            'heading',
            'img',
            'link_tag',
            'nbs',
            'ol',
            'ul',
            'meta'
        );
    }

    protected function _getStringHelpers()
    {
        return array(
            'random_string',
            'repeater',
            'trim_slashes',
            'quotes_to_entities',
            'strip_quotes'
        );
    }

    protected function _getTextHelpers()
    {
        return array(
            'word_limiter',
            'character_limiter',
            'ascii_to_entities',
            'entities_to_ascii',
            'convert_accented_characters',
            'word_censor',
            'highlight_code',
            'highlight_phrase',
            'word_wrap',
            'ellipsize'
        );
    }
}