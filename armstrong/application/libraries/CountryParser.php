<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CountryParser
{
    public static $info = array();
    public static $_hierachy_address;

    protected static $_infoCache = array();

    public function parse($country, $path = null)
    {
        if (!$path)
        {
            // $path = FCPATH . 'res\\json';
            $path = site_url('res/json');
        }

        // $file = $path . '\\' . $filename;
        $file = $path . '/' . $country . '.json';

        $country = strtolower($country);

        if (!isset(static::$_infoCache[$country]))
        {
            $info = @file_get_contents($file);

            $info = @json_decode($info, true);
            static::$_infoCache[$country] = $info;
        }
        else
        {
            $info = static::$_infoCache[$country];
        }
        if (!$info)
        {
            //throw new Exception('Cannot get Info', 1);
        }

        // static::$info = $info;

        $methodName = '_fetchCountry' . ucfirst($country);

        if (method_exists($this, $methodName))
        {

            call_user_func(array($this, $methodName), $info);
        }

        return $this;
    }

    public function parse_hierachy($country, $path = null) {
        if (!static::$_hierachy_address) {
            $this->parse($country, $path);
        }
        return static::$_hierachy_address;
    }

    public function get($key = null, $json = false)
    {
        if ($key)
        {
            return isset(static::$info[$key]) ? static::$info[$key] : false;
        }

        if($json) return json_encode(static::$info);

        return static::$info;
    }

    public function getBy($key, $subKey) {
        if ($key && $subKey) {
            count(static::$info) || $this->get();
            if ($subKey) {
                $subKey = $this->_processString($subKey);
                return isset(static::$info[$key][$subKey]) ? static::$info[$key][$subKey] : false;
            }
        }

        return false;
    }

    public function set($key, $value)
    {
        static::$info[$key] = $value;
    }

    public function add($key, $value, $holding = null)
    {
        // $_val = $this->_processString($value);
        is_null($holding) ? static::$info[$key][$value] = $value : static::$info[$key][$holding][$value] = $value;
    }

    protected function _fetchStateRegion($info)
    {
        foreach ($info['state'] as $stateK => $stateV)
        {
            $this->add('state', $stateK);

            if (isset($stateV['region']))
            {
                foreach ($stateV['region'] as $regionK => $regionV)
                {
                    $stateK = $this->_processString($stateK);
                    $this->add('region', $regionV, $stateK);
                }
            }
        }
    }

    protected function _fetchFull($info)
    {
        foreach ($info['region'] as $regionK => $regionV)
        {
            $this->add('region', $regionK);

            if (isset($regionV['state']))
            {
                foreach ($regionV['state'] as $stateK => $stateV)
                {
                    $regionK = $this->_processString($regionK);
                    $this->add('state', $stateK, $regionK);

                    if (isset($stateV['district']))
                    {
                        foreach ($stateV['district'] as $districtK => $districtV)
                        {
                            $stateK = $this->_processString($stateK);
                            $this->add('district', $districtK, $stateK);

                            if (isset($districtV['suburb']))
                            {
                                foreach ($districtV['suburb'] as $suburbK => $suburbV)
                                {
                                    $districtK = $this->_processString($districtK);
                                    $this->add('suburb', $suburbV, $districtK);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected function _fetchDistrict($info)
    {

        foreach ($info['region'] as $regionK => $regionV)
        {
            $this->add('region', $regionK);
            if (is_object($regionV['state']))
            {

                foreach ($regionV['state'] as $stateK => $stateV)
                {
                    $regionK = $this->_processString($regionK);
                    $this->add('state', $stateK, $regionK);

                    if (isset($stateV['district']))
                    {
                        foreach ($stateV['district'] as $districtK => $districtV)
                        {
                            $stateK = $this->_processString($stateK);
                            $this->add('district', $districtV, $stateK);
                        }
                    }
                }
            }
        }
    }

    protected function _fetchCountryAu($info)
    {
        $this->_fetchStateRegion($info);
        $this->_fetchHierachyStateRegion();
    }

    protected function _fetchCountryHk($info)
    {
        static::$_hierachy_address = array(
            'root' => 'region',
            'region' => 'district',
            'district' => 'suburb'
        );
        foreach ($info['region'] as $regionK => $regionV)
        {
            $this->add('region', $regionK);

            if (isset($regionV['district']))
            {
                foreach ($regionV['district'] as $districtK => $districtV)
                {
                    $regionK = $this->_processString($regionK);
                    $this->add('district', $districtK, $regionK);

                    foreach ($districtV['suburb'] as $suburbK => $suburbV)
                    {
                        $districtK = $this->_processString($districtK);
                        $this->add('suburb', $suburbV, $districtK);
                    }
                }
            }
        }
    }

    protected function _fetchCountryMy($info)
    {
        $this->_fetchFull($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountryPh($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountryNz($info)
    {
        $this->_fetchStateRegion($info);
        $this->_fetchHierachyStateRegion();
    }

    protected function _fetchCountrySa($info)
    {
        $this->_fetchFull($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountrySg($info)
    {
        static::$_hierachy_address = array('root' => 'region');
        foreach ($info as $key => $value)
        {
            if (!empty($key))
            {
                $this->set($key, $value);
            }
        }
    }

    protected function _fetchCountryTh($info)
    {
        $this->_fetchFull($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountryId($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }

    protected function _fetchCountryTe($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountryVn($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyFull();
    }

    protected function _fetchCountryKsa($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }

    protected function _fetchCountryUae($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }

    protected function _fetchCountryBhr($info)
    {
        $this->_fetchHierachyDistrictRegion();
        $this->_fetchDistrict($info);
    }

    protected function _fetchCountryQat($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }

    protected function _fetchCountryOmn($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }

    protected function _fetchCountryKwt($info)
    {
        $this->_fetchDistrict($info);
        $this->_fetchHierachyDistrictRegion();
    }
    protected function _fetchCountryMmr($info)
    {
        static::$_hierachy_address = array(
            'root' => 'region',
            'region' => 'city',
            'city' => 'district'
        );
        foreach ($info['region'] as $regionK => $regionV)
        {
            $this->add('region', $regionK);

            if (isset($regionV['city']))
            {
                foreach ($regionV['city'] as $cityK => $cityV)
                {
                    $regionK = $this->_processString($regionK);
                    $this->add('city', $cityK, $regionK);

                    if (isset($cityV['district']))
                    {
                        foreach ($cityV['district'] as $districtK => $districtV)
                        {
                            $cityK = $this->_processString($cityK);
                            $this->add('district', $districtV, $cityK);
                        }
                    }
                }
            }
        }
    }
    protected function _fetchCountryTw($info)
    {
        static::$_hierachy_address = array(
            'root' => 'region',
            'region' => 'city',
            'city' => 'district'
        );
        foreach ($info['region'] as $regionK => $regionV)
        {
            $this->add('region', $regionK);

            if (isset($regionV['city']))
            {
                foreach ($regionV['city'] as $cityK => $cityV)
                {
                    $regionK = $this->_processString($regionK);
                    $this->add('city', $cityK, $regionK);

                    if (isset($cityV['district']))
                    {
                        foreach ($cityV['district'] as $districtK => $districtV)
                        {
                            $cityK = $this->_processString($cityK);
                            $this->add('district', $districtV, $cityK);
                        }
                    }
                }
            }
        }
    }

    protected function _fetchCountryEgy($info)
    {
        static::$_hierachy_address = array('root' => 'region');
        foreach ($info as $key => $value)
        {
            if (!empty($key))
            {
                $this->set($key, $value);
            }
        }
    }
    protected function _fetchCountryPak($info)
    {
        static::$_hierachy_address = array('root' => 'region');
        if(isset($info) && $info)
        {
            foreach ($info as $key => $value)
            {
                if (!empty($key))
                {
                    $this->set($key, $value);
                }
            }
        }
    }
    protected function _fetchHierachyStateRegion() {
        static::$_hierachy_address = array (
            'root' => 'state',
            'state' => 'region'
        );

        return static::$_hierachy_address;
    }

    protected function _fetchHierachyDistrictRegion() {
        static::$_hierachy_address = array (
            'root' => 'region',
            'region' => 'state',
            'state' => 'district'
        );

        return static::$_hierachy_address;
    }

    protected function _fetchHierachyFull() {
        static::$_hierachy_address = array (
            'root' => 'region',
            'region' => 'state',
            'state' => 'district',
            'district' => 'suburb'
        );
    }

    protected function _processString($string) {
        $string = str_replace(' ', '_', strtolower($string));
        $string = str_replace('-', '_', $string);
        return $string;
    }
}
