<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Date: 8/8/14
 * Time: 1:45 PM
 */
class AMI_Model extends CI_Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */
    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    protected $_order_rule = 'DESC';
    protected $_timestamp = FALSE;
    protected $_write_log = TRUE;
    protected $_ssd_import_error = [];
    public $_hash;
    public $rules = array();

    public $serialized_columns = array(
        'products' => array('sku_number', 'sku_name', 'qty_case', 'qty_pcs', 'free_case', 'free_pcs'),
        'pantry' => array('sku_number', 'current_qty_cases', 'current_qty_pcs', 'consumption_cases', 'consumption_pcs'),
        'pantry_competitor' => array('competitor_name', 'quantity', 'last_attempt', 'status', 'notes'),
        'sampling' => array('sku_number', 'type')
    );
    public $updatedless = array('ssd', 'tfo', 'call_records', 'route_master_temp', 'route_plan', 'country', 'module_fields', 'module_field_countries');
    public $sp_bound_tables = array('ssd', 'customers', 'feedbacks', 'salespersons', 'ssd');
    public $not_selectbox = array('supplier_id', 'ssd_id', 'armstrong_2_call_records_id', 'armstrong_2_tfo_id');

    // get from database
    public $timezones = array(
        'Asia/Ho_chi_minh' => '+07:00',
        'Asia/Ho_Chi_Minh' => '+07:00',
        'Asia/Saigon' => '+07:00',
        'Europe/Berlin' => '+01:00',
        'Asia/Kuala_Lumpur' => '+08:00', //Malaysia
        'Asia/Kuching' => '+08:00', //Malaysia
        'Australia/Lord_Howe' => '+10:30',
        'Australia/Hobart' => '+10:00',
        'Australia/Currie' => '+10:00',
        'Australia/Melbourne' => '+10:00',
        'Australia/Sydney' => '+10:00',
        'Australia/Broken_Hill' => '+09:30',
        'Australia/Brisbane' => '+10:00',
        'Australia/Lindeman' => '+10:00',
        'Australia/Adelaide' => '+09:30',
        'Australia/Darwin' => '+09:30',
        'Australia/Perth' => '+08:00',
        'Australia/Eucla' => '+08:45',
        'Australia/Canberra' => '+10:00',
        'Asia/Hong_Kong' => '+08:00',
        'Africa/Johannesburg' => '+02:00', // South Africa
        'Asia/Singapore' => '+08:00',
        'Asia/Taipei' => '+08:00', // taiwan
        'Asia/Bangkok' => '+07:00', // thailan
        'Asia/Manila' => '+08:00', //Philippines
        'Pacific/Chatham' => '+12:00', //New Zealand
        'Pacific/Auckland' => '+12:00' //New Zealand
    );

    public $as2_id_tables = array(
        'call_records' => 'CAL',
        'customers' => 'OPD',
        'tfo' => 'TFO',
        'psd' => 'PSD',
        'psd_dist' => 'PDT',
        'objective_records' => 'OBJ',
        'pantry_check' => 'PAN',
        'route_plan' => 'ROU',
        'salespersons' => 'SAL',
        'sampling' => 'SAM',
        'distributors' => 'DIS',
        'wholesalers' => 'WHS',
        'kpi' => 'KPI',
//        'promotions' => 'PRO',
        'feedbacks' => 'FBK',
        'templates' => 'TEM',
        'prepare_calls' => 'PRC',
        'material' => 'MTR',
        'absent_day' => 'ASD',
        'holiday' => 'HLD',
        'fulfillment_rate' => 'FFR',
        'chains' => 'CHN',
        'webshop' => 'WBS'
    );
    public $as2_id_tb_log = array(
        'activities_ami_log' => 'A',
        'activities_leader_log' => 'L',
        'activities_log' => 'P',
        'activities_push_log' => 'U'
    );

    public $ssd_import_column = array(
        'armstrong_2_ssd_id',
        'armstrong_2_salespersons_id',
        'salespersons',
        'armstrong_2_customers_id',
        'customers',
        'armstrong_2_distributors_id',
        'distributors_name',
        'armstrong_1_wholesalers_id',
        'armstrong_2_wholesalers_id',
        'wholesalers_name',
        'armstrong_2_call_records_id',
        'sku_number',
        'sku_name',
        'packing_size',
        'qty_cases',
        'qty_pcs',
        'free_qty_cases',
        'free_qty_pcs',
        'total_price',
        'invoice_number',
        'method',
        'ssd_month',
        'ssd_year',
        'ssd_date',
        'date_created',
        'delivery_date',
        'remarks',
        'new_sku',
        'status',
        'business_type',
        'country_id'
    );
    public $ssd_validate_column = array(
        'armstrong_2_salespersons_id',
        'armstrong_2_customers_id',
        'sku_number',
        'country_id',
        'ssd_date'
    );
    protected static $_importKeys = array();

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->_order_by = $this->_order_by ? $this->_order_by : $this->_primary_key;
        $this->_order_by .= ' ' . $this->_order_rule;
        $this->_hash = random_string('alnum', '44');
    }

    public function array_from_post($fields)
    {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function get($id = NULL, $single = FALSE, $order_by = NULL, $cols = '*')
    {
        if ($id != NULL) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row_array';
        } elseif ($single == TRUE) {
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }

        !is_array($cols) || $cols = implode(',', $cols);
        $this->db->select($cols);

        if (!count($this->db->ar_orderby)) {
            $order_by == NULL ? $this->db->order_by($this->_order_by) : $this->db->order_by($order_by);
        }

        $data = $this->db->get($this->_table_name)->$method();
        return $data;
    }

    public function get_by($where, $single = FALSE, $order_by = NULL, $cols = '*', $db_joins = null)
    {
        $this->db->where($where);
        if ($db_joins) {
            foreach ($db_joins as $j) {
                $this->db->join($j['db'], $j['query'], $j['type']);
            }
        }
        return $this->get(NULL, $single, $order_by, $cols);
    }

    public function get_in($field, $values = array(), $single = FALSE, $order_by = NULL, $cols = '*')
    {
        $this->db->where_in($field, $values);
        return $this->get(NULL, $single, $order_by, $cols);
    }

    public function get_limit($limit, $where = null, $order_by = NULL, $cols = '*', $db_joins = NULL)
    {

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $this->db->limit($total, $offset);
        if ($db_joins) {
            foreach ($db_joins as $j) {
                $this->db->join($j['db'], $j['query'], $j['type']);
            }
        }
        if ($where) {
            return $this->get_by($where, false, $order_by, $cols);
            /*if (is_array($where))
            {
                list($field, $values) = $where;
                return $this->get_in($field, $values, false, $order_by, $cols);
            }
            else
            {
                return $this->get_by($where, false, $order_by, $cols);
            }*/
        } else {
            return $this->get(NULL, false, $order_by, $cols);
        }
    }

    public function count($rule, $condition, $db_joins = NULL)
    {
        $this->db->$rule($condition);
        if ($db_joins) {
            foreach ($db_joins as $j) {
                $this->db->join($j['db'], $j['query'], $j['type']);
            }
        }
        return $this->db->count_all_results($this->_table_name);
    }

    public function compare_data($data_old, $data_new)
    {
        $result = array(
            'old' => array(),
            'new' => array()
        );
        if ($data_old != null) {
            $data_old = CI_Controller::get_instance()->convertToArray($data_old);
            $data_new = CI_Controller::get_instance()->convertToArray($data_new);
            unset($data_old['last_updated']);
            unset($data_new['last_updated']);

            foreach ($data_new as $k => $d) {
                if ($d != $data_old[$k]) {
                    $result['old'][$k] = $data_old[$k];
                    $result['new'][$k] = $data_new[$k];
                } else {
                    $result['new'][$k] = $data_new[$k];
                }
            }
        } else {
            $result['old'] = array();
            $result['new'] = $data_new;

        }
        return $result;
    }
    public $_table_has_id_fields = array(
        'call_records',
        'material',
        'tfo',
        'sampling',
        'objective_records',
        'pantry_check',
        'route_plan',
        'psd',
        'psd_dist',
        'prepare_calls',
        'absent_day',
        'holiday',
        'fulfillment_rate',
        'feedbacks',
        'chains',
        'wholesalers',
        'distributors',
        'customers',
        'salespersons',
    );
    //$this->generateArmstrongIdBaseId($insert, $type, $country_id, $data["armstrong_2_{$type}_id"]);
    public function generateArmstrongIdBaseId($insert_id, $table, $country_id, $uniqid = '')
    {
        // file_put_contents('logid.txt', "\n".$insert_id.$table.$country_id.$uniqid, FILE_APPEND);
        $country = $this->getEntry('country', array('id' => $country_id), 1);
        if (!empty($country[0]->id)) {
            $country_code = $country[0]->code;
        } else {
            //  file_put_contents('logid.txt', "\nLỗi country: ".$insert_id.$table.$country_id.$uniqid, FILE_APPEND);
            return array('error' => 'Invalid country id!');
            exit;
        }
        $atoz = array('B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $prefix_key = 0;
        $maxnumber = 99999999;
        if($insert_id > $maxnumber)
        {
            $prefix_key = $insert_id/$maxnumber;
            $insert_id = $insert_id%$maxnumber;
        }
        $zeros = 8 - strlen($insert_id);
        if ($zeros) {
            for ($i = 0; $i < $zeros; $i++) {
                $insert_id = "0".$insert_id;
            }
        }
        $result = strtoupper($this->as2_id_tables[$table].$atoz[$prefix_key] . $insert_id . $country_code);
        $sql = "INSERT INTO `uniqid_log` SET `table` = '".$table."', `insert_id` = '".$insert_id."', `country_id` = '".$country_id."', `uniqid` = '".$uniqid."', `newid` = '".$result."', `date_created` = '".date('Y-m-d H:i:s')."'";
        $this->db->query($sql);
        return $result;
    }
    public function save($data, $id = NULL, $as2_id = false, $event = null, $country_id = null)
    {
        $log_datas = array();
        // trim data
        foreach ($data as &$_data) {
            if (is_array($_data)) {
                foreach ($_data as &$item) {
                    $item = trim($item);
                }
            } else {
                $_data = trim($_data);
            }
        }
        //Set timestamp
        $now = date('Y-m-d H:i:s');
        if ($this->_timestamp == TRUE) {
            // $id || $data['date_created'] = $now;
            $id || $data['date_created'] = (array_key_exists("date_created", $data) ? $data['date_created'] : $now);
            $data['last_updated'] = $now;
        }

        $user = $this->session->userdata('user_data');
        $salespersons_id = (isset($user['id'])) ? $user['id'] : '';
        $key_datas = array_keys($data);
        $oldData = $newData = array();

        foreach ($this->as2_id_tables as $key => $val) {
            $id_column = 'armstrong_2_' . $key . '_id';
            $customer_id = (isset($data[$id_column])) ? $data[$id_column] : $id;
            if (in_array($id_column, $this->db->list_fields($this->_table_name))) {
                $exist = Capsule::table($this->_table_name)->where($id_column, '=', $customer_id)->first();
                if ($event == 'DELETE') {
                    $log_datas['event_name'] = $event;
                } else {
                    if ($exist) {
                        $log_datas['event_name'] = 'UPDATE';
                    } else {
                        $log_datas['event_name'] = 'INSERT';
                    }
                }
                break;
            } else {
                if (!isset($this->as2_id_tables[$this->_table_name]))
                    $exist = Capsule::table($this->_table_name)->where('id', '=', $id)->first();
            }
        }
        // TODO compare data
        $new_id = $log_id = '';
        if ($this->_write_log == true) {
            $data_compare = $this->compare_data($exist, $data);

            if (!isset($log_datas['event_name'])) {
                $exist = Capsule::table($this->_table_name)->where('id', '=', $id)->first();
                if (!$exist) {
                    $log_datas['event_name'] = 'INSERT';
                    foreach ($key_datas as $key_data) {
                        $newData[$key_data] = $data[$key_data];
                    }
                    $oldData = '';
                } else {
                    $log_datas['event_name'] = 'UPDATE';
                    foreach ($key_datas as $key => $key_data) {
                        if (is_array($exist) ? $exist[$key_data] : $exist->$key_data != $data[$key_data]) {
                            $oldData[$key_data] = is_array($exist) ? $exist[$key_data] : $exist->$key_data;
                            $newData[$key_data] = $data[$key_data];
                        }
                    }
                }
            }

//            dd($data_compare);
            $log_datas['armstrong_2_salespersons_id'] = $salespersons_id;
            $log_datas['old_data'] = (count($data_compare['old']) > 0) ? json_encode($data_compare['old']) : '';
            $log_datas['new_data'] = json_encode($data_compare['new']);
            $log_datas['object_name'] = $this->_table_name;
            $log_datas['time_zone'] = date_default_timezone_get();
            $log_datas['country_id'] = (isset($user['country_id'])) ? $user['country_id'] : '';
            $log_datas['ipad_str'] = 'AMI';
            $log_datas['version'] = 'AMI-2.0';
            $log_datas['ip'] = $this->session->userdata['ip_address'];
            $log_table = '';
            $log_table = 'activities_ami_log';
            $log_alias = $this->as2_id_tb_log['activities_ami_log'];

            if ($log_table) {
                $country_code = Capsule::table('country')->select('code')->where('id', '=', $log_datas['country_id'])->first();
                $country_code = CI_Controller::get_instance()->convertToArray($country_code);
                $log_datas['date_start'] = $log_datas['date_end'] = $now;
                // $this->db->insert($log_table, $log_datas);
                // $new_id = $this->db->insert_id();
                $log_id = $log_alias . $new_id;
                // $this->db->update($log_table, array('armstrong_2_log_id' => $log_id), array('id' => $new_id));
            }
        }
        //Insert
        if ($id === NULL) {
            // TODO generate as2_id when insert new record
            if ($as2_id == true) {
                if (isset($this->as2_id_tables[$this->_table_name])) {
                    if($country_id && in_array($this->_table_name, $this->_table_has_id_fields))
                    {
                        $data['armstrong_2_' . $this->_table_name . '_id'] = uniqid();
                    }
                    else
                    {
                        $data['armstrong_2_' . $this->_table_name . '_id'] = $this->generateArmstrongId($user['country_id'], $this->_table_name);
                    }
                }
            }

            if (in_array('log_ids', $this->db->list_fields($this->_table_name))) {
                $data['log_ids'] = $log_id;
            }
            if (!in_array('date_sync', $this->db->list_fields($this->_table_name))) {
                $data['date_sync'] = $now;
            }

            $this->db->set($data);
            $this->db->insert($this->_table_name);
            if ($as2_id == true) {
                if (isset($this->as2_id_tables[$this->_table_name])) {
                    if($country_id && in_array($this->_table_name, $this->_table_has_id_fields))
                    {
                        $insert = $this->db->insert_id();
                        
                        $armstrong_2_id = $this->generateArmstrongIdBaseId($insert, $this->_table_name, $country_id, $data['armstrong_2_' . $this->_table_name . '_id']);
                        $data['armstrong_2_' . $this->_table_name . '_id'] = $armstrong_2_id;
                        $this->db->set('armstrong_2_' . $this->_table_name . '_id', $armstrong_2_id);
                        $this->db->where('id',$insert);
                        $this->db->update($this->_table_name);
                    }
                }
            }
            if(isset($data['armstrong_2_' . $this->_table_name . '_id']))
            {
                $id = $data['armstrong_2_' . $this->_table_name . '_id'];
            }
            else
            {
                $id = $this->db->insert_id();
            }
        } // Update
        else {
            if ($new_id) {
                if (in_array('log_ids', $this->db->list_fields($this->_table_name))) {
                    $logs_id = Capsule::table($this->_table_name)->select('log_ids')->where($this->_primary_key, '=', $id)->first();
                    $logs_id = CI_Controller::get_instance()->convertToArray($logs_id);
                    $data['log_ids'] = $logs_id['log_ids'] . ',' . $log_id;
                }
            }

            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
        }
        return $id;
    }

    public function update_in($data, $field, array $id)
    {
        if ($this->_timestamp == TRUE) {
            $now = date('Y-m-d H:i:s');
            $id || $data['date_created'] = $now;
            $data['last_updated'] = $now;
        }

        $this->db->where_in($field, $id)
            ->update($this->_table_name, $data);

        return $id;
    }

    public function delete($id)
    {
        $filter = $this->_primary_filter;
        $id = $filter($id);
        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }

    /**
     * @param $cols = array('{primary_key}', '{display_column}')
     * @param null $id
     * @param null $where
     * @param bool $single
     * @return array|bool
     */
    public function parse_form($cols, $default = NULL, $where = NULL, $single = FALSE)
    {
        $return = array();
        if ($where != NULL) {
            $data = $this->get_by($where, $single, NULL, $cols);
        } else {
            $data = $this->get(NULL, $single, NULL, $cols);
        }

        $default == NULL || $return[''] = $default;

        if (count($data)) {
            foreach ($data as $val) {
                $return[$val[$this->_primary_key]] = $val[$cols[1]];
            }
        }

        return $return;
    }

    public function getEntry($type, $args = false, $limit = false, $select = '*', $author = false, $array_result = false, $order = false, $time_zone = FALSE)
    {
        $timezone_current = $this->timezones[Date("e")];
        $this->db->select($select);
        if ($time_zone != FALSE) {
            $timezone_convert = $this->timezones[$time_zone['timezone']];
            for ($i = 0; $i < count($time_zone['colum_convert']); $i++) {
                $this->db->select("CONVERT_TZ(`" . $time_zone['colum_convert'][$i] . "`,'" . $timezone_current . "','" . $timezone_convert . "') as `" . $time_zone['colum_convert'][$i] . "`", FALSE);
            }
        }
        $this->db->from($type);
        if (isset($args['ipad_str'])) unset($args['ipad_str']);
        if (isset($args['time_zone'])) unset($args['time_zone']);

        if ($args && is_array($args)) {
            foreach ($args as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $k => $v) {
                        $this->db->where($key, $v);
                    }
                } else {
                    $this->db->where($key, $val);
                }
            }
        }
        if ($author) {
            $this->db->join('admins', "{$type}.admin_ID = admins.ID", 'left');
        }
        if ($order) {
            $this->db->order_by($order);
        }
        if ($limit) {
            if (!is_array($limit)) {
                $this->db->limit($limit);
            } else {
                $this->db->limit($limit[0], $limit[1]);
            }
        }
        $query = $this->db->get();
        $ret = $array_result ? $query->result_array() : $query->result();

        $query->free_result();
        return $ret;
    }

    public function generateArmstrongId($country_id, $table = false)
    {

        $country = $this->getEntry('country', array('id' => $country_id), 1);
        if (!empty($country[0]->id)) {
            $country_code = $country[0]->code;
        } else {
            return array('error' => 'Invalid country id!');
            exit;
        }
        if (!$table) {
            return array('error' => 'Invalid request.');
            exit;
        } else if (!array_key_exists($table, $this->as2_id_tables)) {
            return array('error' => 'Invalid type.');
            exit;
        }
        $tabletype = $this->db->escape($table);
        $query = $this->db->query("SELECT MAX(armstrong_2_" . str_replace(array('\'', '"'), '', $tabletype) . "_id) 
            AS latest_armstrong_id FROM " . str_replace(array('\'', '"'), '', $tabletype) . " 
            WHERE SUBSTR(armstrong_2_" . str_replace(array('\'', '"'), '', $tabletype) . "_id, '13') = '" . strtoupper($country_code) . "' 
                AND SUBSTR(armstrong_2_" . str_replace(array('\'', '"'), '', $tabletype) . "_id, 5,8) REGEXP '^-?[0-9]+$'");

        $result = $query->result();
        $latest_id = !empty($result[0]->latest_armstrong_id) ? $result[0]->latest_armstrong_id : 'AAAA00000000AA';
        return $this->as2_id_tables[$table] . $this->incrementId(strtoupper($country_code), $latest_id);
    }

    private function incrementId($country, $aid = false)
    {
        if ($aid) {
            $atoz = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
            $prefix = substr_replace($aid, '', 0, -11);
            $prefix = substr_replace($prefix, '', 1);
            $increment = substr_replace($aid, '', 0, -10);
            $increment = substr_replace($increment, '', 8);
            settype($increment, 'integer');

            $prefix_key = array_search($prefix, $atoz);
            settype($prefix_key, 'integer');

            if ($increment == 99999999) {
                if ($prefix_key == 25) {
                    return array('error' => 'Limit Reached.');
                    exit;
                }
                $prefix_key++;
                $increment = '00000001';
            } else {
                $increment++;
                $zeros = 8 - strlen($increment);
                if ($zeros) {
                    for ($i = 0; $i < $zeros; $i++) {
                        $increment = "0{$increment}";
                    }
                }
            }
        } else {
            $prefix_key = 0;
            $increment = '00000001';
        }

        $result = $atoz[$prefix_key] . $increment . $country;

        return $result;
    }

    public function importToDb($countryId, array $data, array $excluded = array(), $module = null)
    {
        if (empty($data)) {
            return false;
        }

        $fields = $this->db->list_fields($this->_table_name);

        if (!in_array($this->_primary_key, $excluded)) {
            $excluded[] = $this->_primary_key;
        }

        $fields = array_except(array_flip($fields), $excluded);
        $fields = array_flip($fields);

        // $this->db->trans_start(); // Query will be rolled back

        $first = reset($data);

        $arrayKeys = array();
        foreach ($first as $_value) {
            $arrayKeys[] = strtolower(str_replace(' ', '_', $_value));
        }

        unset($data[0]); // Column titles

        foreach ($data as $key => $value) {

            // if (!$key)
            // {
            //     continue;
            // }

            $value = array_combine($arrayKeys, $value);

            $value = array_only($value, $fields);

            foreach ($value as $key => &$__value) {
                if (!$__value || $__value == '') {
                    $__value = ' ';
                }
            }

            if ($countryId && isset($this->as2_id_tables[$this->_table_name])) {
                $value["armstrong_2_{$this->_table_name}_id"] = $this->generateArmstrongId($countryId, $this->_table_name);
            }
            if ($module == 'customers') {
                $otm_new_fields = [
                    'meals_per_day_new', 'selling_price_per_meal_new', 'weeks_per_year_new', 'days_per_week_new', 'food_turnover_new',
                    'assumption_food_cost_new', 'assumption_ufs_share_new', 'convenience_factor_new', 'food_purchase_value_new',
                    'ufs_share_food_cost_new', 'otm_potential_value_new'
                ];
                foreach ($value as $key => &$val) {
                    if (in_array($key, $otm_new_fields)) {
                        $val = (float)$val;
                    }
                }
            }
            $this->save($value);
        }

        // $this->db->trans_complete();
    }

    public function importSsdToDb($countryId, array $data, array $excluded = array())
    {
        if (empty($data)) {
            return false;
        }
        $this->_write_log = false;
        $fields = $this->db->list_fields($this->_table_name);

        if (!in_array($this->_primary_key, $excluded)) {
            $excluded[] = $this->_primary_key;
        }

        $fields = array_except(array_flip($fields), $excluded);
        $fields = array_flip($fields);
        // $this->db->trans_start(); // Query will be rolled back

        $first = reset($data);

        $arrayKeys = array();
        foreach ($first as $_value) {
            $arrayKeys[] = strtolower(str_replace(' ', '_', $_value));
        }

        unset($data[0]); // Column titles

        $data_import = array();

        foreach ($data as $key => $value) {
            $value = array_combine($arrayKeys, $value);
            // TODO check row have all data to import
            $val_of_array = array_values($value);
            $str_val = '';
            for ($i = 0; $i < count($val_of_array); $i++) {
                $str_val .= $val_of_array[$i];
            }
            if (strlen($str_val) > 0) {
                if ($value['armstrong_2_ssd_id'] == null || $value['armstrong_2_salespersons_id'] == null || $value['armstrong_2_customers_id'] == null || $value['sku_number'] == null || $value['country_id'] == null || $value['ssd_date'] == null) {
                    return false;
                    exit();
                }
                if ($value['armstrong_2_ssd_id'] == '' || $value['armstrong_2_salespersons_id'] == '' || $value['armstrong_2_customers_id'] == '' || $value['sku_number'] == '' || $value['country_id'] == '' || $value['ssd_date'] == '') {
                    return false;
                    exit();
                }
                $check_ssdDate = DateTime::createFromFormat('Y-m-d H:i:s', $value['ssd_date']);
                if (!$check_ssdDate) {
                    return false;
                    exit();
                }
                // TODO Get product name and caculate total price
                $sku_number = $value['sku_number'];
                $product = $this->db
                    ->from('products')
                    ->where(array(
                        'sku_number' => $sku_number,
                        'country_id' => $value['country_id']
                    ))
                    ->get()->result_array();
                if ($product) {
                    $value['sku_name'] = $product[0]['sku_name'];
                    if ($product[0]['price'] > 0) {
                        $total_price = ($product[0]['price'] * $value['qty_cases']) + ($product[0]['price'] / $product[0]['quantity_case'] * $value['qty_pcs']);
                    } else {
                        $total_price = 0;
                    }
                    $value['total_price'] = $total_price;
                }
                if (strtolower(substr($value['armstrong_2_customers_id'], 0, 3)) == 'opd') {
                    $db_import = 'ssd';
                } else {
                    $db_import = 'ssd_push';
                }
                $data_import[$db_import][$value['armstrong_2_ssd_id']][] = $value;
            }
        }

        foreach ($data_import as $tb => $data_tb) {
            foreach ($data_tb as $_datas) {
                foreach ($_datas as $k => &$value) {
                    $value = array_only($value, $fields);
                    foreach ($value as $key => &$__value) {
                        if (!$__value || $__value == '') {
                            $__value = ' ';
                        }
                    }
                    $value['ssd_group'] = ($k == 0) ? 1 : 0;
                    $value['status'] = ($value['status'] == 'pending') ? 0 : 1;
                    if ($tb == 'ssd_push') {
                        $value['armstrong_2_ssd_push_id'] = $value['armstrong_2_ssd_id'];
                        unset($value['armstrong_2_ssd_id']);
                    }
                    if ($k == 0) {
                        if ($tb == 'ssd_push') {
                            $parent_id_insert = $this->ssd_push_m->save($value);
                        } else {
                            $parent_id_insert = $this->save($value);
                        }
                        if ($parent_id_insert) {
                            $ssd_id = generateSsdId($parent_id_insert, $this->country_code);
                            if ($tb == 'ssd_push') {
                                $this->ssd_push_m->save(array('armstrong_2_ssd_push_id' => $ssd_id), $parent_id_insert);
                            } else {
                                $this->save(array('armstrong_2_ssd_id' => $ssd_id), $parent_id_insert);
                            }
                        }
                    } else {
                        if ($tb == 'ssd_push') {
                            $id_insert = $this->ssd_push_m->save($value);
                            if ($id_insert) {
                                $this->ssd_push_m->save(array('armstrong_2_ssd_push_id' => $ssd_id), $id_insert);
                            }
                        } else {
                            $id_insert = $this->save($value);
                            if ($id_insert) {
                                $this->save(array('armstrong_2_ssd_id' => $ssd_id), $id_insert);
                            }
                        }
                    }
                }
            }
        }
        // $this->db->trans_complete();
        $this->_write_log = TRUE;
        return true;
    }

    public function importSSD($countryId, array $data, array $excluded = array())
    {
        if (empty($data)) {
            return false;
        }
        $this->_write_log = false;
        $fields = $this->db->list_fields($this->_table_name);

        if (!in_array($this->_primary_key, $excluded)) {
            $excluded[] = $this->_primary_key;
        }

        $fields = array_except(array_flip($fields), $excluded);
        $fields = array_flip($fields);

        // $this->db->trans_start(); // Query will be rolled back

        $first = reset($data);

        $arrayKeys = array();
        foreach ($first as $_value) {
            $arrayKeys[] = strtolower(str_replace(' ', '_', $_value));
        }
        $data_import = array();
        $data_error = array();
        foreach ($data as $key => $value) {
            $flag_validate = false;
            $value = array_combine($this->ssd_import_column, $value);
            // TODO check row have all data to import
            $val_of_array = array_values($value);
            $str_val = '';
            for ($i = 0; $i < count($val_of_array); $i++) {
                $str_val .= $val_of_array[$i];
            }

            if (strlen($str_val) > 0) {
                // validate cell format
                foreach ($this->ssd_validate_column as $validate) {
                    if (isset($value[$validate])) {
                        if ($value[$validate] == '' || $value[$validate] == null) {
                            $data_error[] = $value;
                            $flag_validate = true;
                            continue;
                        }
                    } else {
                        $data_error[] = $value;
                        $flag_validate = true;
                        continue;
                    }
                }
                if ($flag_validate) {
                    continue;
                }
                $value['ssd_date'] = date('Y-m-d H:i:s', strtotime($value['ssd_date']));
                // TODO Get product name and caculate total price
                $sku_number = $value['sku_number'];
                $product = $this->db
                    ->from('products')
                    ->where(array(
                        'sku_number' => $sku_number,
                        'country_id' => $value['country_id']
                    ))
                    ->get()->result_array();
                if ($product) {
                    $value['sku_name'] = $product[0]['sku_name'];
                    if ($product[0]['price'] > 0) {
                        $total_price = ($product[0]['price'] * $value['qty_cases']) + ($product[0]['price'] / $product[0]['quantity_case'] * $value['qty_pcs']);
                    } else {
                        $total_price = 0;
                    }
                    $value['total_price'] = $total_price;
                }
                if (strtolower(substr($value['armstrong_2_customers_id'], 0, 3)) == 'opd') {
                    $db_import = 'ssd';
                } else {
                    $db_import = 'ssd_push';
                }
                $data_import[$db_import][] = $value;
            }
        }
//        dd($this->_ssd_import_error);
        if ($data_import) {
            foreach ($data_import as $tb => $_datas) {
                foreach ($_datas as $k => &$value) {
                    $value = array_only($value, $fields);

                    foreach ($value as $key => &$__value) {
                        if (!$__value || $__value == '') {
                            $__value = ' ';
                        }
                    }
                    $value['ssd_group'] = ($k == 0) ? 1 : 0;
                    $value['status'] = ($value['status'] == 'approved') ? 1 : 0;
                    if ($tb == 'ssd_push') {
                        $value['armstrong_2_ssd_push_id'] = $value['armstrong_2_ssd_id'];
                        unset($value['armstrong_2_ssd_id']);
                    }

                    if ($k == 0) {
                        if ($tb == 'ssd_push') {
                            $parent_id_insert = $this->ssd_push_m->save($value);
                        } else {
                            $parent_id_insert = $this->save($value);
                        }
                        if ($parent_id_insert) {
                            $ssd_id = generateSsdId($parent_id_insert, $this->country_code);
                            if ($tb == 'ssd_push') {
                                $this->ssd_push_m->save(array('armstrong_2_ssd_push_id' => $ssd_id), $parent_id_insert);
                            } else {
                                $this->save(array('armstrong_2_ssd_id' => $ssd_id), $parent_id_insert);
                            }
                        }
                    } else {
                        if ($tb == 'ssd_push') {
                            $id_insert = $this->ssd_push_m->save($value);
                            if ($id_insert) {
                                $this->ssd_push_m->save(array('armstrong_2_ssd_push_id' => $ssd_id), $id_insert);
                            }
                        } else {
                            $id_insert = $this->save($value);
                            if ($id_insert) {
                                $this->save(array('armstrong_2_ssd_id' => $ssd_id), $id_insert);
                            }
                        }
                    }
                }
            }
        }

        // $this->db->trans_complete();
        $this->_write_log = TRUE;
        return $data_error ?: false;
    }

    public function getTableName()
    {
        return $this->_table_name;
    }

    public function getTablePrimary()
    {
        return $this->_primary_key;
    }


    /**
     * DataTables functions
     */
    protected static $_datatablesSelections = null;
    protected static $_datatablesRelations = null;
    protected static $_datatablesConditions = null;

    public function setDatatalesSelections(array $selections)
    {
        static::$_datatablesSelections = $selections;
        return $this;
    }

    public function setDatatalesRelations(array $relations)
    {
        static::$_datatablesRelations = $relations;
        return $this;
    }

    public function setDatatalesConditions(array $conditions)
    {
        static::$_datatablesConditions = $conditions;
        return $this;
    }

    public function appendToSelectStr()
    {
        return static::$_datatablesSelections;
    }

    public function fromTableStr()
    {
        return $this->_table_name;
    }

    public function joinArray()
    {
        return static::$_datatablesRelations;
    }

    public function whereClauseArray()
    {
        return static::$_datatablesConditions;
    }

    // TODO : save progress to temporary json file (upload) - Erika
    public function saveProgressToJson($current, $total, $session)
    {
        $arr_content['progress'] = true;
        $arr_content['hightrow'] = $total;
        $arr_content['rowcurrent'] = $current;
        file_put_contents('res/up/shipment_import_logs/shipment_' . $session . '.json', json_encode($arr_content));
    }
}