<?php

class AMI_Controller extends CI_Controller
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLE DECLARATION
    |--------------------------------------------------------------------------
    */

    public $data = array();
    public $countries = array();
    public $country = array();
    public $country_code = '';
    public $country_id = '';
    public $country_name = '';
    public $country_currency = '';
    public $method = 'GET';
    public $roleName = '';
    public $userdata = array();
    public $country_region = "";
    private $exception_uri = array(
        'ami/global_ssd/generateMapCsv',
        'ami/dashboard/login',
        'ami/dashboard/check_login',
        'ami/migration',
    );

    private $table_name = 'ami_modules';
    private $left_column_name = 'lft';
    private $right_column_name = 'rgt';
    private $primary_key_column_name = 'id';
    private $parent_column_name = 'parent_id';
    private $text_column_name = 'name';

    protected static $role = array();
    protected static $modules = array();

    public $MEDIA_URL = '';
    public $MEDIA_DIR = '';
    public $MEDIA_FOLDER = array(
        'customers_photos', 'news_feed', 'quality_control', 'salespersons', 'uplift'
    );
    public $app_type = array(
        0 => 'PULL',
        1 => 'PUSH',
        2 => 'ALL'
    );
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->library('debug');
        $this->load->library(array('IlluminateDatabase', 'Allias', 'Request', 'session', 'mail', 'pagination', 'excel'));
        // $this->load->library( 'datatables' );
        $this->load->model('country_m');
        $this->load->model('menu_m');
        $this->load->model('roles_m');
        $this->load->model('authentication_m');
        $this->data['errors'] = array();
        $this->data['page_title'] = config_item('site_name');
        $this->data['per_page'] = 20;
        //$this->session->sess_destroy();

        $this->_prepareTwig();
        $this->_setMediaOptions();

        // Set value for nested_set
        $this->nested_set->setControlParams($this->table_name, $this->left_column_name, $this->right_column_name,
            $this->primary_key_column_name, $this->parent_column_name, $this->text_column_name);

        // check logged in
        $this->restrict();
        $report_perm = [];
        $session_data = $this->session->userdata('user_data');

        $this->userdata = $session_data;
        if(!empty($session_data)){
            $this->user_id = $session_data['id'];
            $this->user_name = $session_data['name'];
        }

        $session_flash_message['message'] = $this->session->flashdata('message') ? $this->session->flashdata('message') : '';
        $session_flash_message['flash_type'] = $this->session->flashdata('flash_type') ? $this->session->flashdata('flash_type') : '';
        if (isset($session_data['country_id'])) {
            $country = $this->country_m->get_by(array('id' => $session_data['country_id']), true);
            $this->country = $country;
            $this->country_id = $country['id'];
            $this->country_name = $country['name'];
            $this->country_code = $session_data['country_code'] = $country['code'];
            $this->country_code = strtolower($this->country_code);
            $this->country_currency = $country['currency'];
            $this->country_region = $country['region'];
            
            $role = $this->roles_m->get_by(array(
                'id' => $session_data['roles_id'],
//                'country_id' => $country['id']
            ), true);

            if ($role) {
                // get report view permission
                $report_arr = array('summary_report_view', 'transaction_report_view', 'master_report_view', 'business_report_view');
                $role_restrict = json_decode($role['restrictions']);
                foreach($report_arr as $_report){
                    if(isset($role_restrict->$_report)){
                        $report_perm[$_report] = 1;
                    }else{
                        $report_perm[$_report] = 0;
                    }
                }

                $this->roleName = $session_data['roleName'] = $role['name'];
            } else if ($session_data['roles_id'] == Authentication_m::ADMIN_ROLES_ID) {
                $this->roleName = $session_data['roleName'] = 'Admin';
            }
        }

        $this->method = $_SERVER['REQUEST_METHOD'];

     //   $countries = $this->db->get('country')->result_array();
        $countries = $this->country_m->get(null, false, 'id');
        $this->countries = $countries;
        $superAdmin = false;
        if(isset($session_data['id'])){
            if($this->isSuperAdmin($session_data['id'])){
                $superAdmin = true;
            }
        }

        $this->twig->addGlobal('super_admin', $superAdmin);
        $this->twig->addGlobal('report_perm', $report_perm);
        $this->twig->addGlobal('countries', $countries);
        $this->twig->addGlobal('session', $session_data);
        $this->twig->addGlobal('session_flash_message', $session_flash_message);
        // $this->twig->addGlobal('router', $this->router);

        // Getting menu items
        $menuItems = $this->menu_m->getMenuItems();
        $menuHierarchy = array();
        // TODO: webshop only shows for SA country
        if($this->country_id != 4){
            unset($menuItems[7]);
        }

        // TODO: shipment plan shows only for MEP countries
        if($this->country_region != "MEP"){
            unset($menuItems[1]['child_nodes'][6]);
        }

        foreach ($menuItems as &$menuItem) {
            $menuItem['canView'] = false;

            if (!empty($menuItem['child_nodes'])) {
                foreach ($menuItem['child_nodes'] as &$item) {
                    $menuHierarchy[$item['slug']] = $menuItem['name'];
                    if ($item['canView'] = $this->hasPermission('view', $item['id'])) {
                        $menuItem['canView'] = true;
                    }
                }
            }
        }
        $this->twig->addGlobal('menuHierarchy', $menuHierarchy);
        $this->twig->addGlobal('menuItems', $menuItems);

        $this->_getPendingCustomer();
        $this->_getPendingSsd();
        $this->getMenuData();
    }

    protected function _prepareTwig()
    {
        $this->twig->addGlobal('Carbon', new Carbon);
        $this->twig->addGlobal('Router', $this->router);

        $this->twig->addFunction('pagination', function ($config) {
            $this->pagination->initialize($config);

            $link = $this->pagination->create_links();
            $get = $this->input->get();
            if (isset($get['p'])) {
                unset($get['p']);
            }
            if (!$get) {
                $link = str_replace('&amp;' . $this->pagination->query_string_segment, '?' . $this->pagination->query_string_segment, $link);
            }

            return $link;
        }, array('is_safe' => array('html')));

        $this->twig->addFunction('has_permission', array($this, 'hasPermission'));
    }

    protected function _setMediaOptions()
    {
        $this->MEDIA_URL = $this->config->item('media_url');
        $this->MEDIA_DIR = $this->config->item('media_dir');

        // Dev Env
//         $this->MEDIA_URL = 'http://dev.ufs-armstrong.com/armstrong-v3/res/up';
//         $this->MEDIA_DIR = '../armstrong-v3/res/up';

        // Prod Env
//        $this->MEDIA_URL = 'http://ufs-armstrong.com/armstrong-v3/res/up';
//        $this->MEDIA_DIR = '../armstrong-v3/res/up';

        //$this->MEDIA_URL = 'http://armstrong-ami/res/up';
        //$this->MEDIA_DIR = '../armstrong-ami/res/up';

        // Media view URL
        $this->twig->addGlobal('MEDIA_URL', $this->MEDIA_URL);
        $folder = '';
        if (in_array($this->router->fetch_class(), $this->MEDIA_FOLDER)) {
            $folder = $this->router->fetch_class() . '/';
        }
        $this->twig->addGlobal('MEDIA_FOLDER', $folder);
    }

    private function restrict()
    {
        if (in_array(uri_string(), $this->exception_uri) == false) {
            if (!$this->session->userdata('user_data')) {
                if ($this->input->is_ajax_request()) {
                    // return $this->json(array(
                    //     '_session_expried' => '1'
                    // ));die;
                    echo json_encode(array('_session_expried' => '1'));
                    die;
                } else {
                    redirect('ami/dashboard/login');
                }

                die;
            }
        }
    }

    protected function _getPendingCustomer()
    {
        $pendingCustomers = 0;
        $pendingDistributors = 0;
        $pendingWholesalers = 0;

        if ($this->hasPermission('manage_all', 'salespersons')) {
            //cus pen
           if (in_array($this->country_code, array('tw', 'my', 'sa'))) {
                $pendingCustomers = $this->db->from('customers')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('((approved = -2 AND active = 1) OR approved IN (0,2))' => null))
                    ->count_all_results();
            } else {
                $pendingCustomers = $this->db->from('customers')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('approved IN (0,2)' => null))
                    ->count_all_results();
            }
            //dis pen
            if (in_array($this->country_code, array('hk', 'tw'))) {
                $pendingDistributors = $this->db->from('distributors')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('approved IN (0,2)' => null))
                    ->count_all_results();
            } else {
                $pendingDistributors = $this->db->from('distributors')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('approved' => 0))
                    ->count_all_results();
            }
            //who pen
            if (in_array($this->country_code, array('hk', 'tw'))) {
                $pendingWholesalers = $this->db->from('wholesalers')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('approved IN (0,2)' => null))
                    ->count_all_results();
            } else {
                $pendingWholesalers = $this->db->from('wholesalers')
                    ->where(array(
                        'is_draft' => 0,
                        'country_id' => $this->country_id
                    ))
                    ->where(array('approved' => 0))
                    ->count_all_results();
            }
        } else if ($this->hasPermission('manage_staff', 'salespersons')) {
            $user = $this->session->userdata('user_data');
            $type = $this->db->select(array('type', 'sub_type'))
                ->from('salespersons')
                ->where('armstrong_2_salespersons_id', $user['id'])
                ->get()
                ->row();

            if (isset($type->sub_type) && $type->sub_type == 'asm') {
                $salespersons = $this->db->select('armstrong_2_salespersons_id')
                    ->from('salespersons')
                    ->where('salespersons_manager_id', $user['id'])
                    ->get()
                    ->result_array();
            } else {
                $salespersons = [
                    ['armstrong_2_salespersons_id' => $user['id']],
                ];
            }
            $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');

            if ($salespersons) {
                //cus pen
                if (in_array($this->country_code, array('hk'))) {
                    $pendingCustomers = $this->db->from('customers')
                        ->where(array(
                            'is_draft' => 0,
                            'country_id' => $this->country_id
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0, 2))
                        ->count_all_results();
                } elseif (in_array($this->country_code, array('tw', 'my', 'sa'))) {
                    $pendingCustomers = $this->db->from('customers')
                        ->where(array(
                            'is_draft' => 0,
                            'country_id' => $this->country_id
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where(array('((approved = -2 AND active = 1) OR approved IN (0,2))' => null))
                        ->count_all_results();
                } else {
                    $pendingCustomers = $this->db->from('customers')
                        ->where(array(
                            // 'approved' => 0,
                            'is_draft' => 0,
                            'country_id' => $this->country_id,
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0, 2))
                        ->count_all_results();
                }
                //dis pen
                if (in_array($this->country_code, array('hk', 'tw'))) {
                    $pendingDistributors = $this->db->from('distributors')
                        ->where(array(
                            'is_draft' => 0,
                            'country_id' => $this->country_id
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0, 2))
                        ->count_all_results();
                } else {
                    $pendingDistributors = $this->db->from('distributors')
                        ->where(array(
                            // 'approved' => 0,
                            'is_draft' => 0,
                            'country_id' => $this->country_id,
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0))
                        ->count_all_results();
                }
                //who pen
                if (in_array($this->country_code, array('hk', 'tw'))) {
                    $pendingWholesalers = $this->db->from('wholesalers')
                        ->where(array(
                            'is_draft' => 0,
                            'country_id' => $this->country_id
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0, 2))
                        ->count_all_results();
                }
                else {
                    $pendingWholesalers = $this->db->from('wholesalers')
                        ->where(array(
                            // 'approved' => 0,
                            'is_draft' => 0,
                            'country_id' => $this->country_id,
                        ))
                        ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
                        ->where_in('approved', array(0))
                        ->count_all_results();
                }
                // lq();
            }
        }
        $this->twig->addGlobal('pendingCustomers', $pendingCustomers);
        $this->twig->addGlobal('pendingDistributors', $pendingDistributors);
        $this->twig->addGlobal('pendingWholesalers', $pendingWholesalers);
        $this->twig->addGlobal('totalpendingCustomers', $pendingCustomers + $pendingDistributors + $pendingWholesalers);
    }

    protected function _getPendingSsd()
    {
        $pendingSsd = 0;

        if (in_array($this->country_code, array('tw')) && $this->roleName == 'Sales Manager') {
            // if ($this->hasPermission('edit', 'ssd'))
            // {
            $user = $this->session->userdata('user_data');

            $salespersons = $this->db->select('armstrong_2_salespersons_id')
                ->from('salespersons')
                ->where('salespersons_manager_id', $user['id'])
                ->get()
                ->result_array();

            $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
            $salespersons = array_keys($salespersons);
            $salespersons[] = $user['id'];
            if ($salespersons) {
                $pendingSsd = $this->db->from('ssd')
                    ->join('salespersons', "ssd.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id")
                    ->join('customers', "ssd.armstrong_2_customers_id = customers.armstrong_2_customers_id", 'left')
                    ->where(array(
                        'ssd.status' => 0,
                        'ssd.is_draft' => 0,
                        'ssd.country_id' => $this->country_id,
                        'ssd.ssd_group' => 1
                    ))
                    ->where_in('ssd.armstrong_2_salespersons_id', $salespersons)
                    ->count_all_results();
            }
            // }
        }

        $this->twig->addGlobal('pendingSsd', $pendingSsd);
    }

    /**
     * Render template with Twig
     *
     * @param string $template
     * @param array $data
     */
    public function render($template, array $data = array())
    {
        // For CI only
        if (!$data) {
            $data = $this->data;
        }

        return $this->twig->display($template, $data);
    }

    public function renderView($template, array $data = array())
    {
        // For CI only
        if (!$data) {
            $data = $this->data;
        }

        return $this->twig->render($template, $data);
    }

    /**
     * HTTP request validation
     *
     * @param string $method HTTP request need to be validated
     * @param bool $showError If true, render a error page
     * @param string $message Error message
     *
     * @return bool|error page
     */
    public function is($method, $showError = true, $message = null)
    {
        if ($this->method == strtoupper($method)) {
            return true;
        }

        if ($showError) {
            if (!$message) {
                $message = "This request can be done via {$method} only!";
            }

            return show_error($message);
        }

        return false;
    }

    /**
     * Check if id is existed or redirect to somewhere
     *
     * @param int $id
     * @param string $redirect If id is not exist, redirect to this location
     *
     * @return bool|redirect
     */
    protected function _assertId($id, $redirect = '/')
    {
        if (!empty($id)) {
            return true;
        }

        return redirect($redirect);
    }

    /**
     * Get limit and offset for SQL query based on page query string
     *
     * @param int $perPage
     * @param string $param
     *
     * @return array
     */
    public function getPageLimit($perPage = null, $param = 'p')
    {
        if (!$perPage) {
            $perPage = $this->data['per_page'];
        }
        $perPage = intval($perPage);
        $page = intval($this->input->get($param) ? $this->input->get($param) : 1);
        if ($page < 1) {
            $page = 1;
        }

        $offset = intval(($page - 1) * $perPage);

        return array($perPage, $offset);
    }

    /**
     * Response JSON format
     *
     * @param mixed $data
     *
     * @return json
     */
    public function json($data)
    {
        return $this->output->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    /**
     * Assert country id to make sure user form country A can not
     * view content of B country
     *
     * @param mixed $id if this is array, catch country_id element
     * @param int $countryId if this is not set, get this from session
     * @param mixed $callback
     *
     * @return error|callback
     */

    public function assertCountry($id, $countryId = null, $callback = null)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        if (is_array($id)) {
            if (isset($id['country_id'])) {
                $id = $id['country_id'];
            }
        }
        $id = intval($id);

        if (!$countryId && isset($this->country_id)) {
            $countryId = $this->country_id;
        }

        $countryId = intval($countryId);

        if ($id === $countryId) {
            return true;
        }

        if ($callback) {
            return call_user_func($callback, array($id, $countryId));
        }

        return $this->noPermission();
    }

    /**
     * No permision error
     */
    public function noPermission()
    {
        // return show_error('You don\'t have permission to view this page or perform this action', 401);
        return $this->render('ami/components/no_permission');
    }

    /**
     *
     */
    public function hasPermission($type, $permission, $user = null)
    {
        if (!$user) {
            $user = $this->session->userdata('user_data');
        }

        $roleId = isset($user['roles_id']) ? $user['roles_id'] : null;

        if ($roleId == Authentication_m::ADMIN_ROLES_ID) {
            return true;
        }
        if($roleId){
            $userRole = $this->roles_m->get($roleId);
            $roleId = $this->roles_m->getRoleIdByRolename(null, $userRole['name']);
        }

        $countryId = $this->country_id;

        if (!$roleId
            || !$countryId
            || !in_array($type, array('view', 'add', 'edit', 'delete', 'manage_all', 'manage_staff'))
        ) {
            return false;
        }

        if (!self::$role) {
            if(is_array($roleId)){
                $roleId = implode(',', $roleId);
            }
            self::$role = $role = $this->roles_m->get_by(array(
                "id IN ($roleId)" => null,
                "(country_id = $countryId OR global = 1)" => null
            ), true);

        } else {
            $role = self::$role;
        }

        if (isset($role['restrictions'])) {
            $permissions = $this->roles_m->parseRestrictions($role['restrictions']);
        }

        if (!isset($permissions[$type])) {
            return false;
        }

        if (is_numeric($permission)) {
            return in_array(intval($permission), $permissions[$type]);
        }

        if (!self::$modules) {
            $modules = $this->menu_m->get();

            self::$modules = $modules = array_rewrite($modules, 'name');
        } else {
            $modules = self::$modules;
        }

        $permissionId = !empty($modules[$permission]['id'])
            ? intval($modules[$permission]['id'])
            : 0;
//        if($type == 'edit'){
//            dd(in_array($permissionId, $permissions[$type]));
//        }
        return in_array($permissionId, $permissions[$type]);
    }

    /**
     * Check if user loggin is Sales Leader
     */
    public function isSalesLeader($id = null)
    {
        if ($id) {
            $this->load->model('salespersons_m');
            return $this->salespersons_m->get($id);
        } else {
            $user = $this->session->userdata('user_data');
            $userID = isset($user['id']) ? $user['id'] : null;
            $this->load->model('salespersons_m');
            $userData = $this->salespersons_m->get($userID);
            $userType = isset($userData['type']) ? $userData['type'] : null;

            if ($userType == 'SL') {
                return true;
            }
        }

        return false;
    }

    public function isSuperAdmin($id = null)
    {
        if ($id) {
            $this->load->model('admins_m');
            return $this->admins_m->get($id);
        } else {
            $user = $this->session->userdata('user_data');
            $roleId = isset($user['roles_id']) ? $user['roles_id'] : null;

            if ($roleId == Authentication_m::ADMIN_ROLES_ID) {
                return true;
            }
        }

        return false;
    }

    public function excel($excel, $filename, $fileExt = 'xlsx', $append = true)
    {
        $file = $append ? $filename . get_date() : $filename;
        $file .= '.' . $fileExt;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $file . '"');
        header('Cache-Control: max-age=0');

//         $format = 'Excel2007';

//         if (strtolower($fileExt) == 'xls')
//         {
//             $format = 'Excel5';
//         }

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function makeSheet(array $data, array $fields = array())
    {
        // $this->excel->setActiveSheetIndex(0);

        $PHPExcel = new PHPExcel;

        $PHPExcel->setActiveSheetIndex(0);

        if (!$data) {
            return $PHPExcel;
        }

        $keys = $labels = array();

        if ($fields) {
            foreach ($fields as $field) {
                if (is_array($field)) {
                    $keys[] = $field[0];
                    $labels[] = $field[1];
                } else {
                    $keys[] = $field;
                    $labels[] = ucfirst(str_replace('_', ' ', $field));
                }
            }
        } else {
            $_data = reset($data);
            $keys = array_keys($_data);

            foreach ($keys as $key) {
                $labels[] = ucfirst(str_replace('_', ' ', $key));
            }
        }

        $col = 0;

        foreach ($labels as $label) {
            $PHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $label);
            $col++;
        }

        $row = 2;
        foreach ($data as $value) {
            $col = 0;

            foreach ($value as $key => $val) {
                if (in_array($key, $keys)) {
                    $PHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $val);
                    $col++;
                }
            }

            $row++;
        }

        return $PHPExcel;
    }

    public function makeWorkSheet(PHPExcel_Worksheet &$objWorksheet, array $data, array $fields = array())
    {
        $keys = $labels = array();

        if ($fields) {
            foreach ($fields as $field) {
                if (is_array($field)) {
                    $keys[] = $field[0];
                    $labels[] = $field[1];
                } else {
                    $keys[] = $field;
                    $labels[] = ucfirst(str_replace('_', ' ', $field));
                }
            }
        } else {
            $_data = reset($data);
            $keys = array_keys($_data);

            foreach ($keys as $key) {
                $labels[] = ucfirst(str_replace('_', ' ', $key));
            }
        }

        $col = 0;

        foreach ($labels as $label) {
            $objWorksheet->setCellValueByColumnAndRow($col, 1, $label);
            $col++;
        }

        $row = 2;
        foreach ($data as $value) {
            $col = 0;

            foreach ($value as $key => $val) {
                if (in_array($key, $keys)) {
                    $objWorksheet->setCellValueByColumnAndRow($col, $row, $val);
                    $col++;
                }
            }

            $row++;
        }
    }

    public function getInput(array $params, $type = 'post')
    {
        $type = strtolower($type);

        $data = array();

        foreach ($params as $param) {
            $data[$param] = $this->input->$type($param);
        }

        return $data;
    }

    public function filter_dummy_data($data)
    {
        $filter_dump_datas = array(
            'test', 'duyen', 'dummy'
        );
        foreach ($data as $key => $val) {
            foreach ($filter_dump_datas as $filter_dump_data) {
                if (preg_match("/" . $filter_dump_data . "/i", (is_array($val)) ? $val['first_name'] : $val->first_name)) {
                    unset($data[$key]);
                }
                if (preg_match("/" . $filter_dump_data . "/i", (is_array($val)) ? $val['last_name'] : $val->last_name)) {
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }

    public function getMenuData()
    {
        $hiddenMenu = $this->db->from('ami_modules')->where(array('status' => 0))->get()->result_array();
        $this->twig->addGlobal('hiddenMenus', $hiddenMenu);
    }

    public function convertToArray($object)
    {
        if (!is_array($object)) {
            $object = (array)$object;
        }
        return $object;
    }

    function getCustomerApprovalConfig(){
        $approve_customers = json_decode($this->country['approve_customers']);
        return $approve_customers;
    }
    function getAutoSending(){
        $auto_sending = json_decode($this->country['auto_sending']);
        return $auto_sending;
    }
    // Check armstrong_2_customers is PULL OR PUSH
    public function _getCustomerTable($armstrong_2_customers_id)
    {
        $table = array(
            'table' => 'customers',
            'id_key' => 'armstrong_2_customers_id',
            'name_key' => 'armstrong_2_customers_name'
        );
        if (preg_match("/whs/i", $armstrong_2_customers_id)) {
            $table = array(
                'table' => 'wholesalers',
                'id_key' => 'armstrong_2_wholesalers_id',
                'name_key' => 'name'
            );
        } elseif (preg_match("/dis/i", $armstrong_2_customers_id)) {
            $table = array(
                'table' => 'distributors',
                'id_key' => 'armstrong_2_distributors_id',
                'name_key' => 'name'
            );
        }

        return $table;
    }

    function dateRangeOverlaps($a_start, $a_end, $b_start, $b_end)
    {
        if ($a_start <= $b_start && $b_start < $a_end) return true; // b starts in a
        if ($a_start <= $b_end && $b_end <= $a_end) return true; // b ends in a
        if ($b_start < $a_start && $a_end < $b_end) return true; // a in b

        return false;
    }

    function multipleDateRangeOverlaps($dates)
    {
        if (count($dates) % 2 !== 0) {
            return false;
        }
        for ($i = 0; $i < count($dates) - 2; $i += 2) {
            for ($j = $i + 2; $j < count($dates); $j += 2) {
                if (
                $this->dateRangeOverlaps(
                    $dates[$i], $dates[$i + 1],
                    $dates[$j], $dates[$j + 1]
                )
                ) return true;
            }
        }
        return false;
    }

    function getCountriesManagement($salesperson_id = null)
    {
        $country_management = '';
        if($this->isSuperAdmin()){
            $country_management = $this->country_id;
        }else{
            $salesperson_id = $salesperson_id ? $salesperson_id : $this->userdata['id'];
            $salesperson = $this->db->from('salespersons')
                ->where(array(
                    'armstrong_2_salespersons_id' => $salesperson_id
                ))
                ->get()->row();
            if($salesperson){
                $country_management = $salesperson->country_management ? $salesperson->country_management : $this->country_id;
            }
        }

        return $country_management;
    }

    function getListSalespersonsConditions()
    {
        $this->load->model('salespersons_m');
        $this->load->model('roles_m');
        $conditions = array("country_id" => $this->country_id, 'is_draft' => 0, 'test_acc' => 0);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename(null, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename(null, 'Salesperson');
        $user = $this->userdata;
        // check type role of user
        if (in_array($user['roles_id'], $roles_salesmanager)) {
            $list_sales = $this->salespersons_m->getListSales($user['id']);
            $list_sales = implode("','", $list_sales);
            $conditions = array("country_id IN ({$this->country_id})" => null, 'is_draft' => 0, 'test_acc' => 0, "armstrong_2_salespersons_id IN ('{$list_sales}')" => null);
        } else if (in_array($user['roles_id'], $roles_salesperson)) {
            $conditions = array("country_id IN ({$this->country_id})" => null, 'is_draft' => 0, 'test_acc' => 0, 'armstrong_2_salespersons_id' => $user['id']);
        }

        return $conditions;
    }

    public function customers_action_v2($data_old, $data, $type = 'customers')
    {
        $approval_config = $this->getCustomerApprovalConfig();
        $auto_sending = $this->getAutoSending();
        
        if($data_old){
            if($data_old['approved'] == 0 && $approval_config->add == 0 && $auto_sending->add == 1) //neu sua cus pending when edit
            {
                return true;
            }
            if($data_old['approved'] != 0 && $approval_config->edit == 0 && $auto_sending->edit == 1) //neu sua cus pending when edit
            {
                return true;
            }
        }
        else
        {
            if(($approval_config->add == 0 && $auto_sending->add == 1) || ($approval_config->add == 1 && $auto_sending->add == 1) ){ //add moi
                return true;
            }
        }
        $status = 0;
        if(isset($data_old['approved']) && $data_old['approved'] == $data['approved']) //ko co thay doi gi
        {
            return true;
        }
        if (in_array($this->country_id, array(6))) {
            if (in_array($data['approved'], array(1, -1))) {
                if ($data_old['approved'] == 1) {
                    if ($data['approved'] == 1) {
                        $status = 1;
                    } else {
                        $status = 3;
                    }
                } else {
                    if ($data['approved'] == 1) {
                        $status = 2;
                    } else {
                        $status = 4;
                    }
                }
            }
        } else {
            if (in_array($data['approved'], array(1, -1))) {
                if ($data['approved'] == -1) {
                    if(isset($data_old['approved']) && $data_old['approved'] == 0) //chon reject
                    {
                        $status = 5;
                    }
                    else
                    {
                        $status = 7;
                    }
                } elseif ($data['approved'] == 1) {
                    $status = 6;
                }
            }
        }
        $sale = $this->salespersons_m->get_by(array('armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id']));
        $info = array(
            'armstrong_2_customers_id' => $data['armstrong_2_'.$type.'_id'],
            'country_code' => strtoupper($this->country_code),
            'salespersons_email' => count($sale) > 0 ? $sale['0']['email'] : 'conghc.dev@gmail.com'
        );
        if($type != 'customers')
        {
            $info['armstrong_2_customers_name'] = $data['name'];
        }
        else
        {
            $info['armstrong_2_customers_name'] = $data['armstrong_2_'.$type.'_name'];
        }
        if($status)
        {
            $result = $this->mail_when_approved($info, $status, $auto_sending);
        }
    }
    public function mail_when_approved($info, $status ,$auto_sending = null)
    {
        // TODO status :
        // TODO 1: approved new, 2: approved exist
        // TODO 3: reject new, 4: reject exist
        // TODO 5: reject for all country but TW, 6: approved for all country but TW
        $html = '';
        $subject = '';
        $list_cc = '';
        if ($status == 1 || $status == 2) {
            $html = '以下客戶已經被您的主管核准,<br />';
            $html .= '1. Name - ' . $info['armstrong_2_customers_name'] . '<br />';
            $html .= '2. ID - ' . $info['armstrong_2_customers_id'] . '<br />';
            $html .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態<br />';
            $html .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除<br />';
            $html .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態<br />';
            $html .= '3.在Customers功能中的該客戶狀態(Active),會從紅色變為橘色,就能操作其他相關功能(打單,route plan等)<br /><br />';

            $subject = '新客戶已核准';
            if ($status == 2) $subject = '舊客戶已核准';
        } elseif ($status == 3) {
            $html = '以下客戶已經被您的主管退件,<br />';
            $html .= '1. Name - ' . $info['armstrong_2_customers_name'] . '<br />';
            $html .= '2. ID - ' . $info['armstrong_2_customers_id'] . '<br />';
            $html .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態<br />';
            $html .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除<br />';
            $html .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態<br />';
            $html .= '3.在Customers功能中,該客戶就不會出現在客戶清單<br /><br />';

            $subject = '新客戶已退件';
        } elseif ($status == 4) {
            $html = '以下客戶已經被您的主管退件,<br />';
            $html .= '1. Name - ' . $info['armstrong_2_customers_name'] . '<br />';
            $html .= '2. ID - ' . $info['armstrong_2_customers_id'] . '<br />';
            $html .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態<br />';
            $html .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除<br />';
            $html .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態<br />';
            $html .= '3.在Customers功能中的該客戶狀態(Active),會從紅色變為橘色,但是客戶資料還是原來舊的資料<br /><br />';

            $subject = '舊客戶已退件';
        } else {
            if ($status == 5) {
                $subject = 'Reject New Customer - ' . date('Y-m-d H:i:s');
                $html = 'Your Customer ' . $info['armstrong_2_customers_id'] . ' has been rejected by your Sales Leader.<br/> Please contact your Sales Leader on the reason for rejection.<br/>';
//                $html .= 'These changes have been made to an new customers. Please follow these steps to edit the customer again. <br/><br/>';
//                $html .= '1. Tap on the Sync Icon <br/>';
//                $html .= '2. Tap on the Module Tab <br/>';
//                $html .= '3. Tap on "Customers" Icon <br/>';
            } else if ($status == 6) {
                $subject = 'Approval Customers - ' . date('Y-m-d H:i:s');
                $html = 'Your Customer ' . $info['armstrong_2_customers_id'] . ' has been approved by your Sales Leader.<br/> Please sync your iPad to see the latest changes. These are the steps needed to sync your iPad<br/><br/>';
                $html .= '1. Tap on the Sync Icon <br/>';
                $html .= '2. Tap on the Module Tab <br/>';
                $html .= '3. Tap on "Customers" Icon <br/>';

                $list_cc = ($auto_sending != null && $auto_sending->add_cc != '') ? $auto_sending->add_cc : '';
            } else if ($status == 7) {
                $subject = 'Reject Existing Customer - ' . date('Y-m-d H:i:s');
                $html = 'Your Customer ' . $info['armstrong_2_customers_id'] . ' has been rejected by your Sales Leader.<br/> Please contact your Sales Leader on the reason for rejection.<br/>';
                $html .= 'These changes have been made to an existing customers. Please follow these steps to edit the customer again. <br/><br/>';
                $html .= '1. Tap on the Sync Icon <br/>';
                $html .= '2. Tap on the Module Tab <br/>';
                $html .= '3. Tap on "Customers" Icon <br/>';
            }
        }
        $this->mail->from('No-Reply-Orders@ufs-app.com', "UFS " . $info['country_code']);

        $this->mail->sendMail($info['salespersons_email'], $subject, $html, $list_cc, 'conghc.dev@gmail.com');
    }
    // TODO : Get progress of file import
    public function getprogress(){
        if(file_exists($this->filelog))
        {
            echo file_get_contents($this->filelog);
        }
        else
        {
            echo json_encode(array(
                'progress' => true,
                'hightrow' => 100,
                'rowcurrent' => 0
            ));
        }
    }
    // TODO : Create Month Dropdown Values
    public function month_values()
    {
        $month = array(
            '1' => "January",
            '2' => "February",
            '3' => "March",
            '4' => "April",
            '5' => "May",
            '6' => "June",
            '7' => "July",
            '8' => "August",
            '9' => "September",
            '10' => "October",
            '11' => "November",
            '12' => "December");

        return $month;
    }
    // TODO : Create Year Dropdown Values
    public function year_values()
    {
        $curr_date = strtotime(date('Y-m-d H:i:s'));
        $year = date('Y', $curr_date) - 5;
        for ($i = 0; $i < 10; $i++) {
            $year_list[$year] = $year;
            $year += 1;
        }

        return $year_list;

    }
}