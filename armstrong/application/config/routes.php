<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "ami/dashboard";
$route['404_override'] = '';

$route['permission-groups'] = 'permission_groups';

$route['ami/reports/generate'] = "ami/reports/generate";
$route['ami/reports/:any'] = "ami/reports/index";

$route['ami/reports/penetration_data_report'] = "ami/reports/index";
$route['ami/new_reports/penetration_data_report'] = "ami/demo/index";
$route['ami/reports/grip_grab_report'] = "ami/demo/index";
$route['ami/reports/rich_media'] = "ami/demo/index";
$route['ami/reports/promotions_activity_report'] = "ami/demo/index";
//$route['ami/reports/last_pantry_check_report'] = "ami/demo/index";
$route['ami/reports/view'] = "ami/demo/view";
$route['ami/demo/generate'] = "ami/demo/generate";
$route['ami/demo/:any'] = "ami/demo/index";
$route['ami/example/users/(:num)']='ami/example/users/id/$1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */
