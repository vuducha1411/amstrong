<?php
/**
 * Date: 8/7/14
 * Time: 3:10 PM
 */
$config['site_name'] = 'Unilever AMI';
$config['report_url'] = 'http://reports.ufs-armstrong.com/dev/';
$config['base_api_url'] = 'http://dev.ufs-armstrong.com/armstrong-v3/';
$config['base_service_url'] = 'http://dev.ufs-armstrong.com:8888/UnilevefoodConvert/';
$config['pdf_api'] = 'http://pdfy.net';
$config['media_url'] = 'http://ufs-armstrong.com/armstrong-v3/res/up';
$config['media_dir'] = '../armstrong-v3/res/up';
/**
 * Date: 26/03/15
 * Time: 04:00 PM
 * app_id , rest_key , master_key connect to parse cloud
 */

$config['parse_key'] = array(
    'app_id' => array(
        'pull' => 'vpeiLdvnSyLEZEgxpA6xAWrlBr1hZJnFi2TiOweY',
        'push' => '',
        'leader' => ''
    ),
    'rest_key' => array(
        'pull' => 'J0Oki3eY9qysXzbGQlFRFsxSldOgla5Py6w6ZOq9',
        'push' => '',
        'leader' => ''
    ),
    'master_key' => array(
        'pull' => '0k8XsOH5poJcPsn8TmrJz40Qxlj0HKnao8E6rtjc',
        'push' => '',
        'leader' => ''
    )
);