<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['aliases'] = array(
	'Illuminate\Database\Capsule\Manager' => 'Capsule',
	'Illuminate\Database\Eloquent\Model' => 'Eloquent',
	'Carbon\Carbon' => 'Carbon'
);