    
    <div id="team" >
        <span id="salesteamheader" class="text-left sectionheader"> Sales Structure </span>
    </div>    
    
    <div id="salespersons">
      <?php if($this->session->userdata('infographicViewType') == "country"){ ?>
      <span width="50%"> 
            <span id="salesleader" class="salesteamcount">
                <!--<?php
                if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_sales_leader'); ?>" data-toggle="ajaxModal"> 
                <?php
                    }
                ?>-->
                <span id="slcount" class="font-size-md "> <?php echo $page['slcount'] /*. '/' . $page['totalSl']*/ ?> </span></br>
                <span id="sllabel" class="font-size-md "> Sales Leader </span>

                <!--<?php
                    if ($is_print != true)  
                ?>
                </a> -->
            </span>    
            <!--<span id="leadergraph" class="salesgraph">      
                <?php
                    $slDraw = 0;
                    if($page['totalSl'])
                    {
                        $slDraw = round(($page['slcount']/$page['totalSl'])*10);
                    }
                    for($i=0;$i<10;$i++){
                        if($i<$slDraw)
                            echo  "<img class='leader human' id='leadericon' src='" . site_url('res/img/infographic_new/leader_icon.png') . "' alt='Sales Leader'/>";
                        else
                            echo  "<img class='leader human' id='leadericonempty' src='" . site_url('res/img/infographic_new/leader_empty.png') . "' alt='Sales Leader'/>";
                    }

                ?>
             </span>-->
        </span> 
        <?php } ?>

        <?php if($this->session->userdata('infographicViewType') == "team"){ 
            if ($this->session->userdata('infographicSr') == ""){ ?>
                <span width="50%"> 
                    <span id="salesleaderName" class="salesteamcount">                        
                        <span id="slcount" class="font-size-md "> <?php echo $page['slName'] /*. '/' . $page['totalSl']*/ ?> </span>
                    </span>    
                </span>  
        <?php }else{ ?>
            <span width="50%"> 
                    <span id="salesrepName" class="salesteamcount">
                        <span id="slcount" class="font-size-md "> <?php echo $page['srName'] /*. '/' . $page['totalSl']*/ ?> </span>
                    </span>    
                </span>
        <?php }} ?>

        <?php if($this->session->userdata('infographicSr') == "" || $this->session->userdata('infographicViewType') == "country"){ ?>
            <span width="50%">    
              <?php if($this->session->userdata('infographicViewType') == "country"){ ?>            
                <span id="salesrep">  
              <?php } else { ?>
                <span id="salesrepteam" <?php if($this->session->userdata('infographicViewType') != "country"){ ?> style="margin-left: 1.2%" <?php } ?> >  
              <?php } ?>
                    <!--<?php
                    if ($is_print != true){
                     ?>
                          <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_sales_rep'); ?>" data-toggle="ajaxModal"> 
                    <?php
                        }
                    ?>-->
                    <span id="srcount" class="font-size-md "> <?php echo $page['countSr'] /*. '/' . $page['totalSr']*/?> </span></br>
                    <span id="srlabel" class="font-size-md "> Sales Rep </span>

                    <!--<?php
                        if ($is_print != true)  
                    ?>
                    </a> -->
                </span>    
                <!--<span id="repgraph" class="salesgraph">
                    <?php
                        $slDraw = 0;
                        if($page['totalSl']) {
                            $srDraw = round(($page['srcount'] / $page['totalSr']) * 10);
                        }
                        for($i=0;$i<10;$i++){
                            if($i<$srDraw)
                                echo  "<img class='salesrep human' id='repicon' src='" . site_url('res/img/infographic_new/salesrep.png') . "' alt='Sales Leader'/>";
                            else
                                echo  "<img class='salesrep human' id='repiconemppty' src='" . site_url('res/img/infographic_new/salesrep_empty.png') . "' alt='Sales Leader'/>";
                        }

                    ?>
                </span>-->
             </span> 
         <?php } ?>

         <span width="50%">    
          <?php if($this->session->userdata('infographicViewType') == "country"){ ?>            
            <span id="salesrep">  
          <?php } else { ?>
            <span id="salesrepteam">  
          <?php } ?>
                <!--<?php
                if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_sales_rep'); ?>" data-toggle="ajaxModal"> 
                <?php
                    }
                ?>--> 
                <span id="srcount" class="font-size-md "> <?php echo number_format($page['customerProfiling']['customerCount']) ?> </span></br>
                <span id="srlabel" class="font-size-md "> Operators </span>

                <!--<?php
                    if ($is_print != true)  
                ?>
                </a> --> 
            </span>    
         </span> 

        <?php if($this->session->userdata('infographicViewType') == "country"){ ?>
         <span width="50%">   
          <?php if($this->session->userdata('infographicViewType') == "country"){ ?>            
            <span id="salesrep">  
          <?php } else { ?>
            <span id="salesrepteam">  
          <?php } ?>
                <!--<?php
                if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_sales_rep'); ?>" data-toggle="ajaxModal"> 
                <?php
                    }
                ?>-->
                <span id="srcount" class="font-size-md "> <?php echo number_format($page['wholesalers']) /*. '/' . $page['totalSr']*/?> </span></br>
                <span id="srlabel" class="font-size-md "> Wholesalers </span>
                <!--
                <?php
                    if ($is_print != true)  
                ?>
                </a>--> 
            </span>    
         </span> 

         <span width="50%">      
          <?php if($this->session->userdata('infographicViewType') == "country"){ ?>            
            <span id="salesrep">  
          <?php } else { ?>
            <span id="salesrepteam">  
          <?php } ?>
                <!--<?php
                if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_sales_rep'); ?>" data-toggle="ajaxModal"> 
                <?php
                    }
                ?>--> 
                <span id="srcount" class="font-size-md "> <?php echo $page['distributors'] /*. '/' . $page['totalSr']*/?> </span></br>
                <span id="srlabel" class="font-size-md "> Distributors </span>

                <!--<?php
                    if ($is_print != true)  
                ?>
                </a> --> 
            </span>    
         </span> 
        <?php } ?>
    </div>

    <div id="performance" width="100%">
        <span id="amountlabel" class="font-size-md "> Total Sales Performance </span>
        <span id="salesamount" class="font-size-md ">            
                <?php echo $countryCurrency->currency .' ' . number_format($page['salesamount']) ?> 
        </span>
    </div>    

