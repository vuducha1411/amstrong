<div id="ccheader" class="text-left  sectionheader">	    
	TFO Growth (YTD)
</div>
<div id="sku_exist">
	<?php
        if ($is_print != true){
     ?>
          <a id="totaltfogrowth" href="<?= site_url('/ami/infographic_new/total_tfo_growth'); ?>" data-toggle="ajaxModal"> 
      <?php
        }
      ?>
	<canvas id="existBar"></canvas>
	<?php
	    if ($is_print != true)  
	  ?>
	     </a> 
</div>

<center>
	<span id="prevYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year'] - 1?>-->PY (Full Year)</span>
	<span id="currYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year']?>-->CY</span>
	<text><br /></text>
</center>

<?php
	if ($page['tfogrowth'][0][0]['exi_sku_curr'] < 0)
		$page['tfogrowth'][0][0]['exi_sku_curr'] = 0;

	if ($page['tfogrowth'][0][0]['new_sku_curr'] < 0)
		$page['tfogrowth'][0][0]['new_sku_curr'] = 0;

	if ($page['tfogrowth'][1][0]['exi_sku_prev'] < 0)
		$page['tfogrowth'][1][0]['exi_sku_prev'] = 0;

	if ($page['tfogrowth'][1][0]['new_sku_prev'] < 0)
		$page['tfogrowth'][1][0]['new_sku_prev'] = 0;
?>

<script>
	var ctx = document.getElementById("existBar");
	var existBar = new Chart(ctx, {
    	type: 'horizontalBar',
    	data: {
        	labels: ["Existing SKU", "New SKU"],
        	datasets: [{
        		label: "CY",
	            fill: true,
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            data: [
	            	<?php echo $page['tfogrowth'][0][0]['exi_sku_curr']; ?>, 
	            	<?php echo $page['tfogrowth'][0][0]['new_sku_curr']; ?>
	            ] 
        	}, {
        		label: "PY",
        		fill: true,
	            backgroundColor: 'rgb(255, 139, 133)',
	            borderColor: 'rgb(255, 139, 133)',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            data: [
	            	<?php echo $page['tfogrowth'][1][0]['exi_sku_prev']; ?>, 
	            	<?php echo $page['tfogrowth'][1][0]['new_sku_prev']; ?>
	            ] 
        	}]
    	},
    	options: {
    		events: false,
    		showToolTips: false,
    		responsive: true,
    		maintainAspectRatio: true,
    		labels: {
				display: true,
    			boxWidth: 50,
    			fontSize: 18
    		},
	        legend: { 
	            display: false, 
	            position: 'bottom',
	            labels: {
	            	fontSize: 18,
	            	fontFamily: 'avenirMed',
	            	fontColor: '#327782'
	            }
	        },
        	scales: {
        		xAxes: [{
        			beginAtZero: true,
	        		display: false,
	        		gridLines: {
	        			display: false
	        		}
        		}],
        		yAxes: [{
        			/*categorySpacing: 1.5,*/
        			beginAtZero: true,
        			barThickness: 50,
	        		ticks : {
	        			fontFamily: 'avenirMed',
	                    fontSize: 16,
	                    fontColor: '#327782'
	        		},
	        		gridLines: {
	        			display: false
	        		}
        		}]
        	},
        	animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;

		          	ctx.textAlign = 'left';
		          	ctx.font = Chart.helpers.fontString(16, 'normal', 'avenir');
		          	ctx.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i].toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","); //model.label
		              		ctx.fillText("<?php echo $countryCurrency->currency?> "+label, left + 15, model.y + 8);
		            	}
		          	});               
		        }
		    }
    	}
    });
</script>