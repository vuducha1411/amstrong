<div id="gripheader" class="text-left  sectionheader"> Grip </div>
<br />
<div>
    <canvas id="gChart" width="500" height="160"></canvas>
</div>

<div id="grip-legend">
        <!--<div id="totalcust"></div><text class="yrCurr">Total Customers</text>-->
        <div id="newgrip"></div><text class="yrCurr">Total Grip</text>
        <div id="lostgrip"></div><text class="yrCurr">New Grip</text>
        <div id="totalgrip"></div><text class="yrCurr">Lost Grip</text>
</div>

<div id="griptable">
    <div class="gripheaderrow">
        <div class="gripheadercell titlecell2  font-size-sm">
          &nbsp; 
        </div>
        <?php 
            for($i=11;$i>=0;$i--){ //generate total grip row
        ?>
            <div class="gripheadercell font-size-sm ">
                 <?php echo $page['grip_monthName'][$i]; ?>
            </div>
        <?php } ?>
        
        
    </div>
    <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2; width: 4%" >
        <div class="titlecell_gg  font-size-sm" style="background: rgb(74, 212, 225)">
            Total Customers
        </div>
         <?php 
            for($i=11;$i>=0;$i--){ //generate total grip row
        ?>
           <div class="datacell2  font-size-sm">
                <?php echo number_format($page['active'][$i][0]['total_active']) ?>
            </div>
        <?php } ?>
        
       
    </div>
    <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
        <div class="titlecell_gg  font-size-sm" style="background: rgb(74, 212, 225)">
            Total Grip
        </div>
        
        <?php 
            for($i=11;$i>=0;$i--){ //generate total grip row
        ?>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grip'][$i][0]['total_grip'])?>
            </div>
        <?php } ?>
    </div>
    <div class="avgcallrow datarow">
        <div class="titlecell_gg  font-size-sm">
            New Grip
        </div>
        <?php 
            for($i=11;$i>=0;$i--){ //generate total new grip row
        ?>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grip'][$i][0]['total_new_grip'])?>
            </div>
        <?php } ?>
    </div>
     <div class="frequencyrow datarow">
        <div class="titlecell_gg  font-size-sm">
            Lost Grip
        </div>
        <?php 
            for($i=11;$i>=0;$i--){ //generate total new grip row
        ?>
        <div class="datacell2  font-size-sm">
          <?php echo number_format(round($page['grip'][$i][0]['total_lost_grip'],2))?>
        </div>
       
        <?php } ?>
    </div>
</div>
<a id="lost_grip" href="/ami/infographic_new/lost_grip_breakdown?month=0" data-toggle="ajaxModal"  style="visibility: hidden"> </a>

<script>
    var month = "<?php echo $page['month']; ?>";
    var year = "<?php echo $page['year']; ?>";

    var gchart = document.getElementById("gChart");
    var gripChart = new Chart(gchart, {
    type: 'bar',
    stacked: true,
    data: {
      labels: [
            "<?php echo $page['grip_monthName'][11]; ?>",
            "<?php echo $page['grip_monthName'][10]; ?>",
            "<?php echo $page['grip_monthName'][9]; ?>",
            "<?php echo $page['grip_monthName'][8]; ?>",
            "<?php echo $page['grip_monthName'][7]; ?>",
            "<?php echo $page['grip_monthName'][6]; ?>",
            "<?php echo $page['grip_monthName'][5]; ?>",
            "<?php echo $page['grip_monthName'][4]; ?>",
            "<?php echo $page['grip_monthName'][3]; ?>",
            "<?php echo $page['grip_monthName'][2]; ?>",
            "<?php echo $page['grip_monthName'][1]; ?>",
            "<?php echo $page['grip_monthName'][0]; ?>"
        ],
        datasets: 
        [ /* bars*/
            {
                type: 'line', //average grab
                lineTension: 0,
                fill: false,
                borderColor: 'rgb(15, 177, 238)',
                borderWidth: 2,
                pointBackgroundColor: '#FFF',
                pointBorderColor: 'rgb(15, 177, 238)',
                // pointBorderWidth: 2,
                pointRadius: 5,
                data: [
                    <?php echo ($page['gripActive'][11][0]['activeGrip']+$page['grip'][11][0]['total_new_grip']+$page['grip'][11][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][10][0]['activeGrip']+$page['grip'][10][0]['total_new_grip']+$page['grip'][10][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][9][0]['activeGrip']+$page['grip'][9][0]['total_new_grip']+$page['grip'][9][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][8][0]['activeGrip']+$page['grip'][8][0]['total_new_grip']+$page['grip'][8][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][7][0]['activeGrip']+$page['grip'][7][0]['total_new_grip']+$page['grip'][7][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][6][0]['activeGrip']+$page['grip'][6][0]['total_new_grip']+$page['grip'][6][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][5][0]['activeGrip']+$page['grip'][5][0]['total_new_grip']+$page['grip'][5][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][4][0]['activeGrip']+$page['grip'][4][0]['total_new_grip']+$page['grip'][4][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][3][0]['activeGrip']+$page['grip'][3][0]['total_new_grip']+$page['grip'][3][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][2][0]['activeGrip']+$page['grip'][2][0]['total_new_grip']+$page['grip'][2][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][1][0]['activeGrip']+$page['grip'][1][0]['total_new_grip']+$page['grip'][1][0]['total_lost_grip'])?>,
                    <?php echo ($page['gripActive'][0][0]['activeGrip']+$page['grip'][0][0]['total_new_grip']+$page['grip'][0][0]['total_lost_grip'])?>
                ]
            },
            { //lostgrip
                backgroundColor: ['#4BD5E2', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', '#4BD5E2'],
                label: 'Total Grip',
                data: [
                    <?php echo $page['gripActive'][11][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][10][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][9][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][8][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][7][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][6][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][5][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][4][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][3][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][2][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][1][0]['activeGrip']?>,
                    <?php echo $page['gripActive'][0][0]['activeGrip']?>
                ]
            },
            { //lostgrip
                backgroundColor: 'rgb(255, 139, 133)',
                label: 'New Grip',
                data: [
                    <?php echo $page['grip'][11][0]['total_new_grip']?>,
                    <?php echo $page['grip'][10][0]['total_new_grip']?>,
                    <?php echo $page['grip'][9][0]['total_new_grip']?>,
                    <?php echo $page['grip'][8][0]['total_new_grip']?>,
                    <?php echo $page['grip'][7][0]['total_new_grip']?>,
                    <?php echo $page['grip'][6][0]['total_new_grip']?>,
                    <?php echo $page['grip'][5][0]['total_new_grip']?>,
                    <?php echo $page['grip'][4][0]['total_new_grip']?>,
                    <?php echo $page['grip'][3][0]['total_new_grip']?>,
                    <?php echo $page['grip'][2][0]['total_new_grip']?>,
                    <?php echo $page['grip'][1][0]['total_new_grip']?>,
                    <?php echo $page['grip'][0][0]['total_new_grip']?>
                ]
            },
            { //new grip
                backgroundColor: 'rgb(44, 160, 204)',
                label: 'Lost Grip',
                data: [
                    <?php echo $page['grip'][11][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][10][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][9][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][8][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][7][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][6][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][5][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][4][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][3][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][2][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][1][0]['total_lost_grip']?>,
                    <?php echo $page['grip'][0][0]['total_lost_grip']?>
                ]
            }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: { 
            display: false
        },
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    stepSize: <?php echo $page['max_grab_bar'] == 0 ? 20 : ceil(($page['max_grip']+500)/7)?>               
                }
            }],
            xAxes: [{
                categoryPercentage: 0.5,
                stacked: true,     
                gridLines: {
                    display: false
                }
            }]
        },
        onClick: graphClickEvent
    }
});

function graphClickEvent(event, array){
    //console.log(event);
    //console.log(array);
    if(array[0]){
        //foo.bar;
    }
}

document.getElementById("gChart").onclick = function(evt){
        //alert("hi");
        var activePoints = gripChart.getElementAtEvent(evt);
        console.log(activePoints);
        var firstPoint = activePoints[0];
        var label = gripChart.data.labels[firstPoint._index];
        var value = gripChart.data.datasets[firstPoint._datasetIndex].label[firstPoint._index];
        var clicked = firstPoint._datasetIndex;
        //if (firstPoint !== undefined){
            console.log(firstPoint._datasetIndex);
            console.log(label);
            //if (label == "OTHER"){
            //    $('#modelWindowGrip').modal('show');    
            //    document.getElementById('classId').value = label;
            //    console.log(gripChart.data.datasets[firstPoint._datasetIndex].label);
                //alert(document.getElementById('classId').value);
            //}
        //}
        
        switch (clicked){
            case 1:
                console.log("total grip was clicked");
                $('#lost_grip')[0].href="/ami/infographic_new/total_grip_breakdown?monthSelected="+label+"&grip=1";
                $('#lost_grip')[0].click();   
                break;
            case 2:
                console.log("new grip was clicked");
                $('#lost_grip')[0].href="/ami/infographic_new/new_grip_breakdown?monthSelected="+label;
                $('#lost_grip')[0].click();  
                break;
            case 3:
                console.log("lost grip was clicked");
                $('#lost_grip')[0].href="/ami/infographic_new/lost_grip_breakdown?monthSelected="+label;
                $('#lost_grip')[0].click();   
                break;
        }


        //if (clicked == 3){
       //     console.log("lost grip was clicked");
        //    $('#lost_grip')[0].href="/ami/infographic_new/lost_grip_breakdown?monthSelected="+label;
        //    $('#lost_grip')[0].click();    
        //} else {
        //    console.log("lost grip was NOT clicked");
        //}
    };

$('#closemodal').click(function() {
    $('#gripModal').modal('hide');
});


</script>

<div class="modal2 fade" id="gripModal" role="dialog" style="overflow:auto; min-height: 30%">
    <div class="modal-body" style="background-color: #fff;">
        <div id="header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="sectionheader">Lost Grip</p>
        </div>
        <div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px; font-family: 'avenir'">

        </div>
    </div>
</div>