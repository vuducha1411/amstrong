<?php
    $percentage = array (0,0,0,0,0,0,0,0,0,0,0,0);
    $monthReport = array (0,0,0,0,0,0,0,0,0,0,0,0);
?>


<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Top SKU (<?php echo str_replace("/", " ", $_GET['skuName']) ?>)</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="700" height="160"></canvas>
		</div>
        <!--<div id="totalcallvalues">
            <?php 
                for($s=11;$s>=0;$s--){
            ?>  
            <span  class="total-top-sku-month">
                <?php 
                    $totalCustomerOrder = 0; 
                    $totalCustomer = 0; 

                    foreach ($page['total_top_sku'][$s] as $key => $val) { 
                        $totalCustomerOrder=$val['totalCustomerOrder']; 
                        $totalCustomer=$val['totalCustomer']; 
                        $percentage[$s] = round(($totalCustomerOrder/$totalCustomer)*100,0);
                        $monthReport[$s] = $val['monthReport']; 
                    }
                    
                    if (trim($percentage[$s])){ echo $percentage[$s]; } else echo "0";
                ?>%
            </span>
            <?php
                }
            ?>
        </div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][11]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][11]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][10]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][10]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][9]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][9]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][8]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][8]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][7]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][7]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][6]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][6]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][5]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][5]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][4]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][4]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][3]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][3]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][2]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][2]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][1]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][1]['percentage']; ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_top_sku'][0]['monthReport'])->format('M'); ?>","<?php echo $page['total_top_sku'][0]['percentage']; ?>%"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_top_sku'][11]['percentage']; ?>,
                <?php echo $page['total_top_sku'][10]['percentage']; ?>,
                <?php echo $page['total_top_sku'][9]['percentage']; ?>,
                <?php echo $page['total_top_sku'][8]['percentage']; ?>,
                <?php echo $page['total_top_sku'][7]['percentage']; ?>,
                <?php echo $page['total_top_sku'][6]['percentage']; ?>,
                <?php echo $page['total_top_sku'][5]['percentage']; ?>,
                <?php echo $page['total_top_sku'][4]['percentage']; ?>,
                <?php echo $page['total_top_sku'][3]['percentage']; ?>,
                <?php echo $page['total_top_sku'][2]['percentage']; ?>,
                <?php echo $page['total_top_sku'][1]['percentage']; ?>,
                <?php echo $page['total_top_sku'][0]['percentage']; ?>
            ] 
        },
        { //total customers
            type: 'bar',
           	label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_top_sku'][11]['percentage']; ?>,
                <?php echo $page['total_top_sku'][10]['percentage']; ?>,
                <?php echo $page['total_top_sku'][9]['percentage']; ?>,
                <?php echo $page['total_top_sku'][8]['percentage']; ?>,
                <?php echo $page['total_top_sku'][7]['percentage']; ?>,
                <?php echo $page['total_top_sku'][6]['percentage']; ?>,
                <?php echo $page['total_top_sku'][5]['percentage']; ?>,
                <?php echo $page['total_top_sku'][4]['percentage']; ?>,
                <?php echo $page['total_top_sku'][3]['percentage']; ?>,
                <?php echo $page['total_top_sku'][2]['percentage']; ?>,
                <?php echo $page['total_top_sku'][1]['percentage']; ?>,
                <?php echo $page['total_top_sku'][0]['percentage']; ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    stepSize: 20 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>