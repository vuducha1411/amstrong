<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Strike Rate</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="700" height="160"></canvas>
		</div>
        <!--<div id="totalcallvalues">
            <?php 
                for($s=11;$s>=0;$s--){
            ?>  
            <span  class="total-strike-rate">
                <?php echo_isset(number_format($page['total_strike_rate'][$s]['strikeRate']), 0); ?>%
            </span>
            <?php
                }
            ?>
        </div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [

            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][11]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][10]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][9]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][8]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][7]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][6]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][5]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][4]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][3]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][2]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][1]['strikeRate']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_strike_rate'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_strike_rate'][0]['strikeRate']), 0); ?>%"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_strike_rate'][11]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][10]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][9]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][8]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][7]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][6]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][5]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][4]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][3]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][2]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][1]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][0]['strikeRate'] ?>
            ] 
        },
        { //total customers
            type: 'bar',
           	label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_strike_rate'][11]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][10]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][9]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][8]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][7]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][6]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][5]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][4]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][3]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][2]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][1]['strikeRate'] ?>,
                <?php echo $page['total_strike_rate'][0]['strikeRate'] ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    stepSize: 20 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>