<div id="gripheader" class="text-left  sectionheader"> Grab </div>

<div id="gripchart">
    <canvas id="grabChart" width="500" height="160"></canvas>
</div>

<div id="grab-legend">
        <div id="newgrip"></div><text class="yrCurr">Total Grab</text>
        <div id="lostgrip"></div><text class="yrCurr">New Grab</text>
        <div id="totalgrip"></div><text class="yrCurr">Lost Grab</text>
        <div id="averagegrab"></div><text class="yrCurr">Average Grab/Grip</text>
</div>


<div id="grabtable">
        <div class="callheaderrow">
            <div class="grabheadercell titlecell2  font-size-sm">
              &nbsp; 
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][11]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][10]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][9]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][8]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][7]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][6]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][5]; ?>
            </div>
            <div class="grabheadercell font-size-sm ">
              <?php echo $page['grip_monthName'][4]; ?>
            </div>
             <div class="grabheadercell  font-size-sm">
              <?php echo $page['grip_monthName'][3]; ?>
            </div>
             <div class="grabheadercell  font-size-sm">
              <?php echo $page['grip_monthName'][2]; ?>
            </div>
           <div class="grabheadercell  font-size-sm">
              <?php echo $page['grip_monthName'][1]; ?>
            </div>
            <div class="grabheadercell  font-size-sm">
              <?php echo $page['grip_monthName'][0]; ?>
            </div>
           
        </div>
        <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
            <div class="titlecell_gg  font-size-sm" style="background: rgb(74, 212, 225)">
                Total Grab
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][11][0]['total_grab'])?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][10][0]['total_grab'])?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][9][0]['total_grab'])?>
            </div>
           <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][8][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][7][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][6][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][5][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][4][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][3][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][2][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][1][0]['total_grab'])?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][0][0]['total_grab'])?>
            </div>
        </div>
        <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
            <div class="titlecell_gg  font-size-sm" style="background: rgb(74, 212, 225)">
                Average Grab/Grip
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][11][0]['ave_grab'],1) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][10][0]['ave_grab'],1) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format($page['grab'][9][0]['ave_grab'], 1) ?>
            </div>
           <div class="datacell2 font-size-sm">
                  <?php echo number_format($page['grab'][8][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                  <?php echo number_format($page['grab'][7][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][6][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][5][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][4][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][3][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][2][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][1][0]['ave_grab'], 1) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format($page['grab'][0][0]['ave_grab'], 1) ?>
            </div>
        </div>
        <div class="avgcallrow datarow">
            <div class="titlecell_gg  font-size-sm">
              New Grab
            </div>
             <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][11][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][10][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][9][0]['new_grab_target'], 1)) ?>
            </div>
           <div class="datacell2 font-size-sm">
                  <?php echo number_format(round($page['grab'][8][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                  <?php echo number_format(round($page['grab'][7][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][6][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][5][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][4][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][3][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][2][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][1][0]['new_grab_target'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][0][0]['new_grab_target'], 1)) ?>
            </div>
        </div>
         <div class="frequencyrow datarow">
            <div class="titlecell_gg  font-size-sm">
                Lost Grab
            </div>
             <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][11][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][10][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2  font-size-sm">
                <?php echo number_format(round($page['grab'][9][0]['lost_grab'], 1)) ?>
            </div>
           <div class="datacell2 font-size-sm">
                  <?php echo number_format(round($page['grab'][8][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                  <?php echo number_format(round($page['grab'][7][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][6][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][5][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][4][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][3][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][2][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][1][0]['lost_grab'], 1)) ?>
            </div>
            <div class="datacell2 font-size-sm">
                <?php echo number_format(round($page['grab'][0][0]['lost_grab'], 1)) ?>
            </div>
        </div>
    </div>

<a id="lost_grab" href="/ami/infographic_new/lost_grip_breakdown?month=0" data-toggle="ajaxModal"  style="visibility: hidden"> </a>

<script>
    var month = "<?php echo $page['month']; ?>";
    var year = "<?php echo $page['year']; ?>";

    var gchart = document.getElementById("grabChart");
    var grbChart = new Chart(gchart, {
    type: 'bar',
    stacked: true,
    data: {
      labels: [
            "<?php echo $page['grip_monthName'][11]; ?>",
            "<?php echo $page['grip_monthName'][10]; ?>",
            "<?php echo $page['grip_monthName'][9]; ?>",
            "<?php echo $page['grip_monthName'][8]; ?>",
            "<?php echo $page['grip_monthName'][7]; ?>",
            "<?php echo $page['grip_monthName'][6]; ?>",
            "<?php echo $page['grip_monthName'][5]; ?>",
            "<?php echo $page['grip_monthName'][4]; ?>",
            "<?php echo $page['grip_monthName'][3]; ?>",
            "<?php echo $page['grip_monthName'][2]; ?>",
            "<?php echo $page['grip_monthName'][1]; ?>",
            "<?php echo $page['grip_monthName'][0]; ?>"
        ],
        datasets: [ /* bars*/
            {
                type: 'line', //average grab
                label: 'Average Grab/Grip',
                lineTension: 0,
                fill: false,
                yAxisID: "new_lost",
                borderColor: 'rgb(15, 177, 238)',
                borderWidth: 2,
                pointBackgroundColor: '#FFF',
                pointBorderColor: 'rgb(15, 177, 238)',
                // pointBorderWidth: 2,
                pointRadius: 5,
                data: [
                    <?php echo number_format($page['grab'][11][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][10][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][9][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][8][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][7][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][6][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][5][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][4][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][3][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][2][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][1][0]['ave_grab'],2)?>,
                    <?php echo number_format($page['grab'][0][0]['ave_grab'],2)?>
                ]
            },
            { //lostgrip
                backgroundColor: ['#4BD5E2', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', '#4BD5E2'],
                yAxisID: 'totals',
                label: 'Total Grab',
                data: [
                    <?php echo $page['grab'][11][0]['total_grab']?>,
                    <?php echo $page['grab'][10][0]['total_grab']?>,
                    <?php echo $page['grab'][9][0]['total_grab']?>,
                    <?php echo $page['grab'][8][0]['total_grab']?>,
                    <?php echo $page['grab'][7][0]['total_grab']?>,
                    <?php echo $page['grab'][6][0]['total_grab']?>,
                    <?php echo $page['grab'][5][0]['total_grab']?>,
                    <?php echo $page['grab'][4][0]['total_grab']?>,
                    <?php echo $page['grab'][3][0]['total_grab']?>,
                    <?php echo $page['grab'][2][0]['total_grab']?>,
                    <?php echo $page['grab'][1][0]['total_grab']?>,
                    <?php echo $page['grab'][0][0]['total_grab']?>
                ]
            },
            /*{ //new grip/
                backgroundColor: 'transparent',
                data: [
                    <?php echo ($page['grab'][11][0]['activeGrip'] + $page['grip'][11][0]['total_lost_grip']) - $page['grip'][11][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][10][0]['activeGrip'] + $page['grip'][10][0]['total_lost_grip']) - $page['grip'][10][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][9][0]['activeGrip'] + $page['grip'][9][0]['total_lost_grip']) - $page['grip'][9][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][8][0]['activeGrip'] + $page['grip'][8][0]['total_lost_grip']) - $page['grip'][8][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][7][0]['activeGrip'] + $page['grip'][7][0]['total_lost_grip']) - $page['grip'][7][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][6][0]['activeGrip'] + $page['grip'][6][0]['total_lost_grip']) - $page['grip'][6][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][5][0]['activeGrip'] + $page['grip'][5][0]['total_lost_grip']) - $page['grip'][5][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][4][0]['activeGrip'] + $page['grip'][4][0]['total_lost_grip']) - $page['grip'][4][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][3][0]['activeGrip'] + $page['grip'][3][0]['total_lost_grip']) - $page['grip'][3][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][2][0]['activeGrip'] + $page['grip'][2][0]['total_lost_grip']) - $page['grip'][2][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][1][0]['activeGrip'] + $page['grip'][1][0]['total_lost_grip']) - $page['grip'][1][0]['total_new_grip'] ?>,
                    <?php echo ($page['grab'][0][0]['activeGrip'] + $page['grip'][0][0]['total_lost_grip']) - $page['grip'][0][0]['total_new_grip'] ?>,
                ],
                stack: 1
            },*/
            { //lostgrip
                backgroundColor: 'rgb(255, 139, 133)',
                yAxisID: 'totals',
                label: 'New Grab',
                data: [
                    <?php echo str_replace(",", "", $page['grab'][11][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][11][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][10][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][10][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][9][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][9][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][8][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][8][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][7][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][7][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][6][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][6][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][5][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][5][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][4][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][4][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][3][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][3][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][2][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][2][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][1][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][1][0]['new_grab_target'],2))?>,
                    <?php echo str_replace(",", "", $page['grab'][0][0]['new_grab_target'] == "" ? 0 : number_format($page['grab'][0][0]['new_grab_target'],2))?>
                ]
            },/*
            {    
                backgroundColor: 'transparent',
                data: [
                    <?php echo ($page['grab'][11][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][10][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][9][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][8][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][7][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][6][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][5][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][4][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][3][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][2][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][1][0]['activeGrip']) ?>,
                    <?php echo ($page['grab'][0][0]['activeGrip']) ?>
                ],     
                stack: 2
            },*/
            { //new grip
                backgroundColor: 'rgb(44, 160, 204)',
                yAxisID: 'totals',
                label: 'Lost Grab',
                data: [
                    <?php echo $page['grab'][11][0]['lost_grab']?>,
                    <?php echo $page['grab'][10][0]['lost_grab']?>,
                    <?php echo $page['grab'][9][0]['lost_grab']?>,
                    <?php echo $page['grab'][8][0]['lost_grab']?>,
                    <?php echo $page['grab'][7][0]['lost_grab']?>,
                    <?php echo $page['grab'][6][0]['lost_grab']?>,
                    <?php echo $page['grab'][5][0]['lost_grab']?>,
                    <?php echo $page['grab'][4][0]['lost_grab']?>,
                    <?php echo $page['grab'][3][0]['lost_grab']?>,
                    <?php echo $page['grab'][2][0]['lost_grab']?>,
                    <?php echo $page['grab'][1][0]['lost_grab']?>,
                    <?php echo $page['grab'][0][0]['lost_grab']?>
                ]
            }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: { 
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    display: true
                },
                position: "left",
                "id": "totals",
                stacked: true,
                ticks: {
                    stepSize: <?php echo $page['max_grab_bar'] == 0 ? 20 : ceil($page['max_grab_bar']/7)?>
                }
            },{
                beginAtZero:true,
                position: "right",
                "id": "new_lost",
                ticks: {
                    min: 0,
                    //max: <?php echo $page['max_grab_line'] == 0 ? 20 : round($page['max_grab_line'])?>, // 50,
                    stepSize: <?php echo $page['max_grab_line'] == 0 ? 20 : round($page['max_grab_line']/4)?>, // 50,
                },
                gridLines: {
                    display: false
                }
            }],
            xAxes: [{
                categoryPercentage: 0.5,
                stacked: true,     
                gridLines: {
                    display: false
                }
            }]
        },
         /*tooltips: {
              callbacks: {
                title: function(tooltipItem, data) {
                    return '';
                },
                label: function(tooltipItem, data) {
                    
                    if(tooltipItem.index==data.datasets[tooltipItem.datasetIndex].data.length-1){
                       //throw '';
                    
                        //Chart.defaults.global.tooltips.enabled = false;

                        return '';
                    }else{
                         return data.datasets[tooltipItem.datasetIndex].label + ": " +  data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    }
                  }
               }
              
            },*/
       /* animation: {
        duration: 0,
        onComplete: function () {
            // render the value of the chart above the bar
           
            var ctx = this.chart.ctx;
            ctx.font = Chart.helpers.fontString(11, 'bold', 'avenir');
            ctx.fillStyle = '#327782';      
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom'; 
            var dsetcount = 0;
            var y = 0;
            var color = ["rgb(15, 177, 238)","rgb(0,0,0)","#4BD5E2","rgb(255, 139, 133)"];
            this.data.datasets.forEach(function (dataset){
                dataset.points.foreach(function (points) {
                   // ctx.fillText(points.value, points.x, points.y - 10);
                   alert(points.value);
                });
                 //write labels if looping through first dataset !!HACK!!
                 if(y<4){
                     ctx.fillStyle = color[y];  
                     for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                         /* ctx.fillText(dataset.data[i] + '%', model.x, model.y + (dataset.data[i]*2.5) );
                         if(i == dataset.data.length -1){
                            ctx.fillText(dataset.data[i], model.x - 1, model.y - 3);
                          }

                      }
                 }   
                 y++; 
             });
           
        }}*/
    }
    });


document.getElementById("grabChart").onclick = function(evt){
        //alert("hi");
        var activePoints = grbChart.getElementAtEvent(evt);
        console.log(activePoints);
        var firstPoint = activePoints[0];
        var label = gripChart.data.labels[firstPoint._index];
        var value = gripChart.data.datasets[firstPoint._datasetIndex].label[firstPoint._index];
        var clicked = firstPoint._datasetIndex;
        //if (firstPoint !== undefined){
            console.log(firstPoint._datasetIndex);
            console.log(label);
            //if (label == "OTHER"){
            //    $('#modelWindowGrip').modal('show');    
            //    document.getElementById('classId').value = label;
            //    console.log(gripChart.data.datasets[firstPoint._datasetIndex].label);
                //alert(document.getElementById('classId').value);
            //}
        //}
        
        switch (clicked){
            case 1:
                console.log("total grab was clicked");
                $('#lost_grab')[0].href="/ami/infographic_new/total_grip_breakdown?monthSelected="+label+"&grip=0";
                $('#lost_grab')[0].click();   
                break;
            case 2:
                console.log("new grab was clicked");
                $('#lost_grab')[0].href="/ami/infographic_new/new_grab_breakdown?monthSelected="+label;
                $('#lost_grab')[0].click();   
                break;
            case 3:
                console.log("lost grab was clicked");
                $('#lost_grab')[0].href="/ami/infographic_new/lost_grab_breakdown?monthSelected="+label;
                $('#lost_grab')[0].click();   
                break;
        }


        //if (clicked == 3){
       //     console.log("lost grip was clicked");
        //    $('#lost_grip')[0].href="/ami/infographic_new/lost_grip_breakdown?monthSelected="+label;
        //    $('#lost_grip')[0].click();    
        //} else {
        //    console.log("lost grip was NOT clicked");
        //}
    };

$('#closemodal').click(function() {
    $('#gripModal').modal('hide');
});
</script>