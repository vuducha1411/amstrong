<?php
    $totalCalls = 0;
    $otm = $_GET['otm'];
    switch ($otm){
        case "A": 
            for ($c=0;$c<12;$c++){
                $page['total_calls'][$c]['totalCalls'] = $page['total_otm_calls'][$c]['otmACalls'];
                $page['max_call_count'] = $page['max_otmAcall_count'];
            }
            break;
        case "B": 
            for ($c=0;$c<12;$c++){
                $page['total_calls'][$c]['totalCalls'] = $page['total_otm_calls'][$c]['otmBCalls'];
                $page['max_call_count'] = $page['max_otmBcall_count'];
            }
            break;
        case "C": 
            for ($c=0;$c<12;$c++){
                $page['total_calls'][$c]['totalCalls'] = $page['total_otm_calls'][$c]['otmCCalls'];
                $page['max_call_count'] = $page['max_otmCcall_count'];
            }
            break;
        case "D": 
            for ($c=0;$c<12;$c++){
                $page['total_calls'][$c]['totalCalls'] = $page['total_otm_calls'][$c]['otmDCalls'];
                $page['max_call_count'] = $page['max_otmDcall_count'];
            }
            break;
        case "U": 
            for ($c=0;$c<12;$c++){
                $page['total_calls'][$c]['totalCalls'] = $page['total_otm_calls'][$c]['otmUCalls'];
                $page['max_call_count'] = $page['max_otmUcall_count'];
            }
            break;
        case "AC":
            for ($c=0;$c<12;$c++){
                $totalCalls = $page['total_otm_calls'][$c]['otmACalls'] + $page['total_otm_calls'][$c]['otmBCalls'] + $page['total_otm_calls'][$c]['otmCCalls'] + $page['total_otm_calls'][$c]['otmDCalls'] + $page['total_otm_calls'][$c]['otmUCalls'];
                $page['total_calls'][$c]['totalCalls'] = round(($totalCalls/$page['total_otm_calls'][$c]['totalActualWorking']), 1);
                $maxAveCall[] = $page['total_calls'][$c]['totalCalls'];
            }
            $page['max_call_count'] = max($maxAveCall);
            break;
        case "TS":
            for ($c=0;$c<12;$c++){
                $totalCalls = $page['total_otm_calls'][$c]['otmACallMins'] + $page['total_otm_calls'][$c]['otmBCallMins'] + $page['total_otm_calls'][$c]['otmCCallMins'] + $page['total_otm_calls'][$c]['otmDCallMins'] + $page['total_otm_calls'][$c]['otmUCallMins'];
                if ($page['total_otm_calls'][$c]['totalSalespersons']>0 && ($page['total_otm_calls'][$c]['workingDay']>0)){
                    $page['total_calls'][$c]['totalCalls'] = round((($totalCalls/($page['total_otm_calls'][$c]['totalSalespersons']*8*60*$page['total_otm_calls'][$c]['workingDay']))*100), 1);
                    //echo $page['total_calls'][$c]['totalCalls'] . "/ ";
                }
                else
                    $page['total_calls'][$c]['totalCalls'] = 0;
                $maxAveCall[] = $page['total_calls'][$c]['totalCalls'];
            }
            $page['max_call_count'] = max($maxAveCall);
            break;
    }
?>


<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total OTM <?php echo str_replace("/", " ", $_GET['otm']) ?> Calls</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="500" height="90"></canvas>
		</div>
		<!--div id="totalcallvalues">
			<?php 
				for($s=11;$s>=0;$s--){
			?>		
			<span  class="total-call-month"> <?php echo_isset(number_format($page['total_calls'][$s]['totalCalls']), 0); ?> </span>

			<?php
				}
			?>
		</div-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");
    
    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [

            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][11]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][10]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][9]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][8]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][7]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][6]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][5]['totalCalls']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][4]['totalCalls']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][3]['totalCalls']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][2]['totalCalls']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][1]['totalCalls']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_otm_calls'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][0]['totalCalls']), 0); ?>"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_calls'][11]['totalCalls'] ?>,
                <?php echo $page['total_calls'][10]['totalCalls'] ?>,
                <?php echo $page['total_calls'][9]['totalCalls'] ?>,
                <?php echo $page['total_calls'][8]['totalCalls'] ?>,
                <?php echo $page['total_calls'][7]['totalCalls'] ?>,
                <?php echo $page['total_calls'][6]['totalCalls'] ?>,
                <?php echo $page['total_calls'][5]['totalCalls'] ?>,
                <?php echo $page['total_calls'][4]['totalCalls'] ?>,
                <?php echo $page['total_calls'][3]['totalCalls'] ?>,
                <?php echo $page['total_calls'][2]['totalCalls'] ?>,
                <?php echo $page['total_calls'][1]['totalCalls'] ?>,
                <?php echo $page['total_calls'][0]['totalCalls'] ?>
            ] 
        },
        { //total customers
            type: 'bar',
           	label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_calls'][11]['totalCalls'] ?>,
                <?php echo $page['total_calls'][10]['totalCalls'] ?>,
                <?php echo $page['total_calls'][9]['totalCalls'] ?>,
                <?php echo $page['total_calls'][8]['totalCalls'] ?>,
                <?php echo $page['total_calls'][7]['totalCalls'] ?>,
                <?php echo $page['total_calls'][6]['totalCalls'] ?>,
                <?php echo $page['total_calls'][5]['totalCalls'] ?>,
                <?php echo $page['total_calls'][4]['totalCalls'] ?>,
                <?php echo $page['total_calls'][3]['totalCalls'] ?>,
                <?php echo $page['total_calls'][2]['totalCalls'] ?>,
                <?php echo $page['total_calls'][1]['totalCalls'] ?>,
                <?php echo $page['total_calls'][0]['totalCalls'] ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    stepSize: <?php echo $page['max_call_count'] == 0 ? 20 : round($page['max_call_count']/5)?>, // 50,
                    //stepSize: 2000 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>