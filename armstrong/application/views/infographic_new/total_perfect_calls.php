<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Perfect Calls</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="700" height="160"></canvas>
		</div>
        <!--<div id="totalcallvalues">
            <?php 
                for($s=11;$s>=0;$s--){
            ?>  
            <span  class="total-perfect-call-month">
                <?php echo_isset(number_format($page['total_perfect_calls'][$s]['perfectCalls']), 0); ?>%
            </span>
            <?php
                }
            ?>
        </div>
		<div id="totalcallvalues" class="total-perfectcall-div">
			<?php 
				for($s=11;$s>=0;$s--){
			?>	
			<span  class="total-perfect-call-values">
                <?php header('Content-type: text/plain'); echo $page['total_perfect_calls'][$s]['perfectCallInt'] . "\r\n/" . $page['total_perfect_calls'][$s]['callCount']?>
            </span>
            <?php
				}
			?>
		</div>
        -->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [

            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][11]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][10]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][9]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][8]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][7]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][6]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][5]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][4]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][3]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][2]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][1]['perfectCalls']), 0); ?>%"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_perfect_calls'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_perfect_calls'][0]['perfectCalls']), 0); ?>%"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_perfect_calls'][11]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][10]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][9]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][8]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][7]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][6]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][5]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][4]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][3]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][2]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][1]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][0]['perfectCalls'] ?>
            ] 
        },
        { //total customers
            type: 'bar',
           	label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_perfect_calls'][11]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][10]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][9]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][8]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][7]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][6]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][5]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][4]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][3]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][2]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][1]['perfectCalls'] ?>,
                <?php echo $page['total_perfect_calls'][0]['perfectCalls'] ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    stepSize: 20 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>