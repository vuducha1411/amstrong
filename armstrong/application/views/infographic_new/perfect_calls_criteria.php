<div id = "ccheader"  class="text-left  sectionheader">6 Perfect Call Criterias</div> 
<div id="perfectcallgraph">
	<span id="perfectcallperc">
	    <?php 
	    if ($page['perfectCall']['callCount'] > 0) {
	        $perfectCallPercentage = intval(((($page['perfectCall']['perfectCalls'] == "" ? 0.00 : number_format($page['perfectCall']['perfectCalls'], 0,'',''))/($page['perfectCall']['callCount'] == "" ? 1.00 : number_format($page['perfectCall']['callCount'], 0,'.','')))*100));
	    } else {
	        $perfectCallPercentage = 0;
	    }	
	    ?>
        <div id="pcpercent" class="font-size-max">
        	 <?php
	            if ($is_print != true){
	         ?>
	              <a id="totalcallcount" href="<?= site_url('/ami/infographic_new/total_perfect_calls'); ?>" data-toggle="ajaxModal"> 
	          <?php
	            }
	          ?>
            
            <?php echo_isset(number_format($perfectCallPercentage,0), 0); ?>%

            <?php
	            if ($is_print != true)  
	          ?>
	             </a> 
        </div>						
		<div id="pclabel" class="font-size-md">Perfect Calls</div>
		<div id="ccleft-total" class="font-size-md"><?php echo_isset(number_format($page['perfectCall']['perfectCalls'])) ?>/<?php echo_isset(number_format($page['perfectCall']['callCount'])) ?></div>
	</span> 
	<span id="perfcallgraph">
		<canvas id="callCriteria" width="680" height="300" style="padding-left: 15px"></canvas>
	</span>	
</div>

<script>
	var ctx = document.getElementById("callCriteria");
    
	var callCriteria = new Chart(ctx, {
    	type: 'horizontalBar',
    	data: {
        	labels: ["Sampling", "Pantry Check", "TFO", "     Personal Objective", "Competitor Info", "Rich Media Demo"],
	        datasets: [{
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            borderWidth: 1,
	            data: [	            		
	            		<?php echo ($page['perfectCall']['samplingCount'] == "" ? 0.00 : number_format($page['perfectCall']['samplingCount'], 0,'','')); ?>,
	            		<?php echo ($page['perfectCall']['pantryCheckCount'] == "" ? 0.00 : number_format($page['perfectCall']['pantryCheckCount'], 0,'','')); ?>,
	            		<?php echo ($page['perfectCall']['tfoCount'] == "" ? 0.00 : number_format($page['perfectCall']['tfoCount'], 0,'','')); ?>,
	            		<?php echo ($page['perfectCall']['personalObjectiveCount'] == "" ? 0.00 : number_format($page['perfectCall']['personalObjectiveCount'], 0,'','')); ?>,
	            		<?php echo ($page['perfectCall']['competitorInfoCount'] == "" ? 0.00 : number_format($page['perfectCall']['competitorInfoCount'], 0,'','')); ?>,
	            		<?php echo ($page['perfectCall']['richMediaCount'] == "" ? 0.00 : number_format($page['perfectCall']['richMediaCount'], 0,'','')); ?>
	            	  ] 
	        }]
	    },
    	options: {
    		events: false,
    		showToolTips: false,
    		responsive: true,
    		maintainAspectRatio: true,
	        legend: { 
	            display: false
	        },
	        scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                    	min: 0,
                    	max: <?php echo $page['callcoverage']['totalCalls'] == "" ? 0.00 : number_format($page['perfectCall']['callCount'], 0,'','') ?>
                    }
                }],
                yAxes: [{
	        		barThickness: 40,
	        		gridLines: {
	        			display: false
	        		},
	        		ticks : {
	        			fontFamily: 'avenir',
	                    fontSize: 18,
	                    fontColor: '#327782'
	        		},
                    labels: {
                        position: "left"
                    }
	        	}]
	        },
	        animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;
		          	ctx.font = Chart.helpers.fontString(16, 'normal', 'avenirBold');
		          	ctx.textAlign = 'left';
		          	ctx.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {		          		
	              		<?php 
							if ($page['perfectCall']['callCount'] > 0) {
								$totalCalls = number_format($page['perfectCall']['callCount'], 0,'','');
							} else {
								$totalCalls = 0;
							}					
						 ?>


		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		var data2 = <?php echo_isset(number_format(($totalCalls == 0 ? 1 : $totalCalls), 0,'','')) ?>;
		              		
		              		ctx.fillText(((label/data2)*100).toFixed(0)+"%", left + 15, model.y + 9);
		              		<?php 
								if ($totalCalls == 0) {
									$totalCalls = 1;
								}
							?>
		              		// ctx.fillText("("+((label/<?php echo_isset(number_format($totalCalls, 0,'','')) ?>)*100).toFixed(2)+"%)", left + 85, model.y + 8);
		            	}
		          	});

		          	var ctx2 = this.chart.ctx;
		          	ctx2.font = Chart.helpers.fontString(16, 'normal', 'avenir');
		          	ctx2.textAlign = 'left';
		          	ctx2.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {		          		
	              		<?php 
							if ($page['perfectCall']['callCount'] > 0) {
								$totalCalls = number_format($page['perfectCall']['callCount'], 0,'','');
							} else {
								$totalCalls = 0;
							}					
						 ?>


		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx2.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		var data2 = <?php echo_isset(number_format(($totalCalls == 0 ? 1 : $totalCalls), 0,'','')) ?>;
		              		
		              		ctx2.fillText("            ("+label.toLocaleString()+"/<?php echo_isset(number_format($totalCalls)) ?>"+")", left + 15, model.y + 12);
		              		<?php 
								if ($totalCalls == 0) {
									$totalCalls = 1;
								}
							?>
		              		// ctx.fillText("("+((label/<?php echo_isset(number_format($totalCalls, 0,'','')) ?>)*100).toFixed(2)+"%)", left + 85, model.y + 8);
		            	}
		          	});           
		        }
		    } 	 	
		}
    });
</script>