<div class="modal-header" style="padding-bottom: 60px;">
    <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; height: 130%;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total TFO Growth <?php echo $this->session->userdata('infographicYear'); ?> </p>
	</div>
	<div id="content" class="text-center cf">
        <div>
            <text style="font-size: 18pt; color: #4BD5E2; font-weight: bold;"> &#9675; </text> <text style="font-family: 'avenirMed'; font-size: 9pt; color: #808080">Existing SKU</text> &nbsp;&nbsp;&nbsp;&nbsp; <text style="font-size: 18pt; color: rgb(255, 139, 133); font-weight: bold; "> &#9675; </text> <text style="font-family: 'avenirMed'; font-size: 9pt; color: #808080"> New SKU </text>   
        </div>
        <div id="totalcallshistory">
		    <canvas id="call-hist" width="850" height="180"></canvas>
		</div>
        <!--<div id="totalcallvalues" style="padding-left: 2%;">
            <?php 
                for($s=11;$s>=0;$s--){
            ?>  
            <span  class="total-tfo-growth-existing">
                <?php echo_isset(number_format($page['total_tfo_growth'][$s]['exi_sku_curr']), 0); ?>
            </span>
            <?php
                }
            ?>
        </div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    height: 200,
    data: {
        labels: [

            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][11][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][11][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][10][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][10][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][9][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][9][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][8][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][8][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][7][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][7][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][6][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][6][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][5][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][5][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][4][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][4][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][3][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][3][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][2][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][2][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][1][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][1][0][0]['exi_sku_curr']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_tfo_growth'][0][0][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_tfo_growth'][0][0][0]['exi_sku_curr']), 0); ?>"]
        ],
        datasets: [{ //total customers
            type: 'line',
            label: 'Existing SKU',
            lineTension: 0,
            fill: false,
            borderColor: '#4BD5E2',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#4BD5E2',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_tfo_growth'][11][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][10][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][9][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][8][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][7][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][6][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][5][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][4][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][3][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][2][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][1][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][0][0][0]['exi_sku_curr'] ?>
            ] 
        },
        { //total customers
            showInLegend: false,
            type: 'bar',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_tfo_growth'][11][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][10][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][9][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][8][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][7][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][6][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][5][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][4][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][3][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][2][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][1][0][0]['exi_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][0][0][0]['exi_sku_curr'] ?>
            ] 
        },{ //total customers
            type: 'line',
            label: 'New SKU',
            lineTension: 0,
            fill: false,
            borderColor: 'rgb(255, 139, 133)',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: 'rgb(255, 139, 133)',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_tfo_growth'][11][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][10][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][9][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][8][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][7][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][6][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][5][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][4][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][3][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][2][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][1][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][0][0][0]['new_sku_curr'] ?>
            ] 
        },
        { //total customers
            showInLegend: false,
            type: 'bar',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_tfo_growth'][11][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][10][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][9][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][8][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][7][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][6][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][5][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][4][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][3][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][2][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][1][0][0]['new_sku_curr'] ?>,
                <?php echo $page['total_tfo_growth'][0][0][0]['exi_sku_curr'] ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    //max: <?php echo intval($page['max_tfo_growth']+5000); ?>,
                    stepSize: <?php echo intval(($page['max_tfo_growth']+5000)/5); ?> ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });   
</script>