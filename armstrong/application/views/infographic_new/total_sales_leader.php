<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Sales Leader</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="500" height="90"></canvas>
		</div>
		<!--<div id="totalcallvalues" class="total-sales-div">
			<?php 
				for($s=11;$s>=0;$s--){
			?>	
			<span  class="total-slCount">
                <?php echo_isset(number_format($page['total_sl'][$s]['slCount']), 0); ?>
            </span>
            <?php
				}
			?>
		</div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][11]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][10]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][9]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][8]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][7]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][6]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][5]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][4]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][3]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][2]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][1]['slCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sl'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sl'][0]['slCount']), 0); ?>"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo_isset(number_format($page['total_sl'][11]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][10]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][9]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][8]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][7]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][6]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][5]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][4]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][3]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][2]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][1]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][0]['slCount']), 0); ?>
            ] 
        },
        { //total customers
            type: 'bar',
            label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo_isset(number_format($page['total_sl'][11]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][10]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][9]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][8]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][7]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][6]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][5]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][4]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][3]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][2]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][1]['slCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sl'][0]['slCount']), 0); ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 20,
                    stepSize: 5 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>