<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Lost Grip for <?php echo $_GET['monthSelected'] ?> </p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
        <div id="totalcallshistory">
            <canvas id="call-hist" width="700" height="180"></canvas>
        </div>
	</div>	
</div>

<script>
    var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [
            <?php
                for ($i=0; $i < count($page['lost_grip_per_channel']); $i++) {   
                    if (strlen($page['lost_grip_per_channel'][$i]['countryChannelsName']) > 25){
                        $page['lost_grip_per_channel'][$i]['countryChannelsName'] = substr($page['lost_grip_per_channel'][$i]['countryChannelsName'], 0, 25);
                    }        
                    if ($page['lost_grip_per_channel'][$i]['channel'] == 0){
                        $page['lost_grip_per_channel'][$i]['countryChannelsName'] = "OTHERS";
                    }
                    if ($i == (count($page['lost_grip_per_channel'])-1))            
                        echo "\"".$page['lost_grip_per_channel'][$i]['countryChannelsName'] . "\"";
                    else
                        echo "\"".$page['lost_grip_per_channel'][$i]['countryChannelsName'] . "\",";
                }
            ?>
        ],
        datasets: [{ 
            type: 'bar',
            label: 'total calls',
            lineTension: 0,
            backgroundColor: '#4BD5E2',
            data: [
                <?php
                    for ($i=0; $i < count($page['lost_grip_per_channel']); $i++) {        
                        if ($i == (count($page['lost_grip_per_channel'])-1))            
                            echo "\"".$page['lost_grip_per_channel'][$i]['lostGrip'] . "\"";
                        else
                            echo "\"".$page['lost_grip_per_channel'][$i]['lostGrip'] . "\",\n";
                    }
                ?>
            ] 
        }
    ]},
    options: {
        events: true,
        showToolTips: true,
        legend: { 
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: <?php
                           echo ceil(max($page['lostGrips'])+(round(max($page['lostGrips']))*0.15));
                         ?>,
                    stepSize: 10 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                    beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
            }]
        },
        animation: {
        onComplete: function () {
            // render the value of the chart above the bar
             //alert('aasa');
            var ctx = this.chart.ctx;
            ctx.font = Chart.helpers.fontString(16, 'bold', 'avenir');
            ctx.fillStyle = '#327782';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            var dsetcount = 0;
            this.data.datasets.forEach(function (dataset) {
              for (var i = 0; i < dataset.data.length; i++) {
                  ctx.fillStyle = '#327782';
                  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                  ctx.fillText(dataset.data[i].toLocaleString('en'), model.x, model.y);
              }
            });
        }}
    }
    });    

</script>
