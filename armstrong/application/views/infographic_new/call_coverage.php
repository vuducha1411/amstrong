<?php
  if($page['callcoverage']['otmACount']>0){
    $otmACount = $page['callcoverage']['otmACount'];
  }else{
    $otmACount = 1;
  }

  if($page['callcoverage']['otmBCount']>0){
    $otmBCount = $page['callcoverage']['otmBCount'];
  }else{
    $otmBCount = 1;
  }
  if($page['callcoverage']['otmCCount']>0){  
    $otmCCount = $page['callcoverage']['otmCCount'];
  }else{
    $otmCCount = 1;
  }

  if($page['callcoverage']['otmDCount']>0){
    $otmDCount = $page['callcoverage']['otmDCount'];
  }else{
    $otmDCount = 1;
  }

  if($page['callcoverage']['otmUCount']>0){
    $otmUCount = $page['callcoverage']['otmUCount'];
  }else{
    $otmUCount = 1;
  }
  //calculate otm frequency

  if($page['callcoverage']['otmAVisits']>0){
    //calcuate otm frequency
    $otmAFrequency = round(($page['callcoverage']['otmACalls']/$page['callcoverage']['otmAVisits']),1);
  }else{
     $otmAFrequency = 0;
  }

  if($page['callcoverage']['otmBVisits']>0){
    //calcuate otm frequency
    $otmBFrequency = round(($page['callcoverage']['otmBCalls']/$page['callcoverage']['otmBVisits']),1);
    
  }else{
     $otmBFrequency = 0;
  }

  if($page['callcoverage']['otmCVisits']>0){
    //calcuate otm frequency
    $otmCFrequency = round(($page['callcoverage']['otmCCalls']/$page['callcoverage']['otmCVisits']),1);
    
  }else{
     $otmCFrequency = 0;
  }

  if($page['callcoverage']['otmDVisits']>0){
    //calcuate otm frequency
    $otmDFrequency = round(($page['callcoverage']['otmDCalls']/$page['callcoverage']['otmDVisits']),1);
    
  }else{
     $otmDFrequency = 0;
  }

  if($page['callcoverage']['otmUVisits']>0){
    //calcuate otm frequency
    $otmUFrequency = round(($page['callcoverage']['otmUCalls']/$page['callcoverage']['otmUVisits']),1);
  }else{
     $otmUFrequency = 0;
  }

  //calculate unique visits

  if($page['callcoverage']['otmACount']>0){
    //calcuate otm frequency
    $otmAVisits = round(($page['callcoverage']['otmAVisits']/$page['callcoverage']['otmACount'])*100,0);
  }else{
     $otmAVisits = 0;
  }

  if($page['callcoverage']['otmBCount']>0){
    //calcuate otm frequency
    $otmBVisits = round(($page['callcoverage']['otmBVisits']/$page['callcoverage']['otmBCount'])*100,0);
    
  }else{
     $otmBVisits = 0;
  }

  if($page['callcoverage']['otmCCount']>0){
    //calcuate otm frequency
    $otmCVisits = round(($page['callcoverage']['otmCVisits']/$page['callcoverage']['otmCCount'])*100,0);
    
  }else{
     $otmCVisits = 0;
  }

  if($page['callcoverage']['otmDCount']>0){
    //calcuate otm frequency
    $otmDVisits = round(($page['callcoverage']['otmDCalls']/$page['callcoverage']['otmDCount'])*100,0);
    
  }else{
     $otmDVisits = 0;
  }

  if($page['callcoverage']['otmUCount']>0){
    //calcuate otm frequency
    $otmUVisits = round(($page['callcoverage']['otmUCalls']/$page['callcoverage']['otmUCount'])*100,0);
    
  }else{
     $otmUVisits = 0;
  }

  $visitsA = $page['callcoverage']['otmAVisits'];
  $visitsB = $page['callcoverage']['otmBVisits'];
  $visitsC = $page['callcoverage']['otmCVisits'];
  $visitsD = $page['callcoverage']['otmDVisits'];
  $visitsU = $page['callcoverage']['otmUVisits'];

  if($page['callcoverage']['totalCalls']>0){
    //calculate otm percentage
    if ($page['callcoverage']['otmAVisits'] > $otmACount ){
      $otmAPercentage = ($page['otmVisits']['otmAVisits']/$otmACount )*100;
      $page['callcoverage']['otmAVisits'] = $page['otmVisits']['otmAVisits'];
    }else{
      $otmAPercentage = ($page['callcoverage']['otmAVisits']/$otmACount )*100;
    }
    $otmAPercentage = round($otmAPercentage,1);

    if ($page['callcoverage']['otmBVisits'] > $otmBCount ){
      $otmBPercentage = ($page['otmVisits']['otmBVisits']/$otmBCount )*100;
      $page['callcoverage']['otmBVisits'] = $page['otmVisits']['otmBVisits'];
    }else{
      $otmBPercentage = ($page['callcoverage']['otmBVisits']/$otmBCount )*100;
    }
    $otmBPercentage = round($otmBPercentage,1);

    if ($page['callcoverage']['otmCVisits'] > $otmCCount ){
      $otmCPercentage = ($page['otmVisits']['otmCVisits']/$otmCCount )*100;
      $page['callcoverage']['otmCVisits'] = $page['otmVisits']['otmCVisits'];
    }else{
      $otmCPercentage = ($page['callcoverage']['otmCVisits']/$otmCCount )*100;
    }
    $otmCPercentage = round($otmCPercentage,1);

    if ($page['callcoverage']['otmDVisits'] > $otmDCount ){
      $otmDPercentage = ($page['otmVisits']['otmDVisits']/$otmDCount )*100;
      $page['callcoverage']['otmDVisits'] = $page['otmVisits']['otmDVisits'];
    }else{
      $otmDPercentage = ($page['callcoverage']['otmDVisits']/$otmDCount )*100;
    }
    $otmDPercentage = round($otmDPercentage,1);

    if ($page['callcoverage']['otmUVisits'] > $otmUCount ){
      $otmUPercentage = ($page['otmVisits']['otmUVisits']/$otmUCount )*100;
      $page['callcoverage']['otmUVisits'] = $page['otmVisits']['otmUVisits'];
    }else{
      $otmUPercentage = ($page['callcoverage']['otmUVisits']/$otmUCount )*100;
    }
    $otmUPercentage = round($otmUPercentage,1);

  }else{
      $otmAPercentage = 0;
      $otmBPercentage = 0;
      $otmCPercentage = 0;
      $otmDPercentage = 0;
      $otmUPercentage = 0;
  }

  //otm call counts
  $otmACallCount = $page['callcoverage']['otmACalls'];
  $otmBCallCount = $page['callcoverage']['otmBCalls'];
  $otmCCallCount = $page['callcoverage']['otmCCalls'];
  $otmDCallCount = $page['callcoverage']['otmDCalls'];
  $otmUCallCount = $page['callcoverage']['otmUCalls'];

  $totalCalls = $otmACallCount + $otmBCallCount + $otmCCallCount + $otmDCallCount + $otmUCallCount;

  //otm customer count
  $otmACount =  $page['callcoverage']['otmACount'];
  $otmBCount =  $page['callcoverage']['otmBCount'];
  $otmCCount = $page['callcoverage']['otmCCount'];
  $otmDCount =  $page['callcoverage']['otmDCount'];
  $otmUCount =  $page['callcoverage']['otmUCount'];

  //otm count divisors
  $otmADiv =  $page['callcoverage']['otmACount']>0 ? $page['callcoverage']['otmACount'] : 1;
  $otmBDiv =  $page['callcoverage']['otmBCount']>0 ? $page['callcoverage']['otmBCount'] : 1;
  $otmCDiv = $page['callcoverage']['otmCCount']>0 ? $page['callcoverage']['otmCCount'] : 1;
  $otmDDiv =  $page['callcoverage']['otmDCount']>0 ? $page['callcoverage']['otmDCount'] : 1;
  $otmUDiv =  $page['callcoverage']['otmUCount']>0 ? $page['callcoverage']['otmUCount'] : 1;

 

  //average call

  $otmAAverageCall = round($otmACallCount/$otmADiv,2);
  $otmBAverageCall = round($otmBCallCount/$otmBDiv,2);
  $otmCAverageCall = round($otmCCallCount/$otmCDiv,2);
  $otmDAverageCall = round($otmDCallCount/$otmDDiv,2);
  $otmUAverageCall = round($otmUCallCount/$otmUDiv,2);

  //average call per day
  if($page['callcoverage']['totalActualWorking']>0){
    $averageCallPerDayA = number_format($otmACallCount / ($page['callcoverage']['totalActualWorking']), 1 );
    $averageCallPerDayB = number_format($otmBCallCount / ($page['callcoverage']['totalActualWorking']), 1 );
    $averageCallPerDayC = number_format($otmCCallCount / ($page['callcoverage']['totalActualWorking']), 1 );
    $averageCallPerDayD = number_format($otmDCallCount / ($page['callcoverage']['totalActualWorking']), 1 );
    $averageCallPerDayU = number_format($otmUCallCount / ($page['callcoverage']['totalActualWorking']), 1 );
    $averageCallPerDay = $averageCallPerDayA + $averageCallPerDayB + $averageCallPerDayC + $averageCallPerDayD + $averageCallPerDayU;
  }else{
    $averageCallPerDay = $page['callcoverage']['totalActualWorking'];
  }

$totalCallMins = $page['callcoverage']['otmACallMins'] + $page['callcoverage']['otmBCallMins'] + $page['callcoverage']['otmCCallMins'] + $page['callcoverage']['otmDCallMins'] + $page['callcoverage']['otmUCallMins'];

  //time spent in call
if($page['callcoverage']['workingDay']>0&&($page['callcoverage']['totalSalespersons']>0)){
    $timeInCall = round(($totalCallMins/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);

    $timeInCallA = round(($page['callcoverage']['otmACallMins']/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);
    $timeInCallB = round(($page['callcoverage']['otmBCallMins']/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);
    $timeInCallC = round(($page['callcoverage']['otmCCallMins']/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);
    $timeInCallD = round(($page['callcoverage']['otmDCallMins']/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);
    $timeInCallU = round(($page['callcoverage']['otmUCallMins']/($page['callcoverage']['totalSalespersons']*8*60*$page['callcoverage']['workingDay']))*100,1);
}else{
    $timeInCallA = 0;
    $timeInCallB = 0;
    $timeInCallC = 0;
    $timeInCallD = 0;
    $timeInCallU = 0;
}
 //average actual working day
if($page['callcoverage']['totalSalespersons']>0)
  $avgActualWorkingDay = number_format((($page['avgWorkingHours']['totalWorkingDays'] - $page['holidays']['holidays'] - $page['avgWorkingHours']['leaves']) * $page['workingHours']['numWorkingHours']) / $page['callcoverage']['totalSalespersons'],0);
else
  $avgActualWorkingDay = 0;

//echo "(" . $page['avgWorkingHours']['totalWorkingDays'] . "-" . $page['holidays']['holidays'] . "-" . $page['avgWorkingHours']['leaves'] . ") * 8";

if($totalCalls>0){
  $perfectCallOtmA = round(($page['perfcallotm']['otmAPerfectCalls']/$totalCalls)*100, 1);
  $perfectCallOtmB = round(($page['perfcallotm']['otmBPerfectCalls']/$totalCalls)*100, 1);
  $perfectCallOtmC = round(($page['perfcallotm']['otmCPerfectCalls']/$totalCalls)*100, 1);
  $perfectCallOtmD = round(($page['perfcallotm']['otmDPerfectCalls']/$totalCalls)*100, 1);
  $perfectCallOtmU = round(($page['perfcallotm']['otmUPerfectCalls']/$totalCalls)*100, 1);
  /*if($perfectCallOtmA>0||$perfectCallOtmB>0||$perfectCallOtmC>0||$perfectCallOtmD>0)
    $pctmU = $page['perfcallotm']['totalPerfectCalls'] - ($page['perfcallotm']['otmAPerfectCalls'] + $page['perfcallotm']['otmBPerfectCalls'] + $page['perfcallotm']['otmCPerfectCalls'] + $page['perfcallotm']['otmDPerfectCalls'] );
  else
    $pctmU = 0;
  $perfectCallOtmU = round(($pctmU/$totalCalls)*100, 1);;
  */
 }else{
    $perfectCallOtmA = 0;
    $perfectCallOtmB = 0;
    $perfectCallOtmC = 0;
    $perfectCallOtmD =0;
    $perfectCallOtmU =0;
 }
?>

<div id="callcovheader">
  <span id="callcoverageheader" class="text-left sectionheader"> Call Information </span>
</div>

<div class="callcounts tree text-center">

      <span id="totalcallcount" class="font-size-max text-center"> 
         <?php
            if ($is_print != true){
         ?>
              <a id="totalcallcount" href="<?= site_url('/ami/infographic_new/total_calls'); ?>" data-toggle="ajaxModal"> 
          <?php
            }
          ?>
            <?php echo /*$page['totalCalls']*/ number_format($page['callcoverage']['totalCalls']) ?>

          <?php
            if ($is_print != true)  
          ?>
             </a> 

        
       </span></br>
      <span id="totalcalls" class="font-size-sm text-center"> Total Calls </span>
      <ul>
        <li>
          <span class="otmborder">
            <span id="otmacallcount" class="otmcount font-size-max"> 
              <!--<?php
                  if ($is_print != true){
               ?>
                    <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_otm_calls?otm=A'); ?>" data-toggle="ajaxModal"> 
                <?php
                  }
                ?>  -->
                <?php echo /*$page['callOtmA']*/ number_format($page['callcoverage']['otmACalls']) ?> 
                <!--<?php
                  if ($is_print != true)  
                ?>
                   </a> -->
            </span></br>
            <span id="otmacall" class="otmlabel font-size-sm"> A </span>
          </span>  
        </li>
        <li class="tree-inner">
            <span class="otmborder">
             <span id="otmbcallcount" class="otmcount font-size-max">  
              <!--<?php
                  if ($is_print != true){
               ?>
                    <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_otm_calls?otm=B'); ?>" data-toggle="ajaxModal"> 
                <?php
                  }
                ?>  -->
                <?php echo /*$page['callOtmB']*/ number_format($page['callcoverage']['otmBCalls'])  ?> 
                <!--<?php
                  if ($is_print != true)  
                ?>
                   </a> -->
             </span></br>
             <span id="otmbcall" class="otmlabel font-size-sm"> B </span>
            </span>
        </li>
        <li class="tree-inner">
            <span class="otmborder">
               <span id="otmccallcount" class="otmcount font-size-max"> 
                <!--<?php
                    if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_otm_calls?otm=C'); ?>" data-toggle="ajaxModal"> 
                  <?php
                    }
                  ?>  -->
                  <?php echo /*$page['callOtmC']*/ number_format($page['callcoverage']['otmCCalls'])  ?> 
                  <!--<?php
                    if ($is_print != true)  
                  ?>
                     </a> -->
               </span></br>
              <span id="otmccall" class="otmlabel font-size-sm"> C </span>
            </span>  
        </li>
        <li class="tree-inner">
          <span class="otmborder">
            <span id="otmdcallcount" class="otmcount font-size-max">
                <!--<?php
                    if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_otm_calls?otm=D'); ?>" data-toggle="ajaxModal"> 
                  <?php
                    }
                  ?> -->
                <?php echo /*$page['callOtmD']*/ number_format($page['callcoverage']['otmDCalls'])  ?>
                  <!--<?php
                    if ($is_print != true)  
                  ?>
                     </a> -->
            </span></br>
            <span id="otmdcall" class="otmlabel font-size-sm"> D </span>
          </span>
        </li>
        <li >
            <span class="otmborder">
             <span id="otmucallcount" class="otmcount font-size-max">
                <!--<?php
                    if ($is_print != true){
                 ?>
                      <a id="totalcallcountwhite" href="<?= site_url('/ami/infographic_new/total_otm_calls?otm=U'); ?>" data-toggle="ajaxModal"> 
                  <?php
                    }
                  ?> -->
                <?php echo /*$page['callOtmU']*/ number_format($page['callcoverage']['otmUCalls'])  ?>
                 <!-- <?php
                    if ($is_print != true)  
                  ?>
                     </a> -->
              </span></br>
             <span id="otmucall" class="otmlabel font-size-sm"> UN </span>
            </span> 
        </li>
      </ul>
  
</div>

	<div id="callcoveragechart">
    	<canvas id="callcov" height="120" ></canvas>
	</div>
	<!--  <div id="calldata">
        <div class="datacallrow">

        	<div id="avgcallperday" class="datacallcell">
          
             <span id="avgcallperdayval" class="datacallvals  font-size-lg"> <?php echo number_format($averageCallPerDay, 1) ?> </span> 
             <span class="font-size-sm" >Average Call per day / SR </span> 
          </div>
        	<div id="percenttimecall" class="datacallcell">             
                <span id="percenttimecallval" class="datacallvals  font-size-lg"> <?php echo $timeInCall ?> </span> 
                <span class="test11  font-size-sm">  % of Time Spent with Customers in Call </span>
          </div>
        	<div id="avgworkingday" class="datacallcell">
                <span id="percenttimecallval" class="datacallvals  font-size-lg"> <?php echo $avgActualWorkingDay ?> </span > 
                <span class=" font-size-sm"> Avg Actual Working Day / Month </span>
           </div>	
        </div>
	</div>  
  -->

    <div id="calltable">
        <div class="callheaderrow">
            <div class="callheadercell titlecell1  font-size-sm">
              &nbsp; 
            </div>
            <div class="callheadercell  font-size-sm">
              Totals/Avg
            </div>
            <div class="callheadercell font-size-sm ">
              A
            </div>
             <div class="callheadercell  font-size-sm">
              B
            </div>
             <div class="callheadercell  font-size-sm">
              C
            </div>
           <div class="callheadercell  font-size-sm">
              D
            </div>
            <div class="callheadercell  font-size-sm">
              UN
            </div>
        </div>
        <div class="avgcallrow datarow">
            <div class="titlecell  font-size-sm">
                Total Customers
            </div>
            <div class="datacell font-size-sm">
              <?php
                $totalOtmCount = $otmACount + $otmBCount + $otmCCount + $otmDCount + $otmUCount; 
                echo number_format($totalOtmCount, 0) 
              ?>
            </div>
            <div class="datacell  font-size-sm">
                <?php echo  number_format($otmACount, 0) ?>
            </div>
             <div class="datacell  font-size-sm">
               <?php echo  number_format($otmBCount, 0) ?>
            </div>
             <div class="datacell  font-size-sm">
               <?php echo  number_format($otmCCount, 0) ?>
            </div>
           <div class="datacell font-size-sm">
               <?php echo  number_format($otmDCount, 0) ?>
            </div>
            <div class="datacell font-size-sm">
               <?php echo  number_format($otmUCount, 0) ?>
            </div>
        </div>
         <div class="frequencyrow datarow">
            <div class="titlecell  font-size-sm">
                Visit Frequency
            </div>
            <div class="datacell  font-size-sm" style="background-color: #BCC2C2">
              <?php 
                $totalFrequency = $otmAFrequency + $otmBFrequency + $otmCFrequency + $otmDFrequency + $otmUFrequency;
                //echo round($totalFrequency/5, 1); 
              ?> &nbsp;
            </div>
            <div class="datacell  font-size-sm">
              <?php echo $otmAFrequency ?>
            </div>
             <div class="datacell  font-size-sm">
              <?php echo $otmBFrequency ?>
            </div>
             <div class="datacell  font-size-sm">
                <?php echo $otmCFrequency ?>
            </div>
           <div class="datacell  font-size-sm">
              <?php echo $otmDFrequency ?>
            </div>
            <div class="datacell  font-size-sm">
              <?php echo $otmUFrequency ?>
            </div>
        </div>
        <div class="timerow datarow">
            <div class="titlecell  font-size-sm">
                Duration (minutes)
            </div>

            <?php
              $tempVisitA = $page['callcoverage']['otmAVisits'];
              $tempVisitB = $page['callcoverage']['otmBVisits'];
              $tempVisitC = $page['callcoverage']['otmCVisits'];
              $tempVisitD = $page['callcoverage']['otmDVisits'];
              $tempVisitU = $page['callcoverage']['otmUVisits'];

              $page['callcoverage']['otmAVisits'] = $visitsA;
              $page['callcoverage']['otmBVisits'] = $visitsB;
              $page['callcoverage']['otmCVisits'] = $visitsC;
              $page['callcoverage']['otmDVisits'] = $visitsD;
              $page['callcoverage']['otmUVisits'] = $visitsU;
            ?>

            <div class="datacell  font-size-sm">
               <?php 
                 $totalVisits = $page['callcoverage']['otmAVisits'] + $page['callcoverage']['otmBVisits'] + $page['callcoverage']['otmCVisits'] + $page['callcoverage']['otmDVisits'] + $page['callcoverage']['otmUVisits'];

                 if($totalVisits==0){
                    $totalVisits = 1;
                 }
                 $totalVisitDuration = $totalCallMins/$totalVisits;  
                /*echo sprintf('%02d:%02d', (int) $totalVisitDuration, fmod($totalVisitDuration, 1) * 60);*/
                echo number_format(round($totalVisitDuration,2)) . " (Avg)";                 
               ?>
              
              
            </div>
            <div class="datacell  font-size-sm">
              <?php 
                if($page['callcoverage']['otmAVisits']<=0)
                   $page['callcoverage']['otmAVisits'] = 1;
              //echo date('H:i', strtotime($startMinutes.' minutes'));
             // echo date('H:i', strtotime($page['callcoverage']['otmACallMins'] .' minutes'));
                  $visitDurationA = round($page['callcoverage']['otmACallMins']/$page['callcoverage']['otmAVisits']);
               /*$visitDurationA = $visitDurationA/60;
                echo sprintf('%02d:%02d', (int) $visitDurationA, fmod($visitDurationA, 1) * 60);*/
                echo number_format(round($visitDurationA,2));
               ?>
            </div>
             <div class="datacell  font-size-sm">
               <?php 
               if($page['callcoverage']['otmBVisits']<=0)
                    $page['callcoverage']['otmBVisits'] = 1;
                //echo date('H:i', strtotime($startMinutes.' minutes'));
               //echo $page['callcoverage']['otmBCallMins'];
                 $visitDurationB = round($page['callcoverage']['otmBCallMins']/$page['callcoverage']['otmBVisits']);
                // $visitDurationB = $visitDurationB/60;
                echo number_format(round($visitDurationB,2));
                 //echo date('H:i', strtotime($visitDurationB.' minutes')); 
                // echo $visitDurationB;
               ?>
                
            </div>
             <div class="datacell  font-size-sm">
                <?php 
                if($page['callcoverage']['otmCVisits']<=0)
                   $page['callcoverage']['otmCVisits'] = 1;
              //echo date('H:i', strtotime($startMinutes.' minutes'));
                //echo $page['callcoverage']['otmCCallMins'];
               $visitDurationC = round($page['callcoverage']['otmCCallMins']/$page['callcoverage']['otmCVisits']);
               /*$visitDurationC = $visitDurationC/60;
                echo sprintf('%02d:%02d', (int) $visitDurationC, fmod($visitDurationC, 1) * 60);*/
                echo number_format(round($visitDurationC,2));
               ?>
            </div>
           <div class="datacell  font-size-sm">
               <?php 
                if($page['callcoverage']['otmDVisits']<=0)
                    $page['callcoverage']['otmDVisits'] = 1;
              //echo date('H:i', strtotime($startMinutes.' minutes'));
                $visitDurationD = round($page['callcoverage']['otmDCallMins']/$page['callcoverage']['otmDVisits']);
             /* $visitDurationD = $visitDurationD/60;
                echo sprintf('%02d:%02d', (int) $visitDurationD, fmod($visitDurationD, 1) * 60);*/
                echo number_format(round($visitDurationD,2));
               ?>
              
            </div>
            <div class="datacell  font-size-sm">
               <?php 
                if($page['callcoverage']['otmUVisits']<=0)
                    $page['callcoverage']['otmUVisits'] = 1;
              //echo date('H:i', strtotime($startMinutes.' minutes'));
                $visitDurationU = round($page['callcoverage']['otmUCallMins']/$page['callcoverage']['otmUVisits']);
             /* $visitDurationU = $visitDurationU/60;
                echo sprintf('%02d:%02d', (int) $visitDurationU, fmod($visitDurationU, 1) * 60);*/
                echo number_format(round($visitDurationU,2));
               ?>              
            </div>
        </div>
        
        <div class="perfcallrow datarow">
            <div class="titlecell  font-size-sm">
                Perfect Call
            </div>
             <div class="datacell  font-size-sm">
              <?php 

               $totalPerfectCall = intval(floor($perfectCallOtmA * 2)/2) + number_format($perfectCallOtmB,0) + number_format($perfectCallOtmC,0) + number_format($perfectCallOtmD,0) + number_format($perfectCallOtmU,0);
            
                echo number_format($totalPerfectCall,0) . '%'; 

              ?>
            </div>
            <div class="datacell  font-size-sm">
               <?php echo intval(floor($perfectCallOtmA * 2)/2) . '%' ?>
            </div>
             <div class="datacell  font-size-sm">
               <?php echo number_format($perfectCallOtmB,0) . '%' ?>
            </div>
             <div class="datacell font-size-sm">
              <?php echo number_format($perfectCallOtmC,0) . '%' ?>
            </div>
           <div class="datacell  font-size-sm">
              <?php echo number_format($perfectCallOtmD,0) . '%' ?>
            </div>
            <div class="datacell  font-size-sm">
              <?php echo number_format($perfectCallOtmU,0) . '%' ?>
            </div>
        </div>
        <div class="perfcallrow datarow">
            <div class="titlecell  font-size-sm">
                Average Call per day / SR
            </div>
             <div class="datacell  font-size-sm">
              <?php echo number_format($averageCallPerDay,1) ?>
            </div>
            <div class="datacell  font-size-sm">
               <?php echo number_format($averageCallPerDayA,1) ?>
            </div>
             <div class="datacell  font-size-sm">
               <?php echo number_format($averageCallPerDayB,1) ?>
            </div>
             <div class="datacell font-size-sm">
             <?php echo number_format($averageCallPerDayC,1) ?>
            </div>
           <div class="datacell  font-size-sm">
              <?php echo number_format($averageCallPerDayD,1) ?>
            </div>
            <div class="datacell  font-size-sm">
              <?php echo number_format($averageCallPerDayU,1) ?>
            </div>
            
        </div> 

         <div class="perfcallrow datarow">
            <div class="titlecell  font-size-sm">
               Time Spent With Customers
            </div>
            <div class="datacell  font-size-sm">
              <?php echo number_format(round($timeInCall,2)) ?>%
            </div>
            <div class="datacell  font-size-sm">
               <?php echo number_format(round($timeInCallA,2)) ?>%
            </div>   
             <div class="datacell  font-size-sm">
              <?php echo number_format(round($timeInCallB,2)) ?>%
            </div>
             <div class="datacell font-size-sm">
             <?php echo number_format(round($timeInCallC,2)) ?>%
            </div>
           <div class="datacell  font-size-sm">
             <?php echo number_format(round($timeInCallD,2)) ?>%
            </div>
            <div class="datacell  font-size-sm">
              <?php echo number_format(round($timeInCallU,2)) ?>%
            </div>
             
        </div> 
        
        <div class="perfcallrow datarow">
            <div class="titlecell  font-size-sm">
               Avg Actual Working Hours/Month 
            </div>
            <div class="datacell  font-size-sm">
               <?php echo $avgActualWorkingDay ?>
            </div>
             <div class="datacell  font-size-sm" style="background-color: #BCC2C2">
               <!--<?php echo $avgActualWorkingDay ?>--> &nbsp;
            </div>
             <div class="datacell font-size-sm" style="background-color: #BCC2C2">
              <!--<?php echo $avgActualWorkingDay ?>--> &nbsp;
            </div>
           <div class="datacell  font-size-sm" style="background-color: #BCC2C2">
             <!--<?php echo $avgActualWorkingDay ?>--> &nbsp;
            </div>
            <div class="datacell  font-size-sm" style="background-color: #BCC2C2">
             <!--<?php echo $avgActualWorkingDay ?>--> &nbsp;
            </div>
             <div class="datacell  font-size-sm" style="background-color: #BCC2C2">
             <!--<?php echo $avgActualWorkingDay ?>--> &nbsp;
            </div>
        </div> 
    </div>  

<?php
  $page['callcoverage']['otmAVisits'] = $tempVisitA;
  $page['callcoverage']['otmBVisits'] = $tempVisitB;
  $page['callcoverage']['otmCVisits'] = $tempVisitC;
  $page['callcoverage']['otmDVisits'] = $tempVisitD;
  $page['callcoverage']['otmUVisits'] = $tempVisitU;

  if ($otmAPercentage <= 0)
    $page['callcoverage']['otmAVisits'] = 0;

  if ($otmBPercentage <= 0)
    $page['callcoverage']['otmBVisits'] = 0;

  if ($otmCPercentage <= 0)
    $page['callcoverage']['otmCVisits'] = 0;

  if ($otmDPercentage <= 0)
    $page['callcoverage']['otmDVisits'] = 0;

  if ($otmUPercentage <= 0)
    $page['callcoverage']['otmUVisits'] = 0;
?>  

<script>
	
	var ctx = document.getElementById("callcov");
	
    var myChart = new Chart(ctx, {
    type: 'bar',
     stacked: true,
    data: {

        labels: [
              "(<?php echo $page['callcoverage']['otmAVisits'] ?>/<?php echo $page['callcoverage']['otmACount'] ?>)", 
              "(<?php echo $page['callcoverage']['otmBVisits'] ?>/<?php echo $page['callcoverage']['otmBCount'] ?>)", 
              "(<?php echo $page['callcoverage']['otmCVisits'] ?>/<?php echo $page['callcoverage']['otmCCount'] ?>)", 
              "(<?php echo $page['callcoverage']['otmDVisits'] ?>/<?php echo $page['callcoverage']['otmDCount'] ?>)", 
              "(<?php echo $page['callcoverage']['otmUVisits'] ?>/<?php echo $page['callcoverage']['otmUCount'] ?>)"],
        datasets: [{
            type: 'bar',
            label: '2016',
            backgroundColor: [
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)'
        
            ],  
            borderColor: [
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)'
            ],
            borderWidth: 1,
            data: [<?php echo  $otmAPercentage ?>, <?php echo  $otmBPercentage ?>, <?php echo  $otmCPercentage ?>, <?php echo  $otmDPercentage ?>, <?php echo  $otmUPercentage ?>]
        },
{
            type: 'bar',
            label: '2016',
            backgroundColor: [
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)'
        
            ],
            borderColor: [
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)',
                'rgb(241, 242, 242)'
            ],
            borderWidth: 1,
            data: [100, 100, 100, 100, 100]
        }
        ]
    },
    options: {
        events: false,
        showToolTips: false,
        gridLines: {
            display: false
        },
        legend: { 
            display: false, 
            position: 'bottom'
        },
        scales: {
           yAxes: [{
            	stacked: true,
                ticks: {
                	display: false,
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    stepSize: 250,

                },
                gridLines: {
		            display: true,
		            color: '#F1F2F2'
		            },
                scaleLabel: {
                    display: true,
                    labelString: 'Call Coverage',
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                }
            }],
            xAxes: [{
                stacked: true,
                ticks: {
                    fontFamily: 'avenirBold',
                    fontSize: 13,
                    fontColor: '#327782'
                },
              	gridLines: {
  		            display: false,
  		            color: '#F1F2F2'
  		          },
                scaleLabel: {
                    display: true,
                    labelString: 'OTM Call Fulfilment Rate',
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                },
                barThickness: 70
                /*display: false*/
            }],
        },
        animation: {
        duration: 0,
        onComplete: function () {
            // render the value of the chart above the bar
             //alert('aasa');
            var ctx = this.chart.ctx;
            ctx.font = Chart.helpers.fontString(14, 'bold', 'avenirBold');
            ctx.fillStyle = '#327782';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            var dsetcount = 0;
            this.data.datasets.forEach(function (dataset) {
                if(dsetcount<1){ //write labels if looping through first dataset !!HACK!!
                  for (var i = 0; i < dataset.data.length; i++) {
                      /*if(dataset.data[i]>5){
                         ctx.fillStyle = '#FFFFFF';
                      }else{
                          ctx.fillStyle = '#327782';
                      }*/
                      ctx.fillStyle = '#327782';
                      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                      if (dataset.data[i] < 50)
                        ctx.fillText(dataset.data[i] + '%', model.x, model.y );
                      else
                        ctx.fillText(dataset.data[i] + '%', model.x, model.y+18 );
                  }
              }
                dsetcount = 1;
            });
            /*
            var ctx2 = this.chart.ctx;
            ctx2.font = Chart.helpers.fontString(15, 'bold', 'avenirBold');
            ctx2.fillStyle = '#327782';
            ctx2.textAlign = 'center';
            ctx2.textBaseline = 'bottom';
            var dsetcount = 0;
            this.data.datasets.forEach(function (dataset) {
                if(dsetcount<1){ //write labels if looping through first dataset !!HACK!!
                  for (var i = 0; i < dataset.data.length; i++) {                     
                      ctx2.fillStyle = '#000000';
                      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                      console.log("i: "+i);
                      switch(i){
                        case 0: ctx2.fillText(<?php echo $otmAVisits ?>+'%', model.x, model.y); break;
                        case 1: ctx2.fillText(<?php echo $otmBVisits ?>+'%', model.x, model.y); break;
                        case 2: ctx2.fillText(<?php echo $otmCVisits ?>+'%', model.x, model.y); break;
                        case 3: ctx2.fillText(<?php echo $otmDVisits ?>+'%', model.x, model.y); break;
                        case 4: ctx2.fillText(<?php echo $otmUVisits ?>+'%', model.x, model.y); break;                        
                      }
                  }
              }
                dsetcount = 1;
            });

            var ctx3 = this.chart.ctx;
            ctx3.font = Chart.helpers.fontString(14, 'bold', 'avenir');
            ctx3.fillStyle = '#327782';
            ctx3.textAlign = 'center';
            ctx3.textBaseline = 'bottom';
            var dsetcount = 0;
            this.data.datasets.forEach(function (dataset) {
                if(dsetcount<1){ //write labels if looping through first dataset !!HACK!!
                  for (var i = 0; i < dataset.data.length; i++) {
                      
                      ctx3.fillStyle = '#000000';
                      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                      switch(i){
                        case 0: ctx3.fillText(<?php echo $page['callcoverage']['otmAVisits']?>+"/"+<?php echo $page['callcoverage']['otmACount'] ?>, model.x, model.y + (dataset.data[i]*0.60)); break;
                        case 1: ctx3.fillText(<?php echo $page['callcoverage']['otmBVisits']?>+"/"+<?php echo $page['callcoverage']['otmBCount'] ?>, model.x, model.y + (dataset.data[i]*0.60)); break;
                        case 2: ctx3.fillText(<?php echo $page['callcoverage']['otmCVisits']?>+"/"+<?php echo $page['callcoverage']['otmCCount'] ?>, model.x, model.y + (dataset.data[i]*0.60)); break;
                        case 3: ctx3.fillText(<?php echo $page['callcoverage']['otmDVisits']?>+"/"+<?php echo $page['callcoverage']['otmDCount'] ?>, model.x, model.y + (dataset.data[i]*0.60)); break;
                        case 4: ctx3.fillText(<?php echo $page['callcoverage']['otmUVisits']?>+"/"+<?php echo $page['callcoverage']['otmUCount'] ?>, model.x, model.y + (dataset.data[i]*0.60)); break;                
                      }
                  }
              }
                dsetcount = 1;
            });*/
        }}

    }
    });    

</script>