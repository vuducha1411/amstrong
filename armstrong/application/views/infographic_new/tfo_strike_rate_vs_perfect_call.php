<div id = "ccheader" class="text-left  sectionheader">Transfer Order & Strike Rate vs. Perfect Call</div>
<div id="tfovscall-top"  class="center">
    <!--
    <div id="tfovscall-box-legend">
        <text class="tc-prev font-size-sm"><?php echo $page['year']-1; ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo $page['year']; ?></text><br />
        <!-/- <text class="tc-prct font-size-sm">Perfect Call</text><br /> -/->
        <text class="tc-prct font-size-sm">
            <?php
                if ($is_print != true){
             ?>
                  <a id="totalperfectcall" href="<?= site_url('/ami/infographic_new/total_perfect_calls'); ?>" data-toggle="ajaxModal"> 
              <?php
                }
              ?>
            Perfect Call %

            <?php
                if ($is_print != true)  
              ?>
                 </a> 
        </text>
        <br />
        <text class="tc-total font-size-sm">
            <?php
                if ($is_print != true){
             ?>
                  <a id="totalstrikerate" href="<?= site_url('/ami/infographic_new/total_strike_rate'); ?>" data-toggle="ajaxModal"> 
              <?php
                }
              ?>
            Strike Rate

            <?php
                if ($is_print != true)  
              ?>
                 </a> 
        </text>
        <br />
    </div>
 
    <div id="tfovscall-box">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>

    <div id="tfovscall-box" transform="translate(150,30)" float="left">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>

    <div id="tfovscall-box" transform="translate(150,30)">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>

    <div id="tfovscall-box" transform="translate(150,30)">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>

    <div id="tfovscall-box" transform="translate(150,0)">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>

    <div id="tfovscall-box" transform="translate(150,30)">
        <text class="tc-prev font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'], 0,'',',')); ?></text><br />
        <text class="tc-curr font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'',',')); ?></text><br />
        <!-/- <text class="tc-prct font-size-sm"><?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'], 0,'',',')); ?>/<?php echo ($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'',',')); ?></text><br /> -/->
        <text class="tc-prct font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'.','')))*100)); ?>%</text><br />
        <text class="tc-total font-size-sm"><?php echo intval(((($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'], 0,'.','')))*100)); ?>%</text>
    </div>
    -->

</div>

<div id="tfovscall-mid">
    <canvas id="myChart" class="strikeratechart" height=""></canvas>
</div>

<div id="tfovscall-btm">
    <div id="new-legend">
        <div id="yrPrev"></div><text class="yrCurr"> <!-- <?php echo $page['year']-1; ?>-->PY</text>
        <div id="yrCurr"></div><text class="yrCurr"><!--<?php echo $page['year']?>-->CY</text>
        <div id="sr"></div><text class="yrCurr">Perfect Call</text>
        <div id="pc"></div><text class="yrCurr">Strike Rate</text>
    </div>
</div>

<div id="striketable">
        <div class="callheaderrow">
            <div class="strikeheadercell titlecell1  font-size-sm"  style="width: 150px;">
              &nbsp; 
            </div>
            <div class="strikeheadercell font-size-sm ">
              <?php echo $page['fiveMonthsAgo']; ?>
            </div>
             <div class="strikeheadercell  font-size-sm">
              <?php echo $page['fourMonthsAgo']; ?>
            </div>
             <div class="strikeheadercell  font-size-sm">
              <?php echo $page['threeMonthsAgo']; ?>
            </div>
           <div class="strikeheadercell  font-size-sm">
              <?php echo $page['twoMonthsAgo']; ?>
            </div>
            <div class="strikeheadercell  font-size-sm">
              <?php echo $page['lastMonth']; ?>
            </div>
            <div class="strikeheadercell  font-size-sm">
              <?php echo $page['thisMonth']; ?>
            </div>
        </div>
        <!-- <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
            <div class="titlecell  font-size-sm" style="background: rgb(74, 212, 225)">
                Strike Rate (<?php echo $page['year']-1 ?>)
            </div>
            <div class="datacell  font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgoLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
             <div class="datacell  font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgoLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
             <div class="datacell  font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgoLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
           <div class="datacell font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgoLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
            <div class="datacell font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsLastMonthLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsLastMonthLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
            <div class="datacell font-size-sm">
                <?php echo ($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'], 0,'',',')) . "/" . ($page['tfoStrikeRateForBarGraph']['callsThisMonthLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThisMonthLastYear'][0]['callRecords'], 0,'.','')); ?>
            </div>
        </div>
        <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
            <div class="titlecell  font-size-sm" style="background: rgb(74, 212, 225)">
                Strike Rate (<?php echo $page['year']-1 ?>) %
            </div>
            <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgoLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
             <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgoLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
             <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgoLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
           <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgoLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgoLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
            <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsLastMonthLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsLastMonthLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
            <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThisMonthLastYear'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThisMonthLastYear'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
        </div> -->
        <!-- <div class="avgcallrow datarow">
            <div class="titlecell  font-size-sm">
                Strike Rate (<?php echo $page['year']?>) %
            </div>
            <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
             <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
             <div class="datacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
           <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
            <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
            <div class="datacell font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'], 0,'.','')))*100)); ?>%
            </div>
        </div> -->
         <div class="frequencyrow datarow">
            <div class="titlecell  font-size-sm" style="width: 150px;">
              <text style="font-size: 9px">
              &nbsp; <br/>
              </text>
              <?php
                if ($is_print != true){
             ?>
                  <a id="totalwhite" href="<?= site_url('/ami/infographic_new/total_perfect_calls'); ?>" data-toggle="ajaxModal"> 
              <?php
                }
              ?>
                Perfect Calls

            <?php
                if ($is_print != true)  
              ?>
                 </a> 
              <text style="font-size: 7px">
              <br/>&nbsp; 
              </text>
            </div>
            <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
              <br/>
              (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'.','')); ?>)
            </div>
             <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
              <br/>
              (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'.','')); ?>)
            </div>
             <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
              <br/>
              (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'.','')); ?>)
            </div>
           <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
              <br/>
              (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'.','')); ?>)
            </div>
            <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'.','')))*100)); ?>%
              <br/>
             (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'.','')); ?>)
            </div>
            <div class="strikedatacell  font-size-sm">
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'.','')))*100)); ?>%
             <br/>
              (<?php echo number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'.','')); ?>)
            </div>
        </div>
        <div class="avgcallrow datarow" style="border: 1px solid #4BD5E2;">
            <div class="titlecell  font-size-sm" style="width: 150px; background: rgb(74, 212, 225); padding:3px">
            <text style="font-size: 10px">
            &nbsp; <br/>
            </text>
            <?php
                if ($is_print != true){
             ?>
                  <a id="totalwhite" href="<?= site_url('/ami/infographic_new/total_strike_rate'); ?>" data-toggle="ajaxModal"> 
              <?php
                }
              ?>
                Strike Rate

            <?php
                if ($is_print != true)  
              ?>
                 </a> 
              <text style="font-size: 6px">
              <br/>&nbsp; 
              </text>
            </div>
            <div class="strikedatacell  font-size-sm">
              
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
                    </br>                    
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'.','')); ?>)
            
            </div>
             <div class="strikedatacell  font-size-sm">
               <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
                 </br>
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'.','')); ?>)
            </div>
             <div class="strikedatacell  font-size-sm">
               
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
                </br>
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'.','')); ?>)
                 
            </div>
           <div class="strikedatacell font-size-sm">
               <?php echo intval(((($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'.','')))*100)); ?>%
                </br>  
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'.','')); ?>)
                
                          
            </div>
            <div class="strikedatacell font-size-sm">
               
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'.','')))*100)); ?>%
                 </br>
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'.','')); ?>)
               
            </div>
            <div class="strikedatacell font-size-sm">
               
                <?php echo intval(((($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'.','')))*100)); ?>%
                  </br>
                (<?php echo number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'','')) . "/" . number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'.','')); ?>)
               
            </div>
        </div>
        <!--  <div class="frequencyrow datarow">
            <div class="titlecell  font-size-sm">
                Perfect Calls %
            </div>
            <div class="datacell  font-size-sm">
              </div>
             <div class="datacell  font-size-sm">
             </div>
             <div class="datacell  font-size-sm">
              </div>
           <div class="datacell  font-size-sm">
                </div>
            <div class="datacell  font-size-sm">
              </div>
            <div class="datacell  font-size-sm">
                </div>
        </div> -->
    </div>

<script>
    var ctx = document.getElementById("myChart");
    Chart.defaults.global.showToolTips = false;
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [ 
                    "<?php echo $page['fiveMonthsAgo']; ?>",
                    "<?php echo $page['fourMonthsAgo']; ?>",
                    "<?php echo $page['threeMonthsAgo']; ?>",
                    "<?php echo $page['twoMonthsAgo']; ?>",
                    "<?php echo $page['lastMonth']; ?>",
                    "<?php echo $page['thisMonth']; ?>"
                ],
        datasets: [{
            type: 'line',
            /*label: 'Perfect Call',*/ /*color and label is changed, switched with the bar graphs*/
            lineTension: 0,
            fill: false,
            yAxisID: "yRight",
            /*backgroundColor: 'rgb(255, 43, 48)',*/
            borderColor: 'rgb(15, 177, 238)',
            fontColor: '#FFF',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: 'rgb(15, 177, 238)',
            pointBorderWidth: 2,
            pointRadius: 7,
            data: [
                    /* strike rate line graph */
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFiveMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsFourMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThreeMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsTwoMonthsAgo'][0]['callRecords'], 0,'.','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsLastMonth'][0]['callRecords'], 0,'.','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'] == 0 ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['callsThisMonth'][0]['callRecords'], 0,'.','')))*100)); ?>
                  ] 
        },{
            type: 'line',
            /*label: 'Strike Rate',*/
            lineTension: 0,
            fill: false,
            yAxisID: "yRight",
            /*backgroundColor: 'rgb(15, 177, 238)',*/
            borderColor: 'rgb(255, 43, 48)',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: 'rgb(255, 43, 48)',
            pointBorderWidth: 2,
            pointRadius: 7,
            data: [
                    /* perfect call line graph*/
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFiveMonthsAgo']['callCount'], 0,'','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsFourMonthsAgo']['callCount'], 0,'','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThreeMonthsAgo']['callCount'], 0,'','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsTwoMonthsAgo']['callCount'], 0,'','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsLastMonth']['callCount'], 0,'','')))*100)); ?>,
                    <?php echo intval(((($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['perfectCalls'], 0,'',''))/($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'] == "" ? 1.00 : number_format($page['tfoStrikeRateForBarGraph']['perfectCallsThisMonth']['callCount'], 0,'','')))*100)); ?>                    
                  ]
        },{
            type: 'bar',
           /* label: '2015',*/
            yAxisID: "yLeft",
             backgroundColor: [
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)'
            ],
            borderColor: [
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)',
                'rgb(255, 139, 133)'
            ],
            borderWidth: 1,
            data: [
                    <?php echo ($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgoLastYear'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgoLastYear'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgoLastYear'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgoLastYear'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonthLastYear'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonthLastYear'][0]['helpme'], 0,'','')); ?>
                  ] 
        },{
            type: 'bar',
            /*label: '2016',*/
            yAxisID: "yLeft",
            backgroundColor: [
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)'
            ],
            borderColor: [
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)',
                'rgb(74, 212, 225)'
            ],
            borderWidth: 1,
            data: [
                    <?php echo ($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fiveMonthsAgo'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['fourMonthsAgo'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['threeMonthsAgo'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['twoMonthsAgo'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['lastMonth'][0]['helpme'], 0,'','')); ?>,
                    <?php echo ($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'] == "" ? 0.00 : number_format($page['tfoStrikeRateForBarGraph']['thisMonth'][0]['helpme'], 0,'','')); ?>
                  ]
        }]
    },
    options: {
        events: false,
        showToolTips: false,
        responsive: true,
        maintainAspectRatio: true,
        showDatasetLabels : true,
        legend: { 
            display: false,/*, 
            position: 'bottom'*/
        },
        tooltips: {
              callbacks: {
               
                label: function(tooltipItem, data) {
                    
                    return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                  }
               }
              
            },
        scales: {
            yAxes: [{
                position: "left",
                "id": "yLeft",
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: <?php 
                            echo(round(max($this->data['page']['callsMax'])+50, -1));
                         ?>,
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782',
                    stepSize: <?php echo str_replace(",", "", number_format(round(max($this->data['page']['callsMax'])+50, -1)/5)); ?>,
                    userCallback: function(value, index, values) {
                        // Convert the number to a string and splite the string every 3 charaters from the end
                        value = value.toString();
                        value = value.split(/(?=(?:...)*$)/);

                        // Convert the array to a string and format the output
                        value = value.join(',');
                        return value;
                    }
                },
                gridLines: {
                    display: true,
                    color: 'rgb(255,255,255)'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'TFOs',//'Number of Calls with TFOs',
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                }
            },{
                position: "right",
                "id": "yRight",
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    stepSize: 20,
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                },
                gridLines: {
                    display: true,
                    color: 'rgb(255,255,255)'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Strike Rate and Perfect Call %',
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                }
            }],
            xAxes: [{
                ticks: {
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                },
                barThickness: 25,
                gridLines: {
                    color: 'rgb(255,255,255)'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Months',
                    fontFamily: 'avenirMed',
                    fontSize: 16,
                    fontColor: '#327782'
                }
                /*display: false*/
            }]
        },
          animation: {
          onComplete: function () {
              // render the value of the chart above the bar
               //alert('aasa');
              var ctx = this.chart.ctx;
              ctx.font = Chart.helpers.fontString(16, 'bold', 'avenir');
              ctx.fillStyle = '#327782';
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              var dsetcount = 0;
              this.data.datasets.forEach(function (dataset) {
                  for (var i = 0; i < dataset.data.length; i++) {
                      console.log(dataset.type);
                      ctx.fillStyle = '#327782';
                      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                      if (dataset.type == "bar")
                        ctx.fillText(dataset.data[i].toLocaleString('en'), model.x, model.y);
                  }
              });
          }}
    }
    });    
</script>