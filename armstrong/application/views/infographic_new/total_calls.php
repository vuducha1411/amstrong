<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Calls</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="500" height="110"></canvas>
		</div>
		<!--<div id="totalcallvalues">
			<?php 
				for($s=11;$s>=0;$s--){
			?>		
			<span  class="total-call-month"> <?php echo_isset(number_format($page['total_calls'][$s]['totalCalls']), 0); ?> </span>

			<?php
				}
			?>
		</div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");
    
    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [

            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][11]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][10]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][9]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][8]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][7]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][6]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][5]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][4]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][3]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][2]['totalCalls']), 1); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][1]['totalCalls']), 1); ?>"],
             ["<?php echo DateTime::createFromFormat('!m', $page['total_calls'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_calls'][0]['totalCalls']), 1); ?>"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo $page['total_calls'][11]['totalCalls'] ?>,
                <?php echo $page['total_calls'][10]['totalCalls'] ?>,
                <?php echo $page['total_calls'][9]['totalCalls'] ?>,
                <?php echo $page['total_calls'][8]['totalCalls'] ?>,
                <?php echo $page['total_calls'][7]['totalCalls'] ?>,
                <?php echo $page['total_calls'][6]['totalCalls'] ?>,
                <?php echo $page['total_calls'][5]['totalCalls'] ?>,
                <?php echo $page['total_calls'][4]['totalCalls'] ?>,
                <?php echo $page['total_calls'][3]['totalCalls'] ?>,
                <?php echo $page['total_calls'][2]['totalCalls'] ?>,
                <?php echo $page['total_calls'][1]['totalCalls'] ?>,
                <?php echo $page['total_calls'][0]['totalCalls'] ?>
            ] 
        },
        { //total customers
            type: 'bar',
           	label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo $page['total_calls'][11]['totalCalls'] ?>,
                <?php echo $page['total_calls'][10]['totalCalls'] ?>,
                <?php echo $page['total_calls'][9]['totalCalls'] ?>,
                <?php echo $page['total_calls'][8]['totalCalls'] ?>,
                <?php echo $page['total_calls'][7]['totalCalls'] ?>,
                <?php echo $page['total_calls'][6]['totalCalls'] ?>,
                <?php echo $page['total_calls'][5]['totalCalls'] ?>,
                <?php echo $page['total_calls'][4]['totalCalls'] ?>,
                <?php echo $page['total_calls'][3]['totalCalls'] ?>,
                <?php echo $page['total_calls'][2]['totalCalls'] ?>,
                <?php echo $page['total_calls'][1]['totalCalls'] ?>,
                <?php echo $page['total_calls'][0]['totalCalls'] ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    stepSize: <?php echo $page['max_call_count'] == 0 ? 20 : round($page['max_call_count']/5)?>, // 50,
                    //stepSize: 2000 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>