<?php

	$customerCount = 0;
	if ($page['customerProfiling']['customerCount'] < 1){
		$customerCount = 1;
	}else{
		$customerCount = $page['customerProfiling']['customerCount'];
	}

	$profiledPercent = intval(number_format(($page['customerProfiling']['profiled']/ $customerCount)*100,1));
	$contactPercent = intval(number_format(($page['customerProfiling']['pContactCount']/ $customerCount)*100,1));
	$channelPercent = intval(number_format(($page['customerProfiling']['channelCount']/ $customerCount)*100,1));
	$cusNamePercent = intval(number_format(($page['customerProfiling']['customerNameCount']/ $customerCount)*100,1));
	$addressPercent = intval(number_format(($page['customerProfiling']['addressCount']/ $customerCount)*100,1));
	$emailPercent = intval(number_format(($page['customerProfiling']['emailMobileCount']/ $customerCount)*100,1));
	$signPercent  = intval(number_format(($page['customerProfiling']['profiled']/ $customerCount)*100,1));
?>

<div id="profiling" >
        <span id="customerprofilingheader" class="text-left sectionheader"> Customer Profiling </span>
</div>   

<div id="profiling-vals">
	<div class="cust-prof-row row">
		<div class="font-size-prof prof-column-header col-main" > Profiled Customer/Total Customers </div>
		<div  class="font-size-sm prof-column-header col-val" > Primary Contact Name </div>
		<div  class="font-size-sm prof-column-header col-val"> Channel </div>
		<div  class="font-size-sm prof-column-header col-val" > Customer Name </div>
		<div  class="font-size-sm prof-column-header col-val" > Address </div>
	 	<div  class="font-size-sm prof-column-header col-val" > Email/No. </div>
	 	<?php if($country_id==1){ ?>
	 		<div  class="font-size-sm prof-column-header col-val" > Signature </div>
	 	<?php } ?>
	</div>
		
	<div class="cust-prof-row row">	
		 <div  class="font-size-sm prof-column-values col-main  col-total"><?php echo number_format($page['customerProfiling']['profiled']) ?> / <?php echo number_format($page['customerProfiling']['customerCount']) ?></div>
		 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val"> <?php echo number_format($page['customerProfiling']['pContactCount']) ?> </span></div>
		 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val"> <?php echo number_format($page['customerProfiling']['channelCount']) ?> </span></div>
		 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val"> <?php echo number_format($page['customerProfiling']['customerNameCount']) ?> </span></div>
		 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val"> <?php echo number_format($page['customerProfiling']['addressCount']) ?> </span></div>
		 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val"> <?php echo number_format($page['customerProfiling']['emailMobileCount']) ?> </span></div>
		 <?php if($country_id==1){?>
			 <div  class="font-size-sm prof-column-values col-val"><span class="criteria-val">  <?php echo number_format($page['customerProfiling']['signatureCount']) ?> </span></div>
	     <?php } ?>
	</div>

	<div class="cust-prof-row row">	
		 <div  class="font-size-sm prof-column-header col-main"> (<?php echo $profiledPercent ?>%) </div>
		 <div  class="font-size-sm prof-column-header  col-val"> <?php echo $contactPercent ?>% </div>
		 <div  class="font-size-sm prof-column-header  col-val"> <?php echo $channelPercent ?>% </div>
		 <div  class="font-size-sm prof-column-header  col-val"> <?php echo $cusNamePercent ?>% </div>
		 <div  class="font-size-sm prof-column-header  col-val"> <?php echo $addressPercent ?>% </div>
		 <div  class="font-size-sm prof-column-header  col-val"> <?php echo $emailPercent ?>% </div>
		 <?php if($country_id==1){ ?>
		 	<div  class="font-size-sm prof-column-header  col-val"> <?php echo $signPercent ?>% </div>
		 <?php } ?>
	</div>	
</div>