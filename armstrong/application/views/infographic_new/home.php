  

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title><?= $page['title'] ?></title>
    <?php foreach($page['stylesheets'] as $item) { ?>
        <link rel="stylesheet" href="<?=base_url().$item?>" />
    <?php } ?>

    <?php foreach($page['scripts'] as $item) { ?>
         <script src="<?= base_url() . $item ?>"></script>
    <?php } ?>

    <?php
    if ($is_print == true){
    ?>
        <link rel="stylesheet" href="<?php echo site_url('res/css/infographic_new/style.css') ?>"/>
        <link rel="stylesheet" media="print" href="<?php echo site_url('res/css/infographic_new/print.css') ?>"/>
        <link rel="stylesheet" media="print" href="<?php echo site_url('res/css/infographic_new/tfogrowth.css') ?>"/>
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="print" href="<?php echo site_url('res/css/infographic_new/print.css') ?>"/>
        <![endif]-->

    <?php
        }else{
    ?>
        <link rel="stylesheet" href="<?php echo site_url('res/css/infographic_new/style.css') ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('res/css/infographic_new/tfogrowth.css') ?>"/>
   <?php
        }
    ?>
    
    
    <!--<link rel="stylesheet" href="<?php echo site_url('res/css/bootstrap.min.css') ?>"/>-->
    <!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/jquery.circliful.js'); ?>"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.min.js"></script>
    <style type="text/css" media="print">
        .textbsheader {
            font-size: 50px;
        }

        .breakpage{
            float:left;
           height: 25%;
           }

        .breakpage2{
           
           height: 25%;
           }   
    </style>

</head>


<body >
    <div id="infographic" class="container-fluid">
        <div id="header" class="row" >
            <div id="title" class="main text-left">
                <!--<label id="pageTitle" class="main"> Infographic Report </label>-->

                <label id="country" class="main headerbold">
                        &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $country_name .',' ; ?>
                </label> 

                <label id="month" class="main headerbold">
                     <?php echo $page['month']; ?>
                </label> 

                <label id="year" class="main headerbold">
                     <?php echo $page['year']; ?>
                </label> 
            </div>    
        </div>  
        <div id="content" class="row" style="margin-right: 20px;">
            <div id = "left" class="col-lg-6">
                <div id="salesteam" class="section">
                    <?php include(dirname(__FILE__) . '/salesteam.php');  ?>
                </div> 

                <div id="customerprofiling" class="section">
                    <?php include(dirname(__FILE__) . '/customer_profiling.php');  ?>
                </div> 

                <div id="callcriteria" class="section">
                    <?php include(dirname(__FILE__) . '/perfect_calls_criteria.php'); ?>
                </div>

                <div id="tfovscall" class="section">
                    <?php include(dirname(__FILE__) . '/tfo_strike_rate_vs_perfect_call.php');  ?>
                </div>
                 
                <div id="tfogrowth" class="section ">
                    <?php include(dirname(__FILE__) . '/tfo_growth.php');  ?>
                </div>
            </div>
            <div id = "right" class="col-lg-6">
                <div id="callcoverage" class="section right">
                    <?php include(dirname(__FILE__) . '/call_coverage.php');  ?>
                </div>

                <div id="topsku" class="section right">
                    <?php include(dirname(__FILE__) . '/top_sku.php');  ?>
                </div>
            </div> 
        </div>   

        <div id="bottom" class="row" style="margin-right: 20px;"> 
             <div id="grip" class="section right">
                <?php include(dirname(__FILE__) . '/grip.php');  ?>
            </div>  
             <div id="grip" class="section right">
                <?php include(dirname(__FILE__) . '/grab.php');  ?>
            </div>
            <div id="left" class="col-lg-6">
                <div class="section">
                    <?php include(dirname(__FILE__) . '/growth_grip.php');  ?>
                </div>
            </div>
            <div id = "right" class="col-lg-6">
                <div id="growth-grip" class="section right">
                    <?php include(dirname(__FILE__) . '/growth_grab.php');  ?>
                </div> 
            </div>
        </div>
    </div>    

<script>
        
        $(document).ready(function () {
          
            <?php
            if ($is_print == 1){
            ?>
                
                setTimeout(function(){
                    window.print();
                }, 2000

                    );
               /*window.print();*/
           
            <?php
            }
            ?>
        }
        );
        
 </script>     

</html>

