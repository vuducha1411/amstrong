<div id="topskuheader" class="text-left  sectionheader">Top 10 SKU Penetration</div>
<div id="topskutable">	
	<?php $i=1; 
		$percentage = array();
		$percentageLastYear = array();
		$penetrationValueDividend = array();
		$penetrationValueDivisor = array();
		$penetrationValueLastYearDividend = array();
		$penetrationValueLastYearDivisor = array();

		$topskupercentLastYear = 0;
		$topskupercent = 0;
		$skuNumber = '';
		$skuName = '';

		$activeGrip=1;
		$activeGripLastYear=1;

		$activeGrip = $page['grip'][0][0]['total_grip'];
		$activeGripLastYear = $page[Infographic_model::TOP_SKU]['activeGripLastYear'][0]['activeGrip'];

		$activeGripByTeam=1;
		$activeGripByTeamLastYear=1;
		if (!is_null($page[Infographic_model::TOP_SKU]['activeGripByTeam'][0]['activeGrip']))
			$activeGripByTeam = $page[Infographic_model::TOP_SKU]['activeGripByTeam'][0]['activeGrip'];
		if (!is_null($page[Infographic_model::TOP_SKU]['activeGripByTeamLastYear'][0]['activeGrip']))
			$activeGripByTeamLastYear = $page[Infographic_model::TOP_SKU]['activeGripByTeamLastYear'][0]['activeGrip'];
	?>

	<?php $i = 1 ?>
	<?php foreach (array_slice($page[Infographic_model::TOP_SKU]['top3'], 0, 10) as $main_sku => $top_sku): ?>
		<div class="topskurow">
			<div id="sku_<?php echo($i) ?>" class="topskudata"><!--  id=sku_1  -->
				<?php if (isset($top_sku['main'])) {
                    if ($top_sku['main']['group_name']){ ?>
						<span id="sku_<?php echo($i) ?>_name" class="top_sku_name text-left font-size-sm"><!--  id=sku_1_name  -->
							<span id="hoverhere">
								<p class="top_sku_name text-left font-size-sm"> 
									<?php echo_isset($top_sku['main']['group_name']); ?>

									<text id="hovervalue"> 
										
										<?php 
											foreach($page[Infographic_model::TOP_SKU]['childrenSku'] as $sku){
					                			if ($sku['mainSku'] == $top_sku['main']['skuNumber']){
					                				echo $sku['skuNumber'] . " - " . $sku['skuName'] . " (" . $sku['quantity_case'] . " X " . $sku['weight_pc'] . ")";
					                				echo "<br/>";
					                			}
					                		}
										?>
									</text> 
								</p>								
							</span>
						</span>
						<div class="wrapper" style=" width: 90%; position: relative;">
						<canvas id="sku_<?php echo($i) ?>_graph" class="topskugraph" height="30">	
							<?php
		                        if (isset($top_sku['main'])) {
		                        	if($this->session->userdata('infographicViewType') == "country"){
			                            $topskupercent = ($top_sku['main']['totalCustomer'] > 0 && $top_sku['main']['totalCustomerOrder'] > 0) ? $top_sku['main']['totalCustomerOrder'] / $activeGrip * 100
			                                : 0;
			                            //echo '<p class="top-sku-percent">[' . number_format($topskupercent) . '%] />';
			                            $skuNumber = $top_sku['main']['skuNumber'];
			                            $skuName = str_replace(" ", "/", $top_sku['main']['group_name']);
			                            $penetrationValueDividend[$i] = $activeGrip;
			                            $penetrationValueDivisor[$i] = $top_sku['main']['totalCustomerOrder'];

			                            /*for($v = 0 ; $v < count($page[Infographic_model::TOP_SKU]['top3LastYear']) ; $v++){
					                		if (isset($top_sku['main'])) {
							                	if ($page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['skuNumber'] ==  $top_sku['main']['skuNumber']){
							                		$topskupercentLastYear = number_format((($page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['totalCustomerOrder']/$activeGrip)*100), 0,'.','');
						                            $penetrationValueLastYearDividend[$i] = $activeGripLastYear;
						                            $penetrationValueLastYearDivisor[$i] = $page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['totalCustomerOrder'];
			                            			$v = count($page[Infographic_model::TOP_SKU]['top3LastYear']);
							                	}else{
							                		$topskupercentLastYear = 0;
						                            //$penetrationValueLastYearDividend[$i] = 0;
						                            $penetrationValueLastYearDivisor[$i] = 0;
							                	}
						                	}
						                }*/

						               /* $key = array_search($skuNumber, $page['topsku']['topLastYear']);*/
						                $penetrationValueLastYearDividend[$i] = $activeGripLastYear;

						                for($v = 0 ; $v < count($page['topsku']['topLastYear']) ; $v++){
					                		if (isset($top_sku['main'])) {
							                	if ($page['topsku']['topLastYear'][$v]['skuNumber'] ==  $top_sku['main']['skuNumber']){
							                		$penetrationValueLastYearDivisor[$i] = $page['topsku']['topLastYear'][$v]['totalCustomerOrder'];
						                			$topskupercentLastYear = number_format((($penetrationValueLastYearDivisor[$i]/$activeGrip)*100), 0,'.','');
						                			
						                			//$v = count($page[Infographic_model::TOP_SKU]['top3LastYear']);
							                		break;
							                	}else{
							                		$topskupercentLastYear = 0;
						                            //$penetrationValueLastYearDividend[$i] = 0;
						                            $penetrationValueLastYearDivisor[$i] = 0;
							                	}
						                	}
						                }		
						                
						               /* if($key){
						                	$penetrationValueLastYearDivisor[$i] = $page['topsku']['topLastYear'][$key]['totalCustomerOrder'];
						                	$topskupercentLastYear = number_format((($penetrationValueLastYearDivisor[$i]/$activeGrip)*100), 0,'.','');
						                }else{
						                	$topskupercentLastYear = 0;
						                    $penetrationValueLastYearDivisor[$i] = 0;
							                	
						                }
										*/
						                 
			                        } else { //team view
			                            $skuNumber = $top_sku['main']['skuNumber'];
			                            $skuName = str_replace(" ", "/", $top_sku['main']['group_name']);
			                        	foreach($page[Infographic_model::TOP_SKU]['topByTeam'] as $sku){
				                			if ($sku['skuNumber'] == $top_sku['main']['skuNumber']){
				                				$topskupercent = number_format(($sku['totalCustomerOrder']/$activeGripByTeam)*100);
					                            $penetrationValueDividend[$i] = $activeGripByTeam;
					                            $penetrationValueDivisor[$i] = $sku['totalCustomerOrder'];
		                            			break;
				                			}else{
				                				$topskupercent = 0;
					                           // $penetrationValueDividend[$i] = 0;
					                            $penetrationValueDivisor[$i] = 0;                			
					                        }

				                			foreach($page[Infographic_model::TOP_SKU]['topByTeamLastYear'] as $sku){
					                			if ($sku['skuNumber'] == $top_sku['main']['skuNumber']){
					                				$topskupercentLastYear = number_format(($sku['totalCustomerOrder']/$activeGripByTeamLastYear)*100);
						                            $penetrationValueLastYearDividend[$i] = $activeGripByTeamLastYear;
						                            $penetrationValueLastYearDivisor[$i] = $sku['totalCustomerOrder'];
		                            				break;
					                			}else{
							                		$topskupercentLastYear = 0;
						                           // $penetrationValueLastYearDividend[$i] = 0;
						                            $penetrationValueLastYearDivisor[$i] = 0;
							                	}
					                		}
				                		}
			                        }
		                        } 
		                     ?>
						</canvas>		
					<?php } else { ?> <!-- display sku name if group name is not available -->
						<span id="sku_<?php echo($i) ?>_name" class="text-left top_sku_name font-size-sm">
							<span id="hoverhere">
								<p class="top_sku_name text-left font-size-sm">
									<?php echo_isset($top_sku['main']['sku_name']) ?>

									<text id="hovervalue"> 
										<?php echo $top_sku['main']['skuNumber']; ?> - <?php echo_isset($top_sku['main']['sku_name']); ?> <?php echo isset($top_sku['main']['pakingSize']) ? "({$top_sku['main']['pakingSize']})" : "" ?>
								
										<?php 
											foreach($page[Infographic_model::TOP_SKU]['childrenSku'] as $sku){
					                			if ($sku['mainSku'] == $top_sku['main']['skuNumber']){
					                				echo "<br/>";
					                				echo $sku['skuNumber'] . " - " . $sku['skuName'] . " (" . $sku['quantity_case'] . " X " . $sku['weight_pc'] . ")";
					                			}
					                		}
										?>
									</text> 
								</p>								
							</span>
						</span>
						<div class="wrapper" style=" width: 90%; position: relative;">

							<?php
		                        if (isset($top_sku['main'])) {
		                        	if($this->session->userdata('infographicViewType') == "country"){
			                            $topskupercent = ($top_sku['main']['totalCustomer'] > 0 && $activeGrip > 0)
			                                ? $top_sku['main']['totalCustomerOrder'] / $activeGrip * 100
			                                : 0;
			                            //echo '<p class="top-sku-percent">[' . number_format($topskupercent) . '%] />';
			                            $skuNumber = $topss_ku['main']['skuNumber'];
			                            $skuName = str_replace(" ", "/", $top_sku['main']['sku_name']);
			                           	$penetrationValueDividend[$i] = $activeGrip;
			                            $penetrationValueDivisor[$i] = $top_sku['main']['totalCustomerOrder'];

			                            

			                           /* for($v = 0 ; $v < count($page[Infographic_model::TOP_SKU]['top3LastYear']) ; $v++){
					                		if (isset($top_sku['main'])) {
							                	if ($page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['skuNumber'] ==  $top_sku['main']['skuNumber']){
							                		$topskupercentLastYear = number_format((($page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['totalCustomerOrder']/$activeGripLastYear)*100), 0,'.','');
						                            $penetrationValueLastYearDividend[$i] = $activeGripLastYear;
						                            $penetrationValueLastYearDivisor[$i] = $page[Infographic_model::TOP_SKU]['top3LastYear'][$v]['totalCustomerOrder'];
			                            			$v = count($page[Infographic_model::TOP_SKU]['top3LastYear']);
							                	}else{
							                		$topskupercentLastYear = 0;
						                           // $penetrationValueLastYearDividend[$i] = 0;
						                            $penetrationValueLastYearDivisor[$i] = 0;
							                	}
						                	}
						                }*/

						                $penetrationValueLastYearDividend[$i] = $activeGripLastYear;

						                for($v = 0 ; $v < count($page['topsku']['topLastYear']) ; $v++){
					                		if (isset($top_sku['main'])) {
							                	if ($page['topsku']['topLastYear'][$v]['skuNumber'] ==  $top_sku['main']['skuNumber']){
							                		$penetrationValueLastYearDivisor[$i] = $page['topsku']['topLastYear'][$v]['totalCustomerOrder'];
						                			$topskupercentLastYear = number_format((($penetrationValueLastYearDivisor[$i]/$activeGrip)*100), 0,'.','');
						                			
						                			//$v = count($page[Infographic_model::TOP_SKU]['top3LastYear']);
							                		break;
							                	}else{
							                		$topskupercentLastYear = 0;
						                            //$penetrationValueLastYearDividend[$i] = 0;
						                            $penetrationValueLastYearDivisor[$i] = 0;
							                	}
						                	}
						                }

			                        } else {
			                            $skuNumber = $top_sku['main']['skuNumber'];
			                            $skuName = str_replace(" ", "/", $top_sku['main']['sku_name']);
			                        	foreach($page[Infographic_model::TOP_SKU]['topByTeam'] as $sku){
				                			if ($sku['skuNumber'] == $top_sku['main']['skuNumber']){
				                				$topskupercent = number_format(($sku['totalCustomerOrder']/$activeGripByTeam)*100);
					                            $penetrationValueDividend[$i] = $activeGripByTeam;
					                            $penetrationValueDivisor[$i] = $sku['totalCustomerOrder'];
					                            break;
				                			}else{
						                		$topskupercent = 0;
					                           // $penetrationValueDividend[$i] = 0;
					                            $penetrationValueDivisor[$i] = 0;
						                	}
				                		}

			                			foreach($page[Infographic_model::TOP_SKU]['topByTeamLastYear'] as $sku){
				                			if ($sku['skuNumber'] == $top_sku['main']['skuNumber']){
				                				$topskupercentLastYear = number_format(($sku['totalCustomerOrder']/$activeGripByTeamLastYear)*100);
					                            $penetrationValueLastYearDividend[$i] = $activeGripByTeamLastYear;
					                            $penetrationValueLastYearDivisor[$i] = $sku['totalCustomerOrder'];
					                            break;
				                			}else{
						                		$topskupercentLastYear = 0;
					                           // $penetrationValueLastYearDividend[$i] = 0;
					                            $penetrationValueLastYearDivisor[$i] = 0;
						                	}
				                		}
			                        }
		                        } 
		                 ?>
						
						<canvas id="sku_<?php echo($i) ?>_graph" class="topskugraph" height="30" >
						</canvas>
					<?php }
                } ?>

                <?php 
                	$percentage[$i] = number_format($topskupercent); 
                	$percentageLastYear[$i] = number_format($topskupercentLastYear); 
                	
                ?>
                <!--?php 
                	foreach (array_slice($page[Infographic_model::TOP_SKU]['top10'], 0, 10) as $main_sku => $top_sku):
                		echo($top_sku); echo(", ");
                	endforeach;
                ?-->
			
			<!-- <div class="skuchange"> -->
				<?php 
					$skuGrowth = $percentage[$i]-$percentageLastYear[$i];
                ?>
                <?php if (isset($top_sku['main'])) { ?>
	                 <div class="controls" style="margin-top: 1%; position: absolute; top:0; right:55;">
	                 	<?php
					        if ($is_print != true){
					     ?>
					          <a id="totaltfogrowth" href="<?= site_url('/ami/infographic_new/total_top_sku?skuName=' . $skuName . '&skuNumber=' . $skuNumber . ''); ?>" data-toggle="ajaxModal"> 
					      <?php
					        }
					      ?>
						<span id="topskuchange_<?php echo($i) ?>" class="topskuchange font-size-md">
							<?php 
								if ($skuGrowth > 0) {
									echo("&nbsp;&nbsp;+$skuGrowth%&nbsp;&nbsp;");
								} else {
									echo("&nbsp;&nbsp;$skuGrowth%&nbsp;&nbsp;");
								} 
							?>
						</span>
						<?php
						    if ($is_print != true)  
						  ?>
						     </a> 
					</div>
				</div>
				<?php } ?>
			<!-- </div>	 -->
			</div>
		</div>
		<?php $i++; ?>
	<?php endforeach; ?>
	
	<center>
		<span id="prevYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year'] - 1?>-->PY</span>
		<span id="currYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year']?>-->CY</span>
	</center>
</div>
<!-- <div class="legend">
<span id="currYr"></span><span class="tfogrowth-text font-size-sm"><?php echo $page['year']?></span>
<span id="prevYr"></span><span class="tfogrowth-text font-size-sm"><?php echo $page['year'] - 1?></span>
</div>  -->

<!-- charts.js scripts -->

<script>
$(document).ready(function(){



/* script for change in top sku percentage */

$('.topskuchange').each(
	function(){
		var id = $(this).attr('id');
		//console.log("erm "+id);
		var a = $('#'+id).html();
		//console.log("erhmmm ["+a+"]");
		//console.log("erhmmm ["+a.charAt(12)+"]");
		//alert(a.charAt(12));
		if(a.trim().charAt(12)=='-'){
			$(this).css('background-color', '#FF2A30');
		}
		//alert($(this.attr('id')).html().charAt(0));
		//if($(this).html().charAt(0)=='-')
			//this.css('background', '#ccc');
	}
);
	<?php 
		for($j=1;$j<$i;$j++){
			
	?>		
		var ctx<?php echo $j ?> = document.getElementById("sku_" + <?php echo $j ?> + "_graph"); 
	
		var top_<?php echo $j ?>_sku = new Chart(ctx<?php echo $j ?>, {
    	type: 'horizontalBar',
    	data: {
        	labels: [""],
        	datasets: [{
        		fill: true,
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            id: "thisYear",
	            data: [<?php echo($percentage[$j]) ?>] 
        	},{
        		fill: true,
	            backgroundColor: 'rgb(255, 139, 133)',
	            borderColor: 'rgb(255, 139, 133)',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            id: "lastYear",
	            data: [<?php echo($percentageLastYear[$j]) ?>] 
        	}]
    	},
    	options: {
    		events: false,
    		showToolTips: false,
    		maintainAspectRatio: true,
    		labels: {
    			display: true
    		},
        	responsive: true,
	        legend: { 
	            display: false, 
	            position: 'right'
	        },
        	scales: {
        		xAxes: [{
	        		display: false,
	        		barThickness: 1,
	        		color: '#F1F2F2',
	        		ticks : {
	        			min: 0,
	        			max: 170,
	        			fontFamily: 'avenirMed',
	                    fontsize: '16pt',
	                    fontColor: '#327782'
	        		},
	        		gridlines: {
	        			display: true,
	                    color: '#F1F2F2'
	        		}
        		}],
        		yAxes: [{
        			display: false,
	        		ticks : {
	        			min: 0,
	        			max: 170,
	        			fontFamily: 'avenirMed',
	                    fontsize: '16pt',
	                    fontColor: '#327782'
	        		},
	        		
	        		barThickness: 25,
	        		gridlines: {
	        			display: true,
	                    color: '#F1F2F2'
	        		}
        		}]
        	},
	        animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;
		          	//ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		          	ctx.textAlign = 'left';
		          	ctx.textBaseline = 'bottom';
		          	ctx.font = Chart.helpers.fontString(17, 'normal', 'avenirBold');
		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		
		              		/*ctx.fillText(label+"%", left + 10, model.y + 8);*/
		              		if (dataset.id == "lastYear")
		              			ctx.fillText(label+"% ", model.x + 5, model.y + 10);
		              		else
		              			ctx.fillText(label+"% ", model.x + 5, model.y + 10);

		            	}
		          	});     

		          	var ctx2 = this.chart.ctx;
		          	//ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		          	ctx2.textAlign = 'left';
		          	ctx2.textBaseline = 'bottom';
		          	ctx2.font = Chart.helpers.fontString(16, 'bold', 'avenir');
		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx2.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		
		              		/*ctx.fillText(label+"%", left + 10, model.y + 8);*/
		              		if (dataset.id == "lastYear")
		              			ctx2.fillText("            ("+<?php echo "\"" . number_format(intval($penetrationValueLastYearDivisor[$j])) . "\""  ?>+"/"+<?php echo "\"" . number_format(intval($penetrationValueLastYearDividend[$j])) . "\"" ?>+")", model.x + 5, model.y + 11);
		              		else
		              			ctx2.fillText("            ("+<?php echo "\"" . number_format(intval($penetrationValueDivisor[$j])) . "\"" ?>+"/"+<?php echo "\"" . number_format(intval($penetrationValueDividend[$j])) . "\"" ?>+")", model.x + 5, model.y + 11);

		            	}
		          	});            
			        }
			    }
	    	}
	    });

	<?php
		
		}	
	?>
	 
});
</script>
