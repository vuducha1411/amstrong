<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<p class="sectionheader">Total Sales Rep</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px;">
		<div id="totalcallshistory">
		    <canvas id="call-hist" width="500" height="90"></canvas>
		</div>
		<!--<div id="totalcallvalues" class="total-salesrep-div">
			<?php 
				for($s=11;$s>=0;$s--){
			?>	
			<span  class="total-srCount">
                <?php echo_isset(number_format($page['total_sr'][$s]['srCount']), 0); ?>
            </span>
            <?php
				}
			?>
		</div>-->
	</div>	
</div>

<script>

var gchart = document.getElementById("call-hist");

    var myChart = new Chart(gchart, {
    type: 'bar',
    data: {
        labels: [
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][11]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][11]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][10]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][10]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][9]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][9]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][8]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][8]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][7]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][7]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][6]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][6]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][5]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][5]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][4]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][4]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][3]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][3]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][2]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][2]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][1]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][1]['srCount']), 0); ?>"],
            ["<?php echo DateTime::createFromFormat('!m', $page['total_sr'][0]['monthReport'])->format('M')  ?>","<?php echo_isset(number_format($page['total_sr'][0]['srCount']), 0); ?>"]
        ],
        datasets: [{ //total customers
            type: 'line',
            lineTension: 0,
            fill: false,
            borderColor: '#327782',
            borderWidth: 2,
            pointBackgroundColor: '#FFF',
            pointBorderColor: '#327782',
            pointBorderWidth: 2,
            pointRadius: 5,
            data: [
                <?php echo_isset(number_format($page['total_sr'][11]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][10]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][9]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][8]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][7]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][6]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][5]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][4]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][3]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][2]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][1]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][0]['srCount']), 0); ?>
            ] 
        },
        { //total customers
            type: 'bar',
            label: 'total calls',
            lineTension: 0,
            backgroundColor: 'transparent',
            data: [
                <?php echo_isset(number_format($page['total_sr'][11]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][10]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][9]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][8]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][7]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][6]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][5]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][4]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][3]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][2]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][1]['srCount']), 0); ?>,
                <?php echo_isset(number_format($page['total_sr'][0]['srCount']), 0); ?>
            ] 
        }
	]},
    options: {
        events: false,
        showToolTips: false,
        legend: { 
            display: false/*, 
            position: 'bottom'*/
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: 0,
                    max: 50,
                    stepSize: 10 ,
                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    display: true,
                    color: '#EBEDED'
                }
            }],
            xAxes: [{
                ticks: {
                	beginAtZero:true,

                    fontFamily: 'avenirMed',
                    fontsize: '8pt',
                    fontColor: '#808080'
                },
                gridLines: {
                    color: '#EBEDED'
                }
                /*display: false */
            }]
        }
    }
    });    

</script>