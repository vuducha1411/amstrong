<div class="text-left sectionheader">Growth Channel (Grab)</div>
<div>
	<canvas id="growthgrab"></canvas>
</div>     
<?php 
    if($this->session->userData('infographicViewType')=='country'){
        $stepSize = ceil(($page['max_growth_grab']+3000)/5);
        $maxVal = $page['max_growth_grab']+3000;
    }else{
        $stepSize = ceil(($page['max_growth_grab']+1000)/5);
        $maxVal = $page['max_growth_grab']+1000;
    }
?>

<center>
	<span id="prevYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year'] - 1?>-->PY</span>
	<span id="currYr"></span><span class="tfogrowth-text font-size-sm"><!--<?php echo $page['year']?>-->CY</span>
</center>           
 
<script>
	var ctx = document.getElementById("growthgrab");
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: [<?php echo $page['grip_category']; ?>],
	        datasets: [{
	        	label: "PY",
	            data: [<?php echo $page['grab_prev']; ?>],
	            backgroundColor: 'rgb(255, 139, 133)',
	            borderColor: 'rgb(255, 139, 133)',
	            borderWidth: 1
	        },{
	            label: "CY",
	            data: [<?php echo $page['grab_curr']; ?>],
	            backgroundColor: 'rgb(75,213,226)',
	            borderColor: 'rgb(75,213,226)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	        events: false,
	        showToolTips: false,
	    	responsive: true,
	    	legend: {
	    		display: false,
	    		position: 'bottom'
	    	},
	        scales: {
	            xAxes: [{
		        	barPercentage: 0.75,
		        	categorySpacing: 2.5,
	                gridLines: {
	                    display: false
	                },
                    ticks: {
                        autoSkip: false,
                        minRotation: 0,
                        maxRotation: 0
                    }
		        }],
		        yAxes: [{
	                ticks: { 	
	                    fontFamily: 'avenirMed',
	                    fontSize: 15,
	                    fontColor: '#327782',	                	
	                	stepSize: <?php echo $stepSize; ?>,
                        max: <?php echo $maxVal ?>,
	                    beginAtZero:true,
	                    userCallback: function(value, index, values) {
	                        // Convert the number to a string and splite the string every 3 charaters from the end
	                        value = value.toString();
	                        value = value.split(/(?=(?:...)*$)/);

	                        // Convert the array to a string and format the output
	                        value = value.join(',');
	                        return value;
	                    }
	                },
		            gridLines: {
	                    display: false
		            }
		        }]
	        },
	        tooltips: {
              callbacks: {
                title: function(tooltipItem, data) {
                    return '';
                },
                label: function(tooltipItem, data) {
                    
                    if(tooltipItem.index==data.datasets[tooltipItem.datasetIndex].data.length-1){
                       //throw '';
                    
                        //Chart.defaults.global.tooltips.enabled = false;

                        return '';
                    }else{
                         return data.datasets[tooltipItem.datasetIndex].label + ": " +  data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    }
                  }
               }
              
            },
	        animation: {
	        onComplete: function () {
	            // render the value of the chart above the bar
	             //alert('aasa');
	            var ctx = this.chart.ctx;
	            ctx.font = Chart.helpers.fontString(16, 'bold', 'avenir');
	            ctx.fillStyle = '#327782';
	            ctx.textAlign = 'center';
	            ctx.textBaseline = 'bottom';
	            var dsetcount = 0;
	            this.data.datasets.forEach(function (dataset) {
                  for (var i = 0; i < dataset.data.length; i++) {
                      ctx.fillStyle = '#327782';
                      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                      ctx.fillText(dataset.data[i].toLocaleString('en'), model.x, model.y);
                  }
	            });
	        }}
	    }
	});

	document.getElementById("growthgrab").onclick = function(evt){
		var activePoints = myChart.getElementsAtEvent(evt);
		var firstPoint = activePoints[1];
		var label = myChart.data.labels[firstPoint._index];
		var value = myChart.data.datasets[firstPoint._datasetIndex].label[firstPoint._index];
		if (firstPoint !== undefined){
			if (label == "OTHER"){
				$('#modelWindowGrab').modal('show');	
				document.getElementById('classId').value = label;
                console.log(myChart.data.datasets[firstPoint._datasetIndex].label);
				//alert(document.getElementById('classId').value);
			}
		}
	};

	$('#closemodal').click(function() {
	    $('#getCodeModal').modal('hide');
	});
</script>


<div class="modal2 fade" id="modelWindowGrab" role="dialog" style="overflow:auto; min-height: 30%">
    <div class="modal-body" style="background-color: #fff;">
        <div id="header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="sectionheader">Growth Channel (Grab)</p>
        </div>
        <div id="content" class="text-center cf" style="padding-bottom: 30px; padding-top: 20px; font-family: 'avenir'">
            <div id="totalcallshistory">
	            <table class="font-size-sm" style="margin: auto;">
	                <tr style="border: 1px solid #808080">
	                    <td style="width: 20%; padding-left: 5px; border-right: 1px solid #808080; background-color: #EBEDED"> Country Channel </td>
	                    <td class="tfogrowth-text" style="text-align: right; padding-right: 5px; width: 5%; border-right: 1px solid #808080; background-color: rgb(255, 139, 133)"> <!--<?php echo $page['year']-1; ?>-->PY</td>
	                    <td class="tfogrowth-text" style="text-align: right; padding-right: 5px; width: 5%; background-color: rgb(75,213,226)"> <!--<?php echo $page['year']; ?>-->CY</td>

	                </tr>
                    <?php
                        foreach($this->data['page']['channelNames'] as $key => $value){
                            echo "<tr class=\"growthChannelTable\"> <td style=\"width: 20%; padding-left: 5px; border-right: 1px solid #808080; \">" . $value . "</td>";
                            $valC = 0;
                            $valP = 0;
                            foreach($this->data['page']['otherChannelsPrev'] as $prev){
                                if ($prev['countryChannelsName'] == $value)
                                    $valP = $prev['totalGrab'];
                            }

                            foreach($this->data['page']['otherChannels'] as $curr){
                                if ($curr['countryChannelsName'] == $value)
                                    $valC = $curr['totalGrab'];
                            }
                            echo "<td style=\"text-align: right; padding-right: 5px; width: 5%; border-right: 1px solid #808080; font-family: 'avenirBold'\">" . $valP . "</td>";
                            echo "<td style=\"text-align: right; padding-right: 5px; width: 5%; font-family: 'avenirBold'\">" . $valC . "</td></tr>";
                        }
                     ?>
	            </table>
            </div>         
        </div>
    </div>
</div>