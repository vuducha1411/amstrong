<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>SALES performance</p>
	</div>
	<?php
	    if (!intval($infographic['salesTarget']))
	    {
	        $percent = '100';
	    }
	    else
	    {
	        $percent = (intval($infographic['salesAchieved']) / intval($infographic['salesTarget'])) * 100;
	    }
	?>

	<div id="content" class="text-center">

	    <div id="content1">
	        <div class="maincontent">
	            <div class="text1">
	                <h1>SALES ACHIEVED<h1>
	                <h2><?php echo $this->country_currency . ' '. number_format($infographic['salesAchieved']) ?></h2>
	                <!-- <h3><?php //echo number_format($percent) ?>% </h3> -->
	            </div>
	            <!-- <div><p>/</p></div>
	            <div class="text2">
	                <h1>SALES TARGET</h1>
	                <h2>$<?php echo number_format($infographic['salesTarget']) ?></h2>
	            </div> -->
	        </div>
	    </div>

	</div>
</div>