<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Call Data</p>
	</div>
	<div id="content" class="text-center">

	    <div id="contanttotal">
	        <div class="ct_total total1">
	            <h1>A</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmA']) ? $infographic['timesCallOtmA'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total2">
	            <h1>B</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmB']) ? $infographic['timesCallOtmB'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total3">
	            <h1>C</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmC']) ? $infographic['timesCallOtmC'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total4">
	            <h1>D</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmD']) ? $infographic['timesCallOtmD'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
			<!-- <div class="ct_total total5">
	            <h1>D</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php// echo number_format($infographic['timesCallOtmD']) ?></h3>
	            <h4>CALLS</h4>
	        </div> -->
            <div class="ct-un-total-full">
                Unclassified OTM has a total of <?php echo isset($infographic['timesCallOtmU']) ? $infographic['timesCallOtmU'] : 0 ?> Calls
            </div>
	        <div class="imgtotal">
	            <img src="<?php echo site_url('res/img/infographic/total5.png'); ?>"/>
	        </div>
	        <p><?php echo isset($infographic['totalCall']) ? $infographic['totalCall'] : 0 ?></p>
	        <div class="ct_total_text">CALLS IN TOTAL</div>
	    </div>

	</div>
</div>