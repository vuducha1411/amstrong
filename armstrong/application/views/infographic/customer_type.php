<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p><?php echo (isset($isssd) && $isssd)  ? 'SSD' : 'TFO' ?> BY CHANNEL</p>
	</div>
	<div id="content" style="width: 900px; padding-bottom: 30px;">
		<div class="contenttype">
			<div class="contentsidebar ctsidebar1">
				<div class="typetext">
					<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][0]['typeName']); ?></p>
				</div>
				<div class="rightcontenttype">
					<svg class="bar1" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect1" x="10" y="10" height="33" width="200" style="fill: #D91F5D"/>
						</svg>
						<svg id="modalpolygon1" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#D91F5D" />

						</svg>
						<svg id="textmodalrect" y="10">
							<text x="95" y="22" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][0]['quantity'],0)); ?>
							</text>
						</svg>
						<svg id="textmodalmodalrect11" y="18">
							<text x="15" y="15" fill="#ffffff" font-size="18" font-family="mohave">Total TFO</text>
						</svg>
					</svg>
					<svg class="bar2" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect2" x="10" y="10" height="33" width="200" style="fill: #8EC641"/>
						</svg>
						<svg id="modalpolygon2" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#8EC641" />
						</svg>
						<svg id="textmodalrect2" y="18">
							<text x="0" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::TYPE]['top3'][0]['valueData'],2)); ?>
							</text>
						</svg>
						<svg id="textmodalrect11" y="18">
							<text x="15" y="15" fill="#ffffff" font-size="18" font-family="mohave">Total Value</text>
						</svg>
					</svg>

				</div>
			</div>
			<div class="contentsidebar ctsidebar2">
				<div class="typetext">
					<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][1]['typeName']); ?></p>
				</div>
				<div class="rightcontenttype">
					<svg class="bar3" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect3" x="10" y="10" height="33" width="200" style="fill: #D91F5D"/>
						</svg>
						<svg id="modalpolygon3" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#D91F5D" />
						</svg>
						<svg id="textmodalrect3" y="18">
							<text x="70" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][1]['quantity'],0)); ?>
							</text>
						</svg>
					</svg>
					<svg class="bar4" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect4" x="10" y="10" height="33" width="200" style="fill: #8EC641"/>
						</svg>
						<svg id="modalpolygon4" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#8EC641" />
						</svg>
						<svg id="textmodalrect4" y="18">
							<text x="0" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::TYPE]['top3'][1]['valueData'],2)); ?>
							</text>
						</svg>
					</svg>

				</div>
			</div>
			<div class="contentsidebar ctsidebar3">
				<div class="typetext">
					<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][2]['typeName']); ?></p>
				</div>
				<div class="rightcontenttype">
					<svg class="bar5" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect5" x="10" y="10" height="33" width="200" style="fill: #D91F5D"/>
						</svg>
						<svg id="modalpolygon5" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#D91F5D" />
						</svg>
						<svg id="textmodalrect5" y="18">
							<text x="70" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][2]['quantity'],0)); ?>
							</text>
						</svg>
					</svg>
					<svg class="bar6" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect6" x="10" y="10" height="33" width="200" style="fill: #8EC641"/>
						</svg>
						<svg id="modalpolygon6" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#8EC641" />
						</svg>
						<svg id="textmodalrect6" y="18">
							<text x="15" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::TYPE]['top3'][2]['valueData'],2)); ?>
							</text>
						</svg>
					</svg>
				</div>
			</div>
			<div class="contentsidebar ctsidebar4">
				<div class="typetext">
					<p>Others</p>
				</div>
				<div class="rightcontenttype">
					<svg class="bar7" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect7" x="10" y="10" height="33" width="200" style="fill: #D91F5D"/>
						</svg>
						<svg id="modalpolygon7" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#D91F5D" />
						</svg>
						<svg id="textmodalrect7" y="18">
							<text x="70" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['other']['totalTfo'],0)); ?>
							</text>
						</svg>
					</svg>
					<svg class="bar8" style="width:100%; height:50px" >
						<svg x="0">
							<rect id="modalrect8" x="10" y="10" height="33" width="200" style="fill: #8EC641"/>
						</svg>
						<svg id="modalpolygon8" y="10">
							<polygon points="0 0,0 0,0 33,20 33" style="fill:#8EC641" />
						</svg>
						<svg id="textmodalrect8" y="18">
							<text x="0" y="15" fill="#ffffff" font-size="18" font-family="mohave">
								<?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::TYPE]['other']['totalValue'],2)); ?>
							</text>
						</svg>
					</svg>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$tfo = array(
	$infographic[Infographic_model::TYPE]['top3'][0]['quantity'],
	$infographic[Infographic_model::TYPE]['top3'][1]['quantity'],
	$infographic[Infographic_model::TYPE]['top3'][2]['quantity'],
	$infographic[Infographic_model::TYPE]['other']['totalTfo'],
);
$revenue = array(
	$infographic[Infographic_model::TYPE]['top3'][0]['valueData'],
	$infographic[Infographic_model::TYPE]['top3'][1]['valueData'],
	$infographic[Infographic_model::TYPE]['top3'][2]['valueData'],
	$infographic[Infographic_model::TYPE]['other']['totalValue'],
);
?>
<script>

	$(document).ready(function()
	{
		// customer type - line chart
		var maxWidth = 740;
		var maxValue = <?php echo_isset(number_format(max($revenue), 0, '', ''));?>;
		var maxTfo = <?php echo_isset(number_format(max($tfo), 0, '', ''));?>;
		var width = 0;

		var modalrect1 = $("#modalrect1");
		var modalpolygon1 = $("#modalpolygon1");
		var textmodalrect = $("#textmodalrect");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[0],0, '', '')); ?>);
		setWidth(modalrect1, modalpolygon1, textmodalrect,width);

		var modalrect2 = $("#modalrect2");
		var modalpolygon2 = $("#modalpolygon2");
		var textmodalrect2 = $("#textmodalrect2");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[0],0, '', '')); ?>);
		setWidth(modalrect2, modalpolygon2,textmodalrect2, width);

		var modalrect3 = $("#modalrect3");
		var modalpolygon3 = $("#modalpolygon3");
		var textmodalrect3 = $("#textmodalrect3");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[1],0, '', '')); ?>);
		setWidth(modalrect3, modalpolygon3,textmodalrect3, width);

		var modalrect4 = $("#modalrect4");
		var modalpolygon4 = $("#modalpolygon4");
		var textmodalrect4 = $("#textmodalrect4");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[1],0, '', '')); ?>);
		setWidth(modalrect4, modalpolygon4,textmodalrect4, width);

		var modalrect5 = $("#modalrect5");
		var modalpolygon5 = $("#modalpolygon5");
		var textmodalrect5 = $("#textmodalrect5");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[2],0, '', '')); ?>);
		setWidth(modalrect5, modalpolygon5,textmodalrect5, width);

		var modalrect6 = $("#modalrect6");
		var modalpolygon6 = $("#modalpolygon6");
		var textmodalrect6 = $("#textmodalrect6");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[2],0, '', '')); ?>);
		setWidth(modalrect6, modalpolygon6,textmodalrect6, width);

		var modalrect7 = $("#modalrect7");
		var modalpolygon7 = $("#modalpolygon7");
		var textmodalrect7 = $("#textmodalrect7");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[3],0, '', '')); ?>);
		setWidth(modalrect7, modalpolygon7,textmodalrect7, width);

		var modalrect8 = $("#modalrect8");
		var modalpolygon8 = $("#modalpolygon8");
		var textmodalrect8 = $("#textmodalrect8");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[3],0, '', '')); ?>);
		setWidth(modalrect8, modalpolygon8,textmodalrect8, width);
	});

	function setWidth(modalrect, polygol, text, width) {
		modalrect.attr("width",width + 0.45);
		polygol.attr("x", width + 10);

        var text_width = text[0].getBoundingClientRect().width;
		text.attr("x", width - (text_width + 5));
        text[0].children[0].setAttribute("x", 0);
	}

	function calculateWidth(maxWidth, maxValue, value) {
		var width = 0;
		width = (value/maxValue) * maxWidth;

		return width;
	}

</script>