<?php
if (isset($infographic[Infographic_model::CALL_RECORD]['totalActualWorking'])) {
    if ($infographic[Infographic_model::CALL_RECORD]['totalCall'] > 0 && $infographic[Infographic_model::CALL_RECORD]['totalActualWorking']) {
        $averageCallsActualDay = $infographic[Infographic_model::CALL_RECORD]['totalCall'] / $infographic[Infographic_model::CALL_RECORD]['totalActualWorking'];
    } else {
        $averageCallsActualDay = 0;
    }
} else {
    $averageCallsActualDay = 0;
}

if (array_sum(array_only($infographic[Infographic_model::CALL_RECORD], ['minuteCallOtmA', 'minuteCallOtmB', 'minuteCallOtmC', 'minuteCallOtmD', 'minuteCallOtmU'])) > 0 && ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'] * 8 * 60 * $infographic[Infographic_model::CALL_RECORD]['workingDay']) > 0) {
    $incallTime = array_sum(array_only($infographic[Infographic_model::CALL_RECORD], ['minuteCallOtmA', 'minuteCallOtmB', 'minuteCallOtmC', 'minuteCallOtmD', 'minuteCallOtmU'])) / ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'] * 8 * 60 * $infographic[Infographic_model::CALL_RECORD]['workingDay']) * 100;
} else {
    $incallTime = 0;
}

if ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels']) {
    if ($infographic[Infographic_model::CALL_RECORD]['totalActualWorking'] > 0 && $infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels']) {
        $averageActualWorkingDay = $infographic[Infographic_model::CALL_RECORD]['totalActualWorking'] / $infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'];
    } else {
        $averageActualWorkingDay = 0;
    }
} else {
    $averageActualWorkingDay = 0;
}
?>
<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Call Data</p>
	</div>
	<div id="content" class="text-center">
        <div class="average-call" style="overflow:hidden;">
            <ul class="mn_cd cf">
                <li class="mn_call">
                    <div class="tablecall1">
                        <p><?php  echo_isset(number_format($averageCallsActualDay), 0); ?></p>
                    </div>
                    <h1>total average calls<br/> /actual working day /SR</h1>
                </li>
                <li class="mn_call">
                    <div class="tablecall2">
                        <p><?php
                            echo_isset(number_format($incallTime), 0);
                            ?>%</p>
                    </div>
                    <h1>% of time spent with customers in call</h1>
                </li>
                <li class="mn_call">
                    <div class="tablecall2">
                        <p><?php
                            echo_isset(number_format($averageActualWorkingDay), 0);
                            ?></p>
                    </div>
                    <h1>average actual<br/>working day / month</h1>
                </li>
            </ul>
        </div>

        <div id="contentcalldata">
            <ul class="mn_ct">
                <li class="mn_tb table1">
                    <h1>
                        <?php
                        $minsA = 0;
                        if(isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'])){
                            if($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'] > 0){
                                $minsA = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'];
                            }
                        }
                        echo_isset(number_format($minsA, 0, '', ''), 0)
                        ?></h1>
                    <h2>MINS</h2>
                    <h3>AVERAGE CALL TIME</h3>
                    <p>OTM A</p>
                    <div class="otma">

                    </div>
                </li>
                <li class="mn_tb table2">
                    <h1><?php
                        $minsB = 0;
                        if(isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'])){
                            if($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'] > 0){
                                $minsB = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'];
                            }
                        }
                        echo_isset(number_format($minsB, 0, '', ''), 0)
                        ?></h1>
                    <h2>MINS</h2>
                    <h3>AVERAGE CALL TIME</h3>
                    <p>OTM B</p>
                    <div class="otma">

                    </div>
                </li>
                <li class="mn_tb table3">
                    <h1><?php
                        $minsC = 0;
                        if(isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'])){
                            if($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'] > 0){
                                $minsC = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'];
                            }
                        }
                        echo_isset(number_format($minsC, 0, '', ''), 0)
                        ?></h1>
                    <h2>MINS</h2>
                    <h3>AVERAGE CALL TIME</h3>
                    <p>OTM C</p>
                    <div class="otma">

                    </div>
                </li>
                <li class="mn_tb table4">
                    <h1><?php
                        $minsD = 0;
                        if(isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'])){
                            if($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'] > 0){
                                $minsD = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'];
                            }
                        }
                        echo_isset(number_format($minsD, 0, '', ''), 0)
                        ?></h1>
                    <h2>MINS</h2>
                    <h3>AVERAGE CALL TIME</h3>
                    <p>OTM D</p>
                    <div class="otma">

                    </div>
                </li>
                <!-- <li class="mn_tb table5">
	                <h1><?php// echo number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'], 0, '', '') ?></h1>
	                <h2>MINS</h2>
	                <h3>AVERAGE CALL TIME</h3>
	                <p>OTM D</p>
	                <div class="otma">

	                </div>
	            </li> -->
            </ul>
        </div>
	</div>
</div>