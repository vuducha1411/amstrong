<div class="main">
<div class="header">
	<div class="textbsheader">
        <?php $dt = DateTime::createFromFormat('!m', $this->session->userdata('infographicMonth')); ?>
        <a href="<?php echo site_url('/') ?>"><?php echo $country_name; ?> - PULL <?php echo_isset($dt->format('M') . ', ' . $this->session->userdata('infographicYear')) ?></a>
    </div>
</div>
<div class="content">
<div id="bscontantmalaypd">
<div class="leftbscontantmalaypd">
<div class="leftbscontantmalaypdtable1">
	<a class="textbstopsku" href="<?= site_url('infographic/customer_data'); ?>" data-toggle="ajaxModal">Customer Data</a>
	<div class="bscustomerdata">
		<div class="leftbscustomerdata">
			<div class="txtleftbscustomerdata1">
				<p class="textp1"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][0]['value']), 0) ?></p>
				<p class="txtp1">total customers</p>
			</div>
			<div class="txtleftbscustomerdata2">
				<p class="textp2"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][1]['value']), 0) ?></p>
				<p class="txtp2">total customers</p>
			</div>
			<div class="txtleftbscustomerdata3">
				<p class="textp3"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][2]['value']), 0) ?></p>
				<p class="txtp3">total customers</p>
			</div>
			<div class="txtleftbscustomerdata4">
				<p class="textp4"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['value']), 0) ?></p>
				<p class="txtp4">total customers</p>
			</div>
			<!-- <div class="txtleftbscustomerdata5_t1">
				<p class="textp5_t1"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['value']), 0) ?></p>
				<p class="txtp5_t1">total customers</p>
			</div> -->
		</div>
		<div class="centerbscustomerdata">
			<p><?php echo_isset($infographic[Infographic_model::DATA]['otm']['otm'][0]['label']) ?></p>
			<p><?php echo_isset($infographic[Infographic_model::DATA]['otm']['otm'][1]['label']) ?></p>
			<p><?php echo_isset($infographic[Infographic_model::DATA]['otm']['otm'][2]['label']) ?></p>
			<p><?php echo_isset($infographic[Infographic_model::DATA]['otm']['otm'][3]['label']) ?></p>
			<!-- <p>UN</p> -->
		</div>
		<div class="rightbscustomerdata">
			<div class="txtrightbscustomerdata1">
				<p class="textp5"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][0]['percentage'], 0), 0) ?>%</p>
				<p class="txtp5">of<br />total customers</p>
			</div>
			<div class="txtrightbscustomerdata2">
				<p class="textp6"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][1]['percentage'], 0), 0) ?>%</p>
				<p class="txtp6">of<br />total customers</p>
			</div>
			<div class="txtrightbscustomerdata3">
				<p class="textp7"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][2]['percentage'], 0), 0) ?>%</p>
				<p class="txtp7">of<br />total customers</p>
			</div>
			<div class="txtrightbscustomerdata4">
				<p class="textp8"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['percentage'], 0), 0) ?>%</p>
				<p class="txtp8">of<br />total customers</p>
			</div>
			<!-- <div class="txtrightbscustomerdata5_t2">
				<p class="textp5_t2"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['percentage'], 0), 0) ?>%</p>
				<p class="txtp5_t2">of<br />total customers</p>
			</div> -->
		</div>
	</div>
	<p class="textbottomcustomerdata">
        <span class="txtrightbscustomerdata-un">Unclassified OTM Total Customers: <?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['value']), 0) ?> (<?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['percentage'], 0), 0) ?>%)</span>
        <br><br>
        <?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['total'])) ?> Total Customers
    </p>
</div>
<!------------------>
<div class="leftbscontantmalaypdtable2">
	<a class="textbssaledperformance" href="<?= site_url('infographic/customer_type'); ?>" data-toggle="ajaxModal">TFO BY CHANNEL TYPE</a>
	<div class="tbsalesperformance">
		<div class="customertype-chartcontent">
			<div class="ct-text">
				<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][0]['typeName']); ?></p>
			</div>
			<div class="ct-rightchart">
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart1" x="10" y="7" height="20" width="200" style="fill: #D91F5D"/>
					</svg>
					<svg id="polygon1" y="7">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D" />
					</svg>
					<svg id="textrect" y="12">
						<text x="100" y="10" fill="#ffffff" text-anchor="end" font-size="12" font-family="mohave">
							<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][0]['quantity'],0)); ?>
						</text>
					</svg>
					<svg id="textrect11" y="12">
						<text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">Total Orders</text>
					</svg>
				</svg>
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart2" x="10" y="5" height="20" width="200" style="fill: #8EC641"/>
					</svg>
					<svg id="polygon2" y="5">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641" />
					</svg>
					<svg id="textrect2" y="10">
						<text x="40" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							€<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][0]['valueData'],2)); ?>
						</text>
					</svg>
					<svg id="textrect11" y="10">
						<text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">Total Value</text>
					</svg>
				</svg>

			</div>
		</div>
		<div class="customertype-chartcontent">
			<div class="ct-text">
				<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][1]['typeName']); ?></p>
			</div>
			<div class="ct-rightchart">
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart3" x="10" y="7" height="20" width="200" style="fill: #D91F5D"/>
					</svg>
					<svg id="polygon3" y="7">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D" />
					</svg>
					<svg id="textrect3" y="12">
						<text x="70" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][1]['quantity'],0)); ?>
						</text>
					</svg>
				</svg>
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart4" x="10" y="5" height="20" width="200" style="fill: #8EC641"/>
					</svg>
					<svg id="polygon4" y="5">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641" />
					</svg>
					<svg id="textrect4" y="10">
						<text x="30" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							€<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][1]['valueData'],2)); ?>
						</text>
					</svg>
				</svg>

			</div>
		</div>
		<div class="customertype-chartcontent">
			<div class="ct-text">
				<p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][2]['typeName']); ?></p>
			</div>
			<div class="ct-rightchart">
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart5" x="10" y="7" height="20" width="200" style="fill: #D91F5D"/>
					</svg>
					<svg id="polygon5" y="7">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D" />
					</svg>
					<svg id="textrect5" y="12">
						<text x="70" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][2]['quantity'],0)); ?>
						</text>
					</svg>
				</svg>
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart6" x="10" y="5" height="20" width="200" style="fill: #8EC641"/>
					</svg>
					<svg id="polygon6" y="5">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641" />
					</svg>
					<svg id="textrect6" y="10">
						<text x="40" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							€<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][2]['valueData'],2)); ?>
						</text>
					</svg>
				</svg>
			</div>
		</div>
		<div class="customertype-chartcontent">
			<div class="ct-text">
				<p>OTHERS</p>
			</div>
			<div class="ct-rightchart">
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart7" x="10" y="7" height="20" width="200" style="fill: #D91F5D"/>
					</svg>
					<svg id="polygon7" y="7">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D" />
					</svg>
					<svg id="textrect7" y="12">
						<text x="70" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['other']['totalTfo'],0)); ?>
						</text>
					</svg>
				</svg>
				<svg style="width:100%; height:27px;" >
					<svg x="0">
						<rect id="linechart8" x="10" y="5" height="20" width="200" style="fill: #8EC641"/>
					</svg>
					<svg id="polygon8" y="5">
						<polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641" />
					</svg>
					<svg id="textrect8" y="10">
						<text x="30" y="10" fill="#ffffff" font-size="12" font-family="mohave">
							€<?php echo_isset(number_format($infographic[Infographic_model::TYPE]['other']['totalValue'],2)); ?>
						</text>
					</svg>
				</svg>
			</div>
		</div>
	</div>
</div>
<!---------------->
<?php
$averageCalls = number_format(($infographic[Infographic_model::CALL_RECORD]['totalCall'] / $infographic[Infographic_model::CALL_RECORD]['workingDay']));
?>
<div class="leftbscontantmalaypdtable3">
	<a class="textbsrichmedia">call data</a>
	<a href="<?= site_url('infographic/call_data_3'); ?>" data-toggle="ajaxModal">
		<div class="bsleftcalldata">
			<div id="bsleftcalldatatable1">
				<div id="imgcalldatatable1">
					<p><?php echo_isset($averageCalls, 0); ?></p>
				</div>
				<h1>total average calls<br /> /work day</h1>
			</div>
			<div id="bsleftcalldatatable2">
				<div id="imgcalldatatable1">
					<p><?php echo_isset(round(($averageCalls / $infographic[Infographic_model::CALL_RECORD]['totalSalespersons']), 0)) ?></p>
				</div>
				<h1>average calls<br />/work day/sr</h1>
			</div>
		</div>
	</a>
	<a href="<?= site_url('infographic/call_data_1'); ?>" data-toggle="ajaxModal">
		<div class="bscalldata">
			<div id="bscontentcalldata">
				<ul class="bsmn_ct">
					<li class="mn_bstb bstable1">
						<h1><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>OTM A</p>
						<div class="bsotma">

						</div>
					</li>
					<li class="mn_bstb bstable2">
						<h1><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>OTM B</p>
						<div class="bsotma">

						</div>
					</li>
					<li class="mn_bstb bstable3">
						<h1><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>OTM C</p>
						<div class="bsotma">

						</div>
					</li>
					<li class="mn_bstb bstable4">
						<h1><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>OTM D</p>
						<div class="bsotma">

						</div>
					</li>
					<li class="mn_bstb bstable5">
						<h1><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>OTM D</p>
						<div class="bsotma">

						</div>
					</li>
				</ul>
			</div>
		</div>
	</a>
</div>

</div>
<!---------------->
<div class="rightbscontantmalaypd">
<div class="rightbscontantmalaypdtable1">
	<a class="textbssalesperformance1">call data</a>

	<div id="rightbscalldata1">
		<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/call_data_total') ?>">
			<div id="contantbstotal">
				<div class="ct_bstotal bstotal1">
					<h1>A</h1>
					<h2>HAS A TOTAL OF</h2>
					<h3><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA']), 0) ?></h3>
					<h4>CALLS</h4>
				</div>
				<div class="ct_bstotal bstotal2">
					<h1>B</h1>
					<h2>HAS A TOTAL OF</h2>
					<h3><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB']), 0) ?></h3>
					<h4>CALLS</h4>
				</div>
				<div class="ct_bstotal bstotal3">
					<h1>C</h1>
					<h2>HAS A TOTAL OF</h2>
					<h3><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC']), 0) ?></h3>
					<h4>CALLS</h4>
				</div>
				<div class="ct_bstotal bstotal4">
					<h1>D</h1>
					<h2>HAS A TOTAL OF</h2>
					<h3><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD']), 0) ?></h3>
					<h4>CALLS</h4>
				</div>
				<div class="ct_bstotal bstotal5">
				<h1>D</h1>
				<h2>HAS A TOTAL OF</h2>
				<h3><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD']), 0) ?></h3>
				<h4>CALLS</h4>
			</div>
				<div class="imgbstotal">
					<img src="<?php echo site_url('res/img/infographic/total5.png'); ?>"/>
				</div>
				<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCall'])) ?></p>
				<a>CALLS IN TOTAL</a>
			</div>
		</a>
	</div>
	
	<div id="rightbscalldata2">
		<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/call_data_2') ?>">
			<div class="mn_bsdata1 bstabledata1">
				<h1>OTM A</h1>
				<h2>call frequency</h2>
				<div class="bsnumber">
					<p><?php echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA']), 1) ?></p>
				</div>
			</div>
			<div class="mn_bsdata1 bstabledata2">
				<h1>OTM B</h1>
				<h2>call frequency</h2>
				<div class="bsnumber">
					<p><?php echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA']), 1) ?></p>
				</div>
			</div>
			<div class="mn_bsdata1 bstabledata3">
				<h1>OTM C</h1>
				<h2>call frequency</h2>
				<div class="bsnumber">
					<p><?php echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA']), 1) ?></p>
				</div>
			</div>
			<div class="mn_bsdata1 bstabledata4">
				<h1>OTM D</h1>
				<h2>call frequency</h2>
				<div class="bsnumber">
					<p><?php echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA']), 1) ?></p>
				</div>
			</div>
			<div class="mn_bsdata1 bstabledata5">
			<h1>OTM D</h1>
			<h2>call frequency</h2>
			<div class="bsnumber">
				<p><?php echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA']), 1) ?></p>
			</div>
		</div>
		</a>
	</div>
</div>
<div class="rightbscontantmalaypdtable2">
	<a class="textbssalesteam">call data</a>
	<?php
		$strikeRate = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] / $infographic[Infographic_model::CALL_RECORD]['totalCall'] * 100;
	?>
	<div class="calldata-piechart">
		<div id="calldata-chart" data-dimension="260" data-width="50" data-fontsize="15" data-percent="65" data-fgcolor="#2BACE2" data-bgcolor="#818285" data-animationstep="0">
		</div>
		<div class="calldata-rightchartinfo">
			<div class="calldata-toppie">
				<div class="calldata-leftctpie">
					<p class="calldata-textleftctpie1">TOTAL CALLS</p>
					<p class="calldata-textleftctpie2">CALLS WITH ORDERS</p>
					<p class="calldata-textleftctpie3">CALLS WITHOUT ORDERS</p>
				</div>
				<div class="calldata-rightctpie">
					<p class="calldata-textrightctpie1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCall'])) ?></p>
					<p class="calldata-textrightctpie2"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>
					<p class="calldata-textrightctpie3"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
				</div>
			</div>
			<div class="calldata-bottompie">
				<div class="calldata-leftbottompie">
					<p class="calldata-textleftbtpie1"><?php echo_isset(number_format($strikeRate, 0)); ?>% <!-- <span>/</span> --></p>
					<p class="calldata-textleftbtpie2">STRIKE RATE</p>
				</div>
				<!-- <div class="calldata-rightbottompie">
					<p class="calldata-textleftrightpie1">50%</p>
					<p class="calldata-textleftrightpie2">TARGET</p>
				</div> -->
			</div>
		</div>

		<div class="calldata-abtext1" id="abtext1">
			<p class="calldata-abtextcall">CALLS WITHOUT ORDERS</p>
			<p class="calldata-abnumcall"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
		</div>
		<div class="calldata-abtext2" id="abtext2">
			<p class="calldata-abtextcall1">CALLS WITH ORDERS</p>
			<p class="calldata-abnumcall1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>
		</div>
		<div class="calldata-abtext3">
			<p><span id="piechart-percent"><?php echo_isset(number_format($strikeRate, 0)); ?>%</span><br/>STRIKE RATE</p>
		</div>
	</div>

	<!-- growth chart -->
	<?php
		$otmAPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] / $infographic[Infographic_model::DATA]['customerOtmA'] * 100;
		$otmBPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] / $infographic[Infographic_model::DATA]['customerOtmB'] * 100;
		$otmCPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] / $infographic[Infographic_model::DATA]['customerOtmC'] * 100;
		$otmDPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] / $infographic[Infographic_model::DATA]['customerOtmD'] * 100;
	?>
	<div id="growthchart">
		<div class="growthchart-chartbar12">
			<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/grow_chart') ?>">
				<div class="growthchart-chartindex1">
					<img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50" height="59"/>
					<p id="growthchart-textct1"><?php echo_isset(number_format($otmAPercent, 0)); ?>%</p>
					<svg id="growthchart-index1" width="150" height="106">
					</svg>
					<p class="growthchart-idex1textchart1"> target is 100%</p>
					<p class="growthchart-idex1textchart2"> OTM A coverag</p>
					<div class="growthchart-index1textchart3">
						<div class="growthchart-leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="growthchart-centerindex1">
							<p>/</p>
						</div>
						<div class="growthchart-rightindex1tc2">							
                            <p><?php echo_isset(number_format($infographic[Infographic_model::DATA]['customerOtmA'], 0, '', '')); ?></p>
                            <a>total customers</a>
						</div>
					</div>
				</div>
			</a>
			<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/grow_chart') ?>">
				<div class="growthchart-chartindex2">
					<img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50" height="59"/>
					<p id="growthchart-textct2"><?php echo_isset(number_format($otmBPercent, 0)); ?>%</p>
					<svg id="growthchart-index2" width="150" height="106">
					</svg>
					<p class="growthchart-index2textchart1"> target is 100%</p>
					<p class="growthchart-index2textchart2"> OTM B coverage</p>
					<div class="growthchart-index1textchart3">
						<div class="growthchart-leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="growthchart-centerindex1">
							<p>/</p>
						</div>
						<div class="growthchart-rightindex1tc2">							
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::DATA]['customerOtmB'], 0, '', '')); ?> </p>
                            <a>total customers</a>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="growthchart-chartbar34">
			<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/grow_chart') ?>">
				<div class="growthchart-chartindex3">
					<img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50" height="59"/>
					<p id="growthchart-textct3"><?php echo_isset(number_format($otmCPercent, 0)); ?>%</p>
					<svg id="growthchart-index3" width="150" height="106">
					</svg>
					<p class="growthchart-idex3textchart2"> OTM c coverage</p>
					<div class="growthchart-index3textchart3">
						<div class="growthchart-leftindex1tc1">							
                            <p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="growthchart-centerindex1">
							<p>/</p>
						</div>
						<div class="growthchart-rightindex1tc2">
							<p> <?php echo_isset(number_format($infographic[Infographic_model::DATA]['customerOtmC'], 0, '', '')); ?> </p>
                            <a>total customers</a>
						</div>
					</div>
				</div>
			</a>
			<a data-toggle="ajaxModal" href="<?php echo site_url('infographic/grow_chart') ?>">
				<div class="growthchart-chartindex4">
					<img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50" height="59"/>
					<p id="growthchart-textct4"><?php echo_isset(number_format($otmDPercent, 0)); ?>%</p>
					<svg id="growthchart-index4" width="150" height="106">
					</svg>
					<p class="growthchart-index4textchart2"> OTM d coverage</p>
					<div class="growthchart-index4textchart3">
						<div class="growthchart-leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="growthchart-centerindex1">
							<p>/</p>
						</div>
						<div class="growthchart-rightindex1tc2">							
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::DATA]['customerOtmD'], 0, '', '')); ?> </p>
                            <a>total customers</a>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
</div>
</div>

</div>
<?php
	$tfo = array(
		$infographic[Infographic_model::TYPE]['top3'][0]['quantity'],
		$infographic[Infographic_model::TYPE]['top3'][1]['quantity'],
		$infographic[Infographic_model::TYPE]['top3'][2]['quantity'],
		$infographic[Infographic_model::TYPE]['other']['totalTfo'],
	);
	$revenue = array(
		$infographic[Infographic_model::TYPE]['top3'][0]['valueData'],
		$infographic[Infographic_model::TYPE]['top3'][1]['valueData'],
		$infographic[Infographic_model::TYPE]['top3'][2]['valueData'],
		$infographic[Infographic_model::TYPE]['other']['totalValue'],
	);
?>
<script>

	$(document).ready(function()
	{
		//piechart
		drawPiechart("calldata-chart", <?php echo_isset(number_format($strikeRate, 0)); ?>);
		//growth chart
		drawGrowthChart(<?php echo_isset(number_format($otmAPercent, 0)); ?>, "growthchart-index1");
		$("#growthchart-textct1").text("<?php echo_isset(number_format($otmAPercent, 0)); ?>%");
		drawGrowthChart(<?php echo_isset(number_format($otmBPercent, 0)); ?>, "growthchart-index2");
		$("#growthchart-textct2").text("<?php echo_isset(number_format($otmBPercent, 0)); ?>%");
		drawGrowthChart(<?php echo_isset(number_format($otmCPercent, 0)); ?>, "growthchart-index3");
		$("#growthchart-textct3").text("<?php echo_isset(number_format($otmCPercent, 0)); ?>%");
		drawGrowthChart(<?php echo_isset(number_format($otmDPercent, 0)); ?>, "growthchart-index4");
		$("#growthchart-textct4").text("<?php echo_isset(number_format($otmDPercent, 0)); ?>%");

		// customer type - line chart
		var maxWidth = 357;
		var maxValue = <?php echo_isset(number_format(max($revenue), 0, '', ''));?>;
		var maxTfo = <?php echo_isset(number_format(max($tfo), 0, '', ''));?>;
		var width = 0;

		var rect1 = document.getElementById("linechart1");
		var polygon1 = document.getElementById("polygon1");
		var textrect = document.getElementById("textrect");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[0],0, '', '')); ?>)
		drawLineChart(rect1, polygon1, textrect,width);

		var rect2 = document.getElementById("linechart2");
		var polygon2 = document.getElementById("polygon2");
		var textrect = document.getElementById("textrect2");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[0],0, '', '')); ?>)
		drawLineChart(rect2, polygon2,textrect2, width);

		var rect3 = document.getElementById("linechart3");
		var polygon3 = document.getElementById("polygon3");
		var textrect = document.getElementById("textrect3");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[1],0, '', '')); ?>)
		drawLineChart(rect3, polygon3,textrect3, width);

		var rect4 = document.getElementById("linechart4");
		var polygon4 = document.getElementById("polygon4");
		var textrect = document.getElementById("textrect4");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[1],0, '', '')); ?>)
		drawLineChart(rect4, polygon4,textrect4, width);

		var rect5 = document.getElementById("linechart5");
		var polygon5 = document.getElementById("polygon5");
		var textrect = document.getElementById("textrect5");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[2],0, '', '')); ?>)
		drawLineChart(rect5, polygon5,textrect5, width);

		var rect6 = document.getElementById("linechart6");
		var polygon6 = document.getElementById("polygon6");
		var textrect = document.getElementById("textrect6");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[2],0, '', '')); ?>)
		drawLineChart(rect6, polygon6,textrect6, width);

		var rect7 = document.getElementById("linechart7");
		var polygon7 = document.getElementById("polygon7");
		var textrect = document.getElementById("textrect7");
		width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[3],0, '', '')); ?>)
		drawLineChart(rect7, polygon7,textrect7, width);

		var rect8 = document.getElementById("linechart8");
		var polygon8 = document.getElementById("polygon8");
		var textrect = document.getElementById("textrect8");
		width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[3],0, '', '')); ?>)
		drawLineChart(rect8, polygon8,textrect8, width);
	});

	//function draw line chart
	function drawLineChart(rect, polygol, text, width) {
		rect.setAttribute("width",width + 0.45);
		polygol.setAttribute("x", width + 10);
		text.setAttribute("x", width -90);
	}

	//function draw growth chart
	function drawGrowthChart(percent, chartid){
		var svgns = "http://www.w3.org/2000/svg";
		var growthChartColumnHeight = [30, 37.5, 45, 52.5, 60, 67.5, 75, 82.5, 90, 97.5];
		var growthChartColumnWidth = 11.5;
		var growthChartHorizonPosition = [0, 15.5, 30.5, 46, 61.5, 77, 92, 107.5, 123, 139];
		var chartHeight = 106;
		var mainColor = "rgb(142, 198, 65)";
		var grayColor = "rgb(189, 190, 192)";

		var svgDocument = document.getElementById(chartid);
		var percentPerTen = percent / 10;

		for (i = 0; i < 10; i++) {
			var currentColumnWidth = growthChartColumnWidth;
			var currentColumnColor = mainColor;
			if(i <= percentPerTen && percentPerTen < i+1) {
				currentColumnWidth = (percent%10)/10 * growthChartColumnWidth;
			} else if (i > percentPerTen) {
				currentColumnColor = grayColor;
			}

			var shape = document.createElementNS(svgns,"rect");
			shape.setAttributeNS(null, "x", growthChartHorizonPosition[i]);
			shape.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
			shape.setAttributeNS(null, "width", currentColumnWidth);
			shape.setAttributeNS(null, "height", growthChartColumnHeight[i]);
			shape.setAttributeNS(null, "fill", currentColumnColor);

			var shape1 = null;
			if(currentColumnWidth < growthChartColumnWidth) {
				shape1 = document.createElementNS(svgns,"rect");
				shape1.setAttributeNS(null, "x", growthChartHorizonPosition[i] + currentColumnWidth - 0.5);
				shape1.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
				shape1.setAttributeNS(null, "width", growthChartColumnWidth - currentColumnWidth);
				shape1.setAttributeNS(null, "height", growthChartColumnHeight[i]);
				shape1.setAttributeNS(null, "fill", grayColor);
			}

			svgDocument.appendChild(shape);
			if(shape1 != null) svgDocument.appendChild(shape1);
		}
	}

	function drawPiechart(chartid, percent) {
		var chart = document.getElementById(chartid);
		var abtext1 = document.getElementById("abtext1");
		var abtext2 = document.getElementById("abtext2");
		var textpercent = document.getElementById("piechart-percent");

		abtext2_newy = 150 + 105 * Math.cos(Math.PI * (1 - percent/100)) - abtext1.offsetHeight/2;
		abtext2_newx = 90 + 105 * Math.sin(Math.PI * (1 - percent/100)) - abtext1.offsetWidth/2;
		abtext1_newy = 150 - 105 * Math.cos(Math.PI * (1 - percent/100)) - abtext2.offsetHeight/2;
		abtext1_newx = 90 - 105 * Math.sin(Math.PI * (1 - percent/100)) - abtext2.offsetWidth/2;
//		abtext1.style.marginTop = abtext1_newy;
//		abtext1.style.marginLeft = abtext1_newx;
//		abtext2.style.marginTop = abtext2_newy;
//		abtext2.style.marginLeft = abtext2_newx;

		$('#abtext1').css({
			marginTop: abtext1_newy,
			marginLeft: abtext1_newx
		});
		$('#abtext2').css({
			marginTop: abtext2_newy,
			marginLeft: abtext2_newx
		})

		// chart.setAttribute("data-text", percent + "%");
		textpercent.innerHTML = percent + "%";
		chart.setAttribute("data-percent", percent);

		$('#' + chartid).circliful();
	}

	function calculateWidth(maxWidth, maxValue, value) {
		var width = 0;
		width = (value/maxValue) * maxWidth;

		return width;
	}

</script>