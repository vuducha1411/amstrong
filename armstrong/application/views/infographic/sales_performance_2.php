<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>SALES performance (Grip & Grab)</p>
	</div>
	<div id="content" class="text-center">
	    <div class="content">
	        <div class="row1">
	            <div class="rowtable1">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text11"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip']), 0) ?></p>
	                    <p class="text22">grip</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text3">
		                    <?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrip']), 0) ?>
	                    </p>
	                    <p class="text4">new grip</p>
	                </div>
	            </div>
	            <div class="rowtable2">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text5"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip']), 0) ?></p>
	                    <p class="text22">grip</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrip']), 0) ?></p>
	                    <p class="text4">new grip</p>
	                </div>
	            </div>
	            <div class="rowtable3">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text7"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip']), 0) ?></p>
	                    <p class="text22">grip</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrip']), 0) ?></p>
	                    <p class="text4">new grip</p>
	                </div>
	            </div>
	            <div class="rowtable4">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text9"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip']), 0) ?></p>
	                    <p class="text22">grip</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintext text3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrip']), 0) ?></p>
	                    <p class="text4">new grip</p>
	                </div>
	            </div>
	        </div>
	        <div class="row2">
	            <div class="bg bg_table">
	                <a title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName']) ?>">
		                <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName']) ?>
	                </a>
	            </div>
	            <div class="bg bg_table2">
	                <a title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName']) ?>">
		                <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName']) ?>
	                </a>
	            </div>
	            <div class="bg bg_table3">
	                <a title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName']) ?>">
		                <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName']) ?>
	                </a>
	            </div>
	            <div class="bg bg_table4">
	                <a>Others</a>
	            </div>
	        </div>
	        <div class="row3">
	            <div class="rowtable1">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab']), 0) ?></p>
	                    <p class="txt2">grab</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrab']), 0) ?></p>
	                    <p class="txt4">new grab</p>
	                </div>
	            </div>
	            <div class="rowtable2">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt5"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab']), 0) ?></p>
	                    <p class="txt2">grab</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrab']), 0) ?></p>
	                    <p class="txt4">new grab</p>
	                </div>
	            </div>
	            <div class="rowtable3">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt7"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab']), 0) ?></p>
	                    <p class="txt2">grab</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrab']), 0) ?></p>
	                    <p class="txt4">new grab</p>
	                </div>
	            </div>
	            <div class="rowtable4">
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt9"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrab']), 0) ?></p>
	                    <p class="txt2">grab</p>
	                </div>
	                <div class="test">
	                    <div class="mntest">
	                    </div>
	                    <p class="maintxt txt3"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrab']), 0) ?></p>
	                    <p class="txt4">new grab</p>
	                </div>
	            </div>
	        </div>
			<div class="row4">
	            <div class="bg bg_table">
		            <?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab']/$infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip'], 2)); ?>
                    <p>Average Grab</p>
	            </div>
	            <div class="bg bg_table2">
		            <?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab']/$infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip'], 2)); ?>
                    <p>Average Grab</p>
                </div>
	            <div class="bg bg_table3">
		            <?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab']/$infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip'], 2)); ?>
                    <p>Average Grab</p>
                </div>
	            <div class="bg bg_table4">
		            <?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrab']/$infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip'], 2)); ?>
                    <p>Average Grab</p>
                </div>
	        </div>
		</div>
	</div>
</div>