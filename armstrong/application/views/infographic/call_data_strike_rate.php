<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<?php
    $strikeRateModal = 0;
	if($infographic[Infographic_model::CALL_RECORD]){
		if($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] > 0 && $infographic[Infographic_model::CALL_RECORD]['totalCall'] > 0){
			$strikeRateModal = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] / $infographic[Infographic_model::CALL_RECORD]['totalCall'] * 100;
		}
	}else{
		$infographic[Infographic_model::CALL_RECORD]['totalCall'] = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] = $infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'] = 0;
	}
	?>
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>CUSTOMER DATA</p>
	</div>
	<div id="content" class="text-center">
		<div class="contentpie">
			<div id="myStat2" data-dimension="484" data-width="90" data-fontsize="75.25" data-percent="65" data-fgcolor="#2BACE2" data-bgcolor="#818285" data-animationstep="0">
			</div>
			<div class="rightcontentpie">
				<div class="toppie">
					<div class="leftctpie">
						<p class="textleftctpie1">Total Calls</p>
						<p class="textleftctpie2">Calls with oders</p>
						<p class="textleftctpie3">Calls without oders</p>
					</div>
					<div class="rightctpie">
						<p class="textrightctpie1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCall'])) ?></p>
						<p class="textrightctpie2"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>
						<p class="textrightctpie3"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
					</div>
				</div>
				<div class="bottompie">
					<div class="leftbottompie">
						<p class="textleftbtpie1"><?php echo_isset(number_format($strikeRateModal, 0)); ?>% <span>/</span></p>
						<p class="textleftbtpie2">Strike Rate</p>
					</div>
<!--					<div class="rightbottompie">-->
<!--						<p class="textleftrightpie1">50%</p>-->
<!--						<p class="textleftrightpie2">target</p>-->
<!--					</div>-->
				</div>
			</div>
			<div class="abtext1" id="modalabtext1">
				<p class="abtextcall">Calls without orders</p>
				<p class="abnumcall"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
			</div>
			<div class="abtext2" id="modalabtext2">
				<p class="abtextcall1">Calls with orders</p>
				<p class="abnumcall1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>
			</div>
            <div class="abtext3" id="modalabtext3">
                <p class="abnumcall2 textleftbtpie1"><?php echo_isset(number_format($strikeRateModal, 0)); ?>%<span>/</span></p>
                <p class="abtextcall2 textleftbtpie2">Strike Rate</p>
            </div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		//piechart
		drawModalPiechart("#myStat2", <?php echo_isset(number_format($strikeRateModal, 0)); ?>);
	});

	function drawModalPiechart(chartid, percent) {
		var chart = $(chartid);
		var modalabtext1 = $('#modalabtext1');
		var modalabtext2 = $('#modalabtext2');
        var modalabtext3 = $('#modalabtext3');

        var modalabtext3_newy = 100 + 197 * Math.cos(Math.PI * (1 - percent/100)) - modalabtext3.offsetHeight/2;
        var modalabtext3_newx = 160 + 197 * Math.sin(Math.PI * (1 - percent/100)) - modalabtext3.offsetWidth/2;
		var modalabtext2_newy = 230 + 197 * Math.cos(Math.PI * (1 - percent/100)) - modalabtext1.offsetHeight/2;
		var modalabtext2_newx = 290 + 197 * Math.sin(Math.PI * (1 - percent/100)) - modalabtext1.offsetWidth/2;
		var modalabtext1_newy = 230 - 197 * Math.cos(Math.PI * (1 - percent/100)) - modalabtext2.offsetHeight/2;
		var modalabtext1_newx = 290 - 197 * Math.sin(Math.PI * (1 - percent/100)) - modalabtext2.offsetWidth/2;

		modalabtext1.css({
			marginTop: modalabtext1_newy,
			marginLeft: modalabtext1_newx
		});
		modalabtext2.css({
			marginTop: modalabtext2_newy,
			marginLeft: modalabtext2_newx
		});
        modalabtext3.css({
            marginTop: modalabtext3_newy,
            marginLeft: modalabtext3_newx
        });

		//chart.attr("data-text", percent + "%");
		//chart.attr("data-percent", percent);

		$(chartid).circliful();
	}

</script>