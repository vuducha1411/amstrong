<?php 
    if (!intval($infographic[Infographic_model::DATA]['salesTarget']))
    {
        $percent = '100';
    }
    else
    {
        $percent = (intval($infographic[Infographic_model::DATA]['salesAchieved']) / intval($infographic[Infographic_model::DATA]['salesTarget'])) * 100;
    }
?>

<div class="main">
    <div class="header">
        <div class="textbsheader">
            <?php $dt = DateTime::createFromFormat('!m', $this->session->userdata('infographicMonth')); ?>
            <a href="<?php echo site_url('/') ?>"><?php echo $country_name; ?> - PULL <?php echo_isset($dt->format('M') . ', ' . $this->session->userdata('infographicYear')) ?></a>
        </div>
    </div>
    <div class="content">
        <div id="contantmalaypd">   
            <div class="leftcontantmalaypd">
                <div class="leftcontantmalaypdtable1">
	                <a class="texttopsku" href="<?= site_url('infographic/top_sku'); ?>" data-toggle="ajaxModal">Top Sku</a>
	                <div class="tbtopskut">
                        <div class="top-sku-background-home">
				<p class="top-sku-title">TOP SKU</p>
	            <div class="top-sku-inside-home">
					<ul class="top-sku-list-home">
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
						<li>
							<p>KNORR CHICKEN STOCK</p>
							<p class="top-sku-percent">[32%]</p>
						</li>
					</ul>
				</div>
	        </div>
                    </div>
                </div>
                <div class="leftcontantmalaypdtable2">
	                <a class="textsaledperformance" href="<?= site_url('infographic/sales_performance_2'); ?>" data-toggle="ajaxModal">SALES performance</a>
	                <div class="tbtopskut2">
                        <div class="mntbtopskut2">
                            <div class="mn_skut mntbtopskut21">
                                <div class="mntbtopskut21top">
                                    <div class="mnpie1">
                                        <p class="red1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip']), 0) ?></p>
                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie2">
                                        <p class="red1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrip']), 0) ?></p>
                                        <p class="white1">new grip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut21center">
                                    <p class="textblack1" title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName']) ?>">
                                        <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName']) ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut21bottom">
                                    <div class="mnpie3">
                                        <p class="red1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab']), 0) ?></p>
                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie4">
                                        <p class="red1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrab']), 0) ?></p>
                                        <p class="white1">new grab</p>
                                    </div>
                                </div>
								<div class="mntbtopskut21center">
									<p class="textblack1">
										N/A
									</p>
								</div>
                            </div>
                            <div class="mn_skut mntbtopskut22">
                                <div class="mntbtopskut22top">
                                    <div class="mnpie2_1">
                                        <p class="blue1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip']), 0) ?></p>
                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie2_2">
                                        <p class="blue1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrip']), 0) ?></p>
                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut22center">
                                    <p class="textblack1" title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName']) ?>">
                                        <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName']) ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut22bottom">
                                    <div class="mnpie2_3">
                                        <p class="blue1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab']), 0) ?></p>
                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie2_4">
                                        <p class="blue1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrab']), 0) ?></p>
                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
								<div class="mntbtopskut22center">
									<p class="textblack1" title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName']) ?>">
										N/A
									</p>
								</div>
                            </div>
                            <div class="mn_skut mntbtopskut23">
                                <div class="mntbtopskut23top">
                                    <div class="mnpie3_1">
                                        <p class="green1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip']), 0) ?></p>
                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie3_2">
                                        <p class="green1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrip']), 0) ?></p>
                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut23center">
                                    <p class="textblack1" title="<?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName']) ?>">
                                        <?php echo_isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName']) ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut23bottom">
                                    <div class="mnpie3_3">
                                        <p class="green1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab']), 0) ?></p>
                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie3_4">
                                        <p class="green1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrab']), 0) ?></p>
                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
								<div class="mntbtopskut23center">
									<p class="textblack1">
										N/A
									</p>
								</div>
                            </div>
                            <div class="mn_skut mntbtopskut24">
                                <div class="mntbtopskut24top">
                                    <div class="mnpie4_1">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip']), 0) ?></p>
                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie4_2">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrip']), 0) ?></p>
                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut24center">
                                    <p class="textblack1">others</p>
                                </div>
                                <div class="mntbtopskut24bottom">
                                    <div class="mnpie4_3">
                                        <p class="black1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrab']), 0) ?></p>
                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie4_4">
                                        <p class="black1">N/A<?php //echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrab']), 0) ?></p>
                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
								<div class="mntbtopskut24center">
									<p class="textblack1">N/A</p>
								</div>
                            </div>
                        </div>
                    </div>
                    <div class="tbsalesperformance"></div>
                </div>
                <div class="leftcontantmalaypdtable3">
                    <a class="textrichmedia">rich media</a>
                    <div class="tbrichmedia">
	                    <a style="margin-left: 0px; margin-top: 10px;" href="<?= site_url('infographic/rich_media_2'); ?>" data-toggle="ajaxModal">
		                    <div class="toptbrichmedia">
			                    <div class="mntbrichmedia toptbrichmedia1">
				                    <p class="textbrichmediat1">Armstrong has <br /> a total of</p>
				                    <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][1]['quantity'] )?></p>
				                    <p class="textbrichmediat3">SKUS</p>
			                    </div>
			                    <div class="mntbrichmedia toptbrichmedia2">
				                    <p class="textbrichmediat1">Armstrong has</br /> a total of</p>
				                    <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][4]['quantity'] )?></p>
				                    <p class="textbrichmediat3">Selling<br /> Stories</p>
			                    </div>
			                    <div class="mntbrichmedia toptbrichmedia3">
				                    <p class="textbrichmediat1">Armstrong has<br />a total of</p>
				                    <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][3]['quantity'] )?></p>
				                    <p class="textbrichmediat3">Videos</p>
			                    </div>
			                    <div class="mntbrichmedia toptbrichmedia4">
				                    <p class="textbrichmediat1">Armstrong has<br />a total of</p>
				                    <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][2]['quantity'] )?></p>
				                    <p class="textbrichmediat3">Recipes</p>
			                    </div>
		                    </div>
                        </a>
	                    <a style="margin-left: 0px; margin-top: 10px;" href="<?= site_url('infographic/rich_media_1'); ?>" data-toggle="ajaxModal">
		                    <div class="bottomtbrichmedia">
		                        <div class="leftbottombrichmedia">
	                                <p class="textleftbottombrichmedia1">
	                                    A total of <?php echo_isset(number_format($infographic[Infographic_model::DATA]['totalCustomerHaveSampled'])) ?> customers
	                                </p>
	                                <p class="textleftbottombrichmedia2">
	                                    have sampled our products
	                                </p>
	                                <div class="imgleftbottombrichmedia">
	                                    <div class="imgleftbottombrichmedia1"></div>
	                                    <div class="imgleftbottombrichmedia2"></div>
	                                    <p class="textimgleftbottombrichmedia"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['totalSamplingTimes'])) ?></p>
                                        <span class="text-below-number">SAMPLING ACTIVITIES</span>
	                                </div>
									<div id="rich-media-left-detail-info-home">
										<ul>
											<li>3324<br/><span>WET</span></li>
											<li>127<br/><span>DRY</span></li>
											<li>1351<br/><span>DEMO</span></li>
										</ul>
									</div>
                                    <p class="textleftbottomrichmedia2">
                                        SAMPLING ITEMS
                                    </p>
	                            </div>
	                            <div class="righbottomtbrichmedia">
	                                <p class="textrightbottombrichmedia1">
	                                    A total of <?php echo_isset(number_format($infographic[Infographic_model::DATA]['totalCustomerHavePantryCheck'])) ?>
	                                </p>
	                                <p class="textrightbottombrichmedia2">
	                                    Pantry checks conducted
	                                </p>
	                                <div class="imgrightbottombrichmedia">
	                                    <div class="imgrightbottombrichmedia1"></div>
	                                    <p class="textimgrightbottombrichmedia"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['totalPantryCheckTimes'])) ?></p>
                                        <span class="text-below-number blue">PANTRY CHECK ITEMS</span>
	                                </div>
	                            </div>
	                        </div>
	                    </a>
                    </div>
                </div>
            </div>
            <div class="rightcontantmalaypd">
                <div class="rightcontantmalaypdtable1">
	                <a class="textsalesperformance1" href="<?= site_url('infographic/sales_performance_1'); ?>" data-toggle="ajaxModal">SALES performance</a>
	                <div class="tbtopskut4">
                        <div class="mntbtopskut4">
                            <div class="tbtopskut4tab1">
                                <p class="txttab1">Sales achieved</p>
                                <p class="txttab2"><?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::DATA]['salesAchieved'])) ?></p>
                                <p class="txttab3"><?php echo_isset(number_format($percent)) ?>%</p>
                            </div>
                            <div class="tbtopskut4tab2">
                                <p class="txttab4">/</p>
                            </div>
                            <div class="tbtopskut4tab3">
                                <p class="txttab1">Sales target</p>
                                <p class="txttab2"><?php echo_isset($this->country_currency . ' '. number_format($infographic[Infographic_model::DATA]['salesTarget'])) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rightcontantmalaypdtable2">
                    <a class="textsalesteam">SALES TEAM</a>
                    <div class="tbsalesteam">
                        <div class="toptbsalesteam">
                        </div>
                        <div class="bottomsalesteam">
                            
                            <div class="block2">
                                <svg id="human-chart" width="339" height="231" xmlns:xlink= "http://www.w3.org/1999/xlink">
                                </svg>
                            </div>          
                            <div class="block3">
                                <div class="textalign">
                                    <p id="txt1"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesLeaders']), 0) ?> SALES LEADERS</p>
                                    <p id="txt2"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesPersonnels']), 0) ?> SALES PERSONNELS</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    
    drawHumanChart(<?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesLeaders']), 0) ?>, <?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesPersonnels']), 0) ?>,"human-chart");
    $("#txt1").text("<?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesLeaders']), 0) ?> SALES LEADERS");
    $("#txt2").text("<?php echo_isset(number_format($infographic[Infographic_model::DATA]['numberSalesPersonnels']), 0) ?> SALES PERSONNELS");
                      
});
    
function drawHumanChart(redhumans, greenhumans, chartid) {
    var greenHumanImage = "<?php echo site_url('res/img/infographic/green_human.png'); ?>",
        redHumanImage = "<?php echo site_url('res/img/infographic/red_human.png'); ?>",
        allHuman = redhumans + greenhumans;
    
    var svgDocument = document.getElementById(chartid);
    
   // var svgWidth = svgDocument.clientWidth,
      //  svgHeight = svgDocument.clientHeight,
       // rowCount = 0;
	var svgWidth = $("#" + chartid).width(),
		svgHeight = $("#" + chartid).height(),
		rowCount = 0;	
        
    if (allHuman < 5) rowCount = 1;
    else if (allHuman < 15) rowCount = 2;
    else if (allHuman < 34) rowCount = 3;
    else if (allHuman < 100) rowCount = 4;
    else rowCount = 5;
    
    var humanHeight;
    if (rowCount > 1) humanHeight = (svgHeight - (rowCount - 1) * 5) / rowCount;
    else humanHeight = svgHeight;
    
    var humanWidth = 68/176 * humanHeight,
        humanPerRow = Math.floor(svgWidth / (humanWidth + 2)),
        humanInLastRow = allHuman - (rowCount - 1) * humanPerRow,
        rowInRed = Math.floor(redhumans / humanPerRow),
        redHumanInLastRow = redhumans - (rowInRed) * humanPerRow;

    for (var i = 0; i < rowCount; i++) {
        for(var j = 0; j< humanPerRow; j++) {
            var currentHumanColor = redHumanImage;
            if((i == rowInRed && j >= redHumanInLastRow) || i > rowInRed) {
                currentHumanColor = greenHumanImage;
            }
            if(j >= allHuman - (i * humanPerRow)) break;

            var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
            svgimg.setAttribute('height', humanHeight);
            svgimg.setAttribute('width', humanWidth);
            svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href', currentHumanColor);
            svgimg.setAttribute('x', humanWidth * j + 2 * j);
            svgimg.setAttribute('y', humanHeight * i + 2 * i);
            
            svgDocument.appendChild(svgimg);
        }
    }
    
    
}

</script>