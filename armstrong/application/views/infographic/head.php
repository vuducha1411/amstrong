<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$page['title']?></title>
    <?php foreach($page['stylesheets'] as $item) { ?>
        <link rel="stylesheet" href="<?=base_url().$item?>" />
    <?php } ?>
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/jquery.circliful.js'); ?>"></script>
    <?php foreach($page['scripts'] as $item) { ?>
        <script src="<?=base_url().$item?>"></script>
    <?php } ?>
</head>
<body>