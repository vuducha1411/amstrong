<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Rich Media</p>
	</div>
	<div id="content" class="text-center cf" style="min-height: 500px; width: 1100px; padding-top: 100px;">
		<div class="leftcontent">
			<h1>A total of <?php echo number_format($infographic['totalCustomerHaveSampled']) ?> customers</h1>
			<h2>have sampled our products</h2>
			<div class="left_img">
				<div class="left_img1">
					<img src="<?php echo site_url('res/img/infographic/pic2.png'); ?>"/>
					<img src="<?php echo site_url('res/img/infographic/pic1.png'); ?>"/>
				</div>
				<p><?php echo number_format($infographic['totalSamplingTimes']) ?></p>
                <span class="text-below-number font-26 font-bold">SAMPLING ACTIVITIES</span>
			</div>
			<div id="rich-media-left-detail-info">
				<ul>
					<li><?php echo number_format($infographic['totalSamplingItemsWet']); ?><br/><span>WET</span></li>
					<li><?php echo number_format($infographic['totalSamplingItemsDry']); ?><br/><span>DRY</span></li>
					<li><?php echo number_format($infographic['totalSamplingItemsCd']); ?><br/><span>DEMO</span></li>
				</ul>
			</div>
            <p class="textleftbottomrichmedia2 font-26 font-bold">
                SAMPLING ITEMS
            </p>
		</div>
		<div class="rightcontent">
			<h1>A total of <?php echo number_format($infographic['totalPantryCheckTimes']) ?></h1>
			<h2>Pantry checks conducted</h2>
			<div class="right_img">
				<img src="<?php echo site_url('res/img/infographic/pic3.png'); ?>"/>
				<p><?php echo number_format($infographic['totalPantryCheckItems']) ?></p>
                <span class="text-below-number font-26 font-bold" style="color: #29AAE1">PANTRY CHECK ITEMS</span>
			</div>
		</div>

	</div>
</div>
