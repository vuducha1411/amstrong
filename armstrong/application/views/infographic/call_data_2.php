<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Call Data</p>
	</div>
	<div id="content" class="text-center">
	    <div id="contentdata1">
	        <ul class="mn_ctdata">
	            <li class="mn_data tabledata1">
	                <h1>OTM A</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
	                    <p><?php echo (isset($infographic['visitedOtmA']) && $infographic['visitedOtmA'] != 0 ) ? number_format(($infographic['timesCallOtmA'] / $infographic['visitedOtmA']), 1) : 0 ?></p>
	                </div>
	            </li>
	            <li class="mn_data tabledata2">
	                <h1>OTM B</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
		                <p><?php echo (isset($infographic['visitedOtmB']) && $infographic['visitedOtmB'] != 0 ) ? number_format(($infographic['timesCallOtmB'] / $infographic['visitedOtmB']), 1) : 0 ?></p>
	                </div>
	            </li>
	            <li class="mn_data tabledata3">
	                <h1>OTM C</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
		                <p><?php echo (isset($infographic['visitedOtmC']) && $infographic['visitedOtmC'] != 0 ) ? number_format(($infographic['timesCallOtmC'] / $infographic['visitedOtmC']), 1) : 0 ?></p>
	                </div>
	            </li>
	            <li class="mn_data tabledata4">
	                <h1>OTM D</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
		                <p><?php echo (isset($infographic['visitedOtmD']) && $infographic['visitedOtmD'] != 0 ) ? number_format(($infographic['timesCallOtmD'] / $infographic['visitedOtmD']), 1) : 0 ?></p>
	                </div>
	            </li>
				<!-- <li class="mn_data tabledata5">
	                <h1>OTM D</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
		                <p><?php //echo (isset($infographic['visitedOtmD']) && $infographic['visitedOtmD'] != 0 ) ? number_format(($infographic['timesCallOtmA'] / $infographic['visitedOtmD']), 1) : 0 ?></p>
	                </div>
	            </li> -->
	        </ul>
            <div class="mn-un-total-full">
                Unclassified OTM call frequency:
                <?php
                $frequencyU = 0;
                if(isset($infographic['visitedOtmU']) && isset($infographic['timesCallOtmU'])){
                    if($infographic['visitedOtmU'] > 0 && $infographic['timesCallOtmU'] > 0){
                        $frequencyU = $infographic['timesCallOtmU'] / $infographic['visitedOtmU'];
                    }
                }
                echo number_format($frequencyU, 1)
                ?>
            </div>
	    </div>
	</div>
</div>