<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Call Data</p>
	</div>
	<div id="content" class="text-center">

	    <div id="contanttotal" style="margin-bottom: 50px;">
	        <div class="ct_total total1">
	            <h1>A</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmA']) ? $infographic['timesCallOtmA'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total2">
	            <h1>B</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmB']) ? $infographic['timesCallOtmB'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total3">
	            <h1>C</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmC']) ? $infographic['timesCallOtmC'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
	        <div class="ct_total total4">
	            <h1>D</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php echo isset($infographic['timesCallOtmD']) ? $infographic['timesCallOtmD'] : 0 ?></h3>
	            <h4>CALLS</h4>
	        </div>
			<!-- <div class="ct_total total5">
	            <h1>D</h1>
	            <h2>HAS A TOTAL OF</h2>
	            <h3><?php// echo number_format($infographic['timesCallOtmD']) ?></h3>
	            <h4>CALLS</h4>
	        </div> -->
            <div class="ct-un-total-full">
                Unclassified OTM has a total of <?php echo isset($infographic['timesCallOtmU']) ? $infographic['timesCallOtmU'] : 0 ?> Calls
            </div>
	        <div class="imgtotal">
	            <img src="<?php echo site_url('res/img/infographic/total5.png'); ?>"/>
	        </div>
	        <p><?php echo isset($infographic['totalCall']) ? $infographic['totalCall'] : 0 ?></p>
	        <div class="ct_total_text">CALLS IN TOTAL</div>
	    </div>
        <div id="contentdata1">
            <ul class="mn_ctdata">
                <li class="mn_data tabledata1">
                    <h1>OTM A</h1>
                    <h2>CALL FREQUENCY</h2>
                    <div class="number">
                        <p><?php
							$frequencyA = 0;
							if (isset($infographic['visitedOtmA']) && isset($infographic['timesCallOtmA'])) {
								if ($infographic['visitedOtmA'] > 0 && $infographic['timesCallOtmA'] > 0) {
									$frequencyA = $infographic['timesCallOtmA'] / $infographic['visitedOtmA'];
								}
							}
							echo number_format($frequencyA, 2);
							?></p>
                    </div>
                </li>
                <li class="mn_data tabledata2">
                    <h1>OTM B</h1>
                    <h2>CALL FREQUENCY</h2>
                    <div class="number">
                        <p><?php
							$frequencyB = 0;
							if (isset($infographic['visitedOtmB']) && isset($infographic['timesCallOtmB'])) {
								if ($infographic['visitedOtmB'] > 0 && $infographic['timesCallOtmB'] > 0) {
									$frequencyB = $infographic['timesCallOtmB'] / $infographic['visitedOtmB'];
								}
							}
							echo number_format($frequencyB, 2);
                            ?></p>
                    </div>
                </li>
                <li class="mn_data tabledata3">
                    <h1>OTM C</h1>
                    <h2>CALL FREQUENCY</h2>
                    <div class="number">
                        <p><?php
							$frequencyC = 0;
							if (isset($infographic['visitedOtmC']) && isset($infographic['timesCallOtmC'])) {
								if ($infographic['visitedOtmC'] > 0 && $infographic['timesCallOtmC'] > 0) {
									$frequencyC = $infographic['timesCallOtmC'] / $infographic['visitedOtmC'];
								}
							}
							echo number_format($frequencyC, 2);
                            ?></p>
                    </div>
                </li>
                <li class="mn_data tabledata4">
                    <h1>OTM D</h1>
                    <h2>CALL FREQUENCY</h2>
                    <div class="number">
                        <p><?php
							$frequencyD = 0;
							if (isset($infographic['visitedOtmD']) && isset($infographic['timesCallOtmD'])) {
								if ($infographic['visitedOtmD'] > 0 && $infographic['timesCallOtmD'] > 0) {
									$frequencyD = $infographic['timesCallOtmD'] / $infographic['visitedOtmD'];
								}
							}
							echo number_format($frequencyD, 2);
                            ?></p>
                    </div>
                </li>
                <!-- <li class="mn_data tabledata5">
	                <h1>OTM D</h1>
	                <h2>CALL FREQUENCY</h2>
	                <div class="number">
		                <p><?php //echo (isset($infographic['visitedOtmD']) && $infographic['visitedOtmD'] != 0 ) ? number_format(($infographic['timesCallOtmA'] / $infographic['visitedOtmD']), 1) : 0 ?></p>
	                </div>
	            </li> -->
            </ul>
            <div class="mn-un-total-full">
                Unclassified OTM call frequency:
                <?php
                $frequencyU = 0;
                if(isset($infographic['visitedOtmU']) && isset($infographic['timesCallOtmU'])){
                    if($infographic['visitedOtmU'] > 0 && $infographic['timesCallOtmU'] > 0){
                        $frequencyU = $infographic['timesCallOtmU'] / $infographic['visitedOtmU'];
                    }
                }
                echo number_format($frequencyU, 2)
                ?>
            </div>
        </div>
	</div>
</div>