<?php
$averageCalls = number_format(($infographic['totalCall'] / $infographic['workingDay']));
?>
<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Call Data</p>
	</div>
	<div id="content" class="text-center">
	    <ul class="mn_cd cf">
	        <li class="mn_call">
	            <div class="tablecall1">
	                <p><?php echo $averageCalls ?></p>
	            </div>
	            <h1>TOTAL AVERAGE CALLS<br />/WORK DAY</h1>
	        </li>
	        <li class="mn_call">
	            <div class="tablecall2">
	                <p><?php
                        if($averageCalls > 0 && $infographic['totalSalespersons'] > 0){
                            echo number_format(($averageCalls / $infographic['totalSalespersons']), 0);
                        }else{
                            echo 0;
                        }
                        ?></p>
	            </div>
	            <h1>AVERAGE CALLS<br />/WORK DAY/SR</h1>
	        </li>
	    </ul>
	</div>
</div>