<?php
$otmAPercent = $otmBPercent = $otmCPercent = $otmDPercent = 0;
if($infographic[Infographic_model::CALL_RECORD]){
	if($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] > 0 && $infographic[Infographic_model::CALL_RECORD]['customerOtmA']){
		$otmAPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] / $infographic[Infographic_model::CALL_RECORD]['customerOtmA'] * 100;
	}
	if($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] > 0 && $infographic[Infographic_model::CALL_RECORD]['customerOtmB']){
		$otmBPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] / $infographic[Infographic_model::CALL_RECORD]['customerOtmB'] * 100;
	}
	if($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] > 0 && $infographic[Infographic_model::CALL_RECORD]['customerOtmC']){
		$otmCPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] / $infographic[Infographic_model::CALL_RECORD]['customerOtmC'] * 100;
	}
	if($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] > 0 && $infographic[Infographic_model::CALL_RECORD]['customerOtmD']){
		$otmDPercent = $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] / $infographic[Infographic_model::CALL_RECORD]['customerOtmD'] * 100;
	}
}else{
	$infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] = $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] = $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] = $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] = 0;
	$infographic[Infographic_model::CALL_RECORD]['customerOtmA'] = $infographic[Infographic_model::CALL_RECORD]['customerOtmB'] = $infographic[Infographic_model::CALL_RECORD]['customerOtmC'] = $infographic[Infographic_model::CALL_RECORD]['customerOtmD'] = 0;
}
?>
<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?=site_url('res/img/infographic/logo.png')?>"><p>CALL DATA</p>
	</div>
	<div id="content">
		<div id="calldatacontent">
			<div class="chartbar12">
				<div class="chartindex1">
					<img src="<?=site_url('res/img/infographic/pic.png')?>"/>
					<p id="textct1"><?php echo_isset(number_format($otmAPercent, 0)); ?>%</p>
					<svg id="index1" width="200" height="142">
					</svg>
					<p class="idex1textchart1"> target is 100%</p>
					<p class="idex1textchart2"> OTM A coverage</p>
					<div class="index1textchart3">
						<div class="leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="centerindex1">
							<p>/</p>
						</div>
						<div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['customerOtmA'], 0, '', '')); ?> </p>
                            <a>total customers</a>							
						</div>
					</div>
				</div>
				<div class="chartindex2">
					<img src="<?=site_url('res/img/infographic/pic.png')?>"/>
					<p id="textct2"><?php echo_isset(number_format($otmBPercent, 0)); ?>%</p>
					<svg id="index2" width="200" height="142">
					</svg>
					<p class="index2textchart1"> target is 100%</p>
					<p class="index2textchart2"> OTM B coverage</p>
					<div class="index1textchart3">
						<div class="leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="centerindex1">
							<p>/</p>
						</div>
						<div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['customerOtmB'], 0, '', '')); ?> </p>
                            <a>total customers</a>							
						</div>
					</div>
				</div>
			</div>
			<div class="chartbar34">
				<div class="chartindex3">
					<img src="<?=site_url('res/img/infographic/pic.png')?>"/>
					<p id="textct3"><?php echo_isset(number_format($otmCPercent, 0)); ?>%</p>
					<svg id="index3" width="200" height="142">
					</svg>
					<p class="idex3textchart2"> OTM c coverage</p>
					<div class="index3textchart3">
						<div class="leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="centerindex1">
							<p>/</p>
						</div>
						<div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['customerOtmC'], 0, '', '')); ?> </p>
                            <a>total customers</a>							
						</div>
					</div>
				</div>
				<div class="chartindex4">
					<img src="<?=site_url('res/img/infographic/pic.png')?>"/>
					<p id="textct4"><?php echo_isset(number_format($otmDPercent, 0)); ?>%</p>
					<svg id="index4" width="200" height="142">
					</svg>
					<p class="index4textchart2"> OTM d coverage</p>
					<div class="index4textchart3">
						<div class="leftindex1tc1">
							<p><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'], 0, '', '')); ?></p>
                            <a>visited customer</a>
						</div>
						<div class="centerindex1">
							<p>/</p>
						</div>
						<div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['customerOtmD'], 0, '', '')); ?> </p>
                            <a>total customers</a>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		drawGrowthChart(<?php echo_isset(number_format($otmAPercent, 0)); ?>, "index1");
		$("#textct1").text("<?php echo_isset(number_format($otmAPercent, 0)); ?>%");
		drawGrowthChart(<?php echo_isset(number_format($otmBPercent, 0)); ?>, "index2");
		$("#textct2").text("<?php echo_isset(number_format($otmBPercent, 0)); ?>%")
		drawGrowthChart(<?php echo_isset(number_format($otmCPercent, 0)); ?>, "index3");
		$("#textct3").text("<?php echo_isset(number_format($otmCPercent, 0)); ?>%");
		drawGrowthChart(<?php echo_isset(number_format($otmDPercent, 0)); ?>, "index4");
		$("#textct4").text("<?php echo_isset(number_format($otmDPercent, 0)); ?>%");

	});

	function drawGrowthChart(percent, chartid){
		var svgns = "http://www.w3.org/2000/svg";
		var growthChartColumnHeight = [40, 50, 60, 70, 80, 90, 100, 110, 120, 130];
		var growthChartColumnWidth = 15.5;
		var growthChartHorizonPosition = [0, 20.5, 41, 61.5, 82, 102.5, 123, 143.5, 164, 184.5];
		var chartHeight = 140;
		var mainColor = "rgb(142, 198, 65)";
		var grayColor = "rgb(189, 190, 192)";

		var svgDocument = document.getElementById(chartid);
		var percentPerTen = percent / 10;

		for (i = 0; i < 10; i++) {
			var currentColumnWidth = growthChartColumnWidth;
			var currentColumnColor = mainColor;
			if(i <= percentPerTen && percentPerTen < i+1) {
				currentColumnWidth = (percent%10)/10 * growthChartColumnWidth;
			} else if (i > percentPerTen) {
				currentColumnColor = grayColor;
			}

			var shape = document.createElementNS(svgns,"rect");
			shape.setAttributeNS(null, "x", growthChartHorizonPosition[i]);
			shape.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
			shape.setAttributeNS(null, "width", currentColumnWidth);
			shape.setAttributeNS(null, "height", growthChartColumnHeight[i]);
			shape.setAttributeNS(null, "fill", currentColumnColor);

			var shape1 = null;
			if(currentColumnWidth < growthChartColumnWidth) {
				shape1 = document.createElementNS(svgns,"rect");
				shape1.setAttributeNS(null, "x", growthChartHorizonPosition[i] + currentColumnWidth - 0.5);
				shape1.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
				shape1.setAttributeNS(null, "width", growthChartColumnWidth - currentColumnWidth);
				shape1.setAttributeNS(null, "height", growthChartColumnHeight[i]);
				shape1.setAttributeNS(null, "fill", grayColor);
			}

			svgDocument.appendChild(shape);
			if(shape1 != null) svgDocument.appendChild(shape1);
		}
	}
</script>