<?php
if ($is_print == true){
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $page['title'] ?></title>
    <!--	--><?php //foreach ($page['stylesheets'] as $item) {
    ?>
    <!--		<link rel="stylesheet" href="--><?//= base_url() . $item
    ?><!--"/>-->
    <!--	--><?php //}
    ?>

    <link rel="stylesheet" href="<?php echo site_url('res/css/infographic/style.css') ?>"/>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/jquery.circliful.js'); ?>"></script>
    <?php foreach ($page['scripts'] as $item) { ?>
        <script src="<?= base_url() . $item ?>"></script>
    <?php } ?>
    <style type="text/css" media="print">
        .textbsheader {
            font-size: 50px;
        }
    </style>
</head>
<body>

<div id="infographic" class="text-center">
    <?php }
    if (!intval($infographic[Infographic_model::DATA]['cus_otm']['salesTarget'])) {
        $percent = '100';
    } else {
        $percent = (intval($infographic[Infographic_model::DATA]['cus_otm']['salesAchieved']) / intval($infographic[Infographic_model::DATA]['cus_otm']['salesTarget'])) * 100;
    }
    ?>
    <div class="bsmain">
        <div class="bsheader">
            <div class="textbsheader">
                <?php $dt = DateTime::createFromFormat('!m', $this->session->userdata('infographicMonth')); ?>
                <a href="<?php echo site_url('/') ?>"><?php echo $country_name; ?> -
                    PULL <?php echo_isset($dt->format('M') . ', ' . $this->session->userdata('infographicYear')) ?></a>
            </div>
        </div>
        <div id="colum1">
            <div class="leftbscontantmalaypd">
                <div class="leftbscontantmalaypdtable1">

                    <a class="textbstopsku" href="<?= site_url('ami/infographic/customer_data'); ?>"
                       data-toggle="ajaxModal">
                        <span class="inner_title">Active Customers (grip)</span></a>


                    <div class="bscustomerdata">
                        <div class="leftbscustomerdata">
                            <div class="txtleftbscustomerdata1">
                                <p class="textp1"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][0]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][0]['value']) : 0 ?></p>

                                <p class="txtp1">buying customers</p>
                            </div>
                            <div class="txtleftbscustomerdata2">
                                <p class="textp2"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][1]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][1]['value']) : 0 ?></p>

                                <p class="txtp2">buying customers</p>
                            </div>
                            <div class="txtleftbscustomerdata3">
                                <p class="textp3"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][2]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][2]['value']) : 0 ?></p>

                                <p class="txtp3">buying customers</p>
                            </div>
                            <div class="txtleftbscustomerdata4">
                                <p class="textp4"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][3]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['value']) : 0 ?></p>

                                <p class="txtp4">buying customers</p>
                            </div>
                            <!-- <div class="txtleftbscustomerdata5_t1">
				<p class="textp5_t1"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][4]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['value']) : 0 ?></p>
				<p class="txtp5_t1">total customers</p>
			</div> -->
                        </div>
                        <div class="centerbscustomerdata">
                            <p><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][0]['label']) ? $infographic[Infographic_model::DATA]['otm']['otm'][0]['label'] : 'A' ?></p>

                            <p><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][1]['label']) ? $infographic[Infographic_model::DATA]['otm']['otm'][1]['label'] : 'B' ?></p>

                            <p><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][2]['label']) ? $infographic[Infographic_model::DATA]['otm']['otm'][2]['label'] : 'C' ?></p>

                            <p><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][3]['label']) ? $infographic[Infographic_model::DATA]['otm']['otm'][3]['label'] : 'D' ?></p>
                            <!-- <p>UN</p> -->

                        </div>
                        <div class="rightbscustomerdata">
                            <div class="txtrightbscustomerdata1">
                                <p class="textp5"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][0]['percentage']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][0]['percentage'], 1) : 0 ?>
                                    %</p>

                                <p class="txtp5">of<br/>buying customers</p>
                            </div>
                            <div class="txtrightbscustomerdata2">
                                <p class="textp6"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][1]['percentage']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][1]['percentage'], 1) : 0 ?>
                                    %</p>

                                <p class="txtp6">of<br/>buying customers</p>
                            </div>
                            <div class="txtrightbscustomerdata3">
                                <p class="textp7"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][2]['percentage']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][2]['percentage'], 1) : 0 ?>
                                    %</p>

                                <p class="txtp7">of<br/>buying customers</p>
                            </div>
                            <div class="txtrightbscustomerdata4">
                                <p class="textp8"><?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][3]['percentage']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][3]['percentage'], 1) : 0 ?>
                                    %</p>

                                <p class="txtp8">of<br/>buying customers</p>
                            </div>
                            <!-- <div class="txtrightbscustomerdata5_t2">
				<p class="textp5_t2"><?php //echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['percentage'], 1), 0) ?>%</p>
				<p class="txtp5_t2">of<br />total customers</p>
			</div> -->
                        </div>
                    </div>
                    <p class="textbottomcustomerdata">
		<span class="txtrightbscustomerdata-un">Unclassified OTM Total number of active buying customers:
            <?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][4]['value']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['value']) : 0 ?>
            (<?php echo isset($infographic[Infographic_model::DATA]['otm']['otm'][4]['percentage']) ? number_format($infographic[Infographic_model::DATA]['otm']['otm'][4]['percentage'], 1) : 0 ?>
            %)
		</span>
                        <br><br>
                        <?php echo_isset(number_format($infographic[Infographic_model::DATA]['otm']['total'])) ?> Total
                        Customers
                    </p>
                </div>

                <div class="leftbscontantmalaypdtable2">
                    <a class="textbssaledperformance" href="<?= site_url('ami/infographic/customer_type'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">TFO BY CHANNEL</span></a>

                    <div class="tbsalesperformance">
                        <div class="customertype-chartcontent">
                            <div class="ct-text">
                                <p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][0]['typeName']); ?></p>
                            </div>
                            <div class="ct-rightchart">
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart1" x="10" y="7" height="20" width="200"
                                              style="fill: #D91F5D"/>
                                    </svg>
                                    <svg id="polygon1" y="7">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D"/>

                                    </svg>
                                    <svg id="textrect" y="12">
                                        <text x="25" y="10" fill="#ffffff" text-anchor="end" font-size="12"
                                              font-family="mohave">
                                            <?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][0]['quantity'], 0)); ?>
                                        </text>
                                    </svg>
                                    <svg id="textrect11" y="12">
                                        <text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">Total
                                            TFO
                                        </text>
                                    </svg>
                                </svg>
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart2" x="10" y="5" height="20" width="200"
                                              style="fill: #8EC641"/>
                                    </svg>
                                    <svg id="polygon2" y="5">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641"/>
                                    </svg>
                                    <svg id="textrect2" y="10">
                                        <text x="40" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset($this->country_currency . ' ' . number_format($infographic[Infographic_model::TYPE]['top3'][0]['valueData'], 2)); ?>
                                        </text>
                                    </svg>
                                    <svg id="textrect11" y="10">
                                        <text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">Total
                                            Value
                                        </text>
                                    </svg>
                                </svg>

                            </div>
                        </div>
                        <div class="customertype-chartcontent">
                            <div class="ct-text">
                                <p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][1]['typeName']); ?></p>
                            </div>
                            <div class="ct-rightchart">
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart3" x="10" y="7" height="20" width="200"
                                              style="fill: #D91F5D"/>
                                    </svg>
                                    <svg id="polygon3" y="7">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D"/>
                                    </svg>
                                    <svg id="textrect3" y="12">
                                        <text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][1]['quantity'], 0)); ?>
                                        </text>
                                    </svg>
                                </svg>
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart4" x="10" y="5" height="20" width="200"
                                              style="fill: #8EC641"/>
                                    </svg>
                                    <svg id="polygon4" y="5">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641"/>
                                    </svg>
                                    <svg id="textrect4" y="10">
                                        <text x="30" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset($this->country_currency . ' ' . number_format($infographic[Infographic_model::TYPE]['top3'][1]['valueData'], 2)); ?>
                                        </text>
                                    </svg>
                                </svg>

                            </div>
                        </div>
                        <div class="customertype-chartcontent">
                            <div class="ct-text">
                                <p><?php echo_isset($infographic[Infographic_model::TYPE]['top3'][2]['typeName']); ?></p>
                            </div>
                            <div class="ct-rightchart">
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart5" x="10" y="7" height="20" width="200"
                                              style="fill: #D91F5D"/>
                                    </svg>
                                    <svg id="polygon5" y="7">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D"/>
                                    </svg>
                                    <svg id="textrect5" y="12">
                                        <text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset(number_format($infographic[Infographic_model::TYPE]['top3'][2]['quantity'], 0)); ?>
                                        </text>
                                    </svg>
                                </svg>
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart6" x="10" y="5" height="20" width="200"
                                              style="fill: #8EC641"/>
                                    </svg>
                                    <svg id="polygon6" y="5">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641"/>
                                    </svg>
                                    <svg id="textrect6" y="10">
                                        <text x="40" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset($this->country_currency . ' ' . number_format($infographic[Infographic_model::TYPE]['top3'][2]['valueData'], 2)); ?>
                                        </text>
                                    </svg>
                                </svg>
                            </div>
                        </div>
                        <div class="customertype-chartcontent">
                            <div class="ct-text">
                                <p>Others</p>
                            </div>
                            <div class="ct-rightchart">
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart7" x="10" y="7" height="20" width="200"
                                              style="fill: #D91F5D"/>
                                    </svg>
                                    <svg id="polygon7" y="7">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#D91F5D"/>
                                    </svg>
                                    <svg id="textrect7" y="12">
                                        <text x="15" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset(number_format($infographic[Infographic_model::TYPE]['other']['totalTfo'], 0)); ?>
                                        </text>
                                    </svg>
                                </svg>
                                <svg style="width:100%; height:30px">
                                    <svg x="0">
                                        <rect id="linechart8" x="10" y="5" height="20" width="200"
                                              style="fill: #8EC641"/>
                                    </svg>
                                    <svg id="polygon8" y="5">
                                        <polygon points="0 0,0 0,0 20, 10 20" style="fill:#8EC641"/>
                                    </svg>
                                    <svg id="textrect8" y="10">
                                        <text x="30" y="10" fill="#ffffff" font-size="12" font-family="mohave">
                                            <?php echo_isset($this->country_currency . ' ' . number_format($infographic[Infographic_model::TYPE]['other']['totalValue'], 2)); ?>
                                        </text>
                                    </svg>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <!---------------->
                <div class="leftcontantmalaypdtable3">
                    <a class="textrichmedia" href="<?= site_url('ami/infographic/rich_media'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">Rich Media</span></a>

                    <div class="tbrichmedia">
                        <!--                        <a style="margin-left: 0px; margin-top: 10px;"-->
                        <!--                           href="-->
                        <? //= site_url('ami/infographic/rich_media_2'); ?><!--"-->
                        <!--                           data-toggle="ajaxModal">-->
                        <div class="toptbrichmedia">
                            <div class="mntbrichmedia toptbrichmedia1">
                                <p class="textbrichmediat1">Armstrong has <br/> a total of</p>

                                <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][1]['quantity']) ?></p>

                                <p class="textbrichmediat3">SKUS</p>
                            </div>
                            <div class="mntbrichmedia toptbrichmedia2">
                                <p class="textbrichmediat1">Armstrong has</br /> a total of</p>

                                <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][4]['quantity']) ?></p>

                                <p class="textbrichmediat3">Selling<br/> Stories</p>
                            </div>
                            <div class="mntbrichmedia toptbrichmedia3">
                                <p class="textbrichmediat1">Armstrong has<br/>a total of</p>

                                <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][3]['quantity']) ?></p>

                                <p class="textbrichmediat3">Videos</p>
                            </div>
                            <div class="mntbrichmedia toptbrichmedia4">
                                <p class="textbrichmediat1">Armstrong has<br/>a total of</p>

                                <p class="textbrichmediat2"><?php echo_isset($infographic[Infographic_model::TYPE]['media'][2]['quantity']) ?></p>

                                <p class="textbrichmediat3">Recipes</p>
                            </div>
                        </div>
                        <!--                        </a>-->
                        <!--                        <a style="margin-left: 0px; margin-top: 10px;"-->
                        <!--                           href="-->
                        <? //= site_url('ami/infographic/rich_media_1'); ?><!--"-->
                        <!--                           data-toggle="ajaxModal">-->
                        <div class="bottomtbrichmedia">
                            <div class="leftbottombrichmedia">
                                <p class="textleftbottombrichmedia1">
                                    A total
                                    of <?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalCustomerHaveSampled'])) ?>
                                    customers
                                </p>

                                <p class="textleftbottombrichmedia2">
                                    have sampled our products
                                </p>

                                <div class="imgleftbottombrichmedia">
                                    <div class="imgleftbottombrichmedia1"></div>
                                    <div class="imgleftbottombrichmedia2"></div>
                                    <p class="textimgleftbottombrichmedia"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingTimes'])) ?></p>
                                    <span class="text-below-number">SAMPLING ACTIVITIES</span>
                                </div>
                                <div id="rich-media-left-detail-info-home">
                                    <ul>
                                        <li><?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsWet']), 0); ?>
                                            <br/>
                                            <div>WET</div>
                                        </li>
                                        <li><?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsDry']), 0); ?>
                                            <br/>
                                            <div>DRY</div>
                                        </li>
                                        <li><?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsCd']), 0); ?>
                                            <br/>
                                            <div>DEMO</div>
                                        </li>
                                    </ul>
                                </div>
                                <p class="textleftbottomrichmedia2">
                                    SAMPLING ITEMS
                                </p>
                            </div>
                            <div class="righbottomtbrichmedia">
                                <p class="textrightbottombrichmedia1">
                                    A total
                                    of <?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalPantryCheckTimes'])) ?>
                                </p>

                                <p class="textrightbottombrichmedia2">
                                    Pantry checks conducted
                                </p>

                                <div class="imgrightbottombrichmedia">
                                    <div class="imgrightbottombrichmedia1"></div>
                                    <p class="textimgrightbottombrichmedia"><?php echo_isset(number_format($infographic[Infographic_model::DATA]['sampling']['totalPantryCheckItems'])) ?></p>
                                    <span class="text-below-number blue">PANTRY CHECK ITEMS</span>
                                </div>
                            </div>
                        </div>
                        <!--                        </a>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="colum2">
            <div class="rightbscontantmalaypd">
                <div class="rightbscontantmalaypdtable1">
                    <a class="textbssalesperformance1" href="<?= site_url('ami/infographic/cal_data_bl1'); ?>"
                       data-toggle="ajaxModal">
                        <span class="inner_title">Call Data</span></a>

                    <div id="rightbscalldata1">
                        <!--                        <a data-toggle="ajaxModal" class="a_total_call"-->
                        <!--                           href="-->
                        <?php //echo site_url('ami/infographic/call_data_total') ?><!--">-->
                        <div id="contantbstotal">
                            <div class="ct_bstotal bstotal1">
                                <h1>A</h1>

                                <h2>HAS A TOTAL OF</h2>

                                <h3><?php
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'])) {
                                        echo(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA']));
                                    } else {
                                        echo 0;
                                    }
                                    ?></h3>
                                <h4>CALLS</h4>
                            </div>
                            <div class="ct_bstotal bstotal2">
                                <h1>B</h1>

                                <h2>HAS A TOTAL OF</h2>

                                <h3><?php
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'])) {
                                        echo(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB']));
                                    } else {
                                        echo 0;
                                    }
                                    ?></h3>
                                <h4>CALLS</h4>
                            </div>
                            <div class="ct_bstotal bstotal3">
                                <h1>C</h1>

                                <h2>HAS A TOTAL OF</h2>

                                <h3><?php
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'])) {
                                        echo(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC']));
                                    } else {
                                        echo 0;
                                    }
                                    ?></h3>
                                <h4>CALLS</h4>
                            </div>
                            <div class="ct_bstotal bstotal4">
                                <h1>D</h1>

                                <h2>HAS A TOTAL OF</h2>

                                <h3><?php
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'])) {
                                        echo(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD']));
                                    } else {
                                        echo 0;
                                    }
                                    ?></h3>
                                <h4>CALLS</h4>
                            </div>
                            <div class="ct-un-total">
                                Unclassified OTM has a total of <?php
                                if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmU'])) {
                                    echo(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmU']));
                                } else {
                                    echo 0;
                                }
                                ?> Calls
                            </div>
                            <!-- <div class="ct_bstotal bstotal5">
				<h1>UN</h1>
				<h2>HAS A TOTAL OF</h2>
				<h3><?php //echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['timesCallOtmU']), 0) ?></h3>
				<h4>CALLS</h4>
			</div> -->
                            <div class="imgbstotal">
                                <img src="<?php echo site_url('res/img/infographic/total5.png'); ?>"/>
                            </div>
                            <p><?php
                                if (isset($infographic[Infographic_model::CALL_RECORD]['totalCall'])) {
                                    echo(number_format($infographic[Infographic_model::CALL_RECORD]['totalCall']));
                                } else {
                                    echo 0;
                                }
                                ?></p>
                            CALLS IN TOTAL
                        </div>
                        <!--                        </a>-->
                    </div>

                    <div id="rightbscalldata2">
                        <!--                        <a data-toggle="ajaxModal" class="a_call_frequency"-->
                        <!--                           href="-->
                        <?php //echo site_url('ami/infographic/call_data_2') ?><!--">-->
                        <div class="mn_bsdata1 bstabledata1">
                            <h1>OTM A</h1>

                            <h2>call frequency</h2>

                            <div class="bsnumber">
                                <p><?php
                                    $frequencyA = 0;
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmA']) && isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'])) {
                                        if ($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] > 0 && $infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'] > 0) {
                                            $frequencyA = $infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmA'];
                                        }
                                    }
                                    echo number_format($frequencyA, 2)
                                    ?></p>
                            </div>
                        </div>
                        <div class="mn_bsdata1 bstabledata2">
                            <h1>OTM B</h1>

                            <h2>call frequency</h2>

                            <div class="bsnumber">
                                <!-- <p><?php //echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmB']), 1) ?></p> -->
                                <p><?php
                                    $frequencyB = 0;
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmB']) && isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'])) {
                                        if ($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] > 0 && $infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] > 0) {
                                            $frequencyB = $infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'];
                                        }
                                    }
                                    echo number_format($frequencyB, 2)
                                    ?></p>
                            </div>
                        </div>
                        <div class="mn_bsdata1 bstabledata3">
                            <h1>OTM C</h1>

                            <h2>call frequency</h2>

                            <div class="bsnumber">
                                <!-- <p><?php //echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmC']), 1) ?></p> -->
                                <p><?php
                                    $frequencyC = 0;
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmC']) && isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'])) {
                                        if ($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] > 0 && $infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] > 0) {
                                            $frequencyC = $infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'];
                                        }
                                    }
                                    echo number_format($frequencyC, 2)
                                    ?></p>
                            </div>
                        </div>
                        <div class="mn_bsdata1 bstabledata4">
                            <h1>OTM D</h1>

                            <h2>call frequency</h2>

                            <div class="bsnumber">
                                <!-- <p><?php //echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmD']), 1) ?></p> -->
                                <p><?php
                                    $frequencyD = 0;
                                    if (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmD']) && isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'])) {
                                        if ($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] > 0 && $infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] > 0) {
                                            $frequencyD = $infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'];
                                        }
                                    }
                                    echo number_format($frequencyD, 2)
                                    ?></p>
                            </div>
                        </div>
                        <div class="mn-un-total">
                            Unclassified OTM call frequency:
                            <?php
                            $frequencyU = 0;
                            if (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmU']) && isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmU'])) {
                                if ($infographic[Infographic_model::CALL_RECORD]['visitedOtmU'] > 0 && $infographic[Infographic_model::CALL_RECORD]['timesCallOtmU'] > 0) {
                                    $frequencyU = $infographic[Infographic_model::CALL_RECORD]['timesCallOtmU'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmU'];
                                }
                            }
                            echo number_format($frequencyU, 2)
                            ?>
                        </div>
                        <!-- <div class="mn_bsdata1 bstabledata5">
			<h1>UN</h1>
			<h2>call frequency</h2>
			<div class="bsnumber">
				<p><?php // echo number_format(($infographic[Infographic_model::CALL_RECORD]['timesCallOtmU'] / $infographic[Infographic_model::CALL_RECORD]['visitedOtmU']), 1) ?></p>
			</div>
		</div> -->
                        <!--                        </a>-->
                    </div>
                </div>
                <div class="rightbscontantmalaypdtable2">
                    <a class="textbssalesteam" href="<?= site_url('ami/infographic/cal_data_bl2'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">Call Data</span></a>
                    <?php
                    $totalCall = isset($infographic[Infographic_model::CALL_RECORD]['totalCall']) ? $infographic[Infographic_model::CALL_RECORD]['totalCall'] : 0;
                    $totalCallWithOutTfo = isset($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo']) ? $infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'] : 0;
                    if (isset($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) {
                        $totalCallWithTfo = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'];
                        $strikeRate = ($totalCallWithTfo > 0 && $totalCall > 0) ? $totalCallWithTfo / $totalCall * 100 : 0;
                    } else {
                        $strikeRate = $totalCallWithTfo = 0;
                    }
                    ?>
                    <div class="calldata-piechart">
                        <!--                        <a data-toggle="ajaxModal" href="-->
                        <?php //echo site_url('ami/infographic/strike_rate') ?><!--">-->
                        <div id="calldata-chart" data-dimension="260" data-width="50" data-fontsize="15"
                             data-percent="65"
                             data-fgcolor="#2BACE2" data-bgcolor="#818285" data-animationstep="0">
                        </div>
                        <div class="calldata-rightchartinfo">
                            <div class="calldata-toppie">
                                <div class="calldata-leftctpie">
                                    <p class="calldata-textleftctpie1">TOTAL CALLS</p>

                                    <p class="calldata-textleftctpie2">CALLS WITH ORDERS</p>

                                    <p class="calldata-textleftctpie3">CALLS WITHOUT ORDERS</p>
                                </div>
                                <div class="calldata-rightctpie">
                                    <p class="calldata-textrightctpie1"><?php echo_isset(number_format($totalCall)) ?></p>

                                    <p class="calldata-textrightctpie2"><?php echo_isset(number_format($totalCallWithTfo)) ?></p>

                                    <p class="calldata-textrightctpie3"><?php echo_isset(number_format($totalCallWithOutTfo)) ?></p>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="calldata-bottompie">
                                <div class="calldata-leftbottompie">
                                    <p class="calldata-textleftbtpie1"><?php echo_isset(number_format($strikeRate, 0)); ?>
                                        %</p>

                                    <p class="calldata-textleftbtpie2">STRIKE RATE</p>
                                </div>
                            </div>
                        </div>
                        <div class="calldata-abtext1" id="abtext1">
                            <p class="calldata-abtextcall">CALLS WITHOUT ORDERS</p>

                            <p class="calldata-abnumcall"><?php echo $totalCallWithOutTfo ?></p>
                        </div>
                        <div class="calldata-abtext2" id="abtext2">
                            <p class="calldata-abtextcall1">CALLS WITH ORDERS</p>

                            <p class="calldata-abnumcall1"><?php echo $totalCallWithTfo ?></p>
                        </div>
                        <div class="calldata-abtext3">
                            <p><span id="piechart-percent"><?php echo_isset(number_format($strikeRate, 0)); ?>
                                    %</span><br/>STRIKE
                                RATE</p>
                        </div>
                        <!--                        </a>-->
                    </div>

                    <!-- growth chart -->

                    <?php
                    $otmAPercent = $otmBPercent = $otmCPercent = $otmDPercent = 0;
                    $customerOtmA = $customerOtmB = $customerOtmC = $customerOtmD = 0;
                    $visitedOtmA = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] : 0;
                    $visitedOtmB = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] : 0;
                    $visitedOtmC = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] : 0;
                    $visitedOtmD = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] : 0;
                    if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmA'])) {
                        $customerOtmA = $infographic[Infographic_model::CALL_RECORD]['customerOtmA'];
                        $otmAPercent = ($customerOtmA > 0 && $visitedOtmA > 0)
                            ? $visitedOtmA / $customerOtmA * 100
                            : 0;
                    }
                    if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmB'])) {
                        $customerOtmB = $infographic[Infographic_model::CALL_RECORD]['customerOtmB'];
                        $otmBPercent = ($customerOtmB > 0 && $visitedOtmB > 0)
                            ? $visitedOtmB / $customerOtmB * 100
                            : 0;
                    }
                    if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmC'])) {
                        $customerOtmC = $infographic[Infographic_model::CALL_RECORD]['customerOtmC'];
                        $otmCPercent = ($customerOtmC > 0 && $visitedOtmC > 0)
                            ? $visitedOtmC / $customerOtmC * 100
                            : 0;
                    }
                    if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmD'])) {
                        $customerOtmD = $infographic[Infographic_model::CALL_RECORD]['customerOtmD'];
                        $otmDPercent = ($customerOtmD > 0 && $visitedOtmD > 0)
                            ? $visitedOtmD / $customerOtmD * 100
                            : 0;
                    }
                    ?>
                    <div id="growthchart">
                        <div class="growthchart-chartbar12">
                            <!--			<a data-toggle="ajaxModal" href="-->
                            <?php //echo site_url('infographic/grow_chart') ?><!--">-->
                            <div class="growthchart-chartindex1">
                                <img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50"
                                     height="59"/>

                                <p id="growthchart-textct1"><?php echo_isset(number_format($otmAPercent, 0)); ?>%</p>
                                <svg id="growthchart-index1" width="150" height="106">
                                </svg>
                                <p class="growthchart-idex1textchart1"> target is 100%</p>

                                <p class="growthchart-idex1textchart2"> OTM A coverage</p>

                                <div class="growthchart-index1textchart3">
                                    <div class="growthchart-leftindex1tc1">
                                        <p><?php echo_isset(number_format($visitedOtmA, 0, '', '')); ?></p>
                                        <a>visited customer</a>
                                    </div>
                                    <div class="growthchart-centerindex1">
                                        <p>/</p>
                                    </div>
                                    <div class="growthchart-rightindex1tc2">
                                        <p><?php echo_isset(number_format($customerOtmA, 0, '', '')); ?></p>
                                        <a>total customers</a>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <!--<a data-toggle="ajaxModal" href="<?php // echo site_url('ami/infographic/grow_chart') ?>">-->
                            <div class="growthchart-chartindex2">
                                <img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50"
                                     height="59"/>

                                <p id="growthchart-textct2"><?php echo_isset(number_format($otmBPercent, 0)); ?>
                                    %</p>
                                <svg id="growthchart-index2" width="150" height="106">
                                </svg>
                                <p class="growthchart-index2textchart1"> target is 100%</p>

                                <p class="growthchart-index2textchart2"> OTM B coverage</p>

                                <div class="growthchart-index1textchart3">
                                    <div class="growthchart-leftindex1tc1">
                                        <p><?php echo_isset(number_format($visitedOtmB, 0, '', '')); ?></p>
                                        <a>visited customer</a>
                                    </div>
                                    <div class="growthchart-centerindex1">
                                        <p>/</p>
                                    </div>
                                    <div class="growthchart-rightindex1tc2">
                                        <p> <?php echo_isset(number_format($customerOtmB, 0, '', '')); ?> </p>
                                        <a>total customers</a>
                                    </div>
                                </div>
                            </div>
                            <!--</a>-->
                        </div>
                        <div class="growthchart-chartbar34">
                            <!--<a data-toggle="ajaxModal" href="<?php // echo site_url('ami/infographic/grow_chart') ?>">-->
                            <div class="growthchart-chartindex3">
                                <img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50"
                                     height="59"/>

                                <p id="growthchart-textct3"><?php echo_isset(number_format($otmCPercent, 0)); ?>
                                    %</p>
                                <svg id="growthchart-index3" width="150" height="106">
                                </svg>
                                <p class="growthchart-idex3textchart2"> OTM c coverage</p>

                                <div class="growthchart-index3textchart3">
                                    <div class="growthchart-leftindex1tc1">
                                        <p><?php echo_isset(number_format($visitedOtmC, 0, '', '')); ?></p>
                                        <a>visited customer</a>
                                    </div>
                                    <div class="growthchart-centerindex1">
                                        <p>/</p>
                                    </div>
                                    <div class="growthchart-rightindex1tc2">
                                        <p> <?php echo_isset(number_format($customerOtmC, 0, '', '')); ?> </p>
                                        <a>total customers</a>
                                    </div>
                                </div>
                            </div>
                            <!--</a>-->
                            <!--<a data-toggle="ajaxModal" href="<?php // echo site_url('ami/infographic/grow_chart') ?>">-->
                            <div class="growthchart-chartindex4">
                                <img src="<?php echo site_url('res/img/infographic/pic.png'); ?>" width="50"
                                     height="59"/>

                                <p id="growthchart-textct4"><?php echo_isset(number_format($otmDPercent, 0)); ?>
                                    %</p>
                                <svg id="growthchart-index4" width="150" height="106">
                                </svg>
                                <p class="growthchart-index4textchart2"> OTM d coverage</p>

                                <div class="growthchart-index4textchart3">
                                    <div class="growthchart-leftindex1tc1">
                                        <p><?php echo_isset(number_format($visitedOtmD, 0, '', '')); ?></p>
                                        <a>visited customer</a>
                                    </div>
                                    <div class="growthchart-centerindex1">
                                        <p>/</p>
                                    </div>
                                    <div class="growthchart-rightindex1tc2">
                                        <p> <?php echo_isset(number_format($customerOtmD, 0, '', '')); ?> </p>
                                        <a>total customers</a>
                                    </div>
                                </div>
                            </div>
                            <!--</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="colum3">
            <div class="leftcontantmalaypd">
                <div class="leftcontantmalaypdtable1">
                    <a class="texttopsku" href="<?= site_url('ami/infographic/top_sku'); ?>" data-toggle="ajaxModal">
                        <span class="inner_title">Top Penetration</span></a>

                    <div class="tbtopskut">
                        <div class="top-sku-background-home">
                            <p class="top-sku-title">TOP Penetration</p>

                            <div class="top-sku-inside-home">
                                <ul class="top-sku-list-home">
                                    <?php foreach ($infographic[Infographic_model::TOP_SKU]['top3'] as $main_sku => $top_sku): ?>
                                        <li>
                                            <div class="name-block">
                                                <?php if (isset($top_sku['main'])) {
                                                    if ($top_sku['main']['group_name']) { ?>
                                                        <p class="top-sku-name"><?php echo_isset($top_sku['main']['group_name']) ?></p>
                                                    <?php } else { ?>
                                                        <p class="top-sku-name"><?php echo_isset($top_sku['main']['sku_name']) ?></p>
                                                        <p class="top-sku-packing-size"><?php echo isset($top_sku['main']['pakingSize']) ? "({$top_sku['main']['pakingSize']})" : "" ?></p>
                                                    <?php }
                                                }
                                                /*else {
                                                    if (isset($top_sku['child'])) {
                                                        foreach ($top_sku['child'] as $child) {
                                                            ?>
                                                            <p class="top-sku-name"><?php echo_isset($child['sku_name']) ?></p>
                                                            <p class="top-sku-packing-size"><?php echo isset($child['pakingSize']) ? "({$child['pakingSize']})" : "" ?></p>
                                                        <?php }
                                                    }
                                                } */
                                                ?>
                                            </div>
                                            <div class="percent-block">
                                                <?php
                                                    if (isset($top_sku['main'])) {
                                                        $topskupercent = ($top_sku['main']['totalCustomer'] > 0 && $top_sku['main']['totalCustomerOrder'] > 0)
                                                            ? $top_sku['main']['totalCustomerOrder'] / $top_sku['main']['totalCustomer'] * 100
                                                            : 0;
                                                        echo '<p class="top-sku-percent">[' . number_format($topskupercent) . '%]';
                                                    }
                                                    /*else {
                                                        if (isset($top_sku['child'])) {
                                                            foreach ($top_sku['child'] as $child) {
                                                                $topskupercent = ($child['totalCustomer'] > 0 && $child['totalCustomerOrder'] > 0)
                                                                    ? $child['totalCustomerOrder'] / $child['totalCustomer'] * 100
                                                                    : 0;
                                                                echo '<p class="top-sku-percent">[' . $topskupercent . '%]';
                                                            }
                                                        }
                                                    }*/
//                                                    echo_isset(number_format($topskupercent), 0)
                                                    ?>
                                            </div>
                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leftcontantmalaypdtable2">
                    <a class="textsaledperformance" href="<?= site_url('ami/infographic/sales_performance_2'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">TFO performance</span></a>

                    <div class="tbtopskut2">
                        <div class="mntbtopskut2">
                            <div class="mn_skut mntbtopskut21">
                                <div class="mntbtopskut21top">
                                    <div class="mnpie1">
                                        <p class="red1"><?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip']) : 0 ?></p>

                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie2">
                                        <p class="red1"><?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrip']) : 0 ?></p>

                                        <p class="white1">new grip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut21center">
                                    <p class="mntbtopsku-middle"
                                       title="<?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName'] : 0; ?>">
                                        <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][0]['countryChannelsName'] : 0; ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut21bottom">
                                    <div class="mnpie3">
                                        <p class="red1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab']) : 0 ?>

                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie4">
                                        <p class="red1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['newGrab']) : 0 ?>

                                        <p class="white1">new grab</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut21center">
                                    <p class="textblack1 small">
                                        <?php
                                        $averageGrabA = 0;
                                        if (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab']) && isset($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip'])) {
                                            if ($infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab'] > 0 && $infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip'] > 0) {
                                                $averageGrabA = $infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrab'] / $infographic[Infographic_model::GRIP_GRAB]['top3'][0]['totalGrip'];
                                            }
                                        }
                                        echo_isset(number_format($averageGrabA, 2));
                                        ?>

                                    <div class="average-grab">Average Grab</div>
                                    </p>
                                </div>
                            </div>
                            <div class="mn_skut mntbtopskut22">
                                <div class="mntbtopskut22top">
                                    <div class="mnpie2_1">
                                        <p class="blue1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip']) : 0 ?>

                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie2_2">
                                        <p class="blue1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrip']) : 0 ?>

                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut22center">
                                    <p class="mntbtopsku-middle"
                                       title="<?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'] : 0 ?>">
                                        <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'] : 0 ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut22bottom">
                                    <div class="mnpie2_3">
                                        <p class="blue1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab']) : 0 ?>

                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie2_4">
                                        <p class="blue1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['newGrab']) : 0 ?>

                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut22center">
                                    <p class="textblack1 small"
                                       title="<?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['countryChannelsName'] : 0 ?>">
                                        <?php
                                        $averageGrabB = 0;
                                        if (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab']) && isset($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip'])) {
                                            if ($infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab'] > 0 && $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip'] > 0) {
                                                $averageGrabB = $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrab'] / $infographic[Infographic_model::GRIP_GRAB]['top3'][1]['totalGrip'];
                                            }
                                        }
                                        echo_isset(number_format($averageGrabB, 2));
                                        ?>

                                    <div class="average-grab">Average Grab</div>
                                    </p>
                                </div>
                            </div>
                            <div class="mn_skut mntbtopskut23">
                                <div class="mntbtopskut23top">
                                    <div class="mnpie3_1">
                                        <p class="green1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip']) : 0 ?>

                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie3_2">
                                        <p class="green1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrip'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrip']) : 0 ?>

                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut23center">
                                    <p class="mntbtopsku-middle"
                                       title="<?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName'] : 0 ?>">
                                        <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName'])) ? $infographic[Infographic_model::GRIP_GRAB]['top3'][2]['countryChannelsName'] : 0 ?>
                                    </p>
                                </div>
                                <div class="mntbtopskut23bottom">
                                    <div class="mnpie3_3">
                                        <p class="green1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab']) : 0 ?>

                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie3_4">
                                        <p class="green1">
                                            <?php echo (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrab'])) ? number_format($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['newGrab']) : 0 ?>

                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut23center">
                                    <p class="textblack1 small">
                                        <?php
                                        $averageGrabC = 0;
                                        if (isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab']) && isset($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip'])) {
                                            if ($infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab'] > 0 && $infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip'] > 0) {
                                                $averageGrabC = $infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrab'] / $infographic[Infographic_model::GRIP_GRAB]['top3'][2]['totalGrip'];
                                            }
                                        }
                                        echo_isset(number_format($averageGrabC, 2));
                                        ?>

                                    <div class="average-grab">Average Grab</div>
                                    </p>
                                </div>
                            </div>
                            <div class="mn_skut mntbtopskut24">
                                <div class="mntbtopskut24top">
                                    <div class="mnpie4_1">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip']), 0) ?></p>

                                        <p class="white1">grip</p>
                                    </div>
                                    <div class="mnpie4_2">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrip']), 0) ?></p>

                                        <p class="white1">newgrip</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut24center">
                                    <p class="mntbtopsku-middle">others</p>
                                </div>
                                <div class="mntbtopskut24bottom">
                                    <div class="mnpie4_3">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['totalGrab']), 0) ?></p>

                                        <p class="white1">grab</p>
                                    </div>
                                    <div class="mnpie4_4">
                                        <p class="black1"><?php echo_isset(number_format($infographic[Infographic_model::GRIP_GRAB]['other']['newGrab']), 0) ?></p>

                                        <p class="white1">newgrab</p>
                                    </div>
                                </div>
                                <div class="mntbtopskut24center">
                                    <p class="textblack1 small">
                                        <?php
                                        $averageGrabD = $infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip']
                                            ? $infographic[Infographic_model::GRIP_GRAB]['other']['totalGrab'] / $infographic[Infographic_model::GRIP_GRAB]['other']['totalGrip']
                                            : 0;
                                        echo_isset(number_format($averageGrabD, 2));
                                        ?>

                                    <div class="average-grab">Average Grab</div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tbsalesperformance"></div>
                </div>
                <?php
                if (isset($infographic[Infographic_model::CALL_RECORD]['totalActualWorking'])) {
                    if ($infographic[Infographic_model::CALL_RECORD]['totalCall'] > 0 && $infographic[Infographic_model::CALL_RECORD]['totalActualWorking']) {
                        $averageCallsActualDay = $infographic[Infographic_model::CALL_RECORD]['totalCall'] / $infographic[Infographic_model::CALL_RECORD]['totalActualWorking'];
                    } else {
                        $averageCallsActualDay = 0;
                    }
                } else {
                    $averageCallsActualDay = 0;
                }

                if (array_sum(array_only($infographic[Infographic_model::CALL_RECORD], ['minuteCallOtmA', 'minuteCallOtmB', 'minuteCallOtmC', 'minuteCallOtmD', 'minuteCallOtmU'])) > 0 && ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'] * 8 * 60 * $infographic[Infographic_model::CALL_RECORD]['workingDay']) > 0) {
                    $incallTime = array_sum(array_only($infographic[Infographic_model::CALL_RECORD], ['minuteCallOtmA', 'minuteCallOtmB', 'minuteCallOtmC', 'minuteCallOtmD', 'minuteCallOtmU'])) / ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'] * 8 * 60 * $infographic[Infographic_model::CALL_RECORD]['workingDay']) * 100;
                } else {
                    $incallTime = 0;
                }

                if ($infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels']) {
                    if ($infographic[Infographic_model::CALL_RECORD]['totalActualWorking'] > 0 && $infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels']) {
                        $averageActualWorkingDay = $infographic[Infographic_model::CALL_RECORD]['totalActualWorking'] / $infographic[Infographic_model::DATA]['cus_otm']['numberSalesPersonnels'];
                    } else {
                        $averageActualWorkingDay = 0;
                    }
                } else {
                    $averageActualWorkingDay = 0;
                }
                ?>
                <div class="leftbscontantmalaypdtable3">
                    <a href="<?= site_url('ami/infographic/cal_data_bl3'); ?>" data-toggle="ajaxModal"
                       class="textbsrichmedia"><span class="inner_title">call data</span></a>
                    <!--                    <a class="a_average" href="-->
                    <? //= site_url('ami/infographic/call_data_3'); ?><!--" data-toggle="ajaxModal">-->
                        <span class="inner_average">
                        <div class="bsleftcalldata">
                            <div class="bsleftcalldatatable">
                                <div class="imgcalldatatable">
                                    <p><?php echo_isset(number_format($averageCallsActualDay), 0); ?></p>
                                </div>
                                <h1>total average calls<br/> /actual working day /SR</h1>
                            </div>
                            <div class="bsleftcalldatatable">
                                <div class="imgcalldatatable">
                                    <p><?php
                                        echo_isset(number_format($incallTime), 0);
                                        ?>%</p>
                                </div>
                                <h1>% of time spent with customers in call</h1>
                            </div>
                            <div class="bsleftcalldatatable">
                                <div class="imgcalldatatable">
                                    <p><?php
                                        echo_isset(number_format($averageActualWorkingDay), 0);
                                        ?></p>
                                </div>
                                <h1>average actual<br/>working day / month</h1>
                            </div>
                        </div>
                        </span>
                    <!--                    </a>-->

                    <div class="clearfix"></div>
                    <!--                    <a href="-->
                    <?php //echo site_url('ami/infographic/call_data_1'); ?><!--" data-toggle="ajaxModal">-->
                    <div class="bscalldata">
                        <div id="bscontentcalldata">
                            <ul class="bsmn_ct">
                                <li class="mn_bstb bstable1">
                                    <h1><?php
                                        $minsA = 0;
                                        if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'])) {
                                            if ($infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'] > 0) {
                                                $minsA = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmA'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmA'];
                                            }
                                        }
                                        echo_isset(number_format($minsA, 0, '', ''), 0)
                                        ?></h1>

                                    <h2>mins</h2>

                                    <h3>AVERAGE CALL TIME</h3>

                                    <p>OTM A</p>

                                    <div class="bsotma">

                                    </div>
                                </li>
                                <li class="mn_bstb bstable2">
                                    <h1><?php
                                        $minsB = 0;
                                        if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'])) {
                                            if ($infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'] > 0) {
                                                $minsB = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmB'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmB'];
                                            }
                                        }
                                        echo_isset(number_format($minsB, 0, '', ''), 0)
                                        ?></h1>

                                    <h2>mins</h2>

                                    <h3>AVERAGE CALL TIME</h3>

                                    <p>OTM B</p>

                                    <div class="bsotma">

                                    </div>
                                </li>
                                <li class="mn_bstb bstable3">
                                    <h1><?php
                                        $minsC = 0;
                                        if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'])) {
                                            if ($infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'] > 0) {
                                                $minsC = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmC'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmC'];
                                            }
                                        }
                                        echo_isset(number_format($minsC, 0, '', ''), 0)
                                        ?></h1>

                                    <h2>mins</h2>

                                    <h3>AVERAGE CALL TIME</h3>

                                    <p>OTM C</p>

                                    <div class="bsotma">

                                    </div>
                                </li>
                                <li class="mn_bstb bstable4">
                                    <h1><?php
                                        $minsD = 0;
                                        if (isset($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD']) && isset($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'])) {
                                            if ($infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'] > 0 && $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'] > 0) {
                                                $minsD = $infographic[Infographic_model::CALL_RECORD]['minuteCallOtmD'] / $infographic[Infographic_model::CALL_RECORD]['timesCallOtmD'];
                                            }
                                        }
                                        echo_isset(number_format($minsD, 0, '', ''), 0)
                                        ?></h1>

                                    <h2>mins</h2>

                                    <h3>AVERAGE CALL TIME</h3>

                                    <p>OTM D</p>

                                    <div class="bsotma">

                                    </div>
                                </li>
                                <!-- <li class="mn_bstb bstable5">
						<h1><?php //echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['minuteCallOtmU']/$infographic[Infographic_model::DATA]['otm']['otm'][4]['value'], 0, '', ''), 0) ?></h1>
						<h2>mins</h2>
						<h3>AVERAGE CALL TIME</h3>
						<p>UN</p>
						<div class="bsotma">

						</div>
					</li> -->
                            </ul>
                        </div>
                    </div>
                    <!--                    </a>-->
                </div>
            </div>
        </div>
        <div id="colum4">
            <div class="rightcontantmalaypd">
                <div class="rightcontantmalaypdtable1">
                    <a class="textsalesperformance1" href="<?= site_url('ami/infographic/sales_performance_1'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">TFO performance<span></a>

                    <div class="tbtopskut4">
                        <div class="mntbtopskut4">
                            <div class="tbtopskut4tab1">
                                <p class="txttab1">Total Sales Performance</p>

                                <p class="txttab2"><?php echo_isset($countryCurrency->currency . ' ' . number_format($infographic[Infographic_model::DATA]['cus_otm']['salesAchieved'])) ?></p>
                                <!--						<p class="txttab3">-->
                                <?php //echo_isset(number_format($percent)) ?><!--%</p>-->
                            </div>
                            <!--					<div class="tbtopskut4tab2">-->
                            <!--						<p class="txttab4">/</p>-->
                            <!--					</div>-->
                            <!--					<div class="tbtopskut4tab3">-->
                            <!--						<p class="txttab1">Sales target</p>-->
                            <!--						<p class="txttab2">€-->
                            <?php //echo_isset(number_format($infographic[Infographic_model::DATA]['salesTarget'])) ?><!--</p>-->
                            <!--					</div>-->
                        </div>
                    </div>

                </div>
                <div class="rightcontantmalaypdtable2">
                    <a class="textsalesteam" href="<?php echo site_url('ami/infographic/sales_team'); ?>"
                       data-toggle="ajaxModal"><span class="inner_title">SALES Team</span></a>
                    <!--			<a class="textsalesteam" href="-->
                    <? //= site_url('ami/infographic/sales_team'); ?><!--" data-toggle="ajaxModal">SALES Team</a>-->
                    <div class="tbsalesteam">
                        <div class="toptbsalesteam">
                            <img src="<?php echo site_url($country_map); ?>" alt="Country Map"/>
                        </div>
                        <div class="bottomsalesteam">

                            <div class="block2">
                                <svg id="human-chart" width="339" height="231"
                                >
                                </svg>
                            </div>

                            <div class="block3">
                                <div class="textalign">
                                    <p id="txt1"><?php echo (isset($infographic['sl_team']['numberSalesLeaders'])) ? number_format($infographic['sl_team']['numberSalesLeaders']) : 0; ?>
                                        SALES LEADERS</p>

                                    <p id="txt2"><?php echo (isset($infographic['sl_team']['numberSalesPersonnels'])) ? number_format($infographic['sl_team']['numberSalesPersonnels']) : 0; ?>
                                        SALES PERSONNELS</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $tfo = array(
        $infographic[Infographic_model::TYPE]['top3'][0]['quantity'],
        $infographic[Infographic_model::TYPE]['top3'][1]['quantity'],
        $infographic[Infographic_model::TYPE]['top3'][2]['quantity'],
        $infographic[Infographic_model::TYPE]['other']['totalTfo'],
    );
    $revenue = array(
        $infographic[Infographic_model::TYPE]['top3'][0]['valueData'],
        $infographic[Infographic_model::TYPE]['top3'][1]['valueData'],
        $infographic[Infographic_model::TYPE]['top3'][2]['valueData'],
        $infographic[Infographic_model::TYPE]['other']['totalValue'],
    );

    ?>
    <script>
        $(document).ready(function () {

            //draw human chart
            drawHumanChart(<?php echo (isset($infographic['sl_team']['numberSalesLeaders'])) ? number_format($infographic['sl_team']['numberSalesLeaders']) : 0; ?>, <?php echo (isset($infographic['sl_team']['numberSalesPersonnels'])) ? number_format($infographic['sl_team']['numberSalesPersonnels']) : 0; ?>, "human-chart");
            $("#txt1").text("<?php echo (isset($infographic['sl_team']['numberSalesLeaders'])) ? number_format($infographic['sl_team']['numberSalesLeaders']) : 0; ?> SALES LEADERS");
            $("#txt2").text("<?php echo (isset($infographic['sl_team']['numberSalesPersonnels'])) ? number_format($infographic['sl_team']['numberSalesPersonnels']) : 0; ?> SALES PERSONNELS");

            //piechart
            drawPiechart("calldata-chart", <?php echo_isset(number_format($strikeRate, 0)); ?>);
            //growth chart
            drawGrowthChart(<?php echo_isset(number_format($otmAPercent, 0)); ?>, "growthchart-index1");
            $("#growthchart-textct1").text("<?php echo_isset(number_format($otmAPercent, 0)); ?>%");
            drawGrowthChart(<?php echo_isset(number_format($otmBPercent, 0)); ?>, "growthchart-index2");
            $("#growthchart-textct2").text("<?php echo_isset(number_format($otmBPercent, 0)); ?>%");
            drawGrowthChart(<?php echo_isset(number_format($otmCPercent, 0)); ?>, "growthchart-index3");
            $("#growthchart-textct3").text("<?php echo_isset(number_format($otmCPercent, 0)); ?>%");
            drawGrowthChart(<?php echo_isset(number_format($otmDPercent, 0)); ?>, "growthchart-index4");
            $("#growthchart-textct4").text("<?php echo_isset(number_format($otmDPercent, 0)); ?>%");

            // customer type - line chart
            var maxWidth = 357;
            var maxValue = <?php echo_isset(number_format(max($revenue), 0, '', ''));?>;
            var maxTfo = <?php echo_isset(number_format(max($tfo), 0, '', ''));?>;
            var width = 0;

            var rect1 = document.getElementById("linechart1");
            var polygon1 = document.getElementById("polygon1");
            var textrect = document.getElementById("textrect");
            width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[0], 0, '', '')); ?>);
            drawLineChart(rect1, polygon1, textrect, width);

            var rect2 = document.getElementById("linechart2");
            var polygon2 = document.getElementById("polygon2");
            var textrect = document.getElementById("textrect2");
            width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[0], 0, '', '')); ?>);
            drawLineChart(rect2, polygon2, textrect2, width);

            var rect3 = document.getElementById("linechart3");
            var polygon3 = document.getElementById("polygon3");
            var textrect = document.getElementById("textrect3");
            width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[1], 0, '', '')); ?>);
            drawLineChart(rect3, polygon3, textrect3, width);

            var rect4 = document.getElementById("linechart4");
            var polygon4 = document.getElementById("polygon4");
            var textrect = document.getElementById("textrect4");
            width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[1], 0, '', '')); ?>);
            drawLineChart(rect4, polygon4, textrect4, width);

            var rect5 = document.getElementById("linechart5");
            var polygon5 = document.getElementById("polygon5");
            var textrect = document.getElementById("textrect5");
            width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[2], 0, '', '')); ?>);
            drawLineChart(rect5, polygon5, textrect5, width);

            var rect6 = document.getElementById("linechart6");
            var polygon6 = document.getElementById("polygon6");
            var textrect = document.getElementById("textrect6");
            width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[2], 0, '', '')); ?>);
            drawLineChart(rect6, polygon6, textrect6, width);

            var rect7 = document.getElementById("linechart7");
            var polygon7 = document.getElementById("polygon7");
            var textrect = document.getElementById("textrect7");
            width = calculateWidth(maxWidth, maxTfo, <?php echo_isset(number_format($tfo[3], 0, '', '')); ?>);
            drawLineChart(rect7, polygon7, textrect7, width);

            var rect8 = document.getElementById("linechart8");
            var polygon8 = document.getElementById("polygon8");
            var textrect = document.getElementById("textrect8");
            width = calculateWidth(maxWidth, maxValue, <?php echo_isset(number_format($revenue[3], 0, '', '')); ?>);
            drawLineChart(rect8, polygon8, textrect8, width);


            <?php if($is_print == true){?>
            $('.textbottomcustomerdata').css('margin-top', '30px');
            $('#growthchart-textct1').css('left', '50px');
            removeElement('.textbstopsku', 'p_textbstopsku');
            removeElement('.textbssalesperformance1', 'p_textbssalesperformance1');
            removeElement('.texttopsku', 'p_texttopsku');
            removeElement('.textsalesperformance1', 'p_textsalesperformance1');
            removeElement('.textsalesteam', 'p_textsalesteam');
            removeElement('.textsaledperformance', 'p_textsaledperformance');
            removeElement('.textbssaledperformance', 'p_textbssaledperformance');
            removeElement('.textrichmedia', 'p_textrichmedia');
            removeElement('.textbssalesteam', 'p_textbssalesteam');
            removeElement('.textbsrichmedia', 'p_textbsrichmedia');
            removeElement('.a_call_frequency', '');
            removeElement('.a_total_call', '');
            // Force browser to print
            window.print();
            <?php }?>
        });

        function drawHumanChart(redhumans, greenhumans, chartid) {
            var greenHumanImage = "<?php echo site_url('res/img/infographic/green_human.png'); ?>",
                redHumanImage = "<?php echo site_url('res/img/infographic/red_human.png'); ?>",
                allHuman = redhumans + greenhumans;
            var svgDocument = document.getElementById(chartid);

            //var svgWidth = svgDocument.clientWidth,
            //svgHeight = svgDocument.clientHeight,
            //rowCount = 0;
            var svgWidth = $("#" + chartid).width(),
                svgHeight = $("#" + chartid).height(),
                rowCount = 0;

            if (allHuman < 5) rowCount = 1;
            else if (allHuman < 15) rowCount = 2;
            else if (allHuman < 34) rowCount = 3;
            else if (allHuman < 100) rowCount = 4;
            else rowCount = 5;

            var humanHeight;
            if (rowCount > 1) humanHeight = (svgHeight - (rowCount - 1) * 5) / rowCount;
            else humanHeight = svgHeight;

            var humanWidth = 68 / 190 * humanHeight,
                humanPerRow = Math.floor(svgWidth / (humanWidth + 2)),
                humanInLastRow = allHuman - (rowCount - 1) * humanPerRow,
                rowInRed = Math.floor(redhumans / humanPerRow),
                redHumanInLastRow = redhumans - (rowInRed) * humanPerRow;
            if (rowCount == 1) humanPerRow = allHuman;
            for (var i = 0; i < rowCount; i++) {
                for (var j = 0; j < humanPerRow; j++) {
                    var currentHumanColor = redHumanImage;
                    if ((i == rowInRed && j >= redHumanInLastRow) || i > rowInRed) {
                        currentHumanColor = greenHumanImage;
                    }
                    if (j >= allHuman - (i * humanPerRow)) break;

                    var svgimg = document.createElementNS('http://www.w3.org/2000/svg', 'image');
                    svgimg.setAttribute('height', humanHeight);
                    svgimg.setAttribute('width', humanWidth);
                    svgimg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', currentHumanColor);
                    svgimg.setAttribute('x', humanWidth * j + 2 * j);
                    svgimg.setAttribute('y', humanHeight * i + 2 * i);
                    svgDocument.appendChild(svgimg);
                }
            }
        }


        //function draw line chart
        function drawLineChart(rect, polygol, text, width) {
            rect.setAttribute("width", width + 0.55);
            polygol.setAttribute("x", width + 10);
            var text_width = text.getBoundingClientRect().width;
            var rect_width = rect.getBoundingClientRect().width;
            if (rect_width <= text_width) {
                text.setAttribute("x", 0);
                text.children[0].setAttribute("x", 15);
            } else {
                text.setAttribute("x", width - (text_width + 10));
            }
        }

        //function draw growth chart
        function drawGrowthChart(percent, chartid) {
            var svgns = "http://www.w3.org/2000/svg";
            var growthChartColumnHeight = [30, 37.5, 45, 52.5, 60, 67.5, 75, 82.5, 90, 97.5];
            var growthChartColumnWidth = 11.5;
            var growthChartHorizonPosition = [0, 15.5, 30.5, 46, 61.5, 77, 92, 107.5, 123, 139];
            var chartHeight = 106;
            var mainColor = "rgb(142, 198, 65)";
            var grayColor = "rgb(189, 190, 192)";

            var svgDocument = document.getElementById(chartid);
            var percentPerTen = percent / 10;

            for (i = 0; i < 10; i++) {
                var currentColumnWidth = growthChartColumnWidth;
                var currentColumnColor = mainColor;
                if (i <= percentPerTen && percentPerTen < i + 1) {
                    currentColumnWidth = (percent % 10) / 10 * growthChartColumnWidth;
                } else if (i > percentPerTen) {
                    currentColumnColor = grayColor;
                }

                var shape = document.createElementNS(svgns, "rect");
                shape.setAttributeNS(null, "x", growthChartHorizonPosition[i]);
                shape.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
                shape.setAttributeNS(null, "width", currentColumnWidth);
                shape.setAttributeNS(null, "height", growthChartColumnHeight[i]);
                shape.setAttributeNS(null, "fill", currentColumnColor);

                var shape1 = null;
                if (currentColumnWidth < growthChartColumnWidth) {
                    shape1 = document.createElementNS(svgns, "rect");
                    shape1.setAttributeNS(null, "x", growthChartHorizonPosition[i] + currentColumnWidth - 0.5);
                    shape1.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
                    shape1.setAttributeNS(null, "width", growthChartColumnWidth - currentColumnWidth);
                    shape1.setAttributeNS(null, "height", growthChartColumnHeight[i]);
                    shape1.setAttributeNS(null, "fill", grayColor);
                }

                svgDocument.appendChild(shape);
                if (shape1 != null) svgDocument.appendChild(shape1);
            }
        }

        function drawPiechart(chartid, percent) {
            var chart = document.getElementById(chartid);
            var abtext1 = document.getElementById("abtext1");
            var abtext2 = document.getElementById("abtext2");
            var textpercent = document.getElementById("piechart-percent");

            abtext2_newy = 150 + 105 * Math.cos(Math.PI * (1 - percent / 100)) - abtext1.offsetHeight / 2;
            abtext2_newx = 90 + 105 * Math.sin(Math.PI * (1 - percent / 100)) - abtext1.offsetWidth / 2;
            abtext1_newy = 150 - 105 * Math.cos(Math.PI * (1 - percent / 100)) - abtext2.offsetHeight / 2;
            abtext1_newx = 90 - 105 * Math.sin(Math.PI * (1 - percent / 100)) - abtext2.offsetWidth / 2;

//		abtext1.style.marginTop = abtext1_newy;
//		abtext1.style.marginLeft = abtext1_newx;
//
//		abtext2.style.marginTop = abtext2_newy;
//		abtext2.style.marginLeft = abtext2_newx;

            $('#abtext1').css({
                marginTop: abtext1_newy,
                marginLeft: abtext1_newx
            });
            $('#abtext2').css({
                marginTop: abtext2_newy,
                marginLeft: abtext2_newx
            })

            // chart.setAttribute("data-text", percent + "%");
            textpercent.innerHTML = percent + "%";
            chart.setAttribute("data-percent", percent);

            $('#' + chartid).circliful();
        }

        function calculateWidth(maxWidth, maxValue, value) {
            var width = 0;
            width = (value / maxValue) * maxWidth;

            return width;
        }
    </script>
</html>