<?php

// array('url' => 'Label')
$infographic = array(
 //    'call_data_total' => 'Call Data Total',
 //    'call_data_1' => 'Call Data Average Call Time',
 //    'call_data_2' => 'Call Data Call Frequency',
 //    'call_data_3' => 'Call Data Average calls/work day',
	// 'strike_rate' => 'Call Data - Strike Rate',
 //    'rich_media_1' => 'Rich Media (Pantry & Sampling)',
 //    'rich_media_2' => 'Rich Media',
 //    'sales_performance_1' => 'Sales Team Sales Performance',
 //    'sales_performance_2' => 'Sales Performance',
 //    'sales_team' => 'Sales Team',
 //    'top_sku' => 'Top SKU',
	'home' => 'PULL data',
	// 'home/1' => 'PULL data 1',
	// 'home/2' => 'PULL data 2',
);

?>

<?php if (!in_array($this->country_code, array('tw', 'ph'))) { ?>
<li class="dropdown <?= $this->router->class == 'infographic' ? ' active' : '' ?>">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span> &nbsp; Infographic</a>
    <ul class="dropdown-menu">
        <?php foreach ($infographic as $url => $label) { ?>
            <li class="<?= (uri_string() == 'infographic/' . $url) ? 'active' : '' ?>">
                <a href="<?= site_url('infographic/' . $url) ?>" style="display: none;"><?= $label ?></a>
                <a href="#InfographicModal" data-href="<?= site_url('infographic/' . $url) ?>" class="infographic-menu" data-toggle="modal"><?= $label ?></a>
            </li>
        <?php } ?>
    </ul>
</li>
<?php } ?>