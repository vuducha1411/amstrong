<div class="modal-header" style="padding-bottom: 60px;">
    <!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
    <div id="header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <img src="<?php echo site_url('res/img/infographic/logo.png'); ?>">

        <p>Top SKU</p>
    </div>
    <div id="content">
        <div id="contentsku" class="text-center">
            <div class="top-sku-background">
                <p class="top-sku-title">TOP Penetration</p>

                <div class="top-sku-inside">
                    <ul class="top-sku-list">
                        <?php foreach ($infographic[Infographic_model::TOP_SKU]['top3'] as $main_sku => $top_sku): ?>

                            <?php if (isset($top_sku['main'])) { ?>
                                <li class="main_sku">
                                    <p><?= $top_sku['main']['group_name'] ? $top_sku['main']['group_name'] : $top_sku['main']['sku_name'] . " ({$top_sku['main']['pakingSize']})" ?></p>

                                    <p class="top-sku-percent">[<?php
                                        $topskupercent = ($top_sku['main']['totalCustomer'] > 0 && $top_sku['main']['totalCustomerOrder'] > 0)
                                            ? $top_sku['main']['totalCustomerOrder'] / $top_sku['main']['totalCustomer'] * 100
                                            : 0;

                                        echo_isset(number_format($topskupercent), 0)
                                        ?>%]</p>
                                </li>
                            <?php }
                            /*else {
                                if (isset($top_sku['child'])) {
                                    foreach ($top_sku['child'] as $child) { ?>
                                        <li class="main_sku">
                                            <p><?= $child['group_name'] ? $child['group_name'] : $child['sku_name'] . " ({$child['pakingSize']})" ?></p>

                                            <p class="top-sku-percent">[<?php
                                                $topskupercent = ($child['totalCustomer'] > 0 && $child['totalCustomerOrder'] > 0)
                                                    ? $child['totalCustomerOrder'] / $child['totalCustomer'] * 100
                                                    : 0;

                                                echo_isset(number_format($topskupercent), 0)
                                                ?>%]</p>
                                        </li>
                                    <?php }
                                }
                            } */
                            ?>

                            <?php if (isset($top_sku['main']) && isset($top_sku['child'])) {
                                foreach ($top_sku['child'] as $child) {
                                    ?>
                                    <li class="child_sku">
                                        <p><?= $child['group_name'] ? "- " . $child['group_name'] . " ({$child['pakingSize']})" : "- " . $child['skuNname'] . " ({$child['pakingSize']})" ?></p>

                                        <p class="top-sku-percent">[<?php
                                            $topskupercent = $child['totalCustomer'] > 0
                                                ? $child['numberCustomerOder'] / $child['totalCustomer'] * 100
                                                : 0;

                                            echo_isset(number_format($topskupercent), 0)
                                            ?>%]</p>
                                    </li>
                                <?php }
                            } ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>