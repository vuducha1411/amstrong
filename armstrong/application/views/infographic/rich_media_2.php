<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Rich Media</p>
	</div>
	<div id="content" class="text-center">

		<ul class="mn_ctmedia" style="min-height: 500px;">
			<li class="mn_media tablemedia1">
				<h1>Armstrong has<br />a total of</h1>
				<h2><?php echo $infographic[1]['quantity'] ?></h2>
				<p>SKUS</p>
			</li>
			<li class="mn_media tablemedia2">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[4]['quantity'] ?></h2>
				<p>SELLING<br />STORIES</p>
			</li>
			<li class="mn_media tablemedia3">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[3]['quantity'] ?></h2>
				<p>VIDEOS</p>
			</li>
			<li class="mn_media tablemedia4">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[2]['quantity'] ?></h2>
				<p>RECIPES</p>
			</li>
		</ul>

	</div>
</div>