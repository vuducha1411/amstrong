<?php 
    if (!intval($infographic['salesTarget']))
    {
        $percent = '100';
    }
    else
    {
        $percent = (intval($infographic['salesAchieved']) / intval($infographic['salesTarget'])) * 100;
    }
?>

<div id="content" class="text-center">

    <div id="content1">
        <div class="maincontent">
            <div class="text1">
                <h1>SALES ACHIEVED<h1>
                <h2>$<?php echo number_format($infographic['salesAchieved']) ?></h2>
                <h3><?php echo number_format($percent) ?>% </h3>
            </div>
            <p>/</p>
            <div class="text2">
                <h1>SALES TARGET</h1>
                <h2>$<?php echo number_format($infographic['salesTarget']) ?></h2>
            </div>      
        </div>  
    </div>

</div>