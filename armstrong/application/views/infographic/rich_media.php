<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Rich Media</p>
	</div>
	<div id="content" class="text-center">

		<ul class="mn_ctmedia" style="min-height: 500px;">
			<li class="mn_media tablemedia1">
				<h1>Armstrong has<br />a total of</h1>
				<h2><?php echo $infographic[Infographic_model::TYPE]['media'][1]['quantity'] ?></h2>
				<p>SKUS</p>
			</li>
			<li class="mn_media tablemedia2">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[Infographic_model::TYPE]['media'][4]['quantity'] ?></h2>
				<p>SELLING<br />STORIES</p>
			</li>
			<li class="mn_media tablemedia3">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[Infographic_model::TYPE]['media'][3]['quantity'] ?></h2>
				<p>VIDEOS</p>
			</li>
			<li class="mn_media tablemedia4">
				<h1>Armstrong has<br>a total of</h1>
				<h2><?php echo $infographic[Infographic_model::TYPE]['media'][2]['quantity'] ?></h2>
				<p>RECIPES</p>
			</li>
		</ul>
        <div class="pantry_sampling" style="overflow:hidden;">
            <div class="leftcontent">
                <h1>A total of <?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalCustomerHaveSampled']) ?> customers</h1>
                <h2>have sampled our products</h2>
                <div class="left_img">
                    <div class="left_img1">
                        <img src="<?php echo site_url('res/img/infographic/pic2.png'); ?>"/>
                        <img src="<?php echo site_url('res/img/infographic/pic1.png'); ?>"/>
                    </div>
                    <p><?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingTimes']) ?></p>
                    <span class="text-below-number font-26 font-bold">SAMPLING ACTIVITIES</span>
                </div>
                <div id="rich-media-left-detail-info">
                    <ul>
                        <li><?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsWet']); ?><br/><span>WET</span></li>
                        <li><?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsDry']); ?><br/><span>DRY</span></li>
                        <li><?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalSamplingItemsCd']); ?><br/><span>DEMO</span></li>
                    </ul>
                </div>
                <p class="textleftbottomrichmedia2 font-26 font-bold">
                    SAMPLING ITEMS
                </p>
            </div>
            <div class="rightcontent">
                <h1>A total of <?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalPantryCheckTimes']) ?></h1>
                <h2>Pantry checks conducted</h2>
                <div class="right_img">
                    <img src="<?php echo site_url('res/img/infographic/pic3.png'); ?>"/>
                    <p><?php echo number_format($infographic[Infographic_model::DATA]['sampling']['totalPantryCheckItems']) ?></p>
                    <span class="text-below-number font-26 font-bold" style="color: #29AAE1">PANTRY CHECK ITEMS</span>
                </div>
            </div>
        </div>
	</div>
</div>