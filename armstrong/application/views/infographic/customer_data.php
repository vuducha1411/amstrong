<div class="modal-header" style="padding-bottom: 60px;">
<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Active Customers (GRIP)</p>
	</div>
	<div id="content" class="text-center cf" style="padding-bottom: 30px;">
		<div class="customerdata">
			<div class="customerdatatable1">
				<div class="leftctdt">
					<h1><?php echo isset($otm[0]['value']) ? number_format($otm[0]['value'], 0) : 0 ?></h1>
					<h2>buying customers</h2>
				</div>
				<div class="centerctdt">
					<p><?php echo isset($otm[0]['label']) ? $otm[0]['label'] : 'A' ?></p>
				</div>
				<div class="rightctdt">
					<h4><?php echo isset($otm[0]['percentage']) ? number_format($otm[0]['percentage'], 1) : 0 ?>%</h4>
					<h5>of</h5>
					<h6>buying customers</h6>
				</div>
			</div>
			<div class="customerdatatable2">
				<div class="leftctdt1">
					<h1><?php echo isset($otm[1]['value']) ? number_format($otm[1]['value'], 0) : 0 ?></h1>
					<h2>buying customers</h2>
				</div>
				<div class="centerctdt1">
					<p><?php echo isset($otm[1]['label']) ? $otm[0]['label'] : 'B' ?></p>
				</div>
				<div class="rightctdt1">
					<h3><?php echo isset($otm[1]['percentage']) ? number_format($otm[1]['percentage'], 1) : 0 ?>%</h3>
					<h4>of</h4>
					<h5>buying customers</h5>
				</div>
			</div>
			<div class="customerdatatable3">
				<div class="leftctdt2">
					<h1><?php echo isset($otm[2]['value']) ? number_format($otm[2]['value'], 0) : 0 ?></h1>
					<h2>buying customers</h2>
				</div>
				<div class="centerctdt2">
					<p><?php echo isset($otm[2]['label']) ? $otm[0]['label'] : 'C' ?></p>
				</div>
				<div class="rightctdt2">
					<h3><?php echo isset($otm[2]['percentage']) ? number_format($otm[2]['percentage'], 1) : 0 ?>%</h3>
					<h4>of</h4>
					<h5>buying customers</h5>
				</div>
			</div>
			<div class="customerdatatable4">
				<div class="leftctdt3">
					<h1><?php echo isset($otm[3]['value']) ? number_format($otm[3]['value'], 0) : 0 ?></h1>
					<h2>buying customers</h2>
				</div>
				<div class="centerctdt3">
					<p class=""><?php echo isset($otm[3]['label']) ? $otm[0]['label'] : 'D' ?></p>
				</div>
				<div class="rightctdt3">
					<h3><?php echo isset($otm[3]['percentage']) ? number_format($otm[3]['percentage'], 1) : 0 ?>%</h3>
					<h4>of</h4>
					<h5>buying customers</h5>
				</div>
			</div>
<!--			 <div class="customerdatatable5">-->
<!--				<div class="leftctdt5">-->
<!--					<h1>--><?php //echo isset($otm[4]['value']) ? number_format($otm[4]['value'], 0) : 0 ?><!--</h1>-->
<!--					<p>buying customers</p>-->
<!--				</div>-->
<!--				<div class="centerctdt5">-->
<!--					<p>--><?php //echo $otm[3]['label'] ?><!--</p>-->
<!--				</div>-->
<!--				<div class="rightctdt5">-->
<!--					<h3>--><?php //echo number_format($otm[3]['percentage'], 1) ?><!--%</h3>-->
<!--					<h4>of</h4>-->
<!--					<h5>buying customers</h5>-->
<!--				</div>-->
<!--			</div>-->
		</div>
        <div class="text-center customer-data-un font-bold">
            Unclassified OTM Total number of active buying customers: <?php echo isset($otm[4]['value']) ? number_format($otm[4]['value']) : 0 ?> (<?php echo isset($otm[4]['percentage']) ? number_format($otm[4]['percentage'], 0) : 0 ?>%)
        </div>
		<div class="text-center customer-data-total">
			<p><?php echo isset($infographic['total']) ? number_format($infographic['total']) : 0 ?> buying customers</p>
		</div>
	</div>
</div>