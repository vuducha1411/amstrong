<div class="modal-header" style="padding-bottom: 60px;">
	<!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff">
	<div id="header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="<?php echo site_url('res/img/infographic/logo.png'); ?>"><p>Sales Team</p>
	</div>
	<div id="content" class="text-center cf">
		<div class="tbsalesteam tbsalesteam_2">
			<div class="toptbsalesteam_2">
                <img src="<?php echo site_url($country_map); ?>" alt="Country Map"/>
			</div>
			<div class="bottomsalesteam">

				<div class="block2">
					<svg id="human-chart-2" width="339" height="231" xmlns:xlink= "http://www.w3.org/1999/xlink">
					</svg>
				</div>
				<div class="block3">
                    <div class="textalign">
                        <p id="txt1"><?php echo (isset($infographic['sl_team']['numberSalesLeaders'])) ? number_format($infographic['sl_team']['numberSalesLeaders']) : 0; ?> SALES LEADERS</p>
                        <p id="txt2"><?php echo (isset($infographic['sl_team']['numberSalesPersonnels'])) ? number_format($infographic['sl_team']['numberSalesPersonnels']) : 0; ?> SALES PERSONNELS</p>
                    </div>
				</div>

			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {

		//draw human chart
        drawHumanChart(<?php echo_isset(number_format($infographic['sl_team']['numberSalesLeaders']), 0) ?>, <?php echo_isset(number_format($infographic['sl_team']['numberSalesPersonnels']), 0) ?>,"human-chart-2");
        $("#txt1").text("<?php echo_isset(number_format($infographic['sl_team']['numberSalesLeaders']), 0) ?> SALES LEADERS");
        $("#txt2").text("<?php echo_isset(number_format($infographic['sl_team']['numberSalesPersonnels']), 0) ?> SALES PERSONNELS");

	});
    function drawHumanChart(redhumans, greenhumans, chartid) {
        var greenHumanImage = "<?php echo site_url('res/img/infographic/green_human.png'); ?>",
            redHumanImage = "<?php echo site_url('res/img/infographic/red_human.png'); ?>",
            allHuman = redhumans + greenhumans;
        var svgDocument = document.getElementById(chartid);

        //var svgWidth = svgDocument.clientWidth,
        //svgHeight = svgDocument.clientHeight,
        //rowCount = 0;
        var svgWidth = $("#" + chartid).width(),
            svgHeight = $("#" + chartid).height(),
            rowCount = 0;

        if (allHuman < 5) rowCount = 1;
        else if (allHuman < 15) rowCount = 2;
        else if (allHuman < 34) rowCount = 3;
        else if (allHuman < 100) rowCount = 4;
        else rowCount = 5;

        var humanHeight;
        if (rowCount > 1) humanHeight = (svgHeight - (rowCount - 1) * 5) / rowCount;
        else humanHeight = svgHeight;

        var humanWidth = 68/190 * humanHeight,
            humanPerRow = Math.floor(svgWidth / (humanWidth + 2)),
            humanInLastRow = allHuman - (rowCount - 1) * humanPerRow,
            rowInRed = Math.floor(redhumans / humanPerRow),
            redHumanInLastRow = redhumans - (rowInRed) * humanPerRow;
        if(rowCount == 1) humanPerRow = allHuman;
        for (var i = 0; i < rowCount; i++) {
            for(var j = 0; j< humanPerRow; j++) {
                var currentHumanColor = redHumanImage;
                if((i == rowInRed && j >= redHumanInLastRow) || i > rowInRed) {
                    currentHumanColor = greenHumanImage;
                }
                if(j >= allHuman - (i * humanPerRow)) break;

                var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
                svgimg.setAttribute('height', humanHeight);
                svgimg.setAttribute('width', humanWidth);
                svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href', currentHumanColor);
                svgimg.setAttribute('x', humanWidth * j + 2 * j);
                svgimg.setAttribute('y', humanHeight * i + 2 * i);
                svgDocument.appendChild(svgimg);
            }
        }
    }
</script>