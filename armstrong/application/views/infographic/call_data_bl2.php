<div class="modal-header" style="padding-bottom: 60px;">
    <!--	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
</div>
<div class="modal-body" style="background-color: #fff; min-height: 750px;">
    <?php
    $strikeRateModal = 0;
    if($infographic[Infographic_model::CALL_RECORD]){
        if($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] > 0 && $infographic[Infographic_model::CALL_RECORD]['totalCall'] > 0){
            $strikeRateModal = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] / $infographic[Infographic_model::CALL_RECORD]['totalCall'] * 100;
        }
    }else{
        $infographic[Infographic_model::CALL_RECORD]['totalCall'] = $infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'] = $infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'] = 0;
    }
    ?>
    <div id="header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <img src="<?php echo site_url('res/img/infographic/logo.png'); ?>">

        <p>Call DATA</p>
    </div>
    <div id="content" class="text-center call_data_bl2">
        <div class="contentpie">
            <div id="myStat2" data-dimension="484" data-width="90" data-fontsize="75.25" data-percent="65"
                 data-fgcolor="#2BACE2" data-bgcolor="#818285" data-animationstep="0">
            </div>
            <div class="rightcontentpie">
                <div class="toppie">
                    <div class="leftctpie">
                        <p class="textleftctpie1">Total Calls</p>

                        <p class="textleftctpie2">Calls with oders</p>

                        <p class="textleftctpie3">Calls without oders</p>
                    </div>
                    <div class="rightctpie">
                        <p class="textrightctpie1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCall'])) ?></p>

                        <p class="textrightctpie2"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>

                        <p class="textrightctpie3"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
                    </div>
                </div>
                <div class="bottompie">
                    <div class="leftbottompie">
                        <p class="textleftbtpie1"><?php echo_isset(number_format($strikeRateModal, 0)); ?>%</p>
<!--                            <!span>/</span>-->


                        <p class="textleftbtpie2">Strike Rate</p>
                    </div>
                    <!--					<div class="rightbottompie">-->
                    <!--						<p class="textleftrightpie1">50%</p>-->
                    <!--						<p class="textleftrightpie2">target</p>-->
                    <!--					</div>-->
                </div>
            </div>
            <div class="abtext1" id="modalabtext1">
                <p class="abtextcall">Calls without orders</p>

                <p class="abnumcall"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithOutTfo'])) ?></p>
            </div>
            <div class="abtext2" id="modalabtext2">
                <p class="abtextcall1">Calls with orders</p>

                <p class="abnumcall1"><?php echo_isset(number_format($infographic[Infographic_model::CALL_RECORD]['totalCallWithTfo'])) ?></p>
            </div>
            <div class="abtext3" id="modalabtext3">
                <p class="abnumcall2 textleftbtpie1"><?php echo_isset(number_format($strikeRateModal, 0)); ?>
                    %</p>

                <p class="abtextcall2 textleftbtpie2">Strike Rate</p>
            </div>
        </div>
        <?php
        $otmAPercent = $otmBPercent = $otmCPercent = $otmDPercent = 0;
        $customerOtmA = $customerOtmB = $customerOtmC = $customerOtmD = 0;
        $visitedOtmA = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmA'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmA'] : 0;
        $visitedOtmB = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmB'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmB'] : 0;
        $visitedOtmC = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmC'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmC'] : 0;
        $visitedOtmD = (isset($infographic[Infographic_model::CALL_RECORD]['visitedOtmD'])) ? $infographic[Infographic_model::CALL_RECORD]['visitedOtmD'] : 0;
        if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmA'])) {
            $customerOtmA = $infographic[Infographic_model::CALL_RECORD]['customerOtmA'];
            $otmAPercent = ($customerOtmA > 0 && $visitedOtmA > 0)
                ? $visitedOtmA / $customerOtmA * 100
                : 0;
        }
        if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmB'])) {
            $customerOtmB = $infographic[Infographic_model::CALL_RECORD]['customerOtmB'];
            $otmBPercent = ($customerOtmB > 0 && $visitedOtmB > 0)
                ? $visitedOtmB / $customerOtmB * 100
                : 0;
        }
        if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmC'])) {
            $customerOtmC = $infographic[Infographic_model::CALL_RECORD]['customerOtmC'];
            $otmCPercent = ($customerOtmC > 0 && $visitedOtmC > 0)
                ? $visitedOtmC / $customerOtmC * 100
                : 0;
        }
        if (isset($infographic[Infographic_model::CALL_RECORD]['customerOtmD'])) {
            $customerOtmD = $infographic[Infographic_model::CALL_RECORD]['customerOtmD'];
            $otmDPercent = ($customerOtmD > 0 && $visitedOtmD > 0)
                ? $visitedOtmD / $customerOtmD * 100
                : 0;
        }
        ?>
        <div id="calldatacontent">
            <div class="chartbar12">
                <div class="chartindex1">
                    <img src="<?= site_url('res/img/infographic/pic.png') ?>"/>

                    <p id="textct1"><?php echo_isset(number_format($otmAPercent, 0)); ?>%</p>
                    <svg id="index1" width="200" height="142">
                    </svg>
                    <p class="idex1textchart1"> target is 100%</p>

                    <p class="idex1textchart2"> OTM A coverage</p>

                    <div class="index1textchart3">
                        <div class="leftindex1tc1">
                            <p><?php echo_isset(number_format($visitedOtmA, 0, '', '')); ?></p>
                            <a>visited customer</a>
                        </div>
                        <div class="centerindex1">
                            <p>/</p>
                        </div>
                        <div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($customerOtmA, 0, '', '')); ?> </p>
                            <a>total customers</a>
                        </div>
                    </div>
                </div>
                <div class="chartindex2">
                    <img src="<?= site_url('res/img/infographic/pic.png') ?>"/>

                    <p id="textct2"><?php echo_isset(number_format($otmBPercent, 0)); ?>%</p>
                    <svg id="index2" width="200" height="142">
                    </svg>
                    <p class="index2textchart1"> target is 100%</p>

                    <p class="index2textchart2"> OTM B coverage</p>

                    <div class="index1textchart3">
                        <div class="leftindex1tc1">
                            <p><?php echo_isset(number_format($visitedOtmB, 0, '', '')); ?></p>
                            <a>visited customer</a>
                        </div>
                        <div class="centerindex1">
                            <p>/</p>
                        </div>
                        <div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($customerOtmB, 0, '', '')); ?> </p>
                            <a>total customers</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chartbar34">
                <div class="chartindex3">
                    <img src="<?= site_url('res/img/infographic/pic.png') ?>"/>

                    <p id="textct3"><?php echo_isset(number_format($otmCPercent, 0)); ?>%</p>
                    <svg id="index3" width="200" height="142">
                    </svg>
                    <p class="idex3textchart2"> OTM c coverage</p>

                    <div class="index3textchart3">
                        <div class="leftindex1tc1">
                            <p><?php echo_isset(number_format($visitedOtmC, 0, '', '')); ?></p>
                            <a>visited customer</a>
                        </div>
                        <div class="centerindex1">
                            <p>/</p>
                        </div>
                        <div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($customerOtmC, 0, '', '')); ?> </p>
                            <a>total customers</a>
                        </div>
                    </div>
                </div>
                <div class="chartindex4">
                    <img src="<?= site_url('res/img/infographic/pic.png') ?>"/>

                    <p id="textct4"><?php echo_isset(number_format($otmDPercent, 0)); ?>%</p>
                    <svg id="index4" width="200" height="142">
                    </svg>
                    <p class="index4textchart2"> OTM d coverage</p>

                    <div class="index4textchart3">
                        <div class="leftindex1tc1">
                            <p><?php echo_isset(number_format($visitedOtmD, 0, '', '')); ?></p>
                            <a>visited customer</a>
                        </div>
                        <div class="centerindex1">
                            <p>/</p>
                        </div>
                        <div class="rightindex1tc2">
                            <p> <?php echo_isset(number_format($customerOtmD, 0, '', '')); ?> </p>
                            <a>total customers</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //piechart
        drawModalPiechart("#myStat2", <?php echo_isset(number_format($strikeRateModal, 0)); ?>);
        drawGrowthChart(<?php echo_isset(number_format($otmAPercent, 0)); ?>, "index1");
//        $("#textct1").text("<?php //echo_isset(number_format($otmAPercent, 0)); ?>//%");
        drawGrowthChart(<?php echo_isset(number_format($otmBPercent, 0)); ?>, "index2");
//        $("#textct2").text("<?php //echo_isset(number_format($otmBPercent, 0)); ?>//%")
        drawGrowthChart(<?php echo_isset(number_format($otmCPercent, 0)); ?>, "index3");
//        $("#textct3").text("<?php //echo_isset(number_format($otmCPercent, 0)); ?>//%");
        drawGrowthChart(<?php echo_isset(number_format($otmDPercent, 0)); ?>, "index4");
//        $("#textct4").text("<?php //echo_isset(number_format($otmDPercent, 0)); ?>//%");
    });

    function drawModalPiechart(chartid, percent) {
        var chart = $(chartid);
        var modalabtext1 = document.getElementById('modalabtext1');
        var modalabtext2 = document.getElementById('modalabtext2');
        var modalabtext3 = $('#modalabtext3');

        var modalabtext3_newy = 100 + 197 * Math.cos(Math.PI * (1 - percent / 100)) - modalabtext3.offsetHeight / 2;
        var modalabtext3_newx = 160 + 197 * Math.sin(Math.PI * (1 - percent / 100)) - modalabtext3.offsetWidth / 2;
        var modalabtext2_newy = 230 + 197 * Math.cos(Math.PI * (1 - percent / 100)) - modalabtext1.offsetHeight / 2;
        var modalabtext2_newx = 290 + 197 * Math.sin(Math.PI * (1 - percent / 100)) - modalabtext1.offsetWidth / 2;
        var modalabtext1_newy = 230 - 197 * Math.cos(Math.PI * (1 - percent / 100)) - modalabtext2.offsetHeight / 2;
        var modalabtext1_newx = 290 - 197 * Math.sin(Math.PI * (1 - percent / 100)) - modalabtext2.offsetWidth / 2;

        $('#modalabtext1').css({
            marginTop: modalabtext1_newy,
            marginLeft: modalabtext1_newx
        });
        $('#modalabtext2').css({
            marginTop: modalabtext2_newy,
            marginLeft: modalabtext2_newx
        });
        modalabtext3.css({
            marginTop: modalabtext3_newy,
            marginLeft: modalabtext3_newx
        });

//        chart.attr("data-text", percent + "%");
        chart.attr("data-percent", percent);

        $(chartid).circliful();
    }

    function drawGrowthChart(percent, chartid) {
        var svgns = "http://www.w3.org/2000/svg";
        var growthChartColumnHeight = [40, 50, 60, 70, 80, 90, 100, 110, 120, 130];
        var growthChartColumnWidth = 15.5;
        var growthChartHorizonPosition = [0, 20.5, 41, 61.5, 82, 102.5, 123, 143.5, 164, 184.5];
        var chartHeight = 140;
        var mainColor = "rgb(142, 198, 65)";
        var grayColor = "rgb(189, 190, 192)";

        var svgDocument = document.getElementById(chartid);
        var percentPerTen = percent / 10;

        for (i = 0; i < 10; i++) {
            var currentColumnWidth = growthChartColumnWidth;
            var currentColumnColor = mainColor;
            if (i <= percentPerTen && percentPerTen < i + 1) {
                currentColumnWidth = (percent % 10) / 10 * growthChartColumnWidth;
            } else if (i > percentPerTen) {
                currentColumnColor = grayColor;
            }

            var shape = document.createElementNS(svgns, "rect");
            shape.setAttributeNS(null, "x", growthChartHorizonPosition[i]);
            shape.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
            shape.setAttributeNS(null, "width", currentColumnWidth);
            shape.setAttributeNS(null, "height", growthChartColumnHeight[i]);
            shape.setAttributeNS(null, "fill", currentColumnColor);

            var shape1 = null;
            if (currentColumnWidth < growthChartColumnWidth) {
                shape1 = document.createElementNS(svgns, "rect");
                shape1.setAttributeNS(null, "x", growthChartHorizonPosition[i] + currentColumnWidth - 0.5);
                shape1.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
                shape1.setAttributeNS(null, "width", growthChartColumnWidth - currentColumnWidth);
                shape1.setAttributeNS(null, "height", growthChartColumnHeight[i]);
                shape1.setAttributeNS(null, "fill", grayColor);
            }

            svgDocument.appendChild(shape);
            if (shape1 != null) svgDocument.appendChild(shape1);
        }
    }

</script>