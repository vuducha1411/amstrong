<div class="login-panel panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Please Sign In</h3>
    </div>
    <div class="panel-body">
        <?php if (validation_errors() != '' || isset($alert_failed)): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
                <?php echo isset($alert_failed) ? $alert_failed : ''; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open('ami/salespersons/check_login'); ?>
        <fieldset>
            <div class="form-group">
                <?php //echo form_dropdown('country_id', $countries, $country_id, 'class="form-control"') ?>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle full-width" type="button" id="dropdownMenu1" data-toggle="dropdown">
                        <span id="countrySelect">Select a Country</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu full-width" role="menu" aria-labelledby="dropdownMenu1">
                        <?php foreach ($countries as $key => $country) 
                        {
                            echo '<li role="presentation"><a class="country_select" data-countryid="' . $key . '" role="menuitem" tabindex="-1" href="#">' . $country . '</a></li>';
                        } ?>
                    </ul>
                </div>
            </div>
            <div class="form-group" for="country">
                <label class="control-label" for="email">Please key in your Armstrong user ID</label>
                <?php echo form_input('email', set_value('email', ''), 'id="email" class="form-control" placeholder="E-mail" autofocus'); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="password">Please key in your Armstrong user password</label>
                <?php echo form_password('password', set_value('password', ''), 'id="password" class="form-control" placeholder="Password" autofocus'); ?>
            </div>
            <input type="hidden" name="country_id" value="" id="countryId">
            <!-- Change this to a button or input when using this as a form -->
            <?php echo form_submit('submit', 'Login', 'class="btn btn-lg btn-success btn-block"'); ?>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>