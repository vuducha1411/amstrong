<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" disabled="" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" disabled="">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li><?php echo anchor('ami/news', '<i class="fa fa-dashboard fa-fw"></i> News'); ?></li>
            <li><?php echo anchor('ami/dashboard', '<i class="fa fa-dashboard fa-fw"></i> Dashboard'); ?></li>
            <!--            <li>-->
            <?php //echo anchor('admin/user', '<i class="fa fa-user fa-fw"></i> Users'); ?><!--</li>-->
            <!--            <li>-->
            <!--                --><?php //echo anchor('admin/partner', '<i class="fa fa-group fa-fw"></i> Partners with iDirect Asia<span class="fa arrow"></span>'); ?>
            <!--                <ul class="nav nav-second-level">-->
            <!--                    <li>-->
            <!--                        --><?php //echo anchor('admin/partner', 'Manage User Profile'); ?>
            <!--                    </li>-->
            <!--                    <li>-->
            <!--                        --><?php //echo anchor('admin/partner/index/0', 'Pending Partner Registration'); ?>
            <!--                    </li>-->
            <!--                </ul>-->
            <!--                <!-- /.nav-second-level-->
            <!--            </li>-->
            <!--            <li>-->
            <?php //echo anchor('admin/edm/', '<i class="fa fa-envelope fa-fw"></i> eDM and Newsletter'); ?><!--</li>-->
            <!--            <li>-->
            <?php //echo anchor('admin/category/', '<i class="fa fa-folder fa-fw"></i> Marketing Materials'); ?><!--</li>-->
            <!--            <li>-->
            <?php //echo anchor('admin/partnersubm/', '<i class="fa fa-thumbs-up fa-fw"></i> Partner Applications'); ?><!--</li>-->
            <!--            <li>-->
            <?php //echo anchor('admin/support/', '<i class="fa fa-inbox fa-fw"></i> Support Requests'); ?><!--</li>-->
            <!--            <li>-->
            <?php //echo anchor('admin/user/logout', '<i class="fa fa-sign-out fa-fw"></i> Logout'); ?><!--</li>-->
        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</div>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>