<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0">
    <title>Unilever AMI</title>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo site_url('res/css/bootstrap.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/jqueryui/jquery-ui-1.10.4.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/font-awesome/css/font-awesome.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/ami.css'); ?>"/>

    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo site_url('res/js/jquery-1.11.0.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/jquery-ui-1.10.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('res/js/plugins/tinymce/tinymce.min.js'); ?>"></script>
    <script type="text/javascript"
            src="<?php echo site_url('res/js/plugins/metisMenu/jquery.metisMenu.js'); ?>"></script>
</head>