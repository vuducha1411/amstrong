<!DOCTYPE html>
<html lang="en" class="">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_title; ?></title>

    <link rel="stylesheet" href="<?php echo site_url('res/css/bootstrap.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/jqueryui/jquery-ui-1.10.4.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/font-awesome/css/font-awesome.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('res/css/ami.css'); ?>"/>
    
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="padding-top: 10px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <?php echo $this->load->view($subview); // Subview is set in controller ?>
            </div>
        </div>
    </div>

<script type="text/javascript" src="<?php echo site_url('res/js/jquery-1.11.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('res/js/jquery-ui-1.10.4.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('res/js/bootstrap.min.js'); ?>"></script>
<script>
$(function() {

    $(document).on('click', '.country_select', function (e) {
        var $this = $(this),
            countryId = $this.data('countryid');

        if (countryId)
        {
            $('#countrySelect').empty().html($this.html());
            $('#countryId').val(countryId);
        }
    });

});
</script>
</body>
</html>
