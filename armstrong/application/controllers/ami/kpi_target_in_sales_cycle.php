<?php

class Kpi_target_in_sales_cycle extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('kpi_target_in_sales_cycle_m');
        $this->load->model('kpi_actual_in_sales_cycle_m');
        $this->load->model('sales_cycle_m');
        $this->load->model('salespersons_m');
        $this->load->model('products_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $conditions = array('kpi_target_in_sales_cycle.country_id' => $this->country_id, 'kpi_target_in_sales_cycle.is_draft' => 0);

        $this->data['data'] = $this->kpi_target_in_sales_cycle_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_target_in_sales_cycle_m->count('where', $conditions);
        $range = $this->sales_cycle_m->getRangeListOption(null, array('country_id' => $this->country_id, 'is_draft' => 0));
        $this->data['range'] = $range;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('KPI Target');
        return $this->render('ami/kpi_target_in_sales_cycle/index1', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $conditions = array('kpi_target_in_sales_cycle.country_id' => $this->country_id, 'kpi_target_in_sales_cycle.is_draft' => 1);

        $this->data['settings'] = $this->kpi_target_in_sales_cycle_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_target_in_sales_cycle_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('KPI Settings');

        return $this->render('ami/kpi_target_in_sales_cycle/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $products = $this->products_m->getProductListOptions('- Select ', array_merge(array('listed' => 1), $conditions));
        $range = $this->sales_cycle_m->getRangeListOption(null, $conditions);
        return array(
            'country' => $this->country,
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', array_merge(array('type' => 'PULL', 'active' => 1), $conditions), true),
            'products' => $products,
            'range' => $range
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();

        $this->data['post'] = array('month' => date('m', time()), 'year' => date('Y', time()));
        return $this->render('ami/kpi_target_in_sales_cycle/edit1', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'kpi_target_in_sales_cycle') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target_in_sales_cycle');

        if ($id) {
            $post = $this->kpi_target_in_sales_cycle_m->get($id);

            $this->data['post'] = $post;
            $this->assertCountry($this->data['post']);
            $this->data['page_title'] = page_title('Edit KPI In Sales Cycle Settings');

            $focused_skus = json_decode($post['focused_skus_target']);
            $special_tasks = json_decode($post['special_tasks_target']);

            $this->data += $this->_getAddEditParams();
            $this->data['focus_skus'] = $focused_skus;
            $this->data['special_tasks'] = $special_tasks;
            return $this->render('ami/kpi_target_in_sales_cycle/edit1', $this->data);
        }

        return redirect('ami/kpi_target_in_sales_cycle');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->kpi_target_in_sales_cycle_m->save(array(
                    'id' => $id,
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
            }
        }

        return redirect('ami/kpi_target_in_sales_cycle');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target_in_sales_cycle');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi_target_in_sales_cycle';
                //$this->roles_m->delete($id);
                $this->kpi_target_in_sales_cycle_m->save(array(
                    'id' => $id,
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/kpi_target_in_sales_cycle/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi_target_in_sales_cycle');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target_in_sales_cycle');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi_target_in_sales_cycle/draft';
                $this->kpi_target_in_sales_cycle_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/kpi_target_in_sales_cycle/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi_target_in_sales_cycle');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'kpi_target_in_sales_cycle') && !$this->hasPermission('edit', 'kpi_target_in_sales_cycle')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->kpi_target_in_sales_cycle_m->array_from_post(array(
            'id', 'sales_cycle_id', 'armstrong_2_salespersons_id', 'turnover_target',
            'touchpoint_target', 'focused_skus_target', 'focused_skus_amount_target', 'special_tasks_target', 'special_tasks_amount_target'
        ));

        $id = $data['id'] ? $data['id'] : null;
        $data['country_id'] = $this->country_id;
        $data['special_tasks_target'] = json_encode(array_values($this->removeEmptyKey($data['special_tasks_target'])));
        $focused_skus_targets = array_values($this->removeEmptyKey($data['focused_skus_target']));
        if($focused_skus_targets){
            foreach($focused_skus_targets as &$focused_skus_target){
                $product = $this->products_m->get_by(array(
                    'sku_number' => $focused_skus_target['sku_number'],
                    'country_id' => $this->country_id,
                    'listed' => 1,
                    'is_draft' => 0
                ), true);

                $focused_skus_target['sku_name'] = $product ? $product['sku_number'] . ' | ' . $product['sku_name'] : '';
            }
        }
        $data['focused_skus_target'] = json_encode($focused_skus_targets);

        $this->kpi_target_in_sales_cycle_m->save($data, $id);

        return redirect('ami/kpi_target_in_sales_cycle');
    }

    function removeEmptyKey($array)
    {
        foreach ($array as $key => $val) {
            $val = array_values($val);
            for($i=0; $i < count($val); $i++){
                if($val[$i] == '') {
                    unset($array[$key]);
                    break;
                }
            }
        }
        return $array;
    }

    public function upload_template()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $range_id = $this->input->post('range_id');
            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                $this->session->set_flashdata('message', PluploadHandler::get_error_message());
                $this->session->set_flashdata('flash_type', 'alert-danger');
            }else{
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
                    {
                        $datas = $worksheet->toArray();
                        foreach($datas as $index => &$data){
                            if($index == 0){
                                $data[] = 'sales_cycle_id';
                                $data[] = 'from_date';
                                $data[] = 'to_date';
                            }else{
                                if ($data[5] == '') $data[5] = json_encode(array());
                                if ($data[7] == '') $data[7] = json_encode(array());
                                $sales_cycle = $this->sales_cycle_m->get($range_id);
                                if($sales_cycle) {
                                    $data[] = $range_id;
                                    $data[] = $sales_cycle['from_date'];
                                    $data[] = $sales_cycle['to_date'];
                                }
                            }
                        }

                        $this->kpi_actual_in_sales_cycle_m->importToDb($this->country_id, $datas, array('id'));
                    }

                    $this->session->set_flashdata('message', 'Import complete.');
                    $this->session->set_flashdata('flash_type', 'alert-success');
                }else{
                    $this->session->set_flashdata('message', 'Upload complete.');
                    $this->session->set_flashdata('flash_type', 'alert-danger');
                }
            }
        }

        return redirect('ami/kpi_target_in_sales_cycle');
    }

}