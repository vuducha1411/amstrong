<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tfo extends AMI_Controller {

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('tfo_m');
        $this->load->model('salespersons_m');
        $this->load->model('customers_m');
        $this->load->model('wholesalers_m');
        $this->load->model('distributors_m');
        $this->load->model('call_records_m');
        $this->load->model('products_m');        
    }

    public function index() 
    {
        if (!$this->hasPermission('view', 'tfo'))
        {
            return $this->noPermission();
        }

        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
          $conditions = $this->getListSalespersonsConditions();
        }
        // $this->data['tfo'] = $this->tfo_m->get_limit($this->getPageLimit(), $conditions);
        // $this->data['total'] = $this->tfo_m->count('where', $conditions);
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons'))
//        {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));

        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Tfo');

        return $this->render('ami/tfo/index', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'tfo'))
        {
            return $this->noPermission();
        }

        // $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        // $this->data['tfo'] = $this->tfo_m->get_limit($this->getPageLimit(), $conditions);
        // $this->data['total'] = $this->tfo_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/tfo/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        // default conditions
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata( 'user_data' );
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, false, null, array('admins_id', 'armstrong_2_salespersons_id' ));
        // check type role of user
        if($user['roles_id'] == $roles_salesmanager){
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id']);
        }else if($user['roles_id'] == $roles_salesperson){
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id']);
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            // 'customers' => $this->customers_m->getCustomerListOptions('- Select -', $conditions),
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true),
            'wholesalers' => $this->wholesalers_m->getSalerListOptions(null, $conditions),
            'call_records' => $this->call_records_m->parse_form(array('armstrong_2_call_records_id', 'armstrong_2_call_records_id'), '- Select -', $conditions),
            'distributors' => $this->distributors_m->getDistributorListOptions(null, $conditions),
            'function_method' => $this->router->fetch_method(),
            'tfo_method' => $this->tfo_m->getMethodListOptions(),
            //'products' => $this->products_m->getProductListOptions('- Select -', $conditions),
            'products' => array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'tfo'))
        {
            return $this->noPermission();
        }
        $this->data += $this->_getAddEditParams();
		
        // $this->data['customers'] = array('' => ' - Select - ');
        return $this->render('ami/tfo/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'tfo') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/tfo');

        if ($id)
        {
            $this->data['tfo'] = $tfo = $this->tfo_m->get($id);
            $this->assertCountry($this->data['tfo']);
            $this->data['tfo']['products_array'] = json_decode($this->data['tfo']['products'], true) ?
                json_decode($this->data['tfo']['products'], true) : array();
            $this->data += $this->_getAddEditParams();

            $this->data['page_title'] = page_title('Edit Tfo');                        

            if ($this->input->is_ajax_request())
            {            
                if (!$this->hasPermission('view', 'tfo'))
                {
                    return $this->noPermission();
                }

                $this->data['tfo']['customer'] = $tfo['armstrong_2_customers_id'] ? $this->customers_m->get($tfo['armstrong_2_customers_id']) : array();
                $this->data['tfo']['salesperson'] = $tfo['armstrong_2_salespersons_id'] ? $this->salespersons_m->get($tfo['armstrong_2_salespersons_id']) : array();
                $this->data['tfo']['wholesaler'] = $tfo['armstrong_2_wholesalers_id'] ? $this->wholesalers_m->get($tfo['armstrong_2_wholesalers_id']) : array();
                $this->data['tfo']['distributor'] = $tfo['armstrong_2_distributors_id'] ? $this->distributors_m->get($tfo['armstrong_2_distributors_id']) : array();

                $this->data['tfo']['call_record'] = $tfo['armstrong_2_call_records_id'] ? $this->call_records_m->get($tfo['armstrong_2_call_records_id']) : array();

                return $this->render('ami/tfo/preview', $this->data);
            }
            else
            {
                $this->data['customers'] = $this->customers_m->getCustomerListOptions(null, array(
                    'country_id' => $this->country_id, 
                    'is_draft' => 0,
                    'armstrong_2_salespersons_id' => $tfo['armstrong_2_salespersons_id'],
                    'approved' => 1,
                    'active' => 1
                ));

                return $this->render('ami/tfo/edit', $this->data);
            }
        }

        return redirect('ami/tfo');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'tfo'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id)
            {
                $this->tfo_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/tfo');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'tfo'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/tfo');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/tfo';
                //$this->tfo_m->delete($id);
                $this->tfo_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/tfo/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Tfo"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/tfo');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'tfo'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/tfo');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/tfo/draft';
                $this->tfo_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/tfo/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Tfo"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/tfo');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'tfo') && !$this->hasPermission('edit', 'tfo'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->tfo_m->array_from_post(array('armstrong_2_customers_id', 'armstrong_2_distributors_id', 'armstrong_2_salespersons_id',
            'armstrong_2_wholesalers_id', 'armstrong_2_call_records_id', 'products', 'total', 'method', 'delivery_date', 'remarks', 'armstrong_2_tfo_id'));

        $data['country_id'] = $this->country_id;

        $id = $data['armstrong_2_tfo_id'] ? $data['armstrong_2_tfo_id'] : null;

        $products = array();

        if ($data['products'])
        {

            foreach ($data['products'] as $product) 
            {
                /* update price */
                if(isset($product['price'])){
                    if(strpos($product['price'],',') > 0){
                        $new_price = str_replace(',','',$product['price']);
                    }else {
                        $new_price = $product['price'];
                    }

                    $product['price'] = (int) $new_price;
                }

                if(in_array($this->country_code, array('au', 'nz'))){
                    $product['newSKU'] = isset($product['newSKU']) ? 'true' : 'false';
                }
                //hieu
                if(isset($product['productencode'])){
                    $old_product = @json_decode($product['productencode']);
                    foreach($old_product as $key => $value)
                    {
                        if(!array_key_exists($key, $product))
                        {
                            $product[$key] = $value;
                        }
                    }
                    unset($product['productencode']);
                }
                //end hieu
                if ($product['sku_number'])
                {
                    $products[] = $product;
                }
            }

        }

        $data['products'] = json_encode($products);
        $data['total'] = floatval(str_replace(',', '', $data['total']));
        // create new record
        if (!$id)
        {
            //$data['armstrong_2_tfo_id'] = $this->tfo_m->generateArmstrongId($this->country_id, 'tfo');
            $newId = $this->tfo_m->save($data, null, true, $this->country_id);
        }
        else
        {
            $newId = $this->tfo_m->save($data, $id);
        }
        return redirect('ami/tfo');
    }   

    public function export($type)
    {    
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
        || !$this->hasPermission('view', 'tfo'))
        {
            return redirect(site_url('ami/tfo'));
        }

        $tfo = $this->tfo_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($tfo);

        return $this->excel($sheet, 'tfo', $type);
    }

    public function import()
    {
        if ($this->is('POST'))
        {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) 
            {
                return $this->json(array(
                    'OK' => 0, 
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } 
            else 
            {
                if (!empty($upload['path']))
                {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) 
                    {                        
                        $this->tfo_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function tfo_data()
    {
        if (!$this->hasPermission('view', 'tfo'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        if ($id = $this->input->post('id'))
        {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date'));

            // if ($startDate->eq($endDate))
            // {
                $endDate->addDay();
            // }

            $tfoTable = $this->tfo_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();

            $conditions = array(
                // 'armstrong_2_salespersons_id' => $id, 
                "{$tfoTable}.is_draft" => $this->input->post('draft'),
                "{$tfoTable}.date_created >=" => $startDate->toDateTimeString(),
                "{$tfoTable}.date_created <=" => $endDate->toDateTimeString(),
                "{$tfoTable}.country_id" => $this->country_id
            );   

        
            if ($id[0] == 'ALL')
            {
                // $this->data['tfo'] = $this->tfo_m->get_by($conditions, false);
                $this->data['tfo'] = $this->db->select("{$tfoTable}.*, 
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($tfoTable)
                    ->join($salespersonsTable, "{$tfoTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$tfoTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where($conditions)
                    ->get()->result_array();
            }
            else
            {
                // $this->data['tfo'] = $this->db->where_in('armstrong_2_salespersons_id', $id)
                //     ->where($conditions)
                //     ->get($this->tfo_m->getTableName())
                //     ->result_array();
                $this->data['tfo'] = $this->db->select("{$tfoTable}.*, 
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($tfoTable)
                    ->join($salespersonsTable, "{$tfoTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$tfoTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where_in("{$tfoTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();
            }


            // $this->data['tfo'] = $this->tfo_m->get_by($conditions, false);

            $this->data['draft'] = $this->input->post('draft');
            $this->data['methods'] = $this->tfo_m->getMethodListOptions();

            return $this->render('ami/tfo/tfo', $this->data);
        }

        return '';
    }
}