<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Route_plan extends AMI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('route_plan_m');
        $this->load->model('customers_m');
        $this->load->model('salespersons_m');
    }

    public function hasPermission($type, $permission, $user = null)
    {
        if ($type == 'delete' && $permission == 'route_plan')
        {
            return false;
        }

        return parent::hasPermission($type, $permission, $user);
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'route_plan'))
        {
            return $this->noPermission();
        }

        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons'))
//        {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Route Plan');

        return $this->render('ami/route_plan/index', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'route_plan'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $defaultOptions = array('' => ' - Select - ');

        if ($this->hasPermission('manage', 'salespersons'))
        {
            $defaultOptions['ALL'] = 'ALL';
        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Route Plan');

        return $this->render('ami/route_plan/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        // default conditions
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata( 'user_data' );
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, false, null, array('admins_id', 'armstrong_2_salespersons_id' ));
        // check type role of user
        if($user['roles_id'] == $roles_salesmanager){
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id']);
        }else if($user['roles_id'] == $roles_salesperson){
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id']);
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            // 'modules' => $this->menu_m->get(),
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true),
            // 'customers' => $this->customers_m->getCustomerListOptions('- Select -', $conditions),
            'status' => $this->route_plan_m->getRouteStatusListOptions()
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['customers'] = array('' => ' - Select - ');
        return $this->render('ami/route_plan/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'route_plan') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/route_plan');

        if ($id)
        {
            $this->data['plan'] = $this->route_plan_m->get($id); 
            $this->data['plan']['planned_date'] = date('Y-m-d', strtotime($this->data['plan']['planned_date']));
            $this->data['plan']['shifted_date'] = date('Y-m-d', strtotime($this->data['plan']['shifted_date']));
            $this->data['page_title'] = page_title('Edit Route Plan');

            $this->data += $this->_getAddEditParams();

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'route_plan'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/route_plan/preview', $this->data);
            }
            else
            {          
                $this->data['customers'] = $this->customers_m->getCustomerListOptions('- Select -', array(
                    'country_id' => $this->country_id, 
                    'is_draft' => 0,
                    'armstrong_2_salespersons_id' => $this->data['plan']['armstrong_2_salespersons_id'],
                    'approved' => 1,
                    'active' => 1
                ));       
                return $this->render('ami/route_plan/edit', $this->data);
            }
        }

        return redirect('ami/route_plan');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id)
            {
                $this->route_plan_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/route_plan');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/route_plan');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/route_plan';
                //$this->route_plan_m->delete($id);
                $this->route_plan_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/route_plan/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/route_plan');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/route_plan');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/route_plan/draft';
                $this->route_plan_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/route_plan/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/route_plan');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'route_plan') && !$this->hasPermission('edit', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->roles_m->array_from_post(array('planned_date', 'shifted_date', 'armstrong_2_customers_id', 
            'armstrong_2_salespersons_id', 'route_status_id', 'armstrong_2_route_plan_id'));        

        $id = $data['armstrong_2_route_plan_id'] ? $data['armstrong_2_route_plan_id'] : null;



        $data['planned_date'] = date('Y-m-d H:i:s', strtotime($data['planned_date']));
        $data['shifted_date'] = $data['shifted_date'] ? date('Y-m-d H:i:s', strtotime($data['shifted_date'])) : $data['planned_date'];
        $data['version'] = 'ami';
        if (!$id)
        {
            $data['armstrong_2_route_plan_id'] = ''; //$this->route_plan_m->generateArmstrongId($this->country_id, 'route_plan');
            $newId = $this->route_plan_m->save($data, null, true, 'INSERT', $this->country_id);
        }
        else
        {
            $newId = $this->route_plan_m->save($data, $id);
        }

        return redirect('ami/route_plan');
    }

    public function export($type)
    {    
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
        || !$this->hasPermission('view', 'route_plan'))
        {
            return redirect(site_url('ami/route_plan'));
        }

        $plans = $this->db->select($this->_table_name . '.*')
            ->from($this->_table_name)
            ->join('salespersons', $this->_table_name . '.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id')
            ->where(array('country_id' => $this->country_id))
            ->get()->result_array();

        $sheet = $this->makeSheet($plans);

        return $this->excel($sheet, 'route_plan', $type);
    }

    public function plans()
    {
        if (!$this->hasPermission('view', 'route_plan'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        if ($id = $this->input->post('id'))
        {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date'));

            if ($startDate->eq($endDate))
            {
                $endDate->addDay();
            }

            $routePlanTable = $this->route_plan_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();

            $conditions = array(
                "{$routePlanTable}.is_draft" => $this->input->post('draft'),
                "{$routePlanTable}.shifted_date >=" => $startDate->toDateTimeString(),
                "{$routePlanTable}.shifted_date <=" => $endDate->toDateTimeString()
            );   

            if ($id[0] == 'ALL')
            {
                $this->data['plans'] = $this->db->select("{$routePlanTable}.*, 
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($routePlanTable)
                    ->join($salespersonsTable, "{$routePlanTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$routePlanTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where(array_merge($conditions, array("{$salespersonsTable}.country_id" => $this->country_id)))
                    ->get()->result_array();
            }
            else
            {
                // $conditions["{$routePlanTable}.armstrong_2_salespersons_id"] = $id;
                // $this->data['plans'] = $this->route_plan_m->get_by($conditions, false);
                $this->data['plans'] = $this->db->select("{$routePlanTable}.*, 
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($routePlanTable)
                    ->join($salespersonsTable, "{$routePlanTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$routePlanTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where_in("{$routePlanTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();
            }

            $this->data['draft'] = $this->input->post('draft');

            return $this->render('ami/route_plan/plan');
        }

        return '';
    }

    public function info()
    {
        $id = $this->input->get('id');

        if (!$id)
        {
            return $this->json(array());
        }

        return $this->json($this->route_plan_m->get($id));
    }
}