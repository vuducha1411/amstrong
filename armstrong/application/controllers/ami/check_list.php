<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Check_list extends AMI_Controller {

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('check_list_m');
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $check_list = $this->check_list_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $check_list = $this->check_list_m->prepareDataTables($check_list);

        return $this->datatables->generate($check_list);
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'check_list'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $this->data['check_list'] = array(
            'active' => $this->check_list_m->get_by(array_merge($conditions, array('active' => 1))),
            'inactive' => $this->check_list_m->get_by(array_merge($conditions, array('active' => 0))),
        );
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Check List');

        return $this->render('ami/check_list/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'check_list'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['check_list'] = $this->check_list_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->check_list_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Check List');

        return $this->render('ami/check_list/index', $this->data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'check_list'))
        {
            return $this->noPermission();
        }

        return $this->render('ami/check_list/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'check_list') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/check_list');

        if ($id = intval($id))
        {
            $this->data['check_list'] = $this->check_list_m->get($id);
            $this->assertCountry($this->data['check_list']);
            $this->data['page_title'] = page_title('Edit Check List');

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'check_list'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/check_list/preview', $this->data);
            }
            else
            {
                return $this->render('ami/check_list/edit', $this->data);
            }
        }

        return redirect('ami/check_list');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'check_list'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id)
        {
            if ($id = intval($id))
            {
                $this->check_list_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/check_list');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'check_list'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/check_list');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/check_list';
                //$this->check_list_m->delete($id);
                $this->check_list_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/check_list/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Check List"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/check_list');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'check_list'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/check_list');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/check_list/draft';
                $this->check_list_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/check_list/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Check List"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/check_list');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'check_list') && !$this->hasPermission('edit', 'check_list'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->check_list_m->array_from_post(array('id', 'name', 'active'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        $check_listId = $this->check_list_m->save($data, $id);

        return redirect('ami/check_list');
    }
}