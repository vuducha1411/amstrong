<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pantry_check extends AMI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('pantry_check_m');
        $this->load->model('call_records_m');
        $this->load->model('products_m');
        $this->load->model('customers_m');
    }

    public function index() 
    {
        if (!$this->hasPermission('view', 'pantry_check'))
        {
            return $this->noPermission();
        }

        // $conditions = array('call_records.country_id' => $this->country_id, 'pantry_check.is_draft' => 0);
        
        // $this->data['pantry_checks'] = $this->pantry_check_m->getPantryCheck($conditions, $this->getPageLimit());
        // $this->data['total'] = $this->pantry_check_m->countPantryCheck('where', $conditions);
        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons'))
//        {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));

        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Pantry Check');

        return $this->render('ami/pantry_check/index', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'pantry_check'))
        {
            return $this->noPermission();
        }

        // $conditions = array('call_records.country_id' => $this->country_id, 'pantry_check.is_draft' => 1);
        
        // $this->data['pantry_checks'] = $this->pantry_check_m->getPantryCheck($conditions, $this->getPageLimit());
        // $this->data['total'] = $this->pantry_check_m->countPantryCheck('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Pantry Check');

        return $this->render('ami/pantry_check/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            'call_records' => $this->call_records_m->getCallRecordListOptions(null, $conditions),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        return $this->render('ami/pantry_check/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'pantry_check') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/pantry_check');

        if ($id)
        {
            $pantry = $this->pantry_check_m->get($id);  

            if (!empty($pantry['pantry']))
            {
                $pantry['pantry_array'] = json_decode($pantry['pantry'], true);

                if (is_array($pantry['pantry_array']))
                {
                    $pantry['pantry_array'] = array_rewrite($pantry['pantry_array'], 'sku_number');
                    // foreach ($pantry['pantry_array'] as $value) 
                    // {
                    //     # code...
                    // }
                    $pantry['products'] = $this->products_m->get_in('sku_number', array_keys($pantry['pantry_array']));
                }
            }

            $this->data['pantry_check'] = $pantry;          
            $this->data['page_title'] = page_title('Edit Pantry Check');

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'pantry_check'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/pantry_check/preview', $this->data);
            }
            else
            {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/pantry_check/edit', $this->data);
            }
        }

        return redirect('ami/pantry_check');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id)
            {
                $this->pantry_check_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/pantry_check');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/pantry_check');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/pantry_check';
                //$this->pantry_check_m->delete($id);
                $this->pantry_check_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/pantry_check/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Pantry Check"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/pantry_check');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/pantry_check');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/pantry_check/draft';
                $this->pantry_check_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/pantry_check/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Pantry Check"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/pantry_check');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'pantry_check') && !$this->hasPermission('edit', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->pantry_check_m->array_from_post(array('armstrong_2_call_records_id', 'notes', 'armstrong_2_pantry_check_id'));

        $id = $data['armstrong_2_pantry_check_id'] ? $data['armstrong_2_pantry_check_id'] : null;

        $data['version_no'] = 'ami';

        // create new record
        if (!$id)
        {
            $data['armstrong_2_pantry_check_id'] = ''; //$this->pantry_check_m->generateArmstrongId($this->country_id, 'pantry_check');
            $newId = $this->pantry_check_m->save($data, null, true, "INSERT", $this->country_id);
        }
        else
        {
            $newId = $this->pantry_check_m->save($data, $id);
        }

        return redirect('ami/pantry_check');
    }   

    public function export($type)
    {    
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
        || !$this->hasPermission('view', 'pantry_check'))
        {
            return redirect(site_url('ami/pantry_check'));
        }

        $pantry_check = $this->pantry_check_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($pantry_check);

        return $this->excel($sheet, 'pantry_check', $type);
    }

    public function import()
    {
        if ($this->is('POST'))
        {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) 
            {
                return $this->json(array(
                    'OK' => 0, 
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } 
            else 
            {
                if (!empty($upload['path']))
                {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) 
                    {                        
                        $this->pantry_check_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function pantry_check_data()
    {
        if (!$this->hasPermission('view', 'pantry_check'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        if ($id = $this->input->post('id'))
        {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date')); 

            // if ($startDate->eq($endDate))
            // {
                $endDate->addDay();
            // }

            $pantryCheckTable = $this->pantry_check_m->getTableName();
            $callRecordsTable = $this->call_records_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();

            $conditions = array(
                "{$pantryCheckTable}.is_draft" => $this->input->post('draft'),
                "{$pantryCheckTable}.date_created >=" => $startDate->toDateTimeString(),
                "{$pantryCheckTable}.date_created <=" => $endDate->toDateTimeString(),
            );   

            if ($id[0] == 'ALL')
            {
                $this->data['pantry_checks'] = $this->db->select("{$pantryCheckTable}.*, 
                        {$callRecordsTable}.armstrong_2_salespersons_id, {$callRecordsTable}.armstrong_2_customers_id, 
                        {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                        {$customersTable}.armstrong_2_customers_name
                    ")
                    ->from($pantryCheckTable)
                    ->join($callRecordsTable, "{$pantryCheckTable}.armstrong_2_call_records_id = {$callRecordsTable}.armstrong_2_call_records_id")
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where(array_merge($conditions, array("{$callRecordsTable}.country_id" => $this->country_id)))
                    ->get()->result_array();
            }
            else
            {

                $this->data['pantry_checks'] = $this->db->select("{$pantryCheckTable}.*, 
                        {$callRecordsTable}.armstrong_2_salespersons_id, {$callRecordsTable}.armstrong_2_customers_id, 
                        {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                        {$customersTable}.armstrong_2_customers_name
                    ")
                    ->from($pantryCheckTable)
                    ->join($callRecordsTable, "{$pantryCheckTable}.armstrong_2_call_records_id = {$callRecordsTable}.armstrong_2_call_records_id")
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where_in("{$callRecordsTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();
            }

            $this->data['draft'] = $this->input->post('draft');

            return $this->render('ami/pantry_check/pantry_check', $this->data);
        }

        return '';
    }
}