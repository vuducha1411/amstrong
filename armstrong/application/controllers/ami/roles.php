<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Date: 8/30/14
 * Time: 7:35 PM
 */

class Roles extends AMI_Controller
{
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
        // Add module to database
	    $parentExtraFields = array(
		    'name' => 'ami_setting',
		    'display_name' => 'AMI Settings',
		    'date_created' => date('Y-m-d H:i:s'),
		    'last_updated' => date('Y-m-d H:i:s'),
	    );

	    $childExtraFields = array(
		    '0' => array(
			    'name' => 'push_notification',
			    'display_name' => 'Push Notification',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '1' => array(
			    'name' => 'force_sync',
			    'display_name' => 'Force Sync',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '2' => array(
			    'name' => 'country_module',
			    'display_name' => 'Country Module',
		    'date_created' => date('Y-m-d H:i:s'),
		    ),
		    '3' => array(
			    'name' => 'rich_media_template',
			    'display_name' => 'Rich Media Template',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '4' => array(
			    'name' => 'gps_tracking',
			    'display_name' => 'GPS Tracking',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '5' => array(
			    'name' => 'working_days',
			    'display_name' => 'Working Days',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '6' => array(
			    'name' => 'price',
			    'display_name' => 'Price Shown',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '7' => array(
			    'name' => 'language_settings',
			    'display_name' => 'Language Setting',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
		    '8' => array(
			    'name' => 'holiday_settings',
			    'display_name' => 'Holiday Settings',
			    'date_created' => date('Y-m-d H:i:s'),
			    'last_updated' => date('Y-m-d H:i:s'),
		    ),
            '9' => array(
                'name' => 'role',
                'display_name' => 'Roles',
                'date_created' => date('Y-m-d H:i:s'),
                'last_updated' => date('Y-m-d H:i:s'),
            )
	    );
		// $parentNode = $this->nested_set->getNodeFromId(1);
	 //    $parentNode = $this->nested_set->initialiseRoot($parentExtraFields);
	 //    dump($parentNode);
	 //    foreach ($childExtraFields as $extraFields) {
		// 	$return = $this->nested_set->insertNewChild($parentNode, $extraFields);
		//     dump($return);
	 //    }

//	    $node = $this->nested_set->getNodeFromId(29);
//	    $this->nested_set->deleteNode($node);
    }
}
