<?php
/**
 * Created by PhpStorm.
 * User: OPSOLUTIONS PH INC - Erika Liongco
 * Date: 8/8/2017
 * Time: 5:20 PM
 */

if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Webshop extends AMI_Controller {

    protected $dtl_table_name = 'webshop_details';
    protected $ptl_table_name = 'potential_customers';
    protected $pull_avp;
    protected $push_avp;
    protected $session_id;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_m');
        $this->load->model('webshop_m');
        $this->load->model('webshop_details_m');
        $this->load->model('distributors_m');
        $this->load->model('customers_m');
        $this->load->model('wholesalers_m');
        $this->load->model('call_records_m');

        /* get AVP values */
        $country_data = $this->webshop_m->getRecord('country','application_config',array('id' => $this->country_id));
        $config = json_decode($country_data[0]->application_config);
        $this->pull_avp = (property_exists($config->pull, 'enable_enhanced_avp') ? $config->pull->enable_enhanced_avp : 0);
        $this->push_avp = (property_exists($config->push,'enable_enhanced_avp') ? $config->push->enable_enhanced_avp : 0);
        /* set session */
        $this->session_id = $this->session->userdata('session_id');
    }

    public function index()
    {
        $this->data['country_id'] = $this->country_id;
        $this->data['session_id'] = $this->session_id;

        return $this->render('ami/webshop/index', $this->data);
    }

    public function get_raw_data()
    {
        $req_fields = "order_id, 
        businessName, 
        CONCAT(firstName,\" \",lastName) AS contact_name, 
        ufsClientNumber, 
        orderTotalPrice, 
        order_date, 
        date_created";
        $where = array("country_id" => $this->country_id, "is_tagged" => '0', 'is_armstrong' => "1", 'is_draft' => 0);
        $result = $this->webshop_m->getRecord('webshop_details', $req_fields, $where, 'order_id');

        foreach ($result as $key => $val) {
            $url = site_url("ami/webshop/tag/{$val->order_id}");
            $result[$key]->edit_del = "<a href={$url} class='btn btn-default-edit' title='Tag'><i class='fa fa-users'></i></a>";
        }

        $wbData['data'] = $result;
        $wbData['recordsTotal'] = count($result);
        $wbData['recordsFiltered'] = count($result);
        $wbData['draw'] = 1;

        return $this->json($wbData);
    }

    public function get_webshop()
    {
        $req_fields = "armstrong_2_webshop_id, 
        webshop_order_id, 
        w.armstrong_2_salespersons_id, 
        c.armstrong_2_customers_name, 
        armstrong_2_call_records_id, 
        total, 
        w.date_created";
        $where = array('w.country_id' => $this->country_id, 'w.is_draft' => 0);

//        $wbData = $this->webshop_m->getRecord('', $req_fields, $where, 'webshop_order_id');
        $wbData = $this->webshop_m->getJoinRecord('', $req_fields,'customers c','w.armstrong_2_customers_id = c.armstrong_2_customers_id','LEFT', $where);
        foreach ($wbData as $key => $val) {
            $url = site_url("ami/webshop/edit/{$val->armstrong_2_webshop_id}");
//            $wbData[$key]->edit_del = "<button onclick='editWebOrder(" . $val->id . ")'><i class='fa fa-edit fa-fw'></i></button>";
            $wbData[$key]->edit_del = "<a href='{$url}' class='btn btn-default-edit' title='Edit'><i class='fa fa-edit'></i></a>";
        }

        $wbDataTable['data'] = $wbData;
        $wbDataTable['recordsTotal'] = count($wbData);
        $wbDataTable['recordsFiltered'] = 10;
        $wbDataTable['draw'] = 1;

        return $this->json($wbDataTable);
    }

    public function get_webshop_untagged()
    {
        $req_fields = "order_id, 
        businessName, 
        CONCAT(firstName,\" \",lastName) AS contact_name, 
        armstrong_1_customers_id, 
        orderTotalPrice, 
        order_date, 
        date_created";
        $where = array("country_id" => $this->country_id, "is_tagged" => '0', 'is_armstrong' => '0', 'is_draft' => 0);

        $result = $this->webshop_m->getRecord('webshop_details', $req_fields, $where, 'order_id');

        foreach ($result as $key => $val) {
            $url = site_url("ami/webshop/view/{$val->order_id}");
            $result[$key]->customer_id = ($val->armstrong_1_customers_id != "" ? $val->armstrong_1_customers_id : "");
            $result[$key]->edit_del = "<a href={$url} class='btn btn-default-edit' title='Edit'><i class='fa fa-users'></i></a>";
        }

        $wbData['data'] = $result;
        $wbData['recordsTotal'] = count($result);
        $wbData['recordsFiltered'] = count($result);
        $wbData['draw'] = 1;

        return $this->json($wbData);
    }

    /*public function get_order_detail_by_id(){
        $data = $_POST;
        $where = array("country_id" => $this->country_id, "order_id" => $data['id']);
        $products = $this->db->select("*")
            ->from("webshop_details")
            ->where($where)
            ->get()
            ->result_array();

        $result['data'] = $products;

        return $this->json($result);
    }*/

    public function tag($order_id)
    {
        $as_total_order = 0;
        /* get webshop details */
        $where = array("country_id" => $this->country_id, "order_id" => $order_id);
        $result = $this->webshop_m->getRecord('webshop_details', '*', $where);

        $this->data['order_id'] = $order_id;
        $this->data['armstrong_2_customers_id'] = "";
        $this->data['armstrong_2_salespersons_id'] = "";
        $this->data += $this->_getAddEditParams();
        /* get customer data with salesperson */
        if($result[0]->ufsClientNumber != ""){
            $customer = $this->webshop_m->getRecord('customers',
                'armstrong_2_customers_id, armstrong_2_salespersons_id',
                array('armstrong_2_customers_id' => $result[0]->ufsClientNumber));
            if(!empty($customer)){
                $this->data['armstrong_2_customers_id'] = $customer[0]->armstrong_2_customers_id;
                $this->data['armstrong_2_salespersons_id'] = $customer[0]->armstrong_2_salespersons_id;
            }
        }

        /* map distributor */
        if($result[0]->tradePartnerId != ""){
            $distributor = $this->webshop_m->getRecord('distributors',
                'armstrong_2_distributors_id, armstrong_2_salespersons_id',
                array('sifu_id' => $result[0]->tradePartnerId));
            if(!empty($distributor)){
                $this->data['armstrong_2_distributors_id'] = $distributor[0]->armstrong_2_distributors_id;
                // $this->data['armstrong_2_salespersons_id'] = $distributor[0]->armstrong_2_salespersons_id;
            }
        }

        foreach($result as $key => $val){
            /*$product = $this->webshop_m->getRecord('products',
                'price, quantity_case',
                array('sku_number'=> $val->productNumber));*/
            // "as" = Armstrong
            /*$as_price = $product[0]->price;
            $as_quantity_case = $product[0]->quantity_case;*/
            $as_total = $val->productQuantity * $val->price;// $as_price;
            $as_total_order += $val->total; // $as_total;

            $this->data['products'][$key] = array(
                'product_number' => $val->productNumber,
                'product_name' => $val->productName,
                'product_quantity' => $val->productQuantity,
                'product_package_type' => ($val->packagingType == 'DU' ? 'Case' : 'Pcs'),
                'product_price' => $val->productPrice,
                'product_total_price' => $val->totalProductPrice,
                /* product converted to armstrong values */
                'armstrong_qty_case' => $val->qty_case,
                'armstrong_qty_pcs' => $val->qty_piece,
                'armstrong_qty_per_case' => $val->quantity_case,
                'armstrong_product_price' => $val->price, //$as_price,
                'armstrong_total_price' => round($val->total, 2) //$as_total
            );
        }
        $this->data['total_order'] = round($as_total_order,2);
        $this->data['order_date'] = $result[0]->order_date;

        return $this->render('ami/webshop/tag', $this->data);
    }

    public function add_new(){
        $formdata = $_POST;

        $this->save($formdata);
        
        return redirect('ami/webshop');
    }

    public function save($data){
        $settings = ($data['armstrong_2_customers_id'] != "" ? $this->pull_avp : $this->push_avp);
        $app_type = ($data['armstrong_2_customers_id'] != "" ? 0 : 1);

        if ($data['armstrong_2_webshop_id'] == "") {
            unset($data['armstrong_2_webshop_id']);

            $orderId = $data['webshop_order_id'];

            /* remove id field on products info */
            foreach($data['armstrong'] as $key => $val){
                foreach($val as $k=>$v){
                    if($k == 'id'){
                        unset($data['armstrong'][$key][$k]);
                    }
                }
            }
            $data['products'] = json_encode($data['armstrong']);

            unset($data['armstrong'], $data['webshop']);

            $now = date('Y-m-d h:i:s');
            $data['country_id'] = $this->country_id;
            $data['date_created'] = $now;
            $data['last_updated'] = $now;

            $newId = $this->webshop_m->save($data, null, true, $this->country_id);
            if ($newId != "") {
                $data['armstrong_2_webshop_id'] = $newId;
                $data['is_armstrong'] = 1;

                $this->update_webshop_details($orderId, $data);
                /* check first if enabled in country settings (after save)*/
                if($settings == 1){
                    $data['app_type'] = $app_type;
                    $avp = $this->get_avp_values($data);
                }
            }
        } else {
            $webshop_id = $data['armstrong_2_webshop_id'];
            unset($data['product'], $data['total'], $data['order_date'], $data['armstrong_2_webshop_id']);
            // update webshop
            $result = $this->webshop_m->updateRecord(null, $data, array('armstrong_2_webshop_id' => $webshop_id));

            $salesperson = $this->webshop_m->getRecord('salespersons','CONCAT(first_name," ",last_name) AS salesperson_name', array('armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id']));
            $data['armstrong_2_salespersons_name'] = ($data['armstrong_2_salespersons_id'] != "" ? $salesperson[0]->salesperson_name : "");

            $customer = $this->webshop_m->getRecord('customers','armstrong_2_customers_name',array('armstrong_2_customers_id' => $data['armstrong_2_customers_id']));
            $data['armstrong_2_customers_name'] = ($data['armstrong_2_customers_id'] != "" ? $customer[0]->armstrong_2_customers_name : "");

            $wholesaler = $this->webshop_m->getRecord('wholesalers','armstrong_2_wholesalers_id, name', array('armstrong_2_wholesalers_id' => $data['armstrong_2_wholesalers_id']));
            $data['armstrong_2_wholesalers_name'] = ($data['armstrong_2_wholesalers_id'] != "" ? $wholesaler[0]->name : "");

            $distributor = $this->webshop_m->getRecord('distributors','armstrong_2_distributors_id, name', array('armstrong_2_distributors_id' => $data['armstrong_2_distributors_id']));
            $data['armstrong_2_distributors_name'] = ($data['armstrong_2_distributors_id'] != "" ? $distributor[0]->name : "");
            // update details
            $details = $this->webshop_m->updateRecord('webshop_details', $data, array('armstrong_2_webshop_id' => $webshop_id));
        }
    }

    public function update_webshop_details($orderId, $data){
        $now = date('Y-m-d h:i:s');
        unset( $data['webshop_order_id'],
            $data['products'],
            $data['total'],
            $data['orderDate'],
            $data['date_created']);

        /* update table with Armstrong related details */
        $data['distributors_group_id'] = "";
        $data['distributors_group_name'] = "";
        $data['channel_id'] = "";
        $data['channel_name'] = "";
        $data['customers_types_id'] = "";
        $data['customers_types_name'] = "";
        $data['business_type_id'] = "";
        $data['business_type_name'] = "";
        $data['global_channels_id'] = "";
        $data['global_channels_name'] = "";

        $salesperson = $this->webshop_m->getRecord('salespersons','CONCAT(first_name," ",last_name) AS salesperson_name', array('armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id']));
        $data['armstrong_2_salespersons_name'] = ($data['armstrong_2_salespersons_id'] != "" ? $salesperson[0]->salesperson_name : "");

        $customer = $this->webshop_m->getRecord('customers','armstrong_2_customers_name',array('armstrong_2_customers_id' => $data['armstrong_2_customers_id']));
        $data['armstrong_2_customers_name'] = ($data['armstrong_2_customers_id'] != "" ? $customer[0]->armstrong_2_customers_name : "");

        $wholesaler = $this->webshop_m->getRecord('wholesalers','armstrong_2_wholesalers_id, name', array('armstrong_2_wholesalers_id' => $data['armstrong_2_wholesalers_id']));
        $data['armstrong_2_wholesalers_name'] = ($data['armstrong_2_wholesalers_id'] != "" ? $wholesaler[0]->name : "");
        /* PENDING : call records */
        if($data['armstrong_2_distributors_id'] != ""){
            $dis_req_field = "distributors.armstrong_2_distributors_id, distributors.`name`, distributors.`groups_id`, groups.`name` AS group_name";
            $where = array('distributors.armstrong_2_distributors_id' => $data['armstrong_2_distributors_id']);
            $distributor = $this->distributors_m->getDistributorDetails($dis_req_field, 'groups', 'groups_id = groups.id', 'LEFT', $where);
            $data['armstrong_2_distributors_name'] = $distributor[0]->name;
            $data['distributors_group_id'] = $distributor[0]->groups_id;
            $data['distributors_group_name'] = $distributor[0]->group_name;
        }

        if($data['armstrong_2_customers_id'] != ""){
            $customers = $this->customers_m->getCustomerDetails($data['armstrong_2_customers_id']);
            $data['channel_id'] = $customers[0]->channel;
            $data['channel_name'] = $customers[0]->channel_name;
            $data['customers_types_id'] = $customers[0]->customers_types_id;
            $data['customers_types_name'] = $customers[0]->customers_type_name;
            $data['business_type_id'] = $customers[0]->business_type;
            $data['business_type_name'] = $customers[0]->business_type_name;
            $data['global_channels_id'] = $customers[0]->global_channels_id;
            $data['global_channels_name'] = $customers[0]->global_channels_name;
        }

        $data['is_tagged'] = '1';
        $data['last_updated'] = $now;
        $where = array("order_id" => $orderId);

        return $this->webshop_m->updateRecord('webshop_details', $data, $where);
    }

    function get_avp_values($data){
        /* get salesperson info */
        $salesperson = $this->webshop_m->getRecord('salespersons','CONCAT(first_name," ",last_name) as name', array('armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id']));
        $data['armstrong_2_salespersons_name'] = $salesperson[0]->name;
        /* get customer info */
        $customer = $this->customers_m->getCustomerDetails($data['armstrong_2_customers_id']);
        $data['armstrong_2_customers_name'] = $customer[0]->armstrong_2_customers_name;
        $data['customer_chanel_id'] = $customer[0]->channel;
        $data['customer_chanel_name'] = $customer[0]->channel_name;

        /* get if order and channel is within range of AVP */
        $product = json_decode($data['products']);
        $where = array(
            'country_id' => $this->country_id,
            '("'.$data['order_date'].'") BETWEEN from_date AND to_date' => null,
            '(channel LIKE "%'.$data['customer_chanel_id'].'%" OR channel = "ALL")' => null);
        $sku_point = $this->webshop_m->getRecord('sku_point','*', $where);
        $data['armstrong_2_tfo_id'] = $data['armstrong_2_webshop_id'];
        unset(
            $data['armstrong_2_webshop_id'],
            $data['armstrong_2_wholesalers_id'],
            $data['armstrong_2_distributors_id'],
            $data['webshop_order_id'],
            $data['order_date'],
            $data['products'],
            $data['total']
        );

        /* check if channels covers AVP */
        foreach($product as $prod){
            $sku_num = $prod->sku_number;

            if($sku_point){
                foreach($sku_point as $sku_pt){
                    $data['sku_point_id'] = $sku_pt->id;
                    $points = json_decode($sku_pt->point_settings);
                    foreach($points as $pts){
                        if($pts->sku == $sku_num){
                            $data['sku_number'] = $sku_num;
                            $data['total_point'] = $pts->point;
                            $this->webshop_m->addNewRecord('tfo_point', $data);
                        }
                    }
                }
            }
        }
    }

    public function edit($id){
        if (!$this->hasPermission('edit', 'webshop') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $wb_order = $this->webshop_m->getRecord('webshop','*', array('armstrong_2_webshop_id' => $id));

        $this->data += $this->_getAddEditParams();

        $data = get_object_vars($wb_order[0]);

        foreach($data as $key => $val){
            if($key == 'products'){
                $products = json_decode($data[$key]);
                foreach ($products as $v){
                    $sku = $this->webshop_m->getRecord('products','sku_name',array('sku_number' => $v->sku_number));
                    $v->sku_name = $sku[0]->sku_name;
                }
                $this->data['products'] = $products;
            }else{
                $this->data[$key] = $val;
            }
        }

        /*foreach($wb_order as $val){
            debug::printArr(get_object_vars($val));
            $this->data['armstrong_2_webshop_id'] = $val->armstrong_2_webshop_id;
            $this->data['armstrong_2_salespersons_id'] = $val->armstrong_2_salespersons_id;
            $this->data['armstrong_2_customers_id'] = $val->armstrong_2_customers_id;
            $this->data['armstrong_2_customers_id'] = $val->armstrong_2_customers_id;
            $products = json_decode($val->products);
            foreach ($products as $v){
                $sku = $this->webshop_m->getRecord('products','sku_name',array('sku_number' => $v->sku_number));
                $v->sku_name = $sku[0]->sku_name;
            }
            $this->data['products'] = $products;
            $this->data['total_order'] = $val->total;
            $this->data['order_date'] = $val->order_date;
        }*/
        $this->render('ami/webshop/edit', $this->data);
    }

    public function view($order_id){
        $as_total_order = 0;
        /* get webshop details */
        $where = array("country_id" => $this->country_id, "order_id" => $order_id);
        $result = $this->webshop_m->getRecord($this->dtl_table_name, '*', $where);

        $this->data['order_id'] = $order_id;
        $this->data['armstrong_1_customers_id'] = $result[0]->clientNumber;
        $this->data['armstrong_2_customers_name'] = $result[0]->businessName;
        $this->data['armstrong_2_salespersons_id'] = $result[0]->armstrong_2_salespersons_id;
        $this->data += $this->_getAddEditParams();

        /* get potential customer data with salesperson */
        $id = ($result[0]->ufsClientNumber != "" ? $result[0]->ufsClientNumber : $result[0]->armstrong_1_customers_id);
        $customer = $this->webshop_m->getRecord($this->ptl_table_name,
                'armstrong_1_customers_id, armstrong_2_customers_name, armstrong_2_salespersons_id',
                array('armstrong_1_customers_id' => $id));
        if(count($customer) > 0){
            $this->data['armstrong_1_customers_id'] = $customer[0]->armstrong_1_customers_id;
            $this->data['armstrong_2_customers_name'] = $customer[0]->armstrong_2_customers_name;
            $this->data['armstrong_2_salespersons_id'] = $customer[0]->armstrong_2_salespersons_id;
        }

        foreach($result as $key => $val) {
            $as_total_order += $val->total;

            $this->data['products'][$key] = array(
                'id' => $val->id,
                'product_number' => $val->productNumber,
                'product_name' => $val->productName,
                'product_quantity' => $val->productQuantity,
                'product_package_type' => ($val->packagingType == 'DU' ? "Case" : "Pcs"),
                'product_price' => $val->productPrice,
                'product_total_price' => $val->totalProductPrice,
                /* product converted to armstrong values */
                'armstrong_qty_case' => $val->qty_case,
                'armstrong_qty_pcs' => $val->qty_piece,
                'armstrong_qty_per_case' => $val->quantity_case,
                'armstrong_product_price' => round($val->price, 2),
                'armstrong_total_price' => round($val->total, 2)
            );
        }

        $this->data['total_order'] = round($as_total_order, 2);
        $this->data['order_date'] = $result[0]->order_date;

        $this->render('ami/webshop/view', $this->data);
    }

    public function update_untag()
    {
        $formdata = $_POST;

        if (isset($formdata['potential'])) {
            /* first: save customer record */
            $customer = $formdata['potential'];

            /* check if existing */
            $exist = $this->webshop_m->getRecord($this->ptl_table_name,'*', array('armstrong_1_customers_id' => $customer['armstrong_1_customers_id']));
            if(!$exist){
                /* get data from order */
                $req_fields = "id, order_id, clientNumber, businessName, CONCAT(firstName,\" \",lastName) AS contact_name, mobilePhone, email, CONCAT(billingStreetName,\" \",billingHouseNumber,\", \", billingCity) AS street_address, billingZipCode ";
                $where = array("order_id" => $formdata['webshop_order_id']);
                $result = $this->webshop_m->getRecord('webshop_details', $req_fields, $where, 'order_id');

                $other_data = array(
                    'armstrong_2_salespersons_id' => $formdata['armstrong_2_salespersons_id'],
                    'street_address' => $result[0]->street_address,
                    'postal_code' => $result[0]->billingZipCode,
                    'phone' => $result[0]->mobilePhone,
                    'email' => $result[0]->email,
                    'country_id' => $this->country_id,
                    'active' => 1,
                    'approved' => 1);
                $pc_data = array_merge($customer, $other_data);

                $save_record = $this->webshop_m->addNewRecord($this->ptl_table_name, $pc_data);
                if($save_record){
                    /* add potential contacts */
                    $contacts = array(
                        'armstrong_1_customers_id' => $customer['armstrong_1_customers_id'],
                        'name' => $result[0]->contact_name,
                        'phone' => $result[0]->mobilePhone,
                        'email' => $result[0]->email,
                        'country_id' => $this->country_id
                    );
                    $result_pc = $this->webshop_m->addNewRecord('potential_contacts', $contacts);
                }
            }

            foreach ($customer as $k => $v) {
                $formdata[$k] = $v;
            }

            /* update webshop detail order */
            $webshop_id = $formdata['webshop_order_id'];
            unset(
                $formdata['potential'],
                $formdata['armstrong'],
                $formdata['webshop_order_id'],
                $formdata['total'],
                $formdata['order_date'],
                $formdata['webshop']
            );

            $salesperson = $this->webshop_m->getRecord('salespersons', 'CONCAT(first_name," ",last_name) AS salesperson_name', array('armstrong_2_salespersons_id' => $formdata['armstrong_2_salespersons_id']));
            $formdata['armstrong_2_salespersons_name'] = ($formdata['armstrong_2_salespersons_id'] != "" ? $salesperson[0]->salesperson_name : "");
            $update = $this->webshop_m->updateRecord($this->dtl_table_name, $formdata, array('order_id' => $webshop_id));
        } else {
            /* save details and webshop */
            $this->save($formdata);
        }
        return redirect('ami/webshop');
    }

    public function download()
    {
        $data = $_POST;
        $url = $this->config->item('base_api_url') . 'v4/restapi/webshop/sync_to_armstrong/format/json/sa/PULL-Dev3.6.4-20170907';

        $data_string = json_encode($data);
        // sync_to_armstrong_post
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_PORT => 80,
//            CURLOPT_HTTPGET => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data_string,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/json',
                'Content-Length:' . strlen($data_string)
            ),
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_ENCODING => "",
            CURLOPT_AUTOREFERER => TRUE,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => 2
//            CURLOPT_USERPWD        => "431315046:6a0dca2a-290f-47ab-903d-a50c307a3cc2"
        );

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo "Curl error: " . curl_error($ch);
            exit;
        }

        if ($httpCode != 200) {
            /*return array(
                "httpCode" => $httpCode,
                "message" => "Return code is {$httpCode} : ". $response['message']);*/
            echo json_decode($response);
            exit;
        }
        echo $response;
        curl_close($ch);
    }

    function load_checker(){
        $data = $_POST;
        $url = $this->config->item('base_api_url').'v4/restapi/webshop/process_checker/format/json/sa/PULL-Dev3.6.4-20170907';

        $data_string = json_encode($data);
        // sync_to_armstrong_post
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_PORT => 80,
//            CURLOPT_HTTPGET => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data_string,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/json',
                'Content-Length:'.strlen($data_string)
            ),
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_ENCODING => "",
            CURLOPT_AUTOREFERER => TRUE,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => 2
//            CURLOPT_USERPWD        => "431315046:6a0dca2a-290f-47ab-903d-a50c307a3cc2"
        );

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo "Curl error: " . curl_error($ch);
            exit;
        }

        if ($httpCode != 200) {
            // echo json_decode($response);
            echo $response;
            exit;
        }

        curl_close($ch);
        echo $response;
    }

    protected function _getAddEditParams()
    {
        // default conditions
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata('user_data');
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, false, null, array('admins_id', 'armstrong_2_salespersons_id'));
        // check type role of user
        if ($user['roles_id'] == $roles_salesmanager) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id'], 'active' => 1);
        } else if ($user['roles_id'] == $roles_salesperson) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id'], 'active' => 1);
        }
//        $conditions_sales['type'] = 'PULL';
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);

        return array(
            'customers' => $this->customers_m->getCustomerListOptions(null, $conditions),
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true),
            'distributors' => $this->distributors_m->getDistributorListOptions(null, array_merge(array('active' => 1), $conditions)),
            'wholesalers' => $this->wholesalers_m->getSalerListOptions(null, array_merge(array('active' => 1), $conditions))
            /*'wholesalers' => $this->wholesalers_m->getSalerListOptions(null, array_merge(array('active' => 1), $conditions)),
            'call_records' => $this->call_records_m->parse_form(array('armstrong_2_call_records_id', 'armstrong_2_call_records_id'), '- Select -', $conditions),
            'distributors' => $this->distributors_m->getDistributorListOptions(null, array_merge(array('active' => 1), $conditions)),*/
            /*'ssd_method' => $this->ssd_m->getMethodListOptions(),
            'products' => array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'),*/
        );
    }

    public function delete($order_id = NULL)
    {
        if (!$this->hasPermission('delete', 'webshop')) {
            return $this->noPermission();
        }

        $this->_assertId($order_id, 'ami/webshop');
        /* get all records under orderID */

        if ($order_id) {
            if ($this->is('POST', FALSE)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/webshop';

                // compare id type
                $code_id = substr($order_id, 0, 3);
                $new_data['is_draft'] = 1;
                $new_data['last_updated'] = get_date();

                if ($code_id == "WBS") {
                    $data = $this->webshop_m->getRecord('webshop', 'id', array('armstrong_2_webshop_id' => $order_id));
                    foreach($data as $v){
                        $id = $v->id;
                        $old_rec = $this->webshop_m->save($new_data, $id, false, 'DELETE');
                    }
                }

                $data = $this->webshop_m->getRecord('webshop_details', 'id', array('order_id' => $order_id));
                foreach ($data as $value) {
                    $id = $value->id;
                    $this->webshop_details_m->save(array(
                        'is_draft' => 1,
                        'last_updated' => get_date()),
                        $id, false, 'DELETE');
                }

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/webshop/delete/{$order_id}"),
                    'message' => "Deleting entry with Order ID: {$order_id} from Webshop"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/webshop');
    }
}