<?php 

/**
 *
 * @author 	Jan Sarmiento
 * 			jan.sarmiento@opsolutions.biz
 *
 * Controller for Global SSD Module
 *
 * ------------------------------------------
 * 		   RULES FOR SAVING MAPPINGS
 * ------------------------------------------
 *
 * CUST: + $customer_id
 * WHS: + $wholesalers_id
 * DT: + $distributors_id
 * 
 * ------------------------------------------
 * 
 */
class Global_ssd extends AMI_Controller {

	private $viewFolder;
	private $viewMapping;
	private $activeUser;
	private $ssdCols;
	private $amiValidIP;
	public $data;

	private $MAP_ENTITY_COLUMNS;

	public function __construct() {	
        parent::__construct();

        // Load Models
        $this->load->model('distributors_m');
        $this->load->model('global_ssd_map_entities_m');
        $this->load->model('global_ssd_mappings_m');
        $this->load->model('global_ssd_m');
        $this->load->model('wholesalers_m');
        $this->load->model('customers_m');

        // Load libraries
        $this->load->library(['session']);

        // Initialize
        $this->viewFolder = 'ami/global_ssd/'; // view folder for global ssd
        $this->viewMapping = [ // map collection view file per function
        	// fn name 	=> view file name
			'index'					=>	$this->viewFolder . 'viewAllData',
			'mappingTable'			=>	$this->viewFolder . 'mappingTable',
			'modDraftMapping'		=>	$this->viewFolder . 'newMappingTable',
			'viewAllMapDTandWHS'	=>	$this->viewFolder . 'viewAllMapDTandWHS',
			'bulkUploadMap'			=>	$this->viewFolder . 'bulkUploadMap',
			'viewAllMap'			=>	$this->viewFolder . 'masterViewAllMap',
			'masterView'			=>	$this->viewFolder . 'masterViewSSD',
			'viewAcknowledged'		=>	$this->viewFolder . 'viewAcknowledged'
		];

		$this->MAP_ENTITY_COLUMNS = [
			"COMBINED_CUST_GROUPING"	=> [
				"DISTRIBUTOR"			=>	"DISTRIBUTOR",
				"CUSTOMERNAME"			=>	"CUSTOMERNAME",
				"CUSTOMER CODE"			=>	"CUSTOMER_CODE",
				"CUSTOMER_CODE"			=>	"CUSTOMER_CODE",
				"CUSTOMER NAME"			=>	"CUSTOMER_NAME",
				"CUSTOMER_NAME"			=>	"CUSTOMER_NAME",
				"GROUPING CUSTOMER"		=>	"GROUPING_CUSTOMER",
				"GROUPING_CUSTOMER"		=>	"GROUPING_CUSTOMER",
				"TEAM"					=>	"TEAM",
				"SALESMAN"				=>	"SALESMAN",
				"CHANNEL"				=>	"CHANNEL",
				"SALESMAN CODE"			=>	"SALESMAN_CODE",
				"SALESMAN_CODE"			=>	"SALESMAN_CODE",
				"WHOLESALE SUB CHANNEL"	=>	"WHOLESALE_SUB_CHANNEL",
				"WHOLESALE_SUB_CHANNEL"	=>	"WHOLESALE_SUB_CHANNEL",
				"GOLD PLUS"				=>	"GOLD_PLUS",
				"GOLD_PLUS"				=>	"GOLD_PLUS",
				"CPU"					=>	"CPU",
				"OEM"					=>	"OEM",
				"ARMSTRONG_2_CUSTOMERS_ID"  =>  "ARMSTRONG_2_CUSTOMERS_ID"
			],
			"COMBINED_PDT_GROUPING"		=> [
				"DISTRIBUTOR"			=>	"DISTRIBUTOR",
				"LSH PRODUCT NAME"		=>	"LSH_PRODUCT_NAME",
				"LSH_PRODUCT_NAME"		=>	"LSH_PRODUCT_NAME",
				"ITEM CODE"				=>	"ITEM_CODE",
				"ITEM_CODE"				=>	"ITEM_CODE",
				"PRODUCT DESCRIPTION"	=>	"PRODUCT_DESCRIPTION",
				"PRODUCT_DESCRIPTION"	=>	"PRODUCT_DESCRIPTION",
				"SAP CODE"				=>	"SAP_CODE",
				"SAP_CODE"				=>	"SAP_CODE",
				"PRODUCT GROUPING"		=>	"PRODUCT_GROUPING",
				"PRODUCT_GROUPING"		=>	"PRODUCT_GROUPING",
				"CATEGORY"				=>	"CATEGORY",
				"MARKET"				=>	"MARKET",
				"HURRICANE 10"			=>	"HURRICANE_10",
				"HURRICANE_10"			=>	"HURRICANE_10",
				"TOP SKUS"				=>	"TOP_SKUS",
				"TOP_SKUS"				=>	"TOP_SKUS",
				"BRAND"					=>	"BRAND",
				"UOM"					=>	"UOM",
				"ARMSTRONG_2_CUSTOMERS_ID"  =>  "ARMSTRONG_2_CUSTOMERS_ID"
			]
		];
		$this->MAP_ENTITY_COLUMNS["PRODUCT_MAPPING"] = $this->MAP_ENTITY_COLUMNS["COMBINED_PDT_GROUPING"];
		$this->MAP_ENTITY_COLUMNS["PRODUCT MAPPING"] = $this->MAP_ENTITY_COLUMNS["COMBINED_PDT_GROUPING"];
		$this->MAP_ENTITY_COLUMNS["CUSTOMER_MAPPING"] = $this->MAP_ENTITY_COLUMNS["COMBINED_CUST_GROUPING"];
		$this->MAP_ENTITY_COLUMNS["CUSTOMER MAPPING"] = $this->MAP_ENTITY_COLUMNS["COMBINED_CUST_GROUPING"];

		$this->ssdCols = [
			"ID",
			"DISTRIBUTOR",
			"DT_CUSTOMER_CODE",
			"DT_CUSTOMER_NAME",
			"CUSTOMERS",
			"GROUPING_CUSTOMER",
			"TEAM",
			"SALESMAN",
			"CHANNEL",
			"SALESMAN_CODE",
			"WHOLESALE_SUB_CHANNEL",
			"GOLD_PLUS",
			"CPU",
			"OEM",
			"DT_PRODUCT_CODE",
			"DT_PRODUCT_NAME",
			"PRODUCT",
			"PRODUCT_CODE",
			"PRODUCT_GROUPING",
			"CATEGORY",
			"MARKET",
			"HURRICANE_10",
			"TOP_SKUS",
			"BRAND",
			"YEAR",
			"MONTH",
			"DATE_OF_TRANSACTION",
			"SUM_OF_TOTAL_PURCHASE_PRODUCT",
			"SUM_OF_TOTAL_QTY_PURCHASE",
			"UOM"
		];

		$this->amiValidIP = [
			"::1"
		];
	}

	public function index() {
		$this->data["ssdCols"] = $this->ssdCols;
		$this->data['page_title']	= 'Global SSD - View reports';
		return $this->render($this->viewMapping['index'], $this->data);
	}

	public function entrySSDDataShowAll($toJson = true) {
		$ack = (@$this->input->get('a') && $this->input->get('a') == "true") ? "TRUE" : "FALSE";

		if( $this->isSuperAdmin() ) {
			if(@$this->input->get("f") && @$this->input->get("t")) {
				$res = $this->global_ssd_m->getSSDbetweenSA(
					$this->input->get("f"),
					$this->input->get("t"),
					$ack
				);
			} else {
				if( $ack == "TRUE" ) {
					$res = $this->global_ssd_m->get();
				} else {
					$res = $this->global_ssd_m->get_by(
						["acknowledged" => 0], false
					);
				}
			}
		} else {
			if(@$this->input->get("f") && @$this->input->get("t")) {
				$res = $this->global_ssd_m->getSSDbySalespersonBetweenSA(
					$this->getActiveUserId(),
					[ "from" => $this->input->get("f"), "to" => $this->input->get("t") ],
					$ack
				);
			} else {
				$res = $this->global_ssd_m->getSSDbySalespersonSA($this->getActiveUserId(), $ack);
			}
		}

		if( $toJson ){ echo json_encode($res); } else { return $res; }
	}

	public function entrySSDData($toJson = true) {
		$ack = (@$this->input->get('a') && $this->input->get('a') == "true") ? "TRUE" : "FALSE";

		if( $this->isSuperAdmin() ) {
			if(@$this->input->get("f") && @$this->input->get("t")) {
				$res = $this->global_ssd_m->getSSDbetween(
					$this->input->get("f"),
					$this->input->get("t"),
					$ack
				);
			} else {
				$res = $this->global_ssd_m->get_by(
					["acknowledged" => ($ack == "TRUE" ? 1 : 0)], false
				);
			}
		} else {
			if(@$this->input->get("f") && @$this->input->get("t")) {
				$res = $this->global_ssd_m->getSSDbySalespersonBetween(
					$this->getActiveUserId(),
					[ "from" => $this->input->get("f"), "to" => $this->input->get("t") ],
					$ack
				);
			} else {
				$res = $this->global_ssd_m->getSSDbySalesperson($this->getActiveUserId(), $ack);
			}
		}

		if( $toJson ){ echo json_encode($res); } else { return $res; }
	}

	public function acknowledge() {
		$postData = $this->input->post();
		if (isset($postData['from']) && isset($postData['to'])) {
			$this->session->set_flashdata('message', [
				'from'  => $postData['from'],
				'to'    => $postData['to']
			]);
		}
		$this->global_ssd_m->update_in(["acknowledged" => true], 'id', $postData['ssdData']);
		return redirect("ami/global_ssd");
	}

	public function masterView() {
		$this->data["ssds"] = $this->global_ssd_m->get();
		$this->data['page_title']	= 'Global SSD - Master Table';
		return $this->render($this->viewMapping['masterView'], $this->data);
	}

	public function doAddNewMap() {
		$map = $this->global_ssd_mappings_m->get_by(
			["id" => $this->input->post('map_id')], true
		);

		if($map && $this->input->post('finalJson')) {
			$postData = $this->input->post();
			$data = [
				"value"	    =>  $postData["finalJson"],
				"is_draft"	=>	$postData["is_draft"]
			];

			$this->global_ssd_mappings_m->update_in($data, 'id', [$map["id"]] );
			$this->session->set_flashdata('message', 'Changes has been saved');
			$this->session->set_flashdata('flash_type', 'save_true');
		} else {
			$this->session->set_flashdata('message', 'Edit map failed.');
			$this->session->set_flashdata('flash_type', 'save_false');
		}
		return redirect("ami/global_ssd/modDraftMapping/" . $this->input->post('map_id'));
	}

//	public function doAddNewMap() {
//		$map = $this->global_ssd_mappings_m->get_by(
//			["id" => $this->input->post('map_id')], true
//		);
//
//		$mapEntity = $this->global_ssd_map_entities_m->get_by(
//			["id" => $map["entity_id"]], true
//		);
//
//		$columns = explode(':', $mapEntity["columns"]);
//
//		$completeRowData = [];
//
//		// transfer CI post data to local variable to be mutable
//		$postData = $this->input->post();
//
//		unset($postData["map_id"]);
//		$draft = $postData["is_draft"];
//		unset($postData["is_draft"]);
//		if(!empty($postData)) {
//			for ($i=0; $i < count(reset($postData)) ; $i++) {
//				$singleRow = [];
//				foreach ($columns as $key => $value) {
//					$colVal = $postData[strtolower($value)][$i];
//					$colVal = $colVal ? htmlspecialchars(trim(strip_tags($colVal))) : null;
//					$singleRow = array_merge($singleRow, [
//						$value => $colVal
//					]);
//				}
//				array_push($completeRowData, $singleRow);
//			}
//		}
//
//		if($completeRowData && count($completeRowData) > 0) {
//			$map["value"] = json_encode(array_merge(json_decode($map["value"], true), $completeRowData));
//		}
//
//		$data = [
//			"value"	    =>  $map["value"],
//			"is_draft"	=>	$draft
//		];
//
//		$this->global_ssd_mappings_m->update_in($data, 'id', [$map["id"]] );
//
//		return redirect("ami/global_ssd/modDraftMapping/" . $map["id"]);
//	}

	public function distributorsList($toJson = true) {
		$where = ["armstrong_2_salespersons_id" => $this->getActiveUserId()];
		$dists = $this->distributors_m->get_by(
			$where, false, "name",
			['id', 'name'],
			null
		);

		if ( $toJson ) { 
			echo json_encode($dists); 
		} else {
			return $dists;
		}
	}

	public function wholesalersList($toJson = true) {
		$where = ["armstrong_2_salespersons_id" => $this->getActiveUserId()];
		$whs = $this->wholesalers_m->get_by(
			$where, false, "name",
			['id', 'name'],
			null
		);

		if( $toJson ) {
			echo json_encode($whs);
		} else {
			return $whs;
		}
	}

	public function distAndWhsList( $toJson = true ) {

		$distAndWhs = [
			"distributors"	=>	$this->distributorsList(false),
			"wholesalers"	=>	$this->wholesalersList(false),
		];

		if( $toJson ) {
			echo json_encode($distAndWhs);
		} else {
			return $distAndWhs;
		}
	}

	public function customersList($toJson = true) {

		$where = ["armstrong_2_salespersons_id" => $this->getActiveUserId()];
		// $where = ["armstrong_2_salespersons_id" =>  'SALA0032TH'];
		$custs = $this->customers_m->get_by(
			$where, false, null,
			['id', 'armstrong_2_customers_name as name'],
			null
		);

		if( $toJson ) {
			echo json_encode($custs);
		} else {
			return $custs;
		}
	}

	public function mappingTable() {
		$this->data['page_title']	= 'Global SSD - Mapping Table'; // set page title
		$this->data['entities']	= $this->global_ssd_map_entities_m->get();
		$this->data['user'] = $this->session->userdata('user_data');
		$this->data["is_admin"]	= $this->isSuperAdmin();


		return $this->render($this->viewMapping['mappingTable'], $this->data);
	}

	public function addNewMappingDraft() {

		$data = [
			'armstrong_2_salesperson_id'	=>	$this->getActiveUserId(),
			'entity_id'						=>	$this->input->post('map_entity_id'),
			'armstrong_fk_id'				=>	$this->input->post('role_entity_id')
		];

		$map = $this->ifMapExists($data);

		if ( $map == null ) {
			$map = $this->global_ssd_mappings_m->save($data);
		}

		return redirect('ami/global_ssd/modDraftMapping/'.$map);
	}

	public function modDraftMapping( $mapId, $mapType = false ) {
		if( @$mapType ) {
			if(stripos($mapId, ":") !== false) {
				$mapWhereClause = ['armstrong_fk_id' => $mapId, 'entity_id' => $mapType];
			} else {
				$mapWhereClause = ['id' => $mapId];
			}
		} else {
			$mapWhereClause = ['id' => $mapId];
		}

		$map = $this->global_ssd_mappings_m->get_by(
			$mapWhereClause,
			true,
			null
		);

		$this->data['page_title'] = "Global SSD - Modify Mapping Table";
		$this->data['title'] = "Edit Map";

		if($map) {
			$map["rawvalue"] = $map["value"];
			$map["value"] = json_decode($map["value"], true);

			$mapEntity = $this->global_ssd_map_entities_m->get_by(
				['id' => $map["entity_id"]],
				true,
				null,
				['columns', 'name']
			);

			if($mapEntity["name"] == "COMBINED_PDT_GROUPING") {
				$mapEntity["name"] = "Product Mapping";
			} else if ( $mapEntity["name"] == "COMBINED_CUST_GROUPING" ) {
				$mapEntity["name"] = "Customer Mapping";
			}

			$this->data['columns'] = explode(':', $mapEntity["columns"]);
			$this->data['map'] = $map;
			$this->data['mapType'] = $mapEntity["name"];
			$this->data['entityRef'] = $this->extractMapEntityReference($map["armstrong_fk_id"]);
		}

		return $this->render($this->viewMapping['modDraftMapping'], $this->data);	
	}

	public function viewAllMapDTandWHS() {
		$this->data["allMaps"] = [
			'distributors'	=>	$this->getMapDistributor(),
			'wholesalers'	=>	$this->getMapWholesaler()
		];

		return $this->render($this->viewMapping['viewAllMapDTandWHS'], $this->data);	
	}

	public function getMap( $id, $entityType = null ) {
		$dbColumn = strpos(':', $id) >= 0 ? 'armstrong_fk_id' : 'id';
		$map = [];

		if ( $id ) {
			if ( strpos(':', $id) >= 0 ) {
				$where = [
					"a.armstrong_fk_id"				=> $id,
					"a.entity_id"					=> $entityType
				];
				switch (explode(':', $id)[0]) {
					case 'DT':
						$map = $this->getMapDistributor($where);
						break;
					case 'WHS':
						$map = $this->getMapWholesaler($where);
						break;
					case 'CUST':
						$map = $this->getMapCustomer($where);
						break;
				}
			} else {
				// TODO Incase of plain global_ssd_id
			}

		}

		echo json_encode($map);
	}

	public function getDTandWHSwithMap( $toJson = true ) {
		$dtAndWhs = [
			"distributors"	=> $this->getDistributorsWithMap(false),
			"wholesalers"	=>	$this->getWholesalersWithMap(false)
		];

		if ( $toJson ) { 
			echo json_encode($dtAndWhs); 
		} else {
			return $dtAndWhs;
		}
	}

	public function getDTandWHSwithoutMap( $toJson = true ) {
		$dtAndWhs = [
			"distributors"	=> $this->getDistributorsWithoutMap(false),
			"wholesalers"	=>	$this->getWholesalersWithoutMap(false)
		];

		if ( $toJson ) { 
			echo json_encode($dtAndWhs); 
		} else {
			return $dtAndWhs;
		}
	}

	public function getDistributorsWithMap( $toJson = true ) {
		$dists = $this->global_ssd_mappings_m->getDistributorsWithMap($this->getActiveUserId());

		if ( $toJson ) { 
			echo json_encode($dists); 
		} else {
			return $dists;
		}
	}

	public function getDistributorsWithoutMap( $toJson = true ) {
		$dists = $this->global_ssd_mappings_m->getDistributorsWithoutMap($this->getActiveUserId());

		if ( $toJson ) { 
			echo json_encode($dists); 
		} else {
			return $dists;
		}
	}

	public function getWholesalersWithMap( $toJson = true ) {
		$whs = $this->global_ssd_mappings_m->getWholesalersWithMap($this->getActiveUserId());

		if ( $toJson ) { 
			echo json_encode($whs); 
		} else {
			return $whs;
		}
	}

	public function getWholesalersWithoutMap( $toJson = true ) {
		$whs = $this->global_ssd_mappings_m->getWholesalersWithoutMap($this->getActiveUserId());

		if ( $toJson ) { 
			echo json_encode($whs); 
		} else {
			return $whs;
		}
	}

	public function getCustomersWithMap( $toJson = true ) {
		$cust = $this->global_ssd_mappings_m->getCustomersWithMap($this->getActiveUserId());

		if ( $toJson ) { 
			echo json_encode($cust); 
		} else {
			return $cust;
		}
	}

	public function bulkUploadMap( $mapId = false ) {
		if( $mapId ) {

			if($this->isSuperAdmin()) {
				$where = [
					"id"    => $mapId
				];
			} else {
				$where = [
					'id' => $mapId,
					'armstrong_2_salesperson_id'	=>	$this->getActiveUserId()
				];
			}

			$map = $this->global_ssd_mappings_m->get_by(
				$where,
				true,
				null,
				["id", "armstrong_fk_id"]
			);

			if ( $map ) {
				$this->data['map'] = $map;
				$this->data['page_title'] = "Mapping Bulk Upload";
				$this->data['entityRef'] = $this->extractMapEntityReference($map["armstrong_fk_id"]);
				return $this->render($this->viewMapping["bulkUploadMap"]);
			} else {
				die("NO MAP FOUND FOR MAP ID $mapId");
			}
		} else {
			die("NO MAP ID. ADMINISTRATOR ONLY");
		}
	}

	private function convertHeader($arr, $mapEntity) {
		return array_map(function($item) use ($mapEntity) {
			return $this->MAP_ENTITY_COLUMNS[$mapEntity][strtoupper(trim($item))];
		}, $arr);
		// return $this->MAP_ENTITY_COLUMNS[$mapEntity][strtoupper(trim($str))];
	}

	private function cleanse($str) {
		return htmlspecialchars(strip_tags(preg_replace('/\s\s+/', ' ', $str)));
//		return htmlspecialchars(strip_tags($str));
	}

	public function doBulkUploadMap() {

		$map = $this->global_ssd_mappings_m->get_by(
			['id' => $this->input->post('map_id')],
			true,
			null
		);

		if( @$map ) {
			
			$mapEntity = $this->global_ssd_map_entities_m->get_by(
				['id' => $map["entity_id"]], 
				false, 
				null,
				['columns', 'name'],
				null
			)[0];

			$columns = $mapEntity["columns"];
			$entityName = $mapEntity["name"];

			$columns = explode(":", $columns);

			$rows   = array_map('str_getcsv', str_getcsv($this->input->post('bulkCsvString'), PHP_EOL));
		    $header = $this->convertHeader(array_shift($rows), $entityName);

		    if ($header == $columns) {
			    $csv    = array();
			    foreach($rows as $row) {
			        $csv[] = array_combine($header, array_map([$this, 'cleanse'], $row));
			    }

				$map["value"] = ($map["value"] == null || $map["value"] == 'null') ? [] : json_decode($map["value"], true);
				$map["value"] = json_encode(array_merge($csv, $map["value"]));

				$this->global_ssd_mappings_m->update_in(['value' => $map["value"]], 'id', [$map["id"]] );
		    } else {
		    	die('Error : CSV headers invalid<br/>Headers must be in order of ' . implode(",", $columns));
		    }
		    die('Bulk upload success.');
		} else {
			die('No mapping found. Create map first');
		}

		/*
		$columns = $this->global_ssd_map_entities_m->get_by(
			['id' => $map["entity_id"]], 
			false, 
			null,
			['columns'],
			null
		)[0]["columns"];

		$csvString = str_getcsv($this->input->post('bulkCsvString'), PHP_EOL);
		$columns = str_getcsv($csvString[0]);

		dump($columns); die;

		foreach ($csvString as $key => $value) {
			
		}
		*/
	}

	public function getCustomersWithoutMap( $toJson = true ) {
		$cust = $this->global_ssd_mappings_m->getCustomersWithoutMap($this->getActiveUserId());
			
		if ( $toJson ) { 
			echo json_encode($cust); 
		} else {
			return $cust;
		}
	}

	private function getMapCustomer($where = []) {
		return $this->decodeJson($this->global_ssd_mappings_m->getMapCustomers(
			$this->getActiveUserId(), $where));
	}

	private function getMapDistributor($where = []) {
		return $this->decodeJson($this->global_ssd_mappings_m->getMapDistributors(
			$this->getActiveUserId(), $where));
	}

	private function getMapWholesaler($where = []) {
		return $this->decodeJson($this->global_ssd_mappings_m->getMapWholesalers(
			$this->getActiveUserId(), $where));
	}

	private function decodeJson( $list ) {
		foreach ($list as &$value) {
			$value["value"] = json_decode($value["value"], true);
		}
		return $list;
	}

	private function getActiveUserId() {
		$this->activeUser = $this->session->userdata('user_data');
		return $this->activeUser['id'];
	}

	private function extractMapEntityReference( $rawId ) {
		$rawId = explode(':', $rawId);
		$where = ['id'	=>	$rawId[1]];

		switch ($rawId[0]) {
			case 'DT':
				$table = "distributors";
				$dbData = $this->distributors_m->get_by( 
					$where, true, null, ["id", "armstrong_2_distributors_id as ufs_id", "name"] );
				break;
			case 'WHS':
				$table = "wholesalers";
				$dbData = $this->wholesalers_m->get_by( 
					$where, true, null, ["id", "armstrong_2_wholesalers_id as ufs_id", "name"] );
				break;
			default:
				$table = "customers";
				$dbData = $this->customers_m->get_by( 
					$where, true, null, ["id", "armstrong_2_customers_id as ufs_id", "armstrong_2_customers_name as name"]);
				break;
		}

		return [
			"table"		=>	$table,
			"dbData"	=>	$dbData,
			"type"		=>	$rawId[0]
		];
	}

	private function ifMapExists( $data ) {
		return @$this->global_ssd_mappings_m->get_by( $data, true )["id"];
	}

	public function getCPDTGmap($toJson = true) {
		$where = ["a.entity_id" => 1];
		$res = [
			"distributors" 	=>	$this->global_ssd_mappings_m->getMapDistributors($this->getActiveUserId(), $where),
			"customers" 	=>	$this->global_ssd_mappings_m->getMapCustomers($this->getActiveUserId(), $where),
			"wholesalers" 	=>	$this->global_ssd_mappings_m->getMapWholesalers($this->getActiveUserId(), $where),
		];

		if ( $toJson ) {
			echo json_encode($res);
		} else {
			return $res;
		}
	}

	public function getCCUSTGmap() {
		$where = ["a.entity_id" => 2];
		$res = [
			"distributors" 	=>	$this->global_ssd_mappings_m->getMapDistributors($this->getActiveUserId(), $where),
			"customers" 	=>	$this->global_ssd_mappings_m->getMapCustomers($this->getActiveUserId(), $where),
			"wholesalers" 	=>	$this->global_ssd_mappings_m->getMapWholesalers($this->getActiveUserId(), $where),
		];

		echo json_encode($res);
	}

	public function getCPDTGmapWithout($toJson = true) {
		$where = [
			"entityId"		=>	1,
			"salesPersonId"	=>	$this->getActiveUserId()
		];

		$res = [
			"distributors"	=>	$this->global_ssd_mappings_m->getDistributorsWithoutMap($where),
			"wholesalers"	=>	$this->global_ssd_mappings_m->getWholesalersWithoutMap($where),
			"customers"		=>	$this->global_ssd_mappings_m->getCustomersWithoutMap($where)
		];

		if( $toJson ) { echo json_encode($res); } else { return $res; }
	}

	public function getCCUSTGmapWithout($toJson = true) {

		$where = [
			"entityId"		=>	2,
			"salesPersonId"	=>	$this->getActiveUserId()
		];

		$res = [
			"distributors"	=>	$this->global_ssd_mappings_m->getDistributorsWithoutMap($where),
			"wholesalers"	=>	$this->global_ssd_mappings_m->getWholesalersWithoutMap($where),
			"customers"		=>	$this->global_ssd_mappings_m->getCustomersWithoutMap($where)
		];

		if( $toJson ) { echo json_encode($res); } else { return $res; }
	}

	public function viewAllMap($mapEntityId = false, $mode = 'INDI') {
		if( $mapEntityId ) {

			$entity = $this->global_ssd_map_entities_m->get_by(
				["id" => $mapEntityId], true
			);

			$this->data["mode"]	= $mode;

			$where = [
				"entity_id"	=>	$mapEntityId
			];

			if( $mode == 'WHLE' ) {
				$this->data["title"] = "Mapping Table - Whole <small>".$entity["name"]."</small>";
				$this->data["allMaps"] = array_merge(
					$this->getMapDistributor($where), 
					$this->getMapWholesaler($where),
					$this->getMapCustomer($where)
				);
				$this->data["columns"] = explode(":", $entity["columns"]);
			} else {
				$this->data["title"] = "Mapping Table - Individual <small>PRODUCT MAPPING</small>";
				$this->data["allMaps"] = [
					'distributors'	=>	$this->getMapDistributor($where),
					'wholesalers'	=>	$this->getMapWholesaler($where),
					'customers'		=>	$this->getMapCustomer($where),
				];
			}

			return $this->render($this->viewMapping['viewAllMap'], $this->data);
		} else {
			return redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function generateMapCsv() {
		$type = @$this->input->get("type");

		if( $type == "CUST" || $type == "COMBINED_CUST_GROUPING" || $type == "CUSTOMER_MAPPING") {
			$mapName = "CUSTOMER_MAPPING";
			$entityMapWhere = ["name"	=>	"COMBINED_CUST_GROUPING"];
		} else if ( $type == "PDT" || $type == "COMBINED_PDT_GROUPING" || $type == "PRODUCT_MAPPING") {
			$mapName = "PRODUCT_MAPPING";
			$entityMapWhere = ["name"	=>	"COMBINED_PDT_GROUPING"];
		} else {
			$mapName = false;
			$entityMapWhere = false;
		}

		if ( $mapName ) {
			$entityMap = $this->global_ssd_map_entities_m->get_by(
				$entityMapWhere, true
			);

			if ( $entityMap["id"] ) {
				$maps = $this->global_ssd_mappings_m->get_by(
					["entity_id" => $entityMap["id"]], false, "value", ["value"]
				);
			} else {
				$maps = [];
			}

			$allMaps = [];
			foreach ($maps as $key => $value) {
				$allMaps = array_merge($allMaps, json_decode($value["value"], true));
			}

			$rawColumns = explode(":", $entityMap["columns"]);
			$columns = implode(",", array_map('htmlspecialchars_decode', $rawColumns));

			$filename = "{$mapName}.csv";
			header('Content-Type: application/excel');
			header('Content-Disposition: attachment; filename=" '. $filename .'"');

			$fp = fopen('php://output', 'w');
		    fputs($fp, $columns . PHP_EOL); // add columns

			foreach ( $allMaps as $value ) {
			    fputs($fp, implode(",", array_map('htmlspecialchars_decode', $value)).PHP_EOL);
			}
			fclose($fp);
		}

		/*
		if( $id ) {
			$map = $this->global_ssd_mappings_m->get_by(
				["id" => $id], true
			);
			if ( $map ) {
				$rawColumns = $this->global_ssd_map_entities_m->get_by(
					["id" => $map["entity_id"]], true
				)["columns"];
				$rawColumns = explode(":", $rawColumns);

				$columns = implode(",", $rawColumns);
				$data = json_decode($map["value"], true);


				$filename = "MAP_ID_{$id}_[" . date("Y-m-d_H-i-s", time()). "].csv";
				header('Content-Type: application/excel');
				header('Content-Disposition: attachment; filename=" '. $filename .'"');

				$fp = fopen('php://output', 'w');
			    fputs($fp, implode($rawColumns, ',').PHP_EOL); // add columns

				foreach ( $data as $value ) {
				    fputs($fp, implode(",", $value).PHP_EOL);
				}
				fclose($fp);
			}
		}
		*/
	}

	public function viewAcknowledged() {
		$this->data["title"] = "Global SSD <small>Acknowledged Data</small>";
		return $this->render($this->viewMapping["viewAcknowledged"], $this->data);
	}
}