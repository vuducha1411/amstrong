<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Organization extends AMI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('salespersons_m');
        $this->load->model('roles_m');
    }

    public function index()
    {
        $conditions = array('country_id' => $this->country_id);

        return $this->render('ami/organization/index', array('page_title' => 'Organization Chart'));
    }

    public function build()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $salespersons = $this->salespersons_m->get_by(array_merge($conditions, array('manager_id !=' => 0)));

        $roles = $this->roles_m->get_by($conditions);
        $roles = array_rewrite($roles, 'id');

        foreach ($roles as $id => &$role) 
        {
            if ($role['depth'] == 1)
            {
                $role['color'] = 'darkred';
            }
            else if ($role['depth'] == 2)
            {
                $role['color'] = 'green';
            }
            else
            {
                $role['color'] = 'blue';
            }
        }

        // var_dump($roles);die;

        $output = array();

        foreach ($salespersons as $key => $salesperson) 
        {
            // $output[$key]['Id'] = intval($salesperson['roles_id']);
            // $output[$key]['ParentId'] = !intval($roles[$salesperson['roles_id']]['parent_id']) 
            //     ? -1 
            //     : intval($roles[$salesperson['roles_id']]['parent_id']);

            // $output[$key]['Name'] = $salesperson['first_name'] . ' ' . $salesperson['last_name'];
            // $output[$key]['Title'] = $roles[$salesperson['roles_id']]['name'];
            // $output[$key]['Image'] = site_url('res/img/m-av_s.png');            
            $output[$key] = array(
                'Id' => intval($roles[$salesperson['roles_id']]['depth']) == -1 ? -1 : intval($salesperson['id']),
                'ParentId' => intval($salesperson['manager_id']),
                'Name' => $salesperson['first_name'] . ' ' . $salesperson['last_name'],
                'Title' => $roles[$salesperson['roles_id']]['name'],
                'Image' => site_url('res/img/m-av_s.png'),
                'Role_Depth' => $roles[$salesperson['roles_id']]['depth'],
                'Color' => $roles[$salesperson['roles_id']]['color'],
                'id' => intval($roles[$salesperson['roles_id']]['depth']) == -1 ? -1 : intval($salesperson['id']),
            );
        }

        // $salespersons = array_only($salespersons, array('Id', 'Fullname', 'Title', 'ParentId'));

        // dump_exit($output);

        usort($output, function($a, $b) {
            return $a['Role_Depth'] - $b['Role_Depth'];
        });

        return $this->json($output);
    }

    public function migrate()
    {
        $conditions = array('country_id' => $this->country_id);

        $salespersons = $this->salespersons_m->get_by($conditions);
        $roles = $this->roles_m->get_by($conditions);        

        $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');

        $jackkee = array(
            1 => 393,
            2 => 221,
            3 => 264,
            4 => 317,
            5 => 144,
            6 => 307,
            7 => 389,
            8 => 5,
            9 => 93,          
        );

        foreach ($salespersons as $key => $value) 
        {   
            if ($value['salespersons_manager_id'])
            {                
                // echo 'update `salespersons` set manager_id = ' . $salespersons[$value['salespersons_manager_id']]['id'] . ' where id = ' . $value['id'];
                // echo '<br>';

                $this->db->query('update `salespersons` set manager_id = ' . $salespersons[$value['salespersons_manager_id']]['id'] . ' where id = ' . $value['id']);
            }
            // else
            // {
            //     $this->db->query('update `salespersons` set manager_id = ' . $jackkee[$this->country_id] . ' where id = ' . $value['id']);
            // }
        }

        $root = array();

        foreach ($roles as $role) 
        {
            if (intval($role['depth']) == 1)
            {
                $root[$role['id']] = $role;
            }
        }

        $this->db->query('update `salespersons` set manager_id = -1 where roles_id in (' . implode(',', array_keys($root)) . ')');

        die('Done');
        // var_dump($root);die;
    }
}