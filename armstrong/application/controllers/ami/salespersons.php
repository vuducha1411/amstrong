<?php

use Eloquent\Salesperson;

class Salespersons extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('salespersons_m');
        $this->load->model('salespersons_history_m');
        $this->load->model('roles_m');
        $this->load->model('customers_m');
    }

    public function index()
    {

        if (!$this->hasPermission('view', 'salespersons')) {
            return $this->noPermission();
        }

        $this->data['spFilter'] = $this->input->get('filter') ? '?filter=' . $this->input->get('filter') : false;
        $this->data['filter'] = $this->input->get('filter');
//        nam remove condition test acc
//        'test_acc' => 0
        $conditions = array("country_id IN ({$this->country_id})" => null, 'is_draft' => 0 );

        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }
        unset($conditions['test_acc']);
        switch ($this->input->get('filter')) {
            case 'inactive':
                $conditions['active'] = 0;
                break;

            default:
                $conditions['active'] = 1;
                break;
        }

        $this->data['salespersons'] = $this->salespersons_m->get_by($conditions);

        return $this->render('ami/salespersons/index', $this->data);
    }

    public function ajaxData()
    {
//        , 'test_acc' => 0
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata('user_data');
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, FALSE, NULL, array('admins_id', 'armstrong_2_salespersons_id'));
        // check type role of user
        if ($user['roles_id'] == $roles_salesmanager) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id']);
        } else if ($user['roles_id'] == $roles_salesperson) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id']);
        }
        switch ($this->input->get('filter')) {
            case 'inactive':
                $conditions['active'] = 0;
                break;

            default:
                $conditions['active'] = 1;
                break;
        }
        $datatables = new Datatable(array('model' => 'Salespersons_dt', 'rowIdCol' => $this->salespersons_m->getTablePrimary()));
        $this->salespersons_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['armstrong_2_salespersons_id'];
            $_data['armstrong_2_salespersons_id'] = '
                <a href="' . site_url('ami/salespersons/edit/' . $_data['armstrong_2_salespersons_id']) . '" data-toggle="ajaxModal">
                    ' . $_data['armstrong_2_salespersons_id'] . '
                </a>
            ';

            $_data['buttons'] = '<div class="btn-group">';


            if ($this->hasPermission('edit', 'salespersons')) {
                $_data['buttons'] .= html_btn(site_url('ami/salespersons/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'salespersons')) {
                $_data['buttons'] .= html_btn(site_url('ami/salespersons/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'salespersons')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $persons = $this->salespersons_m->get_limit($this->getPageLimit(), $conditions);

        $this->data['persons'] = $this->salespersons_m->preparePersons($persons);
        $this->data['total'] = $this->salespersons_m->count('where', $conditions);
        $this->data['draft'] = TRUE;

        return $this->render('ami/salespersons/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        // tuantq
        $role_id = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $conditions_managers = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, 'roles_id' => $role_id, 'test_acc' => 0);
        $countries_list = $this->country_m->getListCountry(null, array('is_draft' => 0));

        return array(
            'country_id' => $this->country_id,
            'countries_list' => $countries_list,
            'roles' => $this->roles_m->get_by($conditions),
            //'managers' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_managers),
            'managers' => $this->salespersons_m->get_by($conditions_managers),
            'types' => array('PULL' => 'PULL', 'PUSH' => 'PUSH', 'MIX' => 'MIX', 'CHAIN' => 'CHAIN', 'SL' => 'SL', 'ADMIN' => 'ADMIN', 'MARKETING' => 'MARKETING', 'TRAINER' => 'TRAINER'),
            'sub_types' => array('svp' => 'svp', 'vp' => 'vp', 'md' => 'md', 'cdops' => 'cdops', 'nsm' => 'nsm', 'ssm' => 'ssm', 'asm' => 'asm')
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'salespersons')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
//        tuantq
        $this->data['person']['active'] = 1;
        return $this->render('ami/salespersons/edit', $this->data);
    }

    public function edit($id = NULL)
    {
        if (!$this->hasPermission('edit', 'salespersons')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            $person = $this->salespersons_m->get($id);
            $this->data['person'] = $this->salespersons_m->preparePerson($person);
            $this->assertCountry($this->data['person']);
            $this->data['page_title'] = page_title('Edit Salespersons');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'salespersons')) {
                    return $this->noPermission();
                }

                return $this->render('ami/salespersons/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
//                dd($this->data);
                $this->data['countries_id'] = array_filter(explode(',', $person['country_management']));
                return $this->render('ami/salespersons/edit', $this->data);
            }
        }

        return redirect('ami/salespersons');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->salespersons_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/salespersons');
    }

    public function delete($id = NULL)
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            if ($this->is('POST', FALSE)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/salespersons';
                //$this->salespersons_m->delete($id);
                $this->salespersons_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/salespersons/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Salespersons"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/salespersons');
    }

    public function restore($id = NULL)
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            if ($this->is('POST', FALSE)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/salespersons/draft';
                $this->salespersons_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);
//ttytryrt
                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/salespersons/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Salespersons"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/salespersons');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'salespersons') && !$this->hasPermission('edit', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $post = $this->input->post();

        $data = $this->salespersons_m->array_from_post(array('armstrong_1_salespersons_id','first_name', 'last_name', 'password', 'email', 'alias', 'depth',
            'roles_id', 'type', 'sub_type', 'territory', 'handphone', 'did', 'salespersons_manager_id', 'persons', 'armstrong_2_salespersons_id', 'active', 'test_acc', 'line_id'));

        if (isset($post['country_management']) && $post['country_management']) {
            $data['country_management'] = implode(',', $post['country_management']);
        } else {
            if ($data['sub_type'] == 'vp' || $data['sub_type'] == 'svp') {
               // $data['country_management'] = '';
            } else {
                $data['country_management'] = $this->country_id;
            }
        }
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        $id = $data['armstrong_2_salespersons_id'] ? $data['armstrong_2_salespersons_id'] : NULL;
        if($id){
            $salesperson_country_id = $this->salespersons_m->get_by(array('armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id']), true);
            $data['country_id'] = $salesperson_country_id['country_id'];
        }else{
            $data['country_id'] = $this->country_id;
        }

        if ($data['depth'] == 1) {
            $data['manager_id'] = -1;
        }
        unset($data['depth']);

        $persons = $data['persons'] ? $data['persons'] : FALSE;
        unset($data['persons']);

        $newId = $this->salespersons_m->saveEntry($data, $id, $this->country_id);

        if ($persons) {
            $this->salespersons_m->setManager($persons, $newId);
        }

        $this->salespersons_m->syncManagerId($newId, $data['salespersons_manager_id']);

        return redirect('ami/salespersons');
    }

    public function roles()
    {
        if (!$this->hasPermission('add', 'salespersons') || !$this->hasPermission('edit', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $parentId = $this->input->post('parent');
        $id = $this->input->post('id');
        $editId = $this->input->post('edit');

        if ($parentId) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

            // $roles = $this->roles_m->get_by(array_merge($conditions, array('depth <' => $depth)));

            // $depth = $depth - 1 > 0 ? $depth - 1 : 1;

            $roleUp = $this->roles_m->get_by(array_merge($conditions, array('id' => $parentId)), TRUE);
            $roleDown = $this->roles_m->get_by(array_merge($conditions, array('parent_id' => $id)));
            $roleDown = array_rewrite($roleDown, 'id');

            $managers = $persons = array();

            if ($roleUp) {
                if ($editId) {
                    $managers = $this->salespersons_m->get_by(array(
                        'roles_id' => $roleUp['id'],
                        'armstrong_2_salespersons_id !=' => $editId
                    ));
                } else {
                    $managers = $this->salespersons_m->get_by(array('roles_id' => $roleUp['id']));
                }
            }

            if ($roleDown) {
                $_persons = $this->salespersons_m->get_in('roles_id', array_keys($roleDown));

                foreach ($_persons as $person) {
                    if ($person['armstrong_2_salespersons_id'] != $editId) {
                        $persons[] = array(
                            'value' => $person['armstrong_2_salespersons_id'],
                            'label' => $person['first_name'] . ' ' . $person['last_name']
                        );
                    }
                }
            }

            $params = array(
                'managers' => $managers,
                'persons' => $persons
            );

            if ($editId) {
                $params['salesperson'] = $this->salespersons_m->get($editId, FALSE);

                $_managers = $this->salespersons_m->get_by(array('salespersons_manager_id' => $editId));
                $_managers = array_rewrite($_managers, 'armstrong_2_salespersons_id');

                $params['salesperson']['managers'] = implode(',', array_keys($_managers));
            }

            return $this->render('ami/salespersons/manager', $params);
        }

        return '';
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'salespersons')
        ) {
            return redirect(site_url('ami/salespersons'));
        }

        $salespersons = $this->salespersons_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($salespersons);

        return $this->excel($sheet, 'salespersons', $type);
    }

    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->salespersons_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => TRUE,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => FALSE,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function validate()
    {
        $get = $this->input->get();

        if ($salesperson = $this->salespersons_m->get_by(array_except($get, array('armstrong_2_salespersons_id')), TRUE)) {
            if (!empty($get['armstrong_2_salespersons_id'])) {
                // Edit data, ignore this case
                if ($salesperson['armstrong_2_salespersons_id'] == $get['armstrong_2_salespersons_id']) {
                    return $this->json(array('validate' => 'success'));
                }
            }

            return show_error('validate error', 404);
        }

        return $this->json(array('validate' => 'success'));
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term) {
            return $this->json(array());
        }
        $conditions = array("first_name LIKE '%{$term}%'", "last_name LIKE '%{$term}%'", "armstrong_2_salespersons_id LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        $salespersons = $this->db
            ->where('country_id', $this->country_id)
            // ->or_like('first_name', $term)
            // ->or_like('last_name', $term)
            ->where($conditions)
            ->get('salespersons')
            ->result_array();

        $output = array();

        foreach ($salespersons as $salesperson) {
            $output[] = array(
                'value' => $salesperson['armstrong_2_salespersons_id'],
                // 'label' => $salesperson['armstrong_2_salespersons_id'],
                'label' => $salesperson['armstrong_2_salespersons_id'] . ' - ' . $salesperson['first_name'] . ' ' . $salesperson['last_name']
            );
        }

        return $this->json($output);
    }

    public function fetch_customer()
    {
        $id = $this->input->post('id');

        if (!$id) {
            return $this->json(array());
        }

        $customers = $this->customers_m->get_by(array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            'armstrong_2_salespersons_id' => $id,
            'approved' => 1,
            'active' => 1
        ));

        return $this->json($customers);
    }

    public function fetch_salesperson()
    {
        $post = $this->input->post();
        $appType = isset($post['app_type']) ? $post['app_type'] : '';
        $manager = isset($post['salespersons_manager_id']) ? $post['salespersons_manager_id'] : false;
        $user = $this->session->userdata('user_data');
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1
        );
        $sale_histories = array();
        if (!in_array(strtoupper($user['type']), array('ADMIN', 'SL', 'MARKETING', 'TRAINER'))) {
            $conditions['armstrong_2_salespersons_id'] = $user['id'];
            $html = '';
        } else {
            if (isset($post['active']) && is_numeric($post['active'])) {
                if ($post['active'] >= 0) {
                    $conditions['active'] = $post['active'];
                } else {
                    unset($conditions['active']);
                }
            }

            if (is_array($appType)) {
                $app_type = implode('","', $appType);
                if (in_array('PULL', $appType) || in_array('PUSH', $appType)) {
                    $app_type .= '","MIX';
                }
                $conditions['type IN ("' . $app_type . '")'] = null;
            } else {
                if (strtolower($appType) == 'sl') {
                    $conditions['type LIKE'] = '%sl%';
                }
                if (in_array(strtolower($appType), ['pull', 'push'])) {
                    $conditions['(type LIKE "%' . $appType . '%" OR type LIKE "%mix%")'] = null;
                }
            }


            if ($manager && $manager != 'ALL') {
                $conditions['salespersons_manager_id'] = $manager;
            }


            if (isset($post['month']) && isset($post['year'])) {
                if ($post['month'] != 0 && $post['year'] != 0) {
                    if (strlen($post['month']) == 1) $post['month'] = '0' . $post['month'];
                    $post['date_from'] = $post['year'] . '-' . $post['month'] . '-01';
                    $post['date_to'] = $post['year'] . '-' . $post['month'] . '-31';
                }
            }
            $hflag = false;
            $flag_ssd = true;
            if(isset($post['action']) && $post['action'] == 'data_ssd') //fet saleperson from ssd_report not use date
            {
                $flag_ssd = false;
            }
            if (isset($post['date_from']) && isset($post['date_to']) && $flag_ssd) {
                //hieu :(
                if (!is_numeric($user['id'])) {
                    if ($this->hasPermission('manage_staff', 'salespersons'))
                    {
                        $hflag = true;
                    }
                }
                $conditions["(date_created >= '{$post['date_from']}' and date_created <= '{$post['date_to']}')"] = null;
                $this->db->select('s.armstrong_2_salespersons_id, sp.first_name,sp.last_name');
                $this->db->from('salespersons_history as s');
                $this->db->where('s.is_draft', 0);
                $this->db->where('s.is_draft_salespersons', 0);
                $this->db->where('s.country_id', $this->country_id);
                if($hflag)
                {
                    $this->db->where('s.salespersons_manager_id', $user['id']);
                }
                if(isset($post['action']) && $post['action'] == 'dish_penetration_report')
                {
                    $this->db->where('sp.type IN ("PULL", "MIX") ', NULL);
                }
                //1482-AVP Points Report-Should not show SLs in salespersons list filter.
                /**
                if(isset($post['action']) && $post['action'] == 'avp_point_report')
                {
                    $this->db->where('sp.type != "SL" ', NULL);
                }
                 * */
                //end hieu
                if (isset($appType)) {
                    if (is_array($appType)) {
                        $this->db->where('(s.type IN ("' . $app_type . '"))', NULL);
                    } else {
                        if (strtolower($appType) == 'sl') {
                            $this->db->where('(s.type LIKE "%push%" || s.type LIKE "%pull%" || s.type LIKE "%mix%")', NULL);
                        } else {
                            $this->db->where('(s.type LIKE "%' . $appType . '%" || s.type LIKE "%mix%")', NULL);
                        }
                    }
                }
                if (isset($post['active']) && $post['active'] != '' && $post['active'] >= 0) {
                    $this->db->where('s.active', $post['active']);
                }
                $this->db->where("(((s.to_date IS NULL OR s.to_date >= '" . $post['date_from'] . " 00:00:00') AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR ((s.from_date IS NULL OR s.from_date <= '" . $post['date_to'] . " 23:59:59') AND s.to_date >= '" . $post['date_from'] . " 00:00:00')
			 OR (s.to_date >= '" . $post['date_from'] . " 00:00:00' AND s.to_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.from_date >= '" . $post['date_from'] . " 00:00:00' AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.to_date IS NULL AND s.from_date IS NULL)
			 )", NULL);
                //$this->db->join('roles as r', 's.roles_id = r.id AND r.name IN ("Salesperson","Sales Manager") AND r.is_draft = 0');
                $this->db->join('salespersons AS sp', 'sp.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id');
                $this->db->where('sp.test_acc', 0);
                $this->db->group_by('s.armstrong_2_salespersons_id');
                $query = $this->db->get();
              //  echo $this->db->last_query();
                $sales_history = $query->result();
                $query->free_result();
//                dd($this->db->last_query());
                if (count($sales_history) > 0) {
                    $sale_histories = array();
                    foreach ($sales_history as $s) {
                        $sale_histories[$s->armstrong_2_salespersons_id] = $s->armstrong_2_salespersons_id . ' - ' . $s->first_name . ' ' . $s->last_name;
                    }
                }
            }
            $html = '<option value="all" selected="selected" data-manager="">ALL</option>';
        }

        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions, true);
        $sales = array_merge($sales, $sale_histories);
//        $html = $sales ? '<option value="" selected="selected" data-manager="">ALL</option>' : '';

        $_sales = Salesperson::select('id', 'armstrong_2_salespersons_id', 'salespersons_manager_id')->whereIn('armstrong_2_salespersons_id', array_keys($sales))->get();

        $__sales = array_rewrite($_sales->toArray(), 'armstrong_2_salespersons_id');
        if ($html) {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '">' . $s . '</option>';
            }
        } else {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '" selected>' . $s . '</option>';
            }
        }
        echo $html;
        die;
    }

    public function fetch_salesperson2()
    {
        $post = $this->input->post();
        $appType = isset($post['app_type']) ? $post['app_type'] : '';
        $manager = isset($post['salespersons_manager_id']) ? $post['salespersons_manager_id'] : false;
        $user = $this->session->userdata('user_data');
        $conditions = array(
            'country_id' => $post['country_id'],
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1
        );
        $sale_histories = array();
        if (!in_array($user['type'], array('ADMIN', 'SL', 'MARKETING'))) {
            $conditions['armstrong_2_salespersons_id'] = $user['id'];
            $html = '';
        } else {
            if (isset($post['active']) && is_numeric($post['active'])) {
                if ($post['active'] >= 0) {
                    $conditions['active'] = $post['active'];
                } else {
                    unset($conditions['active']);
                }
            }

            if (is_array($appType)) {
                $app_type = implode('","', $appType);
                if (in_array('PULL', $appType) || in_array('PUSH', $appType)) {
                    $app_type .= '","MIX';
                }
                $conditions['type IN ("' . $app_type . '")'] = null;
            } else {
                if ($appType == 'sl') {
                    $conditions['type LIKE'] = '%sl%';
                }
                if (in_array($appType, ['pull', 'push'])) {
                    $conditions['(type LIKE "%' . $appType . '%" OR type LIKE "%mix%")'] = null;
                }
            }


            if ($manager && $manager != 'ALL') {
                $conditions['salespersons_manager_id'] = $manager;
            }


            if (isset($post['month']) && isset($post['year'])) {
                if ($post['month'] != 0 && $post['year'] != 0) {
                    if (strlen($post['month']) == 1) $post['month'] = '0' . $post['month'];
                    $post['date_from'] = $post['year'] . '-' . $post['month'] . '-01';
                    $post['date_to'] = $post['year'] . '-' . $post['month'] . '-31';
                }
            }

            if (isset($post['date_from']) && isset($post['date_to'])) {
                $conditions["(date_created >= {$post['date_from']} and date_created <= {$post['date_to']})"] = null;
                $this->db->select('s.armstrong_2_salespersons_id, sp.first_name,sp.last_name');
                $this->db->from('salespersons_history as s');
                $this->db->where('s.is_draft', 0);
                $this->db->where('s.is_draft_salespersons', 0);
                $this->db->where('s.country_id', $post['country_id']);
                if (!is_numeric($user['id'])) {
                    if ($user['type'] == 'SL')
                        $this->db->where('s.salespersons_manager_id', $user['id']);
                }

                if (isset($appType)) {
                    if (is_array($appType)) {
                        $this->db->where('(s.type IN ("' . $app_type . '"))', NULL);
                    } else {
                        if (strtolower($appType) == 'leader') {
                            $this->db->where('(s.type LIKE "%push%" || s.type LIKE "%pull%" || s.type LIKE "%mix%")', NULL);
                        } else {
                            $this->db->where('(s.type LIKE "%' . $appType . '%" || s.type LIKE "%mix%")', NULL);
                        }
                    }
                }
                if (isset($post['active']) && $post['active'] != '' && $post['active'] >= 0) {
                    $this->db->where('s.active', $post['active']);
                }
                $this->db->where("(((s.to_date IS NULL OR s.to_date >= '" . $post['date_from'] . " 00:00:00') AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
             OR ((s.from_date IS NULL OR s.from_date <= '" . $post['date_to'] . " 23:59:59') AND s.to_date >= '" . $post['date_from'] . " 00:00:00')
             OR (s.to_date >= '" . $post['date_from'] . " 00:00:00' AND s.to_date <= '" . $post['date_to'] . " 23:59:59')
             OR (s.from_date >= '" . $post['date_from'] . " 00:00:00' AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
             OR (s.to_date IS NULL AND s.from_date IS NULL)
             )", NULL);
                //$this->db->join('roles as r', 's.roles_id = r.id AND r.name IN ("Salesperson","Sales Manager") AND r.is_draft = 0');
                $this->db->join('salespersons AS sp', 'sp.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id');
                $this->db->where('sp.test_acc', 0);
                $this->db->group_by('s.armstrong_2_salespersons_id');
                $query = $this->db->get();
                $sales_history = $query->result();
                $query->free_result();
//                $sales_history = $this->filter_dummy_data($sales_history);
                if (count($sales_history) > 0) {
                    $sale_histories = array();
                    foreach ($sales_history as $s) {
                        $sale_histories[$s->armstrong_2_salespersons_id] = $s->armstrong_2_salespersons_id . ' - ' . $s->first_name . ' ' . $s->last_name;
                    }
                }
            }
            $html = '<option value="" selected="selected" data-manager="">ALL</option>';
        }
        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions, true);
        $sales = array_merge($sales, $sale_histories);
//        $html = $sales ? '<option value="" selected="selected" data-manager="">ALL</option>' : '';

        $_sales = Salesperson::select('id', 'armstrong_2_salespersons_id', 'salespersons_manager_id')->whereIn('armstrong_2_salespersons_id', array_keys($sales))->get();

        $__sales = array_rewrite($_sales->toArray(), 'armstrong_2_salespersons_id');
        if ($html) {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '">' . $s . '</option>';
            }
        } else {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '" selected>' . $s . '</option>';
            }
        }
        echo $html;
        die;
    }

    public function fetch_salesleader()
    {
        $post = $this->input->post();
        $appType = isset($post['app_type']) ? $post['app_type'] : '';
        $manager = isset($post['salespersons_manager_id']) ? $post['salespersons_manager_id'] : true;
        $user = $this->session->userdata('user_data');
        $conditions = array(
            'country_id' => $post['country_id'],
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1
        );
        $sale_histories = array();
        $this->db->distinct();
        $this->db->select('armstrong_2_salespersons_id, first_name, last_name, sub_type');
        $this->db->from('salespersons');
        $this->db->where('country_id', $post['country_id']);
        $this->db->where('is_draft', 0);
        $this->db->where('active', 1);
        $this->db->where('type', 'SL');
        $this->db->where('test_acc', 0);
        $query = $this->db->get();
        $sales_history = $query->result();
        $query->free_result();
//                $sales_history = $this->filter_dummy_data($sales_history);
        if (count($sales_history) > 0) {
            $sale_histories = array();
            foreach ($sales_history as $s) {
                $sale_histories[$s->armstrong_2_salespersons_id] = $s->first_name . ' ' . $s->last_name;
            }
        }
        $html = '<option value="ALL" selected="selected" data-sub_type="">ALL</option>';


        $sales = array_merge($sale_histories);
//        $html = $sales ? '<option value="" selected="selected" data-manager="">ALL</option>' : '';

        $_sales = Salesperson::select('id', 'armstrong_2_salespersons_id', 'sub_type')->whereIn('armstrong_2_salespersons_id', array_keys($sales))->get();

        $__sales = array_rewrite($_sales->toArray(), 'armstrong_2_salespersons_id');
        if ($html) {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-sub_type="' . $__sales[$k]['sub_type'] . '">' . $s . '</option>';
            }
        } else {
            foreach ($sales as $k => $s) {
                $html .= '<option value="' . $k . '" data-sub_type="' . $__sales[$k]['sub_type'] . '" selected>' . $s . '</option>';
            }
        }
        $html = '<option value="unassigned" data-sub_type="">Unassigned</option>';
        echo $html;
        die;
    }

    public function fetch_salesperson_demo()
    {
        $post = $this->input->post();

        $appType = isset($post['app_type']) ? strtolower($post['app_type']) : '';
        $manager = isset($post['salespersons_manager_id']) ? $post['salespersons_manager_id'] : false;
        $range = isset($post['range']) ? $post['range'] : 0;
        $user = $this->session->userdata('user_data');
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1
        );
        $sale_histories = array();
        if (!in_array($user['type'], array('ADMIN', 'SL'))) {
            $conditions['armstrong_2_salespersons_id'] = $user['id'];
            $html = '';
        } else {
            if (isset($post['active']) && is_numeric($post['active'])) {
                if ($post['active'] >= 0) {
                    $conditions['active'] = $post['active'];
                } else {
                    unset($conditions['active']);
                }
            }

            if ($appType == 'sl') {
                $conditions['type LIKE'] = '%sl%';
            }

            if ($manager && $manager != 'ALL') {
                $manager = '"' . implode('","', $manager) . '"';
                $conditions["salespersons_manager_id IN ({$manager})"] = null;
            }

            if (in_array($appType, ['pull', 'push'])) {
                $conditions['(type LIKE "%' . $appType . '%" OR type LIKE "%mix%")'] = null;
            }

            if (isset($post['month']) && isset($post['year'])) {
                if ($post['month'] != 0 && $post['year'] != 0) {
                    if (strlen($post['month']) == 1) $post['month'] = '0' . $post['month'];
                    $post['date_from'] = $post['year'] . '-' . $post['month'] . '-01';
                    $post['date_to'] = $post['year'] . '-' . $post['month'] . '-31';
                }
            }

            if ($range != 0) {
                switch ($range) {
                    case 1:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 3 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 3 month'));
                        break;
                    case 2:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 6 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 6 month'));
                        break;
                    case 3:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 12 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 12 month'));
                        break;
                    case 4:
                        $post['date_from'] = date('Y-m-01', strtotime('january last year'));
                        $post['date_to'] = date('Y-m-t', strtotime('june last year'));
                        break;
                    case 5:
                        $post['date_from'] = date('Y-m-01', strtotime('july last year'));
                        $post['date_to'] = date('Y-m-t', strtotime('december last year'));
                        break;
                    case 6:
                        $post['date_from'] = date('Y-01-01', strtotime('- 1 year'));
                        $post['date_to'] = date('Y-12-t', strtotime(' - 1 year'));
                        break;
                    default:
                        break;
                }
            }

            if (isset($post['date_from']) && isset($post['date_to'])) {
                $conditions["(date_created >= {$post['date_from']} and date_created <= {$post['date_to']})"] = null;
                $this->db->select('s.armstrong_2_salespersons_id, sp.first_name,sp.last_name');
                $this->db->from('salespersons_history as s');
                $this->db->where('s.is_draft', 0);
                $this->db->where('s.is_draft_salespersons', 0);
                $this->db->where('s.country_id', $this->country_id);
                if (!is_numeric($user['id'])) {
                    if ($user['type'] == 'SL')
                        $this->db->where('s.salespersons_manager_id', $user['id']);
                }

                if (isset($appType)) {
                    if (strtolower($appType) == 'leader') {
                        $this->db->where('(s.type LIKE "%push%" || s.type LIKE "%pull%" || s.type LIKE "%mix%")', NULL);
                    } else {
                        $this->db->where('(s.type LIKE "%' . $appType . '%" || s.type LIKE "%mix%")', NULL);
                    }
                }
                if (isset($post['active']) && $post['active'] != '' && $post['active'] >= 0) {
                    $this->db->where('s.active', $post['active']);
                }
//                $this->db->where("s.salespersons_manager_id IN ({$manager})", null);
                $this->db->where("(((s.to_date IS NULL OR s.to_date >= '" . $post['date_from'] . " 00:00:00') AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR ((s.from_date IS NULL OR s.from_date <= '" . $post['date_to'] . " 23:59:59') AND s.to_date >= '" . $post['date_from'] . " 00:00:00')
			 OR (s.to_date >= '" . $post['date_from'] . " 00:00:00' AND s.to_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.from_date >= '" . $post['date_from'] . " 00:00:00' AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.to_date IS NULL AND s.from_date IS NULL)
			 )", NULL);
                //$this->db->join('roles as r', 's.roles_id = r.id AND r.name IN ("Salesperson","Sales Manager") AND r.is_draft = 0');
                $this->db->join('salespersons AS sp', 'sp.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id');
                $this->db->where('sp.test_acc', 0);
                if ($manager) {
                    $this->db->where("sp.salespersons_manager_id IN ({$manager})", null);
                }
                $this->db->group_by('s.armstrong_2_salespersons_id');
                $query = $this->db->get();
                $sales_history = $query->result();
                $query->free_result();
                if (count($sales_history) > 0) {
                    $sale_histories = array();
                    foreach ($sales_history as $s) {
                        $sale_histories[$s->armstrong_2_salespersons_id] = $s->armstrong_2_salespersons_id . ' - ' . $s->first_name . ' ' . $s->last_name;
                    }
                }
            }
            $html = '';
        }
        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions, true);
        $sales = array_merge($sales, $sale_histories);
//        $html = $sales ? '<option value="" selected="selected" data-manager="">ALL</option>' : '';

        $_sales = Salesperson::select('id', 'armstrong_2_salespersons_id', 'salespersons_manager_id')->whereIn('armstrong_2_salespersons_id', array_keys($sales))->get();

        $__sales = array_rewrite($_sales->toArray(), 'armstrong_2_salespersons_id');
//        if ($range != 0) {
//            foreach ($sales as $k => $s) {
//                $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '" selected>' . $s . '</option>';
//            }
//        } else {
        foreach ($sales as $k => $s) {
            $html .= '<option value="' . $k . '" data-manager="' . $__sales[$k]['salespersons_manager_id'] . '" selected>' . $s . '</option>';
        }
//        }
        $html .= '<option value="unassigned" data-manager="" selected>Unassigned</option>';

        echo $html;
        die;
    }

    public function fetch_salesperson_report()
    {
        $post = $this->input->post();

        $appType = isset($post['app_type']) ? strtolower($post['app_type']) : '';
        $manager = isset($post['salespersons_manager_id']) ? $post['salespersons_manager_id'] : false;
        $range = isset($post['range']) ? $post['range'] : 0;
        $user = $this->session->userdata('user_data');
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1
        );
        $sale_histories = array();
        if (!in_array($user['type'], array('ADMIN', 'SL'))) {
            $conditions['armstrong_2_salespersons_id'] = $user['id'];
            $html = '';
        } else {
            if (isset($post['active']) && is_numeric($post['active'])) {
                if ($post['active'] >= 0) {
                    $conditions['active'] = $post['active'];
                } else {
                    unset($conditions['active']);
                }
            }

            if ($appType == 'sl') {
                $conditions['type LIKE'] = '%sl%';
            }

            if ($manager && $manager != 'ALL') {
                $manager = '"' . implode('","', $manager) . '"';
                $conditions["salespersons_manager_id IN ({$manager})"] = null;
            }

            if (in_array($appType, ['pull', 'push'])) {
                $conditions['(type LIKE "%' . $appType . '%" OR type LIKE "%mix%")'] = null;
            }

            if (isset($post['month']) && isset($post['year'])) {
                if ($post['month'] != 0 && $post['year'] != 0) {
                    if (strlen($post['month']) == 1) $post['month'] = '0' . $post['month'];
                    $post['date_from'] = $post['year'] . '-' . $post['month'] . '-01';
                    $post['date_to'] = $post['year'] . '-' . $post['month'] . '-31';
                }
            }

            if ($range != 0) {
                switch ($range) {
                    case 1:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 3 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 3 month'));
                        break;
                    case 2:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 6 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 6 month'));
                        break;
                    case 3:
                        $post['date_from'] = date('Y-m-d', strtotime($post['date_from'] . ' - 12 month'));
                        $post['date_to'] = date('Y-m-t', strtotime($post['date_to'] . ' - 12 month'));
                        break;
                    case 4:
                        $post['date_from'] = date('Y-m-01', strtotime('january last year'));
                        $post['date_to'] = date('Y-m-t', strtotime('june last year'));
                        break;
                    case 5:
                        $post['date_from'] = date('Y-m-01', strtotime('july last year'));
                        $post['date_to'] = date('Y-m-t', strtotime('december last year'));
                        break;
                    case 6:
                        $post['date_from'] = date('Y-01-01', strtotime('- 1 year'));
                        $post['date_to'] = date('Y-12-t', strtotime(' - 1 year'));
                        break;
                    default:
                        break;
                }
            }

            if (isset($post['date_from']) && isset($post['date_to'])) {
                $conditions["(date_created >= {$post['date_from']} and date_created <= {$post['date_to']})"] = null;
                $this->db->select('s.armstrong_2_salespersons_id, sp.first_name, sp.last_name, sp.salespersons_manager_id, s.type');
                $this->db->from('salespersons_history as s');
                $this->db->where('s.is_draft', 0);
                $this->db->where('s.is_draft_salespersons', 0);
                $this->db->where('s.country_id', $this->country_id);
                if (!is_numeric($user['id'])) {
                    if ($user['type'] == 'SL')
                        $this->db->where('s.salespersons_manager_id', $user['id']);
                }

                if (isset($appType)) {
                    if (strtolower($appType) == 'leader') {
                        $this->db->where('(s.type LIKE "%push%" || s.type LIKE "%pull%" || s.type LIKE "%mix%")', NULL);
                    } else {
                        $this->db->where('(s.type LIKE "%' . $appType . '%" || s.type LIKE "%mix%")', NULL);
                    }
                }
                if (isset($post['active']) && $post['active'] != '' && $post['active'] >= 0) {
                    $this->db->where('s.active', $post['active']);
                }
                $this->db->where("(((s.to_date IS NULL OR s.to_date >= '" . $post['date_from'] . " 00:00:00') AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR ((s.from_date IS NULL OR s.from_date <= '" . $post['date_to'] . " 23:59:59') AND s.to_date >= '" . $post['date_from'] . " 00:00:00')
			 OR (s.to_date >= '" . $post['date_from'] . " 00:00:00' AND s.to_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.from_date >= '" . $post['date_from'] . " 00:00:00' AND s.from_date <= '" . $post['date_to'] . " 23:59:59')
			 OR (s.to_date IS NULL AND s.from_date IS NULL)
			 )", NULL);
                $this->db->join('salespersons AS sp', 'sp.armstrong_2_salespersons_id = s.armstrong_2_salespersons_id');
                $this->db->where('sp.test_acc', 0);
                if ($manager) {
                    $this->db->where("sp.salespersons_manager_id IN ({$manager})", null);
                }
                $this->db->group_by('s.armstrong_2_salespersons_id');
                $query = $this->db->get();
                $sales_history = $query->result_array();
                $query->free_result();
            }
            $html = '';
        }

        $current_salespersons = $this->db->select('armstrong_2_salespersons_id, first_name, last_name, salespersons_manager_id, type')
            ->from('salespersons')
            ->where($conditions)
            ->get()->result_array();
        $list_sales_leader = $this->db->select('armstrong_2_salespersons_id, first_name, last_name, salespersons_manager_id, type')
            ->from('salespersons')
            ->where(array(
                'type' => 'sl',
                'is_draft' => 0,
                'country_id' => $this->country_id,
                'active' => 1
            ))
            ->get()->result_array();
        $sales = array_merge($current_salespersons, $sales_history);
        $reformat_sales = [];
        if ($sales) {
            foreach ($sales as $_sales) {
                $reformat_sales[$_sales['salespersons_manager_id']][] = $_sales;
            }
            foreach ($list_sales_leader as &$sl) {
                $sl['child'] = [];
                foreach ($reformat_sales as $key => $val) {
                    if ($key == $sl['armstrong_2_salespersons_id']) {
                        $sl['child'] = $val;
                    }
                }
            }
        }
        dd($list_sales_leader);


        echo $html;
        die;
    }

}