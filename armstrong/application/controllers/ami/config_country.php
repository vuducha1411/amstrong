<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Eloquent\CountryConfig;
class Config_country extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_config_m');
    }

    public function index()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }

        $config_country = CountryConfig::find($this->country_id);
        if($config_country){
            $config_country = $config_country->toArray();
            $config_country['mighty_10'] = json_decode($config_country['mighty_10']);
            $config_country['channel_basket_penetration'] = json_decode($config_country['channel_basket_penetration']);
            $config_country['active_sku'] = json_decode($config_country['active_sku']);
            $config_country['sampling_history'] = json_decode($config_country['sampling_history']);
        }

        $this->data['post'] = $config_country;
        $this->data['page_title'] = page_title('Customer Analysis Configuration');
        return $this->render('ami/config_country/index', $this->data);
    }

    public function save()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $fields = array('id', 'country_id', 'mighty_10', 'channel_basket_penetration', 'active_sku', 'sampling_history');
        $data = $this->country_config_m->array_from_post($fields);
        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;

        // Create new record
        if (!$id) {
            $data['date_created'] = $data['last_updated'] = date('Y-m-d H:i:s');
        }else{
            $data['last_updated'] = date('Y-m-d H:i:s');
        }
        $mighty = $channel = $active = $sample = null;
        if(is_array($data['mighty_10'])){
            $mighty = json_encode($data['mighty_10']);
        }
        if(is_array($data['channel_basket_penetration'])){
            $channel = json_encode($data['channel_basket_penetration']);
        }
        if(is_array($data['active_sku'])){
            $active = json_encode($data['active_sku']);
        }
        if(is_array($data['sampling_history'])){
            $sample = json_encode($data['sampling_history']);
        }
        $data['mighty_10'] = $mighty;
        $data['channel_basket_penetration'] = $channel;
        $data['active_sku'] = $active;
        $data['sampling_history'] = $sample;
//        dd($data);
        $recordId = $this->country_config_m->save($data, $id);


        return redirect('ami/config_country');
    }

}