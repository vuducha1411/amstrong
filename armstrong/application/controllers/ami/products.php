<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('products_m');
        $this->load->model('media_m');
        $this->load->model('groups_m');
        $this->load->model('brands_m');
        $this->load->model('products_cat_web_m');
        $this->load->model('segments_m');
        $this->load->model('cuisine_channels_m');
        $this->load->model('categories_m');
        $this->load->model('markets_m');
        $this->load->model('sectors_m');
        $this->load->model('sub_sectors_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'product')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        // $this->data['products'] = $this->products_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['products'] = array(
            'active' => $this->products_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->products_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        $this->data['total'] = $this->products_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Products');

        return $this->render('ami/products/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'product')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        // $this->data['products'] = $this->products_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['products'] = array(
            'active' => $this->products_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->products_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        $this->data['total'] = $this->products_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Products');

        return $this->render('ami/products/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            'groups' => $this->groups_m->parse_form($cols, '- Select -', $conditions),
            'app_type' => $this->app_type,
            'brands' => $this->brands_m->parse_form($cols, '- Select -'),
            'products_cat_web' => $this->products_cat_web_m->parse_form($cols, '- Select -', $conditions),
            'segments' => array(0 => '- Select -') + $this->segments_m->parse_form($cols, null, $conditions),
            'categories' => array(0 => '- Select -') + $this->categories_m->parse_form($cols, null, $conditions),
            'markets' => array(0 => '- Select -') + $this->markets_m->parse_form($cols, null, $conditions),
            'sectors' => array(0 => '- Select -') + $this->sectors_m->parse_form($cols, null, $conditions),
            'sub_sectors' => array(0 => '- Select -') + $this->sub_sectors_m->parse_form($cols, null, $conditions),
            'cuisine_channels' => $this->cuisine_channels_m->parse_form($cols, '- Select -', $conditions),
            'main_sku' => $this->products_m->getProductListOptions('- Select -', array_merge(array('listed' => 1), $conditions))
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'product')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['product']['listed'] = 1;
        return $this->render('ami/products/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'product') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/products');

        if ($id = intval($id)) {
            $this->data['product'] = $this->products_m->get($id);
            $this->assertCountry($this->data['product']);
            $this->data['product']['glob_pro_hierarchy_array'] = !empty($this->data['product']['glob_pro_hierarchy'])
                ? explode(',', str_replace(array('{', '}'), array('', ''), $this->data['product']['glob_pro_hierarchy'])) //json_decode($this->data['product']['glob_pro_hierarchy'], true)
                : array();
            $this->data['images'] = $this->media_m->getMedia('image', 'products', $this->country_id, $id, true);
            $this->data['videos'] = $this->media_m->getMedia('video', 'products', $this->country_id, $id, true);
            $this->data['brochures'] = $this->media_m->getMedia('brochure', 'products', $this->country_id, $id, true);
            $this->data['certs'] = $this->media_m->getMedia('cert', 'products', $this->country_id, $id, true);
            $this->data['specs'] = $this->media_m->getMedia('spec', 'products', $this->country_id, $id, true);
            $this->data['page_title'] = page_title('Edit Products');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'product')) {
                    return $this->noPermission();
                }

                return $this->render('ami/products/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();

                return $this->render('ami/products/edit', $this->data);
            }
        }

        return redirect('ami/products');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'product')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->products_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/products');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'product')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/products');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/products';
                //$this->products_m->delete($id);
                $this->products_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/products/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Products"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/products');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'product')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/products');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/products/draft';
                $this->products_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/products/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Products"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/products');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'product') && !$this->hasPermission('edit', 'product')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->products_m->array_from_post(array('app_type','sku_number', 'sku_name', 'name_alt', 'description', 'groups_id',
            'brands_id', 'products_cat_web_id', 'ingredients', 'barcode', 'price', 'weight_pc', 'glob_pro_hierarchy', 'quantity_case',
            'shelf_life', 'pro_claims', 'is_top_ten', 'cuisine_channels_id', 'main_sku', 'pallet_configuration',
            'nutrition_name', 'listed', 'listing_availability', 'id', 'gross_margin'));
//        'segments_id',
        $data['country_id'] = $this->country_id;
        $data['description'] = str_replace('<br />', '<br>', $data['description']);
        $data['ingredients'] = str_replace('<br />', '<br>', $data['ingredients']);

        $data['glob_pro_hierarchy'] = '{' . implode(',', $data['glob_pro_hierarchy']) . '}';

        $id = $data['id'] ? $data['id'] : null;
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        $newId = $this->products_m->save($data, $id);

        // Update existing record
        if ($id) {

            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('products', $id, $unlink);
                }
            }
        }


        $mediaChange = $this->input->post('media_ref_change');
        $brochureChangeAppType = $this->input->post('brochure_app_type');
        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'products',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }
        if($brochureChangeAppType){
            foreach($brochureChangeAppType as $media_id => $changeAppType){
                $this->media_m->updateMediaRef(
                    array(
                        'app_type' => $changeAppType,
                        'last_updated' => date('Y-m-d H:i:s')
                    ),
                    array(
                        'media_id' => $media_id,
                        'entry_id' => $id,
                        'entry_type' => 'products',
                        'country_id' => $this->country_id
                    )
                );
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('path' => $this->input->post('uniqueId')));
            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'products';
                    $args['country_id'] = $this->country_id;
                    $this->media_m->addMediaRef($args);
                }

            }
        }

        return redirect('ami/products');
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term) {
            return $this->json(array());
        }

        $conditions = array("sku_number LIKE '%{$term}%'", "sku_name LIKE '%{$term}%'", "name_alt LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        if($sku_except = trim($this->input->get('sku_except'), ','))
        {
            $conditions = $conditions.' AND sku_number NOT IN ('.$sku_except.')';
        }
        if($this->input->get('products_cat_web_id') != '')
        {
            $conditions = $conditions.' AND products_cat_web_id = '.(int)$this->input->get('products_cat_web_id');
        }
        $this->db->where('country_id', $this->country_id)
            ->where('listed', 1)
            ->where('quantity_case > ', 0)
            ->where('is_draft', 0)
            ->where($conditions);

        $products = $this->db->limit(30)
            ->get($this->products_m->getTableName())
            ->result_array();

        $output = array();

        foreach ($products as $product) {
            $title = in_array($this->country_code, array('tw', 'hk')) ? 'name_alt' : 'sku_name';

            $output[] = array(
                'value' => $product['sku_number'],
                // 'label' => $salesperson['armstrong_2_salespersons_id'],
                'label' => get_product_label($product, $title),
                'price' => $product['price'],
                'pcsprice' => $product['price'] / $product['quantity_case']
            );
        }

        return $this->json($output);
    }

    public function getTopProducts()
    {
        if ($this->input->is_ajax_request()) {
            $output = ['topProducts' => '', 'otherProducts' => ''];
            $topSkuHtml = $otherSkuHtml = '';

            $params = $this->input->post();
            $selected = 'selected';
            if(is_array($params['app_type']) && $params['app_type'])
            {
                if(in_array('PULL', $params['app_type'])){
                    $type[] = 1;
                    $type[] = 2;
                } else if(in_array('PUSH', $params['app_type'])){
                    $type[] = 3;
                    $type[] = 4;
                } else {
                    $type[] = 2;
                    $type[] = 4;
                }
            }
            else
            {
               // $type[] = @$params['app_type'];
                if('PULL' == $params['app_type']){
                    $type[] = 1;
                    $type[] = 2;
                } else if('PUSH' == $params['app_type']){
                    $type[] = 3;
                    $type[] = 4;
                } else {
                    $type[] = 2;
                    $type[] = 4;
                }
            }
            $type = '('.implode(',', $type).')';
            if(isset($params['from']) && isset($params['to']))
            {
                $minyear = date("Y", strtotime($params['from']));
                $maxyear = date("Y", strtotime($params['to']));
                $minmonth = date("m", strtotime($params['from']));
                $maxmonth = date("m", strtotime($params['to']));
//                $where = array(
//                    'top.countryID' => $params['country_id'],
//                    'top.monthReport >= ' => $minmonth,
//                    'top.monthReport <= ' => $maxmonth,
//                    'top.yearReport >= ' => $minyear,
//                    'top.yearReport' => $maxyear,
//                    'top.skuNumber = `mainSku`' => null,
//                    'top.type IN '.$type => null
//                );
                $where = array(
                    'top.countryID' => $params['country_id'],
                    'TO_DAYS(CONCAT(yearReport,"-",monthReport,"-","15")) >= TO_DAYS(CONCAT('.$minyear.',"-",'.$minmonth.',"-","01"))' => null,
                    'TO_DAYS(CONCAT(yearReport,"-",monthReport,"-","15")) <= TO_DAYS(CONCAT('.$maxyear.',"-",'.$maxmonth.',"-","28"))' => null,
                    'top.skuNumber = `mainSku`' => null,
                    'top.type IN '.$type => null
                );
                $selected = '';
            }
            else
            {
              //  $type = $params['app_type'] == 'PULL' ? 2 : 4;
                $where = array(
                    'top.countryID' => $params['country_id'],
                    'top.monthReport' => $params['month'],
                    'top.yearReport' => $params['year'],
                //    'top.skuNumber = `mainSku`' => null,
                    'top.type IN '.$type => null
                );
            }
            $top_sku = $this->db->select(array('top.skuNumber', 'main_sku', 'top.skuNname', 'top.pakingSize', 'p.group_name'))
                ->from('top_sku_on_month top')
                ->join('products p', 'p.sku_number = top.skuNumber', 'left')
                ->where($where)
                ->order_by('top.skuNname', 'asc')
                ->group_by('top.skuNumber')
                ->get()->result_array();
//            echo $this->db->last_query();
//            die;
            $top_sku_number = [];
            if ($top_sku) {
                $topSku = array();
                foreach ($top_sku as $key => $sku) {
//                    $sku_name = $sku['group_name'] ? $sku['group_name'] : $sku['skuNname'] . ' (' . $sku['pakingSize'] .')';
//                    $topSkuHtml .= '<option value="' . $sku['skuNumber'] . '" '.$selected.'>' . $sku_name . '</option>';
                    $top_sku_number[] = $sku['skuNumber'];
                    if($sku['main_sku'])
                    {
                        $sku_name = $sku['group_name'] ? $sku['group_name'] : $sku['skuNname'] . ' (' . $sku['pakingSize'] .')';
                        $topSku[$sku['main_sku']] = '<option value="' . $sku['main_sku'] . '">' . $sku_name .'</option>';
                    }
                    else
                    {
                        $sku_name = $sku['group_name'] ? $sku['group_name'] : $sku['skuNname'] . ' (' . $sku['pakingSize'] .')';
                        $topSku[] .= '<option value="' . $sku['skuNumber'] . '">' . $sku_name .'</option>';
                    }
                }
                $topSkuHtml = implode($topSku);
                if(isset($params['all']))
                {
                    $topSkuHtml = '<option value="" data-sub_type="">None</option><option selected="selected" value="ALL" data-sub_type="">ALL</option>'.$topSkuHtml;
                }
                else
                {
                    $topSkuHtml .= '<option value="" selected="selected" data-sub_type="">None</option>';
                }
            }
            $output['topProducts'] = $topSkuHtml;
            $top_sku_number_str = "'sku_number'";
            if($top_sku_number)
            {
                foreach($top_sku_number as $top_sku)
                {
                    $top_sku_number_str .= ",'$top_sku'";
                }
            }
            //$top_sku_number = implode(',', $top_sku_number);
            $top_sku_number = $top_sku_number_str;
            if ($top_sku_number) {
                $sub_sql = "select main_sku from products where sku_number NOT IN (" . $top_sku_number . ") and country_id = " . $this->country_id . " group by main_sku";
            } else {
                $sub_sql = "select main_sku from products where listed = 1 and is_draft = 0 and country_id = " . $this->country_id . " and main_sku IS NOT NULL AND main_sku != '' group by main_sku";
            }
            $otherProducts = $this->db->select(array('sku_number', 'main_sku', 'sku_name', 'CONCAT(`quantity_case`," x ", `weight_pc`) as pakingSize', 'group_name'))
                ->from('products')
                ->where(array(
                    'listed' => 1,
                    'is_draft' => 0,
                    'is_top_ten' => 0,
                    'country_id' => $this->country_id,
                    '(main_sku IS NULL OR main_sku = "") OR (sku_number IN(' . $sub_sql . ') AND listed = 1 AND main_sku != 0) OR (main_sku IN(' . $sub_sql . ') AND listed = 1 AND main_sku != 0)' => null
                ))
                ->order_by('sku_name', 'asc')
                ->group_by('sku_number')
                ->get()->result_array();
            if ($otherProducts) {
                $others = array();
                foreach ($otherProducts as $key => $sku) {
                    if($sku['main_sku'])
                    {
                        $sku_name = $sku['group_name'] ? $sku['group_name'] : $sku['sku_name'] . ' (' . $sku['pakingSize'] .')';
                        $others[$sku['main_sku']] = '<option value="' . $sku['main_sku'] . '">' . $sku_name .'</option>';
                    }
                    else
                    {
                        $sku_name = $sku['group_name'] ? $sku['group_name'] : $sku['sku_name'] . ' (' . $sku['pakingSize'] .')';
                        $others[] .= '<option value="' . $sku['sku_number'] . '">' . $sku_name .'</option>';
                    }
                }
                $otherSkuHtml = implode($others);
                $output['otherProducts'] = $otherSkuHtml;
            }

            echo json_encode($output);
            exit;
        }
    }

    public function checkExistSkuNumber()
    {
        if ($this->input->is_ajax_request()) {
            $params = $this->input->post();
            if (isset($params['sku_number'])) {
                $result = array(
                    'error' => 0,
                    'message' => ''
                );
                $exist = $this->db->from('products')
                    ->where(array(
                        'country_id' => $this->country_id,
                        'sku_number' => $params['sku_number']
                    ))
                    ->get()->row();
                if ($exist) {
                    $result['error'] = 1;
                    $result['message'] = 'SKU Number has been taken. Please try again.';
                }
                echo json_encode($result);
                exit;
            }
        }
    }

    public function countNumberTopSku()
    {
        if ($this->input->is_ajax_request()) {
            $result = '';
            $params = $this->input->post();
            $top_sku = $this->db->select(array('skuNumber', 'skuNname'))
                ->from('top_sku_on_month')
                ->where(array(
                    'countryID' => $params['country_id'],
                    'monthReport' => $params['month'],
                    'yearReport' => $params['year'],
                    'skuNumber = `mainSku`' => null,
                    'type' => 2
                ))
                ->get()->result_array();
            $result .= '<option value="0" selected>All</option>';
            if (count($top_sku) > 0) {
                $i = 1;
                foreach ($top_sku as $k => $t) {
                    $result .= '<option value="' . $i . '">' . $i . '</option>';
                    $i++;
                }
            }
            echo $result;
            exit;
        }
    }

}