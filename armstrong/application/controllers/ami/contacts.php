<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contacts_m');
        $this->load->model('contacts_amended_m');
        $this->load->model('media_m');
        // $this->load->model('cuisine_channels_m');
        // $this->load->model('products_m');
        $this->load->model('customers_m');
        $this->load->model('wholesalers_m');
        $this->load->model('distributors_m');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'active':
                $filter = array('status' => 1);
                break;

            case 'inactive':
                $filter = array('status' => 0);
                break;

            default:
                $filter = array('status' => 1);
                break;
        }

        return $filter;
    }

    protected function _getListContacts($where)
    {
        $contactsTable = $this->contacts_m->getTableName();
        $customersTable = $this->customers_m->getTableName();
        $wholesalersTable = $this->wholesalers_m->getTableName();
        $distributorsTable = $this->distributors_m->getTableName();

        $this->db->select("{$contactsTable}.*, {$customersTable}.armstrong_2_customers_name as {$customersTable}_name,
            {$wholesalersTable}.name as {$wholesalersTable}_name, {$distributorsTable}.name as {$distributorsTable}_name");
        $this->db->from($contactsTable);

        $this->db->join($customersTable, "{$contactsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id and {$contactsTable}.type = 0", 'left');
        $this->db->join($wholesalersTable, "{$contactsTable}.armstrong_2_customers_id = {$wholesalersTable}.armstrong_2_wholesalers_id and {$contactsTable}.type = 1", 'left');
        $this->db->join($distributorsTable, "{$contactsTable}.armstrong_2_customers_id = {$distributorsTable}.armstrong_2_distributors_id and {$contactsTable}.type = 2", 'left');

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $this->db->limit($total, $offset);

        $this->db->where($where);

        $this->db->order_by("{$contactsTable}.id", 'asc');

        return $this->db->get()->result_array();
    }


    public function index()
    {
        if (!$this->hasPermission('view', 'contacts')) {
            return $this->noPermission();
        }

        $this->data['filter'] = $this->input->get('filter') ? $this->input->get('filter') : 'active';
        return $this->render('ami/contacts/index');
    }

    public function ajaxData()
    {
        $user = $this->session->userdata('user_data');
        $filter = $this->_getFilter();
        $conditions = array_merge(array('c.country_id' => $this->country_id, 'c.is_draft' => 0), $filter);
        if(!$this->isSuperAdmin())
        {
            if ($this->hasPermission('manage_staff', 'salespersons')) {
                $list_sales = $this->salespersons_m->getListSales($user['id']);
                $list_sales = implode("','", array_merge(array($user['id']), $list_sales));
                $cus_conditons = array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    "armstrong_2_salespersons_id IN ('{$list_sales}')" => null
                );
                //ilovephp fix https://ufs-armstrong.slack.com/files/sang/F3RL744FK/pasted_image_at_2017_01_17_11_10_am.png
                $customers = $this->db->from('customers')
                    ->where($cus_conditons)->select('armstrong_2_customers_id as a')->get()->result_array();
                $distributors = $this->db->from('distributors')
                    ->where($cus_conditons)->select('armstrong_2_distributors_id as a')->get()->result_array();
                $wholesalers = $this->db->from('wholesalers')
                    ->where($cus_conditons)->select('armstrong_2_wholesalers_id as a')->get()->result_array();
                $customers = array_merge($customers, $distributors, $wholesalers);
                if($customers){
                    foreach($customers as &$c)
                    {
                        $c = $c['a'];
                    }
                    $customers = implode('","', $customers);
                }
                else
                {
                    $customers = 'armstrong_2_customers_id';
                }
                //end fix
                $conditions['c.armstrong_2_customers_id IN ("' . $customers . '")'] = null;

                /*$salespersons = $this->db->select('armstrong_2_salespersons_id')
                    ->from('salespersons')
                    ->where('salespersons_manager_id', $user['id'])
                    ->get()
                    ->result_array();

                $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
                $salespersons_new = implode("\",\"", array_merge(array($user['id']), array_keys($salespersons)));

                if ($salespersons_new) {
    //                $conditions = array_merge(array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id IN ("'.$salespersons_new.'")' => null ), $filter);
                    $conditions['c.armstrong_2_salespersons_id IN ("' . $salespersons_new . '")'] = null;
                }*/
            }
        }
       // $datatables = new Datatable(array('model' => 'Contacts_dt', 'rowIdCol' => $this->contacts_m->getTablePrimary()));
        $datatables = new Datatable(array('model' => 'Contacts_dt'));

        $this->contacts_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {

            $id = $_data['c']['id'];
            $_data['id'] = '
                <a href="' . site_url('ami/contacts/edit/' . $_data['c']['id']) . '" data-toggle="ajaxModal">
                    ' . $_data['c']['id'] . '
                </a>
            ';
            $_data['c']['primary_contact'] = ($_data['c']['primary_contact'] == 1) ? 'Yes' : 'No';
            $_data['buttons'] = '<div class="btn-group">';

            $_data['buttons'] .= html_btn(site_url('ami/contacts/amended/' . $id), '<i class="fa fa-history"></i>', array('class' => 'btn-default amended', 'title' => 'Amended', 'data-toggle' => 'ajaxModal'));

            if ($this->hasPermission('edit', 'contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/contacts/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/contacts/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';

            if ($_data['c']['signature_base64']) {
                $_data['c']['signature_base64'] = '&#9989;';
            }
        }

        return $this->json($data);
    }


    public function draft()
    {
        $conditions = array('contacts.country_id' => $this->country_id, 'contacts.is_draft' => 1);

        //$this->data['contacts'] = $this->contacts_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['contacts'] = $this->_getListContacts($conditions);
        $this->data['total'] = $this->contacts_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/contacts/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            // 'cuisine_channels' => $this->cuisine_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'products' => $this->products_m->getProductListOptions('- Select -', $conditions),
            'customers' => $this->customers_m->getCustomerListOptions(null, $conditions),
            'wholesalers' => $this->wholesalers_m->getSalerListOptions(null, array_merge(array('active' => 1), $conditions)),
            'distributors' => $this->distributors_m->getDistributorListOptions(null, array_merge(array('active' => 1), $conditions)),
        );
    }

    public function check_primary_contact()
    {
        $customers_id = $this->input->get('customers_id');
        $result = false;
        if (isset($customers_id)) {
            $customers_id = explode(' - ', $customers_id);
            $customers_id = $customers_id['0'];
            $primary_contact = $this->contacts_m->get_by(array(
                'armstrong_2_customers_id' => $customers_id,
                'primary_contact' => 1,
                'is_draft' => 0
            ));
            if (count($primary_contact) > 0) $result = true;
        }
        echo $result;
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'contacts')) {
            return $this->noPermission();
        }

        // $this->data += $this->_getAddEditParams();
        if ($this->input->get('armstrong_2_customers_id')) {
            $this->data['contacts']['armstrong_2_customers_id'] = $this->input->get('armstrong_2_customers_id');
        }
        $this->data['contacts']['status'] = 1;
        return $this->render('ami/contacts/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'contacts') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contacts');

        if ($id = intval($id)) {
            $this->data['contacts'] = $this->contacts_m->get($id);
            $this->data['contacts']['tag_id'] = $this->data['contacts']['armstrong_2_customers_id'];
            $this->data['images'] = $this->media_m->getMedia('image', 'contacts', $this->country_id, $id);
            $this->data['page_title'] = page_title('Edit Contacts');
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'contacts')) {
                    return $this->noPermission();
                }

                return $this->render('ami/contacts/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/contacts/edit', $this->data);
            }
        }

        return redirect('ami/contacts');
    }

    public function update()
    {
        if (!$this->hasPermission('edit', 'contacts')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->contacts_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/contacts');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'contacts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contacts');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/contacts';
                //$this->contacts_m->delete($id);
                $this->contacts_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/contacts/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Contacts"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/contacts');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'contacts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contacts');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/contacts/draft';
                $this->contacts_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/contacts/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Contacts"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/contacts');
    }

    public function amended($id = null)
    {
        $this->data['amendedData'] = $this->_getAmenedData($id);
        $this->data['contactId'] = $id;
        return $this->render('ami/contacts/amended', $this->data);
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'contacts') && !$this->hasPermission('edit', 'contacts')) {
            return $this->noPermission();
        }
        $this->is('POST');
        $fields = array('ext', 'name','first_name','last_name', 'position', 'phone', 'phone_2', 'phone_3', 'mobile', 'mobile_2', 'mobile_3', 'status', 'primary_contact', 'armstrong_2_customers_id',
            'tag_id', 'fax', 'email', 'email_2', 'birthday', 'interests', 'remarks', 'id', 'facebook', 'instagram', 'linkedln', 'signature_base64', 'tea_relevant', 'opt_in_type', 'opt_in', 'check_email', 'check_mobile', 'paper_material', 'online_campaign', 'any_other_format');
        $data = $this->contacts_m->array_from_post($fields);

        $data['country_id'] = $this->country_id;
        // $data['birthday'] = date("Y-m-d", strtotime($data['birthday']));
//        $data['birthday'] = Carbon::parse($data['birthday'])->toDateString();

        $id = $data['id'] ? $data['id'] : null;
        // Only MY can change signature
        if ($this->country_id != 1) {
            unset($data['signature_base64']);
        }
        if (!empty($data['tag_id'][0])) {
            $data['armstrong_2_customers_id'] = $data['tag_id'][0];
            $data['type'] = 0;
        } else if (!empty($data['tag_id'][1])) {
            $data['armstrong_2_customers_id'] = $data['tag_id'][1];
            $data['type'] = 1;
        } else if (!empty($data['tag_id'][2])) {
            $data['armstrong_2_customers_id'] = $data['tag_id'][2];
            $data['type'] = 2;
        }
        unset($data['tag_id']);
        // Convert string to uppercase
        foreach ($data as $key => $_data) {
            if (is_string($_data) && $key != 'signature_base64')
                $data[$key] = strtoupper($_data);
        }
        // Update primary contact
        if ($data['primary_contact'] == 1) {
            if ($id) {
                $condition = array('armstrong_2_customers_id' => $data['armstrong_2_customers_id'], "id != {$id}" => null);
            } else {
                $condition = array('armstrong_2_customers_id' => $data['armstrong_2_customers_id']);
            }
            $sameContact = $this->contacts_m->get_by($condition, false, null, '*', null);
            if ($sameContact) {
                foreach ($sameContact as $_contact) {
                    $newId = $this->contacts_m->save(array('primary_contact' => 0), $_contact['id']);
                }
            }
        }

        // Add Amended data for HK
        if ($this->country_id == 3) {
            if ($id) {
                $contact = $this->contacts_m->get($id);
                $oldData = $newData = array();
                unset($contact['log_ids']);
                unset($contact['date_created']);
                unset($contact['last_updated']);
                unset($contact['date_sync']);
                unset($contact['is_draft']);
                foreach ($fields as $field) {
                    if ($field == 'tag_id') {
                        continue;
                    }
                    if ($contact[$field] != $data[$field]) {
                        $oldData[$field] = $contact[$field];
                        $newData[$field] = $data[$field];
                    }
                }

                if ($oldData && $newData) {
                    $this->db->where('contact_id', $id)->delete($this->contacts_amended_m->getTableName());

                    $this->contacts_amended_m->save(array(
                        'contact_id' => $id,
                        'armstrong_2_customers_id' => $contact['armstrong_2_customers_id'],
                        'contact_old_data' => json_encode($oldData),
                        'contact_new_data' => json_encode($newData),
                        'country_id' => $this->country_id
                    ));
                }

                // Send mail
                $user = $user = $this->session->userdata('user_data');
                $roles_admin = $this->roles_m->getRoleIdByRolename($this->country_id, 'Admin');
                $amenData = $this->_getAmenedData($id);
                if ($amenData) {
                    if (!in_array($user['type'], array('SL', 'ADMIN'))) {
                        $salesLeaderID = $this->db->select('salespersons_manager_id')
                            ->from('salespersons')
                            ->where(array('armstrong_2_salespersons_id' => $user['id']))
                            ->get()->row();
                        if ($salesLeaderID) {
                            $salesLeaderID = $this->convertToArray($salesLeaderID);
                            $salesLeader = $this->db->from('salespersons')
                                ->where(array('armstrong_2_salespersons_id' => $salesLeaderID['salespersons_manager_id']))
                                ->get()->row();
                            if ($salesLeader) {
                                $salesLeader = $this->convertToArray($salesLeader);
                                if ($salesLeader['type'] == 'Admin' || ($salesLeader['type'] == 'SL' && $salesLeader['sub_type'] == 'asm')) {
                                    $to = ($salesLeader['email']) ? $salesLeader['email'] : '';
                                } else {
                                    $to = '';
                                }
                            }
                        }
                    } elseif ($roles_admin == $user['roles_id'] || ($user['type'] == 'SL' && $user['sub_type'] == 'asm')) {
                        $to = ($user['email']) ? $user['email'] : '';
                    }
                    $customer_table = $this->_getCustomerTable($amenData['armstrong_2_customers_id']);
                    $customer = $this->db->from($customer_table['table'])
                        ->where(array($customer_table['id_key'] => $amenData['armstrong_2_customers_id']))
                        ->get()->result_array();
                    $primary_contact = $data['primary_contact'] == 1 ? 'Yes' : 'No';
                    $change_fields = $amenData['fields'];
                    $this->mail->from('No-Reply-Orders@ufs-app.com', "UFS " . ($this->country_code));
                    $subject = 'Edited Contact - ' . date('Y-m-d H:i:s');
                    $cc = null;
                    $bcc = 'conghc.dev@gmail.com';
                    $body = 'Your Contact ' . $data['name'] . ' has been edited.<br/><br/>';
                    $body .= 'Operator ID: ' . $amenData['armstrong_2_customers_id'] . '<br/>';
                    $body .= 'Customer Name: ' . $customer[0][$customer_table['name_key']] . '<br/>';
                    $body .= 'Primary contact: ' . $primary_contact . '<br/><br/>';
                    $body .= '<table style="border-collapse: collapse; border: 1px solid black;">';
                    $body .= '<thead>
								<tr>
									<th style="border: 1px solid black; padding: 5px; height: 50px">Fields</th>
									<th style="border: 1px solid black; padding: 5px; height: 50px">Old Data</th>
									<th style="border: 1px solid black; padding: 5px; height: 50px">New Data</th>
								</tr>
							 </thead>';
                    $body .= '<tbody>';
                    foreach ($change_fields as $_field) {
                        $body .= '<tr>
									<td style="border: 1px solid black; padding: 5px; height: 20px;">' . $_field . '</td>
									<td style="border: 1px solid black; padding: 5px; height: 20px;">' . $amenData['old_data'][$_field] . '</td>
									<td style="border: 1px solid black; padding: 5px; height: 20px;">' . $amenData['new_data'][$_field] . '</td>
								  </tr>';
                    }
                    $body .= '</tbody>';
                    $body .= '</table>';
                    $this->mail->sendMail($to, $subject, $body, $cc, $bcc);
                }
            }
        }
        $newId = $this->contacts_m->save($data, $id);
        // Update existing record
        if ($id) {

            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('contacts', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');

        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'contacts',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'contacts';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);
                }
            }
        }

        return redirect('ami/contacts');
    }

    public function primary($id = null)
    {
        $customerId = $this->input->get('armstrong_2_customers_id');

        $this->_assertId($id, 'ami/customers');

        if (!$customerId) {
            return redirect(site_url('ami/customers/contacts/' . $customerId));
        }

        $this->db->where('armstrong_2_customers_id', $customerId)
            ->set(array('primary_contact' => 0))
            ->update('contacts');

        $this->contacts_m->save(array(
            'primary_contact' => 1
        ), $id);

        return redirect(site_url('ami/customers/contacts/' . $customerId));
    }

    public function _getAmenedData($contactId)
    {
        $data = $this->db->where(array(
            'country_id' => $this->country_id,
            'contact_id' => $contactId,
            'is_draft' => 0
        ))
            ->get('contacts_amended_data')->row_array();

        if ($data) {
            $data['old_data'] = !empty($data['contact_old_data']) ? json_decode($data['contact_old_data'], true) : array();
            $data['new_data'] = !empty($data['contact_new_data']) ? json_decode($data['contact_new_data'], true) : array();

            $data['fields'] = $data['old_data'] ? array_keys($data['old_data']) : array();
        }

        return $data;
    }
}