<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ssd extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */

    protected static $_ssdGroup = false;
    protected $datasets = [];
    private $filelog = '';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ssd_m');
        $this->load->model('ssd_push_m');
        $this->load->model('salespersons_m');
        $this->load->model('customers_m');
        $this->load->model('wholesalers_m');
        $this->load->model('distributors_m');
        $this->load->model('call_records_m');
        $this->load->model('products_m');
        $userc = $this->session->userdata('user_data');
        $this->filelog = 'res/up/ssd_import_logs/'.$userc['id'].'.json';
        $this->data['ssd_status'] = array(
            '1' => 'Approved',
            '0' => 'Pending',
            '-1' => 'Rejected',
            '2' => 'Pending when edit',
        );
    }

    protected function _getSsdList()
    {
        $this->db->select('*');
        $this->db->from('ssd');
        $this->db->left_join('products', 'ssd.sku_number = products.sku_number');
        $this->db->where(array(
            'ssd.country_id' => $this->country_id,
            'ssd.is_draft' => 0,
            'products.country_id' => $this->country_id,
            'status' => in_array($this->country_code, array('tw')) ? $this->_getFilter() : 1
        ));

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $this->db->limit($total, $offset);

        $ssd = $this->db->get()->result_array();
        return $ssd;
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'pending':
                $filter = 0;
                break;

            default:
                $filter = 1;
                break;
        }

        return $filter;
    }

    public function index()
    {
        if(file_exists($this->filelog))
            unlink($this->filelog);
        if (!$this->hasPermission('view', 'ssd')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }

        // $conditions['status'] = (in_array($this->country_code, array('tw')) && $this->roleName == 'Sales Manager') ? $this->_getFilter() : 1;  

        // $this->data['ssd'] = $this->ssd_m->get_limit($this->getPageLimit(), $conditions);   
        // $this->data['total'] = $this->ssd_m->count('where', $conditions);
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons')) {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = false;
        // $this->data['filter'] = (in_array($this->country_code, array('tw')) && $this->roleName == 'Sales Manager') ? $this->_getFilter() : false;
        $this->data['page_title'] = page_title('Ssd');

        return $this->render('ami/ssd/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'ssd')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        // $conditions['status'] = (in_array($this->country_code, array('tw')) && $this->roleName == 'Sales Manager') ? $this->_getFilter() : 1;

        // $this->data['ssd'] = $this->ssd_m->get_limit($this->getPageLimit(), $conditions); 
        // $this->data['total'] = $this->ssd_m->count('where', $conditions);
        // $this->data['salespersons'] = array_merge(array(
        //     '' => ' - Select - ',
        //     'ALL' => 'ALL'
        // ), $this->salespersons_m->getPersonListOptions(null, $conditions));
        $defaultOptions = array('' => ' - Select - ');

        if ($this->hasPermission('manage', 'salespersons')) {
            $defaultOptions['ALL'] = 'ALL';
        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        // $this->data['filter'] = (in_array($this->country_code, array('tw')) && $this->roleName == 'Sales Manager') ? $this->_getFilter() : false;
        $this->data['draft'] = true;

        return $this->render('ami/ssd/index', $this->data);
    }

    public function pending()
    {
        if (!in_array($this->country_code, array('tw'))) {
            return redirect('ami/ssd');
        }

        // if (!$this->hasPermission('edit', 'ssd'))
        // {
        //     return $this->noPermission();
        // }

        $user = $this->session->userdata('user_data');

        $salespersons = $this->db->select('armstrong_2_salespersons_id')
            ->from('salespersons')
            ->where('salespersons_manager_id', $user['id'])
            ->get()
            ->result_array();

        $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');

        // $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'status' => 0, 'ssd_group' => '');

        // $this->data['ssd'] = $this->ssd_m->get_by($conditions, false);
        // $this->data['ssd'] = !empty($salespersons)
        //     ? $this->db->from($this->ssd_m->getTableName())
        //     ->where($conditions)
        //     ->where_in('armstrong_2_salespersons_id', array_keys($salespersons))
        //     ->get()
        //     ->result_array()
        //     : array();

        $ssdTable = $this->ssd_m->getTableName();
        $salespersonsTable = $this->salespersons_m->getTableName();
        $customersTable = $this->customers_m->getTableName();

        $conditions = array(
            // 'armstrong_2_salespersons_id' => $id, 
            "{$ssdTable}.is_draft" => 0,
            "{$ssdTable}.country_id" => $this->country_id,
            "{$ssdTable}.status" => 0,
            "{$ssdTable}.ssd_group" => 1
        );

        $this->data['ssd'] = !empty($salespersons)
            ? $this->db->select("{$ssdTable}.*, 
                {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                {$customersTable}.armstrong_2_customers_name
            ")
                ->from($ssdTable)
                ->join($salespersonsTable, "{$ssdTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                ->join($customersTable, "{$ssdTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id", 'left')
                ->where_in("{$ssdTable}.armstrong_2_salespersons_id", array_keys($salespersons))
                ->where($conditions)
                ->get()->result_array()
            : array();
        return $this->render('ami/ssd/pending');
    }

    protected function _getAddEditParams()
    {
        // default conditions
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata('user_data');
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, false, null, array('admins_id', 'armstrong_2_salespersons_id'));
        // check type role of user
        if ($user['roles_id'] == $roles_salesmanager) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id'], '(type like "%PULL%" OR type like "%MIX%" OR type like "%TRAINER%")' => null);
        } else if ($user['roles_id'] == $roles_salesperson) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id'], '(type like "%PULL%" OR type like "%MIX%" OR type like "%TRAINER%")' => null);
        }
        //$conditions_sales['type'] = '\'PULL\' or \'MIX\'';
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            // 'customers' => $this->customers_m->getCustomerListOptions(null, $conditions),
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true),
            'wholesalers' => $this->wholesalers_m->getSalerListOptions(null, array_merge(array('active' => 1), $conditions)),
            'call_records' => $this->call_records_m->parse_form(array('armstrong_2_call_records_id', 'armstrong_2_call_records_id'), '- Select -', $conditions),
            'distributors' => $this->distributors_m->getDistributorListOptions(null, array_merge(array('active' => 1), $conditions)),
            'ssd_method' => $this->ssd_m->getMethodListOptions(),
            'products' => array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'ssd')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $now = Carbon::now();
        $this->data['ssd']['ssd_month'] = $now->format('n');
        $this->data['ssd']['ssd_year'] = $now->format('Y');
        // $this->data['customers'] = array('' => ' - Select - ');
        return $this->render('ami/ssd/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'ssd') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/ssd');

        if ($id = intval($id)) {
            $this->data['ssd'] = $ssd = $this->ssd_m->get($id);
            $this->assertCountry($this->data['ssd']);
            $this->data['page_title'] = page_title('Edit Ssd');
            $this->data['pending'] = $this->input->get('pending');
            $this->data += $this->_getAddEditParams();
            $this->data['ssdGroups'] = $this->ssd_m->get_by(array(
                'armstrong_2_ssd_id' => $this->data['ssd']['armstrong_2_ssd_id'],
                'country_id' => $this->country_id,
                'is_draft' => 0
            ));
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'ssd')) {
                    return $this->noPermission();
                }

                $this->data['ssd']['product'] = $ssd['sku_number'] ? $this->products_m->get($ssd['sku_number']) : array();
                $this->data['ssd']['customer'] = $ssd['armstrong_2_customers_id'] ? $this->customers_m->get($ssd['armstrong_2_customers_id']) : array();
                $this->data['ssd']['salesperson'] = $ssd['armstrong_2_salespersons_id'] ? $this->salespersons_m->get($ssd['armstrong_2_salespersons_id']) : array();
                $this->data['ssd']['wholesaler'] = $ssd['armstrong_2_wholesalers_id'] ? $this->wholesalers_m->get($ssd['armstrong_2_wholesalers_id']) : array();
                $this->data['ssd']['distributor'] = $ssd['armstrong_2_distributors_id'] ? $this->distributors_m->get($ssd['armstrong_2_distributors_id']) : array();
                $this->data['ssd']['call_record'] = $ssd['armstrong_2_call_records_id'] ? $this->call_records_m->get($ssd['armstrong_2_call_records_id']) : array();
                $this->data['methods'] = $this->ssd_m->getMethodListOptions();

                $this->data['total'] = $this->data['ssd']['total_price'];

                /*foreach ($this->data['ssdGroups'] as $ssdGroup) {
                    $this->data['total'] += $ssdGroup['total_price'];
                }*/

                return $this->render('ami/ssd/preview', $this->data);
            } else {
                $this->data['customers'] = $this->customers_m->getCustomerListOptions(null, array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    'armstrong_2_salespersons_id' => $ssd['armstrong_2_salespersons_id'],
                    'approved' => 1
                ));

                $this->data['products_array'] = array();
                if (strlen($this->data['ssd']['sku_number']) > 0) {
                    $this->data['products_array'][$this->data['ssd']['sku_number']] = $this->data['ssd'];
                    $this->data['products_array'] += array_rewrite($this->data['ssdGroups'], 'sku_number');
                }
                return $this->render('ami/ssd/edit', $this->data);
            }
        }


        return redirect('ami/ssd');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'ssd')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->ssd_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/ssd');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'ssd')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/ssd');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/ssd';
                //$this->ssd_m->delete($id);
                $this->ssd_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/ssd/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Ssd"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/ssd');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'ssd')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/ssd');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/ssd/draft';
                $this->ssd_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/ssd/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Ssd"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/ssd');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'ssd') && !$this->hasPermission('edit', 'ssd')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->ssd_m->array_from_post(array('armstrong_2_customers_id', 'armstrong_2_distributors_id', 'armstrong_2_salespersons_id',
            'status', 'armstrong_2_customers_id', 'armstrong_2_wholesalers_id', 'armstrong_2_call_records_id', /*'sku_number', 'qty_cases', 'qty_pcs',
            'free_qty_cases', 'free_qty_pcs', 'total_price',*/
            'method', 'ssd_date', 'ssd_month', 'ssd_year', 'date_sync', 'remarks', 'id', 'is_consolidated_order'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        // $data['total_price'] = floatval(str_replace(',', '', $data['total_price']));

        // create new record
        if (!$id) {
            $data['status'] = 1;
            $products = $this->input->post('products');

            foreach ($products as $product) {
                if (in_array($this->country_code, array('tw'))) {
                    $data['status'] = 0;
                }

                $product['total_price'] = floatval(str_replace(',', '', $product['total_price']));
                $data['sku_number'] = $product['sku_number'];
                $data['qty_cases'] = $product['qty_cases'];
                $data['qty_pcs'] = $product['qty_pcs'];
                $data['free_qty_cases'] = $product['free_qty_cases'];
                $data['free_qty_pcs'] = $product['free_qty_pcs'];
                $data['total_price'] = $product['total_price'];

                if (!self::$_ssdGroup) {
                    $data['ssd_group'] = 1;
                    $newId = $this->ssd_m->save($data);
                    if($newId){
                        $ssd_id = generateSsdId($newId, $this->country_code);
                        $this->ssd_m->save(array('armstrong_2_ssd_id' => $ssd_id), $newId);
                    }
                    self::$_ssdGroup = $ssd_id;
                }else{
                    $data['ssd_group'] = 0;
                    $data['armstrong_2_ssd_id'] = self::$_ssdGroup;
                    $newId = $this->ssd_m->save($data);
                }
            }
        } else {
            $updateData = $this->getInput(array('ssd_update', 'ssd_delete'));
            $ssd_group_id = generateSsdId($id, $this->country_code);
            if (!empty($updateData['ssd_delete'])) {
                $this->ssd_m->update_in(array('is_draft' => 1), 'id', $updateData['ssd_delete']);
            }
//            dd($updateData['ssd_update']);
            foreach ($updateData['ssd_update'] as $ssdId => $_data) {
                $_data = array_merge($data, $_data);
                $_data['total_price'] = floatval(str_replace(',', '', $_data['total_price']));
                if ($_data['id']) {
                    $newId = $this->ssd_m->save($_data, $ssdId);
                } else {
                    $_data['ssd_group'] = 0;
                    $_data['armstrong_2_ssd_id'] = $ssd_group_id;
                    $newId = $this->ssd_m->save($_data);
                }
            }
        }

        $redirect = $this->input->post('pending') ? 'ami/ssd/pending' : 'ami/ssd';

        return redirect($redirect);
    }

    public function approve_all()
    {
        if ($this->roleName != 'Sales Manager') {
            return $this->noPermission();
        }

        $ids = $this->input->post('ids');
        $response = array(
            'success' => false,
            'message' => 'Can not save'
        );

        if ($ids) {
            foreach($ids as $id)
            {
                $this->ssd_m->approvessd($id);
            }
           // $ssdGroup = $this->ssd_m->update_in(array('status' => 1), 'ssd_group', $ids);
            $response = array(
                'success' => true,
                'message' => 'Saved'
            );
        }

        return $this->json($response);
    }

    public function approve($id = null)
    {
        if (!in_array($this->country_code, array('tw'))) {
            return show_404();
        }

        // if (!$this->hasPermission('edit', 'ssd'))
        if ($this->roleName != 'Sales Manager') {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/ssd');

        if ($id) {
            $this->data['ssd'] = $ssd = $this->ssd_m->get($id);
            $this->assertCountry($this->data['ssd']);
            $this->data['page_title'] = page_title('Approve SSD');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                $this->data['ssd']['product'] = $ssd['sku_number'] ? $this->products_m->get($ssd['sku_number']) : array();
                $this->data['ssd']['customer'] = $ssd['armstrong_2_customers_id'] ? $this->customers_m->get($ssd['armstrong_2_customers_id']) : array();
                $this->data['ssd']['salesperson'] = $ssd['armstrong_2_salespersons_id'] ? $this->salespersons_m->get($ssd['armstrong_2_salespersons_id']) : array();
                $this->data['ssd']['wholesaler'] = $ssd['armstrong_2_wholesalers_id'] ? $this->wholesalers_m->get($ssd['armstrong_2_wholesalers_id']) : array();
                $this->data['ssd']['distributor'] = $ssd['armstrong_2_distributors_id'] ? $this->distributors_m->get($ssd['armstrong_2_distributors_id']) : array();
                $this->data['ssd']['call_record'] = $ssd['armstrong_2_call_records_id'] ? $this->call_records_m->get($ssd['armstrong_2_call_records_id']) : array();

                return $this->render('ami/ssd/pending_action', $this->data);
            } else if ($this->is('POST')) {
//                $this->ssd_m->save(array(
//                    'status' => 1
//                ), $id);
               // $this->ssd_m->update_in(array('status' => 1), 'ssd_group', array($id));
                $this->ssd_m->approvessd($id);
                //$this->ssd_m->update_in(array('status' => 1), 'armstrong_2_ssd_id', array($this->data['ssd']['armstrong_2_ssd_id']));
            }
        }

        return redirect('ami/ssd/pending');
    }

    public function reject($id = null)
    {
        if (!in_array($this->country_code, array('tw'))) {
            return show_404();
        }

        // if (!$this->hasPermission('edit', 'ssd'))
        if ($this->roleName != 'Sales Manager') {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/ssd');

        if ($id) {
            $this->data['ssd'] = $ssd = $this->ssd_m->get($id);
            $this->assertCountry($this->data['ssd']);
            $this->data['page_title'] = page_title('Reject SSD');

            if ($this->input->is_ajax_request()) {
                $this->data['ssd']['product'] = $ssd['sku_number'] ? $this->products_m->get($ssd['sku_number']) : array();
                $this->data['ssd']['customer'] = $ssd['armstrong_2_customers_id'] ? $this->customers_m->get($ssd['armstrong_2_customers_id']) : array();
                $this->data['ssd']['salesperson'] = $ssd['armstrong_2_salespersons_id'] ? $this->salespersons_m->get($ssd['armstrong_2_salespersons_id']) : array();
                $this->data['ssd']['wholesaler'] = $ssd['armstrong_2_wholesalers_id'] ? $this->wholesalers_m->get($ssd['armstrong_2_wholesalers_id']) : array();
                $this->data['ssd']['distributor'] = $ssd['armstrong_2_distributors_id'] ? $this->distributors_m->get($ssd['armstrong_2_distributors_id']) : array();
                $this->data['ssd']['call_record'] = $ssd['armstrong_2_call_records_id'] ? $this->call_records_m->get($ssd['armstrong_2_call_records_id']) : array();

                return $this->render('ami/ssd/pending_action', $this->data);
            } else if ($this->is('POST')) {
//                $this->ssd_m->save(array(
//                    'status' => -1
//                ), $id);
//                $this->ssd_m->update_in(array('status' => -1), 'ssd_group', array($id));
                $this->ssd_m->approvessd($id);
              //  $this->ssd_m->update_in(array('status' => -1), 'armstrong_2_ssd_id', array($this->data['ssd']['armstrong_2_ssd_id']));
            }
        }

        return redirect('ami/ssd/pending');
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'ssd')
        ) {
            return redirect(site_url('ami/ssd'));
        }

        $ssd = $this->ssd_m->get_by(array('country_id' => $this->country_id));

        // $ssd = $this->ssd_m->get_limit(20, array('country_id' => $this->country_id));


        $sheet = $this->makeSheet($ssd);

        return $this->excel($sheet, 'ssd', $type);
    }

    private $_ssd_column = array(
        "A" => 'group',
        "B" => 'armstrong_2_salespersons_id',
        "C" => 'salespersons',
        "D" => 'armstrong_2_customers_id',
        "E" => 'customers_name',
        "F" => 'armstrong_2_distributors_id',
        "G" => 'distributors_name',
        "H" =>'armstrong_1_wholesalers_id',
        "I" =>'armstrong_2_wholesalers_id',
        "J" =>'wholesalers_name',
        "K" =>'armstrong_2_call_records_id',
        "L" => 'sku_number',
        "M" => 'sku_name',
        "N" =>'packing_size',
        "O" => 'qty_cases',
        "P" => 'qty_pcs',
        "Q" =>'free_qty_cases',
        "R" =>'free_qty_pcs',
        "S" => 'total_price',
        "T" =>'invoice_number',
        "U" =>'method',
        "V" => 'ssd_month',
        "W" => 'ssd_year',
        "X" => 'ssd_date',
        "Y" =>'date_created',
        "Z" =>'delivery_date',
        "AA" =>'remarks',
        "AB" =>'new_sku',
        "AC" =>'status',
        "AD" =>'business_type',
        "AE" => 'country_id',
    );
    public $_ssdFieds = array(
        'id' => "",
        'armstrong_2_ssd_id' => "",
        'ssd_group' => "",
        'armstrong_1_distributors_id' => "",
        'armstrong_2_distributors_id' => "",
        'armstrong_1_wholesalers_id' => "",
        'armstrong_2_wholesalers_id' => "",
        'armstrong_1_customers_id' => "",
        'armstrong_2_customers_id' => "",
        'armstrong_2_salespersons_id' => "",
        'armstrong_2_call_records_id' => "",
        'sku_number' => "",
        'sku_name' => "",
        'qty_cases' => "",
        'qty_pcs' => "",
        'free_qty_cases' => "",
        'free_qty_pcs' => "",
        'total_price' => "",
        'quantity_case' => "",
        'invoice_number' => "",
        'method' => "",
        'ssd_month' => "",
        'ssd_year' => "",
        'ssd_date' => "",
        'date_created' => "",
        'delivery_date' => "",
        'remarks' => "",
        'new_sku' => "",
        'status' => "",
        'country_id' => "",
        'last_updated' => "",
        'date_sync' => "",
        'time_zone' => "",
        'gmt' => "",
        'is_draft' => "",
        'log_ids' => "",
        'processing' => "",
        'armstrong_2_tfo_id'=> "",
    );

    private $_salespersons_exist = array();
    private $_customers_exist = array();
    private $_distributors_exist = array();
    private $_wholesalers_1_exist = array();
    private $_wholesalers_2_exist = array();
    private $_armstrong_1_customers = array();
    private $_ssd_customer = array();

    public function checkDataImportSsd($data)
    {
        $conditions = array(
            'armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id'],
            'armstrong_2_customers_id' => $data['armstrong_2_customers_id'],
            '(armstrong_2_distributors_id = "'.$data['armstrong_2_distributors_id'].'" OR armstrong_1_wholesalers_id = "'.$data['armstrong_1_wholesalers_id'].'" OR armstrong_2_wholesalers_id = "'.$data['armstrong_2_wholesalers_id'].'")' => NULL,
            'ssd_date' => $data['ssd_date']
        );
        $ssd = $this->db
            ->from('ssd')
            ->where($conditions)
            ->get()->result_array();
        return $ssd;
    }
    public function validDateTime(&$datetime){
        $datetime = str_replace('000', '00', str_replace('  ', ' ', trim($datetime)));
        $datetime = str_replace('AM', '', str_replace('  ', ' ', trim($datetime)));
        $datetime = str_replace('PM', '', str_replace('  ', ' ', trim($datetime)));
        $datetime = $datetime ? date('Y-m-d H:i:s', strtotime(trim($datetime))) : date('Y-m-d H:i:s');
    }
    public function validationDataSsd(&$data)
    {
        $flag = false;
        foreach($data as $d)
        {
            if($d != '')
            {
                $flag = true;
            }
        }
        if(!$flag) return $flag;
        $data['ssd_group'] = 0;
        $data['qty_cases'] = isset($data['qty_cs']) ? (int)$data['qty_cs'] : 0;
        $data['qty_pcs'] = isset($data['qty_pcs']) ? (int)$data['qty_pcs'] : 0;
        $data['free_qty_cases'] = isset($data['free_qty_cases']) ? (int)$data['free_qty_cases'] : 0;
        $data['total_price'] = isset($data['total_price']) ? (float)$data['total_price'] : 0;
        $data['ssd_month'] = isset($data['ssd_month']) ? (int)$data['ssd_month'] :  date('m');
        $data['ssd_year'] = isset($data['ssd_year']) ? (int)$data['ssd_year'] : date('Y');
        $data['country_id'] = isset($data['country_id']) ? (int)$data['country_id'] : $this->country_id;
        if(!$data['status']) $data['status'] = 1;
        $this->validDateTime($data['ssd_date']);
        $this->validDateTime($data['date_created']);
        $this->validDateTime($data['delivery_date']);
        //#1662 mapping customer 2 id
        //step 1: check by armstrong_1_customers_id in table customers distributors wholesalers
        if(!(isset($data['armstrong_2_customers_id']) && $data['armstrong_2_customers_id'] != ''))
        {
            if(isset($data['armstrong_1_customers_id']) && $data['armstrong_1_customers_id'] != '')
            {
                if(isset($this->_armstrong_1_customers[$data['armstrong_1_customers_id']]))
                {
                    $data['armstrong_2_customers_id'] = $this->_armstrong_1_customers[$data['armstrong_1_customers_id']];
                }
                else
                {
                    if($customer = $this->db
                        ->from('customers')
                        ->where(array(
                            'armstrong_1_customers_id' => $data['armstrong_1_customers_id'],
                        ))
                        ->get()->row())
                    {
                        $data['armstrong_2_customers_id'] = $customer->armstrong_2_customers_id;
                        $this->_armstrong_1_customers[$data['armstrong_1_customers_id']] = $data['armstrong_2_customers_id'];
                    }
                    else if($distributors = $this->db
                        ->from('distributors')
                        ->where(array(
                            'armstrong_1_distributors_id' => $data['armstrong_1_customers_id'],
                        ))
                        ->get()->row())
                    {
                        $data['armstrong_2_customers_id'] = $distributors->armstrong_2_distributors_id;
                        $this->_armstrong_1_customers[$data['armstrong_1_customers_id']] = $data['armstrong_2_customers_id'];
                    }
                    else if($wholesalers= $this->db
                        ->from('wholesalers')
                        ->where(array(
                            'armstrong_1_wholesalers_id' => $data['armstrong_1_customers_id'],
                        ))
                        ->get()->row())
                    {
                        $data['armstrong_2_customers_id'] = $wholesalers->armstrong_2_wholesalers_id;
                        $this->_armstrong_1_customers[$data['armstrong_1_customers_id']] = $data['armstrong_2_customers_id'];
                    }
                }
            }
        }
        //step 2 if not find in step 1: check by table ssd_mapping ssd_id
        if(!(isset($data['armstrong_2_customers_id']) && $data['armstrong_2_customers_id'] != ''))
        {
            if(isset($data['armstrong_2_ssd_id']) && $data['armstrong_2_ssd_id'] != '')
            {
                if(isset($this->_ssd_customer[$data['armstrong_2_ssd_id']]))
                {
                    $data['armstrong_2_customers_id'] = $this->_ssd_customer[$data['armstrong_2_ssd_id']];
                }
                else
                {
                    $ssd = $this->db
                        ->from('ssd_mapping')
                        ->where(array(
                            'armstrong_2_ssd_id' => $data['armstrong_2_ssd_id'],
                        ))
                        ->get()->row();
                    if($ssd)
                    {
                        $data['armstrong_2_customers_id'] = $ssd->armstrong_2_customers_id;
                        $this->_ssd_customer[$data['armstrong_2_ssd_id']] = $data['armstrong_2_customers_id'];
                    }
                }
            }
        }
        //end 1662

        if(!(isset($data['armstrong_2_salespersons_id']) && $data['armstrong_2_salespersons_id'] != ''))
        {
            $flag = false;
            $data['error_column'] = 'armstrong_2_salespersons_id';
            return $flag;
        }
        else
        {
            // TODO check salespersons and only check one time
            if(!array_key_exists($data['armstrong_2_salespersons_id'], $this->_salespersons_exist))
            {
                $check = $this->db
                    ->from('salespersons')
                    ->where(array(
                        'armstrong_2_salespersons_id' => $data['armstrong_2_salespersons_id'],
                      //  'country_id' => $data['country_id']
                    ))
                    ->get()->result_array();
                if (!$check) {
                    $this->_salespersons_exist[$data['armstrong_2_salespersons_id']] = false;
                    $data['error_column'] = 'armstrong_2_salespersons_id not exist';
                    return false;
                }
                else
                {
                    $this->_salespersons_exist[$data['armstrong_2_salespersons_id']] = true;
                }
            }
            else
            {
                if (!$this->_salespersons_exist[$data['armstrong_2_salespersons_id']]) {
                    $data['error_column'] = 'armstrong_2_salespersons_id not exist';
                    return false;
                }
            }
        }
        // TODO check customers
        if(!(isset($data['armstrong_2_customers_id']) && $data['armstrong_2_customers_id'] != ''))
        {
            $flag = false;
            $data['error_column'] = 'armstrong_2_customers_id';
            return $flag;
        }
        else
        {
            // TODO check customers
            if(!array_key_exists($data['armstrong_2_customers_id'], $this->_customers_exist))
            {
                $check = $this->db
                    ->from('customers')
                    ->where(array(
                        'armstrong_2_customers_id' => $data['armstrong_2_customers_id'],
                      //  'country_id' => $data['country_id']
                    ))
                    ->get()->result_array();
                if (!$check) {
                    $this->_customers_exist[$data['armstrong_2_customers_id']] = false;
                    $data['error_column'] = 'armstrong_2_customers_id not exist';
                    return false;
                }
                else
                {
                    $this->_customers_exist[$data['armstrong_2_customers_id']] = true;
                }
            }
            else
            {
                if (!$this->_customers_exist[$data['armstrong_2_customers_id']]) {
                    $data['error_column'] = 'armstrong_2_customers_id not exist';
                    return false;
                }
            }
        }
        /*
        if(isset($data['armstrong_2_distributors_id']) && $data['armstrong_2_distributors_id'] != '')
        {
            // TODO check distributors
            if(!array_key_exists($data['armstrong_2_distributors_id'], $this->_distributors_exist))
            {
                $check = $this->db
                    ->from('distributors')
                    ->where(array(
                        'armstrong_2_distributors_id' => $data['armstrong_2_distributors_id'],
                      //  'country_id' => $data['country_id']
                    ))
                    ->get()->result_array();
                if (!$check) {
                    $this->_distributors_exist[$data['armstrong_2_distributors_id']] = false;
                    $data['error_column'] = 'armstrong_2_distributors_id not exist';
                    return false;
                }
                else
                {
                    $this->_distributors_exist[$data['armstrong_2_distributors_id']] = true;
                }
            }
            else
            {
                if (!$this->_distributors_exist[$data['armstrong_2_distributors_id']]) {
                    $data['error_column'] = 'armstrong_2_distributors_id not exist';
                    return false;
                }
            }
        }
        else if(isset($data['armstrong_1_wholesalers_id']) && $data['armstrong_1_wholesalers_id'] != '')
        {
            // TODO check wholesalers
            if(!array_key_exists($data['armstrong_1_wholesalers_id'], $this->_wholesalers_1_exist))
            {
                $check = $this->db
                    ->from('wholesalers')
                    ->where(array(
                        'armstrong_1_wholesalers_id' => $data['armstrong_1_wholesalers_id'],
                       // 'country_id' => $data['country_id']
                    ))
                    ->get()->result_array();
                if (!$check) {
                    $this->_wholesalers_1_exist[$data['armstrong_1_wholesalers_id']] = false;
                    $data['error_column'] = 'armstrong_1_wholesalers_id not exist';
                    return false;
                }
                else
                {
                    $this->_wholesalers_1_exist[$data['armstrong_1_wholesalers_id']] = true;
                }
            }
            else
            {
                if (!$this->_wholesalers_1_exist[$data['armstrong_1_wholesalers_id']]) {
                    $data['error_column'] = 'armstrong_1_wholesalers_id not exist';
                    return false;
                }
            }
        }
        else if(isset($data['armstrong_2_wholesalers_id']) && $data['armstrong_2_wholesalers_id'] != '')
        {
            // TODO check wholesalers
            if(!array_key_exists($data['armstrong_2_wholesalers_id'], $this->_wholesalers_2_exist))
            {
                $check = $this->db
                    ->from('wholesalers')
                    ->where(array(
                        'armstrong_2_wholesalers_id' => $data['armstrong_2_wholesalers_id'],
                       // 'country_id' => $data['country_id']
                    ))
                    ->get()->result_array();
                if (!$check) {
                    $this->_wholesalers_2_exist[$data['armstrong_2_wholesalers_id']] = false;
                    $data['error_column'] = 'armstrong_2_wholesalers_id not exist';
                    return false;
                }
                else
                {
                    $this->_wholesalers_2_exist[$data['armstrong_2_wholesalers_id']] = true;
                }
            }
            else
            {
                if (!$this->_wholesalers_2_exist[$data['armstrong_2_wholesalers_id']]) {
                    $data['error_column'] = 'armstrong_2_wholesalers_id not exist';
                    return false;
                }
            }
        }
        else
        {
            $flag = false;
            $data['error_column'] = 'armstrong_2_distributors_id OR armstrong_1_wholesalers_id OR armstrong_2_wholesalers_id is require';
            return $flag;
        }
 */

        if(!(isset($data['sku_number']) && $data['sku_number'] != ''))
        {
            $flag = false;
            $data['error_column'] = 'sku_number';
            return $flag;
        }
        else
        {
            // TODO Get product name and caculate total price
            $product = $this->db
                ->from('products')
                ->where(array(
                    'sku_number' => $data['sku_number'],
                   // 'country_id' => $data['country_id']
                ))
                ->get()->result_array();
            if ($product) {
                $data['sku_name'] = $product[0]['sku_name'];
                if ($product[0]['price'] > 0) {
                    $total_price = ($product[0]['price'] * $data['qty_cases']) + ($product[0]['price'] / $product[0]['quantity_case'] * $data['qty_pcs']);
                } else {
                    $total_price = 0;
                }
                $data['total_price'] = $total_price;
            }
            else
            {
                $data['error_column'] = 'sku_number not exist';
                return false;
            }
        }
        // TODO check salespersons and only check one time
        if($ssd = $this->checkDataImportSsd($data))
        {
            foreach($ssd as $s)
            {
                if($data['sku_number'] == $s['sku_number'])
                {
                    $data['error_column'] = 'duplicate data';
                    return false;
                }
            }
            $data['armstrong_2_ssd_id'] = $ssd[0]['armstrong_2_ssd_id'];
        }

        return $flag;
    }
    public function setprogress($data){
        //$this->input->set_cookie($data);
        file_put_contents($this->filelog, json_encode($data));
    }
    public function getprogress(){
        if(file_exists($this->filelog))
        {
            echo file_get_contents($this->filelog);
        }
        else
        {
            echo json_encode(array(
                'progress' => true,
                'hightrow' => 100,
                'rowcurrent' => 0
            ));
        }
    }
    public  function unsetdata($data)
    {
        $ssd = array();
        foreach($this->_ssdFieds as $key => $value)
        {
            if(isset($data[$key]))
            {
                $ssd[$key] =  $data[$key];
            }
            else
            {
                $ssd[$key] =  $value;
            }
        }
        return $ssd;
    }
    public  function maxIdSsd(){
        $maxid = $this->db->query('SELECT MAX(id) AS `maxid` FROM `ssd`')->row()->maxid;
        return $maxid;
    }
    public function import()
    {
        if ($this->is('POST')) {
            if(file_exists($this->filelog))
                unlink($this->filelog);

            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $error_row = [];
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);
                    $highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
                    $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

                    $highestColumm++;
                    $ssd_group = '';

                    $ssd_group_ids = array();
                    //hieu import
                    $dataset = array();
                    $error_flag = false;
                    $datagroups = array();
                    //hieu file error

                    $nb_success = 0;
                    $nb_error = 0;
                    $file_name = 'res' . DIRECTORY_SEPARATOR . 'up' . DIRECTORY_SEPARATOR . 'ssd_import_error' . DIRECTORY_SEPARATOR .'ssd_error_' . time() . '.csv';
                    for ($row = 1; $row < $highestRow + 1; $row++) {
                        if($row == 1)
                        {
                            $column_exel = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO');
                            $this->_ssd_column = array();
                            foreach ($column_exel as $v) {
                                if($key = $objPHPExcel->getActiveSheet()->getCell($v . $row)->getFormattedValue())
                                {
                                    $this->_ssd_column[$v] = $key;
                                }
                            }
                        }
                        else
                        {
                            // TODO update progress
                            unset($datas);
                            $datas = array();
                            foreach ($this->_ssd_column as $column => $value) {
                                $datas[$value] = $objPHPExcel->getActiveSheet()->getCell($column . $row)->getFormattedValue();
                            }
                            $flag = false;
                            foreach($datas as $d)
                            {
                                if($d != '')
                                {
                                    $flag = true;
                                }
                            }
                            if($flag)
                            {
                                if ($this->validationDataSsd($datas)) {
                                    if(isset($datas['armstrong_2_ssd_id']) && $datas['armstrong_2_ssd_id'] != '') //them sku vao ssd
                                    {
                                        $datas = $this->unsetdata($datas);

                                        if(!$this->db->insert('ssd', $datas))
                                        {
                                            $datas['error_column'] = 'Error';
                                            $error_flag = true;
                                        }
                                    }
                                    else
                                    {
                                        $datas['ssd_group'] = 1;
                                        $ssd_id = generateSsdId($this->maxIdSsd() + 1, $this->country_code);
                                        $datas['armstrong_2_ssd_id'] = $ssd_id;
                                        $datas = $this->unsetdata($datas);

                                        if(!$this->db->insert('ssd', $datas))
                                        {
                                            $datas['error_column'] = 'Error';
                                            $error_flag = true;
                                        }
                                    }
                                }
                                else
                                {
                                    $error_flag = true;
                                }
                                $dataprogress = array('progress' => true, 'hightrow' => $highestRow, 'rowcurrent' => $row);
                                $this->setprogress($dataprogress);
                                 //wrire file error
                                if(isset($datas['error_column']))
                                {
                                    $nb_error++;
                                    $log_ms = "\n";
                                    foreach($datas as $value)
                                    {
                                        $log_ms .= $value.',';
                                    }
                                    if(!file_exists($file_name))
                                    {
                                        $log_title = '';
                                        foreach($datas as $key => $value)
                                        {
                                            $log_title .= $key.',';
                                        }
                                        file_put_contents($file_name, trim($log_title, ','), FILE_APPEND);
                                    }
                                    file_put_contents($file_name, $log_ms, FILE_APPEND);
                                }
                                else
                                {
                                    $nb_success++;
                                }
                            }
                        }
                    }
                    // Write new excel file contain error data
                    if ($error_flag) {
                        if(file_exists($this->filelog))
                            unlink($this->filelog);
                        // save error file
                     //   $file_name = 'ssd_error_' . time() . '.xlsx';
                      //  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    //    $objWriter->save('res' . DIRECTORY_SEPARATOR . 'up' . DIRECTORY_SEPARATOR . 'ssd_import_error' . DIRECTORY_SEPARATOR . $file_name);
                        return $this->json(array(
                            'success' => true,
                            'message' => 'Import complete',
                            'type' => 'ssd',
                            'file_error' => $_SERVER['HTTP_HOST'].'/armstrong/'.$file_name,
                            'nb_success' => $nb_success,
                            'nb_error' => $nb_error,
                        ));
                    }
                    if(file_exists($this->filelog))
                        unlink($this->filelog);

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function ssd_data()
    {
        if (!$this->hasPermission('view', 'ssd')) {
            return $this->noPermission();
        }


        $this->is('POST');

        if ($id = $this->input->post('id')) {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date'));

            $endDate->addDay();

            $ssdTable = $this->ssd_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();

            $conditions = array(
                "{$ssdTable}.is_draft" => $this->input->post('draft'),
                "{$ssdTable}.ssd_date >=" => $startDate->toDateTimeString(),
                "{$ssdTable}.ssd_date <=" => $endDate->toDateTimeString(),
                //"{$ssdTable}.date_created >=" => $startDate->toDateTimeString(),
                //"{$ssdTable}.date_created <=" => $endDate->toDateTimeString(),
                "{$ssdTable}.country_id" => $this->country_id,
//                "{$ssdTable}.status" => 1,
//                "{$customersTable}.approved IN (1,2)" => null,
                "{$ssdTable}.ssd_group" => 1
            );

            if ($id[0] == 'ALL') {
                $this->data['ssd'] = $this->db->select("{$ssdTable}.*,
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($ssdTable)
                    ->join($salespersonsTable, "{$ssdTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$ssdTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where($conditions)
                    ->get()->result_array();

            } else {
                $this->data['ssd'] = $this->db->select("{$ssdTable}.*,
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name					
                ")
                    ->from($ssdTable)
                    ->join($salespersonsTable, "{$ssdTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$ssdTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where_in("{$ssdTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();

            }
//            dd($this->db->last_query());
            $this->data['draft'] = $this->input->post('draft');
            return $this->render('ami/ssd/ssd', $this->data);
        }

        return '';
    }
}