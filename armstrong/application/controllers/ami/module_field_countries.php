<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Module_field_countries extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('modules_m');
        $this->load->model('module_fields_m');
        $this->load->model('module_field_countries_m');
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $news = $this->module_field_countries_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $news = $this->module_field_countries_m->prepareDataTables($news);

        return $this->datatables->generate($news);
    }

    protected function _getFilterModules()
    {
        $type = $this->input->get('filter');
        $modules = $this->modules_m->get_by(array('is_draft' => 0), false, false, 'id,name');
        foreach ($modules as $p) {
            if (strtolower($p['name']) == strtolower($type)) {
                return $p['name'];
                exit();
            }
        }
    }

    public function index()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('module_field_countries.country_id' => $this->country_id, 'module_field_countries.is_draft' => 0);

        $filter = $this->_getFilterModules();

        if ($filter !== null) {
            $conditions['module_fields.module LIKE'] = $this->_getFilterModules();
        }

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $data = $this->module_field_countries_m->get_limit($this->getPageLimit(), $conditions, 'last_updated desc', 'module_field_countries.*,module_fields.field,module_fields.module',
            array('0' => array('db' => 'module_fields',
                'query' => 'module_fields.id = module_field_countries.module_field_id',
                'type' => 'inner')));

        $this->data['total'] = $this->module_field_countries_m->count('where', $conditions,
            array('0' => array('db' => 'module_fields',
                'query' => 'module_fields.id = module_field_countries.module_field_id',
                'type' => 'inner')));
        $this->data['filter'] = $this->input->get('filter');
        $this->data['data'] = $data;
        $this->data['modules'] = $this->modules_m->get_by(array('is_draft' => 0), false, 'name asc', 'id,name');
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Module Field Countries');
        return $this->render('ami/module_field_countries/index', $this->data);
    }


    public function process()
    {

        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }
        $filter = $this->_getFilterModules();
        $sort_by = $this->input->get('sort_by');
        $order = $this->input->get('order');

        $conditions = array('is_draft' => 0);

        if ($filter == '' || $filter == NULL) {
            $conditions['module LIKE'] = 'customers';
        } else {
            $conditions['module LIKE'] = $filter;
        }


        $module_fields = $this->module_fields_m->get_by($conditions, false, 'field', 'id,module,field');

        $module_fields_ids = "(0";
        foreach ($module_fields as $k => $p) {
            $module_fields_ids .= ',' . $p['id'];
        }
        $module_fields_ids .= ")";
        if($sort_by){
            if($order == 'asc'){
                $order_by = ($sort_by == 'row' || $sort_by == 'section')  ? 'module_field_countries.'.$sort_by.' asc' : 'module_fields.field asc';

            }else{
                $order_by = ($sort_by == 'row' || $sort_by == 'section')  ? 'module_field_countries.'.$sort_by. ' desc' : 'module_fields.field desc';
            }
            $this->data['sort_by'] = $sort_by;
        }else{
            $order_by = 'module_fields.field asc';
            $this->data['sort_by'] = 'sort_by=field&order=asc';
        }

        $module_fields_country = $this->module_field_countries_m->get_by(array(
            'module_field_countries.country_id' => $this->country_id,
            'module_field_countries.is_draft' => 0,
            'module_field_countries.is_draft' => 0,
            'module_fields.is_draft' => 0,
            'module_field_countries.module_field_id IN ' . $module_fields_ids => NULL
        ), false, $order_by, 'module_field_countries.*,module_fields.module,module_fields.field', array(
            '0' => array('db' => 'module_fields', 'query' => 'module_field_countries.module_field_id = module_fields.id', 'type' => 'inner')
        ));

        $module_fields_country_ids = "(0";
        foreach ($module_fields_country as $p) {
            $module_fields_country_ids .= ',' . $p['module_field_id'];
        }
        $module_fields_country_ids .= ")";

        $conditions['id NOT IN' . $module_fields_country_ids] = NULL;
        $module_fields = $this->module_fields_m->get_by($conditions, false, 'field', 'id,module,field');

        $this->data['module_fields_country'] = $module_fields_country;
        $this->data['page_title'] = page_title('Module Field Countries');
        $this->data['fields'] = $module_fields;
        $this->data['filter'] = $this->input->get('filter') ? $this->input->get('filter') : 'customers';
        $this->data['modules'] = $this->modules_m->get_by(array('is_draft' => 0), false, 'name asc', 'id,name');

        return $this->render('ami/module_field_countries/process', $this->data);

    }

    public function ajax_add_remove_field()
    {
        $this->is('POST');

        $data = $this->module_field_countries_m->array_from_post(array('ids', 'type'));

        $posts = $this->module_fields_m->get_by(array('id IN (' . $data['ids'] . ')' => NULL));

        if (count($posts) > 0) {
            foreach ($posts as $post) {

                if ($data['type'] == 'add') {
                    // TODO add to module_field_countries
                    $data_save = array(
                        'title' => $post['field'],
                        'module_field_id' => $post['id'],
                        'country_id' => $this->country_id);

                    $id = $this->module_field_countries_m->save($data_save);
                } else {
                    // TODO remove to module_field_countries
                    $exist = $this->module_field_countries_m->get_by(array('country_id' => $this->country_id, 'is_draft' => 0, 'module_field_id' => $post['id']));

                    if (count($exist)) {
                        foreach ($exist as $k) {
                            $this->module_field_countries_m->delete($k['id']);
                            //$this->module_field_countries_m->save(array('is_draft' => 1), $k['id']);
                        }
                    }
                }
            }
        }
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'module_field_countries')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['module_field_countries'] = $this->module_field_countries_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->module_field_countries_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('module_field_countries');

        return $this->render('ami/module_field_countries/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            'salespersons' => $this->salespersons_m->getPersonListOptions(' - Select - ', $conditions, true)
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        return $this->render('ami/module_field_countries/edit', $this->data);
    }

    public function edit($id = null, $title = null, $filter = null)
    {
        if (!$this->hasPermission('edit', 'setting_module_fields') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->_assertId($id, 'ami/module_field_countries');

        if ($id = intval($id)) {
            $module_field_countries_m = $this->module_field_countries_m->get_by(array('country_id' => $this->country_id, 'module_field_id' => $id, 'is_draft' => 0));
            $this->data['post'] = $module_field_countries_m['0'];
            //$this->assertCountry($this->data['module_field_countries']);
            $this->data['page_title'] = page_title('Edit Module Field Countries');
            $this->data['permission'] = 'setting_module_fields';
            if ($this->input->is_ajax_request()) {
//                if (!$this->hasPermission('view', 'setting_module_fields')) {
//                    return $this->noPermission();
//                }
                $type_arr = array(
                    '1' => 'View',
                    '2' => 'Input Text',
                    '11' => 'Input Number',
                    '12' => 'Input Phone',
                    '13' => 'Input Email',
                    '3' => 'Dropdown',
                    '4' => 'Click',
                    '15' => 'Checkbox'
                );
                if($filter == 'contacts' || $filter == 'contacts_push'){
                    $this->data['list_type'] = array(
                        '1' => 'View',
                        '2' => 'Input Text',
                        '11' => 'Input Number',
                        '12' => 'Input Phone',
                        '13' => 'Input Email',
                        '3' => 'Dropdown',
                        '4' => 'Click',
                        '7' => 'Input & Access Link',
                        '8' => 'Datepicker',
                        '15' => 'Checkbox'

                    );
                }elseif($filter == 'competitor_products' || $filter == 'competitor_products_push'){
                    $this->data['list_type'] = array(
                        '1' => 'View',
                        '2' => 'Input Text',
                        '11' => 'Input Number',
                        '12' => 'Input Phone',
                        '13' => 'Input Email',
                        '3' => 'Dropdown',
                        '4' => 'Click',
                        '9' => 'Input & Dropdown',
                        '10' => 'Image',
                        '15' => 'Checkbox'
                    );
                }else{
                    $this->data['list_type'] = array(
                        '1' => 'View',
                        '2' => 'Input Text',
                        '11' => 'Input Number',
                        '12' => 'Input Phone',
                        '13' => 'Input Email',
                        '3' => 'Dropdown',
                        '4' => 'Click',
                        '15' => 'Checkbox'
                    );
                }
                $this->data['value_field'] = $title;
                $this->data['filter'] = $filter;
                $this->data['country_name'] = $this->country_name;

                return $this->render('ami/module_field_countries/preview', $this->data);
            } else {
                return $this->render('ami/module_field_countries/edit', $this->data);
            }
        }

        return redirect('ami/module_field_countries');
    }


    public function save()
    {
        if (!$this->hasPermission('add', 'setting_module_fields') && !$this->hasPermission('edit', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->module_field_countries_m->array_from_post(array('id', 'title', 'section', 'type', 'row', 'required', 'filter'));
        $filter = $data['filter'];
        $data['country_id'] = $this->country_id;
        unset($data['filter']);

        $id = $data['id'] ? $data['id'] : null;

        $newId = $this->module_field_countries_m->save($data, $id);

        return redirect('ami/module_field_countries/process?filter=' . $filter);
    }

    public function add_new($data)
    {
        $parse_key = config_item('parse_key');

        $this->load->library('parse-php-sdk/src/Parse/ParseClient');
        $this->load->library('parse-php-sdk/src/Parse/ParseObject');
        $this->load->library('parse-php-sdk/src/Parse/ParsePush');

        $this->parseclient->initialize(
            $parse_key['app_id']['pull'],
            $parse_key['rest_key']['pull'],
            $parse_key['master_key']['pull']
        );
        // save data to parse
        $db = new ParseObject("add_new");
        $db->set("content", json_encode($data));
        @$db->save();

        // push notification
        @ParsePush::send(array(
            "channels" => ["PHPFans"],
            "data" => json_encode($data)
        ));
    }

    public function approve($id = null)
    {
        $hasPermission = false;
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/module_field_countries');

        if ($id) {
            $this->data['module_field_countries'] = $this->module_field_countries_m->get($id);
            $this->assertCountry($this->data['module_field_countries']);
            $this->data['page_title'] = page_title('Approve News Feed');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                //$this->data['amendedData'] = $this->_getAmendedData($this->data['module_field_countries']['id']);

                return $this->render('ami/module_field_countries/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $this->module_field_countries_m->save(array(
                    'status' => 1
                ), $id);
            }
        }

        return redirect('ami/module_field_countries ? filter = pending');
    }

    public function reject($id = null)
    {
        $hasPermission = false;
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/module_field_countries');

        if ($id) {
            $this->data['module_field_countries'] = $this->module_field_countries_m->get($id);
            $this->assertCountry($this->data['module_field_countries']);
            $this->data['page_title'] = page_title('Reject News Feed');

            if ($this->input->is_ajax_request()) {
                //$this->data['amendedData'] = $this->_getAmendedData($this->data['module_field_countries']['id']);

                return $this->render('ami/module_field_countries/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $this->module_field_countries_m->save(array(
                    'status' => -1
                ), $id);
            }
        }

        return redirect('ami/module_field_countries ? filter = pending');
    }
}