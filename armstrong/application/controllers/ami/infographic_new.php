<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Infographic_new extends AMI_Controller{
	public function __construct(){
		parent::__construct();

        $this->data['page'] = array(
            'scripts' => array(
                'res/js/jquery.min.js',
                'res/js/bootstrap.min.js',
                'res/js/jquery.circliful.min.js',
                // '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'
                'res/js/infographic.js'
            ),
            'stylesheets' => array(
                'res/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap-theme.min.css',
                // 'res/css/bootstrap-topnav-fix.css',
                // 'res/lib/bootstrap2.3.2/css/bootstrap-responsive.min.css',
                // 'res/css/infographic.css',
                // 'res/css/admin.css'
                /*'res/css/infographic_new/style.css'*/
            )
        );

        $this->load->model('Infographic_model');

         if ($this->session->userdata('infographicCountry')) {
            $this->country_code = $this->session->userdata('infographicCountry');

            $_countries = array_rewrite($this->countries, 'code');
            $this->country_id = $_countries[$this->session->userdata('infographicCountry')]['id'];
            $this->country_name = $_countries[$this->session->userdata('infographicCountry')]['name'];
        } else {
            $this->country_code = $this->session->userdata('country');
        }

        $this->data['country_id'] = $this->country_id;
        $this->data['country_name'] = $this->country_name;
        $this->data['countryCurrency'] = $this->db->select('currency')->from('country')->where(array('name' => $this->country_name))->get()->row();

        $this->data['infographicMonth'] = $infographicMonth = $this->session->userdata('infographicMonth');
        $this->data['infographicYear'] = $infographicYear = $this->session->userdata('infographicYear');
        $this->data['infographicAction'] = $infoAction = $this->session->userdata('infographicAction');
       
       

    }

    public function tfo_strike_rate_vs_perfect_call(){

    	$this->data['page'] = array(
            'scripts' => array(
                'res/js/jquery.min.js',
                'res/js/bootstrap.min.js',
                'res/js/jquery.circliful.min.js',
                // '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'
                'res/js/infographic.js'
            ),
            'stylesheets' => array(
                'res/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap-theme.min.css',
                // 'res/css/bootstrap-topnav-fix.css',
                // 'res/lib/bootstrap2.3.2/css/bootstrap-responsive.min.css',
                // 'res/css/infographic.css',
                // 'res/css/admin.css'
                'res/css/infographic/style.css'
            )
        );

    	return $this->load->view('infographic_new/tfo_strike_rate_vs_perfect_call');
    }

    public function perfect_calls_criteria(){
    	return $this->load->view('infographic_new/perfect_calls_criteria');
    }

    public function sl_count(){
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $this->data['slcount'] = $this->Infographic_model->getReportAllInOne($this->country_id, array(), $month, $year);
        $sl_count = $this->data['slcount'];
        if (array_key_exists('numberSalesLeaders', $sl_count)){
            return $this->data['slcount']['numberSalesLeaders'];
        } else {
            return 0;
        }
    }

    public function sl_count_forteam(){
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $this->data['slcount'] = $this->Infographic_model->getSalesTeamLeader($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        $sl_count = $this->data['slcount'];
        if (array_key_exists('slCount', $sl_count)){
            return $this->data['slcount']['numberSalesLeaders'];
        } else {
            return 0;
        }
    }

    public function sr_count(){
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $this->data['srcount'] = $this->Infographic_model->getReportAllInOne($this->country_id, array(), $month, $year);
        $sr_count = $this->data['srcount'];
        if (array_key_exists('numberSalesPersonnels', $sr_count)){
            return $this->data['srcount']['numberSalesPersonnels'];
        } else {
            return 0;
        }
    }

    public function sr_count_forteam(){
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $this->data['srcount'] = $this->Infographic_model->getSalesTeamPersons($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        $sr_count = $this->data['srcount'];
        if (array_key_exists('srCount', $sr_count)){
            return $this->data['srcount']['srCount'];
        } else {
            return 0;
        }
    }

    public function total_sales_performance()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $month, $year);
        $totalSales = $this->data['infographic'];
        if (count($totalSales) == 0) {
             return 0;
        } else {

            //echo"<pre>";
            //print_r($this->data['sample']['numberSalesPersonnels']);
            //echo"</pre>";

            return $this->data['infographic']['salesAchieved'];
        }
    }

    public function generate()   
    {
        error_reporting(0);
        
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');

    	$this->data['page']['title'] = 'Infographic';
    	
    	$d = DateTime::createFromFormat('!m', $this->session->userdata('infographicMonth'));
    	$this->data['page']['month'] =  $d->format('F'); 
        $this->data['page']['year'] = $this->session->userdata('infographicYear');

         //sales team counts
         if($this->session->userdata('infographicViewType') == "country"){
	         $this->data['page']['slcount'] = $this->sl_count();
	         $this->data['page']['countSr'] = $this->sr_count();

             $wholesalers = $this->Infographic_model->getWholesalers($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
             //$this->data['page']['wholesalers1'] = $wholesalers;
             $this->data['page']['wholesalers'] = $wholesalers['0']['wholesalers'];

             $distributors = $this->Infographic_model->getDistributors($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
             //$this->data['page']['distributors1'] = $distributors;
             $this->data['page']['distributors'] = $distributors['0']['distributors'];
             
	         $this->data['page']['salesamount'] = $this->total_sales_performance();

	     }else{
            //SR and SL team view
	        $slCount = $this->Infographic_model->getSalesTeamLeader($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSl'), $this->session->userdata('infographicSr'), $this->session->userdata('infographicsalespersonStatus'));
        	$this->data['page']['slcount1'] = $slCount;
        	$this->data['page']['slcount'] = $this->data['page']['slcount1']['slCount'];
            //echo $this->session->userdata('infographicsalespersonStatus');
	        
            $wholesalers = $this->Infographic_model->getWholesalersForTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSl'), $this->session->userdata('infographicSr'), $this->session->userdata('infographicsalespersonStatus'));
            //$this->data['page']['wholesalers1'] = $wholesalers;
            $this->data['page']['wholesalers'] = $wholesalers['0']['wholesalers'];

            $distributors = $this->Infographic_model->getDistributors($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSl'), $this->session->userdata('infographicSr'), $this->session->userdata('infographicsalespersonStatus'));
            //$this->data['page']['distributors1'] = $distributors;
            $this->data['page']['distributors'] = $distributors['0']['distributors'];
            
            $srCount = $this->Infographic_model->getSalesTeamReps($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSl'), $this->session->userdata('infographicSr'), $this->session->userdata('infographicsalespersonStatus'));
        	//$this->data['page']['srcount1'] = $srCount;
        	$this->data['page']['countSr'] = $srCount['srCount'];

			$totalAmount = $this->Infographic_model->getTotalSalesForTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSl'), $this->session->userdata('infographicSr'), $this->session->userdata('infographicsalespersonStatus'));
        	//$this->data['page']['salesamount1'] = $totalAmount;
        	$this->data['page']['salesamount'] = $totalAmount['totalAmount']; 

            $slName = $this->Infographic_model->getSlName($this->session->userdata('infographicSl'));
            //$this->data['page']['slName1'] = $slName;
            $this->data['page']['slName'] = $slName['slName'];

            //debug::printArrDie($this->session->userdata('infographicSr'));

            $srName = $this->Infographic_model->getSlName($this->session->userdata('infographicSr'));
            $this->data['page']['srName1'] = $srName;
            $this->data['page']['srName'] = $this->data['page']['srName1']['slName'];
	     }
/*
         $this->data['page']['totalSl'] =  $this->sl_count();
         $this->data['page']['totalSr'] =  $this->data['page']['srcount'] = $this->sr_count();
         
*/
         //get perfect call section data
       
        if($this->session->userdata('infographicViewType') == "country"){ // if view selected by country..
        	$perfectCall = $this->Infographic_model->getPerfectCallCriteria($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
        }else{
            $perfectCall = $this->Infographic_model->getPerfectCallCriteriaForTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));
        }         

        $this->data['page']['perfectCall'] = $perfectCall;

        //get TFO & Strike Rate data

        $tfoStrikeRateForBarGraph = $this->Infographic_model->getTfoStrikeRateForBarGraph($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));

        $lastSixMonths = array(0 => "thisMonth", "lastMonth", "twoMonthsAgo", "threeMonthsAgo", "fourMonthsAgo", "fiveMonthsAgo");
        $lastSixMonthsLastYear = array(0 => "thisMonthLastYear", "lastMonthLastYear", "twoMonthsAgoLastYear", "threeMonthsAgoLastYear", "fourMonthsAgoLastYear", "fiveMonthsAgoLastYear");
        $totalCallsStrikeRate = array(0 => "callsThisMonth", "callsLastMonth", "callsTwoMonthsAgo", "callsThreeMonthsAgo", "callsFourMonthsAgo", "callsFiveMonthsAgo");
        $totalCallsStrikeRateLastYear = array(0 => "callsThisMonthLastYear", "callsLastMonthLastYear", "callsTwoMonthsAgoLastYear", "callsThreeMonthsAgoLastYear", "callsFourMonthsAgoLastYear", "callsFiveMonthsAgoLastYear");
        $perfectCalls = array(0 => "perfectCallsThisMonth", "perfectCallsLastMonth", "perfectCallsTwoMonthsAgo", "perfectCallsThreeMonthsAgo", "perfectCallsFourMonthsAgo", "perfectCallsFiveMonthsAgo");
        $callsMax = array();

        if($this->session->userdata('infographicViewType') == "country"){ // if view selected by country..
	        for ($c = 0 ; $c < 6 ; $c++){
	        	$ifLastYear = $this->session->userdata('infographicMonth') - $c;
	        	
	        	if ($ifLastYear <= 0){
	        		$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraph($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1);

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraph($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), (($this->session->userdata('infographicYear'))-2));

		        	$strikeRate = $this->Infographic_model->getCallRecords($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1);

                    $this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRate[$c]] = $strikeRate;

                    $strikeRateLastYear = $this->Infographic_model->getCallRecords($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), (($this->session->userdata('infographicYear'))-2));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRateLastYear[$c]] = $strikeRateLastYear;

                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]][0]['helpme']);
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]][0]['helpme']);

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$perfectCalls[$c]] = $this->Infographic_model->getPerfectCallCriteria($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1);
                    if((($this->session->userdata('infographicMonth'))-$c)==0){
                        $d = DateTime::createFromFormat('!m', 12);
                    }else{
                        $d = DateTime::createFromFormat('!m', ($this->session->userdata('infographicMonth'))-$c);
                    }  
		    		$d = DateTime::createFromFormat('!m', 12+($this->session->userdata('infographicMonth'))-$c);  
					$this->data['page'][$lastSixMonths[$c]] =  $d->format('M');    
	        	}else{
		        	$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraph($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraph($this->country_id, (($this->session->userdata('infographicMonth'))-$c), (($this->session->userdata('infographicYear'))-1));

		        	$strikeRate = $this->Infographic_model->getCallRecords($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRate[$c]] = $strikeRate;
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]][0]['helpme']);
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]][0]['helpme']);

                    $strikeRateLastYear = $this->Infographic_model->getCallRecords($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-2);

                    $this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRateLastYear[$c]] = $strikeRateLastYear;

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$perfectCalls[$c]] = $this->Infographic_model->getPerfectCallCriteria($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'));

                    if((($this->session->userdata('infographicMonth'))-$c)==0){
                        $d = DateTime::createFromFormat('!m', 12);
                    }else{
		    		    $d = DateTime::createFromFormat('!m', ($this->session->userdata('infographicMonth'))-$c);
                    }  
					$this->data['page'][$lastSixMonths[$c]] =  $d->format('M');       
	        	}	
	        }
	    }else{
	    	for ($c = 0 ; $c < 6 ; $c++){
	        	//$lastSixMonths[$c] =  (($this->session->userdata('infographicMonth'))-$c);
	        	$ifLastYear = $this->session->userdata('infographicMonth') - $c;
	        	
	        	if ($ifLastYear <= 0){
	        		$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraphForTeam($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraphForTeam($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), (($this->session->userdata('infographicYear'))-2), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		        	$strikeRate = $this->Infographic_model->getCallRecordsForTeam($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRate[$c]] = $strikeRate;

                    $strikeRateLastYear = $this->Infographic_model->getCallRecords($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-2);

                    $this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRateLastYear[$c]] = $strikeRateLastYear;

                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]][0]['helpme']);
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]][0]['helpme']);

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$perfectCalls[$c]] = $this->Infographic_model->getPerfectCallCriteriaForTeam($this->country_id, 12+(($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		    		$d = DateTime::createFromFormat('!m', 12+($this->session->userdata('infographicMonth'))-$c);  
					$this->data['page'][$lastSixMonths[$c]] =  $d->format('M');
	        	}else{
	        		$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraphForTeam($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]] = $this->Infographic_model->getTfoStrikeRateForBarGraphForTeam($this->country_id, (($this->session->userdata('infographicMonth'))-$c), (($this->session->userdata('infographicYear'))-1), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		        	$strikeRate = $this->Infographic_model->getCallRecordsForTeam($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRate[$c]] = $strikeRate;
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonths[$c]][0]['helpme']);
                    array_push($callsMax, $this->data['page']['tfoStrikeRateForBarGraph'][$lastSixMonthsLastYear[$c]][0]['helpme']);

                    $strikeRateLastYear = $this->Infographic_model->getCallRecords($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear')-2);

                    $this->data['page']['tfoStrikeRateForBarGraph'][$totalCallsStrikeRateLastYear[$c]] = $strikeRateLastYear;

		        	$this->data['page']['tfoStrikeRateForBarGraph'][$perfectCalls[$c]] = $this->Infographic_model->getPerfectCallCriteriaForTeam($this->country_id, (($this->session->userdata('infographicMonth'))-$c), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));

		    		$d = DateTime::createFromFormat('!m', ($this->session->userdata('infographicMonth'))-$c);  
					$this->data['page'][$lastSixMonths[$c]] =  $d->format('M');
	        	}        	       	
	        }
	    }
	   $this->data['page']['callsMax'] = $callsMax;

    	// Top SKU
       $topSku = $this->Infographic_model->getTopSku($this->country_id, $this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'));

        $topSkuArray = array();

        $c=0;
        $topSkuWithChild = [];
        foreach ($topSku as &$_sku) {

            if($_sku['mainSku']){
                if ($_sku['skuNumber'] == $_sku['mainSku']) { 
                    $topSkuWithChild[$_sku['mainSku']]['main'] = $_sku;
                }
                $topSkuWithChild[$_sku['mainSku']]['child'][] = $_sku;
                $topSkuArray[$c] = $_sku['mainSku'];
            }else{
                $topSkuWithChild[$_sku['skuNumber']] = $_sku;
                $topSkuArray[$c] = $_sku['skuNumber'];
            }
            $c++;
        }

        $topSkuArray = array_unique($topSkuArray);

        if($this->session->userdata('infographicViewType') == "country"){
            $activeGrip = $this->Infographic_model->getActiveGrip($this->country_id, $this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'));
            $this->data['page'][Infographic_model::TOP_SKU]['activeGrip'] = $activeGrip;

            $activeGripLastYear = $this->Infographic_model->getActiveGrip($this->country_id, $this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear')-1);
            $this->data['page'][Infographic_model::TOP_SKU]['activeGripLastYear'] = $activeGripLastYear;

        }else{
            $activeGripByTeam = $this->Infographic_model->getActiveGripByTeam($this->country_id, $this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $this->data['page'][Infographic_model::TOP_SKU]['activeGripByTeam'] = $activeGripByTeam;

            $activeGripByTeamLastYear = $this->Infographic_model->getActiveGripByTeam($this->country_id, $this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear')-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $this->data['page'][Infographic_model::TOP_SKU]['activeGripByTeamLastYear'] = $activeGripByTeamLastYear;

        }   

        
        $this->data['page'][Infographic_model::TOP_SKU]['top3'] = $topSkuWithChild;

      
        
        
        $topSkuLastYear = $this->Infographic_model->getTopSkuLastYear($this->country_id, $this->session->userdata('infographicMonth'), (($this->session->userdata('infographicYear'))-1), $topSkuArray);

        $topSkuPrevYear = $this->Infographic_model->getTopSkuGripLastYear($this->country_id, $this->session->userdata('infographicMonth'), (($this->session->userdata('infographicYear'))-1), $topSkuArray);

        $childrenSku = $this->Infographic_model->getAllSku($this->country_id,$this->session->userdata('infographicMonth'),($this->session->userdata('infographicYear')), $topSkuArray);
        //print_r($childrenSku);

        $this->data['page'][Infographic_model::TOP_SKU]['childrenSku'] = $childrenSku;
        
        if($this->session->userdata('infographicViewType') == "team"){
        	$topSkuByTeam = $this->Infographic_model->getTopSkuByTeam($this->country_id,$this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $topSkuArray);

        	$topSkuByTeamLastYear = $this->Infographic_model->getTopSkuByTeam($this->country_id,$this->session->userdata('infographicMonth'),($this->session->userdata('infographicYear'))-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $topSkuArray);

        	$this->data['page'][Infographic_model::TOP_SKU]['topByTeam'] = $topSkuByTeam;
        	$this->data['page'][Infographic_model::TOP_SKU]['topByTeamLastYear'] = $topSkuByTeamLastYear;
        }

        $topSkuWithChildLastYear = [];
        
        $this->data['page'][Infographic_model::TOP_SKU]['top3LastYear'] = $topSkuLastYear;
        $this->data['page']['topsku']['topLastYear'] = $topSkuPrevYear;
        
        /* TFO Growth Section */
        //country view
        if($this->session->userdata('infographicViewType') == "country"){
            $tfogrowth = $this->getCountryTfoGrowth();
        }else{ // SR view
            $tfogrowth = $this->getSalespersonTfoGrowth();
             
        }

        $this->data['page']['tfogrowth'] = $tfogrowth;

        /* Grip Section */
        /* Grab Section */

        $months[] = date("m", strtotime(date($year.'-'.$month.'-01')));
        $years[] = date("Y", strtotime(date($year.'-'.$month.'-01')));

        for ($i=1; $i <= 11; $i++) { //get array of months and year
            $months[] .= date("m", strtotime(date($year.'-'.$month.'-01')." -$i months"));
            $years[] .= date("Y", strtotime(date($year.'-'.$month.'-01')." -$i months"));                   
        }

        for($j=0; $j<=11; $j++){
            if($this->session->userdata('infographicViewType') == "country"){
                $grip[] = $this->Infographic_model->getGripRecordByCountry($this->country_id,$months[$j],$years[$j]);
                $gripActive[] = $this->Infographic_model->getActiveGrip($this->country_id, $months[$j],$years[$j]);
                $grab[] = $this->Infographic_model->getGrabRecordByCountry($this->country_id,$months[$j],$years[$j]);
                $active[] = $this->Infographic_model->getActiveCustomersByCountry($this->country_id,$months[$j],$years[$j]);
            
                $kpiTarget[] = $this->Infographic_model->getKPITargetsPerCountry($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
       
            }else{
                $grip[] = $this->Infographic_model->getGripRecordBySalesperson($this->country_id,$months[$j],$years[$j], $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
                //print_r($grip);
                $grab[] = $this->Infographic_model->getGrabRecordBySalesperson($this->country_id,$months[$j],$years[$j], $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
                $active[] = $this->Infographic_model->getActiveCustomersByTeam($this->country_id,$months[$j],$years[$j], $this->session->userdata('infographicSl'),$this->session->userdata('infographicSr'));
                $gripActive[] = $this->Infographic_model->getActiveGripByTeam($this->country_id, $months[$j],$years[$j],$this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
           

            }

            $month_name[] = DateTime::createFromFormat('!m',$months[$j])->format('F');
        }

        /* get max from grab */
        foreach ($grab as $key => $value) {
            foreach ($value as $k => $v) {
                $max_grab[] = round($v['total_grab'], 1);
                $ave_grab[] = $v['ave_grab'];
                $grab_trg[] = $v['grab_target'];
                $lost_grab[] = $v['lost_grab'];
            }
        }
        
        $max_bar[] = max($max_grab);
        $max_bar[] = max($lost_grab);
        $max_line[] = max($ave_grab);
        //$max_line[] = max($grab_trg);
        
        $this->data['page']['max_grab_bar'] = max($max_bar)+2000;
        $this->data['page']['max_grab_line'] = max($max_line);
        
        /* get max from grip */
        //if($this->session->userdata('infographicViewType') == "country"){
            foreach ($gripActive as $key => $value) {
                foreach ($value as $k => $v) {
                    $max_grip[] = $v['activeGrip'];
                }
            }
        //}
        
        /*$this->data['page']['max_grab'] = max($max_grab);*/
        $this->data['page']['ave_grab'] = max($ave_grab);
       
        $this->data['page']['max_grip'] = max($max_grip) + 200;        
        $this->data['page']['gripActive'] = $gripActive;
        $this->data['page']['active'] = $active;    
        $this->data['page']['grip_monthName'] = $month_name;
        $this->data['page']['grip'] = $grip;
        $this->data['page']['grab'] = $grab;

        $this->data['page']['kpiTargets'] = $kpiTarget;



        //call coverage data fetching
        if($this->session->userData('infographicViewType')=='country'){
            $callcoverage = $this->Infographic_model->getCallCoverageByCountry($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
            $perfectCallOtm = $this->Infographic_model->getPerfectCallPerOtm($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
            $otmVisits = $this->Infographic_model->getOtmVisitsAndCount($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
            $avgWorkingHours = $this->Infographic_model->getAvgWorkingHours($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
        }else{
        	$callcoverage = $this->Infographic_model->getCallCoverageByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));
            $perfectCallOtm = $this->Infographic_model->getPerfectCallPerOtmByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));
            $otmVisits = $this->Infographic_model->getOtmVisitsAndCountByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));
            $avgWorkingHours = $this->Infographic_model->getAvgWorkingHoursByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $this->session->userdata('infographicsalespersonStatus'));
        }

        $holidays = $this->Infographic_model->getHolidays($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
        $workingHours = $this->Infographic_model->getStandardWorkingHours($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
        
        $this->data['page']['perfcallotm'] = $perfectCallOtm;
        $this->data['page']['callcoverage'] = $callcoverage;
        $this->data['page']['otmVisits'] = $otmVisits;
        $this->data['page']['avgWorkingHours'] = $avgWorkingHours;
        $this->data['page']['holidays'] = $holidays;
        $this->data['page']['workingHours'] = $workingHours;

        /* growth grip&grab section selected year*/
        if($this->session->userData('infographicViewType')=='country'){
            $growthGripGrab = $this->Infographic_model->getGripGrab($this->country_id, array(), $month, $year);
        }else{
            $growthGripGrab = $this->Infographic_model->getGripByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $growthGrab = $this->Infographic_model->getGripByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
       
       
     //   $this->data['infographic'][Infographic_model::GRIP_GRAB]['other'] = $other;
        $topGripGrab = array_slice($growthGripGrab, 0, 3);
        
        if($this->session->userData('infographicViewType')=='country'){
            $other = $this->Infographic_model->getOtherGripGrab(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);       
        }else{
            $other = $this->Infographic_model->getOtherGripByTeam(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear'], $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        array_push($topGripGrab, $other); 

        if($this->session->userData('infographicViewType')=='country'){
            $growthGripGrabPrev = $this->Infographic_model->getGripGrab($this->country_id, array(), $month, $year-1);
            $otherPrev = $this->Infographic_model->getOtherGripGrab(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']-1);
        }else{
            $otherPrev = $this->Infographic_model->getOtherGripByTeam(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
       
       
     //   $this->data['infographic'][Infographic_model::GRIP_GRAB]['other'] = $other;
        //previous year 'other' channel values 
       
        //array_push($topGripGrabPrev, $otherPrev);

        if($this->session->userData('infographicViewType')=='country'){
           $otherBreakdown = $this->Infographic_model->getOtherGripGrabBreakdown(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);

           $otherBreakdownPrev = $this->Infographic_model->getOtherGripGrabBreakdown(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']-1);
        }else{
            $otherBreakdown = $this->Infographic_model->getOtherGripBreakdownForTeam(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear'], $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));

           $otherBreakdownPrev = $this->Infographic_model->getOtherGripBreakdownForTeam(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        
        $channelNames = [];
        foreach ($otherBreakdown as $key => $value) {
            $channelNames[] = $value['countryChannelsName'];
        }

        foreach ($otherBreakdownPrev as $key => $value) {
            $channelNames[] = $value['countryChannelsName'];            
        }
        $channelNames = array_unique($channelNames);

        $category = "";
        $channels = "";
        $curr_grip = "";
        $prev_grip = "";
        $curr_grab = "";
        $prev_grab = "";

        $totalGripGrowth = 0;
        $totalGripChart = $this->data['page']['gripActive'][0][0]['activeGrip'];
        $totalGripPrev = 0;

        $totalGrabGrowth = 0;
        $totalGrabChart = $this->data['page']['gripActive'][0][0]['activeGrab'];
        $totalGrabPrev = 0;
        if($this->session->userData('infographicViewType')!='country'){
            $gripActivePrev[] = $this->Infographic_model->getActiveGripByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear')-1,$this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $totalGripChartPrev = $gripActivePrev[0][0]['activeGrip'];
            $totalGrabChartPrev = $gripActivePrev[0][0]['activeGrab'];
        }
        foreach ($topGripGrab as $key => $value) {
            if (array_key_exists('countryChannelsName', $value)) {
                $category .= "\"".$value['countryChannelsName']."\"".",";
                $channels .= $value['countryChannels'].",";
            }else{
                $category .= "\"". "OTHER". "\",";
            }
            //echo $value['countryChannelsName'];
            //echo " = ";
            //echo $value['countryChannels'];
            //echo "<br />";
            for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }              

                if (array_key_exists('countryChannels', $value)) {
                    $cChannel = $value['countryChannels'];
                    $this->data['page'][$value['countryChannelsName']][$m] = $this->Infographic_model->getGrowthGripByChannel($this->country_id, $month, $year, $cChannel);
                    //print_r($this->data['page'][$value['countryChannelsName']][$m]); echo "/";
                }
             
            }
            

            $curr_grip .= $value['totalGrip'].",";
            $currGrip[] = $value['totalGrip'];
            $totalGripGrowth = $totalGripGrowth + $value['totalGrip'];

            $curr_grab .= $value['totalGrab'].",";
            $currGrab[] = $value['totalGrab'];
            $totalGrabGrowth = $totalGrabGrowth + $value['totalGrab'];

            if (array_key_exists('countryChannels', $value)) {
                if($this->session->userData('infographicViewType')=='country'){
                    $previousGrip = $this->Infographic_model->getGrowthGripByChannel($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear')-1, $value['countryChannels']);
                }else{
                    $previousGrip = $this->Infographic_model->getGripGrabByTeamChannel($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear')-1, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $value['countryChannels']);
                }
            }else{
               $previousGrip[0]['totalGrip'] = $otherPrev['totalGrip'];
               $previousGrip[0]['totalGrab'] = $otherPrev['totalGrab'];
            }
            $previousGrip2 = (sizeof($previousGrip)>0 ) ? $previousGrip[0]['totalGrip'] : 0;
            if($previousGrip)
                $totalGripPrev = $totalGripPrev + $previousGrip[0]['totalGrip'];
            else
                $totalGripPrev = 0;
            $prev_grip .=  $previousGrip2.",";
            $prevGrip[] =  $previousGrip2;

            $previousGrab = (sizeof($previousGrip)>0 ) ? $previousGrip[0]['totalGrab'] : 0;
            $totalGrabPrev = $totalGrabPrev + $previousGrip[0]['totalGrab'];
            $prev_grab .=  $previousGrab.",";
            $prevGrab[] = $previousGrab;     

            //echo "<br/>=========================================================================================<br/>";
        }
        $discrepancy = $totalGripChart-$totalGripGrowth;
        $discrepancyGrab = $totalGrabChart-$totalGrabGrowth;
        if($this->session->userData('infographicViewType')!='country'){
            $discrepancyPrev = $totalGripChartPrev-$totalGripPrev;
            $discrepancyGrabPrev = $totalGrabChartPrev-$totalGrabPrev;
        }

        $val[] = max($currGrip);
        $val[] = max($prevGrip);
        
        $valGrab[] = max($currGrab);
        $valGrab[] = max($prevGrab);
        
        $this->data['page']['max_growth_grip'] = max($val);
        $this->data['page']['max_growth_grab'] = max($valGrab);
        //$this->data['page']['test'] = $otherPrev;

        $this->data['page']['grip_category'] = $category;
        $this->data['page']['grip_channels'] = $channels;
        $this->data['page']['grip_curr'] = rtrim($curr_grip,",");
        $this->data['page']['grip_prev'] = rtrim($prev_grip,",");

        $this->data['page']['grab_curr'] = rtrim($curr_grab,",");
        $this->data['page']['grab_prev'] = rtrim($prev_grab,",");

        $this->data['page']['channelNames'] = $channelNames;

        $this->data['page']['otherChannels'] = $otherBreakdown; 
        $this->data['page']['otherChannelsPrev'] = $otherBreakdownPrev;

        if($this->session->userData('infographicViewType')!='country'){
            $this->data['page']['otherChannels'][0]['totalGrip'] = $this->data['page']['otherChannels'][0]['totalGrip'] + ($discrepancy);
            $currentGripValue = explode(",", $this->data['page']['grip_curr']);
            $currentGripValue[3] = $currentGripValue[3]+$discrepancy;
            $curr_grip2 = "";
            foreach ($currentGripValue as $value) {
                $curr_grip2 .= $value.",";
            }
            $this->data['page']['grip_curr'] = rtrim($curr_grip2,",");

            if ($discrepancyPrev>0)
                $this->data['page']['otherChannelsPrev'][0]['totalGrip'] = $this->data['page']['otherChannelsPrev'][0]['totalGrip'] + ($discrepancyPrev);

            $prevGripValue = explode(",", $this->data['page']['grip_prev']);
            
            if ($discrepancyPrev>0)
                $prevGripValue[3] = $prevGripValue[3]+($discrepancyPrev);

            $prev_grip2 = "";
            foreach ($prevGripValue as $value) {
                $prev_grip2 .= $value.",";
            }
            $this->data['page']['grip_prev'] = rtrim($prev_grip2,",");

            $this->data['page']['otherChannels'][0]['totalGrab'] = $this->data['page']['otherChannels'][0]['totalGrab'] + ($discrepancyGrab);
            $currentGrabValue = explode(",", $this->data['page']['grab_curr']);
            $currentGrabValue[3] = $currentGrabValue[3]+$discrepancyGrab;
            $curr_grab2 = "";
            foreach ($currentGrabValue as $value) {
                $curr_grab2 .= $value.",";
            }
            $this->data['page']['grab_curr'] = rtrim($curr_grab2,",");

            if ($discrepancyGrabPrev>0)
                $this->data['page']['otherChannelsPrev'][0]['totalGrab'] = $this->data['page']['otherChannelsPrev'][0]['totalGrab'] + ($discrepancyGrabPrev);
            $prevGrabValue = explode(",", $this->data['page']['grab_prev']);
            
            if ($discrepancyGrabPrev>0)
                $prevGrabValue[3] = $prevGrabValue[3]+($discrepancyGrabPrev);
            
            $prev_grab2 = "";
            foreach ($prevGrabValue as $value) {
                $prev_grab2 .= $value.",";
            }
            $this->data['page']['grab_prev'] = rtrim($prev_grab2,",");
        }

        if($this->data['infographicAction']=='exportPDF'){
            $this->data['is_print'] = true;
        }else{
              $this->data['is_print'] = false;
        }
        //debug::printArrDie($this->data['is_print']);
        //customer profiling section generation
        if($this->session->userData('infographicViewType')=='country'){
            $customerProfiling = $this->Infographic_model->getCustomerProfilingPerCountry($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'));
        }else{
            $customerProfiling = $this->Infographic_model->getCustomerProfilingByTeam($this->country_id, $this->session->userdata('infographicMonth'), $this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
          $this->data['page']['customerProfiling'] = $customerProfiling;

        //KPI targets data fetching

        
         return $this->load->view('infographic_new/home', $this->data);


    }

    

    public function total_calls()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_calls[$m] = $this->Infographic_model->getTotalCallsPerCountry($this->country_id, $month, $year);
             
            }
              
            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_calls[$m] = $this->Infographic_model->getTotalCallsPerTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
               
        }

        $this->data['page']['total_calls'] = $total_calls;
      
        foreach ($total_calls as $key => $value) {
            $totalCalls[] = $value['totalCalls'];
        }
        $this->data['page']['max_call_count'] = max($totalCalls);

        

        return $this->load->view('infographic_new/total_calls', $this->data);
    }

    public function total_perfect_calls()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_perfect_calls[$m] = $this->Infographic_model->getPerfectCallsPerCountry($this->country_id, $month, $year);
             
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_perfect_calls[$m] = $this->Infographic_model->getPerfectCallsPerTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }

        $this->data['page']['total_perfect_calls'] = $total_perfect_calls;

        return $this->load->view('infographic_new/total_perfect_calls', $this->data);
    }

    public function total_sales_leader()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_sl[$m] = $this->Infographic_model->getSalesTeamLeader($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_sl[$m] = $this->Infographic_model->getSalesTeamLeader($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }

        $this->data['page']['total_sl'] = $total_sl;  

        return $this->load->view('infographic_new/total_sales_leader', $this->data);
    }

    public function total_sales_rep()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_sr[$m] = $this->Infographic_model->getSalesTeamPersons($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_sr[$m] = $this->Infographic_model->getSalesTeamReps($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }
      
        $this->data['page']['total_sr'] = $total_sr;

        return $this->load->view('infographic_new/total_sales_rep', $this->data);
    }

    public function total_strike_rate()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_strike_rate[$m] = $this->Infographic_model->getStrikeRatePerCountry($this->country_id, $month, $year);
             
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_strike_rate[$m] = $this->Infographic_model->getStrikeRatePerTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }

        $this->data['page']['total_strike_rate'] = $total_strike_rate;

        return $this->load->view('infographic_new/total_strike_rate', $this->data);
    }

    public function total_tfo_growth()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }
              //echo "country: " . $this->country_id . "; month: " . $month . "; year: " . $year . "<br/>";
                if($this->country_id==2||$this->country_id==9){
                    $total_tfo_growth[$m] = $this->Infographic_model->getTotalTfoGrowthByCountryANZ($this->country_id, $month, $year);
                }else{
                    $total_tfo_growth[$m] = $this->Infographic_model->getTotalTfoGrowthByCountryID($this->country_id, $month, $year);
                }
             
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_tfo_growth[$m] = $this->Infographic_model->getTotalTfoGrowthByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }

        $this->data['page']['total_tfo_growth'] = $total_tfo_growth;

        foreach ($total_tfo_growth as $key => $value) {
            $tfo[] = $value[0][0]['new_sku_curr'];
        }

        foreach ($total_tfo_growth as $key => $value) {
            $tfo[] = $value[0][0]['exi_sku_curr'];
        }
        $this->data['page']['max_tfo_growth'] = max($tfo);

        return $this->load->view('infographic_new/total_tfo_growth', $this->data);
    }

    public function total_top_sku()
    {
        $skuNumber = array(1);
        $skuNumber[0] = $_GET['skuNumber']; 
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }
              //echo $month . ", ";
              $topskudata = $this->Infographic_model->getTopSkuLastYear($this->country_id, $month, $year, $skuNumber);

              $percentage = 0;
              $activeGrip = $this->Infographic_model->getActiveGrip($this->country_id, $month, $year);
                foreach ($topskudata as $key => $val) { 
                    $percentage = round(($val['totalCustomerOrder']/$activeGrip[0]['activeGrip'])*100,0);
                }


                $total_top_sku[$m]['percentage'] = $percentage;
                $total_top_sku[$m]['monthReport'] = $month;
              //print_r(array_values($total_top_sku));
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $topskudata = $this->Infographic_model->getTopSkuByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'), $skuNumber);
               $activeGripByTeam = $this->Infographic_model->getActiveGripByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));

               $percentage = 0;
                foreach ($topskudata as $key => $val) { 
                    $percentage = round(($val['totalCustomerOrder']/$activeGripByTeam[0]['activeGrip'])*100,0);
                }
                
                $total_top_sku[$m]['percentage'] = $percentage;
                $total_top_sku[$m]['monthReport'] = $month;
            }
        }

        $this->data['page']['total_top_sku'] = $total_top_sku;

        return $this->load->view('infographic_new/total_top_sku', $this->data);
    }

    public function new_grip_breakdown()
    {
        //clicked month (on the graph)
        $monthSelected[0] = $_GET['monthSelected']; 

        $month = $monthSelected[0];
        $month = date("n",strtotime($month));
        $year = $this->session->userdata('infographicYear');
        $nextMonth = $month + 1;
        $currentMonth = $this->session->userdata('infographicMonth');

        //checks if month is in the previous year 
        $monthDifference = $currentMonth - $month;
        if ($monthDifference < 0){
            $year = $year - 1;
        } 

        if($this->session->userData('infographicViewType')=='country'){
            $new_grip_per_channel = $this->Infographic_model->getNewGripPerChannel($this->country_id, $month, $year, null, null);
            $new_grip = $this->Infographic_model->getGripRecordByCountry($this->country_id, $month, $year);
        }else{
            $new_grip_per_channel = $this->Infographic_model->getNewGripPerChannel($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $new_grip = $this->Infographic_model->getGripRecordBySalesperson($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        $newGrips = array_column($new_grip_per_channel, 'newGrip');
        
        $newGripsSum = array_sum($newGrips);
        // echo array_sum($newGrabs);
        //print_r($newGrabs);

        $discrepancy = $new_grip[0]['total_new_grip'] - $newGripsSum;
        // echo "<br/>";
        // echo $discrepancy;
        // echo "<br/>";
        // echo $newGrabs[0];
        $newGrips[0] = $newGrips[0] + ($discrepancy);
        $new_grip_per_channel[0]['newGrip'] = $newGrips[0];

        rsort($newGrips);
        rsort($new_grip_per_channel);

        $this->data['page']['new_grip_per_channel'] = $new_grip_per_channel;
        $this->data['page']['newGrips'] = $newGrips;
        return $this->load->view('infographic_new/new_grip_breakdown', $this->data);
    }

    public function new_grab_breakdown()
    {
        //clicked month (on the graph)
        $monthSelected[0] = $_GET['monthSelected']; 

        $month = $monthSelected[0];
        $month = date("n",strtotime($month));
        $year = $this->session->userdata('infographicYear');
        $nextMonth = $month + 1;
        $currentMonth = $this->session->userdata('infographicMonth');

        //checks if month is in the previous year 
        $monthDifference = $currentMonth - $month;
        if ($monthDifference < 0){
            $year = $year - 1;
        } 

        if($this->session->userData('infographicViewType')=='country'){
            $new_grab_per_channel = $this->Infographic_model->getNewGrabPerChannel($this->country_id, $month, $year, null, null);
            $total_grab = $this->Infographic_model->getGrabRecordByCountry($this->country_id, $month, $year);
        }else{
            $new_grab_per_channel = $this->Infographic_model->getNewGrabPerChannel($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $total_grab = $this->Infographic_model->getGrabRecordBySalesperson($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        $newGrabs = array_column($new_grab_per_channel, 'newGrab');

        // print_r($newGrabs);
        // echo "<br/>";
        // print_r($total_grab[0]['new_grab_target']);
        // echo "<br/>";
        $newGrabsSum = array_sum($newGrabs);
        // echo array_sum($newGrabs);
        //print_r($newGrabs);

        $discrepancy = $total_grab[0]['new_grab_target'] - $newGrabsSum;
        // echo "<br/>";
        // echo $discrepancy;
        // echo "<br/>";
        // echo $newGrabs[0];
        $newGrabs[0] = $newGrabs[0] + ($discrepancy);
        $new_grab_per_channel[0]['newGrab'] = $newGrabs[0];
        // echo "<br/>" . $newGrabs[0];
        // echo "<br/>";
        // print_r($new_grab_per_channel[0]['newGrab']);
        rsort($newGrabs);
        rsort($new_grab_per_channel);
        //print_r(arsort($new_grab_per_channel));


        $this->data['page']['new_grab_per_channel'] = $new_grab_per_channel;
        $this->data['page']['newGrabs'] = $newGrabs;
        return $this->load->view('infographic_new/new_grab_breakdown', $this->data);
    }

    public function lost_grip_breakdown()
    {
        //clicked month (on the graph)
        $monthSelected[0] = $_GET['monthSelected']; 

        $month = $monthSelected[0];
        $month = date("n",strtotime($month));
        $year = $this->session->userdata('infographicYear');
        $nextMonth = $month + 1;
        $currentMonth = $this->session->userdata('infographicMonth');

        //checks if month is in the previous year 
        $monthDifference = $currentMonth - $month;
        if ($monthDifference < 0){
            $year = $year - 1;
        } 

        if($this->session->userData('infographicViewType')=='country'){
            $lost_grip_per_channel = $this->Infographic_model->getLostGripPerChannel($this->country_id, $month, $year, null, null);
            $lost_grip = $this->Infographic_model->getGripRecordByCountry($this->country_id, $month, $year);
        }else{
            $lost_grip_per_channel = $this->Infographic_model->getLostGripPerChannel($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $lost_grip = $this->Infographic_model->getGripRecordBySalesperson($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        $lostGrips = array_column($lost_grip_per_channel, 'lostGrip');
        $lostGripsSum = array_sum($lostGrips);
        // echo array_sum($newGrabs);
        //print_r($newGrabs);

        $discrepancy = $lost_grip[0]['total_lost_grip'] - $lostGripsSum;
        // echo "<br/>";
        // echo $discrepancy;
        // echo "<br/>";
        // echo $newGrabs[0];
        $lostGrips[0] = $lostGrips[0] + ($discrepancy);
        $lost_grip_per_channel[0]['lostGrip'] = $lostGrips[0];
        //print_r($lost_grip_per_channel);
        rsort($lostGrips);
        rsort($lost_grip_per_channel);

        $this->data['page']['lost_grip_per_channel'] = $lost_grip_per_channel;
        $this->data['page']['lostGrips'] = $lostGrips;
        return $this->load->view('infographic_new/lost_grip_breakdown', $this->data);
    }

    public function lost_grab_breakdown()
    {
        //clicked month (on the graph)
        $monthSelected[0] = $_GET['monthSelected']; 

        $month = $monthSelected[0];
        $month = date("n",strtotime($month));
        $year = $this->session->userdata('infographicYear');
        $nextMonth = $month + 1;
        $currentMonth = $this->session->userdata('infographicMonth');

        //checks if month is in the previous year 
        $monthDifference = $currentMonth - $month;
        if ($monthDifference < 0){
            $year = $year - 1;
        } 

        if($this->session->userData('infographicViewType')=='country'){
            $lost_grab_per_channel = $this->Infographic_model->getLostGrabPerChannel($this->country_id, $month, $year);
            $total_grab = $this->Infographic_model->getGrabRecordByCountry($this->country_id, $month, $year);
        }else{
            $lost_grab_per_channel = $this->Infographic_model->getLostGrabPerChannel($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
            $total_grab = $this->Infographic_model->getGrabRecordBySalesperson($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        $lostGrabs = array_column($lost_grab_per_channel, 'lostGrab');

        // print_r($total_grab[0]['lost_grab']);
        // echo "<br/>";
        $lostGrabsSum = array_sum($lostGrabs);
        // echo array_sum($lostGrabs);
        // print_r($lostGrabs);

        $discrepancy = $total_grab[0]['lost_grab'] - $lostGrabsSum;
        // echo "<br/>";
        // echo $discrepancy;
        // echo "<br/>";
        // echo $lostGrabs[0];
        $lostGrabs[0] = $lostGrabs[0] + ($discrepancy);
        $lost_grab_per_channel[0]['lostGrab'] = $lostGrabs[0];
        // echo "<br/>" . $lostGrabs[0];
        // echo "<br/>";
        // print_r($lost_grab_per_channel[0]['lostGrab']);
        rsort($lostGrabs);
        rsort($lost_grab_per_channel);
        // print_r($lostGrabs);


        $this->data['page']['lost_grab_per_channel'] = $lost_grab_per_channel;
        $this->data['page']['lostGrabs'] = $lostGrabs;
        return $this->load->view('infographic_new/lost_grab_breakdown', $this->data);
    }

    public function total_grip_breakdown()
    {
        //clicked month (on the graph)
        $monthSelected[0] = $_GET['monthSelected']; 

        $month = $monthSelected[0];
        $month = date("n",strtotime($month));
        $year = $this->session->userdata('infographicYear');
        $nextMonth = $month + 1;
        $currentMonth = $this->session->userdata('infographicMonth');

        //checks if month is in the previous year 
        $monthDifference = $currentMonth - $month;
        if ($monthDifference < 0){
            $year = $year - 1;
        } 

        if($this->session->userData('infographicViewType')=='country'){
            $total_grip_per_channel = $this->Infographic_model->getGripByTeam($this->country_id, $month, $year, null, null);
            //print_r($total_grip_per_channel);
        }else{
            $total_grip_per_channel = $this->Infographic_model->getGripByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }

        $country_channels = $this->Infographic_model->getCountryChannels($this->country_id);
        //print_r($country_channels);
        
        //$countryChannelNames = array_column($total_grip_per_channel, 'countryChannelsName');
        $totalGrips = array_column($total_grip_per_channel, 'totalGrip');
        $totalGrabs = array_column($total_grip_per_channel, 'totalGrab');
        
        rsort($total_grip_per_channel);
        rsort($totalGrips);
        rsort($totalGrabs);
        //print_r($countryChannelNames);
        $this->data['page']['total_grip_per_channel'] = $total_grip_per_channel;
        $this->data['page']['totalGrips'] = $totalGrips;
        $this->data['page']['totalGrabs'] = $totalGrabs;
        return $this->load->view('infographic_new/total_grip_breakdown', $this->data);
    }
    public function total_growth_grip()
    {
        $channels = substr($_GET['channels'], 0, -1);
        print_r($channels); 
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

              $total_top_sku[$m] = $this->Infographic_model->getTopSkuLastYear($this->country_id, $month, $year, $skuNumber);
              //print_r(array_values($total_top_sku));
            }            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_top_sku[$m] = $this->Infographic_model->getTfoGrowthBySalesperson($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
             
            }
        }

        /* growth grip&grab section selected year*/
        if($this->session->userData('infographicViewType')=='country'){
            $growthGripGrab = $this->Infographic_model->getGripGrab($this->country_id, array(), $month, $year);
        }else{
            $growthGripGrab = $this->Infographic_model->getGripByTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
       
       
     //   $this->data['infographic'][Infographic_model::GRIP_GRAB]['other'] = $other;
        $topGripGrab = array_slice($growthGripGrab, 0, 3);
        
        if($this->session->userData('infographicViewType')=='country'){
            $other = $this->Infographic_model->getOtherGripGrab(array_keys(array_rewrite($topGripGrab, 'id')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);       
        }else{
            $other = $this->Infographic_model->getOtherGripByTeam(array_keys(array_rewrite($topGripGrab, 'countryChannels')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear'], $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }
        array_push($topGripGrab, $other); 

        $this->data['page']['total_top_sku'] = $total_top_sku;

        return $this->load->view('infographic_new/total_tfo_growth', $this->data);
    }

    public function total_otm_calls()
    {
        $month = $this->session->userdata('infographicMonth');
        $year = $this->session->userdata('infographicYear');
        $thisYear = $year;
        if($this->session->userData('infographicViewType')=='country'){
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                if ($year > ($thisYear-1))
                    $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_otm_calls[$m] = $this->Infographic_model->getTotalOTMCallsPerCountry($this->country_id, $month, $year);
             
            }
              
            
        }else{
          for($m=0;$m<=11;$m++){
           
              if($this->session->userdata('infographicMonth')-$m<=0){
                $year = $year - 1;
                $month = 12+($this->session->userdata('infographicMonth'))-$m;
              }else{
                $month = $this->session->userdata('infographicMonth') - $m;
              }

               $total_otm_calls[$m] = $this->Infographic_model->getTotalOTMCallsPerTeam($this->country_id, $month, $year, $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
                print_r($total_otm_calls[$m]);
            }
               
        }

        $this->data['page']['total_otm_calls'] = $total_otm_calls;        
      
        foreach ($total_otm_calls as $key => $value) {
            $totalOtmACalls[] = $value['otmACalls'];
            $totalOtmBCalls[] = $value['otmBCalls'];
            $totalOtmCCalls[] = $value['otmCCalls'];
            $totalOtmDCalls[] = $value['otmDCalls'];
            $totalOtmUCalls[] = $value['otmUCalls'];
        }
        $this->data['page']['max_otmAcall_count'] = max($totalOtmACalls);
        $this->data['page']['max_otmBcall_count'] = max($totalOtmBCalls);
        $this->data['page']['max_otmCcall_count'] = max($totalOtmCCalls);
        $this->data['page']['max_otmDcall_count'] = max($totalOtmDCalls);
        $this->data['page']['max_otmUcall_count'] = max($totalOtmUCalls);


        return $this->load->view('infographic_new/total_otm_calls', $this->data);
    }

    /*get tfo growth section values*/
    public function getCountryTfoGrowth(){
        if($this->country_id==2||$this->country_id==9){
           $tfogrowth = $this->Infographic_model->getTfoGrowthByCountryANZ($this->country_id,$this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'));
        }else{
            $tfogrowth = $this->Infographic_model->getTfoGrowthByCountryID($this->country_id,$this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'));
        }

        return $tfogrowth;
    }

    public function getSalespersonTfoGrowth(){
        if($this->country_id==2||$this->country_id==9){
            $tfogrowth = $this->Infographic_model->getTfoGrowthBySalespersonANZ($this->country_id,$this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }else{
            $tfogrowth = $this->Infographic_model->getTfoGrowthBySalesperson($this->country_id,$this->session->userdata('infographicMonth'),$this->session->userdata('infographicYear'), $this->session->userdata('infographicSr'),$this->session->userdata('infographicSl'));
        }

        return $tfogrowth;
    }
}