<?php
/*
	
*/

if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cron_Monitoring extends AMI_Controller {

	 public function __construct()
    {
        parent::__construct();

		$this->load->model('Cron_monitoring_model');

		 $this->session_id = $this->session->userdata('session_id');
	}


	 public function index()
    {
        $this->data['country_id'] = $this->country_id;
        $this->data['session_id'] = $this->session_id;
        
        $this->getCronList();

        return $this->render('ami/cron_monitoring/index', $this->data);
    }

    public function getCronList(){

    	$cronlist = $this->Cron_monitoring_model->getAll();
        $i = 0;
        foreach($cronlist as $row){
          
            $crondata = $this->Cron_monitoring_model->getCronData($this->country_id, $row['reference_table'], $row['date_reference'], $row['country_reference']);

            $cronres[$i]['id'] = $row['id'];
            $cronres[$i]['name'] = $row['cronjob_name'];
            $cronres[$i]['desc'] = $row['description'];
            $cronres[$i]['schedule'] = $row['schedule'];
            $cronres[$i]['last_updated'] = $crondata[$row['date_reference']];
            $i++;
        }
       
        return $this->json($cronres);
    }


}


?>