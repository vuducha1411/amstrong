<?php

class Kpi_target extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('kpi_target_m');
        // $this->load->model('menu_m');
        $this->load->model('salespersons_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'kpi_target')) {
            return $this->noPermission();
        }

        $conditions = array('kpi_target.country_id' => $this->country_id, 'kpi_target.is_draft' => 0);

        $this->data['data'] = $this->kpi_target_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_target_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('KPI Target');
        return $this->render('ami/kpi_target/index1', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'kpi_target')) {
            return $this->noPermission();
        }

        $conditions = array('kpi_target.country_id' => $this->country_id, 'kpi_target.is_draft' => 1);

        $this->data['settings'] = $this->kpi_target_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_target_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('KPI Settings');

        return $this->render('ami/kpi_target/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            // 'modules' => $this->menu_m->get(),
            'country' => $this->country,
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions, true),
            // 'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'country_sub_channels' => $this->country_sub_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            // 'assigned_distributors' => $this->roles_m->parse_form(array('armstrong_2_distributors_id', 'name'), '- Select -', $conditions),
        );
    }

    //hieu check config potential in country setting
    private function checkPotential()
    {
        $this->load->model('country_m');
        $country = $this->country_m->get($this->country_id);
        $application_config = json_decode($country['application_config']);
        if (isset($application_config->pull->potential_customer) && $application_config->pull->potential_customer == '1') {
            return true;
        }
        return false;
    }

    //end hieu
    public function add()
    {
        if (!$this->hasPermission('add', 'kpi_target')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['enablepotential'] = $this->checkPotential(); // hieu
        $this->data['post'] = array('month' => date('m', time()), 'year' => date('Y', time()));
        return $this->render('ami/kpi_target/edit1', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'kpi_target') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target');

        if ($id) {
            $post = $this->kpi_target_m->get($id);
            $sp = $this->salespersons_m->get($post['armstrong_2_salespersons_id']);
            $post['sp_type'] = $sp['type'];
            $post['kpi_call_target'] = ($post['kpi_call_target'] == '0') ? $this->getTarget($post['kpi_call_target_weeks']) : $this->getTarget($post['kpi_call_target']);
            $post['kpi_new_grab_target'] = ($post['kpi_new_grab_target'] == '0') ? $this->getTarget($post['kpi_new_grab_target_weeks']) : $this->getTarget($post['kpi_new_grab_target']);
            $post['kpi_new_grip_target'] = ($post['kpi_new_grip_target'] == '0') ? $this->getTarget($post['kpi_new_grip_target_weeks']) : $this->getTarget($post['kpi_new_grip_target']);
            $post['kpi_sale_target'] = ($post['kpi_sale_target'] == '0') ? $this->getTarget($post['kpi_sale_target_weeks']) : $this->getTarget($post['kpi_sale_target']);
            $post['strike_rate_target'] = ($post['strike_rate_target'] == '0') ? $this->getTarget($post['strike_rate_target_weeks']) : $this->getTarget($post['strike_rate_target']);
            $post['total_grip_target'] = ($post['total_grip_target'] == '0') ? $this->getTarget($post['total_grip_target_weeks']) : $this->getTarget($post['total_grip_target']);
            $post['total_grab_target'] = ($post['total_grab_target'] == '0') ? $this->getTarget($post['total_grab_target_weeks']) : $this->getTarget($post['total_grab_target']);
            $post['potential_gripped_target'] = ($post['potential_gripped_target'] == '0') ? $this->getTarget($post['potential_gripped_target_weeks']) : $this->getTarget($post['potential_gripped_target']);

            $post['kpi_call_target_weeks'] = $this->correctWeekData($post['kpi_call_target_weeks']);
            $post['kpi_new_grab_target_weeks'] = $this->correctWeekData($post['kpi_new_grab_target_weeks']);
            $post['kpi_new_grip_target_weeks'] = $this->correctWeekData($post['kpi_new_grip_target_weeks']);
            $post['total_grab_target_weeks'] = $this->correctWeekData($post['total_grab_target_weeks']);
            $post['total_grip_target_weeks'] = $this->correctWeekData($post['total_grip_target_weeks']);
            $post['kpi_sale_target_weeks'] = $this->correctWeekData($post['kpi_sale_target_weeks']);
            $post['strike_rate_target_weeks'] = $this->correctWeekData($post['strike_rate_target_weeks']);
            $post['average_call_per_working_day_target_weeks'] = $this->correctWeekData($post['average_call_per_working_day_target_weeks']);
            $post['average_call_per_actual_working_day_target_weeks'] = $this->correctWeekData($post['average_call_per_actual_working_day_target_weeks']);
            $post['pantry_check_target_weeks'] = $this->correctWeekData($post['pantry_check_target_weeks']);
            $post['sampling_target_weeks'] = $this->correctWeekData($post['sampling_target_weeks']);
            $post['otm_a_fulfilment_rate_target_weeks'] = $this->correctWeekData($post['otm_a_fulfilment_rate_target_weeks']);
            $post['otm_b_fulfilment_rate_target_weeks'] = $this->correctWeekData($post['otm_b_fulfilment_rate_target_weeks']);
            $post['otm_a_call_frequency_target_weeks'] = $this->correctWeekData($post['otm_a_call_frequency_target_weeks']);
            $post['otm_b_call_frequency_target_weeks'] = $this->correctWeekData($post['otm_b_call_frequency_target_weeks']);
            $post['otm_a_average_call_time_target_weeks'] = $this->correctWeekData($post['otm_a_average_call_time_target_weeks']);
            $post['otm_b_average_call_time_target_weeks'] = $this->correctWeekData($post['otm_b_average_call_time_target_weeks']);
            $post['total_tfo_target_weeks'] = $this->correctWeekData($post['total_tfo_target_weeks']);
            $post['total_tfo_value_target_weeks'] = $this->correctWeekData($post['total_tfo_value_target_weeks']);
            $post['average_grab_target_weeks'] = $this->correctWeekData($post['average_grab_target_weeks']);
            $post['push_calls_made_target_weeks'] = $this->correctWeekData($post['push_calls_made_target_weeks']);
            $post['push_call_achievement_target_weeks'] = $this->correctWeekData($post['push_call_achievement_target_weeks']);
            $post['push_route_plan_coverage_target_weeks'] = $this->correctWeekData($post['push_route_plan_coverage_target_weeks']);
            $post['push_tfo_made_target_weeks'] = $this->correctWeekData($post['push_tfo_made_target_weeks']);
            $post['push_tfo_value_target_weeks'] = $this->correctWeekData($post['push_tfo_value_target_weeks']);
            $post['push_strike_rate_target_weeks'] = $this->correctWeekData($post['push_strike_rate_target_weeks']);
            $post['push_new_grab_target_weeks'] = $this->correctWeekData($post['push_new_grab_target_weeks']);
            $post['push_new_grip_target_weeks'] = $this->correctWeekData($post['push_new_grip_target_weeks']);
            $post['push_number_sku_per_ws_target_weeks'] = $this->correctWeekData($post['push_number_sku_per_ws_target_weeks']);
            $post['push_perfect_store_target_weeks'] = $this->correctWeekData($post['push_perfect_store_target_weeks']);
            $post['potential_gripped_target_weeks'] = $this->correctWeekData($post['potential_gripped_target_weeks']);


            $post['average_call_per_working_day_target'] = $this->getTarget($post['average_call_per_working_day_target']);
            $post['average_call_per_actual_working_day_target'] = $this->getTarget($post['average_call_per_actual_working_day_target']);
            $post['pantry_check_target'] = $this->getTarget($post['pantry_check_target']);
            $post['sampling_target'] = $this->getTarget($post['sampling_target']);
            $post['otm_a_fulfilment_rate_target'] = $this->getTarget($post['otm_a_fulfilment_rate_target']);
            $post['otm_b_fulfilment_rate_target'] = $this->getTarget($post['otm_b_fulfilment_rate_target']);
            $post['otm_a_call_frequency_target'] = $this->getTarget($post['otm_a_call_frequency_target']);
            $post['otm_b_call_frequency_target'] = $this->getTarget($post['otm_b_call_frequency_target']);
            $post['otm_a_average_call_time_target'] = $this->getTarget($post['otm_a_average_call_time_target']);
            $post['otm_b_average_call_time_target'] = $this->getTarget($post['otm_b_average_call_time_target']);
            $post['total_tfo_target'] = $this->getTarget($post['total_tfo_target']);
            $post['total_tfo_value_target'] = $this->getTarget($post['total_tfo_value_target']);
            $post['average_grab_target'] = $this->getTarget($post['average_grab_target']);
            $post['push_calls_made_target'] = $this->getTarget($post['push_calls_made_target']);
            $post['push_call_achievement_target'] = $this->getTarget($post['push_call_achievement_target']);
            $post['push_route_plan_coverage_target'] = $this->getTarget($post['push_route_plan_coverage_target']);
            $post['push_tfo_made_target'] = $this->getTarget($post['push_tfo_made_target']);
            $post['push_tfo_value_target'] = $this->getTarget($post['push_tfo_value_target']);
            $post['push_strike_rate_target'] = $this->getTarget($post['push_strike_rate_target']);
            $post['push_new_grab_target'] = $this->getTarget($post['push_new_grab_target']);
            $post['push_new_grip_target'] = $this->getTarget($post['push_new_grip_target']);
            $post['push_number_sku_per_ws_target'] = $this->getTarget($post['push_number_sku_per_ws_target']);
            $post['push_perfect_store_target'] = $this->getTarget($post['push_perfect_store_target']);

            $this->data['post'] = $post;
            $this->assertCountry($this->data['post']);
            $this->data['page_title'] = page_title('Edit KPI Settings');
            $this->data['enablepotential'] = $this->checkPotential(); // hieu
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'kpi_target')) {
                    return $this->noPermission();
                }

                return $this->render('ami/kpi_target/preview1', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/kpi_target/edit1', $this->data);
            }
        }

        return redirect('ami/kpi_target');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'kpi_target')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->kpi_target_m->save(array(
                    'id' => $id,
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
            }
        }

        return redirect('ami/kpi_target');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_target')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi_target';
                //$this->roles_m->delete($id);
                $this->kpi_target_m->save(array(
                    'id' => $id,
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/kpi_target/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi_target');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_target')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi_target');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi_target/draft';
                $this->kpi_target_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/kpi_target/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi_target');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'kpi_target') && !$this->hasPermission('edit', 'kpi_target')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->kpi_target_m->array_from_post(array('month', 'year', 'id', 'armstrong_2_salespersons_id', 'type', 'kpi_call_target', 'kpi_new_grab_target', 'kpi_new_grip_target', 'total_grip_target', 'total_grip_target_weeks', 'total_grab_target', 'total_grab_target_weeks', 'kpi_sale_target', 'strike_rate_target', 'average_call_per_working_day_target', 'average_call_per_actual_working_day_target', 'pantry_check_target', 'sampling_target', 'otm_a_fulfilment_rate_target', 'otm_b_fulfilment_rate_target', 'otm_a_call_frequency_target', 'otm_b_call_frequency_target', 'otm_a_average_call_time_target', 'otm_b_average_call_time_target', 'total_tfo_target', 'total_tfo_value_target', 'average_grab_target', 'push_calls_made_target', 'push_call_achievement_target', 'push_route_plan_coverage_target', 'push_tfo_made_target', 'push_tfo_value_target', 'push_strike_rate_target', 'push_new_grab_target', 'push_new_grip_target', 'push_number_sku_per_ws_target', 'push_perfect_store_target', 'potential_gripped_target',
            'kpi_call_target_weeks', 'kpi_new_grab_target_weeks', 'kpi_new_grip_target_weeks', 'kpi_sale_target_weeks', 'strike_rate_target_weeks', 'average_call_per_working_day_target_weeks', 'average_call_per_actual_working_day_target_weeks', 'pantry_check_target_weeks', 'sampling_target_weeks', 'otm_a_fulfilment_rate_target_weeks', 'otm_b_fulfilment_rate_target_weeks', 'otm_a_call_frequency_target_weeks', 'otm_b_call_frequency_target_weeks', 'otm_a_average_call_time_target_weeks', 'otm_b_average_call_time_target_weeks', 'total_tfo_target_weeks', 'total_tfo_value_target_weeks', 'average_grab_target_weeks', 'push_calls_made_target_weeks', 'push_call_achievement_target_weeks', 'push_route_plan_coverage_target_weeks', 'push_tfo_made_target_weeks', 'push_tfo_value_target_weeks', 'push_strike_rate_target_weeks', 'push_new_grab_target_weeks', 'push_new_grip_target_weeks', 'push_number_sku_per_ws_target_weeks', 'push_perfect_store_target_weeks', 'potential_gripped_target_weeks'));

        $data['kpi_call_target'] = $data['kpi_call_target'] ? $data['kpi_call_target'] : 0;
        $data['kpi_new_grab_target'] = $data['kpi_new_grab_target'] ? $data['kpi_new_grab_target'] : 0;
        $data['kpi_new_grip_target'] = $data['kpi_new_grip_target'] ? $data['kpi_new_grip_target'] : 0;
        $data['total_grab_target'] = $data['total_grab_target'] ? $data['total_grab_target'] : 0;
        $data['total_grip_target'] = $data['total_grip_target'] ? $data['total_grip_target'] : 0;
        $data['kpi_sale_target'] = $data['kpi_sale_target'] ? $data['kpi_sale_target'] : 0;
        $data['strike_rate_target'] = $data['strike_rate_target'] ? $data['strike_rate_target'] : 0;
        $data['average_call_per_working_day_target'] = $data['average_call_per_working_day_target'] ? $data['average_call_per_working_day_target'] : 0;
        $data['average_call_per_actual_working_day_target'] = $data['average_call_per_actual_working_day_target'] ? $data['average_call_per_actual_working_day_target'] : 0;
        $data['pantry_check_target'] = $data['pantry_check_target'] ? $data['pantry_check_target'] : 0;
        $data['sampling_target'] = $data['sampling_target'] ? $data['sampling_target'] : 0;
        $data['otm_a_fulfilment_rate_target'] = $data['otm_a_fulfilment_rate_target'] ? $data['otm_a_fulfilment_rate_target'] : 0;
        $data['otm_b_fulfilment_rate_target'] = $data['otm_b_fulfilment_rate_target'] ? $data['otm_b_fulfilment_rate_target'] : 0;
        $data['otm_a_call_frequency_target'] = $data['otm_a_call_frequency_target'] ? $data['otm_a_call_frequency_target'] : 0;
        $data['otm_b_call_frequency_target'] = $data['otm_b_call_frequency_target'] ? $data['otm_b_call_frequency_target'] : 0;
        $data['otm_a_average_call_time_target'] = $data['otm_a_average_call_time_target'] ? $data['otm_a_average_call_time_target'] : 0;
        $data['otm_b_average_call_time_target'] = $data['otm_b_average_call_time_target'] ? $data['otm_b_average_call_time_target'] : 0;
        $data['total_tfo_target'] = $data['total_tfo_target'] ? $data['total_tfo_target'] : 0;
        $data['total_tfo_value_target'] = $data['total_tfo_value_target'] ? $data['total_tfo_value_target'] : 0;
        $data['average_grab_target'] = $data['average_grab_target'] ? $data['average_grab_target'] : 0;
//        $data['push_calls_made_target'] = $data['push_calls_made_target'] ? $data['push_calls_made_target'] : 0;
        $data['push_call_achievement_target'] = $data['push_call_achievement_target'] ? $data['push_call_achievement_target'] : 0;
        $data['push_route_plan_coverage_target'] = $data['push_route_plan_coverage_target'] ? $data['push_route_plan_coverage_target'] : 0;
//        $data['push_tfo_made_target'] = $data['push_tfo_made_target'] ? $data['push_tfo_made_target'] : 0;
//        $data['push_tfo_value_target'] = $data['push_tfo_value_target'] ? $data['push_tfo_value_target'] : 0;
//        $data['push_strike_rate_target'] = $data['push_strike_rate_target'] ? $data['push_strike_rate_target'] : 0;
//        $data['push_new_grab_target'] = $data['push_new_grab_target'] ? $data['push_new_grab_target'] : 0;
//        $data['push_new_grip_target'] = $data['push_new_grip_target'] ? $data['push_new_grip_target'] : 0;
        $data['push_number_sku_per_ws_target'] = $data['push_number_sku_per_ws_target'] ? $data['push_number_sku_per_ws_target'] : 0;
        $data['push_perfect_store_target'] = $data['push_perfect_store_target'] ? $data['push_perfect_store_target'] : 0;
        $data['potential_gripped_target'] = $data['potential_gripped_target'] ? $data['potential_gripped_target'] : 0;

        if ($data['type'] == 'PULL') { // Pull view
            unset($data['push_calls_made_target']);
            unset($data['push_call_achievement_target']);
            unset($data['push_route_plan_coverage_target']);
            unset($data['push_tfo_made_target']);
            unset($data['push_tfo_value_target']);
            unset($data['push_strike_rate_target']);
            unset($data['push_new_grab_target']);
            unset($data['push_new_grip_target']);
            unset($data['push_number_sku_per_ws_target']);
            unset($data['push_perfect_store_target']);
            unset($data['push_calls_made_target_weeks']);
            unset($data['push_call_achievement_target_weeks']);
            unset($data['push_route_plan_coverage_target_weeks']);
            unset($data['push_tfo_made_target_weeks']);
            unset($data['push_tfo_value_target_weeks']);
            unset($data['push_strike_rate_target_weeks']);
            unset($data['push_new_grab_target_weeks']);
            unset($data['push_new_grip_target_weeks']);
            unset($data['push_number_sku_per_ws_target_weeks']);
            unset($data['push_perfect_store_target_weeks']);
            $arr_each = array('kpi_call_target', 'total_grip_target', 'total_grab_target', 'kpi_new_grab_target', 'kpi_new_grip_target', 'kpi_sale_target', 'strike_rate_target', 'average_call_per_working_day_target', 'average_call_per_actual_working_day_target', 'pantry_check_target', 'sampling_target', 'otm_a_fulfilment_rate_target', 'otm_b_fulfilment_rate_target', 'otm_a_call_frequency_target', 'otm_b_call_frequency_target', 'otm_a_average_call_time_target', 'otm_b_average_call_time_target', 'total_tfo_target', 'total_tfo_value_target', 'average_grab_target', 'potential_gripped_target');
        } else { // push view
            unset($data['average_call_per_working_day_target']);
            unset($data['average_call_per_actual_working_day_target']);
            unset($data['pantry_check_target']);
            unset($data['sampling_target']);
            unset($data['otm_a_fulfilment_rate_target']);
            unset($data['otm_b_fulfilment_rate_target']);
            unset($data['otm_a_call_frequency_target']);
            unset($data['otm_b_call_frequency_target']);
            unset($data['otm_a_average_call_time_target']);
            unset($data['otm_b_average_call_time_target']);
            unset($data['total_tfo_value_target']);
            unset($data['average_grab_target']);
            unset($data['average_call_per_working_day_target_weeks']);
            unset($data['average_call_per_actual_working_day_target_weeks']);
            unset($data['pantry_check_target_weeks']);
            unset($data['sampling_target_weeks']);
            unset($data['otm_a_fulfilment_rate_target_weeks']);
            unset($data['otm_b_fulfilment_rate_target_weeks']);
            unset($data['otm_a_call_frequency_target_weeks']);
            unset($data['otm_b_call_frequency_target_weeks']);
            unset($data['otm_a_average_call_time_target_weeks']);
            unset($data['otm_b_average_call_time_target_weeks']);
            unset($data['total_tfo_value_target_weeks']);
            unset($data['average_grab_target_weeks']);
            unset($data['total_grip_target']);
            unset($data['total_grab_target']);
            unset($data['total_grip_target_weeks']);
            unset($data['total_grab_target_weeks']);
            unset($data['potential_gripped_target']);
            unset($data['potential_gripped_target_weeks']);
            $arr_each = array('kpi_call_target', 'push_call_achievement_target', 'push_route_plan_coverage_target', 'kpi_sale_target', 'strike_rate_target', 'kpi_new_grab_target', 'kpi_new_grip_target', 'total_tfo_target', 'push_number_sku_per_ws_target', 'push_perfect_store_target');
        }
        for ($j = 0; $j < count($arr_each); $j++) {
            $str = '';
            for ($i = 0; $i < count($data[$arr_each[$j] . '_weeks']); $i++) {
                if ($data[$arr_each[$j] . '_weeks'][$i] == '') {
                    $data[$arr_each[$j] . '_weeks'][$i] = 0;
                }
                $str .= $data[$arr_each[$j] . '_weeks'][$i] . ',';

            }

            $str .= $data[$arr_each[$j]];
            $data[$arr_each[$j] . '_weeks'] = $str;
            $data[$arr_each[$j]] = $data[$arr_each[$j] . '_weeks'];
        }

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;
        unset($data['type']);
        $newId = $this->kpi_target_m->save($data, $id);

        return redirect('ami/kpi_target');
    }

    public function ajaxGetSalespersons()
    {
        $get = $this->input->get();
        $html = '';
        $type = isset($get['type']) ? $get['type'] : '';
        $sp_id = isset($get['salesperson']) ? $get['salesperson'] : '';
        $conditions = array('country_id' => $this->country_id, 'type' => $type, 'is_draft' => 0);
        $salespersons = $this->salespersons_m->getPersonListOptions('- Select -', $conditions, true);
        if ($salespersons) {
            foreach ($salespersons as $k => $s) {
                if ($k == $sp_id) {
                    $html .= '<option value="' . $k . '" selected>' . $s . '</option>';
                } else {
                    $html .= '<option value="' . $k . '">' . $s . '</option>';
                }

            }
        }
        echo $html;
        exit;
    }

    function getTarget($str)
    {
        $result = '';
        $arr = explode(',', $str);
        if (is_array($arr)) {
            $result = end($arr);
        }
        return $result;
    }

    function correctWeekData($data)
    {
        $result = $data ? explode(',', $data) : array();
        if (count($result) == 5) {
            $total = $result[4];
            unset($result[4]);
            array_push($result, 0, $total);
        }
        return $result;
    }
}