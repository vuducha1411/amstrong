<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Eloquent\Formula;
use Eloquent\FormulaRef;
use Eloquent\Tfo;

class Minitools extends AMI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('minitools_m');
        $this->load->model('psd_m');
        $this->load->model('psd_dist_m');
        $this->load->model('tfo_m');
        $this->load->model('wholesalers_m');
    }
    public function index()
    {
        return $this->render('ami/minitools/index', $this->data);
    }
    private $_tablesmigration = array(
        'call_records' => 'armstrong_2_call_records_id',
        'call_records_summary' => 'id',
        'classification_customer' => 'id',
        'contacts' => 'id',
        'contacts_amended_data' => 'id',
      //  'customer_mapping' => '',
        'customers_photos' => 'id',
        'pantry_check_details' => 'id',
        'perfect_store_cc_call_records' => 'id',
        'prepare_calls' => 'armstrong_2_prepare_calls_id',
        'quality_control' => 'id',
        'question_answers' => 'id',
        'route_master_temp' => 'id',
        'route_plan' => 'armstrong_2_route_plan_id',
        'sampling_details' => 'id',
        'temp_call_records' => 'armstrong_2_call_records_id',
        'uplift' => 'id',
        'customers_dish' => 'id',
        'customers_dish_products' => 'id',
        'tfo_details' => 'id',
    );
    public $_tableupdatename = array(
      'pantry_check_details',
      'sampling_details',
      'tfo_details',
    );
    public function log($file_log, $fields)
    {
        $file = FCPATH.'res/up/minitools/logs/'.$file_log.'.csv';
        if(!file_exists($file))
        {
            file_put_contents($file, $this->csvescape($fields));
        }
        else
        {
            file_put_contents($file, "\n".$this->csvescape($fields), FILE_APPEND);
        }
    }
    public function import()
    {
        if ($this->is('POST') && isset($_GET['exelname']) && $_GET['exelname'] != '')
        {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/minitools',
                'allow_extensions' => 'csv'
            ));

            if (!$upload)
            {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            }
            else
            {
                if (!empty($upload['path']))
                {
                    $file_migration = 'res/up/minitools/'.$_GET['exelname'].'.csv';
                    if(file_exists($file_migration))
                    {
                        unlink($file_migration);
                    }
                    rename($upload['path'], $file_migration);
                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }
                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
        else
        {
            return $this->json(array(
                'OK' => 0,
                'error' => array(
                    'code' => 1,
                    'message' => 'Errors'
                )
            ));
        }
    }
    public function logtfo($table, $key, $cus_id, $who_id, $datatfo, $datatfo, $note)
    {
        $file = FCPATH.'res/up/minitools/logs/'.$table.'.csv';
        if(!file_exists($file))
        {
            $titles = array('table', 'key', 'customer_id', 'wholesaler_id', 'psd_id', 'tfo_id', 'note');
            file_put_contents($file, implode(',', $titles));
        }
        $fields = array($table, $key, $cus_id, $who_id, $datatfo, $datatfo, $note);
        file_put_contents($file, "\n".implode(',', $fields), FILE_APPEND);
    }
    public function migration()
    {
        $this->data['tables'] = $this->_tablesmigration;
        $this->data['exelname'] = 'Migration_TW_PULL_to_PUSH';
        $this->data['customers'] = $this->getCustomerMigration();
        return $this->render('ami/minitools/migration', $this->data);
    }
    public function runmigration()
    {
        if(isset($_GET['table']))
        {
            $table = trim($_GET['table']);
            $table_log = $table.'_'.date('Y_m_d_H_i_s');
            if(!key_exists($table, $this->_tablesmigration))
            {
                die('Khong the migration bảng này 2');
            }
            $customers = $this->getCustomerMigration();
            if(!$customers) die('Khong co customer');
            $this->log($table_log, array(
                'Date',
                'Customer',
                'Whosaler',
                'Table',
                'Id',
                'SQL',
                'Column effect',
                'Status',
            ));
            $now = date('Y-m-d H:i:s');
            foreach($customers as $cus)
            {
                $whosaler = $this->wholesalers_m->get_by(array('armstrong_2_wholesalers_id' => $cus[1]));
                $whosaler_name = '';
                if($whosaler)
                {
                    $whosaler_name = $whosaler[0]['name'];
                }
                $sql = "SELECT * FROM `".$table."` WHERE `armstrong_2_customers_id` = '".$cus[0]."'";
                $results = $this->db->query($sql)->result_array();
                if($results)
                {
                    foreach($results as $result)
                    {
                        $app_type = (array_key_exists("app_type", $result) ? ", `app_type` = 1" : "");

                        if(in_array($table, $this->_tableupdatename))
                        {
                            $sql = "UPDATE `".$table."` SET `last_updated` = '".$now."', `armstrong_2_customers_id` = '".$cus[1]."', `armstrong_2_customers_name` = '".$whosaler_name."'".$app_type." WHERE `".$this->_tablesmigration[$table]."` = '".$result[$this->_tablesmigration[$table]]."'";
                        }
                        else
                        {
                            $sql = "UPDATE `".$table."` SET `last_updated` = '".$now."', `armstrong_2_customers_id` = '".$cus[1]."'".$app_type." WHERE `".$this->_tablesmigration[$table]."` = '".$result[$this->_tablesmigration[$table]]."'";
                        }
                        $log = array(
                            $now,
                            $cus[0],
                            $cus[1],
                            $table,
                            $result[$this->_tablesmigration[$table]],
                            $sql,
                        );
                        if($this->db->query($sql))
                        {
                            $log[] = $this->db->affected_rows();
                            $log[] = 'Success';
                        }
                        else
                        {
                            $log[] = 'N/A';
                            $log[] = $this->db->_error_message();
                        }
                        $this->log($table_log, $log);
                    }
                }
                else
                {
                    $log = array(
                        $now,
                        $cus[0],
                        $cus[1],
                        $table,
                        '',
                        '',
                        '',
                        'No Data',
                    );
                    $this->log($table_log, $log);
                }
            }
            echo 'xong, dowload file logs in <a href="'.site_url('/res/up/minitools/logs/'.$table_log.'.csv" >here</a>');
        }
        else
        {
            die('Khong the migration bảng này');
        }
    }
    public function movetfo()
    {
        $table = 'tfo';
        $table_log = $table.'_'.date('Y_m_d_H_i_s');
        $customers = $this->getCustomerMigration(); 
        if(!$customers) die('Khong co customer');
        $this->log($table_log, array(
            'Date',
            'Customer',
            'Whosaler',
            'TFO ID',
            'PSD ID',
            'Status',
        ));
        $now = date('Y-m-d H:i:s');
        foreach($customers as $cus)
        {
            $sql = "SELECT * FROM `".$table."` WHERE armstrong_2_customers_id = '".$cus[0]."'";
            $results = $this->db->query($sql)->result_array();
        
            if($results)
            { 
                foreach($results as $result)
                {
                    $tfo_id = $result['armstrong_2_tfo_id'];
                    unset($result['armstrong_2_tfo_id']);
                    unset($result['free_gifts']);
                    unset($result['delivery_address']);
                    unset($result['attention_to']);
                    unset($result['id']);
                    $result['armstrong_2_customers_id'] = $cus[1];
                    $result['last_updated'] = $now;
                    $condition_exist = array(
                        'armstrong_2_customers_id' => $result['armstrong_2_customers_id'],
                        'armstrong_2_salespersons_id' => $result['armstrong_2_salespersons_id'],
                        'armstrong_2_wholesalers_id' => $result['armstrong_2_wholesalers_id'],
                        'armstrong_2_distributors_id' => $result['armstrong_2_distributors_id'],
                        'armstrong_2_call_records_id' => $result['armstrong_2_call_records_id'],
                        'delivery_date' => $result['delivery_date'],
                        'email' => $result['email'],
                    );
                    $string = substr($result['armstrong_2_customers_id'],0,2);
                    if($string == 'WH'){
                        $psd_exist = $this->psd_m->get_by($condition_exist);
                        $log = array(
                            $now,
                            $cus[0],
                            $cus[1],
                            $tfo_id,
                        );
                        if(!$psd_exist)
                        {
                            $psd_id = $this->psd_m->save($result, NULL, true, "TRANFER", $result['country_id']);
                            $sql = "UPDATE tfo_details SET armstrong_2_tfo_id = '".$psd_id."' WHERE armstrong_2_tfo_id = '".$tfo_id."'";
                            $this->db->query($sql);
                            $log[] = $psd_id;
                            $log[] = 'Success';
                        } else {
                            /* Fix issue on date_created and sync equal with last_updated */
                            if (($result['date_created'] == $psd_exist[0]['date_created']) || ($result['date_sync'] == $psd_exist[0]['date_sync'])) {
                                $log[] = $psd_exist[0]['armstrong_2_psd_id'];
                                $log[] = 'Duplicate';
                            } else {
                                $sql = "UPDATE psd SET date_created = '" . $result['date_created'] . "', date_sync = '" . $result['date_sync'] . "', last_updated='".$now."' WHERE armstrong_2_psd_id = '" . $psd_exist[0]['armstrong_2_psd_id'] . "'";
                                $this->db->query($sql);
                                $log[] = $psd_exist[0]['armstrong_2_psd_id'];
                                $log[] = 'Updated';
                            }
                        }
                    } elseif ($string == 'DI') {
                        $psd_exist = $this->psd_dist_m->get_by($condition_exist);
                        $log = array(
                            $now,
                            $cus[0],
                            $cus[1],
                            $tfo_id,
                        );
                        if (!$psd_exist) {
                            $psd_id = $this->psd_dist_m->save($result, NULL, true, "TRANFER", $result['country_id']);
                            $sql = "UPDATE tfo_details SET armstrong_2_tfo_id = '" . $psd_id . "' WHERE armstrong_2_tfo_id = '" . $tfo_id . "'";
                            $this->db->query($sql);
                            $log[] = $psd_id;
                            $log[] = 'Success';
                        } else {
                            if (($result['date_created'] == $psd_exist[0]['date_created']) || ($result['date_sync'] == $psd_exist[0]['date_sync'])) {
                                $log[] = $psd_exist[0]['armstrong_2_psd_dist_id'];
                                $log[] = 'Duplicate';
                            } else {
                                $sql = "UPDATE psd SET date_created = '" . $result['date_created'] . "', date_sync = '" . $result['date_sync'] . "', last_updated='".$now."' WHERE armstrong_2_psd_id = '" . $psd_exist[0]['armstrong_2_psd_id'] . "'";
                                $this->db->query($sql);
                                $log[] = $psd_exist[0]['armstrong_2_psd_id'];
                                $log[] = 'Updated';
                            }
                        }
                    }

                    $this->log($table_log, $log);
                    $this->tfo_m->save(array(
                        'is_draft' => 1,
                        'last_updated' => $now
                    ), $tfo_id);
                }
            } else {
                $log = array(
                    $now,
                    $cus[0],
                    $cus[1],
                    '',
                    '',
                    'No data',
                );
                $this->log($table_log, $log);
            }
        }
        echo 'xong, dowload file logs in <a href="'.site_url('/res/up/minitools/logs/'.$table_log.'.csv" >here</a>');
    }
    private function getCustomerMigration()
    {
        $file = FCPATH.'res/up/minitools/Migration_TW_PULL_to_PUSH.csv';
        if(!file_exists($file))
        {
            return array();
        }
        $row = 1;
        $cus = array();
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if(count($data) == 2)
                {
                    $cus[] = $data;
                }
            }
            fclose($handle);
        }
        return $cus;
    }
    //1745
    private function getTfoSwaps()
    {
        $file = FCPATH.'res/up/minitools/Tfo_Swap.csv';
        if(!file_exists($file))
        {
            return array();
        }
        $cus = array();
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cus[] = $data;
            }
            fclose($handle);
        }
        return $cus;
    }
    function csvescape($values) {
        $line = '';
        $values = array_map(function ($v) {
            if(is_array($v) || is_object($v))
            {
                $v = json_encode($v);
            }
            return '"' . str_replace('"', '""', $v) . '"';
        }, $values);
        $line .= implode(',', $values);

        return $line;
    }
    public function swapsku()
    {
        $this->data['tfoswap'] = $this->getTfoSwaps();
        $this->data['exelname'] = 'Tfo_Swap';
        return $this->render('ami/minitools/swapsku', $this->data);
    }
    public function runswapsku()
    {
        $filename = time();
        $file = FCPATH.'res/up/minitools/logs/swaptfo-'.$filename.'.csv';
        $titles = array('TFO ID', 'CALL ID', 'OLD SKU', 'NEW SKU', 'PRODUCT OLD', 'PRODUCT NEW', 'STATUS');
        file_put_contents($file, implode(',', $titles));
        $tfoswaps = $this->getTfoSwaps();
        $now = date('Y-m-d H:i:s');
        foreach($tfoswaps as $tfo)
        {
            $sql = "SELECT * FROM `tfo` WHERE armstrong_2_tfo_id = '".$tfo[0]."'";
            $results = $this->db->query($sql)->result_array();
            foreach($results as $r)
            {
                if($r['products'] != '' && strpos($r['products'],$tfo[2]))
                {
                    $product = str_replace('"sku_number":"'.$tfo[2].'"', '"sku_number":"'.$tfo[3].'"', $r['products']);
                    $ss = $this->tfo_m->update_in(array('products' => $product, 'last_updated' => $now), 'armstrong_2_tfo_id', array($tfo[0]));
                    if($ss)
                    {
                        $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], $r['products'], $product, 'ok');
                    }
                    else
                    {
                        $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], $r['products'], $product, 'error');
                    }
                }
                else
                {
                    $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], $r['products'], '', '', 'check_again');
                }
                file_put_contents($file, "\n".$this->csvescape($logs), FILE_APPEND);
            }
        }
        echo 'xong, dowload file logs in <a href="'.site_url('/res/up/minitools/logs/swaptfo-'.$filename.'.csv" >here</a>');
    }

    public $product_infor = array();
    public function getProductInfor($sku_number)
    {
        if(isset($this->product_infor[$sku_number]))
        {
            return $this->product_infor[$sku_number];
        }
        $sql = "SELECT * FROM `products` WHERE sku_number = '".$sku_number."'";
        if($results = $this->db->query($sql)->result_array())
        {
            $this->product_infor[$sku_number] = $results[0];
            return $this->product_infor[$sku_number];
        }
        return false;
    }
    public function runswapskudetails()
    {
        $filename = time();
        $file = FCPATH.'res/up/minitools/logs/swaptfo-details-'.$filename.'.csv';
        $titles = array('TFO ID', 'CALL ID', 'OLD SKU', 'NEW SKU', 'DATA OLD', 'DATA NEW', 'ID UPDATE', 'STATUS');
        file_put_contents($file, implode(',', $titles));
        $tfoswaps = $this->getTfoSwaps();
        $now = date('Y-m-d H:i:s');
        foreach($tfoswaps as $tfo)
        {
            $sql = "SELECT * FROM `tfo_details` WHERE armstrong_2_tfo_id = '".$tfo[0]."'";
            $results = $this->db->query($sql)->result_array();
            foreach($results as $r)
            {
                if($r['sku_number'] == $tfo[2])
                {
                    if($product = $this->getProductInfor($tfo[3]))
                    {
                        $update_data = array('sku_number' => $product['sku_number'], 'sku_name' => $product['sku_name'], 'last_updated' => $now);
                        $ss = $this->db->where_in('id', $r['id'])->update('tfo_details', $update_data);
                        if($ss)
                        {
                            $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], json_encode($product), json_encode($update_data), $r['id'], 'ok');
                        }
                        else
                        {
                            $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], json_encode($product), json_encode($update_data), $r['id'], 'error');
                        }
                    }
                    else
                    {
                        $logs = array($tfo[0], $tfo[1], $tfo[2], $tfo[3], 'Product not exist', '', '', 'Product not exist');
                    }
                    file_put_contents($file, "\n".$this->csvescape($logs), FILE_APPEND);
                }
            }
        }
        echo 'xong, dowload file logs in <a href="'.site_url('/res/up/minitools/logs/swaptfo-details-'.$filename.'.csv" >here</a>');
    }
    // function update app_type table quanlity control based on log_id
    public function update_apptype(){
        $table = 'quality_control';
        $sql = "UPDATE `".$table."` SET `app_type` = 0 WHERE `log_ids` LIKE 'P%'";
        $this->db->query($sql);
        echo $this->db->affected_rows() . " ffected rows app type pull<br>";
        $sql = "UPDATE `".$table."` SET `app_type` = 1 WHERE `log_ids` LIKE 'U%'";
        $this->db->query($sql);
        echo $this->db->affected_rows() . " affected rows app type push";
    }

    //function update all record in table working_hour have time equal time in country setting
    public function update_working_hour(){
        $sql_country = "SELECT `country_id`,`no_of_working_hours_per_day` FROM `out_of_trade`";
        $working_hours_of_country = $this->db->query($sql_country)->result_array();
        $num_effect = 0;
        $date = date('Y-m-d H:i:s');
        foreach($working_hours_of_country as $value){
            $sql = "UPDATE working_hours SET is_draft = 1, last_updated = '".$date."' WHERE country_id = '".$value['country_id']."' AND no_of_hours = '".$value['no_of_working_hours_per_day']."'"  ;
            $this->db->query($sql);
            $num_effect += $this->db->affected_rows();
        }
        echo 'Number rows is change to draff : '.$num_effect;
    }

    public function updateIdPsd(){
        $sql = "SELECT * FROM production.psd where armstrong_2_psd_id not like 'PSD%' and last_updated>'2017-04-15'";
        $results = $this->db->query($sql)->result_array();
        $list_psd = 'PSD : ';
        foreach ($results as $result){
            $list_psd .= $result['id'].' ,';
            $armstrong_2_id = $this->psd_m->generateArmstrongIdBaseId($result['id'],'psd', $result['country_id'], $result['armstrong_2_psd_id']);
            $sql = "UPDATE tfo_details SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_tfo_id = '".$armstrong_2_id."' WHERE armstrong_2_tfo_id = '".$result['armstrong_2_psd_id']."'";
            $this->db->query($sql);
            $sql = "UPDATE tfo_point SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_tfo_id = '".$armstrong_2_id."' WHERE armstrong_2_tfo_id = '".$result['armstrong_2_psd_id']."'";
            $this->db->query($sql);
            $sql = "UPDATE psd SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_psd_id = '".$armstrong_2_id."' WHERE armstrong_2_psd_id = '".$result['armstrong_2_psd_id']."'";
            $this->db->query($sql);
        }

        $sql_dist = "SELECT * FROM production.psd_dist where armstrong_2_psd_dist_id not like 'PD%' and last_updated>'2017-04-15'";
        $results_dist = $this->db->query($sql_dist)->result_array();
        $list_psd_dist = '<br>PSD_DIST :';
        foreach ($results_dist as $result){
            $list_psd_dist .= $result['id'].' ,';
            $armstrong_2_id = $this->psd_dist_m->generateArmstrongIdBaseId($result['id'],'psd_dist', $result['country_id'], $result['armstrong_2_psd_dist_id']);
            $sql_dist = "UPDATE tfo_details SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_tfo_id = '".$armstrong_2_id."' WHERE armstrong_2_tfo_id = '".$result['armstrong_2_psd_dist_id']."'";
            $this->db->query($sql_dist);
            $sql_dist = "UPDATE tfo_point SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_tfo_id = '".$armstrong_2_id."' WHERE armstrong_2_tfo_id = '".$result['armstrong_2_psd_dist_id']."'";
            $this->db->query($sql_dist);
            $sql_dist = "UPDATE psd_dist SET `last_updated` = '2017-05-04 18:45:04', armstrong_2_psd_dist_id = '".$armstrong_2_id."' WHERE armstrong_2_psd_dist_id = '".$result['armstrong_2_psd_dist_id']."'";
            $this->db->query($sql_dist);
        }

        echo '<pre>';
        print_r($list_psd.$list_psd_dist);
        echo '</pre>';
        exit;
    }
}
