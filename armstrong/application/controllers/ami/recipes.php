<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Recipes extends AMI_Controller {

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('recipes_m');
        $this->load->model('media_m');
        $this->load->model('cuisine_channels_m');
        $this->load->model('products_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'recipes'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        // $this->data['recipes'] = $this->recipes_m->get_limit($this->getPageLimit(), $conditions);

        $this->data['recipes'] = array(
            'active' => $this->recipes_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->recipes_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        $this->data['total'] = $this->recipes_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Recipes');

        return $this->render('ami/recipes/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'recipes'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        // $this->data['recipes'] = $this->recipes_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['recipes'] = array(
            'active' => $this->recipes_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->recipes_m->get_by(array_merge($conditions, array('listed' => 0))),
        );

        $this->data['total'] = $this->recipes_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/recipes/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            'cuisine_channels' => $this->cuisine_channels_m->parse_form($cols, '- Select -', $conditions),
            'products' => $this->products_m->getProductListOptions('- Select -', $conditions),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'recipes'))
        {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['recipe']['listed'] = 1;
        return $this->render('ami/recipes/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'recipes') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/recipes');

        if ($id = intval($id))
        {
            $this->data['recipe'] = $this->recipes_m->get($id);
            $this->assertCountry($this->data['recipe']);
            $this->data['recipe']['product_ids'] = $this->data['recipe']['sku_number'] ? explode(',', $this->data['recipe']['sku_number']) : [];
            $validate_listed_products = array();
            if($this->data['recipe']['product_ids'])
            {
                $validate_listed_products = $this->db->from('products')
                    ->select('sku_number')
                    ->where_in('sku_number', $this->data['recipe']['product_ids'])
                    ->where('listed', 1)
                    ->where('is_draft', 0)
                    ->group_by('sku_number')
                    ->get()->result_array();
            }
            if($validate_listed_products){
                $this->data['recipe']['product_ids'] = [];
                foreach($validate_listed_products as $sku_number){
                    $this->data['recipe']['product_ids'][] = $sku_number['sku_number'];
                }
            }
            $this->data['images'] = $this->media_m->getMedia('image', 'recipes', $this->country_id, $id);
            $this->data['videos'] = $this->media_m->getMedia('video', 'recipes', $this->country_id, $id);
            $this->data['brochures'] = $this->media_m->getMedia('brochure', 'recipes', $this->country_id, $id);
            $this->data['page_title'] = page_title('Edit Recipes');

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'recipes'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/recipes/preview', $this->data);
            }
            else
            {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/recipes/edit', $this->data);
            }
        }

        return redirect('ami/recipes');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'recipes'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id)
        {
            if ($id = intval($id))
            {
                $this->recipes_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/recipes');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'recipes'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/recipes');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/recipes';
                //$this->recipes_m->delete($id);
                $this->recipes_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/recipes/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Recipes"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/recipes');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'recipes'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/recipes');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/recipes/draft';
                $this->recipes_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/recipes/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Recipes"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/recipes');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'recipes') && !$this->hasPermission('edit', 'recipes'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->recipes_m->array_from_post(array('name', 'name_alt', 'preparation', 'ingredients',
            'cuisine_channels_id', 'listed', 'sku_number','id'));

        $data['country_id'] = $this->country_id;
        $data['preparation'] = str_replace('<br />', '<br>', $data['preparation']);
        $data['ingredients'] = str_replace('<br />', '<br>', $data['ingredients']);
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        $sku_number = array();

        if ($data['sku_number'])
        {
            foreach ($data['sku_number'] as $number)
            {
                $number = intval($number);

                if ($number > 0)
                {
                    $sku_number[] = $number;
                }
            }
        }

        $data['sku_number'] = implode(',', $sku_number);

        $id = $data['id'] ? $data['id'] : null;

        $newId = $this->recipes_m->save($data, $id);

        // Update existing record
        if ($id)
        {
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink)
            {
                foreach ($mediaUnlink as $unlink)
                {
                    $this->media_m->deleteMediaRef('recipes', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');

        if ($mediaChange)
        {
            foreach ($mediaChange as $change)
            {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'recipes',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        $brochureChangeAppType = $this->input->post('brochure_app_type');
        if($brochureChangeAppType){
            foreach($brochureChangeAppType as $media_id => $changeAppType){
                $this->media_m->updateMediaRef(
                    array(
                        'app_type' => $changeAppType,
                        'last_updated' => date('Y-m-d H:i:s')
                    ),
                    array(
                        'media_id' => $media_id,
                        'entry_id' => $id,
                        'entry_type' => 'recipes',
                        'country_id' => $this->country_id
                    )
                );
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId')))
        {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media)
            {
                foreach ($media as $item)
                {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'recipes';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);
                }
            }
        }

        return redirect('ami/recipes');
    }
}