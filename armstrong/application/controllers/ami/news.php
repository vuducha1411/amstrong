<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News extends AMI_Controller {

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('news_m');
        $this->load->model('media_m');        
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $news = $this->news_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $news = $this->news_m->prepareDataTables($news);

        return $this->datatables->generate($news);
    }

    public function index() 
    {
        if (!$this->hasPermission('view', 'news'))
        {
            return $this->noPermission();
        }

        $conditions = array("(country_id = {$this->country_id} || global = 1)" => null, 'is_draft' => 0);

        //$posts = $this->news_m->get_by(array('country_id' => $this->country_id, 'is_draft' => 0));        
        // $this->data['news'] = $this->news_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['news'] = array(
            'active' => $this->news_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->news_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        // $this->data['total'] = $this->news_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('News');

        return $this->render('ami/news/index', $this->data);
        // return $this->render('ami/news/index_ajax', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'news'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['news'] = $this->news_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->news_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('News');

        return $this->render('ami/news/index', $this->data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'news'))
        {
            return $this->noPermission();
        }
        $this->data['app_type'] = ['Pull', 'Push', 'All'];
        return $this->render('ami/news/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'news') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news');

        if ($id = intval($id))
        {
            $this->data['new'] = $this->news_m->get($id);
            $this->assertCountry($this->data['new']);
            $this->data['images'] = $this->media_m->getMedia('image', 'news', '', $id);
            $this->data['page_title'] = page_title('Edit News');
            $this->data['app_type'] = ['Pull', 'Push', 'All'];
            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'news'))
                {
                    return $this->noPermission();
                }
                
                return $this->render('ami/news/preview', $this->data);
            }
            else
            {
                return $this->render('ami/news/edit', $this->data);
            }
        }

        return redirect('ami/news');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'news'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id = intval($id))
            {
                $this->news_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/news');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'news'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/news';
                //$this->news_m->delete($id);
                $this->news_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/news/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from News"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/news');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'news'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news');

        if ($id = intval($id))
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/news/draft';
                $this->news_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/news/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from News"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/news');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'news') && !$this->hasPermission('edit', 'news'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->news_m->array_from_post(array('title', 'sub_title', 'content', 'summary', 'listed', 'id', 'global', 'app_type'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        $newId = $this->news_m->save($data, $id);

        // Update existing record
        if ($id)
        {
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink)
            {
                foreach ($mediaUnlink as $unlink) 
                {
                    $this->media_m->deleteMediaRef('news', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');

        if ($mediaChange)
        {
            foreach ($mediaChange as $change) 
            {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'news',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId')))
        {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media)
            {
                foreach ($media as $item) 
                {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'news';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);                                
                } 
            }               
        }

        return redirect('ami/news');
    }   
}