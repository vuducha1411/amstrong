<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wholesalers extends AMI_Controller
{
    public $customer_relate_history = array(
        'call_records', 'contacts', 'customers_dish', 'customers_dish_products', 'ssd', 'psd'
    );

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('wholesalers_m');
        $this->load->model('salespersons_m');
        $this->load->model('country_channels_m');
        $this->load->model('country_sub_channels_m');
        $this->load->model('business_types_m');
        $this->load->model('distributors_m');
        $this->load->library('CountryParser');
        $this->load->model('chains_m');
//        $this->load->model('wholesalers_amended_m');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'active':
                $filter = array('c.active' => 1);
                break;

            case 'pending':
                $filter = in_array($this->country_code, array('hk', 'tw')) ? array('c.approved IN (0,2)' => null) : array('c.approved' => 0);

                break;

            case 'inactive':
                $filter = array('c.active' => 0);
                break;

            default:
                $filter = array('c.active' => 1);
                break;
        }

        return $filter;
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'wholesalers')) {
            return $this->noPermission();
        }

        $this->data['transfer_btn'] = 2;
        $this->data['country_id'] = $this->country_id;
        $this->data['filter'] = $this->input->get('filter') ? $this->input->get('filter') : 'active';
        return $this->render('ami/wholesalers/index', $this->data);
    }

    public function ajaxData()
    {
        $user = $this->session->userdata('user_data');
        $filter = $this->_getFilter();
        $_filter = $this->input->get('filter') ? $this->input->get('filter') : 'active';

        $conditions = array_merge(array('c.country_id' => $this->country_id, 'c.is_draft' => 0), $filter);
//        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, 'approved' =>1);
      //  dd($conditions);
        if (!$this->isSuperAdmin()) {
            if ($this->hasPermission('manage_staff', 'salespersons')) {
                $salespersons = $this->db->select('armstrong_2_salespersons_id')
                    ->from('salespersons')
                    ->where('salespersons_manager_id', $user['id'])
                    ->get()
                    ->result_array();

                $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
                $salespersons_new = implode('","', array_merge(array($user['id']), array_keys($salespersons)));

                if ($salespersons_new) {

//                $conditions = array_merge(array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id IN ("'.$salespersons_new.'")' => null ), $filter);
                    $conditions['c.armstrong_2_salespersons_id IN ("' . $salespersons_new . '")'] = null;
                }
            }
        }
        $datatables = new Datatable(array('model' => 'Wholesalers_dt'));

        $this->wholesalers_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();

        foreach ($data['data'] as &$_data) {
            $_data['c']['updated_name'] = $_data['c']['updated_name']!='' ? $_data['c']['updated_name'] : 'OPS';
            if(isset($_data['c']['armstrong_2_wholesalers_id']))
            {
                $id = $_data['c']['armstrong_2_wholesalers_id'];
            }
            else
            {
                $id = '';
            }
            $_data['c']['armstrong_2_wholesalers_id'] = '
                <a href="' . site_url('ami/wholesalers/edit/' . $id) . '" data-toggle="ajaxModal">
                    ' . $id . '
                </a>
            ';

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('view', 'contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/customers/contacts/' . $id), '<i class="fa fa-group"></i>', array('class' => 'btn-default edit', 'title' => 'Contacts'));

            }

            if ($_filter != 'pending' && $this->hasPermission('edit', 'wholesalers')) {
                $_data['buttons'] .= html_btn(site_url('ami/wholesalers/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($_filter == 'pending') {
                if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')
                    || $this->hasPermission('manage', 'salespersons')
                ) {
                    $_data['buttons'] .= html_btn(site_url('ami/wholesalers/pending/' . $id . '/app'), '<i class="fa fa-thumbs-up"></i>', array('class' => 'btn-default approve', 'title' => 'Approve', 'data-toggle' => 'ajaxModal'));
                    $_data['buttons'] .= html_btn(site_url('ami/wholesalers/pending/' . $id . '/rej'), '<i class="fa fa-thumbs-down"></i>', array('class' => 'btn-default reject', 'title' => 'Reject', 'data-toggle' => 'ajaxModal'));
                }
            }

            if ($_filter != 'pending' && $this->hasPermission('delete', 'wholesalers')) {
                $_data['buttons'] .= html_btn(site_url('ami/wholesalers/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
            switch ($_data['c']['approved']) {
                case -1:
                    $_data['c']['approved'] = 'Reject';
                    break;
                case -2:
                    $_data['c']['approved'] = 'Close down';
                    break;
                case 0:
                    $_data['c']['approved'] = 'Pending';
                    break;
                case 1:
                    $_data['c']['approved'] = 'Approved';
                    break;
                case 2:
                    $_data['c']['approved'] = 'Pending When Edit';
                    break;

            }
//            $_data['c']['is_perfect_store'] = $_data['c']['is_perfect_store'] ?  'Yes' : 'No';
        }
        return $this->json($data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'wholesalers')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['wholesalers'] = $this->wholesalers_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->wholesalers_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/wholesalers/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $chains = $this->chains_m->getChainListOption('- Select -', array_merge($conditions, array('type' => 1, 'approved IN (0,1,2)' => null)));
        if ($this->country_id == 6) {
            if ($user['type'] == 'SL' && $user['sub_type'] == 'asm') {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, "(salespersons_manager_id ='" . $user['id'] . "' OR (type like '%SL%' OR type like '%ADMIN%'))" => null);
            } else {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);
            }
        } else {
            $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1,
                '(type LIKE "%push%" OR type LIKE "%mix%" OR type like "%TRAINER%")' => NULL
            );
        }

        $cols = array('id', 'name');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);
        $root = $this->countryparser->parse($this->country_code)->get($hierachy['root']);

        $rootValues = array('' => '-Select-');

        foreach ($root as $key => $value) {
            $rootValues[$value] = $value;
        }
        $salespersons = $this->salespersons_m->getPersonListOptions('- Select -', $conditions_salespersons, false);
        //hieu
        if(isset($this->data['wholesalers']['armstrong_2_salespersons_id']))
        {
            if(!array_key_exists($this->data['wholesalers']['armstrong_2_salespersons_id'], $salespersons))
            {
                if($temp_salespersons = $this->salespersons_m->get_by(array('armstrong_2_salespersons_id' => $this->data['wholesalers']['armstrong_2_salespersons_id'])))
                {
                    $salespersons[$this->data['wholesalers']['armstrong_2_salespersons_id']] = $temp_salespersons[0]['armstrong_2_salespersons_id'].' - '.$temp_salespersons[0]['first_name'].' '.$temp_salespersons[0]['last_name'];
                }
            }
        }
        $salespersons_str = substr(implode('","', array_keys($salespersons)), 2);
        $list_customers = [];
        if($salespersons_str) {
            $customers_conditions = array(
                'armstrong_2_salespersons_id IN (' . $salespersons_str . '")' => null,
                "is_draft" => 0
            );
            $customers = $this->wholesalers_m->get_by($customers_conditions);

            foreach ($customers as $key => $_customer) {
                $list_customers[$_customer['armstrong_2_salespersons_id']][$_customer['armstrong_2_wholesalers_id']] = $_customer['armstrong_2_wholesalers_id'] . ' - ' . $_customer['name'];
            }
        }
        $distributors = $this->distributors_m->parse_form_with_sale(array('armstrong_2_distributors_id', 'name', 'armstrong_2_distributors_id'), '- Select -', array_merge(array('active' => 1), $conditions));
        if ($this->country_id == 6) {
            $wholesalers = $this->wholesalers_m->parse_form_with_sale(array('armstrong_2_wholesalers_id', 'name', 'armstrong_2_wholesalers_id'), '- Select -', array_merge(array('active' => 1), $conditions));
            unset($wholesalers['']);
            $distributors = array_merge($distributors, $wholesalers);
        }
        return array(
            //'groups' => $this->groups_m->parse_form($cols, '- Select -', $conditions),
            // 'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions),
            'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            'country_sub_channels' => $this->country_sub_channels_m->parse_form($cols, '- Select -', $conditions),
            'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            'assigned_distributors' => $distributors,
            'countryHierachy' => $hierachy,
            'rootValues' => $rootValues,
            'chains' => $chains,
            'salespersons' => $salespersons,
            'customers' => $list_customers
        );
    }

    public function transfer_customers()
    {
        if (!$this->hasPermission('add', 'wholesalers')) {
            return $this->noPermission();
        }
        $this->data = $this->_getAddEditParams();
        return $this->render('ami/wholesalers/transfer_customers', $this->data);
    }

    public function transfer_customers_do()
    {
        if (!$this->hasPermission('edit', 'wholesalers') && !$this->hasPermission('add', 'wholesalers')) {
            return $this->noPermission();
        }
        $post = $this->input->post();
        $customers_id = isset($post['customers']) ? $post['customers'] : '';
        $salesperson_new = isset($post['armstrong_2_salespersons_id_new']) ? $post['armstrong_2_salespersons_id_new'] : '';
        if ($customers_id) {
            $date_time = date('Y-m-d H:i:s');
            $customers_id = str_replace(',', '","', $customers_id);
            // Update relate table
            $data_item_update = array(
                'armstrong_2_customers_id IN ("' . $customers_id . '")' => null,
                'is_draft' => 0
            );
            foreach($this->customer_relate_history as $table){
                $this->db->where($data_item_update)
                    ->update($table, array('last_updated' => $date_time));
            }

            // get list customer by armstrong_2_salespersons_id
            $customers = $this->wholesalers_m->get_by(array('armstrong_2_wholesalers_id IN ("' . $customers_id . '")' => null));
            // update armstrong_2_salespersons_id
            foreach ($customers as $customer) {
                if ($customer['armstrong_2_wholesalers_id']) {
                    $this->wholesalers_m->save(array(
                        'armstrong_2_salespersons_id' => $salesperson_new
                    ), $customer['armstrong_2_wholesalers_id']);
                }
            }
        }
        $this->data = $this->_getAddEditParams();

        return $this->render('ami/wholesalers/transfer_customers', $this->data);
    }

    // tuantq 6/4/2015
    public function transfer_view()
    {

        if (!$this->hasPermission('add', 'wholesalers')) {
            return $this->noPermission();
        }
        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        return $this->render('ami/wholesalers/transfer', $this->data);
    }

    public function transfer_do($id = null, $id_new)
    {

        if (!$this->hasPermission('edit', 'wholesalers') && !$this->hasPermission('add', 'wholesalers')) {
            return $this->noPermission();
        }

//        $this->is('POST');
//        $fields = array('armstrong_2_salespersons_id', 'armstrong_2_salespersons_id_new');
//        $data = $this->wholesalers_m->array_from_post($fields);

        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        $this->data['wholesalers']['armstrong_2_salespersons_id'] = $id;
        $this->data['wholesalers']['armstrong_2_salespersons_id_new'] = $id_new;

        if ($id == $id_new) {
            $this->data['customer']['armstrong_2_salespersons_id'] = $id;
            $this->data['customer']['armstrong_2_salespersons_id_new'] = $id_new;
            return $this->render('ami/wholesalers/transfer', $this->data);
        }
        // get list customer by armstrong_2_salespersons_id
        $wholesalers = $this->wholesalers_m->get_by(array('armstrong_2_salespersons_id' => $id));
        // update armstrong_2_salespersons_id
        foreach ($wholesalers as $wholesaler) {
            if ($wholesaler['armstrong_2_wholesalers_id']) {
                $this->wholesalers_m->save(array(
                    'armstrong_2_salespersons_id' => $id_new
                ), $wholesaler['armstrong_2_wholesalers_id']);
            }
        }

        return $this->render('ami/wholesalers/transfer', $this->data);
    }

    public function show_wholesalers($id = null)
    {
        if ($id == null || $id == '') {
            $this->data['show_table'] = 0;
        } else {
            $this->data['show_table'] = 1;
        }
        //        $this->data['customers'] = $customers = $this->customers_m->get_by(array('armstrong_2_salespersons_id' => $id));
        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        $this->data['wholesalers'] = array('approved' => 1);
        $this->data['wholesalers']['armstrong_2_salespersons_id'] = $id;

//        echo(json_encode($this->data));

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $id);
        $this->data['wholesalers'] = $this->wholesalers_m->get_by($conditions);
//            array(
//            'active' => $this->wholesalers_m->get_by(array_merge($conditions, array('active' => 1))),
//            'inactive' => $this->wholesalers_m->get_by(array_merge($conditions, array('active' => 0))),
//            'pending' => $this->wholesalers_m->get_by(array_merge($conditions, array('approved' => 0)))
//        );
//        dd($this->db->last_query());
        $this->data['total'] = $this->wholesalers_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Wholesalers');


        return $this->render('ami/wholesalers/transfer', $this->data);
    }
    private function getOtm(){
        return array(
            '' => '- Select -',
            'N/A' => 'N/A',
            'D' => 'D',
            'C' => 'C',
            'B' => 'B',
            'A' => 'A'
        );
    }
    public function add()
    {
        if (!$this->hasPermission('add', 'wholesalers')) {
            return $this->noPermission();
        }
        $this->data['otm'] = $this->getOtm();
        $this->data += $this->_getAddEditParams();
        $this->data['wholesalers']['is_plus'] = 1;
        $this->data['wholesalers']['active'] = 1;
        return $this->render('ami/wholesalers/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('view', 'wholesalers') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/wholesalers');
        $this->data['otm'] = $this->getOtm();
        if ($id) {
            $this->data['wholesalers'] = $this->wholesalers_m->get($id);
//            dd($this->data['wholesalers']);
            $this->assertCountry($this->data['wholesalers']);
//            if (!empty($this->data['wholesalers']['armstrong_2_salespersons_id']))
//            {
//                $this->data['salespersons'] = $this->salespersons_m->get($this->data['wholesalers']['armstrong_2_salespersons_id']);
//            }

//            dd($this->data['salespersons']);
            $this->data['page_title'] = page_title('Edit Wholesalers');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'wholesalers')) {
                    return $this->noPermission();
                }
                if ($this->country_id == 6) {
                    $salesperson = $this->salespersons_m->get_by(array(
                        'armstrong_2_salespersons_id' => $this->data['wholesalers']['armstrong_2_salespersons_id']
                    ));
                    if ($salesperson) {
                        $this->data['wholesalers']['salesperson_name'] = $salesperson[0]['first_name'] . ' ' . $salesperson[0]['last_name'];
                    }
                }
                return $this->render('ami/wholesalers/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();

                $countryValues = array();

                if (count($this->data['countryHierachy']) > 0) {
                    foreach ($this->data['countryHierachy'] as $key => $value) {
                        if (!empty($this->data['countryHierachy'][$key])) {
                            if ($key == 'root') {
                                $countryValues[$value] = array_merge(array('' => '-Select-'), $this->countryparser->parse($this->country_code)->get($value));
                            } else {
                                $countryValues[$value] = (!empty($this->data['wholesalers'][$key]) && $this->countryparser->getBy($value, $this->data['wholesalers'][$key]))
                                    ? $this->countryparser->getBy($value, $this->data['wholesalers'][$key])
                                    : array();
                            }
                        } else {
                            $countryValues[$value] = array();
                        }
                    }
                }

                $this->data['countryValues'] = $countryValues;

                return $this->render('ami/wholesalers/edit', $this->data);
            }
        }

        return redirect('ami/wholesalers');
    }


    public function pending($id, $app)
    {

        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        $this->_assertId($id, 'ami/wholesalers');
        if ($id) {

            $wholesalerTable = 'wholesalers';
            $wholesaler = $this->db->select("{$wholesalerTable}.*,

                country_channels.name as country_channels_name,
                country_sub_channels.name as sub_channel_name,


                customers_types.name as customers_types_name,

                business_types.name as business_type_name")
                ->from($wholesalerTable)
                ->join('country_channels', "{$wholesalerTable}.country_channels_id = country_channels.id", 'left')
                ->join('country_sub_channels', "{$wholesalerTable}.country_sub_channels_id = country_sub_channels.id", 'left')
                ->join('customers_types', "{$wholesalerTable}.customers_types_id = customers_types.id", 'left')
                ->join('business_types', "{$wholesalerTable}.business_types_id = business_types.id", 'left')
                ->where("{$wholesalerTable}.armstrong_2_wholesalers_id", $id)
                ->get()
                ->row_array();

//            dd($wholesaler);
//            $customer['cuisine_channels_id'] = $customer['cuisine_channels_name'];

            $wholesaler['channel'] = $wholesaler['country_channels_name'];
            $wholesaler['sub_channel'] = $wholesaler['sub_channel_name'];


            $wholesaler['customers_types_id'] = $wholesaler['customers_types_name'];

            $wholesaler['business_type'] = $wholesaler['business_type_name'];

            $this->data['wholesalers'] = $wholesaler;
            $this->data['app'] = $app;

            $this->assertCountry($this->data['wholesalers']);

            if ($this->input->is_ajax_request()) {
                $this->data['amendedData'] = array();

                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['wholesalers']['approved'] == 2) {
                    // $this->data['amendedData'] = null;
                     $this->data['amendedData'] = $this->_getAmendedData($this->data['wholesalers']['armstrong_2_wholesalers_id']);
//                        $this->_getAmendedData($this->data['wholesalers']['armstrong_2_wholesalers_id']);
                }
                $this->data['wholesalers'] = array_except($this->data['wholesalers'], array(
                    'country_channels_name', 'sub_channel_name',
                    'customers_types_name', 'business_type_name'));

                return $this->render('ami/wholesalers/pending_action');
            }
        }

        return redirect('ami/wholesalers?filter=pending');
    }

    public function approve($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/wholesalers');

        if ($id) {
            $this->data['wholesalers'] = $this->wholesalers_m->get($id);
            $this->assertCountry($this->data['wholesalers']);
            $this->data['page_title'] = page_title('Approve Wholesalers');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['wholesalers']['approved'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['wholesalers']['armstrong_2_wholesalers_id']);
                }

                return $this->render('ami/wholesalers/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $data_old = $this->wholesalers_m->get($id);
                $this->wholesalers_m->save(array(
                    'approved' => 1,
                    'active' => 1,
                    'updated_by' => $this->user_id,
                    'updated_name' => $this->user_name
                ), $id);
                $this->customers_action_v2($data_old, $this->wholesalers_m->get($id), 'wholesalers');
            }
        }

        return redirect('ami/wholesalers?filter=pending');
    }

    public function reject($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/wholesalers');

        if ($id) {
            $this->data['wholesalers'] = $this->wholesalers_m->get($id);
            $this->assertCountry($this->data['wholesalers']);
            $this->data['page_title'] = page_title('Reject Wholesalers');

            if ($this->input->is_ajax_request()) {
                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['wholesalers']['approve'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['wholesalers']['armstrong_2_wholesalers_id']);
                }

                return $this->render('ami/wholesalers/pending_action', $this->data);
            } else if ($this->is('POST')) {
                if ($this->data['wholesalers']['approved'] == 0) {
                    $data_save = array(
                        'approved' => -1,
                        'is_draft' => 1
                    );
                } else {
                    $data_save = array(
                        'approved' => -1,
                        'active' => 1
                    );
                }

                $data_save['updated_by'] = $this->user_id;
                $data_save['updated_name'] = $this->user_name;

                $data_old = $this->wholesalers_m->get($id);
                $this->wholesalers_m->save($data_save, $id);
                $this->customers_action_v2($data_old, $this->wholesalers_m->get($id), 'wholesalers');
//                $pendingEdit = $this->db->from($this->wholesalers_amended_m->getTableName())
//                    ->where('armstrong_2_wholesalers_id', $id)
//                    ->get()->row_array();
//
//                if ($pendingEdit)
//                {
//                    $this->db->where(array('id' => $pendingEdit['id']))
//                        ->update($this->wholesalers_amended_m->getTableName(), array(
//                            'wholesaler_old_data' => $pendingEdit['wholesaler_new_data'],
//                            'wholesaler_new_data' => $pendingEdit['wholesaler_old_data'],
//                        ));
//                }
            }
        }

        return redirect('ami/wholesalers?filter=pending');
    }


    public function update()
    {
        if (!$this->hasPermission('delete', 'wholesalers')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->wholesalers_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/wholesalers');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'wholesalers')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/wholesalers');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/wholesalers';
                //$this->wholesalers_m->delete($id);
                $this->wholesalers_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/wholesalers/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Wholesalers"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/wholesalers');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'wholesalers')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/wholesalers');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/wholesalers/draft';
                $this->wholesalers_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/wholesalers/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Wholesalers"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/wholesalers');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'wholesalers') && !$this->hasPermission('edit', 'wholesalers')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->wholesalers_m->array_from_post(array('name', 'armstrong_1_wholesalers_id', 'armstrong_2_distributors_id', 'armstrong_2_salespersons_id', 'armstrong_2_secondary_salespersons_id',
            'is_mother', 'armstrong_2_related_mother_id', 'approved', 'ssd_id', 'sap_cust_name', 'street_address', 'city', 'postal_code', 'phone', 'phone_2', 'sms_number', 'email', 'email_2', 'email_3', 'line_id', 'district', 'region', 'state',
            'armstrong_2_chains_id', 'suburb', 'fax', 'update_info', 'active', 'country_channels_id', 'country_sub_channels_id', 'otm',
            'business_types_id', 'assigned_distributor_1', 'assigned_distributor_2', 'contact_details', 'is_plus', 'armstrong_2_wholesalers_id', 'sap_id', 'account_number','is_perfect_store', 'disable_line'));

        $data['country_id'] = $this->country_id;

        $id = $data['armstrong_2_wholesalers_id'] ? $data['armstrong_2_wholesalers_id'] : null;
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        // create new record
        if (!$id) {
            //$data['armstrong_2_wholesalers_id'] = $this->wholesalers_m->generateArmstrongId($this->country_id, 'wholesalers');
        }

        $date_time = date('Y-m-d H:i:s');
        $data_update = array(
            'armstrong_2_customers_id' => $data['armstrong_2_wholesalers_id'],
            'is_draft' => 0
        );
        foreach($this->customer_relate_history as $table){
            $this->db->where($data_update)
                ->update($table, array('last_updated' => $date_time));
        }
        $data_old = $this->wholesalers_m->get($id);
        if (!$id) {
            $this->wholesalers_m->save($data, null, true, 'INSERT', $this->country_id);
        }
        else
        {
            $this->wholesalers_m->save($data, $id);
        }
        $this->customers_action_v2($data_old, $this->wholesalers_m->get($id), 'wholesalers');
        return redirect('ami/wholesalers');
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'wholesalers')
        ) {
            return redirect(site_url('ami/wholesalers'));
        }

        $wholesalers = $this->wholesalers_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($wholesalers);

        return $this->excel($sheet, 'wholesalers', $type);
    }

    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->wholesalers_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');
        $salespersons = $this->input->get('salespersons');
        if (!$term) {
            return $this->json(array());
        }
        $conditions = array("armstrong_2_wholesalers_id LIKE '%{$term}%'", "name LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        if($this->input->get('action') == 'ssd') {
            $this->db
                ->where_in('approved', array(1,2))
                ->where('country_id', $this->country_id)
                ->where('active', 1)
                ->where('is_draft', 0)
                ->where($conditions);
        }
        else
        {
            $this->db
                ->where('country_id', $this->country_id)
                ->where_in('approved', 1)
                ->where('active', 1)
                ->where('is_draft', 0)
                ->where($conditions);
        }

        if ($salespersons && ($this->input->get('action') == 'webshop')) {
            $this->db->where('armstrong_2_salespersons_id', $salespersons);
        }

        $wholesalers = $this->db->limit(30)
            ->get($this->wholesalers_m->getTableName())
            ->result_array();

        $output = array();

        foreach ($wholesalers as $wholesaler) {
            $output[] = array(
                'value' => $wholesaler['armstrong_2_wholesalers_id'],
                'label' => $wholesaler['armstrong_2_wholesalers_id'] . ' - ' . $wholesaler['name']
            );
        }

        return $this->json($output);
    }

    protected function _getAmendedData($customerId)
    {
        $data = $this->db->where(array(
            'country_id' => $this->country_id,
            'armstrong_2_customers_id' => $customerId,
            'is_draft' => 0
        ))
            ->get('customers_amended_data')->row_array();
        
        if ($data) {
            $data['old_data'] = !empty($data['customer_old_data']) ? json_decode($data['customer_old_data'], true) : array();
            $data['new_data'] = !empty($data['customer_new_data']) ? json_decode($data['customer_new_data'], true) : array();

            $data['fields'] = $data['old_data'] ? array_keys($data['old_data']) : array();
        }

        /*$foreignKeys = array(
            'business_type' => 'business_types',
            'sub_channel' => 'country_sub_channels',
            'channel' => 'country_channels',
            'customers_types_id' => 'customers_types',
            'channel_groups_id' => 'groups',
            'cuisine_groups_id' => 'groups'
        );

        foreach ($foreignKeys as $field => $table) {
            if (!empty($data['old_data'][$field])) {
                $oldContent = $this->_getRelationshipAmendedData($table, 'id', $data['old_data'][$field]);

                if (!empty($oldContent['name'])) {
                    $data['old_data'][$field] .= " - {$oldContent['name']}";
                }
            }

            if (!empty($data['new_data'][$field])) {
                $newContent = $this->_getRelationshipAmendedData($table, 'id', $data['new_data'][$field]);

                if (!empty($newContent['name'])) {
                    $data['new_data'][$field] .= " - {$newContent['name']}";
                }
            }
        }*/

        /*if (!empty($data['fields'])) {
            foreach ($data['fields'] as $dataField) {
                if (!in_array($dataField, $foreignKeys)) {
                    switch ($dataField) {
                        case 'armstrong_2_salespersons_id':
                            if (!empty($data['old_data']['armstrong_2_salespersons_id'])) {
                                $oldContent = $this->_getRelationshipAmendedData('salespersons', 'id', $data['old_data']['armstrong_2_salespersons_id']);

                                if (isset($oldContent['first_name']) && isset($oldContent['last_name'])) {
                                    $data['old_data']['armstrong_2_salespersons_id'] = "{$oldContent['first_name']} {$oldContent['last_name']}";
                                }
                            }

                            if (!empty($data['new_data']['armstrong_2_salespersons_id'])) {
                                $newContent = $this->_getRelationshipAmendedData('salespersons', 'id', $data['new_data']['armstrong_2_salespersons_id']);

                                if (!empty($newContent['name'])) {
                                    $data['new_data']['armstrong_2_salespersons_id'] = "{$newContent['first_name']} {$newContent['last_name']}";
                                }
                            }
                            break;

                        case 'primary_supplier':
                            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
                            $distributors = $this->distributors_m->getDistributorListOptions(null, $conditions);
                            $wholesalers = $this->wholesalers_m->getSalerListOptions(null, $conditions);
                            $suppliers = array_merge($distributors, $wholesalers);

                            if (!empty($data['old_data']['primary_supplier'])) {
                                if (isset($suppliers[$data['old_data']['primary_supplier']])) {
                                    $data['old_data']['primary_supplier'] = "{$suppliers[$data['old_data']['primary_supplier']]}";
                                }
                            }

                            if (!empty($data['new_data']['primary_supplier'])) {
                                if (isset($suppliers[$data['new_data']['primary_supplier']])) {
                                    $data['new_data']['primary_supplier'] = "{$suppliers[$data['new_data']['primary_supplier']]}";
                                }
                            }
                            break;

                        case 'secondary_supplier':
                            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
                            $distributors = $this->distributors_m->getDistributorListOptions(null, $conditions);
                            $wholesalers = $this->wholesalers_m->getSalerListOptions(null, $conditions);
                            $suppliers = array_merge($distributors, $wholesalers);

                            if (!empty($data['old_data']['secondary_supplier'])) {
                                if (isset($suppliers[$data['old_data']['secondary_supplier']])) {
                                    $data['old_data']['secondary_supplier'] .= " - {$suppliers[$data['old_data']['secondary_supplier']]}";
                                }
                            }

                            if (!empty($data['new_data']['secondary_supplier'])) {
                                if (isset($suppliers[$data['new_data']['secondary_supplier']])) {
                                    $data['new_data']['secondary_supplier'] .= " - {$suppliers[$data['new_data']['secondary_supplier']]}";
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }*/

        return $data;
    }

    protected function _getRelationshipAmendedData($table, $key, $value)
    {
        return $this->db->from($table)
            ->where($key, $value)
            ->get()->row_array();
    }
}