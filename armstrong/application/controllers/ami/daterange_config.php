<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Eloquent\DaterangeConfig;
class Daterange_config extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('daterange_config_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'daterange_config')) {
            return $this->noPermission();
        }
        $daterange_config = DaterangeConfig::where(array('country_id' => $this->country_id))->first();
        if($daterange_config) $daterange_config = $daterange_config->toArray();
        $this->data['rolling'] = array(
            '0' => 'Current month',
            '3' => 'Rolling 3 months',
            '6' => 'Rolling 6 months',
            '12' => 'Rolling 12 months'
        );

        $this->data['post'] = $daterange_config;
        $this->data['page_title'] = page_title('Customer Analysis Configuration');
        return $this->render('ami/daterange_config/index', $this->data);
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'daterange_config') && !$this->hasPermission('edit', 'daterange_config'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $fields = array('id', 'country_id', 'mighty_10_rolling', 'channel_basket_penetration_rolling', 'active_sku_rolling', 'sampling_history_rolling', 'listing_and_availability_rolling', 'new_grip_rolling', 'new_grab_rolling');
        $data = $this->daterange_config_m->array_from_post($fields);
        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;
//        foreach($data as $key => &$_data){
//            if(preg_match('/_end/i', $key)){
//                $_data = $_data.' 23:59:59';
//            }
//        }
        // Create new record
        if (!$id) {
            $data['date_created'] = $data['last_updated'] = date('Y-m-d H:i:s');
        }else{
            $data['last_updated'] = date('Y-m-d H:i:s');
        }

        $this->daterange_config_m->save($data, $id);


        return redirect('ami/daterange_config');
    }

}