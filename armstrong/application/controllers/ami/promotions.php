<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('promotions_m');
        $this->load->model('media_m');
        $this->load->model('groups_m');
        $this->load->model('brands_m');
        $this->load->model('products_cat_web_m');
        $this->load->model('segments_m');
        $this->load->model('cuisine_channels_m');
        $this->load->model('categories_m');
        $this->load->model('markets_m');
        $this->load->model('sectors_m');
        $this->load->model('sub_sectors_m');
        $this->load->model('products_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'promotions')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $this->data['data'] = array(
            'active' => $this->promotions_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->promotions_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        $this->data['total'] = $this->promotions_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Promotions');

        return $this->render('ami/promotions/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'promotions')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['data'] = array(
            'active' => $this->promotions_m->get_by(array_merge($conditions, array('listed' => 1))),
            'inactive' => $this->promotions_m->get_by(array_merge($conditions, array('listed' => 0))),
        );
        $this->data['total'] = $this->promotions_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Promotions');

        return $this->render('ami/promotions/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        //type 1:OTM, 2:EveryOne, 3:promotion
		$data_type['1'] = 'OTM';
		$data_type['2'] = 'EveryOne';
		$data_type['3'] = 'Promotion';
        return array(
            'type' => $data_type,
            'app_type' => $this->app_type,
            'otm' => array('' => '- Select -', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D'),
			'products' => $this->products_m->getProductListOptions('- Select -', array_merge(array('listed' => 1), $conditions), 'id'),
            'groups' => $this->groups_m->parse_form($cols, '- Select -', $conditions),
            'brands' => $this->brands_m->parse_form($cols, '- Select -'),
            'products_cat_web' => $this->products_cat_web_m->parse_form($cols, '- Select -', $conditions),
            'segments' => array_merge(array(0 => '- Select -'), $this->segments_m->parse_form($cols, null, $conditions)),
            'categories' => array_merge(array(0 => '- Select -'), $this->categories_m->parse_form($cols, null, $conditions)),
            'markets' => array_merge(array(0 => '- Select -'), $this->markets_m->parse_form($cols, null, $conditions)),
            'sectors' => array_merge(array(0 => '- Select -'), $this->sectors_m->parse_form($cols, null, $conditions)),
            'sub_sectors' => array_merge(array(0 => '- Select -'), $this->sub_sectors_m->parse_form($cols, null, $conditions)),
            'cuisine_channels' => $this->cuisine_channels_m->parse_form($cols, '- Select -', $conditions),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'promotions')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['post']['listed'] = 1;
        return $this->render('ami/promotions/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'promotions') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotions');

        if ($id = intval($id)) {
            $this->data['post'] = $this->promotions_m->get($id);
            //$this->assertCountry($this->data['promotions']);
            $this->data['post']['glob_pro_hierarchy_array'] = !empty($this->data['post']['glob_pro_hierarchy'])
                ? explode(',', str_replace(array('{', '}'), array('', ''), $this->data['post']['glob_pro_hierarchy'])) //json_decode($this->data['product']['glob_pro_hierarchy'], true)
                : array();
            $this->data['images'] = $this->media_m->getMedia('promo_image', 'promotions', $this->country_id, $id);
            $this->data['videos'] = $this->media_m->getMedia('video', 'promotions', $this->country_id, $id);
            $this->data['brochures'] = $this->media_m->getMedia('brochure', 'promotions', $this->country_id, $id);
            $this->data['certs'] = $this->media_m->getMedia('cert', 'promotions', $this->country_id, $id);
            $this->data['specs'] = $this->media_m->getMedia('spec', 'promotions', $this->country_id, $id);
            $this->data['page_title'] = page_title('Edit Promotions');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'promotions')) {
                    return $this->noPermission();
                }

                return $this->render('ami/promotions/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/promotions/edit', $this->data);
            }
        }

        return redirect('ami/promotions');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'promotions')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->promotions_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/promotions');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'promotions')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotions');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/promotions';
                //$this->products_m->delete($id);
                $this->promotions_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/promotions/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Promotions"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/promotions');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'promotions')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotions');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/promotions/draft';
                $this->promotions_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/promotions/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Promotions"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/promotions');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'promotions') && !$this->hasPermission('edit', 'promotions')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->promotions_m->array_from_post(array('sku_number', 'sku_name', 'name_alt', 'description',
            'brands_id', 'ingredients', 'barcode', 'price', 'weight_pc', 'glob_pro_hierarchy', 'quantity_case',
            'segments_id', 'shelf_life', 'pro_claims', 'is_top_ten', 'cuisine_channels_id', 'cert_halal', 'related_media',
            'nutrition_name', 'listed', 'type', 'otm', 'id','products_id', 'app_type', 'pallet_configuration', 'sku_code'));

        $data['country_id'] = $this->country_id;
        $data['description'] = str_replace('<br />', '<br>', $data['description']);
        $data['ingredients'] = str_replace('<br />', '<br>', $data['ingredients']);

        $data['glob_pro_hierarchy'] = '{' . implode($data['glob_pro_hierarchy'], ',') . '}';

        $id = $data['id'] ? $data['id'] : null;
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        $newId = $this->promotions_m->save($data, $id);
        $this->promotions_m->save(array('sku_number' => 'promo_' . $newId), $newId);

        // Update existing record
        if ($id) {
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('promotions', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');
        $brochureChangeAppType = $this->input->post('brochure_app_type');
        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'promotions',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }
        if($brochureChangeAppType){
            foreach($brochureChangeAppType as $media_id => $changeAppType){
                $this->media_m->updateMediaRef(
                    array(
                        'app_type' => $changeAppType,
                        'last_updated' => date('Y-m-d H:i:s')
                    ),
                    array(
                        'media_id' => $media_id,
                        'entry_id' => $id,
                        'entry_type' => 'promotions',
                        'country_id' => $this->country_id
                    )
                );
            }
        }
        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'promotions';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);
                }
            }
        }

        return redirect('ami/promotions');
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term) {
            return $this->json(array());
        }

        $conditions = array("sku_number LIKE '%{$term}%'", "sku_name LIKE '%{$term}%'", "name_alt LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        $this->db->where('country_id', $this->country_id)
            ->where('listed', 1)
            ->where('quantity_case > ', 0)
            ->where('is_draft', 0)
            ->where($conditions);

        $promotions = $this->db->limit(30)
            ->get($this->promotions_m->getTableName())
            ->result_array();

        $output = array();

        foreach ($promotions as $p) {
            $title = in_array($this->country_code, array('tw', 'hk')) ? 'name_alt' : 'sku_name';

            $output[] = array(
                'value' => $p['sku_number'],
                // 'label' => $salesperson['armstrong_2_salespersons_id'],
                'label' => get_product_label($p, $title),
                'price' => $p['price'],
                'pcsprice' => $p['price'] / $p['quantity_case']
            );
        }

        return $this->json($output);
    }
}