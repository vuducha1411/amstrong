<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_game extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('prizes_setting_m');
        $this->load->model('country_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'setting_game')) {
            return $this->noPermission();
        }
        return $this->render('ami/setting_game/index', $this->data);
    }

    public function setting_index()
    {
        if (!$this->hasPermission('view', 'setting_game')) {
            return $this->noPermission();
        }

        $seasons = $this->db->order_by('season', 'ASC')->get($this->prizes_setting_m->getTableName())->result_array();
        $now = strtotime(date('Y-m-d'));

        foreach ($seasons as &$season) {
            if ($now >= strtotime($season['date_start']) && $now <= strtotime($season['date_expired'])) {
                $season['is_current_season'] = true;
            } else {
                $season['is_current_season'] = false;
            }
        }
        $this->data['seasons'] = $seasons;
        $this->data['page_title'] = page_title('Game Play');
        return $this->render('ami/setting_game/setting_index', $this->data);
    }

    public function setting_spin()
    {
        if (!$this->hasPermission('view', 'setting_game')) {
            return $this->noPermission();
        }
        return $this->render('ami/setting_game/setting_spin', $this->data);
    }
    public function call_has_coin($call_id)
    {
        $this->db->select('id');
        $this->db->where(array('armstrong_2_call_records_id' => $call_id));
        $table = $this->db->get('prizes')->result_array();
        if($table)
            return true;
        return false;
    }
    public function add_spin()
    {
        //fields criteria_perfect_call empty, log_ids empty
        $call_arrays = array(
            'CALA00008078NZ','CALA00008089NZ','CALA00008090NZ','CALA00008091NZ','CALA00008092NZ','CALA00008093NZ','CALA00008094NZ','CALA00008095NZ','CALA00008096NZ','CALA00008097NZ','CALA00008098NZ','CALA00008099NZ','CALA00008100NZ','CALA00008101NZ','CALA00008111NZ'
        );
        $coutry_id = 9;
        $apptype = 0;
        $salepersion_id = 'SALA0001NZ';
        foreach($call_arrays as $c)
        {
            if(!$this->call_has_coin($c)){
                $spin = array(
                    'armstrong_2_salespersons_id' => $salepersion_id,
                    'armstrong_2_call_records_id' => $c,
                    'type' => $apptype,
                    'prize' => '',
                    'prize_type' => '0',
                    'stack_number' => '0',
                    'is_spined' => 0,
                    'is_special' => '0',
                    'date_completed_valid' => date('Y-m-d H:i:s'),
                    'criteria_perfect_call' => '',
                    'salesleader_config_type' => '',
                    'is_valid' => 1,
                    'date_sync' => date('Y-m-d H:i:s'),
                    'last_updated' => date('Y-m-d H:i:s'),
                    'is_draft' => 0,
                    'log_ids' => '',
                    'time_zone' => '',
                    'country_id' => $coutry_id,
                    'date_created' => date('Y-m-d H:i:s'),
                );
                echo $c.' insert'.'<br>';
                $this->db->insert('prizes', $spin);
            }
            else
            {
                echo $c.' exist'.'<br>';
            }
        }
        $get = array();
        $get['is_draft'] = 0;
        $get['is_valid'] = 1;
        // TODO type 0: pull, 1:leader, 2:push
        $get['type'] = 0;
        $get['is_spined'] = 0;
        $this->db->select('*, COUNT(*) as totalspin');
        $this->db->where($get);
        $this->db->where('armstrong_2_salespersons_id', $salepersion_id);
        $this->db->group_by('armstrong_2_salespersons_id');
        $table = $this->db->get('prizes')->result_array();
        dd($table);
    }
    public function play_setting($id = null)
    {
        if (!$this->hasPermission('edit', 'setting_game') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        if ($id) {
            $prizes_setting = $this->prizes_setting_m->get($id);

            $prizes_setting['config_prize'] = isset($prizes_setting['config_prize']) ? json_decode($prizes_setting['config_prize']) : array();
            $prizes_setting['config_pull_new'] = isset($prizes_setting['config_pull_new']) ? json_decode($prizes_setting['config_pull_new']) : array();
            $prizes_setting['config_push_new'] = isset($prizes_setting['config_push_new']) ? json_decode($prizes_setting['config_push_new']) : array();
            $prizes_setting['config_leader'] = isset($prizes_setting['config_leader']) ? json_decode($prizes_setting['config_leader']) : array();
            $prizes_setting['limit_prize'] = isset($prizes_setting['limit_prize']) ? json_decode($prizes_setting['limit_prize']) : array();
            $prizes_setting['date_prize'] = isset($prizes_setting['date_prize']) ? json_decode($prizes_setting['date_prize']) : array();
//            $prizes_setting['date_start'] = $prizes_setting['date_start'];
//            $prizes_setting['date_expired'] = $prizes_setting['date_expired'];
//            $prizes_setting['id'] = $prizes_setting['id'];

            $this->data['post'] = $prizes_setting;

            $this->data['draft'] = false;
        }
        $this->data['key_map_title']['pull'] = array(
            'min_number_of_criterias' => 'Min_number_of_criterias',
            'pantryCheck' => 'Pantry check',
            'material' => 'Rich media demo',
            'sampling' => 'Samplings',
            'tfos' => 'Place an order (TFO)',
            'competitors' => 'Fill in Competitor product information',
            'objectives' => 'Setup personal objective',
        );
        $this->data['key_map_title']['push'] = array(
            'min_number_of_criterias' => 'Min_number_of_criterias',
            'pantryCheck' => 'Inventory check',
            'material' => 'Rich media demo',
            'perfectStore' => 'Store Audit',
            'tfos' => 'TFO',
            'competitors' => 'Competitor Info',
            'objectives' => 'Set up objective',
        );
        if ($this->input->is_ajax_request()) {
            $config_pull_new_total = (array)$this->data['post']['config_pull_new'];
            $config_push_new_total = (array)$this->data['post']['config_pull_new'];
            $config_leader_total = (array)$this->data['post']['config_leader'];
            $this->data['post']['config_pull_new_total'] = count($config_pull_new_total) - 1;
            $this->data['post']['config_push_new_total'] = count($config_push_new_total) - 1;
            $this->data['post']['config_leader_total'] = count($config_leader_total);
            return $this->render('ami/setting_game/preview', $this->data);
        } else {
            $this->data['page_title'] = page_title('Game Play Settings');
            return $this->render('ami/setting_game/play_setting', $this->data);
        }
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'setting_game') && !$this->hasPermission('edit', 'setting_game')) {
            return $this->noPermission();
        }
        // TODO array insert
        $config_pull_arr = array('pantryCheck', 'material', 'sampling', 'tfos', 'competitors', 'objectives');
        $config_push_arr = array('pantryCheck', 'material', 'perfectStore', 'tfos', 'competitors', 'objectives');

        $config_leader_arr = array('approve_customers', 'view_ufs_route_plan', 'view_ufs_organization', 'set_sales_target');
        // TODO end
        $this->is('POST');
        $params = $this->prizes_setting_m->array_from_post(array('id', 'config_prize', 'config_pull_new',
            'config_push_new', 'config_leader', 'limit_prize', 'date_start', 'date_expired', 'date_prize'));

        $id = $params['id'] ? $params['id'] : null;

        // TODO change previous seasons is_draft to 1
        if (!$id) {
            $sql = "Update prizes_setting SET is_draft = 1 WHERE is_draft = 0";
            $this->db->query($sql);
        }
        // TODO
        $config_pull = $config_push = $config_leader = null;
        if (is_array($params['config_pull_new'])) {
            for ($i = 0; $i < count($config_pull_arr); $i++) {
                if (isset($params['config_pull_new'])) {
                    if (!in_array($config_pull_arr[$i], array_keys($params['config_pull_new']))) {
                        $params['config_pull_new'][$config_pull_arr[$i]] = '0';
                    }
                } else {
                    $params['config_pull_new'][$config_pull_arr[$i]] = '0';
                }
            }
            $config_pull = str_replace('"on"', '"1"', json_encode($params['config_pull_new']));
        }
        if (is_array($params['config_push_new'])) {
            for ($i = 0; $i < count($config_push_arr); $i++) {
                if (isset($params['config_push_new'])) {
                    if (!in_array($config_push_arr[$i], array_keys($params['config_push_new']))) {
                        $params['config_push_new'][$config_push_arr[$i]] = '0';
                    }
                } else {
                    $params['config_push_new'][$config_push_arr[$i]] = '0';
                }
            }
            $config_push = str_replace('"on"', '"1"', json_encode($params['config_push_new']));
        }
        if (is_array($params['config_leader'])) {
            for ($i = 0; $i < count($config_leader_arr); $i++) {
                if (isset($params['config_leader'])) {
                    if (!in_array($config_leader_arr[$i], array_keys($params['config_leader']))) {
                        $params['config_leader'][$config_leader_arr[$i]] = '0';
                    }
                } else {
                    $params['config_leader'][$config_leader_arr[$i]] = '0';
                }
            }
            $config_leader = str_replace('"on"', '"1"', json_encode($params['config_leader']));
        }
        $date_detail = array('from' => $params['date_start'], 'to' => $params['date_expired']);
        $params['date_prize'] = array(
            'prize_special' => $date_detail,
            'prize_first' => $date_detail,
            'prize_second' => $date_detail
        );
        $data = array(
            'config_prize' => json_encode($params['config_prize']),
            'limit_prize' => json_encode($params['limit_prize']),
            'config_pull_new' => $config_pull,
            'config_push_new' => $config_push,
            'config_leader' => $config_leader,
            'date_prize' => json_encode($params['date_prize'])
        );
        $data['date_start'] = $params['date_start'];
        $data['date_expired'] = $params['date_expired'];

        if (!$id) {
            $maxSeasons = $this->db->select_max('season')->get($this->prizes_setting_m->getTableName())->row();
            $data['season'] = $maxSeasons->season + 1;
        }
        $recordId = $this->prizes_setting_m->save($data, $id);

        return redirect('ami/setting_game');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');
        $countries = $this->country_m->get_by(array('is_draft' => 0), false, false, 'id,code');

        foreach ($countries as $k => $p) {
            if (strtolower($p['code']) == strtolower($type)) {
                return $p['id'];
            }
        }
    }

    protected function _conditions()
    {
        return array('is_draft' => 0, 'active' => 1, 'email !=' => '',
            '(type LIKE "%pull%" OR type LIKE "%mix%" OR type LIKE "%sl%")' => NULL);
    }

    public function integrate_with_applications()
    {
        if (!$this->hasPermission('view', 'setting_game')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = $this->_conditions();

        $filter = $this->_getFilter();

        if ($filter !== null) {
            $conditions['country_id'] = $filter;
        }

        $sales = $this->salespersons_m->get_limit(array(10000, 0), $conditions, 'email desc', 'armstrong_2_salespersons_id,first_name,last_name,email,game_icon_sr as game_icon_pull,game_icon_leader,roles_id,type,date_created,last_updated');

        foreach ($sales as $k => $p) {
            $sales[$k]['is_pull'] = $sales[$k]['is_leader'] = 0;
            // TODO 0:pull, 1:push, 2:leader
            switch (strtolower($p['type'])) {
                case 'pull':
                case 'push':
                case 'mix':
                    $sales[$k]['is_pull'] = $p['game_icon_pull'];
                    $sales[$k]['app_type'] = 0;
                    break;
                case 'sl':
                    $sales[$k]['is_leader'] = $p['game_icon_leader'];
                    $sales[$k]['app_type'] = 2;
                    break;
                default:
                    unset($sales[$k]);
                    break;
            }
        }

        $this->data['total'] = $this->salespersons_m->count('where', $conditions);
        $this->data['filter'] = $this->input->get('filter');
        $this->data['country_id'] = $filter;
        $this->data['data'] = array_values($sales);
        $this->data['countries'] = $this->country_m->get_by(array('is_draft' => 0), false, false, 'id,code,name');
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Integrate With Applications');

        return $this->render('ami/setting_game/integrate_with_applications', $this->data);
    }

    public function integrate_with_applications_save($country = false)
    {
        if (!$this->hasPermission('add', 'setting_game') && !$this->hasPermission('edit', 'setting_game')) {
            return $this->noPermission();
        }
        // TODO end
        $this->is('POST');
        $params = $this->salespersons_m->array_from_post(array('armstrong_2_salespersons_id', 'game_icon_pull', 'game_icon_leader', 'cf', 'sales_id'));

        $params['cf']['pull'] = array_keys($params['cf']['pull']);
        $params['cf']['leader'] = array_keys($params['cf']['leader']);


        $conditions = $this->_conditions();

        $filter = $this->_getFilter();
        if ($filter !== null) {
            $conditions['country_id'] = $filter;
        }
        $sales_id_str = "('0'";
        $total_sales = count($params['sales_id']);
        if ($total_sales > 0) {
            for ($i = 0; $i < $total_sales; $i++) {
                $sales_id_str .= ",'" . $params['sales_id'][$i] . "'";
            }
        }
        $sales_id_str .= ')';
        $conditions['armstrong_2_salespersons_id IN ' . $sales_id_str] = NULL;

        $sales = $this->salespersons_m->get_by($conditions, false, false, 'armstrong_2_salespersons_id,game_icon_sr as game_icon_pull,game_icon_leader');

        foreach ($sales as $k => $sale) {
            $id = $sale['armstrong_2_salespersons_id'];

            if (in_array($sale['armstrong_2_salespersons_id'], $params['cf']['pull'])) {
                // update game_icon_sr = 1
                $this->salespersons_m->save(array('game_icon_sr' => 1), $id);
            } else {
                // update game_icon_sr = 0
                $this->salespersons_m->save(array('game_icon_sr' => 0), $id);
            }
            if (in_array($sale['armstrong_2_salespersons_id'], $params['cf']['leader'])) {
                // update game_icon_leader = 1
                $this->salespersons_m->save(array('game_icon_leader' => 1), $id);
            } else {
                // update game_icon_leader = 0
                $this->salespersons_m->save(array('game_icon_leader' => 0), $id);
            }
        }
        if ($filter !== null) {
            return redirect('ami/setting_game/integrate_with_applications?filter=' . $this->input->get('filter'));
        } else {
            return redirect('ami/setting_game/integrate_with_applications');
        }

    }

}