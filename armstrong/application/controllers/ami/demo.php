<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Demo extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */


    public function __construct()
    {
        parent::__construct();
        $this->load->model('customers_m');
        $this->load->model('country_channels_m');
        $this->load->model('salespersons_m');
        $this->load->model('roles_m');
        $this->load->model('country_m');
        $this->load->model('products_m');
        $this->load->model('prizes_setting_m');
        $this->load->model('global_channels_m');
        $this->data['reportEndpoint'] = $this->config->item('report_url');
        $this->data['reportLabels'] = array(
            /*Reports*/
            'report_summary' => 'All-in-One Transactions Summary Report',
            'report_call' => 'Call Report',
            'report_tfo' => 'TFO Report',
            'report_pantry_check' => 'Pantry Report',
            'report_sampling' => 'Sampling Report',
            'report_otm' => 'OTM Report',
            'report_approval' => 'Approval Report',
            'report_customers' => 'Customer Report',
            'top_ten_penetration_sr' => 'Top 10 Penetration Report ',
            'report_route_plan' => 'Route Plan Report',
            'report_grip_and_grab_sr' => 'Grip And Grap SR Report',
            'report_all_in_one' => 'All-in-One Report',
            'get_app_versions_report' => 'App Versions Report',
            'penetration_data_report' => 'SKU Penetration Report',
            'penetration_customers_report' => 'Customers Penetration Report',
            'get_perfect_call_report_tracking' => 'Perfect Call Report',
            'get_perfect_call_report_tracking_detail' => 'Perfect Call Tracking Detail',
            'get_perfect_store_report' => 'Perfect Store Data',
            'coaching_note' => 'Coaching Note Report',
            'route_master_temp' => ' Route Master Template',
            'customer_contact_data' => 'Customer Contact Data',
            /*Data*/
            'data_call' => 'Call and Route  Data',
            'data_otm' => 'OTM Data',
            'data_tfo' => 'TFO Data',
            'data_ssd' => 'SSD Data',
            'data_pantry_check' => 'Pantry Data',
            'data_sampling' => 'Sampling Data',
            'new_customer_report' => 'New Customer Report',
            'top_ten_penetration_calculation_tr' => 'Top 10 Penetration Transaction Calculation Data',
            'new_grip_report' => 'New Grip Data',
            'new_grab_report' => 'New Grab Data',
            'lost_grip_report' => 'Lost Grip Data',
            'lost_grab_report' => 'Lost Grab Data',
            'potential_lost_grip_report' => 'Potential Lost Grip Data',
            'potential_lost_grab_report' => 'Potential Lost Grab Data',
            'top_ten_penetration_tr' => 'Top 10 Penetration Data',
            'competitor_reprot' => 'Competitor Data',
            'objective_record_data' => 'Objective Record Data',
            'activities_log_data' => 'Activities Log Data',
            'data_customers' => 'Customer Data',
            'data_product' => 'Product Data',
            'data_route_plan' => 'Route Plan Data',
            'data_organization' => 'Organization Data',
            'data_region_master' => 'Region Master Data',
            'data_ssd_mt' => 'SSD Data',
            'data_distributors' => 'Distributors Data',
            'data_wholesalers' => 'Wholesalers Data',
            'data_recipes' => 'Recipes Data',
            'get_detail_app_versions_report' => 'Detail App Versions',
            'get_perfect_call_report' => 'Call Report',
            'rich_media' => 'Rich Media Report',
            'promotions_activity_report' => 'Promotion Activities',
            //'last_pantry_check_report' => 'Last Pantry Check',
        );
    }

    protected function _setExportFields()
    {
        return array(
            'from_to' => array('data_route_plan', 'coaching_note', 'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'report_tfo', 'report_call',
                'report_sampling', 'report_pantry_check', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'data_inventory_check'),
            'salespersons' => array('promotions_activity_report', 'coaching_note', 'get_perfect_store_report', 'get_perfect_call_report_tracking',/*'penetration_customers_report',*/
                'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'report_all_in_one', 'data_tfo', 'data_ssd', 'data_call', 'data_sampling', 'data_pantry_check', 'data_customers', 'report_otm', 'report_pantry_check', 'report_customers', /*'report_call',*/
                'top_ten_penetration_sr', 'report_summary_channel', 'report_route_plan', 'report_sampling', 'report_grip_and_grab_sr', 'report_tfo', 'data_route_plan', 'top_ten_penetration_calculation_tr', 'new_grip_report', 'new_grab_report', 'lost_grip_report', 'lost_grab_report', 'potential_lost_grip_report', 'potential_lost_grab_report', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'data_ssd_mt', 'data_wholesalers', 'data_distributors', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'get_perfect_call_report_tracking_detail', 'data_inventory_check', /*'penetration_data_report'*/),
            'month-year' => array('get_perfect_store_report', 'penetration_customers_report', 'penetration_data_report', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr', /*'report_call'*/),
            'customers' => array('customers'),
            'grip_customers' => array('penetration_data_report', 'grib_grap_report'),
            'top-ten' => array('report_summary_channel'),
            'country' => array('data_region_master'),
            'season' => array('get_perfect_call_report_tracking_detail', 'get_perfect_call_report_tracking'),
            'number_sku' => array('penetration_customers_report'),
            'top_products' => array('penetration_data_report'),
            'manager' => array(/*'penetration_customers_report', 'penetration_data_report'*/),
            'app_type' => array('customer_contact_data', 'data_ssd', 'objective_record_data', 'activities_log_data', 'get_detail_app_versions_report', /*'route_master_temp',*/
                'report_otm', 'report_customers', 'report_pantry_check', 'get_app_versions_report', 'data_route_plan', 'coaching_note', 'penetration_customers_report', 'penetration_data_report', 'data_tfo', 'competitor_reprot', 'data_call', 'data_sampling', 'get_perfect_store_report', 'get_perfect_call_report_tracking', 'get_perfect_call_report_tracking_detail', 'data_pantry_check', 'report_call'),
            'custom' => [
                // views/ami/demo/custom/{template}
                'report_call', 'penetration_data_report', 'penetration_customers_report',
                'route_master_temp', 'grip_grab_report', 'promotions_activity_report'/*, 'last_pantry_check_report'*/
            ],
        );

    }

    protected function _setRegionalExportFields()
    {
        return array(
            'from_to' => array('data_route_plan', 'coaching_note', 'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'report_tfo', 'report_call',
                'report_sampling', 'report_pantry_check', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'data_inventory_check'),
            'month-year' => array('get_perfect_store_report', 'penetration_customers_report', 'penetration_data_report', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr', /*'report_call'*/),
            'season' => array('get_perfect_call_report_tracking_detail', 'get_perfect_call_report_tracking'),
            'country_list' => array('get_app_versions_report', 'penetration_customers_report', 'penetration_data_report', /*'report_call',*/
                'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'get_detail_app_versions_report', 'get_perfect_call_report_tracking_detail', 'get_perfect_store_report', 'coaching_note', 'data_customers', 'data_route_plan', 'data_ssd_mt', 'data_distributors', 'data_wholesalers', 'route_master_temp', 'customer_contact_data'),
            'multi_country_list' => array('get_perfect_call_report', 'report_tfo', 'report_call',
                'report_pantry_check', 'report_sampling', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'data_inventory_check',
                'get_perfect_call_report_tracking', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr'),
            'grip_customers' => array('penetration_data_report'),
            'app_type' => array('customer_contact_data', 'route_master_temp',
                'report_otm', 'report_customers', 'report_pantry_check', 'get_app_versions_report', 'data_route_plan', 'coaching_note', 'penetration_customers_report', 'penetration_data_report', 'data_tfo', 'competitor_reprot', 'data_call', 'data_sampling', 'get_perfect_store_report', 'get_perfect_call_report_tracking', 'get_perfect_call_report_tracking_detail', 'data_pantry_check', 'report_call')
        );
    }

    public function index()
    {
        $action = $this->uri->segment(3);
        if (!$action) {
            return redirect('/');
        }

        $filter = $this->input->get('filter');
        if (!$filter) {
            $filter = 'salespersons';
        }
        $this->data['filter'] = $filter;
        $this->data['action'] = $action;
        if ($filter == 'salespersons') {
            $this->data['fields'] = $this->_setExportFields();
        } else {
            $this->data['fields'] = $this->_setRegionalExportFields();
        }
        /* Promotion Activity*/
        $promotion_activities = $this->db->from('promotions_activity')
            ->select(array('id', 'title'))
            ->where(array(
                'country_id' => $this->country_id,
                'is_draft' => 0
            ))->get()->result_array();
        if ($promotion_activities) {
            $list_promotion = [];
            foreach ($promotion_activities as $promotion_activity) {
                $list_promotion[$promotion_activity['id']] = $promotion_activity['title'];
            }
            $this->data['promotion_activities'] = $list_promotion;
        }
        /* From and To */
        if (isset($this->data['fields']['from_to']) && in_array($action, $this->data['fields']['from_to'])) {
            $this->data['report']['from_to'] = array(
                'type' => 'datepicker'
            );
        }
        $countries = $this->country_m->getListCountry(null, array('id !=' => 0));

        /* Season */
        if (isset($this->data['fields']['season']) && in_array($action, $this->data['fields']['season'])) {

            $prizes_setting = $this->prizes_setting_m->get(null, false, 'date_start desc');
            $season_data = array();
            foreach ($prizes_setting as $k => $p) {
                $season_data[$p['date_start'] . '.' . $p['date_expired']] = 'Season ' . $p['season'] . ': ' . date('d/m/Y', strtotime($p['date_start'])) . ' - ' . date('d/m/Y', strtotime($p['date_expired']));
            }
            $this->data['report']['season'] = array(
                'type' => 'season',
                'prizes_setting' => $season_data
            );
        }

        /* Month and Year */
        if (isset($this->data['fields']['month-year']) && in_array($action, $this->data['fields']['month-year'])) {
            $this->data['report']['month-year'] = array(
                'type' => 'month-year'
            );
        }
        if (isset($this->data['fields']['number_sku']) && in_array($action, $this->data['fields']['number_sku'])) {
            $this->data['report']['number_sku'] = array(
                'type' => 'number_sku'
            );
        }
        if (isset($this->data['fields']['top_products']) && in_array($action, $this->data['fields']['top_products'])) {
            $this->data['report']['top_products'] = array(
                'type' => 'top_products'
            );
        }

        // TODO: Hot fix for undefined index ($this->data['fields']['country_list'])
        if (isset($this->data['fields']['country_list']) && in_array($action, $this->data['fields']['country_list'])) {
            $this->data['report']['country_list'] = array(
                'type' => 'country_list'
            );
        }

        if (isset($this->data['fields']['multi_country_list']) && in_array($action, $this->data['fields']['multi_country_list'])) {
            $this->data['report']['multi_country_list'] = array(
                'type' => 'multi_country_list'
            );
            if ($filter == 'regional') {
                $countries = array_merge(array(0 => 'ALL'), $countries);
            }
        }

        if (isset($this->data['fields']['app_type']) && in_array($action, $this->data['fields']['app_type'])) {
            $this->data['report']['app_type'] = array(
                'type' => 'app_type'
            );
        }
        if (isset($this->data['fields']['manager']) && in_array($action, $this->data['fields']['manager'])) {
            $this->data['report']['manager'] = array(
                'type' => 'managerId'
            );
        }

        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1,
            'roles_id' => intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'))

        );

        if (in_array($action, array('data_distributors', 'data_wholesalers'))) {
            $conditions['(type like "%push%" OR type like "%mix%")'] = null;
        } elseif (in_array($action, array('data_customers'))) {
            $conditions['(type like "%pull%" OR type like "%mix%")'] = null;
        } else {
            $conditions['(type like "%pull%" OR type like "%push%" OR type like "%mix%")'] = null;
        }
        if ($this->input->is_ajax_request()) {
            $manageId = $this->uri->segment(4);
            if ($manageId != '') {
                if (in_array(strtolower($manageId), array('push', 'pull', 'sl', 'app_all'))) {
                    if (strtolower($manageId) == 'sl') {
                        $sales = $this->salespersons_m->getPersonListOptions(null, array(
                            'country_id' => $this->country_id,
                            'is_draft' => 0,
                            // 'listed' => 1,
                            'active' => 1,
                            'type LIKE' => '%sl%'
                        ));
                    } elseif (strtolower($manageId) == 'app_all') {
                        $sales = $this->salespersons_m->getPersonListOptions(null, array(
                            'country_id' => $this->country_id,
                            'is_draft' => 0,
                            'active' => 1,
                            // 'listed' => 1
                        ));
                    } else {
                        $conditions = array_merge($conditions, array('(type LIKE "%' . $manageId . '%" OR type LIKE "%mix%")' => NULL));
                        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions);
                    }
                } else {
                    if ($manageId != 'manager_all') {
                        $conditions = array_merge($conditions, array('salespersons_manager_id' => $manageId));
                    }
                    $sales = $this->salespersons_m->getPersonListOptions(null, $conditions);
                }
            }
            if ($manageId != '') {
                $html = '<option value="" selected="selected">ALL</option>';
                foreach ($sales as $k => $s) {
                    $html .= '<option value="' . $k . '">' . $s . '</option>';
                }
            } else {
                $html = '';
            }
            echo $html;
            exit();
        }

        if ($this->hasPermission('manage_all', 'salespersons')) {
            $this->data['sr_manage'] = 'all';
            $salespersons = $this->salespersons_m->getPersonListOptions(null, $conditions);
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);

            $managers = $this->salespersons_m->getPersonListOptions(null, array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                // 'listed' => 1,
                'type LIKE' => '%sl%'
            ));
            $managers = array_merge(array('ALL' => 'All'), $managers);
        } else if ($this->hasPermission('manage_staff', 'salespersons')) {
            $this->data['sr_manage'] = 'staff';
            $user = $this->session->userdata('user_data');
            $conditions['salespersons_manager_id'] = $user['id'];
            $salespersons = $this->salespersons_m->getPersonListOptions(null, $conditions);
            $salespersons = array_merge(array('team' => 'MY TEAM'), $salespersons);

            $managers = $this->salespersons_m->getPersonListOptions(null, array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                // 'listed' => 1,
                'type LIKE' => '%sl%',
                'armstrong_2_salespersons_id' => $user['id']
            ));
            $managers = array_merge(array('ALL' => 'ALL'), $managers);
        } else {
            $salespersons = array();
            $managers = array();
        }

        if (isset($this->data['fields']['salespersons']) && in_array($action, $this->data['fields']['salespersons'])) {
            $this->data['report']['salespersons'] = array(
                'col' => 'armstrong_2_salespersons_id',
                'type' => 'multi_select',
                'list' => $salespersons
            );
            $this->data['report']['managers'] = array(
                'col' => 'managerId',
                'type' => 'select',
                'list' => $managers
            );
        }

        /* Customers / Operators */
        if (isset($this->data['fields']['customers']) && in_array($action, $this->data['fields']['customers'])) {
            $this->data['report']['customers'] = array(
                'col' => 'armstrong_2_customers_id',
                'type' => 'multi_select',
                'list' => $this->customers_m->getCustomerListOptions(null, array('country_id' => $this->country_id, 'approved' => 1))
            );
        }

        if (isset($this->data['fields']['top-ten']) && in_array($action, $this->data['fields']['top-ten'])) {
            $this->data['report']['top-ten'] = array(
                'col' => 'channel_id',
                'type' => 'multi_select',
                'list' => []//$this->country_channels_m->get_by(array('country_id' => $this->country_id))
            );
        }
        // $top_sku = $this->products_m->get_by(array(
        // 	'is_top_ten'=>1,
        // 	'listed'=>1,
        // 	'country_id'=>$this->country_id,
        // 	'is_draft' => 0,
        // 	'(main_sku IS NULL OR LENGTH(main_sku) <1)' => null,
        // ));

        $top_sku = $this->db->from('products')
            ->select('id')
            ->where(array(
                'is_top_ten' => 1,
                'listed' => 1,
                'country_id' => $this->country_id,
                'is_draft' => 0,
                '(main_sku IS NULL OR LENGTH(main_sku) <1)' => null,
            ))
            ->group_by('sku_number')
            ->get()->result_array();

        $this->data['number_sku']['0'] = 'All';
        if (count($top_sku) > 0) {
            $i = 1;
            foreach ($top_sku as $k => $t) {
                $this->data['number_sku'][$i] = $i;
                $i++;
            }
        }
        if (in_array($action, array('penetration_data_report', 'get_perfect_call_report_tracking', 'customer_contact_data', 'report_customers'))) {
            $this->data['app_type'] = array(
                'PULL' => 'PULL',
                'PUSH' => 'PUSH'
            );
        } else {
            $this->data['app_type'] = array(
                'PULL' => 'PULL',
                'PUSH' => 'PUSH',
                'SL' => 'SL'
            );
        }
        if (isset($this->data['fields']['grip_customers']) && in_array($action, $this->data['fields']['grip_customers'])) {
            $this->data['report']['grip_customers'] = array(
                'col' => 'managerId',
                'type' => 'grip_customers',
                'list' => array(
                    '0' => 'TOTAL customers',
                    '1' => '6 Months Grip',
                    '2' => '12 Months Grip'
                )
            );
        }
        $this->data['report']['filter'] = array(
            '-1' => 'All',
            '1' => 'Active',
            '0' => 'Inactive'
        );

        $this->data['report']['action_url'] = end($this->router->uri->segments);
        $this->data['c_tries'] = $countries;
        $this->data['slStatus'] = in_array($action, [
            'data_call', 'data_tfo', 'data_ssd', 'data_pantry_check', 'data_sampling', 'competitor_reprot',
            'objective_record_data', 'activities_log_data', 'get_detail_app_versions_report', 'get_perfect_call_report', 'get_perfect_call_report_tracking_detail', 'get_perfect_store_report', 'customer_contact_data'
        ]);

        $this->data['global_channels'] = $this->global_channels_m->parse_form(array('global_channels_id', 'name', 'id'), '- Select -', array('is_draft' => 0));
        if (isset($this->data['fields']['custom']) && in_array($action, $this->data['fields']['custom'])) {
            $this->data['custom'] = $this->_getCustomParams($action);
        }

        $this->data['permission'] = $this->getViewExportPermission();


        // if ($this->user_role_name == 'Sales Manager') {
        //     $data['salespersons'] = $this->Ami_model->getEntry('salespersons', array('salespersons_manager_id' => $userId, 'country_id' => $this->country_id), null, 'first_name, last_name, armstrong_2_salespersons_id');
        // }
        return $this->render('ami/demo/index');
    }

    protected function _getFullRegions()
    {
        $this->load->library('CountryParser');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);
        $root = $this->countryparser->parse($this->country_code)->get($hierachy['root']);

        // dd($this->countryparser->parse($this->country_code)->get('region'));

        $rootValues = array('ALL' => 'ALL');

        foreach ($root as $key => $value) {
            $rootValues[$value] = $value;
        }

        return [
            'countryHierachy' => $hierachy,
            'rootValues' => $rootValues,
        ];
    }

    protected function _getRegions()
    {
        $this->load->library('CountryParser');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);

        if (in_array('region', array_values($hierachy))) {
            $defaultRegion = array('ALL' => 'ALL');
            $regions = $this->countryparser->parse($this->country_code)->get('region');

            return is_array($regions) ? $defaultRegion + $regions : $defaultRegion;
        }

        return array();
    }

    protected function _getChannels()
    {
        $this->load->model('country_channels_m');

        return $this->country_channels_m->get_by(array('country_id' => $this->country_id, 'is_draft' => 0));
    }

    protected function _getCustomParams($action)
    {
        switch ($action) {
            case 'report_call':
            case 'penetration_customers_report':
            case 'penetration_data_report':
            case 'grip_grab_report':
            case 'promotions_activity_report':
                //case 'last_pantry_check_report':
                $conditions = array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    'active' => 1,
                    // 'roles_id' => intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'))
                );

                //$salesLeaderId = intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager'));
                //$salespersonId = intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'));

                $salesleadersConditions = array(
                    'type' => 'sl',
                    'test_acc' => 0
                );

                $countries = array_rewrite($this->countries, 'id');
                $salesleaders = $this->salespersons_m->get_by(array_merge($salesleadersConditions, $conditions));
                foreach ($salesleaders as $leader) {
                    $manager_id[] = $leader['armstrong_2_salespersons_id'];
                }

                $manager_id = '"' . implode('","', $manager_id) . '"';

                $salespersonsConditions = array(
                    "salespersons_manager_id IN ({$manager_id})" => null,
                    "type IN ('pull','push','mix')" => null,
                    'test_acc' => 0
                );
                $salespersons = $this->db->from('salespersons')->where(array_merge($salespersonsConditions, $conditions))->get()->result_array();
                $channels = $this->_getChannels();
                $unassigned_arr = array(
                    '0' => array(
                        'id' => -11,
                        'name' => 'Unassigned'
                    )
                );
                $params = [
                    'country' => $countries[$this->country_id],
                    'salesleaders' => $salesleaders,
                    'salespersons' => $salespersons,
                    'countryChannels' => array_merge($channels, $unassigned_arr),
                    // 'regions' => $this->_getRegions()
                ];

                break;

            default:
                $params = [];
                break;
        }
        return $params;
    }

    public function generate()
    {
        set_time_limit(0);
        $post = $this->input->post();
        $type = $post['type'];
        $sub_type = isset($post['sub_type']) ? explode(',', $post['sub_type']) : '';
        $filter = isset($post['filter']) ? $post['filter'] : '';
        $user = $this->session->userdata('user_data');
        $userId = $user['roles_id'] > 0 ? $user['id'] : 0;
        $from = !empty($post['from']) ? $post['from'] : '';
        $to = !empty($post['to']) ? $post['to'] : '';
        $app_type = !empty($post['app_type']) ? $post['app_type'] : '';
        $view_by = isset($post['view_by']) ? intval($post['view_by']) : '';
        $view_type = isset($post['view_type']) ? $post['view_type'] : '';
        $range = isset($post['rolling']) ? $post['rolling'] : '';
        $multi_range = isset($post['multi_rolling']) ? $post['multi_rolling'] : '';
        $promotions_name = isset($post['promotion_name']) ? $post['promotion_name'] : '';
        $global_channels = isset($post['global_channels']) ? $post['global_channels'] : '';
        $active = $listed = '';
        $sr_manage = 'all';
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $sr_manage = 'staff';
        }
        $sr_manage = '&sr_manage='. $sr_manage;
        if (isset($post['customers'])) {
            $gripCustomers = !is_null($post['customers']) ? $post['customers'] : '';
        }
        if (isset($post['active'])) {
            $active = !is_null($post['active']) ? $post['active'] : '';
            $listed = $active;
        }

        if (isset($post['filter_listed'])) {
            $listed = !is_null($post['filter_listed']) ? $post['filter_listed'] : '';
        }
        if (isset($post['season'])) {
            $season_arr = explode('.', $post['season']);
            $from = $season_arr['0'] . ' 00:00:00';
            $to = $season_arr['1'] . ' 23:59:59';
        }

        if ($this->input->is_ajax_request()) {
            $typeFile = '&typeFile=html';
        } else {
            $typeFile = '&typeFile=xlsx';
        }

        if ($filter != 'regional') {
            $salesperson_ids = '';
            if (array_key_exists('armstrong_2_salespersons_id', $post)) {

                if ($type != 'data_product' && $type != 'data_region_master') {
                    // if sl == all
                    $salesperson_ids = "&spId=" . $userId;
                    $getall = "&getall=1";
                    // else --> getAll=2 AND spId= sl[]
                    if (isset($post['sales_leader']) && $post['sales_leader']) {
                        if (isset($post['selectAllsales_leader']) && isset($post['selectAllarmstrong_2_salespersons_id'])) {
                            $salesperson_ids = "&spId=0";
                            $getall = '&getall=1';
                        } else {
                            $salesperson_ids = "&spId=";
                            $salesperson_ids .= implode('&spId=', $post['armstrong_2_salespersons_id']);
                            $salesperson_ids = str_replace(',', '&spId=', $salesperson_ids);
                            $getall = "&getall=0";
                        }
                        if ($sub_type) {
                            foreach ($sub_type as $_sub_type) {
                                if (in_array($_sub_type, array('nsm', 'md', 'cdops'))) {
                                    $salesperson_ids = "&spId=" . $userId;
                                    $getall = "&getall=1";
                                    break;
                                }
                            }
                        }
                    }else{
                        $salesperson_ids = "&spId=0";
                        if ($this->hasPermission('manage_staff', 'salespersons'))
                            $salesperson_ids = "&spId=" . $userId;
                    }
                }
            } else {
                $getall = "&getall=0";
                $salesperson_ids = "&spId=" . $userId;
            }

            $channel = $otm = $top_product = '';
            if (isset($post['channel'])) {
                if (in_array(-1, $post['channel']) || $view_by == 0) {
                    $channel .= '&channel=-1';
                } else {
                    $channel .= '&channel=';
                    $channel .= implode('&channel=', $post['channel']);
                    $channel = str_replace(',', '&channel=', $channel);
                }
            }

            $top_product .= '&topProducts=';
            $products = isset($post['top_products']) ? $post['top_products'] : [];
            $otherProducts = (isset($post['other_products']) && array_filter($post['other_products'])) ? $post['other_products'] : [];
            if (!$products && $otherProducts) {
                $products = $otherProducts;
            } elseif ($products && $otherProducts) {
                $products = array_merge($post['top_products'], $post['other_products']);
            }
            $top_product .= implode('&topProducts=', $products);
            $top_product = str_replace(',', '&topProducts=', $top_product);
            // if (isset($post['sales_leader']))
            // {
            // 	if (in_array('ALL', $post['sales_leader']))
            // 	{
            // 		$sLeader .= '&sales_leader=ALL';
            // 	}
            // 	else
            // 	{
            // 		$sLeader .= '&sales_leader=';
            // 		$sLeader .= implode('&sales_leader=', $post['sales_leader']);
            // 	}
            // }

            if (isset($post['otm'])) {
                if (in_array('ALL', $post['otm'])) {
                    $otm .= '&otm=ALL';
                } else {
                    $otm .= '&otm=';
                    $otm .= implode('&otm=', $post['otm']);
                    $otm = str_replace(',', '&otm=', $otm);
                }
            }

            if ($view_by == 1) {
                $getall = '&getall=1';
                $salesperson_ids = "&spId=0";
            }
        } else {
            $getall = '&getall=1';
            $salesperson_ids = "&spId=0";
            $channel = '&channel=-1';
            $otm = '&otm=ALL';
        }
        $from_date = str_replace(':', '%3A', $from . ' 00:00:00');
        $to_date = str_replace(':', '%3A', $to . ' 23:59:59');
        $is_multiCountry = '&is_multi_country=0';
        if (isset($post['country_list']) && $post['country_list']) {
            if (is_array($post['country_list'])) {
                $this->country_id = 'country=';
                $this->country_id = implode('&country=', $post['country_list']);
                $this->country_id = str_replace(',', '&country=', $this->country_id);
                $is_multiCountry = '&is_multi_country=1';
            } else {
                $this->country_id = intval($post['country_list']);
            }
        }
        switch ($type) {
            case 'data_inventory_check':
                $reports = 'transactiondata/data_inventory_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'get_perfect_store_report':
                $active = '&active=' . $active;
                $reports = 'transactiondata/get_perfect_store_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $post['app_type'] . $active . $is_multiCountry;
                break;
            case 'get_perfect_call_report_tracking_detail':
                $active = '&active=' . $active;
                $reports = 'transactiondata/get_perfect_call_report_tracking_detail';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $post['app_type'] . $active . $is_multiCountry;
                break;
            case 'get_perfect_call_report_tracking':
                $reports = 'summaryreports/get_perfect_call_report_tracking';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $post['app_type'] . $is_multiCountry;
                break;
            case 'get_perfect_call_report':
                $reports = 'transactiondata/get_perfect_call_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'get_detail_app_versions_report':
                $reports = 'transactiondata/get_detail_app_versions_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'get_app_versions_report':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/get_app_versions_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $is_multiCountry;
                break;
            case 'report_all_in_one':
                $reports = 'summaryreports/get_all_in_one';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $is_multiCountry;
                break;
            case 'report_call' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/reportcall';
                $holidays = array();
                $workingDay = '&workingDay=' . $this->getWorkingDays($from, $to, $holidays);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $workingDay . '&from_date=' . $from_date . '&to_date=' . $to_date . $channel . $otm . $app_type . $is_multiCountry;
                break;
            case 'report_customers':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/customers';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $is_multiCountry;
                break;
            case 'data_call':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_call';  //  $reports = 'Transaction%20Data:data_call.prpt';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $is_multiCountry;
                break;
            case 'coaching_note':
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/coaching_note';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $is_multiCountry;
                break;
            case 'data_ssd':
                $reports = 'transactiondata/data_ssd';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'data_customers':
                $app_type = '&app_type=PULL';
                $reports = 'masterdata/data_customers';
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $app_type . $is_multiCountry;
                break;
            case 'route_master_temp':
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/route_master_temp';
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $app_type . $is_multiCountry;
                break;
            case 'customer_contact_data':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/customer_contact_data';
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $app_type . $active . $is_multiCountry;
                break;
            case 'report_pantry_check':
                $reports = 'summaryreports/report_pantry_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $app_type . $is_multiCountry;
                break;
            case 'report_otm' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/report_otm';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $is_multiCountry;
                break;
            case 'top_ten_penetration_sr':
                $reports = 'summaryreports/top_ten_penetration_sr';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'report_route_plan':
                $reports = 'summaryreports/route_plan_reprot';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $is_multiCountry;
                break;
            case 'data_pantry_check':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_pantry_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $active . $is_multiCountry;
                break;
            case 'report_sampling':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/sampling_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $is_multiCountry;
                break;
            case 'report_grip_and_grab_sr':
                $reports = 'summaryreports/grip_and_grab_report_sr';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'report_tfo':
                $reports = 'summaryreports/tfo_reprot_sr';  //  $reports = 'Transaction%20Data:data_call.prpt';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'data_route_plan':
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/data_route_plan';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $is_multiCountry;
                break;
            case 'data_product':
                $listed = '&productActive=' . $listed;
                $reports = 'masterdata/product_data';
                $param = $reports . '/?country=' . $this->country_id . $typeFile . $listed . $is_multiCountry;
                break;
            case 'data_sampling':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_sampling';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $is_multiCountry;
                break;
            case 'data_tfo':
                $active = '&active=' . $active;
                $reports = 'transactiondata/data_tfo';
                $app_type = '&app_type=' . $app_type;
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $is_multiCountry;
                break;
            case 'data_organization' :
                $reports = 'masterdata/salespersons';
                $salesperson_ids = "&spId=" . $this->salesperson_id;
                $param = $reports . '?country=' . $this->country_id . $typeFile . $salesperson_ids . $is_multiCountry;
                break;
            case 'top_ten_penetration_calculation_tr' :
                $reports = 'transactiondata/top_ten_penetration_calculation_tr';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'new_grip_report' :
                $reports = 'transactiondata/new_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'new_grab_report' :
                $reports = 'transactiondata/new_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'lost_grip_report' :
                $reports = 'transactiondata/lost_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'lost_grab_report' :
                $reports = 'transactiondata/lost_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'potential_lost_grip_report' :
                $reports = 'transactiondata/potential_lost_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'potential_lost_grab_report' :
                $reports = 'transactiondata/potential_lost_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'data_otm':
                $reports = 'transactiondata/data_otm';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'report_approval':
                $reports = 'summaryreports/approval_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'top_ten_penetration_tr':
                $reports = 'transactiondata/top_ten_penetration_tr';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'grip_grab_report':
                $reports = 'summaryreports/grip_and_grab_report_sr';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . '&app_type=' . $app_type . $channel . $otm . $sr_manage . $is_multiCountry;
                break;
            case 'penetration_data_report':
                $reports = 'summaryreports/penetration_data_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $post['view_by'] = intval($post['view_by']);
                $range = '&range=' . $range;
                $listed = '&productActive=' . $listed;
                $global_channels = '&globalChannels=' . $global_channels;

                if ($post['view_by'] == 0 || $post['view_by'] == 1) {
                    $range = '&range=';
                    $range .= implode('&range=', $multi_range);
                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $app_type . $listed . $channel . $global_channels . $otm . '&isGripCustomer=' . $gripCustomers . $top_product . $range . '&isViewByChannel=' . $post['view_by'] . $sr_manage . $is_multiCountry;
                } elseif ($post['view_by'] == 3) {
                    $reports = 'summaryreports/penetration_data_report_channel_sku';
                    $param = $reports . '?country=' . $this->country_id . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $app_type . $listed. $channel . $global_channels . $otm . '&isGripCustomer=' . $gripCustomers . $top_product . $range . '&isViewByChannel=' . $post['view_by'] . $sr_manage . $is_multiCountry;
                } elseif ($post['view_by'] == 5) {
                    $reports = 'summaryreports/penetration_data_report_sr_sku';
                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $app_type . $global_channels . $listed . $top_product . $range . '&isGripCustomer=' . $gripCustomers . '&isViewByChannel=' . $post['view_by'] . $sr_manage . $is_multiCountry;
                } elseif ($post['view_by'] == 6) {
                    $reports = 'summaryreports/penetration_data_report_sr_channel';
                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $app_type . $listed . $channel . $global_channels . $otm . '&isGripCustomer=' . $gripCustomers . $top_product . $range . '&isViewByChannel=' . $post['view_by'] . $sr_manage . $is_multiCountry;
                }
                break;
            case 'penetration_customers_report':
                $reports = 'summaryreports/penetration_customers_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $post['number_sku'] = intval($post['number_sku']);
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . '&month=' . $post['month'] . '&year=' . $post['year'] . '&numberSku=' . $post['number_sku'] . '&app_type=' . $app_type . $channel . $otm . $is_multiCountry;
                break;
            case 'rich_media':
                $reports = 'summaryreports/rich_media';
                $param = $reports . '?country=' . $this->country_id . $sr_manage;
                break;
            case 'promotions_activity_report':
                $reports = 'summaryreports/promotion_activities_report';
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . '&promotionActivityID=' . $promotions_name . $sr_manage . $is_multiCountry;
                break;
            /*case 'last_pantry_check_report':
                $reports = 'summaryreports/report_last_pantry_check';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $typeFile . $param_date . $app_type . $getall . $salesperson_ids . '&view_type=' . $view_type . $is_multiCountry;
                break;*/
            case 'competitor_reprot':
                $active = '&active=' . $active;
                $reports = 'transactiondata/competitor_reprot';
                $app_type = '&app_type=' . $app_type;
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $is_multiCountry;
                break;
            case 'activities_log_data':
                $reports = 'transactiondata/activities_log_data';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'objective_record_data':
                $reports = 'transactiondata/objective_record_data';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'data_region_master' :
                $loc = substr(__FILE__, 0, 25);
                $files_patch = '';
                switch ($this->country_id) {
                    case 1:
                        $files_patch .= '&files=' . $loc . '\res\json\my.json';
                        break;
                    case 2:
                        $files_patch .= '&files=' . $loc . '\res\json\au.json';
                        break;
                    case 3:
                        $files_patch .= '&files=' . $loc . '\res\json\hk.json';
                        break;
                    case 4:
                        $files_patch .= '&files=' . $loc . '\res\json\sa.json';
                        break;
                    case 5:
                        $files_patch .= '&files=' . $loc . '\res\json\sg.json';
                        break;
                    case 6:
                        $files_patch .= '&files=' . $loc . '\res\json\tw.json';
                        break;
                    case 7:
                        $files_patch .= '&files=' . $loc . '\res\json\th.json';
                        break;
                    default:
                        $files_patch .= '&files=';
                        break;
                }

                $region = str_replace('\\', '%5C', str_replace(':', '%3A', str_replace('.', '%2E', $files_patch)));
                $reports = 'masterdata/region';
                $typefi = str_replace('&', '', $typeFile);
                $param = $reports . '?' . $typefi . $region . '&country=' . $this->country_id . $is_multiCountry;
                break;
            case 'report_summary':
                $reports = 'summaryreports/all_in_one_transactions';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'new_customer_report':
                $reports = 'transactiondata/new_customer_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'data_ssd_mt':
                $reports = 'masterdata/ssd';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'data_distributors':
                $reports = 'masterdata/distributors';
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . $is_multiCountry;
                break;
            case 'data_wholesalers':
                $reports = 'masterdata/wholesalers';
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . $is_multiCountry;
                break;
            case 'data_recipes':
                $reports = 'masterdata/data_recipes';
                $param = $reports . '/?country=' . $this->country_id . $typeFile . $is_multiCountry;
                break;
        }

        if ($this->input->is_ajax_request()) {
            $html[] = $this->config->item('report_url') . $param;
            echo json_encode($html);
        } else {
            header("Location: " . $this->config->item('report_url') . $param);
        }
    }

    public function getWorkingDays($startDate, $endDate, $holidays)
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;
        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);
        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);
        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;
                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }
        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }
        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }
        return $workingDays;
    }

    public function view()
    {
        $get = $this->input->get();
        $data = (isset($get['data'])) ? $get['data'] : '';
        $this->data['data'] = $data;
        $this->data['permission'] = $this->getViewExportPermission();
        $this->data['sr_manage'] = 'all';
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $this->data['sr_manage'] = 'staff';
        }
        return $this->render('ami/demo/view');

    }

    function getViewExportPermission()
    {
        $permission = array();
        if (!$this->isSuperAdmin()) {
            $user = $this->session->userdata('user_data');
            $roleId = isset($user['roles_id']) ? $user['roles_id'] : null;
            $permission = $this->roles_m->checkReportPermission($roleId);
        } else {
            $permission = array(
                'report_view' => 1,
                'report_export' => 1
            );
        }
        return $permission;
    }

    public function generate_pdf()
    {
        $post = $this->input->post();
        if (isset($post['promotion_activity_id'])) {
            $data = $this->db->from('promotions_activity_submit')
                ->where('id', $post['promotion_activity_id'])
                ->get()->result_array();
            if ($data) {
                $data = $data[0];
                $html = '<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <title>Promotions</title>
    <style type="text/css">
		.container-fluid {
			padding: 0;
			margin-right: auto;
			margin-left: auto;
		}
		.col-sm-12 {
			width: 100%;
			float: left;
		}
		.col-sm-6 {
			width: 50%;
			float: left;
		}
		.col-sm-4 {
			width: 33.33333333%;
			float: left;
		}
		.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
			min-height: 1px;
			padding-left: 15px;
			padding-right: 15px;
		}
		* {
			box-sizing: border-box;
		}
		body{
			font-size: 17px;
		}
		.m-t-30{
			margin-top: 30px;
		}
		.mbt-15{
			margin-bottom: 15px;
		}
		.mbt-30{
			margin-bottom: 30px;
		}
		.mbt-70{
			margin-bottom: 70px;
		}
		.form-title {
			margin-top: 30px;
			font-size: 35px;
			font-weight: bold;
			color: #111;
		}
		.form-body {
			margin-top: 30px;
			overflow: hidden;
		}
		.text_block {
			border-bottom: 2px solid #ddd;
		}
		.modal-body{
			position: relative;
			padding: 0 60px 0 60px;
		}
		.modal-content{
			overflow: hidden;
			position: relative;
			background-color: #ffffff;
			border: 1px solid rgba(0, 0, 0, 0.2);
			border-radius: 6px;
			background-clip: padding-box;
			outline: 0;
		}
		.modal-dialog {
			position: relative;
			width: 900px;
			margin: 30px auto;
		}
		.form-group {
			overflow: hidden;
			margin-left: -15px;
			margin-right: -15px;
			margin-bottom: 25px;
		}
		.form-control {
			display: block;
			width: 100%;
			height: 34px;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857143;
			color: #555555;
			background-color: #ffffff;
			background-image: none;
			border: 1px solid #cccccc;
			border-radius: 4px;
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.input-text {
			height: 50px;
			padding: 15px;
			border-radius: 10px;
			border: 1px solid #888;
			font-size: 18px;
		}
        .checkradios-checkbox, .checkradios-radio {
            border: 1px solid #999;
            color: #111;
            width: 1.2em;
            height: 1.2em;
            margin-top: 5px;
        }

        .color-text {
            color: #f4763c;
        }
		label {
			display: inline-block;
			max-width: 100%;
			margin-bottom: 5px;
			font-weight: 700;
		}
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="Tutorial" style="display: block">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text_block">
                        <div class="form-title mbt-15">
                            Entry Form
                        </div>
                    </div>
                    <div class="form-body">
                        <form id="formContact" class="form-horizontal mbt-30">
                            <div class="form-group">
                                <div class="col-sm-6">
									<label>Date of Order</label>
                                    <input id="dateOrder" class="form-control input-text" type="text"
                                           name="date_order" placeholder="Date of Order" value="' . $data['date_order'] . '">
                                </div>
                                <div class="col-sm-6">
									<label>Channel</label>
                                    <input id="channel" class="form-control input-text" type="text"
                                           name="channel" placeholder="Channel" data-title="Channel" value="' . $data['channel'] . '">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
									<label>Full Name</label>
                                    <input name="manual_contact" id="manualContact" type="text"
                                           class="form-control input-text not-require" placeholder="Full Name" value="' . $data['contact_name'] . '">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
									<label>Job Title</label>
                                    <input id="jobTitle" class="form-control input-text" type="text"
                                           name="job_title" placeholder="Job Title" data-title="Job Title" value="' . $data['job_title'] . '">
                                </div>
                                <div class="col-sm-6">
									<label>Business Name</label>
                                    <input id="businessName" class="form-control input-text" type="text"
                                           name="business_name" placeholder="Business Name" value="' . $data['business_name'] . '"
                                           data-title="Business Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
									<label>Address</label>
                                    <input id="address" class="form-control input-text" type="text"
                                           name="address" placeholder="Address" data-title="Address" value="' . $data['address'] . '">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
									<label>Suburb</label>
                                    <input id="suburb" class="form-control input-text not-require" type="text"
                                           name="suburb" placeholder="Suburb" data-title="Suburb" value="' . $data['suburb'] . '">
                                </div>
                                <div class="col-sm-4">
									<label>State</label>
                                    <input id="state" class="form-control input-text" type="text"
                                           name="state" placeholder="State" data-title="State" value="' . $data['state'] . '">
                                </div>
                                <div class="col-sm-4">
									<label>Postcode</label>
                                    <input id="postcode" class="form-control input-text" type="text"
                                           name="postcode" placeholder="Postcode" data-title="Postcode" value="' . $data['postcode'] . '">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
									<label>Phone</label>
                                    <input id="phone" class="form-control input-text" type="text"
                                           name="phone" placeholder="Phone" data-title="Phone" value="' . $data['phone'] . '">
                                </div>
                                <div class="col-sm-6">
									<label>Email</label>
                                    <input id="email" class="form-control input-text not-require" type="text"
                                           name="email" placeholder="Email" data-title="Email" value="' . $data['email'] . '">
                                </div>
                            </div>
                        </form>
                        <div style="width: 50%; float: right; text-align: center">
                            <label>Signature</label>
                            <img alt="Signature" src="data:image/png;base64,' . $data['signature_base64'] . '" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>';
                $path_root = 'res/promotion_activity_html';
                if (!is_dir($path_root)) {
                    mkdir($path_root, 0777, true);
                    chmod($path_root, 0777);
                }
                $file_name = $data['armstrong_2_salespersons_id'] . '_' . date('Y_m_d') . '_' . time() . '.html';
                $path_to_html_file = $path_root . '/' . $file_name;
                $htmlfile = fopen($path_to_html_file, "w") or die("Unable to open file!");
                fwrite($htmlfile, $html);
                $api_url = $this->config->item('pdf_api') . '?url=' . base_url() . $path_to_html_file;
                header("Location: " . $api_url . '&--orientation=Portrait');
            }
        }
    }
}