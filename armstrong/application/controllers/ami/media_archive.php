<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Media_archive extends AMI_Controller
{

    public $country_id;
    public $app_type = 0;
    protected $app_type_text = '';
    protected $zip_media = '';

    protected $source = '';
    protected $source_relative = '';
    protected $destination = '';


    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_m');
        $this->load->model('media_archive_m');
        $this->load->model('media_m');
        $this->load->model('media_ref_m');

        // $this->load->library('zip');
        $this->load->helper('download');
        if(isset($_POST['country_id']) && $this->input->post('country_id')){
            $this->country_id = $this->input->post('country_id');
        }
        if(isset($_POST['app_type']) && $this->input->post('app_type')){
            $this->app_type = $this->input->post('app_type');
        }
        
        $this->source = $this->path()['source'];
        $this->source_relative = $this->path()['source_relative'];
        $this->destination = $this->path()['destination'];

        $this->setAppTypeText();

        $title = "Media Archive";
        $this->data['page_title'] = page_title($title);
        $this->data['module_title'] = $title;

        $this->data['app_type_push_button_state'] = '';
        $this->data['app_type_pull_button_state'] = '';
        $this->data['app_type_sl_button_state'] = '';

        $this->zip_media = new ZipArchive;
    }

    public function pull()
    {
        if (!$this->hasPermission('view', 'media_archive')) {
            return $this->noPermission();
        }

        $this->data['app_type_title'] = 'Pull';
        $this->data['app_type_pull_button_state'] = 'active';
        $this->data['media_archive'] = $this->media_archive_m->getAllArchivePerCountry(0);

        return $this->render('ami/media_archive/index', $this->data);
    }

    public function push()
    {
        if (!$this->hasPermission('view', 'media_archive')) {
            return $this->noPermission();
        }

        $this->data['app_type_title'] = 'Push';
        $this->data['app_type_push_button_state'] = 'active';
        $this->data['media_archive'] = $this->media_archive_m->getAllArchivePerCountry(1);

        return $this->render('ami/media_archive/index', $this->data);
    }

    public function sl()
    {
        if (!$this->hasPermission('view', 'media_archive')) {
            return $this->noPermission();
        }

        $this->data['app_type_title'] = 'SL';
        $this->data['app_type_sl_button_state'] = 'active';
        $this->data['media_archive'] = $this->media_archive_m->getAllArchivePerCountry(2);

        return $this->render('ami/media_archive/index', $this->data);
    }

    // public function getAllMediaPerCountry() {
    //     $no_app_type = $this->media_archive_m->getAllMediaPerCountry(5, 2);
    //     echo "<pre>";
    //     print_r($no_app_type);
    //     echo "</pre>";
    // }

    public function archive()
    {
        if ($this->input->is_ajax_request()) {
            $media = $this->media_archive_m->getAllMediaPerCountry($this->country_id, $this->app_type);

            $test = array();

            if ($media) {
                $isFileExists = array();

                $media_archive = $this->destination . $this->app_type_text . '-' . $this->country_id . '.zip';
                
                $media_archive_rename = $this->destination . 'OLD-' . $this->app_type_text . '-' . $this->country_id . '.zip';
                $old = false;
                if(file_exists($media_archive)) {
                    unlink($media_archive);
                    // $old = rename($media_archive, $media_archive_rename);
                }

                if($this->zip_media->open($media_archive, ZipArchive::CREATE) === TRUE) {
                    foreach ($media as $key => $value) {
                        $file = $this->source . $value['media_type'] . DIRECTORY_SEPARATOR . $value['filename'];
                        $file_relative = $this->source_relative . $value['media_type'] . DIRECTORY_SEPARATOR . $value['filename'];
                        if (file_exists($file_relative)) {
                            $this->zip_media->addFile($file_relative, $value['filename']);
                            $isFileExists[] = true;
                            $test[] = $file_relative;
                        }
                    }
                }

                $this->zip_media->close();

                $destinationExist = $this->isDestinationExistOrCreate();

                if(in_array(true, $isFileExists) && $destinationExist)
                {
                    $data = array(
                            'size' => filesize($media_archive),
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                    $is_media_in_db = $this->media_archive_m->isMediaInDB($this->country_id, $this->app_type);
                    if ($is_media_in_db) {
                        $result = $this->media_archive_m->update($this->country_id, $this->app_type, $data);
                    } else {
                        $this->save($data);
                    }

                    $result = array('isSuccess' => true);

                    // if ($old) {
                    //     $result['rename'] = $media_archive_rename;
                    //     // unlink($media_archive_rename);
                    // }

                } else {
                    $result = array('isSuccess' => false, 'message' => 'No file found.');
                }


            } else {
                $result = array('isSuccess' => false, 'message' => 'No media found.');
            }

            $result['test'] = $test;

            echo json_encode($result);
            exit;
        }
    }

    public function save($data1)
    {
        $data2 = array(
            'app_type' => $this->app_type,
            'country_id' => $this->country_id,
            'created_at' => date('Y-m-d H:i:s')
        );

        $merged = array_merge($data1, $data2);

        $isSuccess = $this->media_archive_m->save($this->country_id, $this->app_type, $merged);

        return $isSuccess;
    }

    public function download()
    {
        $this->country_id = $_GET['country_id'];
        $this->app_type = $_GET['app_type'];

        $this->setAppTypeText();
// destination
        $filename = $this->app_type_text . '-' . $this->country_id . '.zip';
        $file = $this->destination . $filename;

        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-length: " . filesize($file));
        header("Pragma: no-cache"); 
        header("Expires: 0");
        @readfile("$file");
    }

    public function isArchiveExist()
    {
        $filename = $this->app_type_text . '-' . $this->country_id . '.zip';
        $file = $this->destination . $filename;

        if(file_exists($file)) {
            $result = array('isSuccess' => true, 'file' => $file);
        } else {
            $result = array('isSuccess' => false, 'message' => 'No file to download.', 'file' => $file);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function path()
    {
        // source = /armstrong-v3/res/up
        // destination = /armstrong-v3/res/media_archive

        if ($_SERVER['SERVER_NAME'] == "localhost") {
            return [
                'source' => FCPATH . 'test' . DIRECTORY_SEPARATOR . 'source' . DIRECTORY_SEPARATOR,
                'source_relative' => FCPATH . 'test' . DIRECTORY_SEPARATOR . 'source' . DIRECTORY_SEPARATOR,
                'destination' => FCPATH . 'test' . DIRECTORY_SEPARATOR . 'destination' . DIRECTORY_SEPARATOR
            ];
        } else {
            return [
                'source' => $this->config->item('base_api_url')
                                    . 'res'
                                    . DIRECTORY_SEPARATOR . 'up'
                                    . DIRECTORY_SEPARATOR,
                'source_relative' => DIRECTORY_SEPARATOR . 'mnt'
                                    . DIRECTORY_SEPARATOR . 'web'
                                    . DIRECTORY_SEPARATOR . 'armstrong-v3'
                                    . DIRECTORY_SEPARATOR . 'res'
                                    . DIRECTORY_SEPARATOR . 'up'
                                    . DIRECTORY_SEPARATOR,
                'destination' => DIRECTORY_SEPARATOR . 'mnt'
                                    . DIRECTORY_SEPARATOR . 'web'
                                    . DIRECTORY_SEPARATOR . 'armstrong-v3'
                                    . DIRECTORY_SEPARATOR . 'res'
                                    . DIRECTORY_SEPARATOR . 'media_archive'
                                    . DIRECTORY_SEPARATOR
            ];
        }
    }

    public function isDestinationExistOrCreate() {
        $this->destination = $this->path()['destination'];
        $destination = rtrim($this->destination, DIRECTORY_SEPARATOR);

        $isSuccess = false;

        if (!file_exists($destination)) {
            $isSuccess = mkdir($destination, 0700);
        } else {
            $isSuccess = true;
        }

        return $isSuccess;
    }

    public function setAppTypeText() {
        switch ($this->app_type) {
            case 0:
                $this->app_type_text = 'pull';
                break;
            case 1:
                $this->app_type_text = 'push';
                break;
            case 2:
                $this->app_type_text = 'sl'; // all
                break;
        }
    }
}