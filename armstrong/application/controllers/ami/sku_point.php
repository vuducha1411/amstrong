<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sku_point extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sku_point_m');
        $this->load->model('country_channels_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'sku_point')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['sku_points'] = $this->sku_point_m->getEntry('sku_point', $conditions);
        return $this->render('ami/sku_point/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Sku_point_dt'));

        $this->sku_point_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();

        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';
            if ($this->hasPermission('edit', 'sku_point')) {
                $_data['buttons'] .= html_btn(site_url('ami/sku_point/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }
            if ($this->hasPermission('delete', 'sku_point')) {
                $_data['buttons'] .= html_btn(site_url('ami/sku_point/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }
            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    protected function _getAddEditParams()
    {
        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $sql = "SELECT  `application_config` 
                FROM  `country` 
                WHERE  `id` = ".$this->country_id.""; 
        $config = $this->db->query($sql)->row_array();

        $config_array = (array)json_decode($config['application_config'], true);

        $enabled_avp = 0;

        if (isset($config_array['leader']['enable_enhanced_avp'])){
            $enabled_avp = $config_array['leader']['enable_enhanced_avp'];
        }

        //print_r($config_array['leader']['enable_enhanced_avp']);
        
        return array(
            'country_channels' => $this->country_channels_m->get_by($conditions),
            'enable_enhanced_avp' => $enabled_avp
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'sku_point')) {
            return $this->noPermission();
        }


        $this->data += $this->_getAddEditParams();
        return $this->render('ami/sku_point/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'sku_point') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sku_point');
        if ($id) {
            $this->data['sku_point'] = $this->sku_point_m->get($id);
            if(isset($this->data['sku_point']))
            {
                $this->data['sku_point']['point_settings'] = (array)json_decode($this->data['sku_point']['point_settings']);
                foreach($this->data['sku_point']['point_settings'] as &$setting)
                {
                    $setting = objectToArray($setting);
                }

                $this->data['sku_point']['channel'] = (array)json_decode($this->data['sku_point']['channel']);
                foreach($this->data['sku_point']['channel'] as &$channel)
                {
                    $channel = objectToArray($channel);
                }

                $today = date("Y-m-d H:i:s"); 
                $dateFlag = 0;
                if((strtotime($this->data['sku_point']['from_date']) < strtotime($today)) && (strtotime($this->data['sku_point']['to_date']) < strtotime($today))){
                    $dateFlag = 1;
                }

                $this->data['sku_point']['dateFlag'] = $dateFlag;
            }
            
            $this->data['page_title'] = page_title('Edit SKU Point');
            $this->data += $this->_getAddEditParams();
            return $this->render('ami/sku_point/edit', $this->data);
        }

        return redirect('ami/sku_point');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'sku_point') && !$this->hasPermission('edit', 'sku_point')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->sku_point_m->array_from_post(array(
            'id', 'from_date', 'to_date', 'name', 'channel', 'point_settings'
        ));

        $sql = "SELECT  `application_config` 
                FROM  `country` 
                WHERE  `id` = ".$this->country_id.""; 
        $config = $this->db->query($sql)->row_array();

        $config_array = (array)json_decode($config['application_config'], true);
        $enable_enhanced_avp = $config_array['leader']['enable_enhanced_avp'];


        $data['country_id'] = $this->country_id;
        $data['from_date'] = $data['from_date'] . ' 00:00:00';
        $data['to_date'] = $data['to_date'] . ' 23:59:59';
        $data['name'] = $data['name'];
        if ($enable_enhanced_avp == 1){
            if (!$data['channel']){
                $this->data = array(
                        'sku_point' => $data,
                        'errors' => 'Channel is required.'
                    );
                    $this->data += $this->_getAddEditParams();
                    return $this->render("ami/sku_point/edit", $this->data);
            }

            $data['channel'] = json_encode($data['channel']);
        }else{
            $data['channel'] = "\"ALL\"";
        }
        $id = $data['id'] ? $data['id'] : null;

        $idCon = "";
        if ($id!=""){
            $idCon = "and id <> ".$data['id']."";
        }

        // Check overlap datetime
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0
        );
        if ($id) $conditions['id !='] = $id;
        $date_times = $this->db->from('sku_point')
            ->select(['from_date', 'to_date'])
            ->where($conditions)
            ->get()->result_array();
        if ($date_times) {
            $date_arr = [$data['from_date'], $data['to_date']];
            foreach ($date_times as $date_time) {
                $date_arr[] = $date_time['from_date'];
                $date_arr[] = $date_time['to_date'];
            }
            if ($enable_enhanced_avp != 1){
                if($this->multipleDateRangeOverlaps($date_arr)){
                    $this->data = array(
                        'sku_point' => $data,
                        'errors' => 'Date overlaps with another SKU Point'
                    );
                    $this->data += $this->_getAddEditParams();
                    return $this->render("ami/sku_point/edit", $this->data);
                }
            }
        }
        
        if ($enable_enhanced_avp == 1){
            //check overlapping of channels
            $sql = "SELECT channel, name
                    FROM `sku_point` 
                    WHERE country_id={$this->country_id} 
                    and is_draft = 0
                    ".$idCon."
                    and ((`from_date` between '".$data['from_date']."' and '".$data['to_date']."' or `to_date` between '".$data['from_date']."' and '".$data['to_date']."') or ('".$data['from_date']."' between from_date and to_date) or ('".$data['to_date']."' between from_date and to_date))"; 
            $channels = $this->db->query($sql)->result_array();
            
            $ch_array = array();
            if ($channels) {
                foreach ($channels as $channel) {
                    $channel2 = (array)json_decode($channel['channel']);
                    foreach ($channel2 as $ch) {
                        array_push($ch_array, $ch);
                    }
                }
            }
            $channels_arr = (array)json_decode($data['channel']);
            
            foreach ($channels_arr as $ch2) {
                if (in_array($ch2, $ch_array)){
                    $this->data = array(
                        'sku_point' => $data,
                        'errors' => 'Channels exists in another SKU Point that overlaps with the dates chosen'
                    );

                    $this->data += $this->_getAddEditParams();
                    return $this->render("ami/sku_point/edit", $this->data);
                }
            }
        }

        if(isset($data['point_settings']))
        {
            $settingdata = array();
            foreach($data['point_settings'] as $setting)
            {
                $settingdata[] = (object)($setting);
            }
            $data['point_settings'] = json_encode($settingdata);
        }
        else
        {
            $data['point_settings'] = json_encode(array());
        }
        $this->sku_point_m->save($data, $id);

        return redirect('ami/sku_point');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'sku_point')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->sku_point_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/sku_point');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'sku_point')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sku_point');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/sku_point';
                //$this->chainss_m->delete($id);
                $this->sku_point_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/sku_point/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from sku point"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/sku_point');
    }

}