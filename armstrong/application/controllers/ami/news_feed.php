<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_feed extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_feed_m');
        $this->load->model('media_m');
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $news = $this->news_feed_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $news = $this->news_feed_m->prepareDataTables($news);

        return $this->datatables->generate($news);
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'approved':
                $filter = 1;
                break;

            case 'pending':
                $filter = 0;
                break;
            case 'reject':
                $filter = -1;
                break;
            default:
                $filter = null;
                break;
        }

        return $filter;
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'news_feed')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $filter = $this->_getFilter();

        if ($filter !== null) {
            $conditions['status'] = $this->_getFilter();
        }

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        if ($this->hasPermission('manage_all', 'salespersons')) {

        } else if ($this->hasPermission('manage_staff', 'salespersons')) {
            $salespersons = $this->db->select('armstrong_2_salespersons_id')
                ->from('salespersons')
                ->where('salespersons_manager_id', $user['id'])
                ->get()
                ->result_array();

            $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
            $conditions = array_merge($conditions, array('armstrong_2_salespersons_id IN ' . array_to_string(array_keys($salespersons)) => NULL));
        } else {
            $conditions = array_merge($conditions, array('armstrong_2_salespersons_id' => $user['id']));
        }

        $news_feed = $this->news_feed_m->get_limit($this->getPageLimit(), $conditions, 'last_updated desc');

        $this->data['total'] = $this->news_feed_m->count('where', $conditions);
        $this->data['filter'] = $this->input->get('filter');
        $this->data['news_feed'] = $news_feed;
        // $this->data['total'] = $this->customers_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('News Feed');
        return $this->render('ami/news_feed/index', $this->data);
    }

    public function pending($id)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        $this->_assertId($id, 'ami/news_feed');

        if ($id) {
            $news_feedTable = $this->news_feed_m->getTableName();

            $news_feed = $this->db->select("{$news_feedTable}.*")
                ->from($news_feedTable)
                ->where("{$news_feedTable}.id", $id)
                ->get()
                ->row_array();
            $news_feed['newsfeed_type'] = $news_feed['newsfeed_type'] == 0 ? 'Local' : 'Global';

            $this->data['news_feed'] = $news_feed;
            $this->assertCountry($this->data['news_feed']);

            if ($this->input->is_ajax_request()) {
                $this->data['amendedData'] = array();

                $this->data['news_feed'] = array_except($this->data['news_feed'], array('title', 'status', 'is_draft', 'log_ids', 'reason', 'country_id'));

                return $this->render('ami/news_feed/pending_action');
            }
        }

        return redirect('ami/news_feed?filter=pending');
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'news_feed')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['news_feed'] = $this->news_feed_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->news_feed_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('News_feed');

        return $this->render('ami/news_feed/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions, true)
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'news_feed')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        //$this->data['customer'] = array('approved' => 1);
        return $this->render('ami/news_feed/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'news_feed') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->data += $this->_getAddEditParams();
        $this->_assertId($id, 'ami/news_feed');

        if ($id = intval($id)) {
            $this->data['news_feed'] = $this->news_feed_m->get($id);
            $this->assertCountry($this->data['news_feed']);
            $this->data['images'] = $this->media_m->getMedia('image', 'news_feed', $this->country_id, $id);
            $this->data['videos'] = $this->media_m->getMedia('video', 'news_feed', $this->country_id, $id);
            $this->data['page_title'] = page_title('Edit News Feed');
            $this->data['filter'] = $this->input->get('filter');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'news_feed')) {
                    return $this->noPermission();
                }

                return $this->render('ami/news_feed/preview', $this->data);
            } else {
                return $this->render('ami/news_feed/edit', $this->data);
            }
        }

        return redirect('ami/news_feed');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'news_feed')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->news_feed_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
            }
        }
        return redirect('ami/news_feed');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'news_feed')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news_feed');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/news_feed';
                //$this->news_feed_m->delete($id);
                $this->news_feed_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/news_feed/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from News Feed"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/news_feed');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'news_feed')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news_feed');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/news_feed/draft';
                $this->news_feed_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/news_feed/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from News Feed"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/news_feed');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'news_feed') && !$this->hasPermission('edit', 'news_feed')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->news_feed_m->array_from_post(array('armstrong_2_salespersons_id', 'content', 'status', 'newsfeed_type', 'id'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;
        if ($id == null) {
            if (!$this->hasPermission('manage_staff', 'salespersons') && !$this->hasPermission('manage_all', 'salespersons')) {
                $country = $this->country_m->get($this->country_id);
                //$approval_news_feed   0:local, 1:global, 2:both, 3:none
                $approval_news_feed = $country['approval_news_feed'];
                switch ($approval_news_feed) {
                    case 0:
                        $data['status'] = $data['newsfeed_type'] == 0 ? 0 : 1;
                        break;
                    case 1:
                        $data['status'] = $data['newsfeed_type'] == 1 ? 0 : 1;
                        break;
                    case 2:
                        $data['status'] = 0;
                        break;
                    case 3:
                        $data['status'] = 1;
                        break;
                    default:
                        $data['status'] = 0;
                }
            }
            $this->add_new($data);
        }

        $newId = $this->news_feed_m->save($data, $id);

        // Update existing record
        if ($id) {
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('news_feed', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');

        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'news_feed',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'news_feed';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);
                }
            }
        }

        return redirect('ami/news_feed?filter=' . $this->input->post('filter'));
    }

    public function add_new($data)
    {
        $parse_key = config_item('parse_key');

        $this->load->library('parse-php-sdk/src/Parse/ParseClient');
        $this->load->library('parse-php-sdk/src/Parse/ParseObject');
        $this->load->library('parse-php-sdk/src/Parse/ParsePush');

        $this->parseclient->initialize(
            $parse_key['app_id']['pull'],
            $parse_key['rest_key']['pull'],
            $parse_key['master_key']['pull']
        );
        // save data
        /*$db = new ParseObject("test");

        $db->set("content", json_encode($data));

        @$db->save();
        */
        // push notification
        @ParsePush::send(array(
            "channels" => ["PULL"],
            "data" => json_encode($data)
        ));
        //try {
        //    $db->save();
        //    echo 'New object created with objectId: ' . $db->getObjectId();
        //} catch (ParseException $ex) {
        //    echo 'Failed to create new object, with error message: ' + $ex->getMessage();
        //}
    }

    public function approve($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news_feed');

        if ($id) {
            $this->data['news_feed'] = $this->news_feed_m->get($id);
            $this->assertCountry($this->data['news_feed']);
            $this->data['page_title'] = page_title('Approve News Feed');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                //$this->data['amendedData'] = $this->_getAmendedData($this->data['news_feed']['id']);

                return $this->render('ami/news_feed/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $this->news_feed_m->save(array(
                    'status' => 1
                ), $id);
            }
        }

        return redirect('ami/news_feed?filter=pending');
    }

    public function reject($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/news_feed');

        if ($id) {
            $this->data['news_feed'] = $this->news_feed_m->get($id);
            $this->assertCountry($this->data['news_feed']);
            $this->data['page_title'] = page_title('Reject News Feed');

            if ($this->input->is_ajax_request()) {
                //$this->data['amendedData'] = $this->_getAmendedData($this->data['news_feed']['id']);

                return $this->render('ami/news_feed/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $data_update = array(
                    'status' => -1
                );
                if ($this->hasPermission('manage_all', 'salespersons')) {
                    $data_update = array_merge(array('is_draft' => 1), $data_update);
                }
                $this->news_feed_m->save($data_update, $id);
            }
        }

        return redirect('ami/news_feed?filter=pending');
    }
}