<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Eloquent\Tfo;

class Potential_Customers extends AMI_Controller
{
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('potential_customers_m');
        $this->load->model('customers_m');
        $this->load->model('sub_channels_m');
        $this->load->model('customers_types_m');
        $this->load->model('groups_m');
        $this->load->model('business_types_m');
        $this->load->model('country_channels_m');
        $this->load->model('country_sub_channels_m');
        $this->load->model('distributors_m');
        $this->load->model('wholesalers_m');
        $this->load->model('salespersons_m');
        $this->load->model('contacts_m');
        $this->load->model('customers_amended_m');
        $this->load->model('chains_m');
        $this->load->model('global_channels_m');
        $this->load->library('CountryParser');
    }


    public function index()
    {
        if (!$this->hasPermission('view', 'customer')) {
            return $this->noPermission();
        }

        $this->data['country_id'] = $this->country_id;
        return $this->render('ami/potential_customers/index');
    }

    public function ajaxData()
    {
        $user = $this->session->userdata('user_data');

        $conditions = array(
            'c.country_id' => $this->country_id,
            'c.is_draft' => 0,
            'c.approved != -2' => null,
            'c.synced_customer' => 0
        );
        if (!$this->isSuperAdmin()) {
            if ($this->hasPermission('manage_staff', 'salespersons')) {
                $type = $this->db->select(array('type', 'sub_type'))
                    ->from('salespersons')
                    ->where('armstrong_2_salespersons_id', $user['id'])
                    ->get()
                    ->row();

                if (isset($type->sub_type) && $type->sub_type == 'asm') {
                    $salespersons = $this->db->select('armstrong_2_salespersons_id')
                        ->from('salespersons')
                        ->where('salespersons_manager_id', $user['id'])
                        ->get()
                        ->result_array();

                    $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
                    $salespersons_new = implode("\",\"", array_keys($salespersons));

                    if ($salespersons_new) {

                        //                $conditions = array_merge(array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id IN ("'.$salespersons_new.'")' => null ), $filter);
                        $conditions['c.armstrong_2_salespersons_id IN ("' . $salespersons_new . '")'] = null;
                    }
                } else {
                    $conditions["c.armstrong_2_salespersons_id ='" . $user['id'] . "'"] = null;
                }
            }
        }

        $datatables = new Datatable(array('model' => 'Potential_customers_dt'));
        $this->potential_customers_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['c']['id'];

            $_data['c']['active'] = $_data['c']['active'] == 1 ? 'Yes' : 'No';

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'potential_customers')) {
                $_data['buttons'] .= html_btn(site_url('ami/potential_customers/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'potential_customers')) {
                $_data['buttons'] .= html_btn(site_url('ami/potential_customers/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';

        }

        return $this->json($data);
    }

    protected function _getAddEditParams()
    {
        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->country_id == 6) {
            if ($user['type'] == 'SL' && $user['sub_type'] == 'asm') {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, "(salespersons_manager_id ='" . $user['id'] . "' OR (type like '%SL%' OR type like '%ADMIN%'))" => null);
            } else {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);
            }
        } else {
            $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, '(type like "%PULL%" OR type like "%MIX%" OR type like "%TRAINER%")' => null);
        }
        // get country ufs_share config
        $country_config = $this->db->select('application_config')
            ->from('country')
            ->where(array('id' => $this->country_id))
            ->get()->result_array();
        $application_config = json_decode($country_config[0]['application_config']);

        $conditions_new = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, 'approved' => 1);
        $cols = array('id', 'name');
        $hierachy = $this->countryparser->parse_hierachy($this->country_code);
        $root = $this->countryparser->parse($this->country_code)->get($hierachy['root']);

        $rootValues = array('' => '-Select-');
        foreach ($root as $key => $value) {
            $rootValues[$value] = $value;
        }
        $countries = array_rewrite($this->countries, 'id');
        $distributors = $this->distributors_m->getDistributorListOptions(null, $conditions_new);
        $wholesalers = $this->wholesalers_m->getSalerListOptions(null, $conditions_new);
        $chains = $this->chains_m->getChainListOption('- Select -', $conditions);
        $salespersons = $this->salespersons_m->getPersonListOptions('- Select -', $conditions_salespersons, false);

        $salespersons_str = substr(implode('","', array_keys($salespersons)), 2);
        $list_customers = [];
        if ($salespersons_str) {
            $customers_conditions = array(
                'armstrong_2_salespersons_id IN (' . $salespersons_str . '")' => null,
                "is_draft" => 0
            );
            //$customers = $this->customers_m->get_by($customers_conditions);
            $customers = array();
            foreach ($customers as $key => $_customer) {
                $list_customers[$_customer['armstrong_2_salespersons_id']][$_customer['armstrong_2_customers_id']] = $_customer['armstrong_2_customers_id'] . ' - ' . $_customer['armstrong_2_customers_name'];
            }
        }

        return array(
            'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            'sub_channels' => $this->country_sub_channels_m->get_by($conditions),
            'customers_types' => $this->customers_types_m->parse_form($cols, '- Select -', array(
                "FIND_IN_SET('{$this->country_id}', country_id) != " => 0,
                'is_draft' => 0
            )),
            // 'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            'country_channels' => $this->country_channels_m->get_by($conditions),
            'global_channels' => $this->global_channels_m->parse_form(array('global_channels_id', 'name', 'id'), '- Select -', array('is_draft' => 0)),
            'channel_groups' => $this->groups_m->parse_form($cols, '- Select -', array_merge($conditions, array('type' => 3))),
            'cuisine_groups' => $this->groups_m->parse_form($cols, '- Select -', array_merge($conditions, array('type' => 4))),
            'countryHierachy' => $hierachy,
            'rootValues' => $rootValues,
            'country' => $countries[$this->country_id],
            'chains' => $chains,
            'suppliers' => array_merge(array('' => '- No Supplier -'), $distributors, $wholesalers),
            'salespersons' => $salespersons,
            'customers' => $list_customers,
            'otm_ufs_share_config' => isset($application_config->pull->otm_ufs_share_config) ? $application_config->pull->otm_ufs_share_config : 0
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'potential_customers')) {
            return $this->noPermission();
        }
        $user = $this->session->userdata('user_data');
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        // Calculate real_otm
        $formula = Capsule::table('formula')->leftJoin('country', 'formula.id', '=', 'country.real_otm')
            ->where('country.id', '=', $this->country_id)
            ->where('formula.is_draft', '=', 0)
            ->first();
        $real_otm = 'D';
        if(is_object($formula))
        {
            $formula = objectToArray($formula);
        }
        if (isset($formula['formula'])) {
            $now = Carbon::now();
            $formulaRef = Capsule::table('formula_ref')->whereIn('id', explode('*', $formula['formula']))
                ->where('date_start', '<=', $now->toDateString())
                ->where('date_end', '>=', $now->toDateString())
                ->first();
            if(is_object($formulaRef))
            {
                $formulaRef = objectToArray($formulaRef);
            }
            if (isset($formulaRef['date_start'], $formulaRef['date_end'])) {
                $start = Carbon::parse($formulaRef['date_start'])->startOfDay();
                $end = Carbon::parse($formulaRef['date_end'])->endOfDay();
                $tfo = Tfo::filter($this->country_id)->whereBetween('date_created', [$start->toDateTimeString(), $end->toDateTimeString()])->get();

                $total = 0;

                foreach ($tfo as $_tfo) {
                    $total += $_tfo->total;
                }

                $bandwidth = Capsule::table('otm_bandwidth')
                    ->where('country_id', '=', $this->country_id)
                    ->where('is_draft', '=', 0)
                    ->where('range_from', '<=', $total)
                    ->where('range_to', '>=', $total)
                    ->first();

                if (isset($bandwidth['class'])) {
                    $real_otm = $bandwidth['class'];
                }
            }
        }

        $otm = "D";
        $this->data['customer'] = array(
            'approved' => 1,
            'real_otm' => strtoupper($real_otm),
            'otm' => strtoupper($otm)
        );

        return $this->render('ami/potential_customers/edit', $this->data);
    }

    public function checkid($id = null){
//        'c.is_draft' => 0,
//            'c.approved != -2' => null,
//            'c.synced_customer' => 0
        if($id)
        {
            $pc = $this->potential_customers_m->get_by(array('is_draft' => 0, 'country_id' => $this->country_id, 'id != ' => $id, 'armstrong_1_customers_id' => $_POST['armstrong_1_customers_id']));
        }
        else
        {
            $pc = $this->potential_customers_m->get_by(array('is_draft' => 0, 'country_id' => $this->country_id, 'armstrong_1_customers_id' => $_POST['armstrong_1_customers_id']));
        }
        if($pc)
        {
            echo 'no';
        }
        else
        {
            echo 'yes';
        }
    }
    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'potential_customers') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/potential_customers');

        if ($id) {
            $this->data['customer'] = $this->potential_customers_m->get($id);
            $this->assertCountry($this->data['customer']);
            $openingHours = explode(' - ', $this->data['customer']['opening_hours']);
            $this->data['customer']['opening_hours_from'] = isset($openingHours[0]) ? $openingHours[0] : null;
            $this->data['customer']['opening_hours_to'] = isset($openingHours[1]) ? $openingHours[1] : null;

            $this->data['page_title'] = page_title('Edit Potential Customers');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'potential_customers')) {
                    return $this->noPermission();
                }
                if ($this->country_id == 6) {
                    $salesperson = $this->salespersons_m->get_by(array(
                        'armstrong_2_salespersons_id' => $this->data['customer']['armstrong_2_salespersons_id']
                    ));
                    if ($salesperson) {
                        $this->data['customer']['salesperson_name'] = $salesperson[0]['first_name'] . ' ' . $salesperson[0]['last_name'];
                    }
                }
                return $this->render('ami/potential_customers/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();

                $countryValues = array();

                if (count($this->data['countryHierachy']) > 0) {
                    foreach ($this->data['countryHierachy'] as $key => $value) {
                        if (!empty($this->data['countryHierachy'][$key])) {
                            if ($key == 'root') {
                                $countryValues[$value] = array_merge(array('' => '-Select-'), $this->countryparser->parse($this->country_code)->get($value));
                            } else {
                                $countryValues[$value] = (!empty($this->data['customer'][$key]) && $this->countryparser->getBy($value, $this->data['customer'][$key]))
                                    ? $this->countryparser->getBy($value, $this->data['customer'][$key])
                                    : array();
                            }
                        } else {
                            $countryValues[$value] = array();
                        }
                    }
                }

                $this->data['countryValues'] = $countryValues;
                $this->data['filter'] = $this->input->get('filter');

                $this->data['country_id'] = $this->country_id;
//                dd($this->data);
                return $this->render('ami/potential_customers/edit', $this->data);
            }
        }

        return redirect('ami/customers');
    }

    protected function _getAmendedData($customerId)
    {
        $data = $this->db->where(array(
            'country_id' => $this->country_id,
            'armstrong_2_customers_id' => $customerId,
            'is_draft' => 0
        ))
            ->get('customers_amended_data')->row_array();

        if ($data) {
            $data['old_data'] = !empty($data['customer_old_data']) ? json_decode($data['customer_old_data'], true) : array();
            $data['new_data'] = !empty($data['customer_new_data']) ? json_decode($data['customer_new_data'], true) : array();

            $data['fields'] = $data['old_data'] ? array_keys($data['old_data']) : array();
        }

        $foreignKeys = array(
            'business_type' => 'business_types',
            'sub_channel' => 'country_sub_channels',
            'channel' => 'country_channels',
            'customers_types_id' => 'customers_types',
            'channel_groups_id' => 'groups',
            'cuisine_groups_id' => 'groups'
        );

        foreach ($foreignKeys as $field => $table) {
            if (!empty($data['old_data'][$field])) {
                $oldContent = $this->_getRelationshipAmendedData($table, 'id', $data['old_data'][$field]);

                if (!empty($oldContent['name'])) {
                    $data['old_data'][$field] .= " - {$oldContent['name']}";
                }
            }

            if (!empty($data['new_data'][$field])) {
                $newContent = $this->_getRelationshipAmendedData($table, 'id', $data['new_data'][$field]);

                if (!empty($newContent['name'])) {
                    $data['new_data'][$field] .= " - {$newContent['name']}";
                }
            }
        }

        if (!empty($data['fields'])) {
            foreach ($data['fields'] as $dataField) {
                if (!in_array($dataField, $foreignKeys)) {
                    switch ($dataField) {
                        case 'armstrong_2_salespersons_id':
                            if (!empty($data['old_data']['armstrong_2_salespersons_id'])) {
                                $oldContent = $this->_getRelationshipAmendedData('salespersons', 'id', $data['old_data']['armstrong_2_salespersons_id']);

                                if (isset($oldContent['first_name']) && isset($oldContent['last_name'])) {
                                    $data['old_data']['armstrong_2_salespersons_id'] = "{$oldContent['first_name']} {$oldContent['last_name']}";
                                }
                            }

                            if (!empty($data['new_data']['armstrong_2_salespersons_id'])) {
                                $newContent = $this->_getRelationshipAmendedData('salespersons', 'id', $data['new_data']['armstrong_2_salespersons_id']);

                                if (!empty($newContent['name'])) {
                                    $data['new_data']['armstrong_2_salespersons_id'] = "{$newContent['first_name']} {$newContent['last_name']}";
                                }
                            }
                            break;

                        case 'primary_supplier':
                            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
                            $distributors = $this->distributors_m->getDistributorListOptions(null, $conditions);
                            $wholesalers = $this->wholesalers_m->getSalerListOptions(null, $conditions);
                            $suppliers = array_merge($distributors, $wholesalers);

                            if (!empty($data['old_data']['primary_supplier'])) {
                                if (isset($suppliers[$data['old_data']['primary_supplier']])) {
                                    $data['old_data']['primary_supplier'] = "{$suppliers[$data['old_data']['primary_supplier']]}";
                                }
                            }

                            if (!empty($data['new_data']['primary_supplier'])) {
                                if (isset($suppliers[$data['new_data']['primary_supplier']])) {
                                    $data['new_data']['primary_supplier'] = "{$suppliers[$data['new_data']['primary_supplier']]}";
                                }
                            }
                            break;

                        case 'secondary_supplier':
                            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
                            $distributors = $this->distributors_m->getDistributorListOptions(null, $conditions);
                            $wholesalers = $this->wholesalers_m->getSalerListOptions(null, $conditions);
                            $suppliers = array_merge($distributors, $wholesalers);

                            if (!empty($data['old_data']['secondary_supplier'])) {
                                if (isset($suppliers[$data['old_data']['secondary_supplier']])) {
                                    $data['old_data']['secondary_supplier'] .= " - {$suppliers[$data['old_data']['secondary_supplier']]}";
                                }
                            }

                            if (!empty($data['new_data']['secondary_supplier'])) {
                                if (isset($suppliers[$data['new_data']['secondary_supplier']])) {
                                    $data['new_data']['secondary_supplier'] .= " - {$suppliers[$data['new_data']['secondary_supplier']]}";
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        return $data;
    }

    protected function _getRelationshipAmendedData($table, $key, $value)
    {
        return $this->db->from($table)
            ->where($key, $value)
            ->get()->row_array();
    }

    public function pending($id, $app)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        $this->_assertId($id, 'ami/customers');

        if ($id) {
            // $this->data['customer'] = $this->customers_m->get($id);

            $customerTable = $this->customers_m->getTableName();

            $customer = $this->db->select("{$customerTable}.*,
				cuisine_channels.name as cuisine_channels_name,
				country_channels.name as country_channels_name,
				country_sub_channels.name as sub_channel_name,
				groups.name as cuisine_groups_name,
				serving_types.name as serving_types_name,
				customers_types.name as customers_types_name,
				global_channels.name as global_channels_name,
				business_types.name as business_type_name,
				salespersons.first_name, salespersons.last_name")
                ->from($customerTable)
                ->join('cuisine_channels', "{$customerTable}.cuisine_channels_id = cuisine_channels.cuisine_id AND cuisine_channels.country_id = {$this->country_id}", 'left')
                ->join('country_channels', "{$customerTable}.channel = country_channels.id", 'left')
                ->join('country_sub_channels', "{$customerTable}.sub_channel = country_sub_channels.id", 'left')
                ->join('groups', "{$customerTable}.cuisine_groups_id = groups.id", 'left')
                ->join('serving_types', "{$customerTable}.serving_types_id = serving_types.id", 'left')
                ->join('customers_types', "{$customerTable}.customers_types_id = customers_types.id", 'left')
                ->join('global_channels', "country_channels.global_channels_id = global_channels.id", 'left')
                ->join('business_types', "{$customerTable}.business_type = business_types.id", 'left')
                ->join('salespersons', "{$customerTable}.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id", 'left')
                ->where("{$customerTable}.armstrong_2_customers_id", $id)
                ->get()
                ->row_array();

//            $customer['cuisine_channels_id'] = $customer['cuisine_channels_name'];
            $customer['cuisine_name'] = $customer['cuisine_channels_name'];
            $customer['channel'] = $customer['country_channels_name'];
            $customer['sub_channel'] = $customer['sub_channel_name'];
            $customer['cuisine_groups_id'] = $customer['cuisine_groups_name'];
//            $customer['serving_types_id'] = $customer['serving_types_name'];
            $customer['serving_name'] = $customer['serving_types_name'];
            $customer['customers_types_id'] = $customer['customers_types_name'];
            $customer['global_channels_id'] = $customer['global_channels_name'];
            $customer['business_type'] = $customer['business_type_name'];
            if ($this->country_id == 6) {
                $customer['salesperson_name'] = $customer['first_name'] . ' ' . $customer['last_name'];
                if ($customer['primary_supplier']) {
                    $customer['primary_supplier_name'] = $this->getSupplierName($customer['primary_supplier']);
                }
                if ($customer['secondary_supplier']) {
                    $customer['secondary_supplier_name'] = $this->getSupplierName($customer['secondary_supplier']);
                }
            }

            unset($customer['first_name']);
            unset($customer['last_name']);

            $this->data['customer'] = $customer;
            $this->data['app'] = $app;

            $this->assertCountry($this->data['customer']);
            if ($this->input->is_ajax_request()) {
                $this->data['amendedData'] = array();
                if ($this->data['customer']['approved'] == 2) {

                    $this->data['amendedData'] = $this->_getAmendedData($this->data['customer']['armstrong_2_customers_id']);
                }

                $this->data['customer'] = array_except($this->data['customer'], array('ssd_id_2', 'cuisine_channels_name',
                    'country_channels_name', 'sub_channel_name', 'cuisine_groups_name', 'serving_types_name',
                    'customers_types_name', 'global_channels_name', 'business_type_name', 'kosher_type'));
                return $this->render('ami/customers/pending_action');
            }
        }

        return redirect('ami/customers?filter=pending');
    }

    public function approve($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/customers');

        if ($id) {
            $this->data['customer'] = $this->customers_m->get($id);
            $this->assertCountry($this->data['customer']);
            $this->data['page_title'] = page_title('Approve Customers');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                if ($this->data['customer']['approved'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['customer']['armstrong_2_customers_id']);
                }

                return $this->render('ami/customers/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $dataSave = array();
                if ($this->data['customer']['approved'] == -2) {
                    $dataSave = array(
                        'approved' => -2,
                        'active' => 0
                    );
                } else {
                    $dataSave = array(
                        'approved' => 1,
                        'active' => 1
                    );
                }
                $this->customers_m->save($dataSave, $id);
            }
        }

        // Send mail
        if ($this->data['customer']['approved'] == 0 || $this->data['customer']['approved'] == 2) {
            $this->mail->from('No-Reply-Orders@ufs-app.com', "UFS " . ($this->country_code));
            $salesPersonID = $this->data['customer']['armstrong_2_salespersons_id'];
            $salesPersonEmail = $this->salespersons_m->getEmail($salesPersonID);
            $to = ($salesPersonEmail['email']) ? $salesPersonEmail['email'] : '';
            $cc = $this->country_id == 6 ? 'joey.wang@unilever.com' : '';
            $bcc = 'conghc.dev@gmail.com';
            if ($this->country_id == 6) {
                if ($this->data['customer']['approved'] == 0) { //TODO approved new customer
                    $subject = '新客戶已核准 - ' . date('Y-m-d H:i:s');
                    $body = '以下客戶已經被您的主管核准, <br/>';
                    $body .= '1. Name - ' . $this->data['customer']['armstrong_2_customers_name'] . '<br/>';
                    $body .= '2. ID - ' . $this->data['customer']['armstrong_2_customers_id'] . '<br/><br/>';
                    $body .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態 <br/>';
                    $body .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除 <br/>';
                    $body .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態 <br/>';
                    $body .= '3.在Customers功能中的該客戶狀態(Active),會從紅色變為橘色,就能操作其他相關功能(打單,route plan等) <br/>';
                } else { // TODO approved existed customer
                    $subject = '舊客戶已核准 - ' . date('Y-m-d H:i:s');
                    $body = '以下客戶已經被您的主管核准, <br/>';
                    $body .= '1. Name - ' . $this->data['customer']['armstrong_2_customers_name'] . '<br/>';
                    $body .= '2. ID - ' . $this->data['customer']['armstrong_2_customers_id'] . '<br/><br/>';
                    $body .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態 <br/>';
                    $body .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除 <br/>';
                    $body .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態 <br/>';
                    $body .= '3.在Customers功能中的該客戶狀態(Active),會從紅色變為橘色,就能操作其他相關功能(打單,route plan等) <br/>';
                }

            } else {
                $subject = 'Approval Customers - ' . date('Y-m-d H:i:s');
                $body = 'Your Customer ' . $this->data['customer']['armstrong_2_customers_id'] . ' has been approved by your Sales Leader.<br/> Please sync your iPad to see the latest changes. These are the steps needed to sync your iPad<br/><br/>';
                $body .= '1. Tap on the Sync Icon <br/>';
                $body .= '2. Tap on the Module Tab <br/>';
                $body .= '3. Tap on "Customers" Icon <br/>';
            }


            $this->mail->sendMail($to, $subject, $body, $cc, $bcc);
        }
        return redirect('ami/customers?filter=pending');
    }

    public function reject($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/customers');

        if ($id) {
            $this->data['customer'] = $this->customers_m->get($id);
            $this->assertCountry($this->data['customer']);
            $this->data['page_title'] = page_title('Reject Customers');

            if ($this->input->is_ajax_request()) {
                if ($this->data['customer']['approve'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['customer']['armstrong_2_customers_id']);
                }

                return $this->render('ami/customers/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $old_data = array();
                if ($this->data['customer']['approved'] == 2) {
                    $amendata = $this->_getAmendedData($this->data['customer']['armstrong_2_customers_id']);
                    $old_data = (array)json_decode($amendata['customer_old_data']);
                }

                if ($this->data['customer']['approved'] == -2 || $this->data['customer']['approved'] == 2) {
                    $data_update = array(
                        'approved' => 1,
                        'active' => 1,
                        'is_draft' => 0
                    );
                } elseif ($this->data['customer']['approved'] == 0) {
                    $data_update = array(
                        'approved' => -1,
                        'active' => 0,
                        'is_draft' => 0
                    );
                } else {
                    $data_update = array(
                        'approved' => -1,
                        'active' => 1
                    );
                }


                if ($this->hasPermission('manage_all', 'salespersons')) {
                    $data_update = array_merge(array('is_draft' => 1), $data_update);
                }
                $data_update = array_merge($data_update, $old_data);

                $this->customers_m->save($data_update, $id);

                $pendingEdit = $this->db->from($this->customers_amended_m->getTableName())
                    ->where('armstrong_2_customers_id', $id)
                    ->get()->row_array();

                if ($pendingEdit) {
                    $this->db->where(array('id' => $pendingEdit['id']))
                        ->update($this->customers_amended_m->getTableName(), array(
                            'customer_old_data' => $pendingEdit['customer_new_data'],
                            'customer_new_data' => $pendingEdit['customer_old_data'],
                        ));
                }
            }
        }

        // Send mail
        if ($this->data['customer']['approved'] == 0 || $this->data['customer']['approved'] == 2) {
            if ($this->country_id == 6) {
                if ($this->data['customer']['approved'] == 0) { // TODO reject new customer
                    $subject = '新客戶已退件 - ' . date('Y-m-d H:i:s');
                    $body = '以下客戶已經被您的主管退件, <br/>';
                    $body .= '1. Name - ' . $this->data['customer']['armstrong_2_customers_name'] . '<br/>';
                    $body .= '2. ID - ' . $this->data['customer']['armstrong_2_customers_id'] . '<br/><br/>';
                    $body .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態 <br/>';
                    $body .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除 <br/>';
                    $body .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態 <br/>';
                    $body .= '3.在Customers功能中,該客戶就不會出現在客戶清單 <br/>';
                } elseif ($this->data['customer']['approved'] == 2) { // TODO reject existed customer
                    $subject = '舊客戶已退件 - ' . date('Y-m-d H:i:s');
                    $body = '以下客戶已經被您的主管退件,<br/>';
                    $body .= '1. Name - ' . $this->data['customer']['armstrong_2_customers_name'] . '<br/>';
                    $body .= '2. ID - ' . $this->data['customer']['armstrong_2_customers_id'] . '<br/><br/>';
                    $body .= '請登入APP並操作以下步驟,才能看到該客戶的最新狀態 <br/>';
                    $body .= '1.請先將所有未上傳的資料(Calls, orders, e-mail與module)先做Sync(上傳到AMI),以避免未上傳資料被刪除 <br/>';
                    $body .= '2.再到Sync功能中的Module功能,點Customers,下載客戶的最新狀態 <br/>';
                    $body .= '3. 在Customers功能中的該客戶狀態(Active),會從紅色變為橘色,但是客戶資料還是原來舊的資料 <br/>';
                }
            } else {
                if ($this->data['customer']['approved'] == 0) {
                    $subject = 'Reject Customer - ' . date('Y-m-d H:i:s');
                    $body = 'Your Customer ' . $this->data['customer']['armstrong_2_customers_id'] . ' has been rejected by your Sales Leader.<br/> Please contact your Sales Leader on the reason for rejection.';
                } elseif ($this->data['customer']['approved'] == 2) {
                    $subject = 'Reject Existing Customer - ' . date('Y-m-d H:i:s');
                    $body = 'Your Customer ' . $this->data['customer']['armstrong_2_customers_id'] . ' has been rejected by your Sales Leader.<br/> Please contact your Sales Leader on the reason for rejection.<br/>';
                    $body .= 'These changes have been made to an existing customers. Please follow these steps to edit the customer again. <br/><br/>';
                    $body .= '1. Tap on the Sync Icon <br/>';
                    $body .= '2. Tap on the Module Tab <br/>';
                    $body .= '3. Tap on "Customers" Icon <br/>';
                }
            }
            $this->mail->from('No-Reply-Orders@ufs-app.com', "UFS " . strtoupper($this->country_code));
            $salesPersonID = $this->data['customer']['armstrong_2_salespersons_id'];
            $salesPersonEmail = $this->salespersons_m->getEmail($salesPersonID);
            $to = ($salesPersonEmail['email']) ? $salesPersonEmail['email'] : '';
            $cc = $this->country_id == 6 ? 'joey.wang@unilever.com' : '';
            $bcc = 'conghc.dev@gmail.com';
            $this->mail->sendMail($to, $subject, $body, $cc, $bcc);
        }

        return redirect('ami/customers?filter=pending');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'customer')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->customers_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
            }
        }

        return redirect('ami/customers');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'potential_customers')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/potential_customers');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/potential_customers';
                //$this->customers_m->delete($id);
                $this->potential_customers_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/potential_customers/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Potential Customers"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/potential_customers');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'customer')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/customers');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/customers/draft';
                $this->customers_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/customers/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Customers"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/customers');
    }

    public function save()
    {
        if (!$this->hasPermission('edit', 'potential_customers') && !$this->hasPermission('add', 'potential_customers')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $fields = array('id', 'armstrong_2_customers_name', 'armstrong_2_salespersons_id', 'armstrong_2_chains_id', 'meals_per_day_new', 'selling_price_per_meal_new', 'weeks_per_year_new', 'days_per_week_new', 'food_turnover_new', 'assumption_food_cost_new', 'assumption_ufs_share_new', 'convenience_factor_new', 'food_purchase_value_new', 'ufs_share_food_cost_new', 'otm_potential_value_new', 'otm_new',
            'sap_cust_name', 'sap_cust_name_2', 'street_address', 'city', 'state', 'postal_code', 'phone', 'phone_two', 'email', 'email_two', 'global_channels_id', 'armstrong_2_secondary_salespersons_id',
            'suburb', 'region', 'district', 'fax', 'update_info', 'active', 'web_url', 'channel_weightage', 'channel', 'sub_channel', 'otm', 'real_otm',
            'key_acct', 'business_type', 'kosher_type', 'primary_supplier', 'secondary_supplier', 'trading_days_per_yr', 'trading_wks_per_yr',
            'turnover', 'capacity', 'no_of_outlets', 'convenience_lvl', 'meals_per_day', 'selling_price', 'budget_price', 'food_cost', 'acct_opened',
            'best_time_to_call', 'notes', 'latitude', 'longitude', 'display_name', 'registered_name', 'opening_hours', 'ufs_share', 'buy_frequency', 'territory',
            'customers_types_id', 'channel_groups_id', 'cuisine_groups_id', 'approved', 'approved_message', 'approved_compare', 'armstrong_2_customers_id', 'ssd_id', 'ssd_id_2', 'active', 'armstrong_1_customers_id', 'sap_id');

        $data = $this->potential_customers_m->array_from_post($fields);

        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;

        $data['opening_hours'] = ($data['opening_hours'][0] && $data['opening_hours'][1]) ? implode($data['opening_hours'], ' - ') : null;
        $data['channel'] = $data['channel'] ?: 0;
        $data['approved'] = 1;
        $data['otm'] = $data['otm_new'];

        // Calculate real_otm
        $formula_real_otm = Capsule::table('formula')->leftJoin('country', 'formula.id', '=', 'country.real_otm')
            ->where('country.id', '=', $this->country_id)
            ->where('formula.is_draft', '=', 0)
            ->first();
        $formula_real_otm = $this->convertToArray($formula_real_otm);
        if (isset($formula_real_otm['formula'])) {
            $now = Carbon::now();
            $formulaRef = Capsule::table('formula_ref')->whereIn('id', explode('*', $formula_real_otm['formula']))
                ->where('date_start', '<=', $now->toDateString())
                ->where('date_end', '>=', $now->toDateString())
                ->first();
            $formulaRef = $this->convertToArray($formulaRef);
            if (isset($formulaRef['date_start'], $formulaRef['date_end'])) {
                $start = Carbon::parse($formulaRef['date_start'])->startOfDay();
                $end = Carbon::parse($formulaRef['date_end'])->endOfDay();
                $tfo = Tfo::filter($this->country_id)->where('armstrong_2_salespersons_id', '=', $data['armstrong_2_salespersons_id'])->whereBetween('date_created', [$start->toDateTimeString(), $end->toDateTimeString()])->get();

                $total = 0;
                if ($tfo) {
                    foreach ($tfo as $_tfo) {
                        $total += $_tfo->total;
                    }
                }
                $bandwidth = Capsule::table('otm_bandwidth')
                    ->where('country_id', '=', $this->country_id)
                    ->where('is_draft', '=', 0)
                    ->where('range_from', '<=', $total)
                    ->where('range_to', '>=', $total)
                    ->first();
                $bandwidth = $this->convertToArray($bandwidth);
                if (isset($bandwidth['class'])) {
                    $data['real_otm'] = $bandwidth['class'];
                }
            }
        } // end calculate real_otm

        foreach ($data as $key => $_data) {
            if (is_string($_data))
                $data[$key] = strtoupper($_data);
        }

        $this->potential_customers_m->save($data, $id);

        $redirect = $this->input->post('filter') ? 'ami/potential_customers?filter=pending' : 'ami/potential_customers';

        return redirect($redirect);
    }

    public function country_channel()
    {
        $params = $this->input->post();
        $channel_convenience_lvl = $this->country_channels_m->getConvenience_lvl($params);
        return $this->json(array('convenience_lvl' => $channel_convenience_lvl));
    }

    public function country()
    {
        $params = $this->input->post();
        $hierachy_address = $this->countryparser->parse_hierachy($this->country_code);

        // foreach ($params as $key => $val) {
        //     if (!isset($hierachy_address[$key['key']]))
        //         return $this->json(array('msg' => 'empty'));

        //     $address['value'] = $this->countryparser->getBy($hierachy_address[$key], $val);
        // }

        if (!isset($hierachy_address[$params['key']])) {
            return $this->json(array('msg' => 'empty'));
        }

        $address['value'] = $this->countryparser->getBy($hierachy_address[$params['key']], $params['value']);

        $address['field'] = $hierachy_address[$params['key']];

        return $this->json($address);
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'customers')
        ) {
            return redirect(site_url('ami/customers'));
        }

        $customers = $this->customers_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($customers);

        return $this->excel($sheet, 'customers', $type);
    }

    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->potential_customers_m->importToDb($this->country_id, $worksheet->toArray(), array('id'), 'potential_customers');
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function contacts($id = null)
    {
        if (!$this->hasPermission('view', 'contacts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/customers');

        $this->data['contacts'] = $this->contacts_m->get_by(array(
            'type' => 0,
            'armstrong_2_customers_id' => $id,
            'country_id' => $this->country_id,
            'is_draft' => 0,
        ));

        $this->data['customers'] = $this->customers_m->get($id);

        return $this->render('ami/customers/contacts');
    }

//    tuantq 6/4/2015
//    public function change_salespersons($id = null)
//    {
//        if (!$this->hasPermission('view', 'contacts'))
//        {
//            return $this->noPermission();
//        }
//
//        $this->_assertId($id, 'ami/customers');
//
//        $this->data['contacts'] = $this->contacts_m->get_by(array(
//            'type' => 0,
//            'tag_id' => $id,
//            'country_id' => $this->country_id,
//            'is_draft' => 0,
//        ));
//
//        $this->data['customers'] = $this->customers_m->get($id);
//
//        return $this->render('ami/customers/change_salespersons');
//    }

    public function tokenfield()
    {
        $term = $this->input->get('term');
        $salespersons = $this->input->get('salespersons');

        if (!$term) {
            return $this->json(array());
        }

        $conditions = array("armstrong_2_customers_name LIKE '%{$term}%'", "armstrong_1_customers_id LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        $this->db->where('country_id', $this->country_id)
            ->where('approved', 1)
            ->where('is_draft', 0)
            ->where('active', 1)
            ->where($conditions);

        if ($salespersons) {
            $this->db->where('armstrong_2_salespersons_id', $salespersons);
        }

        $customers = $this->db->limit(30)
            ->get('potential_customers')
            ->result_array();
        $output = array();

        foreach ($customers as $customer) {
            $output[] = array(
                'value' => $customer['armstrong_1_customers_id'],
                'name' => $customer['armstrong_2_customers_name'],
                // 'label' => $salesperson['armstrong_2_salespersons_id'],
                'label' => $customer['armstrong_1_customers_id'] . ' - ' . $customer['armstrong_2_customers_name']
            );
        }

        return $this->json($output);
    }

    public function getOtmBandwidth()
    {
        $post = $this->input->post();
        $otmVal = isset($post['otm']) ? $post['otm'] : 0;

        $otm_bandwidth = $this->db->select(array('class', 'range_from', 'range_to'))
            ->from('otm_bandwidth')
            ->where(array('country_id' => $this->country_id))
            ->get()->result_array();
        $otmNew = 'N/A';
        foreach ($otm_bandwidth as $bandwidth) {
            if ($otmVal >= $bandwidth['range_from'] && $otmVal <= $bandwidth['range_to']) {
                $otmNew = strtoupper($bandwidth['class']);
            }
            if ($bandwidth['class'] == 'a' && $otmVal > $bandwidth['range_to']) {
                $otmNew = 'A';
            }
        }
        echo $otmNew;
        exit;
    }

    public function calculate_otm()
    {
        $customers = $this->db->from('customers')
            ->where(array(
                'country_id' => $this->country_id,
                'is_draft' => 0
            ))
            ->get()->result_array();
        if ($customers) {
            $otm_bandwidth = $this->db->select(array('class', 'range_from', 'range_to'))
                ->from('otm_bandwidth')
                ->where(array('country_id' => $this->country_id))
                ->get()->result_array();


            $datetime = date('Y-m-d H:i:s');
            foreach ($customers as $customer) {
                $otmNew = 'N/A';
                $meals_per_day_new = (float)$customer['meals_per_day_new'];
                $selling_price_per_meal_new = (float)$customer['selling_price_per_meal_new'];
                $weeks_per_year_new = (float)$customer['weeks_per_year_new'];
                $days_per_week_new = (float)$customer['days_per_week_new'];
                $assumption_food_cost_new = (float)$customer['assumption_food_cost_new'];
                $assumption_ufs_share_new = (float)$customer['assumption_ufs_share_new'];
                $convenience_factor_new = (float)$customer['convenience_factor_new'];

                $food_turnover_new = $meals_per_day_new * $selling_price_per_meal_new * $weeks_per_year_new * $days_per_week_new;
                $food_purchase_value_new = $food_turnover_new * $assumption_food_cost_new;
                $ufs_share_food_cost_new = $food_purchase_value_new * $assumption_ufs_share_new;
                $otm_potential_value_new = $ufs_share_food_cost_new * $convenience_factor_new;
                if ($otm_potential_value_new > 0) {
                    foreach ($otm_bandwidth as $bandwidth) {
                        if ($otm_potential_value_new >= $bandwidth['range_from'] && $otm_potential_value_new <= $bandwidth['range_to']) {
                            $otmNew = strtoupper($bandwidth['class']);
                        }
                        if ($bandwidth['class'] == 'a' && $otm_potential_value_new > $bandwidth['range_to']) {
                            $otmNew = 'A';
                        }
                    }
                }
                $query_update = "UPDATE customers SET otm_new = '{$otmNew}', otm = '{$otmNew}', otm_potential_value_new = {$otm_potential_value_new}, ufs_share_food_cost_new = {$ufs_share_food_cost_new}, food_purchase_value_new = {$food_purchase_value_new}, food_turnover_new = {$food_turnover_new}, last_updated = '{$datetime}' where armstrong_2_customers_id = '{$customer['armstrong_2_customers_id']}'";
                $this->db->query($query_update);
            }
            $this->data['message'] = 'OTM has been updated';

            return $this->render('ami/customers/index', $this->data);
        }
    }

    function getSupplierName($supplierID)
    {
        $table = $id_column = $name = '';
        if (preg_match("/whs/i", $supplierID)) {
            $table = 'wholesalers';
            $id_column = 'armstrong_2_wholesalers_id';
        } elseif (preg_match("/dis/i", $supplierID)) {
            $table = 'distributors';
            $id_column = 'armstrong_2_distributors_id';
        }
        $supplier = $this->db->select('name')
            ->from($table)
            ->where(array($id_column => $supplierID))
            ->get()
            ->result_array();
        if ($supplier) {
            $name = $supplier[0]['name'];
        }
        return $name;
    }

    function getCustomerBySalespersons()
    {
        $post = $this->input->post();
        $output = '';
        $condition = array(
            'country_id' => $this->country_id,
            'approved' => 1,
            'is_draft' => 0
        );
        $salespersons = (isset($post['armstrong_2_salespersons_id']) && $post['armstrong_2_salespersons_id']) ? $post['armstrong_2_salespersons_id'] : [];
        $channel = (isset($post['channel']) && $post['channel']) ? $post['channel'] : [];
        if ($salespersons) {
            $salespersons_str = implode("','", $salespersons);
            $condition = array_merge($condition, array("armstrong_2_salespersons_id IN ('{$salespersons_str}')" => null));
        }
        if ($channel) {
            $channel_str = implode(",", $channel);
            $condition = array_merge($condition, array("channel IN ({$channel_str})" => null));
        }

        $customer_list = $this->customers_m->getCustomerListOptions(null, $condition);
        if ($customer_list) {
            foreach ($customer_list as $id => $name) {
                $output .= "<option value='{$id}'>{$name}</option>";
            }
        }
        echo $output;
        exit;
    }
}
