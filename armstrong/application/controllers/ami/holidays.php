<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Holidays extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('holidays_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'holiday_settings')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['holidays'] = $this->holidays_m->get_by($conditions);
        return $this->render('ami/holidays/index', $this->data);
    }

    public function ajaxData()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Holidays_dt', 'rowIdCol' => $this->holidays_m->getTablePrimary()));

        $this->chains_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['armstrong_2_holiday_id'];
            $_data['armstrong_2_holiday_id'] = '
                <a href="' . site_url('ami/holidays/edit/' . $_data['armstrong_2_holiday_id']) . '" data-toggle="ajaxModal">
                    ' . $_data['armstrong_2_holiday_id'] . '
                </a>
            ';
            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'holiday_settings')) {
                $_data['buttons'] .= html_btn(site_url('ami/holidays/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'chains')) {
                $_data['buttons'] .= html_btn(site_url('ami/holidays/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'holiday_settings')) {
            return $this->noPermission();
        }

        if ($this->input->get('armstrong_2_holiday_id')) {
            $this->data['holidays']['armstrong_2_holiday_id'] = $this->input->get('armstrong_2_holiday_id');
        }

        return $this->render('ami/holidays/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'holiday_settings') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/holidays');
        if ($id) {

            $this->data['holidays'] = $this->holidays_m->get($id);
            $this->data['page_title'] = page_title('Edit holidays');

            return $this->render('ami/holidays/edit', $this->data);
        }

        return redirect('ami/holidays');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'holiday_settings') && !$this->hasPermission('edit', 'holiday_settings')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->holidays_m->array_from_post(array('armstrong_2_holiday_id', 'title', 'date', 'holiday'));

        $data['country_id'] = $this->country_id;
        $id = $data['armstrong_2_holiday_id'] ? $data['armstrong_2_holiday_id'] : null;
        if (!$id) {
            foreach ($data['holiday'] as $holiday) {
                $holiday['country_id'] = $this->country_id;
                $holiday['armstrong_2_holiday_id'] = ''; //$this->holidays_m->generateArmstrongId($this->country_id, 'holiday');
                $this->holidays_m->save($holiday, $id, true, "INSERT", $this->country_id);
            }
        } else {
            unset($data['holiday']);
            $this->holidays_m->save($data, $id);
        }

        return redirect('ami/holidays');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'holiday_settings')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->holidays_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/holidays');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'holiday_settings')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/holidays');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/holidays';
                //$this->chainss_m->delete($id);
                $this->holidays_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/holidays/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from holidays"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/holidays');
    }

    public function checkDuplicate()
    {
        if ($this->input->is_ajax_request()) {
            $params = $this->input->post();
            $holiday_id = $params['id'];
            $dates = $params['dates'];
            $output = [];
            if ($dates) {
                if (!$holiday_id) {
                    foreach ($dates as &$date) {
                        $date = $date . ' 00:00:00';
                    }
                    $date_str = implode('","', $dates);
                    $conditions = array(
                        'date IN ("' . $date_str . '")' => null,
                        'country_id' => $this->country_id
                    );
                } else {
                    $conditions = array(
                        'country_id' => $this->country_id,
                        'date' => $dates[0] . ' 00:00:00',
                        'armstrong_2_holiday_id != "' . $holiday_id . '"' => null
                    );
                }
                $check_existed = $this->holidays_m->get_by($conditions);
                if ($check_existed) {
                    $output = array(
                        'status' => 'error',
                        'ms' => 'Date input has been duplicate!'
                    );
                } else {
                    $output = array('status' => 'ok');
                }
            }
            echo json_encode($output);
            exit();
        }
    }
}