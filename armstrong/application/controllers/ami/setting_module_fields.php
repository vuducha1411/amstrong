<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_module_fields extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('modules_m');
        $this->load->model('module_fields_m');
        $this->load->model('module_field_countries_m');
    }

    public function index()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }
        return $this->render('ami/setting_module_fields/index', $this->data);
    }

    public function modules()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }
        $data = $this->modules_m->get();

        $this->data['data'] = $data;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Modules Settings');
        //$this->data += $this->_getAddEditParams();
        return $this->render('ami/setting_module_fields/modules', $this->data);
    }

    public function modules_process()
    {
        if (!$this->hasPermission('manage_all', 'salespersons')) {
            return $this->noPermission();
        }
        $this->is('POST');
        $params = $this->modules_m->array_from_post(array('id', 'name'));
        $id = $params['id'] ? $params['id'] : null;
        if ($id == null) {
            $check_exist = $this->modules_m->count('where', array('name LIKE' => $params['name']));
            if ($check_exist > 0) return redirect('ami/setting_module_fields/modules');
        }

        $recordId = $this->modules_m->save(array('name' => $params['name']), $id);

        return redirect('ami/setting_module_fields/modules');
    }

    protected function _getFilterModules()
    {
        $type = $this->input->get('filter');
        $modules = $this->modules_m->get_by(array('is_draft' => 0), false, false, 'id,name');
        foreach ($modules as $p) {
            if (strtolower($p['name']) == strtolower($type)) {
                return $p['name'];
                exit();
            }
        }
    }

    public function module_fields()
    {
        if (!$this->hasPermission('view', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('is_draft' => 0);

        $filter = $this->_getFilterModules();

        if ($filter != '' || $filter !== NULL) {
            $conditions['module'] = $this->_getFilterModules();
        }
        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $data = $this->module_fields_m->get_limit($this->getPageLimit(), $conditions, 'last_updated desc');

        $this->data['total'] = $this->module_fields_m->count('where', $conditions);
        $this->data['filter'] = $this->input->get('filter');
        $this->data['data'] = $data;
        $this->data['modules'] = $this->modules_m->get_by(array('is_draft' => 0), false, 'name asc', 'id,name');
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Module Fields');

        return $this->render('ami/setting_module_fields/module_fields', $this->data);
    }

    public function _modules($ajax = false)
    {
        $modules = $this->modules_m->getListOptions('- Select -', array('is_draft' => 0), true);

        if ($ajax == true) {
            $html = '';
            foreach ($modules as $k => $p) {
                $html .= '<option value="' . $k . '">' . $p . '</option>';
            }
            echo $html;
            exit();
        } else {
            return array(
                'modules' => $modules
            );
        }
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->data += $this->_modules();
        return $this->render('ami/setting_module_fields/module_fields_edit', $this->data);
    }

    public function module_fields_edit($id = null)
    {
        if (!$this->hasPermission('edit', 'setting_module_fields') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->_assertId($id, 'ami/setting_module_fields/module_fields');

        if ($id = intval($id)) {
            $post = $this->module_fields_m->get($id);
            $this->data['post'] = $post;
            $this->data['page_title'] = page_title('Edit Module Fields');
            $this->data['filter'] = $this->input->get('filter');

            $this->data += $this->_modules();

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'setting_module_fields')) {
                    return $this->noPermission();
                }

                return $this->render('ami/setting_module_fields/module_fields_preview', $this->data);
            } else {
                return $this->render('ami/setting_module_fields/module_fields_edit', $this->data);
            }
        }

        return redirect('ami/setting_module_fields/module_fields');
    }

    public function module_fields_save()
    {
        if (!$this->hasPermission('add', 'setting_module_fields') && !$this->hasPermission('edit', 'setting_module_fields')) {
            return $this->noPermission();
        }
        $this->is('POST');

        $data = $this->module_fields_m->array_from_post(array('id', 'field', 'module'));

        $id = $data['id'] ? $data['id'] : null;

        $postId = $this->module_fields_m->save($data, $id);


        return redirect('ami/setting_module_fields/module_fields?filter=' . $this->input->post('filter'));
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/setting_module_fields/');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/setting_module_fields/module_fields';
                $data_del = array('is_draft' => 1, 'last_updated' => get_date());
                $this->module_fields_m->save($data_del, $id, false, 'DELETE');
                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/setting_module_fields/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Setting Module Fields"
                );
                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/setting_module_fields/');
    }


}