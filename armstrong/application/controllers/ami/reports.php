<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/dompdf/dompdf_config.inc.php');


class Reports extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */


    public function __construct()
    {
        parent::__construct();
        $this->load->model('customers_m');
        $this->load->model('country_channels_m');
        $this->load->model('salespersons_m');
        $this->load->model('roles_m');
        $this->load->model('country_m');
        $this->load->model('products_m');
        $this->load->model('prizes_setting_m');

        $this->data['reportEndpoint'] = $this->config->item('report_url');
        $this->data['reportLabels'] = array(
            /*Reports*/
            'report_summary' => 'All-in-One Transactions Summary Report',
            'report_call' => 'Call Report',
            'report_tfo' => 'TFO Report',
            'report_pantry_check' => 'Pantry Report',
            'report_sampling' => 'Sampling Report',
            'report_otm' => 'OTM Report',
            'report_approval' => 'Approval Report',
            'report_customers' => 'Customer Report',
            'top_ten_penetration_sr' => 'Top 10 Penetration Report ',
            'report_route_plan' => 'Route Plan Report',
            'report_grip_and_grab_sr' => 'Grip And Grap SR Report',
            'report_all_in_one' => 'All-in-One Report',
            'get_app_versions_report' => 'App Versions Report',
            'penetration_data_report' => 'SKU Penetration Report',
            'penetration_customers_report' => 'Customers Penetration Report',
            'get_perfect_call_report_tracking' => 'Perfect Call Report',
            'get_perfect_call_report_tracking_detail' => 'Perfect Call Tracking Detail',
            'get_perfect_store_report' => 'Perfect Store Data',
            'coaching_note' => 'Coaching Note Report',
            'coaching_summary' => 'Coaching Summary',
            'route_master_temp' => ' Route Master Template',
            'customer_contact_data' => 'Customer Contact Data',
            'report_last_pantry_check' => 'Last Pantry Check',
            'regional_sync_report' => 'Regional Sync Report',
            'rap_report' => 'RAP Report',
            'dish_penetration_report' => 'Dish Penetration Report',
            'free_gift_report' => 'Free Gift Report',
            'potential_customers_report' => 'Potential Customers Report',
            /*Data*/
            'data_call' => 'Call and Route  Data',
            'data_otm' => 'OTM Data',
            'data_tfo' => 'TFO Data',
            'data_ssd' => 'SSD Data',
            'data_pantry_check' => 'Pantry Data',
            'data_sampling' => 'Sampling Data',
            'new_customer_report' => 'New Customer Report',
            'top_ten_penetration_calculation_tr' => 'Top 10 Penetration Transaction Calculation Data',
            'new_grip_report' => 'New Grip Data',
            'new_grab_report' => 'New Grab Data',
            'lost_grip_report' => 'Lost Grip Data',
            'lost_grab_report' => 'Lost Grab Data',
            'potential_lost_grip_report' => 'Potential Lost Grip Data',
            'potential_lost_grab_report' => 'Potential Lost Grab Data',
            'top_ten_penetration_tr' => 'Top 10 Penetration Data',
            'competitor_reprot' => 'Competitor Data',
            'objective_record_data' => 'Objective Record Data',
            'activities_log_data' => 'Activities Log Data',
            'data_customers' => 'Customer Data',
            'data_product' => 'Product Data',
            'data_route_plan' => 'Route Plan Data',
            'data_organization' => 'Organization Data',
            'data_region_master' => 'Region Master Data',
            'data_ssd_mt' => 'SSD Data',
            'data_distributors' => 'Distributors Data',
            'data_wholesalers' => 'Wholesalers Data',
            'data_recipes' => 'Recipes Data',
            'get_detail_app_versions_report' => 'Detail App Versions Report',
            'get_perfect_call_report' => 'Call Report',
            /*Business Reports*/
            'customer_profiling' => 'Customer Profiling Report',
            'customer_profiling_transaction' => 'Customer Profiling Transaction Report',
            'ggg_report' => 'GGG Report',
            'notes_report' => 'Notes Report',
            'email_report' => 'Email Report',
            'uplift_report' => 'UPLIFT Transactional Report',
            'qc_report' => 'QC Transactional Report',
            'avp_point_report' => 'AVP Point Report',
            'gift_voucher_report' => 'Gift Voucher Report',
            'assessment_report' => 'Assessment Reports',
            'out_of_trade_report' => 'Out Of Trade Reports',
            'rich_media_demonstration' => 'Rich Media Demonstration Report',
            'summary_out_of_trade_report' => 'Out Of Trade Reports',
            'transaction_rich_media_demonstration' => 'Rich Media Demonstration Report',
            'perfect_store_master_data' => 'Perfect Store Master Data',
            'ttl_report' => 'TTL Report',
            'bo_file' => 'BO File',
        );
    }

    protected function _setExportFields()
    {
        if($this->country_id == '4') //only for SA
        {
            $top_sku = array('data_tfo', 'data_pantry_check');
            $productlist = array('data_pantry_check', 'ggg_report', 'data_tfo', 'data_ssd');
        }
        else
        {
            $top_sku = array();
            $productlist = array('ggg_report', 'data_tfo', 'data_ssd');
        } //
        $top_sku = array('data_tfo', 'data_pantry_check');
        $productlist = array('data_pantry_check', 'ggg_report', 'data_tfo', 'data_ssd');
        return array(
            'years' => array('ggg_report'),
            'month_ranger' => array('ggg_report'),
            'from_to' => array('get_perfect_call_report_tracking','get_perfect_call_report_tracking_detail','summary_out_of_trade_report','out_of_trade_report', 'assessment_report', 'gift_voucher_report', 'avp_point_report', 'qc_report', 'uplift_report', 'email_report', 'notes_report', 'data_ssd_mt', 'data_route_plan', 'coaching_note', 'coaching_summary', 'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'report_last_pantry_check', 'report_tfo', 'report_call',
                'dish_penetration_report', 'report_sampling', 'report_pantry_check', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'data_inventory_check', 'customer_profiling_transaction', 'rap_report',
                'free_gift_report', 'potential_customers_report'),
            'salespersons_status' => array('transaction_rich_media_demonstration','rich_media_demonstration', 'assessment_report'),
            'salespersons' => array('transaction_rich_media_demonstration','summary_out_of_trade_report','rich_media_demonstration', 'out_of_trade_report', 'assessment_report', 'gift_voucher_report', 'avp_point_report', 'qc_report', 'uplift_report', 'email_report', 'notes_report', 'ggg_report', 'coaching_note', 'coaching_summary', 'get_perfect_store_report', 'get_perfect_call_report_tracking',/*'penetration_customers_report',*/
                'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'report_all_in_one', 'data_tfo', 'data_ssd', 'data_call', 'data_sampling', 'data_pantry_check', 'report_last_pantry_check', 'report_otm', 'report_pantry_check', 'report_customers', /*'report_call',*/
                'dish_penetration_report', 'rap_report', 'top_ten_penetration_sr', 'report_summary_channel', 'report_route_plan', 'report_sampling', 'report_grip_and_grab_sr', 'report_tfo', 'data_route_plan', 'top_ten_penetration_calculation_tr', 'new_grip_report', 'new_grab_report', 'lost_grip_report', 'lost_grab_report', 'potential_lost_grip_report', 'potential_lost_grab_report', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'data_ssd_mt', 'data_wholesalers', 'data_distributors',
                'competitor_reprot', 'objective_record_data', 'activities_log_data', 'get_perfect_call_report_tracking_detail', 'data_inventory_check', 'route_master_temp', 'free_gift_report', 'perfect_store_master_data','data_customers'),
            'month-year' => array('transaction_rich_media_demonstration','rich_media_demonstration', 'data_ssd_mt', 'get_perfect_store_report', 'penetration_customers_report', 'penetration_data_report', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr', /*'report_call'*/ 'ttl_report', 'bo_file'),
            'customers' => array('gift_voucher_report', 'customers', 'notes_report'), //, 'dish_penetration_report'
            'grip_customers' => array('penetration_data_report'),
            'top-ten' => array('report_summary_channel'),
            'country_list' => array('data_region_master'/*, 'customer_profiling_transaction'*/),
            'multi_country_list' => array(),
            'season' => array('get_perfect_call_report_tracking_detail', 'get_perfect_call_report_tracking'),
            'number_sku' => array('penetration_customers_report'),
            'manager' => array('ggg_report'/*'penetration_customers_report', 'penetration_data_report'*/),
            'app_type' => array('summary_out_of_trade_report','notes_report', 'qc_report', 'uplift_report', 'email_report', 'ggg_report','customer_contact_data', 'get_detail_app_versions_report', 'get_perfect_call_report_tracking_detail', 'route_master_temp',
            'free_gift_report', 'report_otm', 'report_customers', 'report_pantry_check', 'get_app_versions_report', 'data_route_plan', 'coaching_summary', 'penetration_customers_report', 'penetration_data_report', 'get_perfect_call_report_tracking', 'report_last_pantry_check', 'report_call', 'customer_profiling_transaction'),
            'multi_app_type' => array('avp_point_report','data_call', 'data_tfo', 'data_ssd', 'data_pantry_check', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'get_perfect_store_report', 'coaching_note', 'rap_report', 'data_sampling'),
            'custom' => [
                // views/ami/reports/custom/{template}
                'report_call', 'penetration_data_report', 'penetration_customers_report', 'customer_profiling_transaction',
//                'route_master_temp',
            ],
            'customer_channel' => array('avp_point_report', 'ggg_report', 'dish_penetration_report'), //, 'dish_penetration_report'
            'products' => $productlist, // 'dish_penetration_report',
            'top_sku' => $top_sku,
            'salesleader' => array('ggg_report'),
            'report_type' => array('dish_penetration_report'),
            'note_type' => array('notes_report'),
        );

    }

    protected function _setRegionalExportFields()
    {
        return array(
            'from_to' => array('get_perfect_call_report_tracking','get_perfect_call_report_tracking_detail','data_route_plan', 'coaching_note', 'coaching_summary', 'get_perfect_call_report', 'get_app_versions_report', 'get_detail_app_versions_report', 'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'report_last_pantry_check', 'report_tfo', 'report_call', 'report_sampling', 'report_pantry_check', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'data_inventory_check', 'customer_profiling'),
            'month-year' => array('regional_sync_report', 'get_perfect_store_report', 'penetration_customers_report', 'penetration_data_report', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr', /*'report_call'*/),
            'season' => array('get_perfect_call_report_tracking_detail', 'get_perfect_call_report_tracking'),
            'country_list' => array('get_app_versions_report', 'penetration_customers_report', 'penetration_data_report', /*'report_call',*/
            'data_ssd', 'data_tfo', 'data_call', 'data_sampling', 'data_pantry_check', 'report_last_pantry_check', 'competitor_reprot', 'objective_record_data', 'activities_log_data', 'get_detail_app_versions_report', 'get_perfect_call_report_tracking_detail', 'get_perfect_store_report', 'coaching_note', 'coaching_summary', 'data_customers', 'data_route_plan', 'data_ssd_mt', 'data_distributors', 'data_wholesalers', 'route_master_temp', 'customer_contact_data'),
            'multi_country_list' => array('get_perfect_call_report', 'report_tfo', 'report_call',
            'report_pantry_check', 'report_sampling', 'top_ten_penetration_sr', 'report_summary_channel', 'report_sampling', 'top_ten_penetration_calculation_tr', 'data_otm', 'report_approval', 'top_ten_penetration_tr', 'report_summary', 'new_customer_report', 'data_inventory_check',
            'get_perfect_call_report_tracking', 'report_otm', 'report_customers', 'report_route_plan', 'report_grip_and_grab_sr', 'customer_profiling', 'regional_sync_report'),
            'grip_customers' => array('penetration_data_report'),
            'app_type' => array('customer_contact_data', 'route_master_temp',
            'report_otm', 'report_customers', 'report_pantry_check', 'get_app_versions_report', 'data_route_plan', 'coaching_note', 'coaching_summary', 'penetration_customers_report', 'penetration_data_report', 'data_tfo', 'competitor_reprot', 'data_call', 'data_sampling', 'get_perfect_store_report', 'get_perfect_call_report_tracking', 'get_perfect_call_report_tracking_detail', 'data_pantry_check', 'report_last_pantry_check', 'report_call', 'customer_profiling', 'regional_sync_report'),
            'show_graph' => array('customer_profiling')
        );
    }

    public function index()
    {
        $action = $this->uri->segment(3);
        if (!$action) {
            return redirect('/');
        }
        $filter = $this->input->get('filter');
        if (!$filter) {
            $filter = 'salespersons';
        }
        $this->data['filter'] = $filter;
        $this->data['action'] = $action;
        if ($filter == 'salespersons') {
            $this->data['fields'] = $this->_setExportFields();
        } else {
            $this->data['fields'] = $this->_setRegionalExportFields();
        }
        //hieu filter
        if (isset($this->data['fields']['report_type']) && in_array($action, $this->data['fields']['report_type'])) {
            $this->data['report']['report_type'] = array(
                'type' => 'report_type',
                'list' => array(
                    'customer' => 'Customer',
                    'channel' => 'Channel',
                )
            );
        }
        if (isset($this->data['fields']['note_type']) && in_array($action, $this->data['fields']['note_type'])) {
            $this->data['report']['note_type'] = array(
                'type' => 'note_type',
                'list' => array(
                    'customer' => 'Customer Note',
                    'call' => 'Call Note',
                    'remark' => 'Remark',
                    'reminder' => 'Reminder',
                )
            );
        }
        if (isset($this->data['fields']['years']) && in_array($action, $this->data['fields']['years'])) {
            $this->data['yearcurrent'] = (int)date('Y');
            $this->data['report']['years'] = array(
                'type' => 'years'
            );
        }
        if (isset($this->data['fields']['month_ranger']) && in_array($action, $this->data['fields']['month_ranger'])) {
            $months = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July ',
                'August',
                'September',
                'October',
                'November',
                'December',
            );
            $this->data['months'] = $months;
            $this->data['report']['month_ranger'] = array(
                'type' => 'month_ranger'
            );
        }
//        Saler leader
        if (isset($this->data['fields']['salesleader']) && in_array($action, $this->data['fields']['salesleader'])) {
            $salesleader = $this->salespersons_m->getLeaderListOptions();
            $this->data['report']['salesleader'] = array(
                'type' => 'salesleader',
                'salesleader' => $salesleader
            );
        }
        /* From and To */
        if (isset($this->data['fields']['from_to']) && in_array($action, $this->data['fields']['from_to'])) {
            if (($action == 'data_ssd_mt' && $this->country_code == 'tw') || $action != 'data_ssd_mt') {
                $this->data['report']['from_to'] = array(
                    'type' => 'datepicker'
                );
            }
        }
        if ($action == 'customer_profiling') {
            $countries = (in_array($this->country_id, array(12, 13, 14, 15, 16, 17,18))) ? $this->country_m->getListCountry(null, array('id !=' => 0, 'id in (12,13,14,15,16,17,18,88)' => null)) : $this->country_m->getListCountry(null, array('id !=' => 0, 'id !=' => 88));
        } else {
            $countries = (in_array($this->country_id, array(12, 13, 14, 15, 16, 17,18))) ? $this->country_m->getListCountry(null, array('id !=' => 0, 'id in (12,13,14,15,16,17,18)' => null)) : $this->country_m->getListCountry(null, array('id !=' => 0));
        }
        /* Season */
        if (isset($this->data['fields']['season']) && in_array($action, $this->data['fields']['season'])) {

            $prizes_setting = $this->prizes_setting_m->get(null, false, 'date_start desc');
            $season_data = array();
            foreach ($prizes_setting as $k => $p) {
                $season_data[$p['date_start'] . '.' . $p['date_expired']] = 'Season ' . $p['season'] . ': ' . date('d/m/Y', strtotime($p['date_start'])) . ' - ' . date('d/m/Y', strtotime($p['date_expired']));
            }
            $this->data['report']['season'] = array(
                'type' => 'season',
                'prizes_setting' => $season_data
            );
        }
        /* Month and Year */
        if (isset($this->data['fields']['month-year']) && in_array($action, $this->data['fields']['month-year'])) {
            if (($action == 'data_ssd_mt' && $this->country_code != 'tw') || $action != 'data_ssd_mt') {
                $this->data['report']['month-year'] = array(
                    'type' => 'month-year'
                );
            }
        }

        if (isset($this->data['fields']['number_sku']) && in_array($action, $this->data['fields']['number_sku'])) {
            $this->data['report']['number_sku'] = array(
                'type' => 'number_sku'
            );
        }



        // TODO: Hot fix for undefined index ($this->data['fields']['country_list'])
        if (isset($this->data['fields']['country_list']) && in_array($action, $this->data['fields']['country_list'])) {
            $this->data['report']['country_list'] = array(
                'type' => 'country_list'
            );
        }

        if (isset($this->data['fields']['multi_country_list']) && in_array($action, $this->data['fields']['multi_country_list'])) {
            $this->data['report']['multi_country_list'] = array(
                'type' => 'multi_country_list'
            );
            $temp[0] = 'All';
            foreach ($countries as $c_id => $name) {
                $temp[$c_id] = $name;
            }
            $countries = $temp;
        }

        if (isset($this->data['fields']['app_type']) && in_array($action, $this->data['fields']['app_type'])) {
            if (($action == 'data_ssd_mt' && $this->country_code == 'tw') || $action != 'data_ssd_mt') {
                $this->data['report']['app_type'] = array(
                    'type' => 'app_type'
                );
            }
        }

        if (isset($this->data['fields']['multi_app_type']) && in_array($action, $this->data['fields']['multi_app_type'])) {
            if (($action == 'data_ssd_mt' && $this->country_code == 'tw') || $action != 'data_ssd_mt') {
                $this->data['report']['multi_app_type'] = array(
                    'type' => 'multi_app_type'
                );
            }
        }

        if (isset($this->data['fields']['manager']) && in_array($action, $this->data['fields']['manager'])) {
            $this->data['report']['manager'] = array(
                'type' => 'managerId'
            );
        }

        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
            'active' => 1,
//            'roles_id' => intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'))

        );
        if (in_array($action, array('data_distributors', 'data_wholesalers'))) {
            if($this->country_id == 6){
                $conditions['(type like "%pull%" OR type like "%push%" OR type like "%mix%")'] = null;
            }else{
                $conditions['(type like "%push%" OR type like "%mix%")'] = null;
            }

        } elseif (in_array($action, array('data_customers'))) {
            $conditions['(type like "%pull%" OR type like "%mix%")'] = null;
        } else {
            $conditions['(type like "%pull%" OR type like "%push%" OR type like "%mix%")'] = null;
        }
        if (isset($this->data['fields']['salespersons_status']) && in_array($action, $this->data['fields']['salespersons_status'])) {
            $this->data['report']['salespersons_status'] = array(
                'col' => 'channel_id',
                'type' => 'salespersons_status',
                'list' => []
            );
        }
        if ($this->input->is_ajax_request()) {
            $manageId = $this->uri->segment(4);

            if ($manageId != '') {
                if (in_array(strtolower($manageId), array('push', 'pull', 'sl', 'app_all'))) {
                    if (strtolower($manageId) == 'sl') {
                        $sales = $this->salespersons_m->getPersonListOptions(null, array(
                            'country_id' => $this->country_id,
                            'is_draft' => 0,
                            // 'listed' => 1,
                            'active' => 1,
                            'type LIKE' => '%sl%'
                        ));
                    } elseif (strtolower($manageId) == 'app_all') {
                        $sales = $this->salespersons_m->getPersonListOptions(null, array(
                            'country_id' => $this->country_id,
                            'is_draft' => 0,
                            'active' => 1,
                            // 'listed' => 1
                        ));
                    } else {
                        $conditions = array_merge($conditions, array('(type LIKE "%' . $manageId . '%" OR type LIKE "%mix%")' => NULL));
                        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions);
                    }
                } else {
                    if ($manageId != 'manager_all') {
                        $conditions = array_merge($conditions, array('salespersons_manager_id' => $manageId));
                    }
                    $sales = $this->salespersons_m->getPersonListOptions(null, $conditions);
                }
            }
            if ($manageId != '') {
                $html = '<option value="" selected="selected">ALL</option>';
                foreach ($sales as $k => $s) {
                    $html .= '<option value="' . $k . '">' . $s . '</option>';
                }
            } else {
                $html = '';
            }
            echo $html;
            exit();
        }

        if ($this->hasPermission('manage_all', 'salespersons')) {
            $this->data['sr_manage'] = 'all';
            $salespersons = $this->salespersons_m->getPersonListOptions(null, $conditions);
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);

            $managers = $this->salespersons_m->getPersonListOptions(null, array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                // 'listed' => 1,
                'type LIKE' => '%sl%'
            ));

            $managers = array_merge(array('ALL' => 'All'), $managers);
        } else if ($this->hasPermission('manage_staff', 'salespersons')) {
            $this->data['sr_manage'] = 'staff';
            $user = $this->session->userdata('user_data');
            $conditions['salespersons_manager_id'] = $user['id'];
            $salespersons = $this->salespersons_m->getPersonListOptions(null, $conditions);
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);

            $managers = $this->salespersons_m->getPersonListOptions(null, array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                // 'listed' => 1,
                'type LIKE' => '%sl%',
                'armstrong_2_salespersons_id' => $user['id']
            ), true);
            $managers = array_merge(array('ALL' => 'ALL'), $managers);
        } else {
            $salespersons = array();
            $managers = array();
        }
        if (isset($this->data['fields']['salespersons']) && in_array($action, $this->data['fields']['salespersons'])) {
            if (($action == 'data_ssd_mt' && $this->country_code == 'tw') || $action != 'data_ssd_mt' || $action == 'ggg_report') {
                $this->data['report']['salespersons'] = array(
                    'col' => 'armstrong_2_salespersons_id',
                    'type' => 'multi_select',
                    'list' => $salespersons
                );
                $this->data['report']['managers'] = array(
                    'col' => 'managerId',
                    'type' => 'select',
                    'list' => $managers
                );
            }
        }
        if (isset($this->data['fields']['customer_channel']) && in_array($action, $this->data['fields']['customer_channel'])) {
            $channels = $this->_getChannels();
            $unassigned_arr = array(
                '0' => array(
                    'id' => -11,
                    'name' => 'Unassigned'
                )
            );
            $merges = array_merge($channels, $unassigned_arr);
            $channel_output = ['all' => 'All'];
            if ($merges) {
                foreach ($merges as $merge) {
                    $channel_output[$merge['id']] = trim($merge['name'], "\n\"");
                }
            }
            $this->data['report']['customers_channel'] = array(
                'col' => 'customer_channel',
                'type' => 'multi_select',
                'list' => $channel_output,
                'selected' => array('all'),
            );
        }
        /* Customers / Operators */
        if (isset($this->data['fields']['customers']) && in_array($action, $this->data['fields']['customers'])) {
            $this->data['report']['customers'] = array(
                'col' => 'armstrong_2_customers_id',
                'type' => 'multi_select',
                'list' => array_merge(array('all' => 'All'), $this->customers_m->getCustomerListOptions(null, array('country_id' => $this->country_id, 'approved' => 1, 'is_draft' => 0))),
                'selected' => array('all'),
            );
        }

        if (isset($this->data['fields']['top-ten']) && in_array($action, $this->data['fields']['top-ten'])) {
            $this->data['report']['top-ten'] = array(
                'col' => 'channel_id',
                'type' => 'multi_select',
                'list' => []//$this->country_channels_m->get_by(array('country_id' => $this->country_id))
            );
        }
//        hieu
        if (isset($this->data['fields']['top_sku']) && in_array($action, $this->data['fields']['top_sku'])) {
            $this->data['report']['top_sku'] = array(
                'type' => 'top_sku'
            );
            $this->data['showtopsku'] = true;
        }
        if (isset($this->data['fields']['products']) && in_array($action, $this->data['fields']['products'])) {
            $list_sku = array('' => 'All');
            if($this->country_id == 4) // add none option for report of sa because them have top sku option
            {
                $list_sku = array('none' => 'None', '' => 'All');
            }
            $list_products = $this->products_m->getProductListOptions(null, array(
                'listed' => 1,
                'country_id' => $this->country_id,
                'is_draft' => 0,
            ));
            foreach($list_products as $sku=>$_products){
                $list_sku[$sku] = $_products;
            }
            $this->data['report']['products'] = array(
                'col' => 'sku',
                'type' => 'multi_select',
                'list' => $list_sku
            );
        }
        // $top_sku = $this->products_m->get_by(array(
        //  'is_top_ten'=>1,
        //  'listed'=>1,
        //  'country_id'=>$this->country_id,
        //  'is_draft' => 0,
        //  '(main_sku IS NULL OR LENGTH(main_sku) <1)' => null,
        // ));

        $top_sku = $this->db->from('products')
            ->select('id')
            ->where(array(
                'is_top_ten' => 1,
                'listed' => 1,
                'country_id' => $this->country_id,
                'is_draft' => 0,
                '(main_sku IS NULL OR LENGTH(main_sku) <1)' => null,
            ))
            ->group_by('sku_number')
            ->get()->result_array();

        $this->data['top_sku']['0'] = 'All';
        if (count($top_sku) > 0) {
            $i = 1;
            foreach ($top_sku as $k => $t) {
                $this->data['top_sku'][$i] = $i;
                $i++;
            }
        }

        $this->data['number_sku']['0'] = 'All';
        if (count($top_sku) > 0) {
            $i = 1;
            foreach ($top_sku as $k => $t) {
                $this->data['number_sku'][$i] = $i;
                $i++;
            }
        }
        if (in_array($this->uri->segment(3), array('get_perfect_call_report_tracking', 'customer_contact_data', 'report_customers'))) {
            $this->data['app_type'] = array(
                'PULL' => 'PULL',
                'PUSH' => 'PUSH'
            );
        } else {
            $this->data['app_type'] = array(
                'PULL' => 'PULL',
                'PUSH' => 'PUSH',
                'SL' => 'SL'
            );
        }
        if (isset($this->data['fields']['grip_customers']) && in_array($action, $this->data['fields']['grip_customers'])) {
            $this->data['report']['grip_customers'] = array(
                'col' => 'managerId',
                'type' => 'grip_customers',
                'list' => array(
                    '1' => 'TOTAL customers',
                    '0' => 'GRIP customers'
                )
            );
        }

        if (isset($this->data['fields']['show_graph']) && in_array($action, $this->data['fields']['show_graph'])) {
            $this->data['report']['show_graph'] = array(
                'col' => 'managerId',
                'type' => 'show_graph',
                'list' => array(
                    '0' => 'Stats and Graph',
                    '1' => 'Stats Only',
                    '2' => 'Graph Only'
                )
            );
        }

        $this->data['report']['filter'] = array(
            '-1' => 'All',
            '1' => 'Active',
            '0' => 'Inactive'
        );
        $this->data['report']['action_url'] = end($this->router->uri->segments);
        $this->data['c_tries'] = $countries;
        $this->data['slStatus'] = in_array($action, [
            'qc_report', 'uplift_report', 'data_call', 'data_tfo', 'data_ssd', 'data_pantry_check', 'report_last_pantry_check', 'data_sampling', 'competitor_reprot', 'route_master_temp', 'get_app_versions_report',
            'objective_record_data', 'activities_log_data', 'get_detail_app_versions_report', 'get_perfect_call_report', 'get_perfect_call_report_tracking_detail', 'get_perfect_store_report', 'customer_contact_data',
            'customer_profiling_transaction', 'email_report', 'regional_sync_report', 'rap_report', 'free_gift_report', 'potential_customers_report'
        ]);

        if($this->country_id == 2 || $this->country_id == 9){
            $this->data['tfoSource'] = in_array($action, [
                'data_tfo'
            ]);
        }

        $this->data['templateType'] = in_array($action, [
            'perfect_store_master_data'
        ]);
        $this->data['productStatus'] = in_array($action, [
           // 'data_tfo'
        ]);
        $this->data['cStatus'] = in_array($action, [
            'customer_profiling_transaction'
        ]);
        $this->data['viewFilter'] = in_array($action, [
            'customer_profiling_transaction'
        ]);
        $this->data['syncReportType'] = in_array($action, [
            'regional_sync_report'
        ]);
        $this->data['syncReportView'] = in_array($action, [
            'regional_sync_report'
        ]);

        if (isset($this->data['fields']['custom']) && in_array($action, $this->data['fields']['custom'])) {
            $this->data['custom'] = $this->_getCustomParams($action);
        }
//        dd($this->data);
        $this->data['permission'] = array();
        if (!$this->isSuperAdmin()) {
            $user = $this->session->userdata('user_data');
            $roleId = isset($user['roles_id']) ? $user['roles_id'] : null;
            $this->data['permission'] = $this->roles_m->checkReportPermission($roleId);
        } else {
            $this->data['permission'] = array(
                'report_view' => 1,
                'report_export' => 1
            );
        }
        // if ($this->user_role_name == 'Sales Manager') {
        //     $data['salespersons'] = $this->Ami_model->getEntry('salespersons', array('salespersons_manager_id' => $userId, 'country_id' => $this->country_id), null, 'first_name, last_name, armstrong_2_salespersons_id');
        // }
//        echo('<pre>');
//        dd($this->data);
//        exit();
        return $this->render('ami/reports/index');
    }

    protected function _getFullRegions()
    {
        $this->load->library('CountryParser');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);
        $root = $this->countryparser->parse($this->country_code)->get($hierachy['root']);

        // dd($this->countryparser->parse($this->country_code)->get('region'));

        $rootValues = array('ALL' => 'ALL');

        foreach ($root as $key => $value) {
            $rootValues[$value] = $value;
        }

        return [
            'countryHierachy' => $hierachy,
            'rootValues' => $rootValues,
        ];
    }

    protected function _getRegions()
    {
        $this->load->library('CountryParser');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);

        if (in_array('region', array_values($hierachy))) {
            $defaultRegion = array('ALL' => 'ALL');
            $regions = $this->countryparser->parse($this->country_code)->get('region');

            return is_array($regions) ? $defaultRegion + $regions : $defaultRegion;
        }

        return array();
    }

    protected function _getChannels()
    {
        $this->load->model('country_channels_m');

        return $this->country_channels_m->get_by(array('country_id' => $this->country_id, 'is_draft' => 0));
    }

    protected function _getCustomParams($action)
    {
        switch ($action) {
            case 'report_call':
            case 'penetration_customers_report':
            case 'penetration_data_report':
            case 'customer_profiling_transaction':
            case 'data_customers':


                $conditions = array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    'active' => 1,
                    // 'roles_id' => intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'))
                );

                //$salesLeaderId = intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager'));
                //$salespersonId = intval($this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson'));

                $salesleadersConditions = array(
                    'type' => 'sl',
                    'test_acc' => 0
                );
                $salespersonsConditions = array(
                    "type IN ('pull','push','mix')" => null,
                    'test_acc' => 0
                );
                $countries = array_rewrite($this->countries, 'id');
                $salesleaders = $this->salespersons_m->get_by(array_merge($salesleadersConditions, $conditions));
//                $salesleaders = $this->filter_dummy_data($salesleaders);

                $salespersons = $this->salespersons_m->get_by(array_merge($salespersonsConditions, $conditions));
//                $salespersons = $this->filter_dummy_data($salespersons);

                $channels = $this->_getChannels();
                $unassigned_arr = array(
                    '0' => array(
                        'id' => -11,
                        'name' => 'Unassigned'
                    )
                );
                $params = [
                    'country' => $countries[$this->country_id],
                    'salesleaders' => $salesleaders,
                    'salespersons' => $salespersons,
                    'countryChannels' => array_merge($channels, $unassigned_arr),
                    // 'regions' => $this->_getRegions()
                ];

                break;

            default:
                $params = [];
                break;
        }

        return $params;
    }
    public function getTopProductsArray($to, $from, $apptype)
    {
        $topproduct = array();
        $selected = 'selected';
        if(is_array($apptype) && $apptype)
        {
            if(in_array('PULL', $apptype)){
                $type[] = 2;
            } else if(in_array('PUSH', $apptype)){
                $type[] = 4;
            } else {
                $type[] = 2;
                $type[] = 4;
            }
        }
        else
        {
            $type[] = 2;
        }
        $type = '('.implode(',', $type).')';
        $minyear = date("Y", strtotime($from));
        $maxyear = date("Y", strtotime($to));
        $minmonth = date("m", strtotime($from));
        $maxmonth = date("m", strtotime($to));
        $where = array(
            'top.countryID' => $this->country_id,
            'top.monthReport >= ' => $minmonth,
            'top.monthReport <= ' => $maxmonth,
            'top.yearReport >= ' => $minyear,
            'top.yearReport' => $maxyear,
            'top.skuNumber = `mainSku`' => null,
            'top.type IN '.$type => null
        );
        $selected = '';

        $top_sku = $this->db->select(array('top.skuNumber'))
            ->from('top_sku_on_month top')
            ->join('products p', 'p.sku_number = top.skuNumber', 'left')
            ->where($where)
            ->order_by('top.skuNname', 'asc')
            ->group_by('top.skuNumber')
            ->get()->result_array();

        $top_sku_number = [];
        if ($top_sku) {
            foreach ($top_sku as $key => $sku) {
                $topproduct[] = $sku['skuNumber'];
            }
        }
        return $topproduct;
    }
    public function generate()
    {
        set_time_limit(0);
        $post = $this->input->post();
        $type = $post['type'];
        $sub_type = explode(',', $post['sub_type']);
        $filter = $post['filter'];
        $user = $this->session->userdata('user_data');
        $userId = $user['roles_id'] > 0 ? $user['id'] : 0;

        if($type == 'get_perfect_call_report_tracking_detail' || $type == 'get_perfect_call_report_tracking'){
            if($post['date_type'] == 0){
                if (isset($post['season'])) {
                    $season_arr = explode('.', $post['season']);
                    $from = $season_arr['0'] . ' 00:00:00';
                    $to = $season_arr['1'] . ' 23:59:59';
                }
            }else{
                $from = !empty($post['from']) ? $post['from'] : '';
                $to = !empty($post['to']) ? $post['to'] : '';
            }
        }else{
            $from = !empty($post['from']) ? $post['from'] : '';
            $to = !empty($post['to']) ? $post['to'] : '';
            if (isset($post['season'])) {
                $season_arr = explode('.', $post['season']);
                $from = $season_arr['0'] . ' 00:00:00';
                $to = $season_arr['1'] . ' 23:59:59';
            }
        }
        $app_type = !empty($post['app_type']) ? $post['app_type'] : '';
        $report_type = !empty($post['report_type']) ? '&report_type='.$post['report_type'] : '';
        $cStatus = !empty($post['c_status']) ? $post['c_status'] : '';
        $templateType = !empty($post['templateType']) ? $post['templateType'] : '';
        $tfoSource = !empty($post['tfoSource']) ? $post['tfoSource'] : '';
        $productStatus = !empty($post['productStatus']) ? $post['productStatus'] : '';
        $syncReportType = !empty($post['syncReportType']) ? $post['syncReportType'] : '';
        $syncReportView = !empty($post['syncReportView']) ? $post['syncReportView'] : '';
        $viewFilter = !empty($post['viewFilter']) ? $post['viewFilter'] : '';
        $active = $listed = $customer_channel = $cusId = $sku = '';
        $sr_manage = 'staff';
        if ($this->hasPermission('manage_all', 'salespersons')) {
            $sr_manage = 'all';
        }
        $sr_manage = '&sr_manage=' . $sr_manage;
        if (isset($post['multi_app_type'])) {
            $multi_app_type = !empty($post['multi_app_type']) ? $post['multi_app_type'] : '';
            $app_type = '&app_type=';
            $app_type .= implode('&app_type=', $multi_app_type);
        }
        if (isset($post['customers'])) {
            $gripCustomers = !is_null($post['customers']) ? $post['customers'] : '';
        }
        if (isset($post['active'])) {
            $active = !is_null($post['active']) ? $post['active'] : '';
            $listed = $active;
        }

        $cus_channel = isset($post['customer_channel']) ? $post['customer_channel'] : [];
        $customer_channel = '&customer_channel=';
        $customer_channel .= implode('&customer_channel=', $cus_channel);

        $customerId = isset($post['armstrong_2_customers_id']) ? $post['armstrong_2_customers_id'] : [];
        if(isset($customerId[0]) && $customerId[0] == 'all')
        {
            $customerId = array('0');
        }
        $cusId = '&cusId=';
        $cusId .= implode('&cusId=', $customerId);
        $sku_number = isset($post['sku']) ? $post['sku'] : [];
        $sku = '&sku=';

        if(in_array('', $sku_number)){
//            $list_products = $this->products_m->getProductListOptions(null, array(
//                'listed' => 1,
//                'country_id' => $this->country_id,
//                'is_draft' => 0,
//            ));
//            $list_sku = [];
//            foreach($list_products as $key=>$list_product){
//                $list_sku[] = $key;
//            }
//            $sku .= implode('&sku=', $list_sku);
            $sku = '&sku=0';
        }else{
            $sku .= implode('&sku=', $sku_number);
        }
        //hieu top sku
        if($sku != '&sku=0')
        {
            if(isset($post['top_products'])){
                $topsku = @$post['top_products'];
                if(in_array('ALL', $post['top_products']))
                {
                    $topsku = $this->getTopProductsArray($to, $from, $app_type);
                }
                $sku .= implode('&sku=', array_merge($sku_number, $topsku));
            }
        }
        //end top sku
        if (isset($post['filter_listed'])) {
            $listed = !is_null($post['filter_listed']) ? $post['filter_listed'] : '';
        }

        $pdf = false;
        if ($this->input->is_ajax_request()) {
            $typeFile = '&typeFile=html';
        } elseif (isset($post['typeFile'])) {
            if ($post['typeFile'] == 'pdf') {
                $pdf = true;
            }
            $typeFile = '&typeFile=' . $post['typeFile'];
        } else {
            $typeFile = '&typeFile=xlsx';
        }
        if ($filter != 'regional') {
            $salesperson_ids = '';
            if (array_key_exists('armstrong_2_salespersons_id', $post)) {
                if ($type != 'data_product' && $type != 'data_region_master') {
                    if (!$post['armstrong_2_salespersons_id'][0] || (isset($post['armstrong_2_salespersons_id'][0]) && $post['armstrong_2_salespersons_id'][0] == 'all')) {
                        // if sl == all
                        $salesperson_ids = "&spId=" . $userId;
                        $getall = "&getall=1";
                        // else --> getAll=2 AND spId= sl[]
                        if (isset($post['sales_leader'])) {
                            if (!in_array('ALL', $post['sales_leader'])) {
                                $salesperson_ids = "&spId=";
                                $salesperson_ids .= implode('&spId=', $post['sales_leader']);
                                $getall = '&getall=2';
                            } else {
                                $getall = "&getall=1";
                                $salesperson_ids = "&spId=0";
                            }

                            foreach ($sub_type as $_sub_type) {
                                if (in_array($_sub_type, array('nsm', 'md', 'cdops'))) {
                                    $salesperson_ids = "&spId=" . $userId;
                                    $getall = "&getall=1";
                                    break;
                                }
                            }
                        } else {
                            $salesperson_ids = "&spId=0";
                            if ($this->hasPermission('manage_staff', 'salespersons'))
                                $salesperson_ids = "&spId=" . $userId;
                        }
                    } else {
                        if (in_array('unassigned', $post['armstrong_2_salespersons_id'])) {
                            $salesperson_ids = "&spId=";
                            $salesperson_ids .= implode('&spId=', $post['armstrong_2_salespersons_id']);
                            $getall = "&getall=9";
                        } else {
                            $salesperson_ids = "&spId=";
                            if(isset($post['armstrong_2_salespersons_id'][0]) && $post['armstrong_2_salespersons_id'][0] == 'all')
                            {
                                $salesperson_ids .= '0';
                            }
                            else
                            {
                                $salesperson_ids .= implode('&spId=', $post['armstrong_2_salespersons_id']);
                            }
                            $getall = "&getall=0";
                        }
                    }
                }
            } else {
                $getall = "&getall=0";
                $salesperson_ids = "&spId=" . $userId;

            }
            $channel = $otm = '';

            if (isset($post['channel'])) {
                if (in_array(-1, $post['channel'])) {
                    $channel .= '&channel=-1';
                } else {
                    $channel .= '&channel=';
                    $channel .= implode('&channel=', $post['channel']);
                }
            }

            // if (isset($post['sales_leader']))
            // {
            //  if (in_array('ALL', $post['sales_leader']))
            //  {
            //      $sLeader .= '&sales_leader=ALL';
            //  }
            //  else
            //  {
            //      $sLeader .= '&sales_leader=';
            //      $sLeader .= implode('&sales_leader=', $post['sales_leader']);
            //  }
            // }

            if (isset($post['otm'])) {
                if (in_array('ALL', $post['otm'])) {
                    $otm .= '&otm=ALL';
                } else {
                    $otm .= '&otm=';
                    $otm .= implode('&otm=', $post['otm']);
                }
            }
        } else {
            $getall = '&getall=1';
            $salesperson_ids = "&spId=0";
            $channel = '&channel=-1';
            $otm = '&otm=ALL';
        }
        $from_date = str_replace(':', '%3A', $from . ' 00:00:00');
        $to_date = str_replace(':', '%3A', $to . ' 23:59:59');
        $is_multiCountry = '&is_multi_country=0';
        if (isset($post['country_list'])) {
            if (is_array($post['country_list'])) {
                $this->country_id = 'country=';
                $this->country_id = implode('&country=', $post['country_list']);
                $is_multiCountry = '&is_multi_country=1';
            } else {
                $this->country_id = intval($post['country_list']);
            }
        }
        $cusId = '&custId=';
        $cusId .= implode('&custId=', $customerId);
        switch ($type) {
            case 'assessment_report':
                $active = '&active=' . $active;
                $reports = 'transactiondata/assessment_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                //end fix params
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $active.$typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'out_of_trade_report':
                $active = '&active=' . $active;
                $reports = 'transactiondata/out_of_trade_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                //end fix params
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids .$typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'summary_out_of_trade_report':
                $active = '&active=' . $active;
                $reports = 'summaryreports/out_of_trade_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $app_type = '&app_type=' . $app_type;
                //end fix params
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids .$typeFile.$app_type . $getall . $param_date . $is_multiCountry;
                break;
            case 'gift_voucher_report':
                $reports = 'transactiondata/gift_voucher_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                //end fix params
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids .$cusId. $typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'avp_point_report':
                $reports = 'transactiondata/avp_point_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                //end fix params
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids .'&app_type='.$app_type.$customer_channel. $typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'data_inventory_check':
                $reports = 'transactiondata/data_inventory_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'notes_report':
                $reports = 'transactiondata/notes_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $notetype = '&note_type='.$post['note_type'];
                $app_type = '&app_type=' . $app_type;
                //fix params
                $cusId = '&custId=';
                $cusId .= implode('&custId=', $customerId);
                //end fix params
                $param = $reports . '?country=' . $this->country_id .$app_type. $cusId.$notetype. $salesperson_ids . $typeFile . $getall . $param_date . $is_multiCountry;
                break;
            case 'get_perfect_store_report':
                $active = '&active=' . $active;
                $reports = 'transactiondata/get_perfect_store_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'get_perfect_call_report_tracking_detail':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/get_perfect_call_report_tracking_detail';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'get_perfect_call_report_tracking':
                $reports = 'summaryreports/get_perfect_call_report_tracking';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $post['app_type'] . $sr_manage . $is_multiCountry;
                break;
            case 'get_perfect_call_report':
                $reports = 'transactiondata/get_perfect_call_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'get_detail_app_versions_report':
                $reports = 'transactiondata/get_detail_app_versions_report';
                $active = '&active=' . $active;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $post['app_type'] . $active . $is_multiCountry;
                break;
            case 'get_app_versions_report':
                $listed = '&productActive=' . $listed;
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/get_app_versions_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $listed . $sr_manage . $is_multiCountry;
                break;
            case 'report_all_in_one':
                $reports = 'summaryreports/get_all_in_one';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $is_multiCountry;
                break;
            case 'report_call' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/reportcall';
                $holidays = array();
                $workingDay = '&workingDay=' . $this->getWorkingDays($from, $to, $holidays);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $workingDay . '&from_date=' . $from_date . '&to_date=' . $to_date . $channel . $otm . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'report_customers':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/customers';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'ggg_report':
                $app_type = '&app_type=' . $app_type;
                $salesperson_ids = "&spId=" . $this->salesperson_id;
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $reports = 'transactiondata/ggg_report';
                $param = $reports . '/?country=' . $this->country_id . $app_type.$getall . $salesperson_ids . $typeFile . $sku . $customer_channel . $param_date . $is_multiCountry;
                break;
            case 'data_call':
                $active = '&active=' . $active;
//                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_call';  //  $reports = 'Transaction%20Data:data_call.prpt';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'coaching_note':
//                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/coaching_note';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'coaching_summary':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/coaching_summary';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'data_ssd':
//                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_ssd';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . /*$sku .*/
                    $app_type . $sku . $sr_manage . $is_multiCountry;
//                $post['year'] = intval($post['year']);
//                $post['month'] = intval($post['month']);
//
//                if($this->country_code == 'tw'){
//                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $is_multiCountry;
//                }else{
//                    $param = $reports . '?country=' . $this->country_id . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $is_multiCountry;
//                }
                break;
            case 'data_customers':
                $app_type = '&app_type=PULL';
                if($cStatus == 2)
                {
                    $reports = 'masterdata/data_potential_customers';
                }
                else
                {
                    $reports = 'masterdata/data_customers';
                }
                $cStatus = '&cStatus=' . $cStatus;
                $param = $reports . '/?country=' . $this->country_id . $getall . $cStatus . $salesperson_ids . $typeFile . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'route_master_temp':
                $listed = '&productActive=' . $listed;
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/route_master_temp';
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $app_type . $listed . $sr_manage . $is_multiCountry;
                break;
            case 'customer_contact_data':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/customer_contact_data';
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'report_pantry_check':
                $reports = 'summaryreports/report_pantry_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . '&app_type=' . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'report_otm' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/report_otm';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type .$active. $sr_manage . $is_multiCountry;
                break;
            case 'email_report' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/email_report';
                $active = '&active=' . $active;
                $sent = '&sent_status=' . $post['sent'];
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall .$active. $sent.$salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'uplift_report' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/uplift_report';
                $active = '&active=' . $active;
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $active.$salesperson_ids . $typeFile .$param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'qc_report' :
                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/qc_report';
                $active = '&active=' . $active;
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall .$active. $salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'top_ten_penetration_sr':
                $reports = 'summaryreports/top_ten_penetration_sr';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'report_route_plan':
                $reports = 'summaryreports/route_plan_reprot';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $is_multiCountry;
                break;
            case 'data_pantry_check':
                $active = '&active=' . $active;
//                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_pantry_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $sku . $active . $sr_manage . $is_multiCountry;
                break;
            case 'report_last_pantry_check':
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/report_last_pantry_check';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'report_sampling':
                $app_type = '&app_type=' . $app_type;
                $reports = 'summaryreports/sampling_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $is_multiCountry;
                break;
            case 'report_grip_and_grab_sr':
                $reports = 'summaryreports/grip_and_grab_report_sr';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $is_multiCountry;
                break;
            case 'report_tfo':
                $reports = 'summaryreports/tfo_reprot_sr';  //  $reports = 'Transaction%20Data:data_call.prpt';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'data_route_plan':
                $app_type = '&app_type=' . $app_type;
                $reports = 'masterdata/data_route_plan';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '/?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'data_product':
                $listed = '&productActive=' . $listed;
                $reports = 'masterdata/product_data';
                $param = $reports . '/?country=' . $this->country_id . $typeFile . $listed . $sr_manage . $is_multiCountry;
                break;
            case 'data_sampling':
                $active = '&active=' . $active;
//                $app_type = '&app_type=' . $app_type;
                $reports = 'transactiondata/data_sampling';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'data_tfo':
                $active = '&active=' . $active;
                $reports = 'transactiondata/data_tfo';
                if ($this->country_id != '2' && $this->country_id != '9') {
                    $tfoSource = '&tfoSource=1';
                } else {
                    $tfoSource = '&tfoSource=' . $tfoSource;
                }
                $productStatus = '&productStatus=' . $productStatus;
//                $app_type = '&app_type=' . $app_type;
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $sku . $tfoSource . $productStatus . $sr_manage . $is_multiCountry;
                break;
            case 'data_organization' :
                $reports = 'masterdata/salespersons';
                $salesperson_ids = "&spId=" . $this->salesperson_id;
                $param = $reports . '?country=' . $this->country_id . $typeFile . $salesperson_ids . $sr_manage . $is_multiCountry;
                break;
            case 'top_ten_penetration_calculation_tr' :
                $reports = 'transactiondata/top_ten_penetration_calculation_tr';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'new_grip_report' :
                $reports = 'transactiondata/new_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'new_grab_report' :
                $reports = 'transactiondata/new_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'lost_grip_report' :
                $reports = 'transactiondata/lost_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'lost_grab_report' :
                $reports = 'transactiondata/lost_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'potential_lost_grip_report' :
                $reports = 'transactiondata/potential_lost_grip_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'potential_lost_grab_report' :
                $reports = 'transactiondata/potential_lost_grab_report';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $is_multiCountry;
                break;
            case 'data_otm':
                $reports = 'transactiondata/data_otm';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'report_approval':
                $reports = 'summaryreports/approval_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'top_ten_penetration_tr':
                $reports = 'transactiondata/top_ten_penetration_tr';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'penetration_data_report':
                $reports = 'summaryreports/penetration_data_report_old';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $top_product = "&topProducts=";
                $viewBy = '&isViewByChannel=0';
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . '&app_type=' . $app_type . $channel . $otm . $top_product . '&isGripCustomer=' . $gripCustomers . $viewBy . $is_multiCountry;
                break;
            case 'penetration_customers_report':
                $reports = 'summaryreports/penetration_customers_report';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $post['number_sku'] = intval($post['number_sku']);
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . '&month=' . $post['month'] . '&year=' . $post['year'] . '&numberSku=' . $post['number_sku'] . '&app_type=' . $app_type . $channel . $otm . $sr_manage . $is_multiCountry;
                break;
            case 'competitor_reprot':
                $active = '&active=' . $active;
                $reports = 'transactiondata/competitor_reprot';
//                $app_type = '&app_type=' . $app_type;
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'activities_log_data':
                $reports = 'transactiondata/activities_log_data';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'objective_record_data':
                $reports = 'transactiondata/objective_record_data';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $active = '&active=' . $active;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'data_region_master' :
                $loc = substr(__FILE__, 0, 25);
                $files_patch = '';
                switch ($this->country_id) {
                    case 1:
                        $files_patch .= '&files=' . $loc . '\res\json\my.json';
                        break;
                    case 2:
                        $files_patch .= '&files=' . $loc . '\res\json\au.json';
                        break;
                    case 3:
                        $files_patch .= '&files=' . $loc . '\res\json\hk.json';
                        break;
                    case 4:
                        $files_patch .= '&files=' . $loc . '\res\json\sa.json';
                        break;
                    case 5:
                        $files_patch .= '&files=' . $loc . '\res\json\sg.json';
                        break;
                    case 6:
                        $files_patch .= '&files=' . $loc . '\res\json\tw.json';
                        break;
                    case 7:
                        $files_patch .= '&files=' . $loc . '\res\json\th.json';
                        break;
                    default:
                        $files_patch .= '&files=';
                        break;
                }

                $region = str_replace('\\', '%5C', str_replace(':', '%3A', str_replace('.', '%2E', $files_patch)));
                $reports = 'masterdata/region';
                $typeFile = str_replace('&', '', $typeFile);
                $param = $reports . '?' . $typeFile . '&country=' . $this->country_id . $sr_manage . $is_multiCountry;
                break;
            case 'report_summary':
                $reports = 'summaryreports/all_in_one_transactions';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'new_customer_report':
                $reports = 'transactiondata/new_customer_report';
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $is_multiCountry;
                break;
            case 'data_ssd_mt':
                $reports = 'masterdata/ssd';
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                if ($this->country_code == 'tw') {
                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $sr_manage . $is_multiCountry;
                } else {
                    $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $sr_manage . $is_multiCountry;
                }

                break;
            case 'data_distributors':
                $reports = 'masterdata/distributors';
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . $sr_manage . $is_multiCountry;
                break;
            case 'data_wholesalers':
                $reports = 'masterdata/wholesalers';
                $param = $reports . '?country=' . $this->country_id . $typeFile . $getall . $salesperson_ids . $sr_manage . $is_multiCountry;
                break;
            case 'data_recipes':
                $reports = 'masterdata/data_recipes';
                $param = $reports . '/?country=' . $this->country_id . $typeFile . $sr_manage . $is_multiCountry;
                break;
            case 'customer_profiling':
                $reports = 'summaryreports/customer_profiling_report';
                $app_type = '&app_type=' . $app_type;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&from_date=' . $from_date . '&to_date=' . $to_date . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'customer_profiling_transaction':
                $active = '&active=' . $active;
                if ($viewFilter == 0)
                    $reports = 'transactiondata/customer_profiling_transaction_report';
                else
                    $reports = 'transactiondata/customer_profiling_transaction_report_details';
                $app_type = '&app_type=' . $app_type;
                $cStatus = '&cStatus=' . $cStatus;
                $param_date = "&from_date=" . $from_date . "&to_date=" . $to_date;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $app_type . $active . $cStatus . $sr_manage . $is_multiCountry;
                break;
            case 'regional_sync_report':
                $active = '&active=' . $active;
                $reports = 'summaryreports/regional_sync_report';
                $app_type = '&app_type=' . $app_type;
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $syncReportType = '&syncReportType=' . $syncReportType;
                $syncReportView = '&syncReportView=' . $syncReportView;
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . '&month=' . $post['month'] . '&year=' . $post['year'] . $app_type . $active . $syncReportType . $syncReportView . $is_multiCountry;
                break;
            case 'rap_report':
                $active = '&active=' . $active;
                $reports = 'transactiondata/rap_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                break;
            case 'dish_penetration_report':
                $reports = 'transactiondata/dish_penetration_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                if($post['report_type'] == 'customer')
                {
                    $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $report_type.$typeFile . $getall . $cusId . $customer_channel. $sr_manage.$param_date . $is_multiCountry;
                }
                else
                {
                    $cStatus = '&cStatus=' . $cStatus;
                    $param = $reports . '?country=' . $this->country_id . $cStatus.$salesperson_ids .$report_type.$typeFile . $getall .$cusId. $customer_channel . $sr_manage.$is_multiCountry;
                }
                break;
            case 'free_gift_report':
                $reports = $pdf ? 'json/transactiondata/free_gift_report' : 'transactiondata/free_gift_report';
                $active = '&active=' . $active;
                $app_type = '&app_type=' . $app_type;
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                if ($pdf) {
                    $param = '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                    $param = rawurlencode(str_replace('%3A', ':', $param));
                    $param = str_replace('%3F', '?', $param);
                    $param = str_replace('%3D', '=', $param);
                    $param = str_replace('%26', '&', $param);
                    $java_api = $this->config->item('report_url') . $reports . $param;

                    $options = array(
                        'http' => array(
                            'protocol_version' => '1.1',
                            'method' => 'GET'
                        ),
                    );
                    $context = stream_context_create($options);
                    $data = file_get_contents($java_api, false, $context);
                    if ($data) {
                        $data = json_decode($data);
                        $html = $this->generate_free_gift_html($data);
                        $file_name = 'free_gift_report['.date('dFY', strtotime($from)).'-'.date('dFY', strtotime($to)).']'.'['.strtoupper($this->country_code).'].pdf';
                        $dompdf = new DOMPDF();
                        $dompdf->load_html($html, 'UTF-8');
                        $dompdf->render();
                        $dompdf->stream($file_name);
                    }
                } else {
                    $param = $reports . '?country=' . $this->country_id . $salesperson_ids . $typeFile . $getall . $param_date . $app_type . $active . $sr_manage . $is_multiCountry;
                }

                break;
            case 'potential_customers_report':
                $reports = 'transactiondata/potential_customers_report';
                $param_date = '&from_date=' . $from_date . '&to_date=' . $to_date;
                $active = '&active=' . $active;
                $param = $reports . '?country=' . $this->country_id . $getall . $typeFile . $param_date . $active;
                break;
            case 'rich_media_demonstration':
                $reports = 'summaryreports/rich_media_demostration_report';
                $active = '&active=' . $active;
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $active . '&month=' . $post['month']. '&year='.$post['year'] . $sr_manage . $is_multiCountry;
                break;
            case 'transaction_rich_media_demonstration':
                $reports = 'transactiondata/rich_media_demostration_report';
                $active = '&active=' . $active;
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '?country=' . $this->country_id . $getall . $salesperson_ids . $typeFile . $param_date . $active . '&month=' . $post['month']. '&year='.$post['year'] . $sr_manage . $is_multiCountry;
                break;
            case 'perfect_store_master_data':     
                $reports = 'masterdata/perfect_store_master_data';          
                $templateType = '&templateType=' . $templateType;
                $param = $reports . '/?country=' . $this->country_id . $getall . $templateType . $salesperson_ids . $typeFile . $app_type . $sr_manage . $is_multiCountry;
                break;
            case 'ttl_report':     
                $reports = 'transactiondata/ttl_report';   
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '/?country=' . $this->country_id . $getall . $typeFile . $from_date . $to_date . $active . '&month=' . $post['month']. '&year='.$post['year'];
                break;
            case 'bo_file':     
                $reports = 'transactiondata/bo_file';   
                $post['year'] = intval($post['year']);
                $post['month'] = intval($post['month']);
                $from_date = str_replace(':', '%3A', $post['year'] . '-' . $post['month'] . '-01 00:00:00');
                $to_date = str_replace(':', '%3A', date("Y-m-t", strtotime($post['year'] . '-' . $post['month'] . '-01')) . ' 23:59:59');
                $param = $reports . '/?country=' . $this->country_id . $getall . $typeFile . $from_date . $to_date . $active . '&month=' . $post['month']. '&year='.$post['year'];
                break;
        }
        if ($this->input->is_ajax_request()) {
            $html[] = $this->config->item('report_url') . $param;
            echo json_encode($html);
        } else {
            header("Location: " . $this->config->item('report_url') . $param);
        }
    }

    public function getWorkingDays($startDate, $endDate, $holidays)
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;
        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);
        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);
        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;
                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }
        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }
        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }
        return $workingDays;
    }

    function generate_free_gift_html($data)
    {
        $html = '';
        $html .= '
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <style>
                        html, body, div, span, applet, object, iframe,
                        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
                        a, abbr, acronym, address, big, cite, code,
                        del, dfn, em, img, ins, kbd, q, s, samp,
                        small, strike, strong, sub, sup, tt, var,
                        b, u, i, center,
                        dl, dt, dd, ol, ul, li,
                        fieldset, form, label, legend,
                        table, caption, tbody, tfoot, thead, tr, th, td,
                        article, aside, canvas, details, embed,
                        figure, figcaption, footer, header, hgroup,
                        menu, nav, output, ruby, section, summary,
                        time, mark, audio, video {margin: 0;
                            padding: 0;
                            border: 0;
                            vertical-align: baseline;
                        }
                        article, aside, details, figcaption, figure,
                        footer, header, hgroup, menu, nav, section {
                            display: block;
                        }
                        body {
                            line-height: 1;
                            font-size:35px
                        }
                        ol, ul {
                            list-style: none;
                        }
                        blockquote, q {
                            quotes: none;
                        }
                        blockquote:before, blockquote:after,
                        q:before, q:after {
                            content: \'\';
                            content: none;
                        }
                        table {
                            border-collapse: collapse;
                            border-spacing: 0;
                        }
                        *{font-family: sans-serif;}
                        #content{margin: 1em 3em}
                        #content h1{font-size: 1.5em;font-weight: none;text-align: center;}
                        #content table.generic td{padding: 20px 10px; border: 1px solid #ddd;}
                        #content table.generic td.center{text-align: center;}
                        #content table.generic{width: 100%;}
                        #content table.generic td{text-align: left;}
                        #content table.generic td.colon{width: 50px;}
                        body{padding-top:122px}
                        #content table.generic td.field{width: 480px;}
                        #content table.generic td.extra{width: 400px}
                        #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
                    </style>
                    </head>
                    <body>
                        <div id="content">
                        <img src="' . base_url() . 'res/img/unilever-pdf-logo.png" />
                        <h1>Free Gift Report</h1>
                        <br />
                        <hr />
                        <table class="generic">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Salespersons ID</th>
                                    <th>Salespersons Name</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Call ID</th>
                                    <th>Item Number</th>
                                    <th>Quantity</th>
                                    <th>Date Created</th>
                                    <th>Signature</th>
                                </tr>
                            </thead>
                            <tbody>';
        if ($data->listData) {
            $mime = 'image/png';

            foreach ($data->listData as $i =>$item) {
                $item_number = $i + 1;
                $sig = trim($item->signature) ? '<img src="data:' . $mime . ';base64,' . $item->signature . '" style="width:250px">' : '';
                $html .= "      <tr>
                                    <td class='center'>
                                        {$item_number}
                                    </td>
                                    <td class='center'>
                                        {$item->armstrong2SalespersonsId}
                                    </td>
                                    <td class='center'>
                                        {$item->armstrong2SalespersonsName}
                                    </td>
                                    <td class='center'>
                                        {$item->armstrong2CustomersId}
                                    </td>
                                    <td class='center'>
                                        {$item->armstrong2CustomersName}
                                    </td>
                                    <td class='center'>
                                        {$item->armstrong2CallRecordsId}
                                    </td>
                                    <td class='center'>
                                        {$item->freeGiftItem}
                                    </td>
                                    <td class='center'>
                                        {$item->quantity}
                                    </td>
                                    <td class='center'>
                                        {$item->dateCreated}
                                    </td>
                                    <td class='center'>
                                        {$sig}
                                    </td>
                                </tr>";
            }
        }
        else{
            $html .= '<tr><td colspan="10">No data</td></tr>';
        }
        $html .= '
                            </tbody>
                        </table>
                    </body>
                </html>';
        return $html;
    }
}