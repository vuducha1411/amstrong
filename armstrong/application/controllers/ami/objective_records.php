<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Objective_records extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('objective_records_m');
        $this->load->model('country_channels_m');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');
        switch ($type) {
            case 'country':
                $filter = 0;
                break;
            case 'channel':
                $filter = 1;
                break;
            default:
                $filter = null;
                break;
        }

        return $filter;
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $data = $this->objective_records_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $data = $this->objective_records_m->prepareDataTables($data);

        return $this->datatables->generate($data);
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'objective_records')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $filter = $this->_getFilter();

        if ($filter !== null) {
            $conditions['type'] = $this->_getFilter();
        }
        $active = $this->input->get('active');
        if($active === "0")
        {
            $conditions['datetime_end < "'.get_date().'"'] = null;
            $this->data['active'] = 0;
        }
        else
        {
            $conditions['datetime_end >= "'.get_date().'"'] = null;
            $this->data['active'] = 1;
        }
        $conditions['type IN (0,1)'] = NULL;
        $conditions['armstrong_2_call_records_id'] = '';

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }
        $total = 10000;
        $offset = 0;
        if ($this->hasPermission('manage_all', 'salespersons')) {
            $salespersons = $this->db->select('armstrong_2_salespersons_id')
                ->from('salespersons')
                ->where('country_id', $this->country_id)
                ->get()
                ->result_array();
        } else if ($this->hasPermission('manage_staff', 'salespersons')) {
            $salespersons = $this->db->select('armstrong_2_salespersons_id')
                ->from('salespersons')
                ->where('salespersons_manager_id', $user['id'])
                ->get()
                ->result_array();

            $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
        } else {

        }

//        $data = $this->objective_records_m->get_limit($this->getPageLimit(), $conditions, 'order_number asc, last_updated desc');
        $data = $this->objective_records_m->get_limit(9999, $conditions, 'order_number asc, last_updated desc');

        $this->data['total'] = $this->objective_records_m->count('where', $conditions);
        $this->data['filter'] = $this->input->get('filter');
        $this->data['data'] = $data;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Objective Records');
        return $this->render('ami/objective_records/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'objective_records')) {
            return $this->noPermission();
        }

        $conditions = array("armstrong_2_objective_records_id LIKE '%" . $this->country_code . "'" => NULL, 'is_draft' => 1);

        $this->data['data'] = $this->objective_records_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->objective_records_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Objective Records');

        return $this->render('ami/objective_records/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            'country_channels' => $this->country_channels_m->getCountryChannelsListOptions(false, $conditions, true)
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'objective_records')) {
            return $this->noPermission();
        }
        $this->data += $this->_getAddEditParams();

        return $this->render('ami/objective_records/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'objective_records') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/objective_records');
        if ($id != null) {
            $post = $this->objective_records_m->get($id);
            if ($post['type'] == 1) {
                $ref_id = json_decode($post['ref_id']);
                $post['ref_id'] = explode(',', $ref_id->country_channels_id);
            }
            $this->data['post'] = $post;
            //$this->assertCountry($this->data['objective_records']);
            $this->data['page_title'] = page_title('Edit Objective Records');
            $this->data += $this->_getAddEditParams();
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'objective_records')) {
                    return $this->noPermission();
                }

                return $this->render('ami/objective_records/preview', $this->data);
            } else {
                return $this->render('ami/objective_records/edit', $this->data);
            }
        }

        return redirect('ami/objective_records');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'objective_records')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        $order = 0;
        foreach ($ids as $id) {
                if(isset($_POST['draft']))
                {
                    $this->objective_records_m->save(array(
                        'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'last_updated' => get_date()
                    ), $id);
                }
                else
                {
                    $this->objective_records_m->save(array(
                        // 'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'order_number' => $order++,
                        'last_updated' => get_date()
                    ), $id);
                }
        }
        $referent_url = $_SERVER['HTTP_REFERER'];
        if ($this->input->is_ajax_request()) {
            echo 'ok';
        }
        else
        {
            return redirect($referent_url);
        }
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'objective_records')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/objective_records');

        if ($id != null) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/objective_records';
                //$this->objective_records_m->delete($id);
                $this->objective_records_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/objective_records/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Objective Records"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/objective_records');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'objective_records')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/objective_records');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/objective_records/draft';
                $this->objective_records_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/objective_records/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Objective Records"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/objective_records');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'objective_records') && !$this->hasPermission('edit', 'objective_records')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->objective_records_m->array_from_post(array('armstrong_2_objective_records_id', 'type','app_type', 'description', 'ref_id', 'datetime_start', 'datetime_end'));

        $id = $data['armstrong_2_objective_records_id'] ? $data['armstrong_2_objective_records_id'] : null;
        $data['country_id'] = $this->country_id;

        if ($data['type'] == 0) {
            $data['ref_id'] = json_encode(array('country_id' => $this->country_id));
        } elseif ($data['type'] == 1) {
            $ref_id = implode(',', $data['ref_id']);
            $data['ref_id'] = json_encode(array('country_channels_id' => $ref_id));
        }
        $data['armstrong_2_call_records_id'] = '';
        unset($data['country_channels_id']);
        if(!$id)
        {
            if($maxorder = $this->db->query('select max(order_number) as maxorder from objective_records where country_id = '.$this->country_id))
            {
                $order_number = $maxorder->result_array()[0]['maxorder'];
                $data['order_number'] = $order_number + 1;
            }
            else
            {
                $data['order_number'] = 0;
            }
        }
        if (!$id) {
            $data['armstrong_2_objective_records_id'] = '';// $this->objective_records_m->generateArmstrongId($this->country_id, 'objective_records');
            $post_id = $this->objective_records_m->save($data, null, true, 'INSERT', $this->country_id);
        }
        else
        {
            $post_id = $this->objective_records_m->save($data, $id);
        }

        return redirect('ami/objective_records');
    }
    //update change record int to string
    public function updateRecord(){
        $sql = "SELECT * FROM production.objective_call_records where objective_records not like '%\"objective_id\":\"OBJ%' and last_updated>'2017-04-16' and last_updated<'2017-04-18' and armstrong_2_call_records_id not like 'CAL%'";

        $results = $this->db->query($sql)->result_array();
        $list = '';
        foreach ($results as $result){
           $objective_records = json_decode($result['objective_records'],true);
           $list .=  $result['id']. ',';
           $call_id = $result['armstrong_2_call_records_id'];
           if(is_array($objective_records)){
               foreach ($objective_records as &$ob){
                   $objective_call_record =  $this->db->query('SELECT * FROM objective_records WHERE id ='.$ob['objective_id'])->result_array();

                   if(isset($objective_call_record[0])){
                       $ob['objective_id'] = $objective_call_record[0]['armstrong_2_objective_records_id'];
                       $call_id = $objective_call_record[0]['armstrong_2_call_records_id'];
                   }

               }
           }
           $objective_records = json_encode($objective_records);
           $_sql = "UPDATE `objective_call_records` SET `last_updated` = '2017-04-21 17:58:48', `armstrong_2_call_records_id` = '".$call_id."', `objective_records` = '".$objective_records."' WHERE `id` = '".$result['id']."'";
           $this->db->query($_sql);
        }
        echo $list;

    }
}