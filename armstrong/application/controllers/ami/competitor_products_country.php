<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Competitor_products_country extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('competitor_products_country_m');
        $this->load->model('competitor_products_m');
        $this->load->model('products_m');
        $this->load->model('products_cat_web_m');
        $this->load->model('competitor_products_country_m');
    }
    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $check_list = $this->competitor_products_country_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $check_list = $this->competitor_products_country_m->prepareDataTables($check_list);

        return $this->datatables->generate($check_list);
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $this->data['data'] = $this->competitor_products_country_m->get_by($conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Competitor Products Master List');

        return $this->render('ami/competitor_products_country/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['data'] = $this->competitor_products_country_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->competitor_products_country_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Competitor Products Master List');

        return $this->render('ami/competitor_products_country/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $brands = $this->db->select(array('id', 'name'))->from('competitor_brand')
            ->where(array('country_id' => $this->country_id, 'is_draft' => 0, 'name !=' => ''))
            ->get()->result_array();
        $result = array('' => '- Select -');
        foreach($brands as $brand)
        {
            $result[$brand['id']] = $brand['name'];
        }
        $cat_web = $this->products_cat_web_m->getListOptions('- Select -', $conditions);
        return array(
            'products' => array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'),
            'app_list' => array(
                '0' => 'PULL',
                '1' => 'PUSH',
                '2' => 'BOTH'
            ),
            'brands' => $result,
            'cat_web' => $cat_web,
        );
    }
    public function add()
    {
        if (!$this->hasPermission('add', 'competitor_products_country')) {
            return $this->noPermission();
        }
        $this->data += $this->_getAddEditParams();
        return $this->render('ami/competitor_products_country/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'competitor_products_country') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products_country');

        if ($id = intval($id)) {
            $this->data['post'] = $this->competitor_products_country_m->get($id);
            $ufs_skus = array_filter(explode(',', $this->data['post']['ufs_skus']));
            $this->data['post']['products_array'] = count($ufs_skus) > 0 ? $ufs_skus : array();
            $this->assertCountry($this->data['post']);
            $this->data['page_title'] = page_title('Edit Competitor Products Master List');
            $this->data += $this->_getAddEditParams();
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'competitor_products_country')) {
                    return $this->noPermission();
                }
                return $this->render('ami/competitor_products_country/preview', $this->data);
            } else {
                return $this->render('ami/competitor_products_country/edit', $this->data);
            }
        }

        return redirect('ami/competitor_products_country');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->competitor_products_country_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/competitor_products_country');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products_country');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/competitor_products_country';
                // Change record is_draft to 1
                $this->competitor_products_country_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                $competitorProducts = $this->getCompetitorProducts($id);
                $this->competitor_products_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $competitorProducts[0]['id']);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/competitor_products_country/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Competitor Products Master List"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/competitor_products_country');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products_country');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/competitor_products_country/draft';
                $this->competitor_products_country_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/competitor_products_country/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Competitor Products Master List"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/competitor_products_country');
    }
    public function save()
    {
        if (!$this->hasPermission('add', 'competitor_products_country') && !$this->hasPermission('edit', 'competitor_products_country')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->competitor_products_country_m->array_from_post(array('id', 'active', 'app_type', 'product_cat_web_id', 'brand', 'brand_id', 'name', 'product_name', 'quantity', 'price', 'products', 'ufs_skus', 'weight_per_pc'));
        $ufs_skus = '';
        if ($data['products']) {
            foreach ($data['products'] as $product) {
                $ufs_skus .= $product['sku_number'] . ',';
            }
        }
        unset($data['products']);
        $data['ufs_skus'] = substr($ufs_skus, 0, -1);

        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;
        // New record
        if(!$id){
            $data['date_sync'] = date('Y-m-d H:i:s');
            // Save to competitor products country
            $competitorProductsCountryId = $this->competitor_products_country_m->save($data, $id);

            // Save to competitor products
            $data['armstrong_2_call_records_id'] = '';
            $data['competitor_products_country_id'] = $competitorProductsCountryId;

          //  $postId = $this->competitor_products_m->save($data, $id);
        }else{ // Edit existed record
            // Save to competitor products country
            $this->competitor_products_country_m->save($data, $id);

            // Save to competitor products
            $competitorProducts = $this->getCompetitorProducts($id);
            unset($data['id']);
           // $postId = $this->competitor_products_m->save($data, $competitorProducts[0]['id']);
        }

        return redirect('ami/competitor_products_country');
    }

    public function getCompetitorProducts($id){
        $conditions = ['competitor_products_country_id' => $id];
        return $this->competitor_products_m->get_by($conditions);
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'competitor_products_country'))
        {
            return redirect(site_url('ami/competitor_products_country'));
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $records = $this->competitor_products_country_m->get_by($conditions);
        $sheet = $this->makeSheet($records);

        return $this->excel($sheet, 'competitor_products_country', $type);
    }

    //function copy all data have log id P or U from competitor product contry to competitor product salepersion
    public function copy_record_competitor(){
        $p = '%P%';
        $u = '%U%';
        $conditions = ["log_ids LIKE '{$p}'","log_ids LIKE '{$u}'"];
        $conditions = '(' . implode(' OR ', $conditions) . ')';
        $conditions = $conditions. ' AND is_draft = 0';
        $records = $this->competitor_products_country_m->get_by($conditions);
        $list_log = ['begin=>'];
        foreach ($records as $record){
            $data = [
                'name' => $record['name'],
                'product_name' => $record['product_name'],
                'product_cat_web_id' => $record['product_cat_web_id'],
                'brand' => $record['brand'],
                'weight_per_pc' => $record['weight_per_pc'],
                'ufs_skus' => $record['ufs_skus'],
                'quantity' => $record['quantity'],
                'price' => $record['price'],
                'country_id' => $record['country_id'],
                'is_draft' => $record['is_draft'],
                'date_created' => $record['date_created'],
                'date_sync' => $record['date_sync'],
                'last_updated' => date('Y-m-d H:i:s'),
                'log_ids' => $record['log_ids'],
            ];
            $list_log[]= $record['log_ids'];
            $this->db->insert('competitor_products_saleperson',$data);

            $this->db->where('id', $record['id']);
            $data_update = [
                'is_draft' => 1,
                'last_updated' => date('Y-m-d H:i:s'),
            ];
            $this->db->update('competitor_products_country', $data_update);

        }
        $list_log[] = '=>end';
        echo implode(',',$list_log);
        exit;
    }
}