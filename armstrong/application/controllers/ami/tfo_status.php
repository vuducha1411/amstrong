<?php

class tfo_status extends AMI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('tfo_status_m');
    }

    public function index(){

        if (!$this->hasPermission('view', 'salespersons')) {
            return $this->noPermission();
        }
		$data['page_title'] = page_title('TFO Status');
        $data['tfo'] = $this->tfo_status_m->getTFOStatus($this->getPageLimit(), $this->country_id);
        $data['total'] = $this->tfo_status_m->getTFOCount();
        return $this->render('ami/tfo_status/index', $data);
    }

    public function tfo_stat_data(){
        $data['tfo'] = $this->tfo_status_m->getTFOStatus($this->getPageLimit(), $this->country_id);
        //$data['total'] = $this->tfo_status_m->getTFOCount();
        return $this->json($data);
    }
}
?>