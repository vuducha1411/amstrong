<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions_activity extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('promotions_activity_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'promotions_activity')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['promotions_activity'] = $this->promotions_activity_m->get_by($conditions);
        return $this->render('ami/promotions_activity/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Promotions_activity_dt', 'rowIdCol' => $this->promotions_activity_m->getTablePrimary()));

        $this->promotions_activity_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];
            $_data['id'] = '
                <a href="' . site_url('ami/promotions_activity/edit/' . $_data['id']) . '" data-toggle="ajaxModal">
                    ' . $_data['id'] . '
                </a>
            ';
            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'promotions_activity')) {
                $_data['buttons'] .= html_btn(site_url('ami/promotions_activity/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'promotions_activity')) {
                $_data['buttons'] .= html_btn(site_url('ami/promotions_activity/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'promotions_activity'))
        {
            return $this->noPermission();
        }

        if ($this->input->get('armstrong_2_promotions_activity_id'))
        {
            $this->data['promotions_activity']['armstrong_2_promotions_activity_id'] = $this->input->get('armstrong_2_promotions_activity_id');
        }

        return $this->render('ami/promotions_activity/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'promotions_activity') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotions_activity');
        if ($id) {

            $this->data['promotions_activity'] = $this->promotions_activity_m->get($id);
            $this->data['page_title'] = page_title('Edit promotions Activity');

            return $this->render('ami/promotions_activity/edit', $this->data);
        }

        return redirect('ami/promotions_activity');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'promotions_activity') && !$this->hasPermission('edit', 'promotions_activity'))
        {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->promotions_activity_m->array_from_post(array('id', 'title', 'subject', 'date_start', 'date_end', 'url'));
        $data['country_id'] = $this->country_id;

        if(isset($_FILES['image'])){
            if($_FILES['image']['tmp_name']){
                $uploadOk = 1;
                $target_dir = "res/up/image/";
                $fileExt = explode('.', basename($_FILES["image"]["name"]));
                $fileExt = $fileExt[(count($fileExt) - 1)];
                $fileName = uniqid() . '.' . $fileExt;
                $target_file = $target_dir . $fileName;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check == false) {
                    $uploadOk = 0;
                }
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                    $uploadOk = 0;
                }

                if ($uploadOk == 1) {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                        $data['image'] = base_url() . 'res/up/image/' . $fileName;
                    }
                }
            }
        }

        $id = $data['id'] ? $data['id'] : null;
        $this->promotions_activity_m->save($data, $id);

        return redirect('ami/promotions_activity');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'promotions_activity')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->promotions_activity_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/promotions_activity');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'promotions_activity')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotions_activity');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/promotions_activity';
                //$this->promotions_activitys_m->delete($id);
                $this->promotions_activity_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/promotions_activity/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from promotions activity"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/promotions_activity');
    }
}