<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Working_days extends AMI_Controller
{
    private $convert_arr = array(
        1 => 'Sun',
        2 => 'Mon',
        3 => 'Tue',
        4 => 'Wed',
        5 => 'Thu',
        6 => 'Fri',
        7 => 'Sar'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('working_days_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'working_days')) {
            return $this->noPermission();
        }
        return $this->render('ami/working_days/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('c.country_id' => $this->country_id, 'c.is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Working_days_dt'));

        $this->working_days_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();

        foreach($data['data'] as &$_data){
            $_data['c']['work_days'] = implode(', ', $this->getStrDate($_data['c']['work_days']));
        }

        return $this->json($data);
    }

    function getStrDate($str_number)
    {
        $str_number = str_replace('[', '', $str_number);
        $str_number = str_replace(']', '', $str_number);
        $arr_number = explode(',', $str_number);
        $result = [];
        foreach($this->convert_arr as $key => $val){
            if(in_array($key, $arr_number)){
                $result[] = $val;
            }
        }
        return $result;
    }

}