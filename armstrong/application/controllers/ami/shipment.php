<?php
/**
 * Created by PhpStorm.
 * User: Erika Liongco
 * Date: 12/7/2017
 * Time: 6:46 PM
 */

class Shipment extends AMI_Controller {

    protected $session_id;
    protected $filelog;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('shipment_m');
        $userc = $this->session->userdata('user_data');
        /* set session */
        $this->session_id = $this->session->userdata('session_id');
        // $this->filelog = 'res/up/shipment_import_logs/'.$this->session_id.'.json';
        $this->curr_date = date('Y-m-d H:i:s');
    }

    public function index(){
        if (!$this->hasPermission('view', 'shipment')) {
            return $this->noPermission();
        }

        return $this->render('ami/shipment/index');
    }

    public function shipment_plan()
    {
        if (!$this->hasPermission('view', 'shipment')) {
            return $this->noPermission();
        }
        $this->data['year'] = date('Y', strtotime($this->curr_date));
        $this->data['month'] = date('m', strtotime($this->curr_date));
        $this->data['monthList'] = $this->month_values();
        $this->data['yearList'] = $this->year_values();
        return $this->render('ami/shipment/shipment_plan', $this->data);
    }

    public function get_shipment_plan()
    {
        $post = $this->input->post();
        $where = array(
            'month' => $post['month'],
            'year' => $post['year']
        );

        $data = $this->shipment_m->get_all_record('shipment_plan', $where);
        foreach ($data as $datum) {
//            $datum->sku = $datum->sku_number . " - " . ($datum->sku_name == "" ? $datum->sku_description : $datum->sku_name);
            $datum->sku_name = ($datum->sku_name == "" ? $datum->sku_description : $datum->sku_name);
        }
        $result['data'] = $data;
        $result['recordsTotal'] = count($data);
        $result['recordsFiltered'] = count($data);
        $result['draw'] = 1;

        return $this->json($result);
    }

    public function import(){
        if ($this->is('POST')) {
            if(file_exists($this->filelog))
                unlink($this->filelog);

            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->shipment_m->import_to_db('shipment_plan', $worksheet->toArray(), $this->session_id);
                    }

                    return $this->json(array(
                        'success' => TRUE,
                        'message' => 'Import complete',
                        'type' => 'shipment'
                    ));
                }

                return $this->json(array(
                    'success' => FALSE,
                    'message' => 'Upload complete',
                    'type' => 'shipment'
                ));
            }
        }
    }

    public function sap_invoice()
    {
        if (!$this->hasPermission('view', 'shipment')) {
            return $this->noPermission();
        }

        $this->data['year'] = date('Y', strtotime($this->curr_date));
        $this->data['month'] = date('m', strtotime($this->curr_date));
        $this->data['monthList'] = $this->month_values();
        $this->data['yearList'] = $this->year_values();

        return $this->render('ami/shipment/sap_invoice', $this->data);
    }

    public function get_sap_invoice(){
        $post = $this->input->post();
        $where = array(
            'month(invoice_date)' => $post['month'],
            'year(invoice_date)' => $post['year']
        );
        $data = $this->shipment_m->get_all_record('sap_invoicing', $where);
        /*foreach ($data as $datum) {
            $datum->sku_name = $datum->sku_number." - ".$datum->sku_name;
        }*/
        $result['data'] = $data;
        $result['recordsTotal'] = count($data);
        $result['recordsFiltered'] = count($data);
        $result['draw'] = 1;

        return $this->json($result);
    }

    public function import_sap(){
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $status = $this->shipment_m->import_sap_to_db($worksheet->toArray(), $this->session_id);
                    }

                    if($status){
                        return $this->json(array(
                            'success' => TRUE,
                            'message' => 'Import complete',
                            'type' => 'sap_invoice'
                        ));
                    }
                }

                return $this->json(array(
                    'success' => true,
                    'message' => 'Upload complete',
                    'type' => 'sap_invoice'
                ));
            }
        }
    }

    public function git(){
        if (!$this->hasPermission('view', 'shipment')) {
            return $this->noPermission();
        }

        $this->data['year'] = date('Y', strtotime($this->curr_date));
        $this->data['month'] = date('m', strtotime($this->curr_date));
        $this->data['monthList'] = $this->month_values();
        $this->data['yearList'] = $this->year_values();

        return $this->render('ami/shipment/git', $this->data);
    }

    public function get_git(){
        $post = $this->input->post();
        $where = array(
            'month' => $post['month'],
            'year' => $post['year']
        );

        $data = $this->shipment_m->get_all_record('git', $where);
        /*foreach ($data as $datum) {
            $datum->sku = $datum->sku_number." - ".$datum->sku_name;
        }*/
        $result['data'] = $data;
        $result['recordsTotal'] = count($data);
        $result['recordsFiltered'] = count($data);
        $result['draw'] = 1;

        return $this->json($result);
    }

    public function import_git(){
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->shipment_m->import_to_db('git',$worksheet->toArray(), $this->session_id);
                    }

                    return $this->json(array(
                        'success' => TRUE,
                        'message' => 'Import complete',
                        'type' => 'git'
                    ));
                }

                return $this->json(array(
                    'success' => FALSE,
                    'message' => 'Upload complete',
                    'type' => 'git'
                ));
            }
        }
    }

    public function load_checker() {
        $this->filelog = "res/up/shipment_import_logs/shipment_".$this->session_id.".json";

        if(file_exists($this->filelog)) {
            echo file_get_contents($this->filelog);
        } else {
            echo json_encode(array(
                'progress' => true,
                'hightrow' => 100,
                'rowcurrent' => 0
            ));
        }
    }
}