<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_template extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('master_template_m');
        $this->load->model('customers_m');
        $this->load->model('salespersons_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'route_plan_master_template')) {
            return $this->noPermission();
        }
        $this->data['transfer_btn'] = 2;
        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }

        // $this->data['salespersons'] = $this->salespersons_m->getPersonListOptions(' - Select - ', $conditions);
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons')) {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Master Template');

        return $this->render('ami/master_template/index', $this->data);
    }

    function _getTransferParams(){
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, '(type like "%PULL%" OR type like "%PUSH%")' => null);
        $salespersons = $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true);
        return array(
            'salespersons' => $salespersons
        );
    }

    function getMasterTemplateBySalesperson(){
        $post = $this->input->post();
        $salesperson_id = $post['armstrong_2_salespersons_id'];
        $list_master_temp = '<ul>
                            <li class="selectall">
                                <div>
                                    <input name="checkall" class="checkAll" type="checkbox"> Select All
                                </div>
                            </li>';
        $master_temp_conditions = array(
            'route_master_temp.armstrong_2_salespersons_id IN ("' . $salesperson_id . '")' => null,
            "route_master_temp.is_draft" => 0
        );
        $master_temp = $this->db->select(array('route_master_temp.*', "CASE WHEN route_master_temp.armstrong_2_customers_id LIKE '%OPD%' THEN customers.armstrong_2_customers_name WHEN route_master_temp.armstrong_2_customers_id LIKE '%DIS%' THEN distributors.`name` WHEN route_master_temp.armstrong_2_customers_id LIKE '%WHS%' THEN wholesalers.`name` END as armstrong_2_customers_name"))
            ->from('route_master_temp')
            ->join('customers', 'route_master_temp.armstrong_2_customers_id = customers.armstrong_2_customers_id', 'left')
            ->join('distributors', 'route_master_temp.armstrong_2_customers_id = distributors.armstrong_2_distributors_id', 'left')
            ->join('wholesalers', 'route_master_temp.armstrong_2_customers_id = wholesalers.armstrong_2_wholesalers_id', 'left')
            ->where($master_temp_conditions)
            ->order_by('planned_day')
            ->get()->result_array();

        foreach ($master_temp as $key => $_master_temp) {
            if ($_master_temp['planned_day'] < 10) {
                $_master_temp['planned_day'] = 0 . $_master_temp['planned_day'];
            }
            $customer_name = $_master_temp['planned_day'] . ' - ' . $_master_temp['armstrong_2_customers_name'];
            $list_master_temp .= "<li class='fillable'>
                                        <div data-id={$_master_temp['id']}
                                             data-salesperson={$salesperson_id}><input name='checkbox_' class='check-box'
                                                                                 type='checkbox'> {$customer_name}</div>
                                    </li>";
        }
        $list_master_temp .= '</ul>';
        echo $list_master_temp;
        exit();
    }

    public function transfer()
    {
        if (!$this->hasPermission('edit', 'route_plan_master_template')) {
            return $this->noPermission();
        }
        $this->data = $this->_getTransferParams();
        return $this->render('ami/master_template/transfer_data', $this->data);
    }

    public function transfer_do()
    {
        if (!$this->hasPermission('edit', 'route_plan_master_template') && !$this->hasPermission('add', 'route_plan_master_template')) {
            return $this->noPermission();
        }
        $post = $this->input->post();
        $datas = isset($post['datas']) ? $post['datas'] : '';
        $salesperson_new = isset($post['armstrong_2_salespersons_id_new']) ? $post['armstrong_2_salespersons_id_new'] : '';
        $salespersons_info = $this->salespersons_m->get_by(array('armstrong_2_salespersons_id' => $salesperson_new));
        if ($datas) {
            $master_temp = $this->master_template_m->get_by(array('id IN (' . $datas . ')' => null));
            $app_type = strtolower($salespersons_info[0]['type']) == 'pull' ? 0 : 1;
            // update armstrong_2_salespersons_id
            foreach ($master_temp as $_master_temp) {
                $this->master_template_m->save(array(
                    'armstrong_2_salespersons_id' => $salesperson_new,
                    'app_type' => $app_type
                ), $_master_temp['id']);
            }
        }
        $this->data = $this->_getTransferParams();
        return $this->render('ami/master_template/transfer_data', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $defaultOptions = array('' => ' - Select - ');

        if ($this->hasPermission('manage', 'salespersons')) {
            $defaultOptions['ALL'] = 'ALL';
        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Master Template');

        return $this->render('ami/master_template/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        // default conditions
        $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);
        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata('user_data');
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, false, null, array('admins_id', 'armstrong_2_salespersons_id'));
        // check type role of user
        if ($user['roles_id'] == $roles_salesmanager) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id']);
        } else if ($user['roles_id'] == $roles_salesperson) {
            $conditions_sales = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id']);
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');
        return array(
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_sales, true),
            // 'customers' => $this->customers_m->getCustomerListOptions('- Select -', $conditions)
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        //$this->data['customers'] = array('' => ' - Select - ');
        //$this->data['customers'] = $this->customers_m->getCustomerListOptions('- Select -', array('country_id' => $this->country_id, 'active' => 1, 'approved' => 1, 'is_draft' => 0));
        if ($this->input->is_ajax_request()) {
            if (!$this->hasPermission('view', 'route_plan_master_template')) {
                return $this->noPermission();
            }

            $this->data['customers'] = $this->customers_m->getCustomerListOptions('- Select -', array('country_id' => $this->country_id, 'is_draft' => 0));

            return $this->render('ami/master_template/preview', $this->data);
        }
        return $this->render('ami/master_template/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'route_plan_master_template') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/master_template');

        if ($id) {
            $this->data['template'] = $this->master_template_m->get($id);
            $this->data['page_title'] = page_title('Edit Master Template');

            $this->data += $this->_getAddEditParams();

            $this->data['customers'] = $this->customers_m->getCustomerListOptions('- Select -', array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                'armstrong_2_salespersons_id' => $this->data['template']['armstrong_2_salespersons_id'],
                'approved' => 1,
                'active' => 1
            ));

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'route_plan_master_template')) {
                    return $this->noPermission();
                }

                $this->data['customers'] = $this->customers_m->getCustomerListOptions('- Select -', array('country_id' => $this->country_id, 'is_draft' => 0));

                return $this->render('ami/master_template/preview', $this->data);
            } else {
                return $this->render('ami/master_template/edit', $this->data);
            }
        }

        return redirect('ami/master_template');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->master_template_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/master_template');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/master_template');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/master_template';
                //$this->master_template_m->delete($id);
                $this->master_template_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/master_template/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/master_template');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/master_template');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/master_template/draft';
                $this->master_template_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/master_template/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/master_template');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'route_plan_master_template') && !$this->hasPermission('edit', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->roles_m->array_from_post(array('planned_day', 'planned_week', 'armstrong_2_customers_id',
            'armstrong_2_salespersons_id', 'master_id', 'id'));

        $id = $data['id'] ? $data['id'] : null;

        $data['version'] = 'ami';

        $newId = $this->master_template_m->save($data, $id);

        return redirect('ami/master_template');
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'route_plan_master_template')
        ) {
            return redirect(site_url('ami/master_template'));
        }

        $templates = $this->master_template_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($templates);

        return $this->excel($sheet, 'route_plan_master_template', $type);
    }

    public function templates()
    {
        if (!$this->hasPermission('view', 'route_plan_master_template')) {
            return $this->noPermission();
        }

        $this->is('POST');

        if ($id = $this->input->post('id')) {
            if ($id[0] == 'ALL') {
                $templates = $this->db->select('route_master_temp.*, salespersons.first_name, salespersons.last_name, customers.armstrong_2_customers_name')
                    ->from('route_master_temp')
                    ->join('salespersons', 'route_master_temp.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id')
                    ->join('customers', 'route_master_temp.armstrong_2_customers_id = customers.armstrong_2_customers_id')
                    ->where('salespersons.country_id', $this->country_id)
                    ->get()->result_array();
            } else {
                $conditions = array(
                    'armstrong_2_salespersons_id' => $id,
                    'is_draft' => $this->input->post('draft'),
                    // 'country_id' => $this->country_id
                );

                // $templates = $this->master_template_m->get_by($conditions, false, 'master_id asc, id asc');
                $templates = $this->db->select('route_master_temp.*, salespersons.first_name, salespersons.last_name, customers.armstrong_2_customers_name')
                    ->from($this->master_template_m->getTableName())
                    ->join('salespersons', 'route_master_temp.armstrong_2_salespersons_id = salespersons.armstrong_2_salespersons_id')
                    ->join('customers', 'route_master_temp.armstrong_2_customers_id = customers.armstrong_2_customers_id')
                    ->where_in('route_master_temp.armstrong_2_salespersons_id', $id)
                    ->where(array(
                        'route_master_temp.is_draft' => $this->input->post('draft'),
                        'salespersons.country_id' => $this->country_id
                    ))->get()
                    ->result_array();
            }

            $_templates = array();

            foreach ($templates as $template) {
                $_templates[$template['master_id']][$template['id']] = $template;
            }

            ksort($_templates);

            $this->data['templates'] = $_templates;
            $this->data['draft'] = $this->input->post('draft');

            return $this->render('ami/master_template/template');
        }

        return '';
    }
        public function tokenfield()
    {
        $term = $this->input->get('term');

        $except = trim($this->input->get('except'), ',');
        $salespersons = $this->input->get('salespersons');

        if (!$term) {
            return $this->json(array());
        }

        $conditions = array("armstrong_2_customers_name LIKE '%{$term}%'", "armstrong_2_customers_id LIKE '%{$term}%'");
        $sr_type = '';
        if($salespersons){
            $this->db->where('armstrong_2_salespersons_id', $salespersons);
            $sql = 'SELECT `type` FROM salespersons WHERE armstrong_2_salespersons_id = "'.$salespersons.'"';
            $result = $this->db->query($sql)->result_array();
            $sr_type = $result[0]['type'];
        }

        if($this->input->get('action') == 'master_template' && $sr_type == 'PUSH'){
            $condition1 = array("name LIKE '%{$term}%'", "armstrong_2_distributors_id LIKE '%{$term}%'");
            $condition2 = array("name LIKE '%{$term}%'", "armstrong_2_wholesalers_id LIKE '%{$term}%'");
            $condition1 = '(' . implode(' OR ', $condition1) . ')';
            $condition2 = '(' . implode(' OR ', $condition2) . ')';
        }

        $conditions = '(' . implode(' OR ', $conditions) . ')';
        if($except)
        {
            $conditions = $conditions. ' AND armstrong_2_customers_id NOT IN ('.$except.')';
        }
        if($this->input->get('action') == 'ssd') { //ssd get both customer pending when edit and approve
            $this->db->where('country_id', $this->country_id)
                ->where_in('approved', array(1,2))
                ->where('is_draft', 0)
                ->where('active', 1);
        }
        else
        {
            $this->db->where('country_id', $this->country_id)
                ->where_in('approved', 1)
                ->where('is_draft', 0)
                ->where('active', 1);
        }
        $type = !is_null($this->input->get('action')) ? $this->input->get('type') : null;


        if ($this->input->get('action') == 'master_template' && $sr_type == 'PUSH') {
            $customer1 = $this->db->where('armstrong_2_salespersons_id', $salespersons)
                ->where('country_id', $this->country_id)
                ->where_in('approved', 1)
                ->where('is_draft', 0)
                ->where('active', 1)
                ->where($condition1)->limit(30)
                ->get('distributors')
                ->result_array();
            $customer2 = $this->db->where('country_id', $this->country_id)
                ->where('armstrong_2_salespersons_id', $salespersons)
                ->where_in('approved', 1)
                ->where('is_draft', 0)
                ->where('active', 1)->where($condition2)->limit(30)
                ->get('wholesalers')
                ->result_array();
        }else{
            $customers = $this->db->where($conditions)->limit(30)
                ->get($this->customers_m->getTableName())
                ->result_array();
        }



        $output = array();
        if(isset($customers)){
            foreach ($customers as $customer) {
                $output[] = array(
                    'value' => $customer['armstrong_2_customers_id'],
                    // 'label' => $salesperson['armstrong_2_salespersons_id'],
                    'label' => $customer['armstrong_2_customers_id'] . ' - ' . $customer['armstrong_2_customers_name']
                );
            }
        }else{
            foreach ($customer1 as $customer) {
                $output[] = array(
                    'value' => $customer['armstrong_2_distributors_id'],
                    // 'label' => $salesperson['armstrong_2_salespersons_id'],
                    'label' => $customer['armstrong_2_distributors_id'] . ' - ' . $customer['name']
                );
            }
            foreach ($customer2 as $customer) {
                $output[] = array(
                    'value' => $customer['armstrong_2_wholesalers_id'],
                    // 'label' => $salesperson['armstrong_2_salespersons_id'],
                    'label' => $customer['armstrong_2_wholesalers_id'] . ' - ' . $customer['name']
                );
            }
        }


        return $this->json($output);
    }
}