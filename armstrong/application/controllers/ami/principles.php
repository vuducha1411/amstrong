<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Principles extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('principles_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'principles')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'column_type != ""' => null);
        $principles = $this->principles_m->get_by($conditions);
        $format_array = [];
        foreach ($principles as $item) {
            $format_array[$item['column_type']][$item['app_type']][] = $item;
        }
        foreach ($format_array as $key => &$value) {
            if ($value) {
                foreach ($value as &$item) {
                    $item = count($item);
                }
            }
        }
        $this->data['principles'] = $format_array;
        return $this->render('ami/principles/index', $this->data);
    }

//    public function ajaxData()
//    {
//
//        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
//
//        $datatables = new Datatable(array('model' => 'Chains_dt', 'rowIdCol' => $this->principles_m->getTablePrimary()));
//
//        $this->principles_m->setDatatalesConditions($conditions);
//
//        $data = $datatables->datatableJson();
//        foreach ($data['data'] as &$_data) {
//            $id = $_data['armstrong_2_principles_id'];
//            $_data['armstrong_2_principles_id'] = '
//                <a href="' . site_url('ami/principles/edit/' . $_data['armstrong_2_principles_id']) . '" data-toggle="ajaxModal">
//                    ' . $_data['armstrong_2_principles_id'] . '
//                </a>
//            ';
//            $_data['buttons'] = '<div class="btn-group">';
//
//            if ($this->hasPermission('edit', 'principles')) {
//                $_data['buttons'] .= html_btn(site_url('ami/principles/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
//            }
//
//            if ($this->hasPermission('delete', 'principles')) {
//                $_data['buttons'] .= html_btn(site_url('ami/principles/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
//            }
//
//            $_data['buttons'] .= '</div>';
//        }
//
//        return $this->json($data);
//    }

    public function add()
    {
        if (!$this->hasPermission('add', 'principles')) {
            return $this->noPermission();
        }
        $this->data['column_type'] = $this->principles_m->getColumnTypeList();
        $this->data['action'] = 'add';

        return $this->render('ami/principles/edit', $this->data);
    }

    public function edit($column = null, $app_type = null)
    {
        if (!$this->hasPermission('edit', 'principles') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

//        $this->_assertId($column, 'ami/principles');
        if ($column) {

            $this->data['principles'] = $this->principles_m->get_by(array('column_type' => $column, 'app_type' => $app_type, 'country_id' => $this->country_id, 'is_draft' => 0), false, 'order_number');

            $this->data['page_title'] = page_title('Edit principles');
            $this->data['action'] = 'edit';
            $this->data['app_type'] = $app_type;
            $this->data['column_type'] = $column;
            return $this->render('ami/principles/edit', $this->data);
        }

        return redirect('ami/principles');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'principles') && !$this->hasPermission('edit', 'principles')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $post = $this->input->post();
        $data = $post['data_arr'] ? $post['data_arr'] : '';
        $action = $post['action'] ? $post['action'] : '';
        $column_type = $post['column_type'] ? $post['column_type'] : '';
        $app_type = $post['app_type'] ? $post['app_type'] : '';
        if ($data) {
            if ($action == 'add') {
                $max_order = $this->db->from('principles')
                    ->select_max('order_number')
                    ->where(array(
                        'column_type' => $column_type,
                        'app_type' => $app_type,
                        'country_id' => $this->country_id
                    ))
                    ->get()->row();
                $order = $max_order->order_number;
            } else {
                $order = 0;
            }
            foreach ($data as $_item) {
                $id = $_item['id'] ? $_item['id'] : null;
                $_item['country_id'] = $this->country_id;
                if ($id) { // edit
                    if ($_item['status'] != 'delete') {
                        $order++;
                        $_item['order_number'] = $order;
                    } else { // delete record
                        $_item['order_number'] = 0;
                        $this->principles_m->save(array(
                            'is_draft' => 1,
                            'last_updated' => get_date()
                        ), $_item['id']);
                    }
                } else { // add
                    $order++;
                    $_item['order_number'] = $order;
                }

                unset($_item['status']);
                $this->principles_m->save($_item, $id);
            }
        }

        echo 'ami/principles';
        exit();
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'principles')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->principles_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/principles');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'principles')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/principles');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/principles';
                //$this->principless_m->delete($id);
                $this->principles_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/principles/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from principles"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/principles');
    }
}