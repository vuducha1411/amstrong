<?php

class Kpi extends AMI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('kpi_m');
        // $this->load->model('menu_m');
        $this->load->model('salespersons_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $conditions = array('kpi_target.country_id' => $this->country_id, 'kpi_target.is_draft' => 0);        
        
        $this->data['settings'] = $this->kpi_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('KPI Settings');

        return $this->render('ami/kpi/index', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $conditions = array('kpi_target.country_id' => $this->country_id, 'kpi_target.is_draft' => 1);

        $this->data['settings'] = $this->kpi_m->getKpiSettings($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->kpi_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('KPI Settings');

        return $this->render('ami/kpi/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            // 'modules' => $this->menu_m->get(),
            'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions, true),
            // 'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'country_sub_channels' => $this->country_sub_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            // 'assigned_distributors' => $this->roles_m->parse_form(array('armstrong_2_distributors_id', 'name'), '- Select -', $conditions),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $now = Carbon::now();

        $exists = $this->kpi_m->get_by(array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            'date_created >=' => $now->startOfMonth()->toDateTimeString(),
            'date_created <=' => $now->endOfMonth()->toDateTimeString(),
        ));

        $this->data['exists'] = !empty($exists) ? true : false;

        $this->data += $this->_getAddEditParams();
        $this->data['setting'] = array('month' => date('m', time()), 'year' => date('Y', time()));
        return $this->render('ami/kpi/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'kpi_settings') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi');

        if ($id)
        {
            $this->data['setting'] = $this->kpi_m->get($id); 
            $this->assertCountry($this->data['setting']);            
            $this->data['page_title'] = page_title('Edit KPI Settings');

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'kpi_settings'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/kpi/preview', $this->data);
            }
            else
            {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/kpi/edit', $this->data);
            }
        }

        return redirect('ami/kpi');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id)
            {
                $this->kpi_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/kpi');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi';
                //$this->roles_m->delete($id);
                $this->kpi_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/kpi/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/kpi');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/kpi/draft';
                $this->kpi_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/kpi/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/kpi');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'kpi_settings') && !$this->hasPermission('edit', 'kpi_settings'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->kpi_m->array_from_post(array('armstrong_2_salespersons_id', 'kpi_call_target', 'kpi_new_grab_target', 
            'kpi_new_grip_target', 'kpi_sale_target', 'month', 'year', 'id'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        $newId = $this->kpi_m->save($data, $id);

        return redirect('ami/kpi');
    }
}