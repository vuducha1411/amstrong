<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Potential_Contacts extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('potential_contacts_m');
        $this->load->model('potential_customers_m');
        $this->load->model('media_m');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'active':
                $filter = array('status' => 1);
                break;

            case 'inactive':
                $filter = array('status' => 0);
                break;

            default:
                $filter = array('status' => 1);
                break;
        }

        return $filter;
    }

    protected function _getListContacts($where)
    {
        $contactsTable = $this->potential_contacts_m->getTableName();
        $customersTable = $this->potential_customers_m->getTableName();

        $this->db->select("{$contactsTable}.*, {$customersTable}.armstrong_2_customers_name as {$customersTable}_name");
        $this->db->from($contactsTable);

        $this->db->join($customersTable, "{$contactsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id and {$contactsTable}.type = 0", 'left');

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        $this->db->limit($total, $offset);

        $this->db->where($where);

        $this->db->order_by("{$contactsTable}.id", 'asc');

        return $this->db->get()->result_array();
    }


    public function index()
    {
        if (!$this->hasPermission('view', 'potential_contacts')) {
            return $this->noPermission();
        }

        $this->data['filter'] = $this->input->get('filter') ? $this->input->get('filter') : 'active';
        return $this->render('ami/potential_contacts/index');
    }

    public function ajaxData()
    {
        $user = $this->session->userdata('user_data');
        $filter = $this->_getFilter();
        $conditions = array_merge(array('c.country_id' => $this->country_id, 'c.is_draft' => 0), $filter);
        if(!$this->isSuperAdmin())
        {
            if ($this->hasPermission('manage_staff', 'salespersons')) {
                $list_sales = $this->salespersons_m->getListSales($user['id']);
                $list_sales = implode("','", array_merge(array($user['id']), $list_sales));
                $cus_conditons = array(
                    'country_id' => $this->country_id,
                    'synced_customer' => 0,
                    'is_draft' => 0,
                    "armstrong_2_salespersons_id IN ('{$list_sales}')" => null
                );
                $customers = $this->db->from('potential_customers')
                    ->where($cus_conditons)->get()->result_array();
                $customers = implode('","', $customers);
                $conditions['c.armstrong_1_customers_id IN ("' . $customers . '")'] = null;
            }
        }
        $datatables = new Datatable(array('model' => 'Potential_Contacts_dt'));
        $this->potential_contacts_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson(array(), 'id');
        foreach ($data['data'] as &$_data) {
            $_data['id'] = $_data['c']['id'];
            $id = $_data['id'];
            $_data['id'] = '
                <a href="' . site_url('ami/potential_contacts/edit/' . $_data['id']) . '" data-toggle="ajaxModal">
                    ' . $_data['id'] . '
                </a>
            ';
            $_data['c']['primary_contact'] = ($_data['c']['primary_contact'] == 1) ? 'Yes' : 'No';
            $_data['buttons'] = '<div class="btn-group">';

           // $_data['buttons'] .= html_btn(site_url('ami/potential_contacts/amended/' . $id), '<i class="fa fa-history"></i>', array('class' => 'btn-default amended', 'title' => 'Amended', 'data-toggle' => 'ajaxModal'));

            if ($this->hasPermission('edit', 'potential_contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/potential_contacts/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'potential_contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/potential_contacts/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';

            if ($_data['c']['signature_base64']) {
                $_data['c']['signature_base64'] = '&#9989;';
            }
        }

        return $this->json($data);
    }


    public function draft()
    {
        $conditions = array('contacts.country_id' => $this->country_id, 'contacts.is_draft' => 1);

        //$this->data['contacts'] = $this->contacts_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['contacts'] = $this->_getListContacts($conditions);
        $this->data['total'] = $this->contacts_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/potential_contacts/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            'potential_customers' => $this->potential_customers_m->getCustomerListOptions(null, $conditions),
        );
    }

    public function check_primary_contact()
    {
        $customers_id = $this->input->get('customers_id');
        $result = false;
        if (isset($customers_id)) {
            $customers_id = explode(' - ', $customers_id);
            $customers_id = $customers_id['0'];
            $primary_contact = $this->potential_contacts_m->get_by(array(
                'armstrong_1_customers_id' => $customers_id,
                'primary_contact' => 1,
                'is_draft' => 0
            ));
            if (count($primary_contact) > 0) $result = true;
        }
        echo $result;
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'potential_contacts')) {
            return $this->noPermission();
        }

        // $this->data += $this->_getAddEditParams();
        if ($this->input->get('armstrong_2_customers_id')) {
            $this->data['potential_contacts']['armstrong_2_customers_id'] = $this->input->get('armstrong_2_customers_id');
        }
        $this->data['potential_contacts']['status'] = 1;
        return $this->render('ami/potential_contacts/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'potential_contacts') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/potential_contacts');

        if ($id = intval($id)) {
            $this->data['contacts'] = $this->potential_contacts_m->get($id);
            $this->data['contacts']['tag_id'] = $this->data['contacts']['armstrong_1_customers_id'];
            $this->data['images'] = $this->media_m->getMedia('image', 'potential_contacts', $this->country_id, $id);
            $this->data['page_title'] = page_title('Edit Potential Contacts');
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'potential_contacts')) {
                    return $this->noPermission();
                }
                return $this->render('ami/potential_contacts/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/potential_contacts/edit', $this->data);
            }
        }
        return redirect('ami/potential_contacts');
    }

    public function update()
    {
        if (!$this->hasPermission('edit', 'potential_contacts')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->contacts_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/potential_contacts');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'contacts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contacts');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/potential_contacts';
                //$this->contacts_m->delete($id);
                $this->potential_contacts_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/potential_contacts/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Contacts"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/potential_contacts');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'potential_contacts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/potential_contacts');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/potential_contacts/draft';
                $this->contacts_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/potential_contacts/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Contacts"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/potential_contacts');
    }

    public function amended($id = null)
    {
        $this->data['amendedData'] = $this->_getAmenedData($id);
        $this->data['contactId'] = $id;
        return $this->render('ami/potential_contacts/amended', $this->data);
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'potential_contacts') && !$this->hasPermission('edit', 'potential_contacts')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $fields = array('ext', 'name', 'position', 'phone', 'phone_2', 'phone_3', 'mobile', 'mobile_2', 'mobile_3', 'status', 'primary_contact', 'armstrong_1_customers_id',
            'tag_id', 'fax', 'email', 'email_2', 'birthday', 'interests', 'remarks', 'id', 'facebook', 'instagram', 'linkedln', 'signature_base64');
        $data = $this->potential_contacts_m->array_from_post($fields);

        $data['country_id'] = $this->country_id;
        // $data['birthday'] = date("Y-m-d", strtotime($data['birthday']));
//        $data['birthday'] = Carbon::parse($data['birthday'])->toDateString();

        $id = $data['id'] ? $data['id'] : null;
        // Only MY can change signature
        if ($this->country_id != 1) {
            unset($data['signature_base64']);
        }
        if (!empty($data['tag_id'][0])) {
            $data['armstrong_1_customers_id'] = $data['tag_id'][0];
            $data['type'] = 0;
        } else if (!empty($data['tag_id'][1])) {
            $data['armstrong_1_customers_id'] = $data['tag_id'][1];
            $data['type'] = 1;
        } else if (!empty($data['tag_id'][2])) {
            $data['armstrong_1_customers_id'] = $data['tag_id'][2];
            $data['type'] = 2;
        }
        unset($data['tag_id']);
        // Convert string to uppercase
        foreach ($data as $key => $_data) {
            if (is_string($_data) && $key != 'signature_base64')
                $data[$key] = strtoupper($_data);
        }
        // Update primary contact
        if ($data['primary_contact'] == 1) {
            if ($id) {
                $condition = array('armstrong_1_customers_id' => $data['armstrong_1_customers_id'], "id != {$id}" => null);
            } else {
                $condition = array('armstrong_1_customers_id' => $data['armstrong_1_customers_id']);
            }
            $sameContact = $this->potential_contacts_m->get_by($condition, false, null, '*', null);
            if ($sameContact) {
                foreach ($sameContact as $_contact) {
                    $newId = $this->potential_contacts_m->save(array('primary_contact' => 0), $_contact['id']);
                }
            }
        }

        $newId = $this->potential_contacts_m->save($data, $id);
     //   dd($newId);
        // Update existing record
        if ($id) {
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('potential_contacts', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');

        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'contacts',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('name' => $this->input->post('uniqueId')));

            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'contacts';
                    $args['country_id'] = $this->country_id;

                    $this->media_m->addMediaRef($args);
                }
            }
        }

        return redirect('ami/potential_contacts');
    }

    public function primary($id = null)
    {
        $customerId = $this->input->get('armstrong_2_customers_id');

        $this->_assertId($id, 'ami/potential_contacts');

        if (!$customerId) {
            return redirect(site_url('ami/potential_contacts/contacts/' . $customerId));
        }

        $this->db->where('armstrong_2_customers_id', $customerId)
            ->set(array('primary_contact' => 0))
            ->update('contacts');

        $this->contacts_m->save(array(
            'primary_contact' => 1
        ), $id);

        return redirect(site_url('ami/customers/potential_contacts/' . $customerId));
    }

    public function _getAmenedData($contactId)
    {
        $data = $this->db->where(array(
            'country_id' => $this->country_id,
            'contact_id' => $contactId,
            'is_draft' => 0
        ))
            ->get('contacts_amended_data')->row_array();

        if ($data) {
            $data['old_data'] = !empty($data['contact_old_data']) ? json_decode($data['contact_old_data'], true) : array();
            $data['new_data'] = !empty($data['contact_new_data']) ? json_decode($data['contact_new_data'], true) : array();

            $data['fields'] = $data['old_data'] ? array_keys($data['old_data']) : array();
        }

        return $data;
    }
    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->potential_contacts_m->importToDb($this->country_id, $worksheet->toArray(), array('id'), 'potential_contacts');
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }
}