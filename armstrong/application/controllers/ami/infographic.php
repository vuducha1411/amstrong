<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Infographic extends AMI_Controller
{
    public $month;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Infographic_model');

        $this->data['page'] = array(
            'scripts' => array(
                'res/js/jquery.min.js',
                'res/js/bootstrap.min.js',
                'res/js/jquery.circliful.min.js',
                // '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'
                'res/js/infographic.js'
            ),
            'stylesheets' => array(
                'res/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap.min.css',
//                'res/lib/bootstrapModal/css/bootstrap-theme.min.css',
                // 'res/css/bootstrap-topnav-fix.css',
                // 'res/lib/bootstrap2.3.2/css/bootstrap-responsive.min.css',
                // 'res/css/infographic.css',
                // 'res/css/admin.css'
                'res/css/infographic/style.css'
            )
        );

        if ($this->session->userdata('infographicCountry')) {
            $this->country_code = $this->session->userdata('infographicCountry');

            $_countries = array_rewrite($this->countries, 'code');
            $this->country_id = $_countries[$this->session->userdata('infographicCountry')]['id'];
            $this->country_name = $_countries[$this->session->userdata('infographicCountry')]['name'];
        } else {
            $this->country_code = $this->session->userdata('country');
        }

        $countryCurrency = $this->db->select('currency')->from('country')->where(array('code' => $this->country_code))->get()->row();
        $this->country_currency = @$countryCurrency->currency;

        $this->data['infographicMonth'] = $infographicMonth = $this->session->userdata('infographicMonth');
        $this->data['infographicYear'] = $infographicYear = $this->session->userdata('infographicYear');
        $this->data['infographicAction'] = $infoAction = $this->session->userdata('infographicAction');

        if (!$infographicMonth || !$infographicYear /*|| in_array($this->country_code, array('ph'))*/) {
            return redirect('/');
        }

        $this->load->model('Infographic_model');
        // $this->_getData();

        // Set infographic map for countries
        $this->data['country_map'] = 'res/img/infographic/country_map/' . $this->country_code . '.png';

        // $this->data['infographic'][Infographic_model::GRIP_GRAB] = $this->Infographic_model->getData(Infographic_model::GRIP_GRAB, null, array('countryId' => $this->country_id));
    }

    protected function _getData($tableType = null)
    {
        $this->data['is_print'] = false;
        $this->data['infographic']['sl_team'] = $this->Infographic_model->getReportAllInOne($this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);
        $this->data['infographic'][Infographic_model::CALL_RECORD] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);

        $richMedia = $this->Infographic_model->getData(Infographic_model::TYPE, null,
            array('groupsTypes' => 1, 'countryId' => $this->country_id), $this->data['infographicMonth'], $this->data['infographicYear']);
        $this->data['infographic'][Infographic_model::TYPE]['media'] = array_rewrite($richMedia, 'typeId');
        $this->data['infographic'][Infographic_model::DATA]['sampling'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear'], array('totalCustomerHaveSampled', 'totalSamplingItems', 'totalSamplingTimes', 'totalSamplingItemsDry', 'totalSamplingItemsWet', 'totalSamplingItemsCd', 'totalCustomerHavePantryCheck', 'totalPantryCheckItems', 'totalPantryCheckTimes'));
        if ($tableType) {
            $this->Infographic_model->setTableType($tableType);
        }
        $this->data['infographic'][Infographic_model::DATA]['cus_otm'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);
        $customerData = $this->Infographic_model->getCustomerData($this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);
        $topCustomerData = array_slice($customerData, 0, 3);
        // 12.10.2014
        if (count($topCustomerData) < 4) {
            for ($i = count($topCustomerData); $i < 4; $i++) {
                $topCustomerData[$i] = array(
                    'typeName' => '',
                    'quantity' => 0,
                    'valueData' => 0
                );
            }
        }

        $customerOther = $this->Infographic_model->getOtherCustomerData(array_keys(array_rewrite($topCustomerData, 'id')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);
        $this->data['infographic'][Infographic_model::TYPE]['top3'] = $topCustomerData;
        $this->data['infographic'][Infographic_model::TYPE]['other'] = $customerOther;

        // Grip Grab
        $gripGrab = $this->Infographic_model->getGripGrab($this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);

        $topGripGrab = array_slice($gripGrab, 0, 3);
        $other = $this->Infographic_model->getOtherGripGrab(array_keys(array_rewrite($topGripGrab, 'id')), $this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);

        $this->data['infographic'][Infographic_model::GRIP_GRAB]['top3'] = $topGripGrab;
        $this->data['infographic'][Infographic_model::GRIP_GRAB]['other'] = $other;

        // Top SKU
        $topSku = $this->Infographic_model->getTopSku($this->country_id, $this->data['infographicMonth'], $this->data['infographicYear']);
//            $this->Infographic_model->getTopSku($this->country_id, 2, $this->data['infographicMonth'], $this->data['infographicYear']),
//            $this->Infographic_model->getTopSku($this->country_id, 3, $this->data['infographicMonth'], $this->data['infographicYear']),
//            $this->Infographic_model->getTopSku($this->country_id, array(1,2,3), $this->data['infographicMonth'], $this->data['infographicYear']),


        $topSkuWithChild = [];
        foreach ($topSku as &$_sku) {
            if($_sku['mainSku']){
                if ($_sku['skuNumber'] == $_sku['mainSku']) {
                    $topSkuWithChild[$_sku['mainSku']]['main'] = $_sku;
                }
                $topSkuWithChild[$_sku['mainSku']]['child'][] = $_sku;
            }else{
                $topSkuWithChild[$_sku['skuNumber']] = $_sku;
            }
        }

        $this->data['infographic'][Infographic_model::TOP_SKU]['top3'] = $topSkuWithChild;
        $totalCustomers = array_only($this->data['infographic'][Infographic_model::DATA]['cus_otm'], array('customerOtmA', 'customerOtmB', 'customerOtmC', 'customerOtmD', 'customerOtmU'));
        $this->data['infographic']['totalCustomers'] = array_sum($totalCustomers);


//        $infoData = $this->data['infographic'][Infographic_model::DATA]['cus_otm'];
        $infoData = $this->data['infographic']['sl_team'];
        $data = array_only($infoData, array('customerOtmA', 'customerOtmB', 'customerOtmC', 'customerOtmD', 'customerOtmU'));
//        asort($data);

//        $total = array_sum($data) + $infoData['customerOtmU'];
        $total = isset($this->data['infographic']['sl_team']['totalGrip']) ? $this->data['infographic']['sl_team']['totalGrip'] : 0;
        $otm = array();

        foreach ($data as $key => $value) {
            $otm[] = array(
                'label' => substr($key, -1),
                'value' => $value,
                'percentage' => ($value > 0 && $total > 0) ? $value / $total * 100 : 0
            );
        }

        $this->data['infographic'][Infographic_model::DATA]['otm'] = array(
            'total' => $total,
            'otm' => $otm
        );

        $this->data['country_name'] = $this->country_name;

        $this->data['countryCurrency'] = $this->db->select('currency')->from('country')->where(array('name' => $this->country_name))->get()->row();

        $this->country_currency = @$this->data['countryCurrency']->currency;
        // if (in_array($this->country_currency, array('au', 'nz', 'sa', 'hk')))
        // {
        //     $this->data['countryCurrency'] = '$';
        // }
        // prd($this->data);
    }

    public function index()
    {

    }

    public function ssd()
    {
        //$this->Infographic_model->setTableType('ssd');
        $this->_getData('ssd');
        $this->data['page']['title'] = 'Infographic';
        $this->data['type'] = 'ssd';
        $this->data['subView'] = 'infographic/home_ssd';
        switch ($this->data['infographicAction']) {
            case 'exportPDF':
                $this->data['is_print'] = true;
                // TODO load html and print with window.print();
                return $this->load->view('infographic/home_ssd', $this->data);

//				$pdf = new TCPDF('L', 'mm', 'A3', true, 'UTF-8', false);
//				$pdf->SetTitle('Infographic Report');
//				$pdf->SetHeaderMargin(30);
//				$pdf->SetTopMargin(20);
//				$pdf->setFooterMargin(20);
//				$pdf->SetAutoPageBreak(true);
//				$pdf->SetAuthor('UFS Armstrong');
//				$pdf->SetDisplayMode('real', 'default');
////              $content = htmlentities($this->load->view('infographic/pdf_home', $this->data, TRUE));
//				$content = $this->load->view('infographic/pdf_home', $this->data, TRUE);
////              $this->load->view('infographic/pdf_home', $this->data, TRUE);
////              $content = ob_get_contents();
////              ob_end_clean();
//				@$pdf->writeHTML($content, true, false, true, false, '');
//				$pdf->Output('test.pdf', 'D');

                dd('Finished');
                break;
            case 'generate':
                return $this->load->view('infographic/base', $this->data);
                break;
        }
    }

    public function tfo()
    {
        $this->_getData();

        $this->data['page']['title'] = 'Infographic';
        $this->data['type'] = 'tfo';
        $this->data['subView'] = 'infographic/home';
        switch ($this->data['infographicAction']) {
            case 'exportPDF':
                $this->data['is_print'] = true;
                // TODO load html and print with window.print();
                return $this->load->view('infographic/home', $this->data);

//				$pdf = new TCPDF('L', 'mm', 'A3', true, 'UTF-8', false);
//				$pdf->SetTitle('Infographic Report');
//				$pdf->SetHeaderMargin(30);
//				$pdf->SetTopMargin(20);
//				$pdf->setFooterMargin(20);
//				$pdf->SetAutoPageBreak(true);
//				$pdf->SetAuthor('UFS Armstrong');
//				$pdf->SetDisplayMode('real', 'default');
////              $content = htmlentities($this->load->view('infographic/pdf_home', $this->data, TRUE));
//				$content = $this->load->view('infographic/pdf_home', $this->data, TRUE);
////              $this->load->view('infographic/pdf_home', $this->data, TRUE);
////              $content = ob_get_contents();
////              ob_end_clean();
//				@$pdf->writeHTML($content, true, false, true, false, '');
//				$pdf->Output('test.pdf', 'D');

                dd('Finished');
                break;
            case 'generate':
                return $this->load->view('infographic/base', $this->data);
                break;
        }
    }

    public function home($id = null)
    {
        if (!$id) {
            return redirect(site_url('infographic'));
        }

        $this->data['page']['title'] = 'Infographic';
        $this->data['subView'] = 'infographic/home_' . $id;

        return $this->load->view('infographic/base', $this->data);
    }

    public function cal_data_bl1()
    {
        $this->data['page']['title'] = 'Call Data Total';
        $get = $this->input->get();
//        if (isset($get['type']) && $get['type'] == 'ssd') {
//            $this->Infographic_model->setTableType('ssd');
//        }
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);

        return $this->load->view('infographic/call_data_bl1', $this->data);
    }

    public function cal_data_bl2()
    {
        $this->data['page']['title'] = 'Call Data';
        $get = $this->input->get();
//        if (isset($get['type']) && $get['type'] == 'ssd') {
//            $this->Infographic_model->setTableType('ssd');
//        }
        $this->_getData();

        return $this->load->view('infographic/call_data_bl2', $this->data);
    }

    public function cal_data_bl3()
    {
        $this->data['page']['title'] = 'Call Data';
        $get = $this->input->get();
//        if (isset($get['type']) && $get['type'] == 'ssd') {
//            $this->Infographic_model->setTableType('ssd');
//        }
        $this->_getData();

        return $this->load->view('infographic/call_data_bl3', $this->data);
    }

    public function call_data_total()
    {
        $this->data['page']['title'] = 'Call Data Total';
        $this->data['subView'] = 'infographic/call_data_total';
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);

        return $this->load->view('infographic/call_data_total', $this->data);
    }

    public function call_data_1()
    {
        $this->data['page']['title'] = 'Call Data';
        $this->_getData();
        return $this->load->view('infographic/call_data_1', $this->data);
    }

    public function call_data_2()
    {
        $this->data['page']['title'] = 'Call Data';
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);

        return $this->load->view('infographic/call_data_2', $this->data);
    }

    public function call_data_3()
    {
        $this->data['page']['title'] = 'Call Data';
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);

        return $this->load->view('infographic/call_data_3', $this->data);
    }

    public function strike_rate()
    {
        $this->data['page']['title'] = 'Call Data';
//        $this->_getData();
        $this->data['infographic'][Infographic_model::CALL_RECORD] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array('appType' => 0), $this->data['infographicMonth'], $this->data['infographicYear']);
//		dd($this->data['infographic']);
        return $this->load->view('infographic/call_data_strike_rate', $this->data);
    }

    public function rich_media()
    {
        $this->data['page']['title'] = 'Rich Media';
        $this->data['subView'] = 'infographic/rich_media';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
           // $this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
        }
        else
        {
            $this->_getData();
        }
//        dd($this->data['infographic']);
        return $this->load->view('infographic/rich_media', $this->data);
    }

    public function rich_media_1()
    {
        $this->data['page']['title'] = 'Rich Media';
        $this->data['subView'] = 'infographic/rich_media_1';
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);
        return $this->load->view('infographic/rich_media_1', $this->data);
    }

    public function rich_media_2()
    {
        $this->data['page']['title'] = 'Rich Media';
        $infographic = $this->Infographic_model->getData(Infographic_model::TYPE, null,
            array('groupsTypes' => 1, 'countryId' => $this->country_id), $this->data['infographicMonth'], $this->data['infographicYear']);

        $this->data['infographic'] = array_rewrite($infographic, 'typeId');

//        $this->data['subView'] = 'infographic/rich_media_2';
        return $this->load->view('infographic/rich_media_2', $this->data);
    }

    public function sales_performance_1()
    {
        $this->data['page']['title'] = 'Sales Performance';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
            $this->Infographic_model->setTableType('ssd');
        }
        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);
        return $this->load->view('infographic/sales_performance_1', $this->data);
    }

    public function sales_performance_2()
    {
        $this->data['page']['title'] = 'Sales Performance';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
            //$this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
        }
        else
        {
            $this->_getData();
        }
        return $this->load->view('infographic/sales_performance_2', $this->data);
    }

    public function sales_team()
    {
        $this->data['page']['title'] = 'Sales Team';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
            //$this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
        }
        else
        {
            $this->_getData();
        }
//        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id);

        return $this->load->view('infographic/sales_team', $this->data);
    }

    public function top_sku()
    {
        $this->data['page']['title'] = 'Top SKU';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
            //$this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
        }
        else
        {
            $this->_getData();
        }
//        $this->data['infographic'] = $this->Infographic_model->getData(Infographic_model::CALL_RECORD, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);

        return $this->load->view('infographic/top_sku', $this->data);
    }

    public function customer_data()
    {
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
           // $this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
        }
        else
        {
            $this->_getData();
        }
        $this->data['page']['title'] = 'Customer Media';

        $infographic = $this->Infographic_model->getData(Infographic_model::DATA, $this->country_id, array(), $this->data['infographicMonth'], $this->data['infographicYear']);

        $infoData = $this->data['infographic']['sl_team'];
        $data = array_only($infoData, array('customerOtmA', 'customerOtmB', 'customerOtmC', 'customerOtmD', 'customerOtmU'));
//        asort($data);

        // $total = array_sum($data) + $infographic['customerOtmU'];
        $total = isset($this->data['infographic']['sl_team']['totalGrip']) ? $this->data['infographic']['sl_team']['totalGrip'] : 0;
        $otm = array();

        foreach ($data as $key => $value) {
            $otm[] = array(
                'label' => substr($key, -1),
                'value' => $value,
                'percentage' => ($value > 0 && $total > 0) ? $value / $total * 100 : 0
            );
        }

        $infographic['total'] = $total;

        $this->data['infographic'] = $infographic;
        $this->data['otm'] = $otm;

        //var_dump($this->data);die;

        return $this->load->view('infographic/customer_data', $this->data);
    }

    public function customer_type()
    {
        $this->data['page']['title'] = 'Customer Type';
        $get = $this->input->get();
        if (isset($get['type']) && $get['type'] == 'ssd') {
            //$this->Infographic_model->setTableType('ssd');
            $this->_getData('ssd');
            $this->data['isssd'] = true;
        }
        else
        {
            $this->_getData();
        }
        return $this->load->view('infographic/customer_type', $this->data);
    }

    public function grow_chart()
    {
        $this->data['page']['title'] = 'Call Data';
        $this->_getData();
        return $this->load->view('infographic/grow_chart', $this->data);
    }
}