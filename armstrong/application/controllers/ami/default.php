<?php

use Eloquent\Salesperson;

class Home extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('salespersons_m');
        $this->load->model('roles_m');
        $this->load->model('customers_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'salespersons')) {
            return $this->noPermission();
        }

        $this->data['spFilter'] = $this->input->get('filter') ? '?filter=' . $this->input->get('filter') : false;
        $this->data['filter'] = $this->input->get('filter');

        return $this->render('ami/salespersons/index', $this->data);
    }

    public function ajaxData()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        switch ($this->input->get('filter')) 
        {
            case 'inactive':
                $conditions['active'] = 0;
                break;
            
            default:
                $conditions['active'] = 1;
                break;
        }

        // get roles for sales and salesmanage from country
        $roles_salesmanager = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $roles_salesperson = $this->roles_m->getRoleIdByRolename($this->country_id, 'Salesperson');
        // get user from session
        $user = $this->session->userdata('user_data');
        // get admins_id from salesperson table
        $where['armstrong_2_salespersons_id'] = $user['id'];
        $admins_id_sales = $this->salespersons_m->get_by($where, FALSE, NULL, array('admins_id', 'armstrong_2_salespersons_id'));
        // check type role of user
        if ($user['roles_id'] == $roles_salesmanager) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'salespersons_manager_id' => $admins_id_sales[0]['armstrong_2_salespersons_id'], 'active' => 1);
        } else if ($user['roles_id'] == $roles_salesperson) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $user['id'], 'active' => 1);
        }

        $datatables = new Datatable(array('model' => 'Salespersons_dt', 'rowIdCol' => $this->salespersons_m->getTablePrimary()));
        $this->salespersons_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['armstrong_2_salespersons_id'];
            $_data['armstrong_2_salespersons_id'] = '
                <a href="' . site_url('ami/salespersons/edit/' . $_data['armstrong_2_salespersons_id']) . '" data-toggle="ajaxModal">
                    ' . $_data['armstrong_2_salespersons_id'] . '
                </a>
            ';

            $_data['buttons'] = '<div class="btn-group">';


            if ($this->hasPermission('edit', 'salespersons')) {
                $_data['buttons'] .= html_btn(site_url('ami/salespersons/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'salespersons')) {
                $_data['buttons'] .= html_btn(site_url('ami/salespersons/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'salespersons')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $persons = $this->salespersons_m->get_limit($this->getPageLimit(), $conditions);

        $this->data['persons'] = $this->salespersons_m->preparePersons($persons);
        $this->data['total'] = $this->salespersons_m->count('where', $conditions);
        $this->data['draft'] = TRUE;

        return $this->render('ami/salespersons/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        // tuantq
        $role_id = $this->roles_m->getRoleIdByRolename($this->country_id, 'Sales Manager');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $conditions_managers = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, 'roles_id' => $role_id);
        $cols = array('id', 'name');
        return array(
            // 'roles' => $this->roles_m->parse_form($cols, '- Select -', $conditions),
            'roles' => $this->roles_m->get_by($conditions),
            //'managers' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions_managers),
            'managers' => $this->salespersons_m->get_by($conditions_managers),
            'types' => array('PULL' => 'PULL', 'PUSH' => 'PUSH', 'SL' => 'SL', 'ADMIN' => 'ADMIN', 'MARKETING' => 'MARKETING',),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'salespersons')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
//        tuantq
        $this->data['person']['active'] = 1;
        return $this->render('ami/salespersons/edit', $this->data);
    }

    public function edit($id = NULL)
    {
        if (!$this->hasPermission('edit', 'salespersons') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            $person = $this->salespersons_m->get($id);
            $this->data['person'] = $this->salespersons_m->preparePerson($person);
            $this->assertCountry($this->data['person']);
            $this->data['page_title'] = page_title('Edit Salespersons');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'salespersons')) {
                    return $this->noPermission();
                }

                return $this->render('ami/salespersons/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                //dd($this->data['managers']);
                return $this->render('ami/salespersons/edit', $this->data);
            }
        }

        return redirect('ami/salespersons');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->salespersons_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/salespersons');
    }

    public function delete($id = NULL)
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            if ($this->is('POST', FALSE)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/salespersons';
                //$this->salespersons_m->delete($id);
                $this->salespersons_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/salespersons/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Salespersons"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/salespersons');
    }

    public function restore($id = NULL)
    {
        if (!$this->hasPermission('delete', 'salespersons')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/salespersons');

        if ($id) {
            if ($this->is('POST', FALSE)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/salespersons/draft';
                $this->salespersons_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/salespersons/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Salespersons"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/salespersons');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'salespersons') && !$this->hasPermission('edit', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->salespersons_m->array_from_post(array('first_name', 'last_name', 'password', 'email', 'alias', 'depth',
            'roles_id', 'type', 'territory', 'handphone', 'did', 'salespersons_manager_id', 'persons', 'armstrong_2_salespersons_id', 'active'));

        $data['country_id'] = $this->country_id;

        $id = $data['armstrong_2_salespersons_id'] ? $data['armstrong_2_salespersons_id'] : NULL;

        if ($data['depth'] == 1) {
            $data['manager_id'] = -1;
        }
        unset($data['depth']);

        $persons = $data['persons'] ? $data['persons'] : FALSE;
        unset($data['persons']);

        $newId = $this->salespersons_m->saveEntry($data, $id);

        if ($persons) {
            $this->salespersons_m->setManager($persons, $newId);
        }

        $this->salespersons_m->syncManagerId($newId, $data['salespersons_manager_id']);

        return redirect('ami/salespersons');
    }

    public function roles()
    {
        if (!$this->hasPermission('add', 'salespersons') || !$this->hasPermission('edit', 'salespersons')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $parentId = $this->input->post('parent');
        $id = $this->input->post('id');
        $editId = $this->input->post('edit');

        if ($parentId) {
            $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

            // $roles = $this->roles_m->get_by(array_merge($conditions, array('depth <' => $depth)));

            // $depth = $depth - 1 > 0 ? $depth - 1 : 1;

            $roleUp = $this->roles_m->get_by(array_merge($conditions, array('id' => $parentId)), TRUE);
            $roleDown = $this->roles_m->get_by(array_merge($conditions, array('parent_id' => $id)));
            $roleDown = array_rewrite($roleDown, 'id');

            $managers = $persons = array();

            if ($roleUp) {
                if ($editId) {
                    $managers = $this->salespersons_m->get_by(array(
                        'roles_id' => $roleUp['id'],
                        'armstrong_2_salespersons_id !=' => $editId
                    ));
                } else {
                    $managers = $this->salespersons_m->get_by(array('roles_id' => $roleUp['id']));
                }
            }

            if ($roleDown) {
                $_persons = $this->salespersons_m->get_in('roles_id', array_keys($roleDown));

                foreach ($_persons as $person) {
                    if ($person['armstrong_2_salespersons_id'] != $editId) {
                        $persons[] = array(
                            'value' => $person['armstrong_2_salespersons_id'],
                            'label' => $person['first_name'] . ' ' . $person['last_name']
                        );
                    }
                }
            }

            $params = array(
                'managers' => $managers,
                'persons' => $persons
            );

            if ($editId) {
                $params['salesperson'] = $this->salespersons_m->get($editId, FALSE);

                $_managers = $this->salespersons_m->get_by(array('salespersons_manager_id' => $editId));
                $_managers = array_rewrite($_managers, 'armstrong_2_salespersons_id');

                $params['salesperson']['managers'] = implode(',', array_keys($_managers));
            }

            return $this->render('ami/salespersons/manager', $params);
        }

        return '';
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'salespersons')
        ) {
            return redirect(site_url('ami/salespersons'));
        }

        $salespersons = $this->salespersons_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($salespersons);

        return $this->excel($sheet, 'salespersons', $type);
    }

    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->salespersons_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => TRUE,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => FALSE,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function validate()
    {
        $get = $this->input->get();

        if ($salesperson = $this->salespersons_m->get_by(array_except($get, array('armstrong_2_salespersons_id')), TRUE)) {
            if (!empty($get['armstrong_2_salespersons_id'])) {
                // Edit data, ignore this case
                if ($salesperson['armstrong_2_salespersons_id'] == $get['armstrong_2_salespersons_id']) {
                    return $this->json(array('validate' => 'success'));
                }
            }

            return show_error('validate error', 404);
        }

        return $this->json(array('validate' => 'success'));
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term) {
            return $this->json(array());
        }
        $conditions = array("first_name LIKE '%{$term}%'", "last_name LIKE '%{$term}%'", "armstrong_2_salespersons_id LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        $salespersons = $this->db
            ->where('country_id', $this->country_id)
            // ->or_like('first_name', $term)
            // ->or_like('last_name', $term)
            ->where($conditions)
            ->get('salespersons')
            ->result_array();

        $output = array();

        foreach ($salespersons as $salesperson) {
            $output[] = array(
                'value' => $salesperson['armstrong_2_salespersons_id'],
                // 'label' => $salesperson['armstrong_2_salespersons_id'],
                'label' => $salesperson['armstrong_2_salespersons_id'] . ' - ' . $salesperson['first_name'] . ' ' . $salesperson['last_name']
            );
        }

        return $this->json($output);
    }

    public function fetch_customer()
    {
        $id = $this->input->post('id');

        if (!$id) {
            return $this->json(array());
        }

        $customers = $this->customers_m->get_by(array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            'armstrong_2_salespersons_id' => $id,
            'approved' => 1,
            'active' => 1
        ));

        return $this->json($customers);
    }

    public function fetch_salesperson()
    {
        $post = $this->input->post();

        $appType = isset($post['app_type']) ? strtolower($post['app_type']) : '';

        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            // 'listed' => 1,
        );

        if (isset($post['app_type']) && is_numeric($post['active']) && $post['active'] >= 0)
        {
            $conditions['active'] = $post['active'];
        }

        if ($appType == 'sl')
        {
            $conditions['type LIKE'] = '%sl%';
        }

        if (in_array($appType, ['pull', 'push']))
        {
            $conditions['(type LIKE "%'.$appType.'%" OR type LIKE "%mix%")'] = null;
        }

        $sales = $this->salespersons_m->getPersonListOptions(null, $conditions, true);

        $html = $sales ? '<option value="" selected="selected">ALL</option>': '';
        
        foreach($sales as $k=>$s)
        {
            $html .= '<option value="'.$k.'">'.$s.'</option>';
        }

        echo $html;die;
    }
}