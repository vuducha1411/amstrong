<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Perfect_store extends AMI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('perfect_store_content_m');
        $this->load->model('perfect_store_m');
        $this->load->model('perfect_store_cc_m');
        $this->load->model('perfect_store_groups_m');
        $this->load->model('products_m');
        $this->load->model('call_records_m');
        $this->load->model('brands_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $this->data['stores'] = $this->perfect_store_m->get_by($conditions);
        // $this->data['total'] = $this->perfect_store_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Perfect Store');

        return $this->render('ami/perfect_store/index', $this->data);
    }

    public function ajaxData()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $datatables = new Datatable(array('model' => 'Perfect_store_dt', 'rowIdCol' => $this->perfect_store_m->getTablePrimary()));

        $this->perfect_store_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];
            $_data['id'] = '
                <a href="' . site_url('ami/perfect_store/edit/' . $_data['id']) . '" data-toggle="ajaxModal">
                    ' . $_data['id'] . '
                </a>
            ';

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'wholesalers')) {
                $_data['buttons'] .= html_btn(site_url('ami/perfect_store/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'wholesalers')) {
                $_data['buttons'] .= html_btn(site_url('ami/perfect_store/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';

            switch ($_data['type']) {
                case 0:
                    $_data['type'] = 'Product';
                    break;
                case 1:
                    $_data['type'] = 'Place';
                    break;
                case 2:
                    $_data['type'] = 'Promotion';
                    break;

            }
        }

        return $this->json($data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['stores'] = $this->perfect_store_m->get_by($conditions);
        // $this->data['total'] = $this->perfect_store_m->count('where', $conditions);
        $this->data['draft'] = true;

        return $this->render('ami/perfect_store/index', $this->data);
    }

    public function template()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $storeContent = $this->perfect_store_content_m->get_by($conditions);
        $this->data['storeContent'] = $this->perfect_store_content_m->prepareContent($storeContent);
        $this->data += $this->_getAddEditParams();

        return $this->render('ami/perfect_store/template');
    }

    public function cc_template()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $ccContent = $this->perfect_store_cc_m->get_by($conditions);
        $this->data['ccContent'] = $this->perfect_store_cc_m->prepareContent($ccContent);
        $this->data += $this->_getAddEditParams();
        return $this->render('ami/perfect_store/cc_templates');
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            'products' => $this->products_m->getProductListOptions('- Select -', array_merge(array('listed' => 1), $conditions), 'id'),
            'brands' => $this->brands_m->getBrandListOptions('- Select -', array('is_draft' => 0), 'id'),
            'groups' => $this->perfect_store_groups_m->getGroupsListOptions('- Select -', array('is_draft' => 0), 'id')
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'perfect_store'))
        {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);
        $existingCallRecords = $this->db->select('armstrong_2_call_records_id')
            ->from($this->perfect_store_m->getTableName())
            ->where($conditions)
            ->get()->result_array();
        $existingCallRecords = array_rewrite($existingCallRecords, 'armstrong_2_call_records_id');
        $callRecords = $this->call_records_m->getCallRecordListOptions('- Select -', $conditions);
        $this->data['callRecords'] = array_except($callRecords, array_keys($existingCallRecords));
        $this->data += $this->_getAddEditParams();
        return $this->render('ami/perfect_store/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'perfect_store') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/perfect_store');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);
        $existingCallRecords = $this->db->select('armstrong_2_call_records_id')
            ->from($this->perfect_store_m->getTableName())
            ->where($conditions)
            ->get()->result_array();
        $existingCallRecords = array_rewrite($existingCallRecords, 'armstrong_2_call_records_id');
        $callRecords = $this->call_records_m->getCallRecordListOptions('- Select -', $conditions);
        $this->data['callRecords'] = array_except($callRecords, array_keys($existingCallRecords));
        if ($id)
        {
            $this->data['perfectStore'] = $this->perfect_store_m->get($id);
            $this->assertCountry($this->data['perfectStore']);

            $this->data['perfectStore']['active'] = json_decode($this->data['perfectStore']['perfect_store_content_ids']);

            $this->data['page_title'] = page_title('Edit Perfect Store');

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'perfect_store'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/perfect_store/preview', $this->data);
            }
            else
            {
                $this->data += $this->_getAddEditParams();
                return $this->render('ami/perfect_store/edit', $this->data);
            }
        }

        return redirect('ami/perfect_store');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id)
        {
            if ($id)
            {
                $this->perfect_store_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/perfect_store');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/perfect_store');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/perfect_store';
                //$this->salespersons_m->delete($id);
                $this->perfect_store_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/perfect_store/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Perfect Store"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/perfect_store');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/perfect_store');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/perfect_store/draft';
                $this->perfect_store_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/perfect_store/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Perfect Store"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/perfect_store');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'perfect_store') && !$this->hasPermission('edit', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->salespersons_m->array_from_post(array('active', 'armstrong_2_call_records_id', 'armstrong_2_salespersons_id', 'id'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        $data['active'] = $data['active'] ? $data['active'] : array();
        $data['perfect_store_content_ids'] = json_encode($data['active']);
        unset($data['active']);

        $this->perfect_store_m->save($data, $id);

        return redirect('ami/perfect_store');
    }

    public function save_template()
    {
        if (!$this->hasPermission('add', 'perfect_store') && !$this->hasPermission('edit', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $_types = $this->perfect_store_content_m->types;

        foreach ($_types as $key => $_type)
        {
            $_types[] = "{$_type}_delete";
        }

        $data = $this->getInput($_types);

        $types = array_flip($_types);

        $this->db->trans_start();

        // $this->db->from($this->perfect_store_content_m->getTableName())
        //     ->where('country_id', $this->country_id)
        //     ->delete();

        foreach ($data as $key => $values)
        {
            if (in_array($key, $this->perfect_store_content_m->types))
            {
                foreach ($values as $value)
                {
                    $value['country_id'] = $this->country_id;
                    $value['type'] = $types[$key];
                    $id = !empty($value['id']) ? $value['id'] : null;
                    $this->perfect_store_content_m->save($value, $id);
                }
            }
            else if (strpos($key, '_delete') !== false)
            {
                foreach ($values as $value)
                {
                    $this->perfect_store_content_m->save(array(
                        'is_draft' =>1
                    ), $value);
                }
            }
        }

        $this->db->trans_complete();

        return redirect('ami/perfect_store');
    }

    public function save_cc_template()
    {
        if (!$this->hasPermission('add', 'perfect_store') && !$this->hasPermission('edit', 'perfect_store'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $_types = $this->perfect_store_cc_m->types;
        foreach ($_types as $key => $_type)
        {
            $_types[] = "{$_type}_delete";
        }

        $data = $this->getInput($_types);
        $types = array_flip($_types);

        $this->db->trans_start();
        foreach ($data as $key => $values)
        {
            if (in_array($key, $this->perfect_store_cc_m->types))
            {
                if($key == ['price']) dd($values);
                foreach ($values as $value)
                {
                    if($value['group_1'] && $value['title'] != '- Select -'){
                        if(isset($value['group_2'])){
                            $value['group_2'] = $value['group_2'] == '- Select -' ? '' : $value['group_2'];
                        }
                        $value['country_id'] = $this->country_id;
                        $value['type'] = $types[$key];
                        $value['order_number'] = $this->formatOrder($value['order_number']);
                        $id = !empty($value['id']) ? $value['id'] : null;
                        $this->perfect_store_cc_m->save($value, $id);
                    }
                }
            }
            else if (strpos($key, '_delete') !== false)
            {
                foreach ($values as $value)
                {
                    $this->perfect_store_cc_m->save(array(
                        'is_draft' =>1
                    ), $value);
                }
            }
        }

        $this->db->trans_complete();

        return redirect('ami/perfect_store');
    }

    function formatOrder($order){
        $output = $order;
        $explode_str = explode('.', $order);
        if($explode_str){
            foreach($explode_str as &$str){
                if(strlen($str) == 1){
                    $str = '0'.$str;
                }
            }
            $output = implode('.', $explode_str);
        }
        return $output;
    }
}