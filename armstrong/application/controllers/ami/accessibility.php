<?php

/**
 * User: vietthang
 * Date: 10/27/14
 * Time: 11:31 AM
 */
class Accessibility extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('roles_m');
        $this->load->model('menu_m');
        $this->load->model('salespersons_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'accessibility')) {
            return $this->noPermission();
        }

        $conditions = array("(country_id = {$this->country_id} OR global = 1)" => null, 'is_draft' => 0);

        $this->data['p_groups'] = $this->roles_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->roles_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Accessibility');

        $roles = $this->roles_m->get_by(array("(country_id = {$this->country_id} OR global = 1)" => null, 'is_draft' => 0));

        $rolesTree = $this->roles_m->makeNestedList($this->roles_m->groupListByParentId($roles));

        $this->data['rolesTemplate'] = $this->roles_m->renderNestedTemplate($rolesTree);

        return $this->render('ami/accessibility/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'accessibility')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['p_groups'] = $this->roles_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->roles_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Accessibility');

        $roles = $this->roles_m->get_by(array('country_id' => $this->country_id, 'is_draft' => 0));
        $rolesTree = $this->roles_m->makeNestedList($this->roles_m->groupListByParentId($roles));

        $this->data['rolesTemplate'] = $this->roles_m->renderNestedTemplate($rolesTree);

        return $this->render('ami/accessibility/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        return array(
            'modules' => $this->menu_m->get(),
            // 'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions),
            // 'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'country_sub_channels' => $this->country_sub_channels_m->parse_form($cols, '- Select -', $conditions),
            // 'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            // 'assigned_distributors' => $this->roles_m->parse_form(array('armstrong_2_distributors_id', 'name'), '- Select -', $conditions),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'accessibility')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        return $this->render('ami/accessibility/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'accessibility') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/accessibility');

        if ($id) {
            $this->data['group'] = $this->roles_m->get($id);
            $this->assertCountry($this->data['group']);
            $this->data['group']['restrictions'] = $this->roles_m->parseRestrictions($this->data['group']['restrictions']);
            $this->data['page_title'] = page_title('Edit Permission');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'accessibility')) {
                    return $this->noPermission();
                }

                return $this->render('ami/accessibility/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                $this->data['salespersons'] = $this->salespersons_m->get_by(array('roles_id' => $id));
                return $this->render('ami/accessibility/edit', $this->data);
            }
        }

        return redirect('ami/accessibility');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'accessibility')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->roles_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/accessibility');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'accessibility')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/accessibility');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/accessibility';
                //$this->roles_m->delete($id);
                $this->roles_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/accessibility/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/accessibility');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'accessibility')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/accessibility');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/accessibility/draft';
                $this->roles_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/accessibility/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id}"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/accessibility');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'accessibility') && !$this->hasPermission('edit', 'accessibility')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->roles_m->array_from_post(array('name', 'restrictions', 'salespersons', 'id', 'global'));

        $salespersons = !empty($data['salespersons']) ? $data['salespersons'] : false;
        unset($data['salespersons']);

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        if ($data['restrictions']) {
            foreach ($data['restrictions'] as $key => &$value) {
                $data['restrictions'][$key] = implode(',', $value);
            }
        }

        if (!isset($data['restrictions']['manage_staff']) && !isset($data['restrictions']['manage_all'])) {
            $data['restrictions']['manage_staff'] = 5;
        }

        if($data['global']){
            $data['country_id'] = 0;
            $data['restrictions']['manage_all'] = 5;
            unset($data['restrictions']['manage_staff']);
        }

        $data['restrictions'] = json_encode($data['restrictions']);
        $newId = $this->roles_m->save($data, $id);
        if ($salespersons) {
            $salespersonsId = explode(',', preg_replace('/\s+/', '', $salespersons));

            $this->salespersons_m->update_in(array('roles_id' => $newId), 'armstrong_2_salespersons_id', $salespersonsId);
        }

        return redirect('ami/accessibility');
    }

    public function chart()
    {
        if (!$this->hasPermission('edit', 'accessibility')) {
            return $this->json(array('error' => 'No Permission!'));
        }

        if ($this->is('POST', false)) {
            $list = $this->input->post('list');
            if ($list) {
                $listArray = json_decode($list, true);

                if ($listArray) {
                    foreach ($listArray as $item) {
                        $id = ($item['item_id'] != null) ? $item['item_id'] : 0;
                        $parentId = ($item['parent_id'] != null) ? $item['parent_id'] : 0;

                        $this->roles_m->save(array('parent_id' => $parentId, 'depth' => $item['depth']), $id);
                    }

                    return $this->json(array('ok' => 'Changes Saved'));
                }

                return $this->json(array('error' => 'Internal server error'));
            }
        }

        return $this->json(array('error' => 'Bad request!'));
    }

    public function salespersons($action, $id)
    {
        if (!$this->hasPermission('add', 'accessibility') || !$this->hasPermission('edit', 'accessibility')) {
            return $this->noPermission();
        }

        if (in_array($action, array('edit', 'delete')) && !empty($id)) {
            $action = strtolower($action);

            if ($this->is('GET', false) && $this->input->is_ajax_request()) {
                $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
                $cols = array('id', 'name');

                $params = array(
                    'salesperson' => $this->salespersons_m->get($id, true),
                    'roles' => $this->roles_m->parse_form($cols, '', $conditions)
                );

                return $this->render('ami/accessibility/salespersons/modal_' . $action, $params);
            }

            if ($this->is('POST', false)) {
                switch ($action) {
                    case 'edit':
                        $data = $this->salespersons_m->array_from_post(array('roles_id'));
                        $this->salespersons_m->save($data, $id);
                        break;

                    case 'delete':
                        $this->salespersons_m->save(array('roles_id' => 0), $id);
                        break;

                    default:
                        break;
                }
            }
        }

        return redirect('ami/accessibility');
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term) {
            return $this->json(array());
        }
        $conditions = array("first_name LIKE '%{$term}%'", "last_name LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        $salespersons = $this->db
            ->where('country_id', $this->country_id)
            // ->or_like('first_name', $term)
            // ->or_like('last_name', $term)
            ->where($conditions)
            ->get('salespersons')
            ->result_array();

        $output = array();

        foreach ($salespersons as $salesperson) {
            $output[] = array(
                'value' => $salesperson['armstrong_2_salespersons_id'],
                'label' => $salesperson['first_name'] . ' ' . $salesperson['last_name']
            );
        }

        return $this->json($output);
    }
}