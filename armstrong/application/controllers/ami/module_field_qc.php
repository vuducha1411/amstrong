<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class module_field_qc extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('module_field_qc_m');
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $news = $this->module_field_qc_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $news = $this->module_field_qc_m->prepareDataTables($news);

        return $this->datatables->generate($news);
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'pull':
                $filter = 0;
                break;

            case 'push':
                $filter = 1;
                break;
            case 'leader':
                $filter = 2;
                break;
            default:
                $filter = null;
                break;
        }

        return $filter;
    }

    public function index($id = null)
    {
        if (!$this->hasPermission('view', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $filter = $this->_getFilter();

        if ($filter !== null) {
            $conditions['app_type'] = $this->_getFilter();
        }

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

        // $questions = $this->module_field_qc_m->get_limit($this->getPageLimit(), $conditions, 'order_number asc');
        if($id)
        {
            $conditions['parent_id'] = $id;
            $this->data['page_title'] = page_title('Module Fields Quality Control');
            $this->data['tittle_page'] = 'Module Fields Quality Control';
            $this->data['istitle'] = true;
            $this->data['list_fields'] = $this->getListFields('quality_control');
            $this->data['list_fields']['image_path'] = 'image_path';
            $this->data['list_types'] = ['1'=>'View',
                                        '2'=>'Input',
                                        '3'=>'Dropdown',
                                        '4'=>'Click',
                                        '5'=>'Date',
                                        '6'=>'Photo',
                                        '7'=>'RadioButton',
                                        '11'=>'Input Number',
                                        '12'=>'Input Phone',
                                        '13'=>'Input Email',
                                        '14'=>'Signature',
                                        '15'=>'Input multiple lines'];

        }
        else
        {
            $conditions['parent_id'] = 0;
            $this->data['page_title'] = page_title('Module Section Quality Control');
            $this->data['tittle_page'] = 'Module Section Quality Control';
            $this->data['istitle'] = false;
        }
        $this->data['parent_id'] = $id;
        $quality_controls = $this->module_field_qc_m->get_by($conditions, FALSE, 'order_number asc');

        foreach($quality_controls as &$q)
        {
            $this->db->select('id');
            $this->db->from('module_field_qc');
            $this->db->where(array('parent_id' => $q['id'],'is_draft'=>0));
            $q['total_element'] = $this->db->count_all_results();
        }
        $this->data['total'] = $this->module_field_qc_m->count('where', $conditions);

        $this->data['filter'] = $this->input->get('filter');
        $this->data['data'] = $quality_controls;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Quality Control');
        return $this->render('ami/module_field_qc/index', $this->data);
    }

    public function getListFields($table,$field_name = null){
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $exist_fields = $this->module_field_qc_m->get_by($conditions,False,null,'field_name');
        $exist= [];
        foreach($exist_fields as $key => $field){
            if($field['field_name'] != '' && $field['field_name'] != $field_name){
                $exist[] = $field['field_name'];
            }
        }
        $list_fields = $this->db->list_fields($table);
        $new_lists = [];
        foreach($list_fields as $k => $v){
            if(!in_array($v,$exist)){
                $new_lists[$v] = $v;
            }
        }

        return $new_lists;
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['questions'] = $this->module_field_qc_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->module_field_qc_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('questions');

        return $this->render('ami/module_field_qc/index', $this->data);
    }

    public function cat_questions($type = 0, $ajax = false)
    {
        // app_type : 0:pull, 1:push, 2:leader
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'parent_id' => 0, 'app_type' => $type);

        $cat_questions = $this->module_field_qc_m->getListOptions('- Select -', $conditions, true);

        if ($ajax == true) {
            $html = '';
            foreach ($cat_questions as $k => $p) {
                $html .= '<option value="' . $k . '">' . $p . '</option>';
            }
            echo $html;
            exit();
        } else {
            return array(
                'cat_questions' => $cat_questions
            );
        }
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->data += $this->cat_questions();
        return $this->render('ami/module_field_qc/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'questions') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->_assertId($id, 'ami/module_field_qc');

        if ($id = intval($id)) {
            $post = $this->module_field_qc_m->get($id);
            $post['type_question'] = $post['parent_id'] == 0 ? 0 : 1;
            $this->data['post'] = $post;

            $this->data['list_fields'] = $this->getListFields('quality_control',$post['field_name']);
            $this->data['istitle'] =true;
//            return $this->render('ami/module_field_qc/edit', $this->data);
            echo json_encode($post);
            die;
        }

        return redirect('ami/module_field_qc');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'setting_module_fields')) {
            return $this->noPermission();
        }
        $referent_url = $_SERVER['HTTP_REFERER'];
        $this->is('POST');

        $ids = $this->input->post('ids');
        $order = 0;
        foreach ($ids as $id) {
            if ($id = intval($id)) {
                if(isset($_POST['draft']))
                {
                    $this->module_field_qc_m->save(array(
                        'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'last_updated' => get_date()
                    ), $id);
                }
                else
                {
                    $this->module_field_qc_m->save(array(
                        // 'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'order_number' => $order++,
                        'last_updated' => get_date()
                    ), $id);
                }
            }
        }
        if ($this->input->is_ajax_request()) {
            echo 'ok';
        }
        else
        {
            return redirect($referent_url);
        }
        // return redirect('ami/module_field_qc');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/module_field_qc');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/module_field_qc';
                //$this->module_field_qc_m->delete($id);
                $this->module_field_qc_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/module_field_qc/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Questions"
                );
                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/module_field_qc');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/module_field_qc');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/module_field_qc/draft';
                $this->module_field_qc_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/module_field_qc/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Questions"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/module_field_qc');
    }

    public function save()
    {

        if (!$this->hasPermission('add', 'questions') && !$this->hasPermission('edit', 'setting_module_fields')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->module_field_qc_m->array_from_post(array('id', 'title', 'app_type', 'parent_id','type_question', 'require' ,'field_name','type'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;

        if ($data['type_question'] == 0) {
            $data['parent_id'] = 0;
        }
        if($data['parent_id'] && $catinfor = $this->module_field_qc_m->get($data['parent_id']))
        {
            $data['app_type'] = $catinfor['app_type'];
        }

        if(!$id)
        {

            if($maxorder = $this->db->query('select max(order_number) as maxorder from module_field_qc where parent_id = '.$data['parent_id'].' and country_id = '.$this->country_id))
            {
                $order_number = $maxorder->result_array()[0]['maxorder'];
                $data['order_number'] = $order_number + 1;
            }
            else
            {
                $data['order_number'] = 0;
            }

        }
        unset($data['type_question']);
        $postId = $this->module_field_qc_m->save($data, $id);
        $this->db->query('update `module_field_qc` set app_type = "'.$data['app_type'].'" where parent_id = '.$postId);
        $filter = $this->input->post('filter') ? $this->input->post('filter') : 'all';
        return redirect('ami/module_field_qc/index/'.$data['parent_id']);
    } 
}