<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shifted_reason extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('shifted_reason_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'shifted_reason')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['shifted_reason'] = $this->shifted_reason_m->get_by($conditions);
        return $this->render('ami/shifted_reason/index', $this->data);
    }
//
//    public function ajaxData()
//    {
//
//        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
//
//        $datatables = new Datatable(array('model' => 'Free_gifts_dt', 'rowIdCol' => $this->free_gifts_m->getTablePrimary()));
//
//        $this->free_gifts_m->setDatatalesConditions($conditions);
//
//        $data = $datatables->datatableJson();
//        foreach ($data['data'] as &$_data) {
//            $id = $_data['id'];
//            $_data['buttons'] = '<div class="btn-group">';
//
//            if ($this->hasPermission('edit', 'free_gifts')) {
//                $_data['buttons'] .= html_btn(site_url('ami/free_gifts/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
//            }
//
//            if ($this->hasPermission('delete', 'free_gifts')) {
//                $_data['buttons'] .= html_btn(site_url('ami/free_gifts/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
//            }
//
//            $_data['buttons'] .= '</div>';
//        }
//
//        return $this->json($data);
//    }

    public function add()
    {
        if (!$this->hasPermission('add', 'shifted_reason'))
        {
            return $this->noPermission();
        }

        if ($this->input->get('id'))
        {
            $this->data['shifted_reason']['id'] = $this->input->get('id');
        }

        return $this->render('ami/shifted_reason/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'shifted_reason') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/shifted_reason');
        if ($id) {

            $this->data['shifted_reason'] = $this->shifted_reason_m->get($id);
            $this->data['page_title'] = page_title('Edit Shifted Reason');

            return $this->render('ami/shifted_reason/edit', $this->data);
        }

        return redirect('ami/shifted_reason');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'shifted_reason') && !$this->hasPermission('edit', 'shifted_reason'))
        {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->shifted_reason_m->array_from_post(array('id', 'name', 'active'));
        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;
        $this->shifted_reason_m->save($data, $id);
        return redirect('ami/shifted_reason');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'shifted_reason')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->shifted_reason_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/shifted_reason');
    }
    
    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'shifted_reason')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/shifted_reason');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/shifted_reason';
                //$this->free_giftss_m->delete($id);
                $this->shifted_reason_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/shifted_reason/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Shifted Reason"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/shifted_reason');
    }
}