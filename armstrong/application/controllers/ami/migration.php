<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Date: 8/30/14
 * Time: 10:04 AM
*/

class Migration extends AMI_Controller {

    // GLOBAL VARIABLE DECLARATION

    // FUNCTIONS DECLARATION
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('migration');

        if (! $this->migration->current()) {
            show_error($this->migration->error_string(), 404);
        } else {
            echo "Migration worked!";
        }
    }

}