<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Date: 3/4/15
 * Time: 3:24 PM
 */
class Newsfeed extends AMI_Controller
{
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsfeed_m');
    }

    public function ajaxData() {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $newsfeeds = $this->newsfeed_m->get_limit($this->getPageLimit(null, 'start', $conditions));

        $newsfeeds = $this->newsfeed_m->prepareDataTables($newsfeeds);

        return $this->datatables->generate($newsfeeds);
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'newsfeed')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        // Preparing data for view
        $this->data['draft'] = false;
        $this->data['newsfeeds'] = $this->newsfeed_m->get_by($conditions);
        $this->data['newsfeeds'] = $this->newsfeed_m->prepareDataTables($this->data['newsfeeds']);

        $this->data['page_title'] = page_title('Newsfeed');

        // Return view
        return $this->render('ami/newsfeed/index', $this->data);
    }
}