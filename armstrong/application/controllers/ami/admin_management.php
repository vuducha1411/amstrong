<?php
/**
 * User: vietthang
 * Date: 10/20/14
 * Time: 10:10 AM
 */

class Admin_management extends AMI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admins_m');
	}

	public function index() {
        
        if (!$this->hasPermission('view', 'admin_management'))
        {
            return $this->noPermission();
        }

		$conditions = array("(country_id = {$this->country_id} OR country_id = 0)" => null, 'is_draft' => 0);

		$this->data['admins'] = $this->admins_m->get_limit($this->getPageLimit(), $conditions);
		$this->data['total'] = $this->admins_m->count('where', $conditions);
		$this->data['draft'] = false;
		$this->data['page_title'] = page_title('Admin Management');

		return $this->render('ami/admin_management/index', $this->data);
	}

	public function edit($id) {

        if (!$this->hasPermission('edit', 'admin_management') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

		$this->_assertId($id, 'ami/admin_management');

		if ($id = intval($id))
		{
			$this->data['admin'] = $this->admins_m->get($id);
            $this->assertCountry($this->data['admin']);
			$this->data['page_title'] = page_title('Edit Administrator Info');
            // Get list roles global
            $globalRoles = $this->roles_m->getListRolesGlobal();
            $this->data['globalRoles'] = $globalRoles;
			if ($this->input->is_ajax_request()){
                if (!$this->hasPermission('view', 'admin_management'))
                {
                    return $this->noPermission();
                }
                
				return $this->render('ami/admin_management/preview', $this->data);
			} else {
				return $this->render('ami/admin_management/edit', $this->data);
			}
		}

		return redirect('ami/admin_management');
	}

	public function draft()
	{
        if (!$this->hasPermission('view', 'admin_management'))
        {
            return $this->noPermission();
        }

		$conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

		$this->data['admins'] = $this->admins_m->get_limit($this->getPageLimit(), $conditions);
		$this->data['total'] = $this->admins_m->count('where', $conditions);
		$this->data['draft'] = true;
        $this->data['page_title'] = page_title('Admin Management');

		return $this->render('ami/admin_management/index', $this->data);
	}

	public function add() {
        if (!$this->hasPermission('add', 'admin_management'))
        {
            return $this->noPermission();
        }
        // Get list roles global
        $globalRoles = $this->roles_m->getListRolesGlobal();
        $this->data['globalRoles'] = $globalRoles;
		return $this->render('ami/admin_management/edit', $this->data);
	}

	public function delete($id = NULL) {

        if (!$this->hasPermission('delete', 'admin_management'))
        {
            return $this->noPermission();
        }

		$this->_assertId($id, 'ami/admin_management');

		if ($id = intval($id)) {
			if ( $this->is( 'POST', false ) ) {
				$redirect = $this->input->post( 'redirect' ) ? $this->input->post( 'redirect' ) : 'ami/admin_management';

				$this->admins_m->save( array(
					'is_draft'     => 1,
					'last_updated' => get_date()
				), $id, false, 'DELETE');

				return redirect( $redirect );
			} else {
				$params = array(
					'action'  => site_url( "ami/admin_management/delete/{$id}" ),
					'message' => "Deleting entry with ID: {$id} from Admins"
				);

				return $this->render( 'ami/components/modal_form', $params );
			}
		}
	}

	public function save() {

        if (!$this->hasPermission('view', 'admin_management') && !$this->hasPermission('edit', 'admin_management'))
        {
            return $this->noPermission();
        }

		// Validate form method
		$this->is('POST');

		// Define post fields and retrieve data from $_POST
		$post_fields = array('first_name', 'last_name', 'email', 'password', 'id', 'role_id');
		$post_data = $this->admins_m->array_from_post($post_fields);
		// Initialize id
		$id = $post_data['id'] ? $post_data['id'] : NULL;

		// Get hash string
		$_hash = $this->admins_m->_hash;

		if ($post_data['password']) {
			// Encrypt password
			$post_data['password'] = crypt($post_data['password'], md5($_hash));

			// Add hash and temp_hash string
			$post_data['hash'] = $_hash;
			$post_data['temp_hash'] = $_hash;
		} else {
			unset($post_data['password']);
		}

		// Update admin information
		if (intval($id)) {
            if($post_data['role_id'] != 0){
                $post_data['country_id'] = 0;
            }else{
                $post_data['country_id'] = $this->country_id;
            }
			$this->admins_m->save($post_data,$id);

		} else {
			// Add additional data
			// TODO: create mechanism for admin role and other user role
//			$post_data['role_id'] = 0;
            if($post_data['role_id'] != 0){
                $post_data['country_id'] = 0;
            }else{
                $post_data['country_id'] = $this->country_id;
            }

			$this->admins_m->save($post_data);
		}

		redirect('ami/admin_management');
	}

	public function restore($id = null)
	{
        if (!$this->hasPermission('delete', 'admin_management'))
        {
            return $this->noPermission();
        }
        
		$this->_assertId($id, 'ami/admin_management');

		if ($id = intval($id))
		{
			if ($this->is('POST', false))
			{
				$redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/admin_management/draft';
				$this->admins_m->save(array(
					'is_draft' => 0,
					'last_updated' => get_date()
				), $id);

				return redirect($redirect);
			}
			else
			{
				$params = array(
					'action' => site_url("ami/admin_management/restore/{$id}"),
					'message' => "Restore entry with ID: {$id} from Admins"
				);

				return $this->render('ami/components/modal_form', $params);
			}
		}

		return redirect('ami/admin_management');
	}

	function email_validation() {
		$email = $this->input->get('email');
		$id = $this->uri->segment(4);

		if (empty($email) || $email == "") {
			$return = array('error' => "This value is required");
			$this->output->set_status_header('404');
			echo json_encode($return);
			exit();
		}

		if (!$id) {
			$admin = $this->admins_m->get_by(array('email' => $email), TRUE);

			if (count($admin)) {
				$return = array('error' => "Email is existed, please enter a new one");
				$this->output->set_status_header('404');
				echo json_encode($return);
				exit();
			}
		}

		echo json_encode(array('success' => 'Valid data'));
	}
}