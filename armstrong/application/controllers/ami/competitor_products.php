<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Competitor_products extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('competitor_products_m');
        $this->load->model('products_m');
    }

    public function ajaxData()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_call_records_id != ""' => null);

        $datatables = new Datatable(array('model' => 'competitor_products_dt'));

        $this->competitor_products_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';
            if ($this->hasPermission('edit', 'competitor_products')) {
                $_data['buttons'] .= html_btn(site_url('ami/competitor_products/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }
            if ($this->hasPermission('delete', 'competitor_products')) {
                $_data['buttons'] .= html_btn(site_url('ami/competitor_products/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }
            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
        /*
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $check_list = $this->competitor_products_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $check_list = $this->competitor_products_m->prepareDataTables($check_list);

        return $this->datatables->generate($check_list);
        */
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'competitor_products')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_call_records_id LIKE' => 'CALA0000' . strtoupper($this->country_code));

        $this->data['data'] = $this->competitor_products_m->get_by($conditions);
     //   $this->data['data'] = array();
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Competitor Products');

        return $this->render('ami/competitor_products/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'competitor_products')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1, 'armstrong_2_call_records_id LIKE' => 'CALA0000' . strtoupper($this->country_code));

        $this->data['data'] = $this->competitor_products_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->competitor_products_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Competitor Products');

        return $this->render('ami/competitor_products/index', $this->data);
    }

    protected function _getAddEditParams()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        return array(
            'products' => array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'),
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'competitor_products')) {
            return $this->noPermission();
        }
        $this->data += $this->_getAddEditParams();
        return $this->render('ami/competitor_products/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'competitor_products') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products');

        if ($id = intval($id)) {
            $this->data['post'] = $this->competitor_products_m->get($id);
            $ufs_skus = explode(',', $this->data['post']['ufs_skus']);
            $this->data['post']['products_array'] = count($ufs_skus) > 0 ? $ufs_skus : array();
            $this->assertCountry($this->data['post']);
            $this->data['page_title'] = page_title('Edit Competitor Products');
            $this->data += $this->_getAddEditParams();

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'competitor_products')) {
                    return $this->noPermission();
                }

                return $this->render('ami/competitor_products/preview', $this->data);
            } else {
                return $this->render('ami/competitor_products/edit', $this->data);
            }
        }

        return redirect('ami/competitor_products');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'competitor_products')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id = intval($id)) {
                $this->competitor_products_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/competitor_products');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'competitor_products')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/competitor_products';
                //$this->competitor_products_m->delete($id);
                $this->competitor_products_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/competitor_products/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Competitor Products"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/competitor_products');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'competitor_products')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/competitor_products');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/competitor_products/draft';
                $this->competitor_products_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/competitor_products/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Competitor Products"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/competitor_products');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'competitor_products') && !$this->hasPermission('edit', 'competitor_products')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->competitor_products_m->array_from_post(array('id', 'product_name', 'quantity', 'price', 'products'));
        $ufs_skus = '';
        if ($data['products']) {
            foreach ($data['products'] as $product) {
                if($product['sku_number'] != '')
                {
                    $ufs_skus .= $product['sku_number'] . ',';
                }
            }
        }
        unset($data['products']);
        $data['ufs_skus'] = substr($ufs_skus, 0, -1);

        $data['country_id'] = $this->country_id;
        $data['armstrong_2_call_records_id'] = 'CALA0000' . strtoupper($this->country_code);

        $id = $data['id'] ? $data['id'] : null;

        $postId = $this->competitor_products_m->save($data, $id);

        return redirect('ami/competitor_products');
    }
}