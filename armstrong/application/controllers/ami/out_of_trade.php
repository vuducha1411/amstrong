<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Out_of_trade extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('out_of_trade_m');
        //$this->load->model('out_of_trade');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'out_of_trade')) {
            return $this->noPermission();
        }
        $setting_country = $this->out_of_trade_m->getEntry('out_of_trade', array('country_id' => $this->country_id));
        if($setting_country)
        {
            $this->data['out_of_trade'] = $setting_country[0];
        }
        return $this->render('ami/out_of_trade/edit', $this->data);
    }

    function save()
    {
        if(isset($_POST['average_calls_per_day_target']) && isset($_POST['no_of_working_hours_per_day']))
        {
            $now = date('Y-m-d H:i:s');
            if($setting = $this->out_of_trade_m->getEntry('out_of_trade', array('country_id' => $this->country_id)))
            {
                $this->db->update('out_of_trade', array(
                    'average_calls_per_day_target' => $_POST['average_calls_per_day_target'],
                    'no_of_working_hours_per_day' => $_POST['no_of_working_hours_per_day'],
                    'last_updated' => $now,
                ), array('id' => $setting[0]->id));
            }
            else
            {
                $this->db->insert('out_of_trade', array(
                    'country_id' => $this->country_id,
                    'average_calls_per_day_target' => $_POST['average_calls_per_day_target'],
                    'no_of_working_hours_per_day' => $_POST['no_of_working_hours_per_day'],
                    'date_created' => $now,
                    'last_updated' => $now,
                    'date_sync' => $now,
                    'app_type' => 1,
                    'is_draft' => 0,
                ));
            }
        }
        return redirect('ami/out_of_trade');
    }

}