<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Free_gifts extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('free_gifts_m');
        $this->data['apps'] = array(
          '0' => 'PULL',
          '1' => 'PUSH',
          '2' => 'MIX',
        );
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'free_gifts')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['free_gifts'] = $this->free_gifts_m->get_by($conditions);
        return $this->render('ami/free_gifts/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Free_gifts_dt', 'rowIdCol' => $this->free_gifts_m->getTablePrimary()));

        $this->free_gifts_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];
            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'free_gifts')) {
                $_data['buttons'] .= html_btn(site_url('ami/free_gifts/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'free_gifts')) {
                $_data['buttons'] .= html_btn(site_url('ami/free_gifts/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'free_gifts'))
        {
            return $this->noPermission();
        }

        if ($this->input->get('id'))
        {
            $this->data['free_gifts']['id'] = $this->input->get('id');
        }

        return $this->render('ami/free_gifts/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'free_gifts') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/free_gifts');
        if ($id) {

            $this->data['free_gifts'] = $this->free_gifts_m->get($id);
            if(isset($this->data['free_gifts']['expiry_date']))
            {
                $this->data['free_gifts']['expiry_date'] = date('m/d/Y H:i A', strtotime($this->data['free_gifts']['expiry_date']));
            }
            $this->data['page_title'] = page_title('Edit Free Gifts');

            return $this->render('ami/free_gifts/edit', $this->data);
        }

        return redirect('ami/free_gifts');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'free_gifts') && !$this->hasPermission('edit', 'free_gifts'))
        {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->free_gifts_m->array_from_post(array('id', 'name', 'expiry_date', 'active', 'app_type'));
        $data['expiry_date'] = date('Y-m-d H:i:s', strtotime($data['expiry_date']));
        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;
        $this->free_gifts_m->save($data, $id);
        return redirect('ami/free_gifts');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'free_gifts')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->free_gifts_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/free_gifts');
    }
    
    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'free_gifts')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/free_gifts');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/free_gifts';
                //$this->free_giftss_m->delete($id);
                $this->free_gifts_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/free_gifts/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Free Gifts"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/free_gifts');
    }
}