<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Eloquent\FormulaRef;

class Setting_cycle extends AMI_Controller
{
	public function index()
	{
		if (!$this->hasPermission('view', 'setting_cycle')) 
		{
			return $this->noPermission();
		}

		$this->data['cycles'] = FormulaRef::filter($this->country_id)
			->where('type', '=', 1)
			->orderBy('date_start', 'asc')
			->orderBy('date_end', 'asc')
			->get();

		$this->data['page_title'] = page_title('OTM Cycle Settings');

		return $this->render('ami/setting_cycle/index');
	}

	public function add()
	{
		if (!$this->hasPermission('add', 'setting_cycle')) 
		{
			return $this->noPermission();
		}

		$this->data['lastCycle'] = FormulaRef::filter($this->country_id)
			->where('type', '=', 1)
			->orderBy('date_end', 'desc')
			->first();

		$this->data['page_title'] = page_title('Add Cycle');

		return $this->render('ami/setting_cycle/edit');
	}

	public function save()
	{
		if (!$this->hasPermission('add', 'setting_cycle') && !$this->hasPermission('edit', 'setting_cycle')) 
		{
			return $this->noPermission();
		}

		$this->is('POST');

		$data = $this->request->except('cycle');

		$data['type'] = 1;
		$data['country_id'] = $this->country_id;

		FormulaRef::create($data);

		return redirect('ami/setting_cycle');
	}

	public function update()
	{
		if (!$this->hasPermission('add', 'setting_cycle') && !$this->hasPermission('edit', 'setting_cycle')) 
		{
			return $this->noPermission();
		}

		$this->is('POST');

		$data = $this->request->input('cycle');

		foreach ($data as $id => $cycle) 
		{
			if ($id)
			{
				FormulaRef::find($id)->update($cycle);
			}
		}

		return redirect('ami/setting_cycle');
	}

	public function delete($id = NULL)
	{
		if (!$this->hasPermission('delete', 'setting_cycle')) 
		{
			return $this->noPermission();
		}

		$this->_assertId($id, 'ami/setting_cycle');

		if ($id) 
		{
			if ($this->is('POST', FALSE)) 
			{
				$redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/setting_cycle';
				
				FormulaRef::find($id)->softDelete();

				return redirect($redirect);
			} 
			else 
			{
				$params = array(
					'action' => site_url("ami/setting_cycle/delete/{$id}"),
					'message' => "Deleting this entry from Cycle"
				);

				return $this->render('ami/components/modal_form', $params);
			}
		}

		return redirect('ami/setting_cycle');
	}
}