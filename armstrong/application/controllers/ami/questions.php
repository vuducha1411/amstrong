<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class questions extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('questions_m');
    }

    public function ajaxData()
    {
        $input = $this->input->get();

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $news = $this->questions_m->get_limit($this->getPageLimit(null, 'start'), $conditions);

        $news = $this->questions_m->prepareDataTables($news);

        return $this->datatables->generate($news);
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'pull':
                $filter = 0;
                break;

            case 'push':
                $filter = 1;
                break;
            case 'leader':
                $filter = 2;
                break;
            default:
                $filter = null;
                break;
        }

        return $filter;
    }

    public function index($id = null)
    {
        if (!$this->hasPermission('view', 'questions')) {
            return $this->noPermission();
        }

        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $filter = $this->_getFilter();

        if ($filter !== null) {
            $conditions['app_type'] = $this->_getFilter();
        }

        $limit = $this->getPageLimit();

        if (is_array($limit)) {
            list($total, $offset) = $limit;
        } else {
            $total = $limit;
            $offset = 0;
        }

       // $questions = $this->questions_m->get_limit($this->getPageLimit(), $conditions, 'order_number asc');
        if($id)
        {
            $conditions['parent_id'] = $id;
            $this->data['page_title'] = page_title('Questions');
            $this->data['tittle_page'] = 'Questions';
            $this->data['isquestion'] = true;
        }
        else
        {
            $conditions['parent_id'] = 0;
            $this->data['page_title'] = page_title('Category Questions');
            $this->data['tittle_page'] = 'Category Questions';
            $this->data['isquestion'] = false;
        }
        $this->data['parent_id'] = $id;
        $questions = $this->questions_m->get_by($conditions, FALSE, 'order_number asc');
        foreach($questions as &$q)
        {
            $this->db->select('id');
            $this->db->from('questions');
            $this->db->where(array('parent_id' => $q['id']));
            $q['total_question'] = $this->db->count_all_results();
        }
        $this->data['total'] = $this->questions_m->count('where', $conditions);
        $this->data['filter'] = $this->input->get('filter');
        $this->data['data'] = $questions;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Category Questions');
        return $this->render('ami/questions/index', $this->data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'questions')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['questions'] = $this->questions_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->questions_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('questions');

        return $this->render('ami/questions/index', $this->data);
    }

    public function cat_questions($type = 0, $ajax = false)
    {
        // app_type : 0:pull, 1:push, 2:leader
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'parent_id' => 0, 'app_type' => $type);

        $cat_questions = $this->questions_m->getListOptions('- Select -', $conditions, true);

        if ($ajax == true) {
            $html = '';
            foreach ($cat_questions as $k => $p) {
                $html .= '<option value="' . $k . '">' . $p . '</option>';
            }
            echo $html;
            exit();
        } else {
            return array(
                'cat_questions' => $cat_questions
            );
        }
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'questions')) {
            return $this->noPermission();
        }

        $this->data += $this->cat_questions();
        return $this->render('ami/questions/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'questions') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->_assertId($id, 'ami/questions');

        if ($id = intval($id)) {
            $post = $this->questions_m->get($id);
            $post['type_question'] = $post['parent_id'] == 0 ? 0 : 1;
            $this->data['post'] = $post;
            echo json_encode($post);
            die;
            $this->assertCountry($this->data['post']);
            $this->data['page_title'] = page_title('Edit Questions');
            $this->data['filter'] = $this->input->get('filter');

            $this->data += $this->cat_questions($post['app_type']);

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'questions')) {
                    return $this->noPermission();
                }
                return $this->render('ami/questions/preview', $this->data);
            } else {
                return $this->render('ami/questions/edit', $this->data);
            }
        }

        return redirect('ami/questions');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'questions')) {
            return $this->noPermission();
        }
        $referent_url = $_SERVER['HTTP_REFERER'];
        $this->is('POST');

        $ids = $this->input->post('ids');
        $order = 0;
        foreach ($ids as $id) {
            if ($id = intval($id)) {
                if(isset($_POST['draft']))
                {
                    $this->questions_m->save(array(
                        'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'last_updated' => get_date()
                    ), $id);
                }
                else
                {
                    $this->questions_m->save(array(
                       // 'is_draft' => $this->input->post('draft') ? 0 : 1,
                        'order_number' => $order++,
                        'last_updated' => get_date()
                    ), $id);
                }
            }
        }
        if ($this->input->is_ajax_request()) {
            echo 'ok';
        }
        else
        {
            return redirect($referent_url);
        }
       // return redirect('ami/questions');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'questions')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/questions');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/questions';
                //$this->questions_m->delete($id);
                $this->questions_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');
                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/questions/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Questions"
                );
                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/questions');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'questions')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/questions');

        if ($id = intval($id)) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/questions/draft';
                $this->questions_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/questions/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Questions"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/questions');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'questions') && !$this->hasPermission('edit', 'questions')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->questions_m->array_from_post(array('id', 'title', 'app_type', 'parent_id', 'type_question', 'require'));

        $data['country_id'] = $this->country_id;

        $id = $data['id'] ? $data['id'] : null;
        if ($data['type_question'] == 0) {
            $data['parent_id'] = 0;
        }
        if($data['parent_id'] && $catinfor = $this->questions_m->get($data['parent_id']))
        {
            $data['app_type'] = $catinfor['app_type'];
        }
        if(!$id)
        {
            if($maxorder = $this->db->query('select max(order_number) as maxorder from questions where parent_id = '.$data['parent_id'].' and country_id = '.$this->country_id))
            {
                $order_number = $maxorder->result_array()[0]['maxorder'];
                $data['order_number'] = $order_number + 1;
            }
            else
            {
                $data['order_number'] = 0;
            }
        }
        unset($data['type_question']);
        $postId = $this->questions_m->save($data, $id);
        $this->db->query('update `questions` set app_type = "'.$data['app_type'].'" where parent_id = '.$postId);
		$filter = $this->input->post('filter') ? $this->input->post('filter') : 'all';
        return redirect('ami/questions/index/'.$data['parent_id']);
    }


}