<?php if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * Date: 8/8/14
 * Time: 3:14 PM
 */
class Dashboard extends AMI_Controller {

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONAL DECLARATION
	|--------------------------------------------------------------------------
	*/
	public function __construct() {
		parent::__construct();
		$this->load->model('salespersons_m');
		$this->load->model('salespersons_history_m');
		$this->load->model('authentication_m');
	}

    public function index() {
        // dump_exit($this->isSuperAdmin());
        $this->render('ami/dashboard/wellcome', $this->data);
        //return redirect('ami/salespersons');
    }

    public function country($country)
    {
        if (!$this->isSuperAdmin())
        {
            return $this->noPermission();
        }

        $redirect = $this->input->get('redirect') 
            ? $this->input->get('redirect')
            : 'ami/dashboard';

        $this->_assertId($country, $redirect);

        if ($country)
        {
            $_country = $this->country_m->get_by(array('code' => $country), true);
            if ($_country)
            {
                $sessionData = $this->session->userdata('user_data');
                $sessionData['country_id'] =  $_country['id'];
                $this->session->set_userdata('user_data', $sessionData);
            }
        }

        return redirect($redirect);
    }

	public function login() {
		// Check login
		if ($this->session->userdata('user_data')) {
			return redirect('ami/dashboard');
		}

		// Parse data to view
		$this->data['country_id'] = ($this->input->post('country_id')) ? $this->input->post('country_id') : '';
		//$this->data['countries'] = $this->country_m->parse_form($cols, $default);
		$this->data['countries'] = $this->_getCountrySelect();
		// $this->data['subview']   = 'ami/login';
		// $this->load->view('ami/modal', $this->data);

        return $this->render('ami/login');
	}

	public function check_login() {
		// Get rules from model
		$rules = $this->salespersons_m->rules_login;
		// Set rules
		$this->form_validation->set_rules($rules);
		// Validation
		if ($this->form_validation->run() === true) {
			$data = array('email', 'password', 'country_id');
			$data = $this->salespersons_m->array_from_post($data);
//            $check_login = $this->salespersons_m->loginEmail($data['country_id'], $data['email'],$data['password']);
			$check_login = $this->authentication_m->login($data['email'], $data['password'], $data['country_id']);
			if ($check_login['code'] != 0) {
				if($check_login['code'] == 2){
					$this->data['alert_failed'] = $check_login['msg'];
				}else{
					redirect('ami/dashboard');	
				}
			} else {
				$this->data['alert_failed'] = $check_login['msg'];
			}
		}

		// Parse data to view

		$this->data['country_id'] = ($this->input->post('country_id')) ? $this->input->post('country_id') : '';
		$this->data['countries']  = $this->_getCountrySelect();
		// $this->data['subview']    = 'ami/login';
		// $this->load->view('ami/modal', $this->data);
        return $this->render('ami/login');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(site_url());
	}

    public function infographic()
    {
        if ($this->is('POST', false))
        {
            $post = $this->input->post();

            if (empty($post['month']) || empty($post['year']) || empty($post['type'])) {
                return redirect('/');
            }

            if (!empty($post['year'])) {
                $this->session->set_userdata('infographicYear', $post['year']);
            }

            if (!empty($post['month'])) {
                $this->session->set_userdata('infographicMonth', $post['month']);
            }

            if (!empty($post['btn_submit'])) {
                $this->session->set_userdata('infographicAction', $post['btn_submit']);
            }

            if (!empty($post['country'])) {
                $this->session->set_userdata('infographicCountry', $post['country']);
            }

            return redirect(site_url('ami/infographic/' . $post['type']));
        }
        else
        {
            $countries = array();

            foreach ($this->countries as $key => $country) 
            {
                $countries[$country['code']] = $country['name'];
            }                

            $this->data['month'] = date('m', time());
            $this->data['year'] = date('Y', time());
            $this->data['countries'] = $countries;//array_except($countries, array('ph'));
            return $this->render('ami/dashboard/infographic');
        }
    }

    public function infographic_new()
    {
        if ($this->is('POST', false))
        {

            $post = $this->input->post();

            if (empty($post['month']) || empty($post['year'])) {
                return redirect('/');
            }

            if (!empty($post['year'])) {
                $this->session->set_userdata('infographicYear', $post['year']);
            }


            if (!empty($post['appType'])) {
                $this->session->set_userdata('infographicType', $post['appType']);
            }

       //     if (!empty($post['salesleader'])) {
                $this->session->set_userdata('infographicSl', $post['salesleader']);
         //   }

            if (isset($post['salespersons'])) {
                $this->session->set_userdata('infographicSr', $post['salespersons']);
            }

            if (!empty($post['month'])) {
                $this->session->set_userdata('infographicMonth', $post['month']);
            }
            if (isset($post['salespersonStatus'])) {
                $this->session->set_userdata('infographicsalespersonStatus', $post['salespersonStatus']);
//                echo $this->session->userdata('infographicsalespersonStatus');
//                die;
            }

            if (!empty($post['btn_submit'])) {
                $this->session->set_userdata('infographicAction', $post['btn_submit']);
            }

            if (!empty($post['country'])) {
                $this->session->set_userdata('infographicCountry', $post['country']);
            }
            
            if (!empty($post['viewType'])) {
                $this->session->set_userdata('infographicViewType', $post['viewType']);
            }

            return redirect(site_url('ami/infographic_new/generate'));
        }
        else
        {
            $countries = array();

            foreach ($this->countries as $key => $country) 
            {
                $countries[$country['code']] = $country['name'];
            }                
           // if(!$month = $this->session->userdata('infographicMonth'))
                $month = date('m', time());
            $this->data['month'] = $month;

          //  if(!$year = $this->session->userdata('infographicYear'))
                $year = date('Y', time());
            $this->data['year'] = $year;
            if($month < 10)
            {
                $month = '0'.$month;
            }
            $from = $year.'-'.$month.'-'.'01 00:00:00';
            $to = $year.'-'.$month.'-'.'31 00:00:00';
            $this->data['countries'] = $countries;//array_except($countries, array('ph'));

            $this->data['type'] = array(
            'tfo' => 'TFO + SSD + Pantry Check'/*,
            'ssd' => 'SSD',
            'pantry' => 'SSD + Pantry Check'*/
             );
            $this->data['salesperson_status'] = array(
                '2' => 'ALL',
                '1' => 'Active',
                '0' => 'Inactive'
             );

             $this->data['viewType'] = array(
            'country' => 'Country',
            'team' => 'Team'
             );

            //salespersons query

            $conditions = array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
                // 'listed' => 1,
              //  'active' => 1,
                'type != "PUSH"' => null,
                '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
            );
            $salespersons = array();
            $salespersons = $this->salespersons_history_m->getPersonListOptions(null, $conditions);
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);

            $this->data['salespersons'] = $salespersons;

            //salesleader 
            $slwhere = array(
                'country_id' => $this->country_id,
                'is_draft' => 0,
             //   'active' => 1,
                'type' => "SL",
                '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
                );

            $salesleader = array();
            $salesleader = $this->salespersons_history_m->getLeaderListOptions(null,$slwhere);
            $salesleader = array_merge(array('' => 'ALL'), $salesleader);

            $this->data['salesleader'] = $salesleader;
            return $this->render('ami/dashboard/infographic_new');
        }
    }
    private function getSaleHistoryBy($from, $to, $conditions)
    {
        $conditions['(((s.to_date IS NULL OR s.to_date >= "'.$from.'") AND s.from_date <= "'.$to.'")
 OR ((s.from_date IS NULL OR s.from_date <= "'.$to.'") AND s.to_date >= "'.$from.'")
 OR (s.from_date >= "'.$from.'" AND s.from_date <= "'.$to.'")
 OR (s.to_date >= "'.$from.'" AND s.to_date <= "'.$to.'")
 OR (s.to_date IS NULL AND s.from_date IS NULL))'] = null;
        $this->db->select($conditions);
        $this->db->where($conditions);
        $sales = $this->db->get('salespersons_history s')->result_array();
    }
    public function getSaleByCountry()
    {
        $country_infor = $this->salespersons_m->getEntry('country', array('code' => $_POST['country_id']), false, 'id');

        if($country_infor)
        {
            $country_id = $country_infor[0]->id;
        }
        else
        {
            $country_id = $_POST['country_id'];//miss
        }
        $month = isset($_POST['month']) ? $_POST['month'] : date('m');
        if($month < 10)
        {
            $month = '0'.$month;
        }
        $year = isset($_POST['year']) ? $_POST['year'] : date('Y');
        $from = $year.'-'.$month.'-'.'01 00:00:00';
        $to = $year.'-'.$month.'-'.'31 00:00:00';
        $salespersonStatus = isset($_POST['salespersonStatus']) ? $_POST['salespersonStatus'] : 2;
        $conditions = array(
            'country_id' => $country_id,
            'is_draft' => 0,
            'type != "PUSH"' => null,
            '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
        );
        if($salespersonStatus != 2)
        {
            $conditions['active'] = (int)$salespersonStatus;
        }
        $salespersons = array();
        $salespersons = $this->salespersons_history_m->getPersonListOptions(null, $conditions);
      //  $this->data['salespersons'] = $salespersons;
       // dd($salespersons);
        //salesleader
        $slwhere = array(
            'country_id' => $country_id,
            'is_draft' => 0,
          //  'active' => 1,
            'type' => "SL",
            '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
        );
        if($salespersonStatus != 2)
        {
            $slwhere['active'] = (int)$salespersonStatus;
        }
        $salesleader = array();
        $salesleader = $this->salespersons_history_m->getLeaderListOptions(null,$slwhere);
        $html_salespersons = $html_salesleader = "";
        if(!$salespersons){
            $html_salespersons ="<option value='0'>No Record Found</option>";
        }else{
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);
            foreach ($salespersons as $key => $value) {
                 $html_salespersons .= "<option value='" . $key . "'>" . $value . "</option>";
            }
        }
        if(!$salesleader){
            $html_salesleader = "<option value='0'>No Record Found</option>";
        }else{
            $salesleader = array_merge(array('' => 'ALL'), $salesleader);
            foreach ($salesleader as $key => $value) {
                $html_salesleader .="<option value='".$key."'>".$value."</option>";
            }
        }
        echo json_encode(array('status' => true, 'salesleader' => $html_salesleader, 'salespersons' => $html_salespersons));
        exit();
    }
	protected function _getCountrySelect() {
		// Get country list and push to drop-down box
		$cols    = array('id', 'name', 'code');
		$default = 'Select One Country';

		$countries = $this->country_m->get(null, false, 'id', $cols);

		$output = array();

		foreach ($countries as $country) {
			$output[ $country['id'] ] = '<img src="' . site_url('res/img/flags') . '/' . $country['code'] . '.png"> ' . $country['name'];
		}

		return $output;
	}

    public function test_email()
    {
        $this->mail->sendMail('jenky.w0w@gmail.com', 'Test', 'Hello, this is a test email');
    }

    public function getSalespersonByLeader(){
        $html="";
        $conditions = array();
        $post = $this->input->post();

        $leaderID = $post['sl_id'];
        $country_infor = $this->salespersons_m->getEntry('country', array('code' => $post['country_id']), false, 'id');
        if($country_infor)
        {
            $country_id = $country_infor[0]->id;
        }
        else
        {
            $country_id = $_POST['country_id'];//miss
        }
        $month = isset($_POST['month']) ? $_POST['month'] : date('m');
        if($month < 10)
        {
            $month = '0'.$month;
        }
        $year = isset($_POST['year']) ? $_POST['year'] : date('Y');
        $from = $year.'-'.$month.'-'.'01 00:00:00';
        $to = $year.'-'.$month.'-'.'31 00:00:00';
        if($leaderID != "" && $leaderID != "0"){ // condition: ALL
            $conditions = array(
                'country_id' => $country_id,
                'is_draft' => 0,
              //  'active' => 1,
                'salespersons_manager_id' => $leaderID,
                'type != "PUSH"' => null,
                '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
            );
        }else{
            $conditions = array( // condition: per SL
                'country_id' => $country_id,
                'is_draft' => 0,
              //  'active' => 1,
                'type != "PUSH"' => null,
                '(((to_date IS NULL OR to_date >= "'.$from.'") AND from_date <= "'.$to.'")
     OR ((from_date IS NULL OR from_date <= "'.$to.'") AND to_date >= "'.$from.'")
     OR (from_date >= "'.$from.'" AND from_date <= "'.$to.'")
     OR (to_date >= "'.$from.'" AND to_date <= "'.$to.'")
     OR (to_date IS NULL AND from_date IS NULL))' => null,
            );
        }
        $salespersonStatus = isset($_POST['salespersonStatus']) ? $_POST['salespersonStatus'] : 2;
        if($salespersonStatus != 2)
        {
            $conditions['active'] = (int)$salespersonStatus;
        }
        $salespersons = array();
        $salespersons = $this->salespersons_history_m->getPersonListOptions(null, $conditions);
        $cntSR = count($salespersons);

        if($cntSR <= 0){
            $html="<option value='0'>No Record Found</option>";
        }else{
            $salespersons = array_merge(array('' => 'ALL'), $salespersons);
            foreach ($salespersons as $key => $value) {
               // print_r($key);
                $html .="<option value='".$key."'>".$value."</option>";
            }    
        }

        echo $html;
        die;
    }
}