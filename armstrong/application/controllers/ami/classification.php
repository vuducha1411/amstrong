<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Classification extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('classification_m');
        $this->data['date_end_month'] = date('Y').'-'.date('m').'-'.cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')); //.' 23:59:59'
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'classification')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $this->data['classification'] = $this->classification_m->getEntry('classification', $conditions);
        return $this->render('ami/classification/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'classification_dt'));

        $this->classification_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();

        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';
            if ($this->hasPermission('edit', 'classification')) {
                $_data['buttons'] .= html_btn(site_url('ami/classification/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }
            if ($this->hasPermission('delete', 'classification')) {
                $_data['buttons'] .= html_btn(site_url('ami/classification/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }
            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'classification')) {
            return $this->noPermission();
        }

        return $this->render('ami/classification/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'classification') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/classification');
        if ($id) {
            $this->data['classification'] = $this->classification_m->get($id);
            if(isset($this->data['classification']))
            {
                $this->db->select('*');
                $this->db->from('classification_customer');
                $this->db->join('customers', 'classification_customer.armstrong_2_customers_id = customers.armstrong_2_customers_id');
                $this->db->where('classification_id', $id);
                $query = $this->db->get();
                $this->data['classification']['customer'] = $query->result_object();
            }
            $this->data['page_title'] = page_title('Edit Classification');
            return $this->render('ami/classification/edit', $this->data);
        }

        return redirect('ami/classification');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'classification') && !$this->hasPermission('edit', 'classification')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->classification_m->array_from_post(array(
            'id', 'from_date', 'to_date', 'customer', 'name',
        ));
        $id = $data['id'] ? $data['id'] : null;
        $now = date('Y-m-d H:i:s');
        $classificationdata = array(
            'from_date' => $data['from_date'].' 00-00-00',
            'to_date' => $data['to_date'].' 23-59-59',
            'name' => $data['name'],
            'country_id' => $this->country_id,
            'date_created' => $now,
            'last_updated' => $now,
            'date_sync' => $now,
        );
        if($classification_id = $this->classification_m->save($classificationdata, $id))
        {
            $this->db->where('classification_id', $classification_id);
            $this->db->delete('classification_customer');
            if($data['customer'])
            {
                foreach($data['customer'] as $customer)
                {
                    $cc = array(
                        'armstrong_2_customers_id' => $customer,
                        'classification_id' => $classification_id,
                        'country_id' => $this->country_id,
                        'date_created' => $now,
                        'last_updated' => $now,
                        'date_sync' => $now,
                    );
                    $this->db->insert('classification_customer', $cc);
                }
            }
        }
        return redirect('ami/classification');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'classification')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->classification_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/classification');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'classification')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/classification');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/classification';
                //$this->chainss_m->delete($id);
                $this->classification_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/classification/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from classification"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }
        return redirect('ami/classification');
    }

}