<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_country extends AMI_Controller
{

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_m');
    }
    public function triggerHistoryWorkingdays($data = array()){
        $work_days = json_encode($data);
        $sql = "SELECT * FROM `working_day_country` WHERE country_id = ".$this->country_id." ORDER BY `id` DESC LIMIT 0,1";
        $last_update = $this->db->query($sql)->result_object();
        if(isset($last_update[0]->work_days) && $last_update[0]->work_days == $work_days)
        {
            $this->db->where('id', $last_update[0]->id);
            $now = date('Y-m-d H:i:s');
            $data = array(
                'last_updated' => $now,
            );
            $this->db->update('working_day_country', $data);
        }
        else
        {
            $now = date('Y-m-d H:i:s');
            $data = array(
                'work_days' => $work_days,
                'country_id' => $this->country_id,
                'date_created' => $now,
                'last_updated' => $now,
                'date_sync' => $now,
            );
            $this->db->insert('working_day_country', $data);
        }
    }
    public function index()
    {
        if (!$this->hasPermission('view', 'setting_country')) {
            return $this->noPermission();
        }
        $country = $this->country_m->get($this->country_id);
        $json_decode = json_decode($country['mandatory_incall_tabs']);
        $customer_approval = json_decode($country['approve_customers']);
//        $auto_sending = json_decode($country['auto_sending']);
        $customer_history_config = (!empty($country['customer_history_config']) ? json_decode($country['customer_history_config']) : array("pull" => array("isCallHistoryDefault" => 1, "viewByDeliveryDate" => 0, "sortOrderBy" => -1), "push" => array("isCallHistoryDefault" => 1, "viewByDeliveryDate" => 0, "sortOrderBy" => -1)));
        $application_config = json_decode($country['application_config']);
        $mandatory_incall_tabs = array();

        $sql = "SELECT count(id) as count
                FROM `sku_point` 
                WHERE CURDATE() between from_date and to_date
                AND is_draft = 0
                AND country_id={$this->country_id}";
        $activeAVP = $this->db->query($sql)->row_array();

        if ($json_decode) {
            foreach ($json_decode as $k => $p) {
                $mandatory_incall_tabs[str_replace(' ', '_', strtolower($p->title))] = $p;
            }
        }
        if ($application_config) {
            foreach ($application_config as &$app_type) {
                if (isset($app_type->price_config)) {
                    if ($app_type->price_config->case == 1 || $app_type->price_config->piece == 1 || $app_type->price_config->total == 1) {
                        $app_type->price_config->enable = 1;
                    }
                }
            }

        }
        // additional field: customer_history_config

        $customer_history_sortBy = array(
            "-1" => "None",
            "0" => "Item Name",
            "1" => "Date",
            "2" => "ID",
            "3" => "Case",
            "4" => "Piece",
            "5" => "Type",
            "6" => "Source",
            "7" => "Sub Total");

        $country['mandatory_incall_tabs'] = $mandatory_incall_tabs;
        $this->data['approve_customers'] = (array)$customer_approval;
//        $this->data['auto_sending'] = (array)$auto_sending;
        $this->data['application_config'] = (array)$application_config;

        $this->data['customer_history_config'] = (array)$customer_history_config;
        $this->data['customer_history_sortBy'] = $customer_history_sortBy;
        $this->data['data'] = $country;
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Country Setting');
        $this->data['activeAVP'] = $activeAVP['count'];

        return $this->render('ami/setting_country/edit', $this->data);
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'setting_country') && !$this->hasPermission('edit', 'setting_country')) {
            return $this->noPermission();
        }
        $this->is('POST');
        $data = $this->country_m->array_from_post(array('id', 'mandatory_incall_tabs', 'approval_news_feed', 'client_time_out', 'criteria_of_sc',
            'enable_telesales', 'enable_price', 'survey_key', 'enable_sms_report', 'promotion_activity', 'enable_price_push',
            'approve_customers', 'application_config','customer_history_config'));
        // removed: auto_sending, interval_settings
        $id = $data['id'] ? $data['id'] : null;

        $mandatory_incall_tabs = '';
        $i = 0;
        if($data['mandatory_incall_tabs'])
        {
            foreach ($data['mandatory_incall_tabs'] as $k => $p) {
                if ($k == 'demo') {
                    $title = strtoupper($k);
                } else {
                    $title = ucwords(str_replace('_', ' ', $k));
                }
                $mandatory_incall_tabs[$i] = array(
                    'title' => $title,
                    'order' => (string)$i,
                    'mandatory' => $p
                );
                $i++;
            }
        }
        if (isset($data['application_config'])) {
            foreach ($data['application_config'] as $app_type => &$application) {
                // Price config
                if (isset($application['price_config'])) {
                    if ($app_type == 'pull') {
                        if (isset($application['price_config']['case']) || isset($application['price_config']['piece'])) {
                            $data['enable_price'] = 1;
                        } elseif($application['price_config']['enable'] == 2){
                            $data['enable_price'] = 2;
                        } else {
                            $data['enable_price'] = 0;
                        }
                    }
                    if ($app_type == 'push') {
                        if (isset($application['price_config']['case']) || isset($application['price_config']['piece'])) {
                            $data['enable_price_push'] = 1;
                        } elseif($application['price_config']['enable'] == 2){
                            $data['enable_price'] = 2;
                        } else {
                            $data['enable_price_push'] = 0;
                        }
                    }
                    $application['price_config']['user_setting'] = ($application['price_config']['enable'] == 2) ? 1 : 0;
                    if (!isset($application['price_config']['case'])) $application['price_config']['case'] = 0;
                    if (!isset($application['price_config']['piece'])) $application['price_config']['piece'] = 0;
                    if (!isset($application['price_config']['total'])) $application['price_config']['total'] = 0;
                    unset($application['price_config']['enable']);
                }

                // analysis data source config
                if (isset($application['analysis_datasource_config'])) {
                    if (!isset($application['analysis_datasource_config']['tfo'])) $application['analysis_datasource_config']['tfo'] = 0;
                    if (!isset($application['analysis_datasource_config']['ssd'])) $application['analysis_datasource_config']['ssd'] = 0;
                    if (!isset($application['analysis_datasource_config']['pc'])) $application['analysis_datasource_config']['pc'] = 0;
                }

                // list and availabiliy compute
                if (isset($application['list_and_availabiliy_compute'])) {
                    if (!isset($application['list_and_availabiliy_compute']['tfo'])) $application['list_and_availabiliy_compute']['tfo'] = 0;
                    if (!isset($application['list_and_availabiliy_compute']['ssd'])) $application['list_and_availabiliy_compute']['ssd'] = 0;
                    if (!isset($application['list_and_availabiliy_compute']['pc'])) $application['list_and_availabiliy_compute']['pc'] = 0;
                }

                // coaching forms
                if(isset($application['coaching_forms'])){
                    if(isset($application['coaching_forms']['areas_of_excellence_new'])) $application['coaching_forms']['areas_of_excellence_new'] = array_values(array_filter($application['coaching_forms']['areas_of_excellence_new']));
                    if(isset($application['coaching_forms']['areas_of_improvement_new'])) $application['coaching_forms']['areas_of_improvement_new'] = array_values(array_filter($application['coaching_forms']['areas_of_improvement_new']));
                    if(isset($application['coaching_forms']['development_plan_new'])) $application['coaching_forms']['development_plan_new'] = array_values(array_filter($application['coaching_forms']['development_plan_new']));
                }

                // phone length default is 10 digits number
                if(isset($application['phone_length'])){
                    if(!$application['phone_length']) $application['phone_length'] = 10;
                }
                // TODO add config for shifted call #hieu
                if (!isset($application['enable_shifted_call'])) $application['enable_shifted_call'] = 0;
                if (!isset($application['require_reason'])) $application['require_reason'] = 0;

                // migrate to push, leader
                if($app_type == 'pull'){
                    if(isset($application['sku_number'])){
                        $data['application_config']['push']['sku_number'] = $data['application_config']['leader']['sku_number'] = $application['sku_number'];
                    }

                    if (isset($application['list_and_availabiliy_compute'])) {
                        $data['application_config']['push']['list_and_availabiliy_compute'] = $data['application_config']['leader']['list_and_availabiliy_compute'] = $application['list_and_availabiliy_compute'];
                    }

                    if (isset($application['privacy'])) {
                        $data['application_config']['push']['privacy'] = $application['privacy'];
                        $data['personal_privacy_notes'] = $application['privacy']['personal_privacy_notes'];
                    }

                    if(isset($application['phone_length'])){
                        $data['application_config']['push']['phone_length'] = $data['application_config']['leader']['phone_length'] = $application['phone_length'];
                    }

                    if(isset($application['working_day'])){
                        /** 1703 trigger history working day */
                        $this->triggerHistoryWorkingdays($application['working_day']);
                        $data['application_config']['push']['working_day'] = $application['working_day'];
                    }else{
                        $data['application_config']['push']['working_day'] = $application['working_day'] = [];
                    }

                    if(isset($application['contact_strategy'])){
                        $data['application_config']['push']['contact_strategy'] = $data['application_config']['leader']['contact_strategy'] = $application['contact_strategy'];
                    }

                    if(isset($application['sales_cycle'])){
                        $data['application_config']['push']['sales_cycle'] = $data['application_config']['leader']['sales_cycle'] = $application['sales_cycle'];
                    }

                    if(isset($application['potential_customer'])){
                        $data['application_config']['push']['potential_customer'] = $data['application_config']['leader']['potential_customer'] = $application['potential_customer'];
                    }
                    // TODO add config for shifted call #hieu - copy for leader app and push app
                    if(isset($application['enable_shifted_call'])){
                        $data['application_config']['push']['enable_shifted_call'] = $data['application_config']['leader']['enable_shifted_call'] = $application['enable_shifted_call'];
                    }
                    if(isset($application['require_reason_v4'])){
                        $data['application_config']['push']['require_reason_v4'] = $data['application_config']['leader']['require_reason_v4'] = $application['require_reason_v4'];
                        if($application['require_reason_v4'] == 2)
                        {
                            $data['application_config']['push']['require_reason'] = $data['application_config']['leader']['require_reason'] = 0;
                        }
                        else
                        {
                            $data['application_config']['pull']['require_reason'] = $data['application_config']['push']['require_reason'] = $data['application_config']['leader']['require_reason'] = $application['require_reason_v4'];
                        }
                    }
                    if(isset($application['enable_ams'])){
                        $data['application_config']['push']['enable_ams'] = $data['application_config']['leader']['enable_ams'] = $application['enable_ams'];
                    }
                    if(isset($application['enable_pantry_check_warning'])){
                        $data['application_config']['push']['enable_pantry_check_warning'] = $data['application_config']['leader']['enable_pantry_check_warning'] = $application['enable_pantry_check_warning'];
                    }
                    if(isset($application['enable_enhanced_avp'])){
                        $data['application_config']['push']['enable_enhanced_avp'] = $data['application_config']['leader']['enable_enhanced_avp'] = $application['enable_enhanced_avp'];
                    }else{
                        $data['application_config']['push']['enable_enhanced_avp'] = $data['application_config']['leader']['enable_enhanced_avp'] = 0;
                        $data['application_config']['pull']['enable_enhanced_avp'] = 0;
                    }
                }
            }

            $data['application_config'] = json_encode($data['application_config']);
        } else {
            $data['application_config'] = '';
        }

        $data['customer_history_config'] = json_encode($data['customer_history_config']);
        $data['approve_customers']['add_manager'] = $data['approve_customers']['edit_manager'] = 1;
        $data['approve_customers'] = json_encode($data['approve_customers']);
//        $data['auto_sending'] = json_encode($data['auto_sending']);

        $data['mandatory_incall_tabs'] = json_encode($mandatory_incall_tabs);
        $data['last_updated'] = date('Y-m-d H:i:s');
        $result = $this->country_m->save($data, $id);
//        echo $result;
        return redirect('ami/setting_country');
    }

}