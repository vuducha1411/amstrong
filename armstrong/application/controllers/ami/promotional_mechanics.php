<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotional_mechanics extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('discount_model_range_m');
        $this->load->model('promotional_mechanics_m');
        $this->load->model('products_m');
        $this->load->model('media_m');
    }

    public function overview()
    {
        if (!$this->hasPermission('view', 'promotional_mechanics')) {
            return $this->noPermission();
        }
        return $this->render('ami/promotional_mechanics/overview', $this->data);
    }

    public function range()
    {
        if (!$this->hasPermission('view', 'promotional_mechanics')) {
            return $this->noPermission();
        }
        return $this->render('ami/promotional_mechanics/range', $this->data);
    }

    public function ajaxRangeData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'type' => 1);

        $datatables = new Datatable(array('model' => 'Discount_model_range_dt'));

        $this->discount_model_range_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'discount_model')) {
                $_data['buttons'] .= html_btn(site_url('ami/promotional_mechanics/edit_range/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'discount_model')) {
                $_data['buttons'] .= html_btn(site_url('ami/promotional_mechanics/delete_range/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function edit_range($id = null)
    {
        if (!$this->hasPermission('edit', 'promotional_mechanics') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotional_mechanics');
        if ($id) {

            $this->data['discount_model_range'] = $this->discount_model_range_m->get($id);
            $this->data['page_title'] = page_title('Edit Discount Range');
            return $this->render('ami/promotional_mechanics/edit_range', $this->data);
        }

        return redirect('ami/discount_model');
    }

    public function add_range()
    {
        if (!$this->hasPermission('add', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        return $this->render('ami/promotional_mechanics/edit_range', $this->data);
    }

    public function save_range()
    {
        if (!$this->hasPermission('add', 'promotional_mechanics') && !$this->hasPermission('edit', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->discount_model_range_m->array_from_post(array(
            'id', 'title', 'from_date', 'to_date', 'active'
        ));

        $data['country_id'] = $this->country_id;
        $data['from_date'] = $data['from_date'] . ' 00:00:00';
        $data['to_date'] = $data['to_date'] . ' 23:59:59';
        $data['type'] = 1;
        $id = $data['id'] ? $data['id'] : null;

        // Check overlap datetime
        /*$conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            'type' => 1
        );

        if ($id) $conditions['id !='] = $id;
        $date_times = $this->db->from('discount_model_range')
            ->select(['from_date', 'to_date'])
            ->where($conditions)
            ->get()->result_array();
        if ($date_times) {
            $date_arr = [$data['from_date'], $data['to_date']];
            foreach ($date_times as $date_time) {
                $date_arr[] = $date_time['from_date'];
                $date_arr[] = $date_time['to_date'];
            }
            if ($this->multipleDateRangeOverlaps($date_arr)) {
                $this->data = array(
                    'discount_model_range' => $data,
                    'errors' => 'Date has been overlaps'
                );
                return $this->render("ami/promotional_mechanics/edit_range", $this->data);
            }
        }*/

        $this->discount_model_range_m->save($data, $id);

        return redirect('ami/promotional_mechanics/range');
    }

    public function update_range()
    {
        if (!$this->hasPermission('delete', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->discount_model_range_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/promotional_mechanics/range');
    }

    public function delete_range($id = null)
    {
        if (!$this->hasPermission('delete', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotional_mechanics');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/promotional_mechanics/range';
                //$this->chainss_m->delete($id);
                $this->discount_model_range_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/promotional_mechanics/delete_range/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from discount range"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/promotional_mechanics/range');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'promotional_mechanics')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $filter = $this->input->get('filter');
        switch ($filter) {
            case "promo_1":
                $type = array('type' => 1);
                break;
            case "promo_2":
                $type = array('type' => 2);
                break;
            case "promo_3":
                $type = array('type' => 3);
                break;
            case "promo_4":
                $type = array('type' => 4);
                break;
            case "promo_5":
                $type = array('type' => 5);
                break;
            default:
                $type = array('type' => 1);
                break;
        };

        $conditions = array_merge($conditions, $type);
        $promotional_mechanics = $this->promotional_mechanics_m->get_by($conditions);
        if ($promotional_mechanics) {
            foreach ($promotional_mechanics as &$promotional_mechanic) {
                $product = $this->products_m->get_by(array('sku_number' => $promotional_mechanic['purchased_sku_number'], 'country_id' => $this->country_id), true);
                $promotional_mechanic['purchased_sku_name'] = $product ? $product['sku_name'] : '';
                $promotional_mechanic['range'] = $this->discount_model_range_m->get($promotional_mechanic['range_id']);
            }
        }
        $range = $this->discount_model_range_m->getRangeListOption(null, array_merge(array('active' => 1, 'type' => 0), $conditions));
        $this->data['range'] = $range;
        $this->data['promotional_mechanics'] = $promotional_mechanics;
        $this->data['filter'] = $filter ? $filter : 'promo_1';
        return $this->render('ami/promotional_mechanics/index', $this->data);
    }

    public function _getAddEditParam()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $products = $this->products_m->getProductListOptions('- Select -', array_merge(array('listed' => 1), $conditions), 'sku_number');
        $comparison_period = array(
            'mtd' => 'Month to Date',
            'qtd' => 'Quarter to Date',
            'ytd' => 'Year to Date'
        );
        $product_applicable = array(
            'discount' => 'Discount',
            'money' => 'Money',
            'voucher' => 'Voucher',
            'product' => 'Product'
        );
        $quantity_type = array(
            'case' => 'Case',
            'pc' => 'PC'
        );
        $range = $this->discount_model_range_m->getRangeListOption(null, array_merge(array('active' => 1, 'type' => 1), $conditions));
        return array(
            'products' => $products,
            'filter' => $this->input->get('filter') ? $this->input->get('filter') : 'promo_1',
            'comparison_period' => $comparison_period,
            'product_applicable' => $product_applicable,
            'quantity_type' => $quantity_type,
            'range' => $range
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->data = $this->_getAddEditParam();

        return $this->render('ami/promotional_mechanics/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'promotional_mechanics') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotional_mechanics');
        if ($id) {
            $this->data = $this->_getAddEditParam();
            $this->data['promotional'] = $this->promotional_mechanics_m->get($id);
            $this->data['images'] = $this->media_m->getMedia('image', 'promotional_mechanics', $this->country_id, $id, true);
            $this->data['videos'] = $this->media_m->getMedia('video', 'promotional_mechanics', $this->country_id, $id, true);
            $this->data['brochures'] = $this->media_m->getMedia('brochure', 'promotional_mechanics', $this->country_id, $id, true);
            $this->data['page_title'] = page_title('Edit Promotion');

            return $this->render('ami/promotional_mechanics/edit', $this->data);
        }

        return redirect('ami/promotional_mechanics');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'promotional_mechanics') && !$this->hasPermission('edit', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->promotional_mechanics_m->array_from_post(array(
            'id', 'type', 'name', 'sub_name', 'range_id', 'comparison_period', 'promotion_free_gift', 'active',
            'n_months', 'x_mounts', 'purchased_sku_quantity', 'purchased_sku_number', 'percentage_start_range', 'percentage_end_range',
            'free_sku_quantity', 'free_sku_name', 'free_sku_number', 'discount_percent', 'money_amount', 'purchased_sku_quantity_type', 'free_sku_quantity_type'
        ));

        $data['country_id'] = $this->country_id;
        $id = $data['id'] ? $data['id'] : null;
        if(isset($data['free_sku_number'])){
            $free_sku = $this->products_m->get_by(array('country_id' => $this->country_id, 'sku_number' => $data['free_sku_number']), true);
            if($free_sku){
                $data['free_sku_name'] = $free_sku['sku_name'];
            }
        }
        $newId = $this->promotional_mechanics_m->save($data, $id);

        if($id){
            // Remove existing media ref
            $mediaUnlink = $this->input->post('media_ref_unlink');
            if ($mediaUnlink) {
                foreach ($mediaUnlink as $unlink) {
                    $this->media_m->deleteMediaRef('promotional_mechanics', $id, $unlink);
                }
            }
        }

        $mediaChange = $this->input->post('media_ref_change');
        if ($mediaChange) {
            foreach ($mediaChange as $change) {
                $this->media_m->addMediaRef(array(
                    'entry_id' => $newId ? $newId : $id,
                    'entry_type' => 'promotional_mechanics',
                    'media_id' => $change,
                    'country_id' => $this->country_id
                ));
            }
        }

        // Add media ref
        if (!empty($this->input->post('uniqueId'))) {
            $media = $this->media_m->get_by(array('path' => $this->input->post('uniqueId')));
            if ($media) {
                foreach ($media as $item) {
                    $args = array();
                    $args['entry_id'] = $newId;
                    $args['media_id'] = $item['id'];
                    $args['entry_type'] = 'promotional_mechanics';
                    $args['country_id'] = $this->country_id;
                    $this->media_m->addMediaRef($args);
                }

            }
        }

        $filter = $this->input->post('filter') ? $this->input->post('filter') : '';
        $redirect = $filter ? 'ami/promotional_mechanics?filter=' . $filter : 'ami/promotional_mechanics';
        return redirect($redirect);
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->promotional_mechanics_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/promotional_mechanics');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'promotional_mechanics')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/promotional_mechanics');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/promotional_mechanics';
                //$this->chainss_m->delete($id);
                $this->promotional_mechanics_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/promotional_mechanics/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from promotional mechanics"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/promotional_mechanics');
    }
}