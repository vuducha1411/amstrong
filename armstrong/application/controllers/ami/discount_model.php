<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Discount_model extends AMI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('discount_model_m');
        $this->load->model('discount_model_range_m');
        $this->load->model('products_cat_web_m');
        $this->load->model('products_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'discount_model')) {
            return $this->noPermission();
        }
        return $this->render('ami/discount_model/index', $this->data);
    }

    public function range()
    {
        if (!$this->hasPermission('view', 'discount_model')) {
            return $this->noPermission();
        }
        return $this->render('ami/discount_model/range', $this->data);
    }

    public function ajaxRangeData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'type' => 0);

        $datatables = new Datatable(array('model' => 'Discount_model_range_dt'));

        $this->discount_model_range_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('edit', 'discount_model')) {
                $_data['buttons'] .= html_btn(site_url('ami/discount_model/edit_range/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($this->hasPermission('delete', 'discount_model')) {
                $_data['buttons'] .= html_btn(site_url('ami/discount_model/delete_range/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function edit_range($id = null)
    {
        if (!$this->hasPermission('edit', 'discount_model') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/discount_model');
        if ($id) {

            $this->data['discount_model_range'] = $this->discount_model_range_m->get($id);
            $this->data['page_title'] = page_title('Edit Discount Range');
            return $this->render('ami/discount_model/edit_range', $this->data);
        }

        return redirect('ami/discount_model');
    }

    public function add_range()
    {
        if (!$this->hasPermission('add', 'discount_model')) {
            return $this->noPermission();
        }

        return $this->render('ami/discount_model/edit_range', $this->data);
    }

    public function save_range()
    {
        if (!$this->hasPermission('add', 'discount_model') && !$this->hasPermission('edit', 'discount_model')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->discount_model_m->array_from_post(array(
            'id', 'title', 'from_date', 'to_date', 'active'
        ));

        $data['country_id'] = $this->country_id;
        $data['from_date'] = $data['from_date'] . ' 00:00:00';
        $data['to_date'] = $data['to_date'] . ' 23:59:59';
        $data['type'] = 0;
        $id = $data['id'] ? $data['id'] : null;

        // Check overlap datetime
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0,
            'type' => 0
        );
        if ($id) $conditions['id !='] = $id;
        $date_times = $this->db->from('discount_model_range')
            ->select(['from_date', 'to_date'])
            ->where($conditions)
            ->get()->result_array();
        if ($date_times) {
            $date_arr = [$data['from_date'], $data['to_date']];
            foreach ($date_times as $date_time) {
                $date_arr[] = $date_time['from_date'];
                $date_arr[] = $date_time['to_date'];
            }
            if ($this->multipleDateRangeOverlaps($date_arr)) {
                $this->data = array(
                    'discount_model_range' => $data,
                    'errors' => 'Date has been overlaps'
                );
                return $this->render("ami/discount_model/edit_range", $this->data);
            }
        }

        $this->discount_model_range_m->save($data, $id);

        return redirect('ami/discount_model/range');
    }

    public function update_range()
    {
        if (!$this->hasPermission('delete', 'discount_model')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->discount_model_range_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/discount_model/range');
    }

    public function delete_range($id = null)
    {
        if (!$this->hasPermission('delete', 'discount_model')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/discount_model');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/discount_model/range';
                //$this->chainss_m->delete($id);
                $this->discount_model_range_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/discount_model/delete_range/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from discount range"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/discount_model/range');
    }

    public function setting()
    {
        if (!$this->hasPermission('view', 'discount_model')) {
            return $this->noPermission();
        }
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $settings = $this->db->from('discount_model')
            ->where($conditions)->get()->result_array();
        if ($settings) {
            foreach ($settings as &$setting) {
                $discount = json_decode($setting['discount']);
                $setting['sku_number'] = str_replace(",", "<br>", $setting['sku_number']);
                $setting['tier_1_case'] = isset($discount->tier_1_case) ? $discount->tier_1_case : '';
                $setting['tier_2_case'] = isset($discount->tier_2_case) ? $discount->tier_2_case : '';
                $setting['tier_3_case'] = isset($discount->tier_3_case) ? $discount->tier_3_case : '';
                $setting['tier_1_discount'] = isset($discount->tier_1_discount) ? $discount->tier_1_discount . '%' : '';
                $setting['tier_2_discount'] = isset($discount->tier_2_discount) ? $discount->tier_2_discount . '%' : '';
                $setting['tier_3_discount'] = isset($discount->tier_3_discount) ? $discount->tier_3_discount . '%' : '';
                $setting['focus'] = $setting['focus'] == 1 ? "Low" : "High";
                $setting['type'] = $setting['type'] == 1 ? "Category" : "Products";

                // get range
                $setting['range'] = $this->discount_model_range_m->get($setting['range_id']);
            }
        }
        $range = $this->discount_model_range_m->getRangeListOption(null, array_merge(array('active' => 1, 'type' => 0), $conditions));
        $this->data['range'] = $range;
        $this->data['settings'] = $settings;
        return $this->render('ami/discount_model/setting', $this->data);
    }

    public function _getAddEditSettingParam()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $select_type = array(
            1 => 'Category',
            2 => 'Products'
        );
        $products = $this->products_m->get_by(array_merge(array('listed' => 1), $conditions));
        $cat_web = $this->products_cat_web_m->getListOptions('- Select -', $conditions);
        $range = $this->discount_model_range_m->getRangeListOption(null, array_merge(array('active' => 1, 'type' => 0), $conditions));
        return array(
            'cat_web' => $cat_web,
            'range' => $range,
            'select_type' => $select_type,
            'products' => $products
        );
    }

    public function edit_setting($id = null)
    {
        if (!$this->hasPermission('edit', 'discount_model') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/discount_model');
        if ($id) {
            $settings = $this->discount_model_m->get($id);
            $discount = [];
            if($settings['discount']){
                $discounts = objectToArray(json_decode($settings['discount']));
                $tier_total = count($discounts) / 2;
                for($i=1; $i<=$tier_total; $i++){
                    $discount[$i] = array(
                        'tier_case' => $discounts['tier_'.$i.'_case'],
                        'tier_discount' => $discounts['tier_'.$i.'_discount'],
                    );
                }
            }

            $this->data = $this->_getAddEditSettingParam();
            $this->data['discount_model'] = $settings;
            $this->data['discount'] = $discount;
            $this->data['recent_product'] = explode(',', $settings['sku_number']);
            $this->data['page_title'] = page_title('Edit Discount');
            return $this->render('ami/discount_model/edit_setting', $this->data);
        }

        return redirect('ami/discount_model');
    }

    public function add_setting()
    {
        if (!$this->hasPermission('add', 'discount_model')) {
            return $this->noPermission();
        }

        $this->data = $this->_getAddEditSettingParam();
        return $this->render('ami/discount_model/edit_setting', $this->data);
    }

    public function save_setting()
    {
        if (!$this->hasPermission('add', 'discount_model') && !$this->hasPermission('edit', 'discount_model')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $post = $this->input->post();
        $data = $this->discount_model_m->array_from_post(array(
            'cat_web_id', 'range_id', 'type', 'section'
        ));

        $data['country_id'] = $this->country_id;
        $product_cat_web = $this->products_cat_web_m->get_by(array('id' => $data['cat_web_id'], 'country_id' => $this->country_id), true);
        $data['cat_web_name'] = $product_cat_web ? $product_cat_web['name'] : '';

        if($data['type'] == 1){
            $id = $data['section'][0]['id'] ? $data['section'][0]['id'] : null;
            $data['discount'] = isset($data['section'][0]['tier']) ? json_encode($data['section'][0]['tier']) : '';
            unset($data['section']);

            $this->discount_model_m->save($data, $id);
        }else{
            foreach($data['section'] as $section){
                $data_insert = [];
                $data_insert = $data;
                $id = (isset($section['id']) && $section['id']) ? $section['id'] : null;
                $data_insert['sku_number'] = $section['sku_number'] ? implode(',', $section['sku_number']) : '';
                $data_insert['discount'] = json_encode($section['tier']);
                unset($data_insert['section']);

                $this->discount_model_m->save($data_insert, $id);
            }
        }

        return redirect('ami/discount_model/setting');
    }

    public function update_setting()
    {
        if (!$this->hasPermission('delete', 'discount_model')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->discount_model_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/discount_model/setting');
    }

    public function delete_setting($id = null)
    {
        if (!$this->hasPermission('delete', 'discount_model')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/discount_model');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/discount_model/setting';
                //$this->chainss_m->delete($id);
                $this->discount_model_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/discount_model/delete_setting/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from discount"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/discount_model/setting');
    }

    public function upload_template()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $range_id = $this->input->post('range_id');
            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                $this->session->set_flashdata('message', PluploadHandler::get_error_message());
                $this->session->set_flashdata('flash_type', 'alert-danger');
            }else{
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
                    {
                        $datas = $worksheet->toArray();
                        foreach($datas as $index => &$data){
                            if($index == 0){
                                $data[] = 'discount';
                                $data[] = 'range_id';
                            }else{
                                $discount = [];
                                if ($data[3]) $discount['tier_1_case'] = $data[3];
                                if ($data[4]) $discount['tier_1_discount'] = $data[4];
                                if ($data[5]) $discount['tier_2_case'] = $data[5];
                                if ($data[6]) $discount['tier_2_discount'] = $data[6];
                                if ($data[7]) $discount['tier_3_case'] = $data[7];
                                if ($data[8]) $discount['tier_3_discount'] = $data[8];

                                $data[] = json_encode($discount);
                                $data[] = $range_id;
                                $data[9] = $data[10] == 'high' ? 2 : 1;
                            }
                            unset($data[3]);
                            unset($data[4]);
                            unset($data[5]);
                            unset($data[6]);
                            unset($data[7]);
                            unset($data[8]);
                        }

                        $this->discount_model_m->importToDb($this->country_id, $datas, array('id'));
                    }

                    $this->session->set_flashdata('message', 'Import complete.');
                    $this->session->set_flashdata('flash_type', 'alert-success');
                }else{
                    $this->session->set_flashdata('message', 'Upload complete.');
                    $this->session->set_flashdata('flash_type', 'alert-danger');
                }
            }
        }

        return redirect('ami/discount_model/setting');
    }

    function validate()
    {
        $get = $this->input->get();
        $type = $get['type'];
        $cat_web_id = $get['cat_web_id'];
        $sku_number = isset($get['sku_number']) ? $get['sku_number'] : '';
        $range_id = $get['range_id'];
        $id = $get['id'];
        $pass = true;
        $add_query = [];
        $message = '';
        if($id){
            $add_query['id !='] = $id;
        }

        if($type == 1){
            $conditions = array(
                'country_id' => $this->country_id,
                'range_id' => $range_id,
                'cat_web_id' => $cat_web_id,
                'is_draft' => 0
            );
            $existed_data = $this->discount_model_m->get_by(array_merge($add_query, $conditions), true);
            if($existed_data){
                $pass = false;
                $message = 'Category already been set to this period time';
            }
        }elseif($type == 2){
            if($sku_number){
                foreach($sku_number as $sku){
                    $conditions = array(
                        "country_id" => $this->country_id,
                        "range_id" => $range_id,
                        "sku_number like '%{$sku}%'" => null,
                        'is_draft' => 0
                    );
                    $existed_data = $this->discount_model_m->get_by(array_merge($add_query, $conditions), true);

                    if($existed_data){
                        $pass = false;
                        $message = 'Product already been set to this period time';
                        break;
                    }
                }
            }
        }

        if(!$pass){
            echo json_encode(array(
                'success' => false,
                'type' => $type,
                'message' => $message
            ));
        }else{
            echo json_encode(array(
                'success' => true,
                'type' => $type,
                'message' => $message
            ));
        }
    }
}