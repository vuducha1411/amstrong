<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_strategy_daterange extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_strategy_daterange_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'contact_strategy_daterange')) {
            return $this->noPermission();
        }
        return $this->render('ami/contact_strategy_daterange/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('s.country_id' => $this->country_id, 's.is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Contact_strategy_daterange_dt'));

        $this->contact_strategy_daterange_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();

        foreach ($data['data'] as &$_data) {
            $id = $_data['s']['id'];

            $_data['buttons'] = '<div class="btn-group">';
            if ($this->hasPermission('edit', 'contact_strategy_daterange')) {
                $_data['buttons'] .= html_btn(site_url('ami/contact_strategy_daterange/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }
            if ($this->hasPermission('delete', 'contact_strategy_daterange')) {
                $_data['buttons'] .= html_btn(site_url('ami/contact_strategy_daterange/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }
            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'contact_strategy_daterange')) {
            return $this->noPermission();
        }

        return $this->render('ami/contact_strategy_daterange/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'contact_strategy_daterange') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contact_strategy_daterange');
        if ($id) {

            $this->data['contact_strategy_daterange'] = $this->contact_strategy_daterange_m->get($id);
            $this->data['page_title'] = page_title('Edit sales cycle');
            return $this->render('ami/contact_strategy_daterange/edit', $this->data);
        }

        return redirect('ami/contact_strategy_daterange');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'contact_strategy_daterange') && !$this->hasPermission('edit', 'contact_strategy_daterange')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->contact_strategy_daterange_m->array_from_post(array(
            'otm_A_customers_visited', 'otm_A_customers_visited_in_weeks', 'otm_B_customers_visited', 'otm_B_customers_visited_in_weeks',
            'otm_C_customers_visited', 'otm_C_customers_visited_in_weeks', 'otm_D_customers_visited', 'otm_D_customers_visited_in_weeks',
            'from_date', 'to_date', 'id'
        ));

        $data['country_id'] = $this->country_id;
        $data['from_date'] = $data['from_date'] . ' 00:00:00';
        $data['to_date'] = $data['to_date'] . ' 23:59:59';
        $id = $data['id'] ? $data['id'] : null;

        // Check overlap datetime
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0
        );
        if ($id) $conditions['id !='] = $id;
        $date_times = $this->db->from('contact_strategy_daterange')
            ->select(['from_date', 'to_date'])
            ->where($conditions)
            ->get()->result_array();
        if ($date_times) {
            $date_arr = [$data['from_date'], $data['to_date']];
            foreach ($date_times as $date_time) {
                $date_arr[] = $date_time['from_date'];
                $date_arr[] = $date_time['to_date'];
            }
            if($this->multipleDateRangeOverlaps($date_arr)){
                $this->data = array(
                    'contact_strategy_daterange' => $data,
                    'errors' => 'Date has been overlaps'
                );
                return $this->render("ami/contact_strategy_daterange/edit", $this->data);
            }
        }

        $this->contact_strategy_daterange_m->save($data, $id);

        return redirect('ami/contact_strategy_daterange');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'contact_strategy_daterange')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->contact_strategy_daterange_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/contact_strategy_daterange');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'contact_strategy_daterange')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/contact_strategy_daterange');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/contact_strategy_daterange';
                //$this->chainss_m->delete($id);
                $this->contact_strategy_daterange_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/contact_strategy_daterange/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from sales cycle"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/contact_strategy_daterange');
    }

}