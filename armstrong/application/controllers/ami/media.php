<?php

class Media extends AMI_Controller
{

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONAL DECLARATION
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('media_m');
		$this->load->helper('image_resize');
	}

	public function index()
	{
		return $this->json(array('123'));
	}

	public function ckupload()
	{
		$get = $this->input->get();
		@set_time_limit(5 * 60);

		$now = Carbon::now();
		$targetDir = $this->MEDIA_DIR . '/image/' . $now->format('Y-m') . '/';

		if (!file_exists($targetDir)) @mkdir($targetDir);

		if (file_exists($targetDir . $_FILES["upload"]["name"]))
		{
			echo $_FILES["upload"]["name"] . " already exists please choose another image. ";
		}
		else
		{
			move_uploaded_file($_FILES["upload"]["tmp_name"], $targetDir . $_FILES["upload"]["name"]);

			// Required: anonymous function reference number as explained above.
			$funcNum = $get['CKEditorFuncNum'] ;
			// Optional: instance name (might be used to load a specific configuration file or anything else).
			$CKEditor = $get['CKEditor'] ;
			// Optional: might be used to provide localized messages.
			$langCode = $get['langCode'] ;
			 
			// Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
			$url = $this->MEDIA_URL . '/image/' . $now->format('Y-m') . '/' . $_FILES["upload"]["name"];
			// Usually you will only assign something here if the file could not be uploaded.
			$message = '';

			echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		}
	}

	public function upload()
	{
		$get = $this->input->post();

		/* HTTP headers for no cache etc */
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// $targetDir = 'res/up/' . $get['class'];
		$targetDir = $this->MEDIA_DIR . '/' . $get['class'];
		if (in_array($get['entry_type'], $this->MEDIA_FOLDER)) {
			$targetDir .= '/' . $get['entry_type'];
		}

		/* 5 minutes execution time */
		@set_time_limit(5 * 60);

		/* Get parameters */
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		/* Clean the fileName for security reasons */
		//$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

		/* Hashed filename */
		$fileExt = explode('.', $fileName);
		$fileExt = $fileExt[(count($fileExt) - 1)];
		$fileName_old = $fileName;
		$fileName = uniqid() . '.' . $fileExt;

		/* Make sure the fileName is unique but only if chunking is disabled */
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) $count++;
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		/* Create target dir */
		if (!file_exists($targetDir)) @mkdir($targetDir);

		/* Look for the content type header */
		if (isset($_SERVER["HTTP_CONTENT_TYPE"])) $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"])) $contentType = $_SERVER["CONTENT_TYPE"];

		/* Handle non multipart uploads older WebKit versions didn't support multipart in HTML5 */
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				/* Open temp file */
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					/* Read binary input stream and append it to temp file */
					$in = fopen($_FILES['file']['tmp_name'], "rb");

					if ($in) {
						while ($buff = fread($in, 4096)) {
							fwrite($out, $buff);
						}
						$fileSize = round($_FILES["file"]["size"] / 1024);
						$uploaded = $this->media_m->addMedia($get['class'], $get['entry_type'], $this->country_id, $get['uniqueId'], array($fileName_old, $fileName), $fileSize);
					} else {
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					}
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
				}
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}

		} else {
			/* Open temp file */
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				/* Read binary input stream and append it to temp file */
				$in = fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096)) {
						fwrite($out, $buff);
					}
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
				fclose($in);
				fclose($out);
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}
		}

		/* Check if file has been uploaded*/
		if (!$chunks || $chunk == $chunks - 1) {
			/* Strip the temp .part suffix off */
			rename("{$filePath}.part", $filePath);
		}

		if ($get['class'] == 'image') {
			$tnPath = $targetDir . DIRECTORY_SEPARATOR . 'thumb/' . $fileName;
			copy($filePath, $tnPath);
			$sourcefile = $tnPath;
			$targetfile = $tnPath;
			$dest_x = 175;
			$dest_y = 175;
			image_resize($sourcefile, $dest_x, $dest_y, $targetfile);
		}

		die('{"jsonrpc" : "2.0", "result" : null, "id" : "' . $uploaded . '", "cleanFileName" : "' . $fileName . '", "test" : "' . $get['class'] . '"}');
	}

	public function delete($id = null)
	{
		if ($this->is('POST', false)) {
			if ($id = intval($id)) {
				$redirect = $this->input->post('redirect') ? $this->input->post('redirect') : '/';
				$this->media_m->deleteMedia($this->country_id, $id);

				return $this->json(array(
					'success' => 1,
					'message' => "Successfully deleted entry ID: {$id}",
					'redirect' => $redirect
				));
			}
		}

		return $this->json(array(
			'success' => 0,
			'message' => 'POST request only'
		));
	}

	public function view($class = null, $type = null)
	{
		if (!$class) {
			return show_error('Missing media class');
		}

		if (!$type) {
			//return show_error('Missing entry type');
		}

		/*if(!$this->input->is_ajax_request())
		{
			return redirect(site_url());
		}*/

		$this->data['media'] = $this->media_m->getMedia($class, $type, $this->country_id);
		$this->data['class'] = $class;

		// if (in_array($type, array('contacts')))
		// {
		//     $this->data['class'] = 'single';
		// }

		foreach ($this->data['media'] as &$media) {
			switch ($media['class']) {
				case 'image':
					$media['thumb'] = $this->MEDIA_URL . '/image/thumb/' . $media['path'];
					break;

				case 'video':
					$media['thumb'] = site_url('res/img/video.png');
					break;

				case 'promo_image':
					$media['thumb'] = $this->MEDIA_URL . '/promo_image/' . $media['path'];
                    $this->data['class'] = 'image';
					break;

				default:
					$media['thumb'] = site_url('res/img/pdf.png');
					break;
			}
		}
		return $this->render('ami/media/view', $this->data);
	}

	public function deleteMedia()
	{
		if ($this->is('POST', false)) {
			$data = $this->input->post();
			$mediaId = $data['id'] ?: '';
			$mediaUrl = $data['media_url'] ?: '';
			$mediaUrl = str_replace($this->config->item('base_api_url'), '', $mediaUrl);
			if($mediaId){
				$relations = $this->db->from('media_ref')
					->where(array(
						'media_id' => $mediaId,
						'is_draft' => 0,
						'country_id' => $this->country_id
					))
					->get()->row();
				if($relations){
					return $this->json(array(
						'success' => 0
					));
				}
				$this->media_m->deleteMedia($this->country_id, $mediaId);
				// Delete file
				$url = $this->config->item('base_api_url') .'restapi/media/removeMediaFile/format/json/ph/PULL-PRO3.0.2-20150120';
				$data = array(
					'url' => $mediaUrl
				);
				$options = array(
					'http' => array(
						'header' => "Content-type: application/x-www-form-urlencoded\r\n",
						'method' => 'POST',
						'content' => http_build_query($data),
					),
				);
				$context = stream_context_create($options);
				$result = file_get_contents($url, false, $context);

				return $this->json(array(
					'success' => 1
				));
			}
		}
		return $this->json(array(
			'success' => 0
		));
	}
}