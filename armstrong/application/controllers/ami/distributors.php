<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Distributors extends AMI_Controller
{

    public $customer_relate_history = array(
        'call_records', 'contacts', 'customers_dish', 'customers_dish_products', 'ssd', 'psd_dist'
    );

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('distributors_m');
        $this->load->model('salespersons_m');
        $this->load->model('groups_m');
        $this->load->model('country_channels_m');
        $this->load->model('country_sub_channels_m');
        $this->load->model('business_types_m');
        $this->load->model('chains_m');
        $this->load->library('CountryParser');
    }

    protected function _getFilter()
    {
        $type = $this->input->get('filter');

        switch ($type) {
            case 'active':
                $filter = array('c.active' => 1);
                break;

            case 'pending':
                $filter = in_array($this->country_code, array('hk', 'tw')) ? array('c.approved IN (0,2)' => null) : array('c.approved' => 0);

                break;

            case 'inactive':
                $filter = array('c.active' => 0);
                break;

            default:
                $filter = array('c.active' => 1);
                break;
        }

        return $filter;
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'distributor')) {
            return $this->noPermission();
        }

        $this->data['transfer_btn'] = 3;
        $this->data['country_id'] = $this->country_id;
        $this->data['filter'] = $this->input->get('filter') ? $this->input->get('filter') : 'active';
        return $this->render('ami/distributors/index', $this->data);
    }

    public function ajaxData()
    {
        $user = $this->session->userdata('user_data');
        $filter = $this->_getFilter();
        $_filter = $this->input->get('filter') ? $this->input->get('filter') : 'active';
        $conditions = array_merge(array('c.country_id' => $this->country_id, 'c.is_draft' => 0), $filter);
//            array('country_id' => $this->country_id, 'is_draft' => 0, 'active' =>1, 'approved' =>1);

        if (!$this->isSuperAdmin()) {
            if ($this->hasPermission('manage_staff', 'salespersons')) {
                $salespersons = $this->db->select('armstrong_2_salespersons_id')
                    ->from('salespersons')
                    ->where('salespersons_manager_id', $user['id'])
                    ->get()
                    ->result_array();

                $salespersons = array_rewrite($salespersons, 'armstrong_2_salespersons_id');
                $salespersons_new = implode('","', array_merge(array($user['id']), array_keys($salespersons)));

                if ($salespersons_new) {
                    $conditions['c.armstrong_2_salespersons_id IN ("' . $salespersons_new . '")'] = null;
                }
            }
        }

        $datatables = new Datatable(array('model' => 'Distributors_dt'));

        $this->distributors_m->setDatatalesConditions($conditions);
        $data = $datatables->datatableJson();
        foreach ($data['data'] as &$_data) {
            $_data['c']['updated_name'] = $_data['c']['updated_name']!='' ? $_data['c']['updated_name'] : 'OPS';
            if(isset($_data['c']['armstrong_2_distributors_id']))
            {
                $id = $_data['c']['armstrong_2_distributors_id'];
            }
            else
            {
                $id = '';
            }
            $_data['c']['armstrong_2_distributors_id'] = '
                <a href="' . site_url('ami/distributors/edit/' . $id) . '" data-toggle="ajaxModal">
                    ' . $id . '
                </a>
            ';

            $_data['buttons'] = '<div class="btn-group">';

            if ($this->hasPermission('view', 'contacts')) {
                $_data['buttons'] .= html_btn(site_url('ami/customers/contacts/' . $id), '<i class="fa fa-group"></i>', array('class' => 'btn-default edit', 'title' => 'Contacts'));

            }

            if ($_filter != 'pending' && $this->hasPermission('edit', 'distributor')) {
                $_data['buttons'] .= html_btn(site_url('ami/distributors/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }

            if ($_filter == 'pending') {
                if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')
                    || $this->hasPermission('manage', 'salespersons')
                ) {
                    $_data['buttons'] .= html_btn(site_url('ami/distributors/pending/' . $id . '/app'), '<i class="fa fa-thumbs-up"></i>', array('class' => 'btn-default approve', 'title' => 'Approve', 'data-toggle' => 'ajaxModal'));
                    $_data['buttons'] .= html_btn(site_url('ami/distributors/pending/' . $id . '/rej'), '<i class="fa fa-thumbs-down"></i>', array('class' => 'btn-default reject', 'title' => 'Reject', 'data-toggle' => 'ajaxModal'));
                }
            }

            if ($_filter != 'pending' && $this->hasPermission('delete', 'distributor')) {
                $_data['buttons'] .= html_btn(site_url('ami/distributors/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }

            $_data['buttons'] .= '</div>';
            switch ($_data['c']['approved']) {
                case -1:
                    $_data['c']['approved'] = 'Reject';
                    break;
                case -2:
                    $_data['c']['approved'] = 'Close down';
                    break;
                case 0:
                    $_data['c']['approved'] = 'Pending';
                    break;
                case 1:
                    $_data['c']['approved'] = 'Approved';
                    break;
                case 2:
                    $_data['c']['approved'] = 'Pending When Edit';
                    break;

            }
//            $_data['c']['is_perfect_store'] = $_data['c']['is_perfect_store'] ?  'Yes' : 'No';
        }
        return $this->json($data);
    }

    public function draft()
    {
        if (!$this->hasPermission('view', 'distributor')) {
            return $this->noPermission();
        }

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 1);

        $this->data['distributors'] = $this->distributors_m->get_limit($this->getPageLimit(), $conditions);
        $this->data['total'] = $this->distributors_m->count('where', $conditions);
        $this->data['draft'] = true;
        $this->data['page_title'] = page_title('Distributors');

        return $this->render('ami/distributors/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $user = $this->session->userdata('user_data');
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->country_id == 6) {
            if ($user['type'] == 'SL' && $user['sub_type'] == 'asm') {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1, "(salespersons_manager_id ='" . $user['id'] . "' OR (type like '%SL%' OR type like '%ADMIN%'))" => null);
            } else {
                $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1);
            }
        } else {
            $conditions_salespersons = array('country_id' => $this->country_id, 'is_draft' => 0, 'active' => 1,
                '(type LIKE "%push%" OR type LIKE "%mix%" OR type like "%TRAINER%")' => NULL
            );
        }
        $chains = $this->chains_m->getChainListOption('- Select -', array_merge($conditions, array('type' => 1, 'approved IN (0,1,2)' => null)));
        $cols = array('id', 'name');

        $hierachy = $this->countryparser->parse_hierachy($this->country_code);
        $root = $this->countryparser->parse($this->country_code)->get($hierachy['root']);

        $rootValues = array('' => '-Select-');

        foreach ($root as $key => $value) {
            $rootValues[$value] = $value;
        }

        $salespersons = $this->salespersons_m->getPersonListOptions('- Select -', $conditions_salespersons, false);
        if(isset($this->data['distributor']['armstrong_2_salespersons_id']))
        {
            if(!array_key_exists($this->data['distributor']['armstrong_2_salespersons_id'], $salespersons))
            {
                if($temp_salespersons = $this->salespersons_m->get_by(array('armstrong_2_salespersons_id' => $this->data['distributor']['armstrong_2_salespersons_id'])))
                {
                    $salespersons[$this->data['distributor']['armstrong_2_salespersons_id']] = $temp_salespersons[0]['armstrong_2_salespersons_id'].' - '.$temp_salespersons[0]['first_name'].' '.$temp_salespersons[0]['last_name'];
                }
            }
        }
        $salespersons_str = substr(implode('","', array_keys($salespersons)), 2);
        $list_customers = [];
        if ($salespersons_str) {
            $customers_conditions = array(
                'armstrong_2_salespersons_id IN (' . $salespersons_str . '")' => null,
                "is_draft" => 0
            );
            $customers = $this->distributors_m->get_by($customers_conditions);

            foreach ($customers as $key => $_customer) {
                $list_customers[$_customer['armstrong_2_salespersons_id']][$_customer['armstrong_2_distributors_id']] = $_customer['armstrong_2_distributors_id'] . ' - ' . $_customer['name'];
            }
        }
        return array(
            'groups' => $this->groups_m->parse_form($cols, '- Select -', array_merge(array('type' => 1), $conditions)),
            // 'salespersons' => $this->salespersons_m->getPersonListOptions('- Select -', $conditions),
            'country_channels' => $this->country_channels_m->parse_form($cols, '- Select -', $conditions),
            'country_sub_channels' => $this->country_sub_channels_m->parse_form($cols, '- Select -', $conditions),
            'business_types' => $this->business_types_m->parse_form($cols, '- Select -', $conditions),
            'assigned_distributors' => $this->distributors_m->parse_form(array('armstrong_2_distributors_id', 'name'), '- Select -', $conditions),
            'countryHierachy' => $hierachy,
            'rootValues' => $rootValues,
            'chains' => $chains,
            'salespersons' => $salespersons,
            'customers' => $list_customers
        );
    }

    public function transfer_customers()
    {
        if (!$this->hasPermission('add', 'distributor')) {
            return $this->noPermission();
        }
        $this->data = $this->_getAddEditParams();

        return $this->render('ami/distributors/transfer_customers', $this->data);
    }

    public function transfer_customers_do()
    {
        if (!$this->hasPermission('edit', 'distributor') && !$this->hasPermission('add', 'distributor')) {
            return $this->noPermission();
        }
        $post = $this->input->post();
        $customers_id = isset($post['customers']) ? $post['customers'] : '';
        $salesperson_new = isset($post['armstrong_2_salespersons_id_new']) ? $post['armstrong_2_salespersons_id_new'] : '';
        if ($customers_id) {
            $date_time = date('Y-m-d H:i:s');
            $customers_id = str_replace(',', '","', $customers_id);
            // Update relate table
            $data_item_update = array(
                'armstrong_2_customers_id IN ("' . $customers_id . '")' => null,
                'is_draft' => 0
            );
            foreach ($this->customer_relate_history as $table) {
                $this->db->where($data_item_update)
                    ->update($table, array('last_updated' => $date_time));
            }

            // get list customer by armstrong_2_salespersons_id
            $customers = $this->distributors_m->get_by(array('armstrong_2_distributors_id IN ("' . $customers_id . '")' => null));
            // update armstrong_2_salespersons_id
            foreach ($customers as $customer) {
                if ($customer['armstrong_2_distributors_id']) {
                    $this->distributors_m->save(array(
                        'armstrong_2_salespersons_id' => $salesperson_new
                    ), $customer['armstrong_2_distributors_id']);
                }
            }
        }
        $this->data = $this->_getAddEditParams();

        return $this->render('ami/distributors/transfer_customers', $this->data);
    }

    // tuantq 6/4/2015
    public function transfer_view()
    {

        if (!$this->hasPermission('add', 'distributor')) {
            return $this->noPermission();
        }
        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        $this->data['distributor'] = array('approved' => 1);
        return $this->render('ami/distributors/transfer', $this->data);
    }

    public function transfer_do($id = null, $id_new = null)
    {

        if (!$this->hasPermission('edit', 'distributor') && !$this->hasPermission('add', 'distributor')) {
            return $this->noPermission();
        }

//        $this->is('POST');
//        $fields = array('armstrong_2_salespersons_id', 'armstrong_2_salespersons_id_new');
//        $data = $this->distributors_m->array_from_post($fields);

        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        $this->data['distributor'] = array('approved' => 1);
        $this->data['distributor']['armstrong_2_salespersons_id'] = $id;
        $this->data['distributor']['armstrong_2_salespersons_id_new'] = $id_new;
        if ($id == $id_new) {
            $this->data['customer']['armstrong_2_salespersons_id'] = $id;
            $this->data['customer']['armstrong_2_salespersons_id_new'] = $id_new;
            return $this->render('ami/distributors/transfer', $this->data);
        }

        // get list customer by armstrong_2_salespersons_id
        $distributors = $this->distributors_m->get_by(array('armstrong_2_salespersons_id' => $id));

        // update armstrong_2_salespersons_id
        foreach ($distributors as $distributor) {
            if ($distributor['armstrong_2_distributors_id']) {
                $this->distributors_m->save(array(
                    'armstrong_2_salespersons_id' => $id_new
                ), $distributor['armstrong_2_distributors_id']);
            }
        }

        return $this->render('ami/distributors/transfer', $this->data);
    }

    public function show_distributors($id = null)
    {
        if ($id == null || $id == '') {
            $this->data['show_table'] = 0;
        } else {
            $this->data['show_table'] = 1;
        }
//        $this->data['customers'] = $customers = $this->customers_m->get_by(array('armstrong_2_salespersons_id' => $id));
        $this->data['show_by_salespersons'] = 1;
        $this->data['country_id'] = $this->country_id;
        $this->data += $this->_getAddEditParams();
        $this->data['distributor'] = array('approved' => 1);
        $this->data['distributor']['armstrong_2_salespersons_id'] = $id;

//        echo(json_encode($this->data));

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0, 'armstrong_2_salespersons_id' => $id);
        $this->data['distributors'] = $this->distributors_m->get_by($conditions);
//            array(
//            'active' => $this->distributors_m->get_by(array_merge($conditions, array('active' => 1))),
//            'inactive' => $this->distributors_m->get_by(array_merge($conditions, array('active' => 0))),
//            'pending' => $this->distributors_m->get_by(array_merge($conditions, array('approved' => 0)))
//        );
//        dd($this->db->last_query());
        $this->data['total'] = $this->distributors_m->count('where', $conditions);
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Distributors');
//        $this->data['show_table'] = 1;
        return $this->render('ami/distributors/transfer', $this->data);
    }
    private function getOtm(){
        return array(
            '' => '- Select -',
            'N/A' => 'N/A',
            'D' => 'D',
            'C' => 'C',
            'B' => 'B',
            'A' => 'A'
        );
    }
    public function add()
    {
        if (!$this->hasPermission('add', 'distributor')) {
            return $this->noPermission();
        }
        $this->data['otm'] = $this->getOtm();
        $this->data += $this->_getAddEditParams();
        $this->data['distributor']['active'] = 1;
        return $this->render('ami/distributors/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'distributor') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }
        $this->data['otm'] = $this->getOtm();

        $this->_assertId($id, 'ami/distributors');

        if ($id) {
            $this->data['distributor'] = $this->distributors_m->get($id);
            $this->assertCountry($this->data['distributor']);
//            if (!empty($this->data['distributor']['armstrong_2_salespersons_id']))
//            {
//                $this->data['salespersons'] = $this->salespersons_m->get($this->data['distributor']['armstrong_2_salespersons_id']);
//            }

            $this->data['page_title'] = page_title('Edit Distributors');

            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'distributor')) {
                    return $this->noPermission();
                }

                if ($this->country_id == 6) {
                    $salesperson = $this->salespersons_m->get_by(array(
                        'armstrong_2_salespersons_id' => $this->data['distributor']['armstrong_2_salespersons_id']
                    ));
                    if ($salesperson) {
                        $this->data['distributor']['salesperson_name'] = $salesperson[0]['first_name'] . ' ' . $salesperson[0]['last_name'];
                    }
                }
                return $this->render('ami/distributors/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();

                $countryValues = array();

                foreach ($this->data['countryHierachy'] as $key => $value) {
                    if (!empty($this->data['countryHierachy'][$key])) {
                        if ($key == 'root') {
                            $countryValues[$value] = array_merge(array('' => '-Select-'), $this->countryparser->parse($this->country_code)->get($value));
                        } else {
                            $countryValues[$value] = (!empty($this->data['distributor'][$key]) && $this->countryparser->getBy($value, $this->data['distributor'][$key]))
                                ? $this->countryparser->getBy($value, $this->data['distributor'][$key])
                                : array();
                        }
                    } else {
                        $countryValues[$value] = array();
                    }
                }

                $this->data['countryValues'] = $countryValues;
//                echo '<pre>';
//                print_r($this->data);
//                echo '</pre>';
//                exit;
                return $this->render('ami/distributors/edit', $this->data);
            }
        }

        return redirect('ami/distributors');
    }

    public function pending($id, $app)
    {

        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        $this->_assertId($id, 'ami/distributors');
        if ($id) {

            $distributorTable = 'distributors';
            $distributor = $this->db->select("{$distributorTable}.*,

                country_channels.name as country_channels_name,
                country_sub_channels.name as sub_channel_name,


                customers_types.name as customers_types_name,

                business_types.name as business_type_name")
                ->from($distributorTable)
                ->join('country_channels', "{$distributorTable}.country_channels_id = country_channels.id", 'left')
                ->join('country_sub_channels', "{$distributorTable}.country_sub_channels_id = country_sub_channels.id", 'left')
                ->join('customers_types', "{$distributorTable}.customers_types_id = customers_types.id", 'left')
                ->join('business_types', "{$distributorTable}.business_types_id = business_types.id", 'left')
                ->where("{$distributorTable}.armstrong_2_distributors_id", $id)
                ->get()
                ->row_array();

//            dd($distributor);
//            $customer['cuisine_channels_id'] = $customer['cuisine_channels_name'];

            $distributor['channel'] = $distributor['country_channels_name'];
            $distributor['sub_channel'] = $distributor['sub_channel_name'];


            $distributor['customers_types_id'] = $distributor['customers_types_name'];

            $distributor['business_type'] = $distributor['business_type_name'];

            $this->data['distributor'] = $distributor;
            $this->data['app'] = $app;

            $this->assertCountry($this->data['distributor']);

            if ($this->input->is_ajax_request()) {
                $this->data['amendedData'] = array();

                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['distributor']['approved'] == 2) {
                    $this->data['amendedData'] = null;
//                        $this->_getAmendedData($this->data['wholesaler']['armstrong_2_wholesalers_id']);
                }

                $this->data['distributor'] = array_except($this->data['distributor'], array(
                    'country_channels_name', 'sub_channel_name',
                    'customers_types_name', 'business_type_name'));
                return $this->render('ami/distributors/pending_action');
            }
        }

        return redirect('ami/distributors?filter=pending');
    }

    public function approve($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/distributors');

        if ($id) {
            $this->data['distributor'] = $this->distributors_m->get($id);
            $this->assertCountry($this->data['distributor']);
            $this->data['page_title'] = page_title('Approve Distributors');
            $this->data['approve'] = true;

            if ($this->input->is_ajax_request()) {
                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['distributor']['approved'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['distributor']['armstrong_2_distributors_id']);
                }

                return $this->render('ami/distributors/pending_action', $this->data);
            } else if ($this->is('POST')) {
                $data_old = $this->distributors_m->get($id);
                $this->distributors_m->save(array(
                    'approved' => 1,
                    'active' => 1,
                    'updated_by' => $this->user_id,
                    'updated_name' => $this->user_name
                ), $id);
                $this->customers_action_v2($data_old, $this->distributors_m->get($id), 'distributors');
            }
        }

        return redirect('ami/distributors?filter=pending');
    }

    public function reject($id = null)
    {
        $hasPermission = false;
        if ($this->hasPermission('manage_staff', 'salespersons') || $this->hasPermission('manage_all', 'salespersons')) {
            $hasPermission = true;
        }

        if (!$hasPermission) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/distributors');

        if ($id) {
            $this->data['distributor'] = $this->distributors_m->get($id);
            $this->assertCountry($this->data['distributor']);
            $this->data['page_title'] = page_title('Reject Distributors');

            if ($this->input->is_ajax_request()) {
                if (in_array($this->country_code, array('hk', 'tw')) && $this->data['distributor']['approve'] == 2) {
                    $this->data['amendedData'] = $this->_getAmendedData($this->data['distributor']['armstrong_2_distributors_id']);
                }

                return $this->render('ami/distributors/pending_action', $this->data);
            } else if ($this->is('POST')) {
                if ($this->data['distributor']['approved'] == 0) {
                    $data_save = array(
                        'approved' => -1,
                        'is_draft' => 1
                    );
                } else {
                    $data_save = array(
                        'approved' => -1,
                        'active' => 1
                    );
                }
                $data_old = $this->distributors_m->get($id);
                $this->distributors_m->save($data_save, $id);
                $this->customers_action_v2($data_old, $this->distributors_m->get($id), 'distributors');
//                $pendingEdit = $this->db->from($this->distributors_amended_m->getTableName())
//                    ->where('armstrong_2_distributors_id', $id)
//                    ->get()->row_array();
//
//                if ($pendingEdit)
//                {
//                    $this->db->where(array('id' => $pendingEdit['id']))
//                        ->update($this->distributors_amended_m->getTableName(), array(
//                            'distributor_old_data' => $pendingEdit['distributor_new_data'],
//                            'distributor_new_data' => $pendingEdit['distributor_old_data'],
//                        ));
//                }
            }
        }

        return redirect('ami/distributors?filter=pending');
    }


    public function update()
    {
        if (!$this->hasPermission('delete', 'distributor')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->distributors_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/distributors');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'distributor')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/distributors');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/distributors';
                //$this->distributors_m->delete($id);
                $this->distributors_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/distributors/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Distributors"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/distributors');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'distributor')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/distributors');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/distributors/draft';
                $this->distributors_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/distributors/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Distributors"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/distributors');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'distributor') && !$this->hasPermission('edit', 'distributor')) {
            return $this->noPermission();
        }

        $this->is('POST');


        $data = $this->distributors_m->array_from_post(array('name', 'groups_id', 'asst_manager', 'armstrong_1_distributors_id', 'sifu_id', 'armstrong_2_salespersons_id', 'armstrong_2_secondary_salespersons_id',
            'is_mother', 'armstrong_2_related_mother_id', 'approved', 'ssd_id', 'sap_cust_name', 'street_address', 'city', 'postal_code', 'phone', 'phone_2', 'sms_number', 'email', 'email_2', 'email_3', 'line_id', 'suburb', 'region',
            'armstrong_2_chains_id', 'state', 'district', 'province', 'fax', 'update_info', 'active', 'country_channels_id', 'country_sub_channels_id', 'otm',
            'business_types_id', 'assigned_distributor_1', 'assigned_distributor_2', 'contact_details', 'armstrong_2_distributors_id', 'sap_id', 'account_number','is_perfect_store', 'disable_line'));

        $data['country_id'] = $this->country_id;

        $id = $data['armstrong_2_distributors_id'] ? $data['armstrong_2_distributors_id'] : null;
        $user = $this->session->userdata('user_data');
        $data['updated_by'] = $user['id'];
        $data['updated_name'] = $user['name'];
        // create new record
        if (!$id) {
            $data['armstrong_2_distributors_id'] = ''; //$this->distributors_m->generateArmstrongId($this->country_id, 'distributors');
        }
        // Update Call Records
        $date_time = date('Y-m-d H:i:s');
        $data_update = array(
            'armstrong_2_customers_id' => $data['armstrong_2_distributors_id'],
            'is_draft' => 0
        );
        foreach ($this->customer_relate_history as $table) {
            $this->db->where($data_update)
                ->update($table, array('last_updated' => $date_time));
        }

        $data_old = $this->distributors_m->get($id);
        if (!$id) {
            $this->distributors_m->save($data, null, true, 'INSERT', $this->country_id);
        }
        else
        {
            $this->distributors_m->save($data, $id);
        }
        $this->customers_action_v2($data_old, $this->distributors_m->get($id), 'distributors');
        return redirect('ami/distributors');
    }

    public function export($type)
    {
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
            || !$this->hasPermission('view', 'distributors')
        ) {
            return redirect(site_url('ami/distributors'));
        }

        $distributors = $this->distributors_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($distributors);

        return $this->excel($sheet, 'distributors', $type);
    }

    public function import()
    {
        if ($this->is('POST')) {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) {
                return $this->json(array(
                    'OK' => 0,
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } else {
                if (!empty($upload['path'])) {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $this->distributors_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');
        $salespersons = $this->input->get('salespersons');
        if (!$term) {
            return $this->json(array());
        }
        $conditions = array("armstrong_2_distributors_id LIKE '%{$term}%'", "name LIKE '%{$term}%'");
        $conditions = '(' . implode(' OR ', $conditions) . ')';

        if($this->input->get('action') == 'ssd') {
            $this->db
                ->where_in('approved', array(1,2))
                ->where('country_id', $this->country_id)
                ->where('active', 1)
                ->where('is_draft', 0)
                ->where($conditions);
        }
        else
        {
            $this->db
                ->where('country_id', $this->country_id)
                ->where_in('approved', 1)
                ->where('active', 1)
                ->where('is_draft', 0)
                ->where($conditions);
        }

        /*if ($salespersons && $this->country_id != 4) {
            $this->db->where('armstrong_2_salespersons_id', $salespersons);
        }*/

        $distributors = $this->db->limit(30)
            ->get($this->distributors_m->getTableName())
            ->result_array();

        $output = array();

        foreach ($distributors as $distributor) {
            $output[] = array(
                'value' => $distributor['armstrong_2_distributors_id'],
                'label' => $distributor['armstrong_2_distributors_id'] . ' - ' . $distributor['name']
            );
        }

        return $this->json($output);
    }
}
