<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Call_records extends AMI_Controller {

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONAL DECLARATION
    |--------------------------------------------------------------------------
    */
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('call_records_m');
        $this->load->model('salespersons_m');
        $this->load->model('customers_m');
        $this->load->model('route_plan_m');
    }

    public function index() 
    {
        if (!$this->hasPermission('view', 'call_records'))
        {
            return $this->noPermission();
        }

        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }
        // $this->data['callRecords'] = $this->call_records_m->get_limit($this->getPageLimit(), $conditions);
        // $this->data['total'] = $this->call_records_m->count('where', $conditions);       
        
        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons'))
//        {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Call Records');

        return $this->render('ami/call_records/index', $this->data);
    }

    public function draft() 
    {
        if (!$this->hasPermission('view', 'call_records'))
        {
            return $this->noPermission();
        }

        // $conditions = array('country_id' => $t:his->country_id, 'is_draft' => 1);

        // $this->data['callRecords'] = $this->call_records_m->get_limit($this->getPageLimit(), $conditions);
        // $this->data['total'] = $this->call_records_m->count('where', $conditions);       
        
        $defaultOptions = array('' => ' - Select - ');

        if ($this->hasPermission('manage', 'salespersons'))
        {
            $defaultOptions['ALL'] = 'ALL';
        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));
        $this->data['draft'] = true;

        return $this->render('ami/call_records/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');

        $routePlanTable = $this->route_plan_m->getTableName();
        $salespersonsTable = $this->salespersons_m->getTableName();

        $existingRoutePlans = $this->db->select("{$routePlanTable}.armstrong_2_route_plan_id")
            ->from($routePlanTable)
            ->join($salespersonsTable, "{$routePlanTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
            ->where(array(
                "{$routePlanTable}.is_draft" => 0,
                "{$salespersonsTable}.country_id" => $this->country_id
            ))
            ->get()->result_array();

        $existingRoutePlans = array_rewrite($existingRoutePlans, 'armstrong_2_route_plan_id');

        $plans = $this->route_plan_m->getRoutePlanListOptions(array('country_id' => $this->country_id));
        $plans = array_except($plans , array_keys($existingRoutePlans));

        return array(
            'salespersons' => $this->salespersons_m->getPersonListOptions(' - Select - ', $conditions),
            'customers' => $this->customers_m->getCustomerListOptions(' - Select - ', $conditions),
            'plans' => array_merge(array('' => '- Select -'), $plans)
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        return $this->render('ami/call_records/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('view', 'call_records') && !$this->input->is_ajax_request())
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/call_records');

        if ($id)
        {
            $this->data['record'] = $this->call_records_m->get($id); 
            $this->assertCountry($this->data['record']);           
            $this->data['page_title'] = page_title('Edit Call Records');            

            $this->data += $this->_getAddEditParams();

            if ($this->input->is_ajax_request())
            {
                if (!$this->hasPermission('view', 'call_records'))
                {
                    return $this->noPermission();
                }

                return $this->render('ami/call_records/preview', $this->data);
            }
            else
            {
                // $this->data += $this->_getAddEditParams();
                return $this->render('ami/call_records/edit', $this->data);
            }
        }

        return redirect('ami/call_records');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) 
        {            
            if ($id)
            {
                $this->call_records_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/call_records');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/call_records');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/call_records';
                //$this->call_records_m->delete($id);
                $this->call_records_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/call_records/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Call Records"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/call_records');        
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/call_records');

        if ($id)
        {
            if ($this->is('POST', false))
            {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/call_records/draft';
                $this->call_records_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            }
            else
            {
                $params = array(
                    'action' => site_url("ami/call_records/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Call Records"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/call_records');        
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'call_records') && !$this->hasPermission('edit', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->call_records_m->array_from_post(array('armstrong_2_customers_id', 'armstrong_2_salespersons_id',
            'armstrong_2_route_plan_id', 'datetime_call_start', 'datetime_call_end', 'armstrong_2_call_records_id'));

        $data['country_id'] = $this->country_id;

        $id = $data['armstrong_2_call_records_id'] ? $data['armstrong_2_call_records_id'] : null;

        $data['version'] = 'ami';
        
        // create new record
        if (!$id)
        {
           // $data['armstrong_2_call_records_id'] = $this->call_records_m->generateArmstrongId($this->country_id, 'call_records');
            $newId = $this->call_records_m->save($data, $id, true, 'INSERT', $this->country_id);
        }
        else
        {
            $newId = $this->call_records_m->save($data, $id);
        }
        // var_dump($data);die;

        return redirect('ami/call_records');
    }   

    public function export($type)
    {    
        if (!in_array(strtolower($type), array('csv', 'xls', 'xlsx'))
        || !$this->hasPermission('view', 'call_records'))
        {
            return redirect(site_url('ami/call_records'));
        }

        $records = $this->call_records_m->get_by(array('country_id' => $this->country_id));

        $sheet = $this->makeSheet($records);

        return $this->excel($sheet, 'call_records', $type);
    }

    public function import()
    {
        if ($this->is('POST'))
        {
            PluploadHandler::no_cache_headers();
            PluploadHandler::cors_headers();

            $upload = PluploadHandler::handle(array(
                'target_dir' => 'res/up/import',
                'allow_extensions' => 'csv,xls,xlsx'
            ));

            if (!$upload) 
            {
                return $this->json(array(
                    'OK' => 0, 
                    'error' => array(
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    )
                ));
            } 
            else 
            {
                if (!empty($upload['path']))
                {
                    $objPHPExcel = PHPExcel_IOFactory::load($upload['path']);

                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) 
                    {                        
                        $this->call_records_m->importToDb($this->country_id, $worksheet->toArray(), array('id'));
                    }

                    return $this->json(array(
                        'success' => true,
                        'message' => 'Import complete'
                    ));
                }

                return $this->json(array(
                    'success' => false,
                    'message' => 'Upload complete'
                ));
            }
        }
    }

    public function records()
    {
        if (!$this->hasPermission('view', 'call_records'))
        {
            return $this->noPermission();
        }

        $this->is('POST');
        if ($id = $this->input->post('id'))
        {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date'));

            // if ($startDate->eq($endDate))
            // {
                $endDate->addDay();
            // }

            $callRecordsTable = 'call_records_map_only_route_plan';
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();
            $distributorsTable = 'distributors';
            $wholesalersTable = 'wholesalers';

            $conditions = array(
                // 'armstrong_2_salespersons_id' => $id, 
                "{$callRecordsTable}.is_draft" => $this->input->post('draft'),
                "{$callRecordsTable}.datetime_call_start >=" => $startDate->toDateTimeString(),
                "{$callRecordsTable}.datetime_call_start <=" => $endDate->toDateTimeString(),
                "{$callRecordsTable}.country_id" => $this->country_id
            );

            if ($id[0] == 'ALL')
            {
                // $this->data['records'] = $this->call_records_m->get_by($conditions, false);
                $this->data['records'] = $this->db->select("{$callRecordsTable}.*,
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($callRecordsTable)
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where(array_merge($conditions, array("{$salespersonsTable}.country_id" => $this->country_id)))
                    ->get()->result_array();


            }
            else
            {
                // $this->data['records'] = $this->db->where_in('armstrong_2_salespersons_id', $id)
                //     ->where($conditions)
                //     ->get($this->call_records_m->getTableName())
                //     ->result_array();

                $this->data['records'] = $this->db->select("{$callRecordsTable}.*, 
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                    {$customersTable}.armstrong_2_customers_name
                ")
                    ->from($callRecordsTable)
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id")
                    ->where_in("{$callRecordsTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();

                $distributors_records = $this->db->select("{$callRecordsTable}.*,
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name,
                    {$distributorsTable}.name
                ")
                    ->from($callRecordsTable)
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($distributorsTable, "{$callRecordsTable}.armstrong_2_customers_id = {$distributorsTable}.armstrong_2_distributors_id")
                    ->where_in("{$callRecordsTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();
                $wholesalers_records = $this->db->select("{$callRecordsTable}.*,
                    {$salespersonsTable}.first_name, {$salespersonsTable}.last_name,
                    {$wholesalersTable}.name
                ")
                    ->from($callRecordsTable)
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id")
                    ->join($wholesalersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$wholesalersTable}.armstrong_2_wholesalers_id")
                    ->where_in("{$callRecordsTable}.armstrong_2_salespersons_id", $id)
                    ->where(array_merge($conditions, array("{$salespersonsTable}.country_id" => $this->country_id)))
                    ->get()->result_array();
                $this->data['records'] = array_merge($this->data['records'],$distributors_records);
                $this->data['records'] = array_merge($this->data['records'],$wholesalers_records);
            }

            // $this->data['records'] = $this->call_records_m->get_by($conditions, false);
            $this->data['draft'] = $this->input->post('draft');

            return $this->render('ami/call_records/record', $this->data);
        }

        return '';
    }

    public function info()
    {
        $id = $this->input->get('id');

        if (!$id)
        {
            return $this->json(array());
        }

        return $this->json($this->call_records_m->get($id));
    }

    public function tokenfield()
    {
        $term = $this->input->get('term');

        if (!$term)
        {
            return $this->json(array());
        }
        
        // $conditions = array ("armstrong_2_customers_name LIKE '%{$term}%'", "armstrong_2_customers_id LIKE '%{$term}%'");
        // $conditions = '(' . implode(' OR ', $conditions) . ')';

        $callRecords = $this->db
            ->where('country_id', $this->country_id)
            ->or_like('armstrong_2_call_records_id', $term)
            // ->or_like('last_name', $term)
            ->limit(30)
            // ->where($conditions)
            ->get($this->call_records_m->getTableName())
            ->result_array();

        $output = array();

        foreach ($callRecords as $callRecord) 
        {
            $output[] = array(
                'value' => $callRecord['armstrong_2_call_records_id'],
                'label' => $callRecord['armstrong_2_call_records_id']
            );
        }

        return $this->json($output);
    }
}