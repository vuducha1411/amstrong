<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales_cycle extends AMI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sales_cycle_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'sales_cycle')) {
            return $this->noPermission();
        }
        return $this->render('ami/sales_cycle/index', $this->data);
    }

    public function ajaxData()
    {

        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);

        $datatables = new Datatable(array('model' => 'Sales_cycle_dt'));

        $this->sales_cycle_m->setDatatalesConditions($conditions);

        $data = $datatables->datatableJson();

        foreach ($data['data'] as &$_data) {
            $id = $_data['id'];

            $_data['buttons'] = '<div class="btn-group">';
            if ($this->hasPermission('edit', 'sales_cycle')) {
                $_data['buttons'] .= html_btn(site_url('ami/sales_cycle/edit/' . $id), '<i class="fa fa-edit"></i>', array('class' => 'btn-default edit', 'title' => 'Edit'));
            }
            if ($this->hasPermission('delete', 'sales_cycle')) {
                $_data['buttons'] .= html_btn(site_url('ami/sales_cycle/delete/' . $id), '<i class="fa fa-remove"></i>', array('class' => 'btn-default delete', 'title' => 'Delete', 'data-toggle' => 'ajaxModal'));
            }
            $_data['buttons'] .= '</div>';
        }

        return $this->json($data);
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'sales_cycle')) {
            return $this->noPermission();
        }

        return $this->render('ami/sales_cycle/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'sales_cycle') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sales_cycle');
        if ($id) {

            $this->data['sales_cycle'] = $this->sales_cycle_m->get($id);
            $this->data['page_title'] = page_title('Edit sales cycle');
            return $this->render('ami/sales_cycle/edit', $this->data);
        }

        return redirect('ami/sales_cycle');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'sales_cycle') && !$this->hasPermission('edit', 'sales_cycle')) {
            return $this->noPermission();
        }

        $this->is('POST');
        $data = $this->sales_cycle_m->array_from_post(array(
            'from_date', 'to_date', 'id'
        ));

        $data['country_id'] = $this->country_id;
        $data['from_date'] = $data['from_date'] . ' 00:00:00';
        $data['to_date'] = $data['to_date'] . ' 23:59:59';
        $id = $data['id'] ? $data['id'] : null;

        // Check overlap datetime
        $conditions = array(
            'country_id' => $this->country_id,
            'is_draft' => 0
        );
        if ($id) $conditions['id !='] = $id;
        $date_times = $this->db->from('sales_cycle')
            ->select(['from_date', 'to_date'])
            ->where($conditions)
            ->get()->result_array();
        if ($date_times) {
            $date_arr = [$data['from_date'], $data['to_date']];
            foreach ($date_times as $date_time) {
                $date_arr[] = $date_time['from_date'];
                $date_arr[] = $date_time['to_date'];
            }
            if($this->multipleDateRangeOverlaps($date_arr)){
                $this->data = array(
                    'sales_cycle' => $data,
                    'errors' => 'Date has been overlaps'
                );
                return $this->render("ami/sales_cycle/edit", $this->data);
            }
        }

        $this->sales_cycle_m->save($data, $id);

        return redirect('ami/sales_cycle');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'sales_cycle')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            if ($id) {
                $this->sales_cycle_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/sales_cycle');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'sales_cycle')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sales_cycle');

        if ($id) {
            if ($this->is('POST', false)) {

                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/sales_cycle';
                //$this->chainss_m->delete($id);
                $this->sales_cycle_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/sales_cycle/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from sales cycle"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/sales_cycle');
    }

}