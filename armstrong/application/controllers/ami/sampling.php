<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sampling extends AMI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sampling_m');
        $this->load->model('salespersons_m');
        $this->load->model('call_records_m');
        $this->load->model('customers_m');
        $this->load->model('distributors_m');
        $this->load->model('wholesalers_m');
        $this->load->model('products_m');
        $this->load->model('promotions_m');
        $this->load->model('recipes_m');
    }

    public function index()
    {
        if (!$this->hasPermission('view', 'sampling')) {
            return $this->noPermission();
        }

        // default conditions
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        if ($this->hasPermission('manage_staff', 'salespersons')) {
            $conditions = $this->getListSalespersonsConditions();
        }

        $defaultOptions = array('' => ' - Select - ');

//        if ($this->hasPermission('manage', 'salespersons')) {
            $defaultOptions['ALL'] = 'ALL';
//        }

        $this->data['salespersons'] = array_merge($defaultOptions, $this->salespersons_m->getPersonListOptions(null, $conditions, true));

        $this->data['draft'] = false;
        $this->data['page_title'] = page_title('Sampling');

        return $this->render('ami/sampling/index', $this->data);
    }

    protected function _getAddEditParams()
    {
        $conditions = array('country_id' => $this->country_id, 'is_draft' => 0);
        $cols = array('id', 'name');
        $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';

        return array(
            'salespersons' => $this->salespersons_m->getPersonListOptions('-Select-', array_merge($conditions, array('type not like "%sl%"' => null))),
            'products' => $this->convertProductArray(array_rewrite($this->products_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'), $product_title),
            'recipes' => $this->convertProductArray(array_rewrite($this->recipes_m->get_by(array_merge(array('listed' => 1), $conditions)), 'id'), 'name', 'id'),
            'promotions' => $this->convertProductArray(array_rewrite($this->promotions_m->get_by(array_merge(array('listed' => 1, 'quantity_case > ' => 0), $conditions)), 'sku_number'), $product_title),
            'item_type' => [0 => 'Product', 1 => 'Recipe', 2 => 'Benefit', 3 => 'Video', 4 => 'Promotion'],
            'type' => [0 => 'Dry', 1 => 'Wet', 2 => 'Demo']
        );
    }

    public function add()
    {
        if (!$this->hasPermission('add', 'sampling')) {
            return $this->noPermission();
        }

        $this->data += $this->_getAddEditParams();
        $this->data['customers'] = [];
        $this->data['call_records'] = [];
        return $this->render('ami/sampling/edit', $this->data);
    }

    public function edit($id = null)
    {
        if (!$this->hasPermission('edit', 'sampling') && !$this->input->is_ajax_request()) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sampling');

        if ($id) {
            $sampling = $this->sampling_m->get($id);


            if (!empty($sampling['sampling'])) {
                $sampling['sampling_array'] = json_decode($sampling['sampling'], true);

                if (is_array($sampling['sampling_array'])) {
                    $products = [];
                    foreach ($sampling['sampling_array'] as $val) {
                        $pro_table = 'products';
                        $sku_number_column = 'sku_number';

                        if ($val['item_type'] == '1') {
                            $pro_table = 'recipes';
                            $sku_number_column = 'id';
                        } elseif ($val['item_type'] == '4') {
                            $pro_table = 'promotions';
                        }
                        $product = $this->db->from($pro_table)
                            ->where(array(
                                'country_id' => $this->country_id,
                                'listed' => 1,
                                $sku_number_column => $val['sku_number']
                            ))->get()->result_array();
                        if ($product) {
                            $products[] = $product[0];
                        }
                    }

                    $sampling['products'] = $products;
                }
            }

            $this->data['page_title'] = page_title('Edit Sampling');
            if ($this->input->is_ajax_request()) {
                if (!$this->hasPermission('view', 'sampling')) {
                    return $this->noPermission();
                }

                return $this->render('ami/sampling/preview', $this->data);
            } else {
                $this->data += $this->_getAddEditParams();
                $call_record = $this->call_records_m->get_by(array('armstrong_2_call_records_id' => $sampling['armstrong_2_call_records_id']), false, null, 'armstrong_2_salespersons_id, armstrong_2_customers_id');
                $customers = $call_records_list = $distributors = $wholesalers = [];
                if ($call_record) {
                    $call_record = $call_record[0];
                    $conditions = array(
                        'armstrong_2_salespersons_id' => $call_record['armstrong_2_salespersons_id'],
                        'country_id' => $this->country_id,
                        'active' => 1,
                        'is_draft' => 0
                    );
                    $customers = $this->customers_m->getCustomerListOptions('-Select-', $conditions);
                    $distributors = $this->distributors_m->getDistributorListOptions('-Select-', $conditions);
                    $wholesalers = $this->wholesalers_m->getSalerListOptions('-Select-', $conditions);
                    $call_records_list = $this->call_records_m->getCallRecordListOptions('-Select-', array(
                        'armstrong_2_salespersons_id' => $call_record['armstrong_2_salespersons_id'],
                        'armstrong_2_customers_id' => $call_record['armstrong_2_customers_id'],
                        'country_id' => $this->country_id,
                        'is_draft' => 0
                    ));
                }
                $benefits = $this->db->from('products')
                    ->join('media_ref', 'media_ref.entry_id = products.id', 'left')
                    ->join('media', 'media.id = media_ref.media_id', 'left')
                    ->where(array(
                        'media.class' => 'brochure',
                        'media_ref.entry_type' => 'products',
                        'media_ref.is_draft' => 0,
                        'media.is_draft' => 0,
                        'products.country_id' => $this->country_id,
                        'products.is_draft' => 0,
                        'products.listed' => 1
                    ))
                    ->group_by('products.sku_number')
                    ->get()->result_array();

                $products_video = $this->db->from('products')
                    ->join('media_ref', 'media_ref.entry_id = products.id', 'left')
                    ->join('media', 'media.id = media_ref.media_id', 'left')
                    ->where(array(
                        'media.class' => 'video',
                        'media_ref.entry_type' => 'products',
                        'media_ref.is_draft' => 0,
                        'media.is_draft' => 0,
                        'products.country_id' => $this->country_id,
                        'products.is_draft' => 0,
                        'products.listed' => 1
                    ))
                    ->group_by('products.sku_number')
                    ->get()->result_array();
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';
                $this->data['benefits'] = $this->convertProductArray(array_rewrite($benefits, 'sku_number'), $product_title, 'sku_number');
                $this->data['products_video'] = $this->convertProductArray(array_rewrite($products_video, 'sku_number'), $product_title, 'sku_number');
                $this->data['sampling'] = array_merge($sampling, $call_record);
                $this->data['customers'] = array_merge($customers, $distributors, $wholesalers);
                $this->data['call_records'] = $call_records_list;
                return $this->render('ami/sampling/edit', $this->data);
            }
        }

        return redirect('ami/sampling');
    }

    public function update()
    {
        if (!$this->hasPermission('delete', 'sampling')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $ids = $this->input->post('ids');

        foreach ($ids as $id) {
            if ($id) {
                $this->sampling_m->save(array(
                    'is_draft' => $this->input->post('draft') ? 0 : 1,
                    'last_updated' => get_date()
                ), $id);
            }
        }

        return redirect('ami/sampling');
    }

    public function delete($id = null)
    {
        if (!$this->hasPermission('delete', 'sampling')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sampling');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/sampling';
                //$this->pantry_check_m->delete($id);
                $this->sampling_m->save(array(
                    'is_draft' => 1,
                    'last_updated' => get_date()
                ), $id, false, 'DELETE');

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/sampling/delete/{$id}"),
                    'message' => "Deleting entry with ID: {$id} from Sampling"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/sampling');
    }

    public function restore($id = null)
    {
        if (!$this->hasPermission('delete', 'sampling')) {
            return $this->noPermission();
        }

        $this->_assertId($id, 'ami/sampling');

        if ($id) {
            if ($this->is('POST', false)) {
                $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'ami/sampling/draft';
                $this->sampling_m->save(array(
                    'is_draft' => 0,
                    'last_updated' => get_date()
                ), $id);

                return redirect($redirect);
            } else {
                $params = array(
                    'action' => site_url("ami/sampling/restore/{$id}"),
                    'message' => "Restore entry with ID: {$id} from Sampling"
                );

                return $this->render('ami/components/modal_form', $params);
            }
        }

        return redirect('ami/sampling');
    }

    public function save()
    {
        if (!$this->hasPermission('add', 'sampling') && !$this->hasPermission('edit', 'sampling')) {
            return $this->noPermission();
        }

        $this->is('POST');

        $data = $this->sampling_m->array_from_post(array('armstrong_2_call_records_id', 'comments', 'armstrong_2_sampling_id', 'products'));

        $id = $data['armstrong_2_sampling_id'] ? $data['armstrong_2_sampling_id'] : null;
        $data['version'] = 'ami';
        if ($data['products']) {
            foreach ($data['products'] as $key => $product) {
                if (!$product['sku_number']) {
                    unset($data['products'][$key]);
                }
            }
        }
        $data['products'] = array_values($data['products']);
        $data['sampling'] = $data['products'] ? json_encode($data['products']) : '';
        unset($data['products']);
        // create new record
        if (!$id) {
            //$data['armstrong_2_sampling_id'] = $this->sampling_m->generateArmstrongId($this->country_id, 'sampling');
            $newId = $this->sampling_m->save($data, null, true, 'INSERT', $this->country_id);
        }
        else
        {
            $newId = $this->sampling_m->save($data, $id);
        }
        return redirect('ami/sampling');
    }

    public function sampling_data()
    {
        if (!$this->hasPermission('view', 'pantry_check')) {
            return $this->noPermission();
        }

        $this->is('POST');

        if ($id = $this->input->post('id')) {
            $startDate = parse_datetime($this->input->post('start_date'));
            $endDate = parse_datetime($this->input->post('end_date'));

            $endDate->addDay();

            $samplingTable = $this->sampling_m->getTableName();
            $callRecordsTable = $this->call_records_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $customersTable = $this->customers_m->getTableName();

            $conditions = array(
                "{$samplingTable}.is_draft" => $this->input->post('draft'),
                "{$samplingTable}.date_created >=" => $startDate->toDateTimeString(),
                "{$samplingTable}.date_created <=" => $endDate->toDateTimeString(),
            );

            if ($id[0] == 'ALL') {
                $this->data['samplings'] = $this->db->select("{$samplingTable}.*, 
                        {$callRecordsTable}.armstrong_2_salespersons_id, {$callRecordsTable}.armstrong_2_customers_id, 
                        {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                        {$customersTable}.armstrong_2_customers_name
                    ")
                    ->from($samplingTable)
                    ->join($callRecordsTable, "{$samplingTable}.armstrong_2_call_records_id = {$callRecordsTable}.armstrong_2_call_records_id", 'left')
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'left')
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id", 'left')
                    ->where(array_merge($conditions, array("{$callRecordsTable}.country_id" => $this->country_id)))
                    ->get()->result_array();
            } else {

                $this->data['samplings'] = $this->db->select("{$samplingTable}.*, 
                        {$callRecordsTable}.armstrong_2_salespersons_id, {$callRecordsTable}.armstrong_2_customers_id, 
                        {$salespersonsTable}.first_name, {$salespersonsTable}.last_name, 
                        {$customersTable}.armstrong_2_customers_name
                    ")
                    ->from($samplingTable)
                    ->join($callRecordsTable, "{$samplingTable}.armstrong_2_call_records_id = {$callRecordsTable}.armstrong_2_call_records_id", 'left')
                    ->join($salespersonsTable, "{$callRecordsTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'left')
                    ->join($customersTable, "{$callRecordsTable}.armstrong_2_customers_id = {$customersTable}.armstrong_2_customers_id", 'left')
                    ->where_in("{$callRecordsTable}.armstrong_2_salespersons_id", $id)
                    ->where($conditions)
                    ->get()->result_array();
            }

            $this->data['draft'] = $this->input->post('draft');

            return $this->render('ami/sampling/sampling', $this->data);
        }

        return '';
    }

    public function getCustomerFromSalesperson()
    {
        $this->is('POST');
        $post = $this->input->post();
        $output = '';
        $armstrong_2_salespersons_id = $post['armstrong_2_salespersons_id'];
        $conditions = array(
            'armstrong_2_salespersons_id' => $armstrong_2_salespersons_id,
            'country_id' => $this->country_id,
            'active' => 1,
            'is_draft' => 0
        );
        $customers = $this->customers_m->get_by($conditions, false, null, 'armstrong_2_customers_id as customer_id, armstrong_2_customers_name as customer_name');
        $distributors = $this->distributors_m->get_by($conditions, false, null, 'armstrong_2_distributors_id as customer_id, name as customer_name');
        $wholesalers = $this->wholesalers_m->get_by($conditions, false, null, 'armstrong_2_wholesalers_id as customer_id, name as customer_name');
        $customers_list = array_merge($customers, $distributors, $wholesalers);
        if ($customers_list) {
            foreach ($customers_list as $cus_info) {
                $output .= "<option value='{$cus_info['customer_id']}'>{$cus_info['customer_id']} - {$cus_info['customer_name']}</option>";
            }
        }
        echo $output;
        exit;
    }

    public function getCallFromSalesCus()
    {
        $this->is('POST');
        $post = $this->input->post();
        $output = '';
        $armstrong_2_salespersons_id = $post['armstrong_2_salespersons_id'];
        $armstrong_2_customers_id = $post['armstrong_2_customers_id'];
        $conditions = array(
            'armstrong_2_salespersons_id' => $armstrong_2_salespersons_id,
            'armstrong_2_customers_id' => $armstrong_2_customers_id,
            'country_id' => $this->country_id,
            'is_draft' => 0
        );
        $call_record = $this->call_records_m->get_by($conditions, false, null, 'armstrong_2_call_records_id');
        if ($call_record) {
            foreach ($call_record as $call) {
                $output .= "<option value='{$call['armstrong_2_call_records_id']}'>{$call['armstrong_2_call_records_id']}</option>";
            }
        }
        echo $output;
        exit();
    }

    public function getProductByItemType()
    {
        $this->is('POST');
        $post = $this->input->post();
        $item_type = $post['item_type'];
        $conditions = array(
            'products.country_id' => $this->country_id,
            'products.is_draft' => 0,
            'products.listed' => 1
        );
        $array_key_rewrite = 'sku_number';
        $output = '';
        switch ($item_type) {
            case 0:
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';
                $this->db->select('*');
                $this->db->from('products');
                $this->db->where($conditions);
                $this->db->group_by('products.sku_number');
                break;
            case 1:
                $array_key_rewrite = 'id';
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'name';
                $this->db->select('*');
                $this->db->from('recipes');
                $this->db->where(array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    'listed' => 1
                ));
                break;
            case 2:
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';
                $this->db->select('*');
                $this->db->from('products');
                $this->db->join('media_ref', 'media_ref.entry_id = products.id', 'left');
                $this->db->join('media', 'media.id = media_ref.media_id', 'left');
                $this->db->where(array_merge($conditions, array(
                    'media.class' => 'brochure',
                    'media_ref.entry_type' => 'products',
                    'media_ref.is_draft' => 0,
                    'media.is_draft' => 0
                )));
                $this->db->group_by('products.sku_number');
                break;
            case 3:
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';
                $this->db->select('*');
                $this->db->from('products');
                $this->db->join('media_ref', 'media_ref.entry_id = products.id', 'left');
                $this->db->join('media', 'media.id = media_ref.media_id', 'left');
                $this->db->where(array_merge($conditions, array(
                    'media.class' => 'video',
                    'media_ref.entry_type' => 'products',
                    'media_ref.is_draft' => 0,
                    'media.is_draft' => 0
                )));
                $this->db->group_by('products.sku_number');
                break;
            case 4:
                $product_title = in_array($this->country_id, array(3, 6)) ? 'name_alt' : 'sku_name';
                $this->db->select('*');
                $this->db->from('promotions');
                $this->db->where(array(
                    'country_id' => $this->country_id,
                    'is_draft' => 0,
                    'listed' => 1
                ));
                $this->db->group_by('sku_number');
                break;
            default:
                break;
        }
        $query = $this->db->get()->result_array();
        $result = $this->convertProductArray(array_rewrite($query, $array_key_rewrite), $product_title, $array_key_rewrite);
        if ($result) {
            foreach ($result as $id => $name) {
                $output .= "<option value='{$id}'>{$name}</option>";
            }
        }
        echo $output;
        exit;
    }

    function convertProductArray($products, $title = 'sku_name', $key = 'sku_number')
    {
        $output[''] = '-Select-';
        foreach ($products as $product) {
            $packing_size = (isset($product['quantity_case']) && isset($product['weight_pc'])) ? " ({$product['quantity_case']}x{$product['weight_pc']}) " : '';
            $price = isset($product['price']) ? '-' . number_format($product['price'], 2) : '';
            $output[$product[$key]] = $product[$key] . ' - ' . $product[$title] . $packing_size . $price;
        }
        return $output;
    }
}