<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spin_and_win extends PublicController
{
    public $countries = array();
    public $data = array();
    public $now;

    protected $_table;

    protected $_seasons = array(
        array('01-15', '03-15'),
        array('04-01', '05-31'),
        // Test
        // array('01-15', '03-20'),
        // array('03-21', '05-15'),

        array('05-16', '07-15'),
        array('07-16', '09-15'),
        array('09-16', '11-15'),
        array('11-16', '01-15'),
    );

    protected $_allowedSeasons = [
        'day', 'week', 'season',
        'season1', 'season2'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('prize_m');
        $this->load->model('prizes_setting_m');
        $this->load->model('salespersons_m');
        $this->load->model('call_records_m');
        $this->load->model('route_plan_m');
        $this->load->model('roles_m');

        $this->load->library('user_agent');

        $this->now = Carbon::now();
        $this->_table = $this->prize_m->getTableName();
        $countries = $this->db->get('country')->result_array();
        $this->countries = $countries;
        $this->data['countries'] = $countries;

        // Set filter condition for the site.
        if (!$this->session->userdata('spin_and_win_filter_condition')) {
            $this->_setFilter('season');
        }

        $this->data['week'] = ceil(date('j', strtotime('today')) / 7);
        $this->data['filter'] = $this->_getFilter();
        //$this->data['season'] = $this->_detectSeason();

        //if (!$this->data['season']) {
        //    dd('No season found');
        //}
    }

    public function _getPrevSeason()
    {
        $seasons = $this->db->select_max('season')->get($this->prizes_setting_m->getTableName())->row();
        return $seasons;
    }

    protected function _getFilter()
    {

        $filter = array(
            'filter' => $this->session->userdata('spin_and_win_filter_condition'),
            'season' => $this->session->userdata('spin_and_win_filter_season_condition')
        );
        //$filter = $this->session->userdata('spin_and_win_filter_condition');

//        if (!$filter || !in_array($filter, $this->_allowedSeasons)) {
//            $filter = 'season';
//        }

        return $filter;
    }

    protected function _setFilter($filter, $season = null)
    {
        if ($season) {
            $this->session->set_userdata(array('spin_and_win_filter_season_condition' => $season));
        } else {
            $season = $this->_getPrevSeason();
            $this->session->set_userdata(array('spin_and_win_filter_season_condition' => $season->season));
        }
        $this->session->set_userdata(array('spin_and_win_filter_condition' => $filter));
    }

    protected function _getSeasonDate($season = false)
    {
        if ($season) {
            $season_date = $this->db->from('prizes_setting')->select(array('date_start', 'date_expired'))->where(array('season' => $season))->get()
                ->row();
        } else {
            $season = $this->_getPrevSeason();
            $season_date = $this->db->from('prizes_setting')->select(array('date_start', 'date_expired'))->where(array('season' => $season->season))->get()
                ->row();
        }

        $date = array(
            date('Y-m-d', strtotime($season_date->date_start)),
            date('Y-m-d', strtotime($season_date->date_expired))
        );

        return $date;
    }

    protected function _detectSeason($keyOnly = false)
    {
        // Automatic detect current season
        // foreach ($this->_seasons as $key => $season)
        // {
        //     // list($startDate, $endDate) = $this->_parseSeason($season);

        //     // if ($this->now->gte($startDate) && $this->now->lte($endDate))
        //     if ($this->now->between($startDate, $endDate))
        //     {
        //         if ($keyOnly)
        //         {
        //             return $key;
        //         }

        //         return array($startDate, $endDate);
        //     }
        // }

        // return false;

        switch ($this->_getFilter()) {
            case 'season1':
                $season = 0;
                break;

            default:
                $season = 1;
                break;
        }

        return $this->_parseSeason($this->_seasons[$season]);
    }

    protected function _parseSeason($season)
    {
        list($start, $end) = $season;

        $start = explode('-', $start);
        $end = explode('-', $end);

        $startDate = Carbon::createFromDate($start[0], $start[1], $start[2])->startOfDay();
        $endDate = Carbon::createFromDate($end[0], $end[1], $end[2])->endOfDay();

        return array($startDate, $endDate);
    }

    protected function _getCycle($startDate, $endDate)
    {
        $startDateTs = $startDate->format('U');
        $endDateTs = $endDate->format('U');

        $separatedTs = $startDateTs + (($endDateTs - $startDateTs) / 2);

        return Carbon::createFromTimestamp($separatedTs);
    }

    protected function _getWinner()
    {
        $salespersonsTable = $this->salespersons_m->getTableName();

        $where = array(
            "{$this->_table}.prize_type" => 1,
            "{$this->_table}.is_spined" => 1,
            "{$this->_table}.is_valid" => 1,
            "{$this->_table}.is_draft" => 0,
            "{$salespersonsTable}.is_draft" => 0,
            "{$salespersonsTable}.active" => 1,
        );
        $where = array_merge($where, $this->_getFilterConditions($this->_getFilter()));

        $winner = $this->db->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where($where)
            ->order_by("{$this->_table}.last_updated", 'desc')
            ->get()->row_array();

        return $winner;
    }

    protected function _getTopWinners()
    {
        $salespersonsTable = $this->salespersons_m->getTableName();

        $conditions = $this->_getFilterConditions($this->_getFilter());

        $topWinners1 = $this->db->select("*, COUNT({$this->_table}.id) AS first_prize_count")
            ->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where(array(
                    "{$this->_table}.prize_type" => 2,
                    "{$this->_table}.is_spined" => 1,
                    "{$this->_table}.is_valid" => 1,
                    "{$this->_table}.is_draft" => 0,
                    "{$salespersonsTable}.is_draft" => 0,
                    "{$salespersonsTable}.active" => 1,
                ) + $conditions)->group_by("{$this->_table}.armstrong_2_salespersons_id")
            ->order_by('first_prize_count', 'desc')
            ->get()->result_array();

        $topWinners2 = $this->db->select("*, COUNT({$this->_table}.id) AS second_prize_count")
            ->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where(array(
                    "{$this->_table}.prize_type" => 3,
                    "{$this->_table}.is_spined" => 1,
                    "{$this->_table}.is_valid" => 1,
                    "{$this->_table}.is_draft" => 0,
                    "{$salespersonsTable}.is_draft" => 0,
                    "{$salespersonsTable}.active" => 1,
                ) + $conditions)->group_by("{$this->_table}.armstrong_2_salespersons_id")
            ->order_by('second_prize_count', 'desc')
            ->get()->result_array();

        $topWinners3 = $this->db->select("*, COUNT({$this->_table}.id) AS mega_prize_count")
            ->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where(array(
                    "{$this->_table}.prize_type" => 1,
                    "{$this->_table}.is_spined" => 1,
                    "{$this->_table}.is_valid" => 1,
                    "{$this->_table}.is_draft" => 0,
                    "{$salespersonsTable}.is_draft" => 0,
                    "{$salespersonsTable}.active" => 1,
                ) + $conditions)->group_by("{$this->_table}.armstrong_2_salespersons_id")
            ->order_by('mega_prize_count', 'desc')
            ->get()->result_array();


        $topWinners = array();

        foreach ($topWinners1 as $topWinner1) {
            $topWinners[$topWinner1['armstrong_2_salespersons_id']] = $topWinner1;
            $topWinners[$topWinner1['armstrong_2_salespersons_id']]['second_prize_count'] = 0;
            $topWinners[$topWinner1['armstrong_2_salespersons_id']]['mega_prize_count'] = 0;
        }

        foreach ($topWinners2 as $topWinner2) {
            if (isset($topWinners[$topWinner2['armstrong_2_salespersons_id']])) {
                $topWinners[$topWinner2['armstrong_2_salespersons_id']]['second_prize_count'] = $topWinner2['second_prize_count'];
            } else {
                $topWinners[$topWinner2['armstrong_2_salespersons_id']] = $topWinner2;
                $topWinners[$topWinner2['armstrong_2_salespersons_id']]['first_prize_count'] = 0;
                $topWinners[$topWinner2['armstrong_2_salespersons_id']]['mega_prize_count'] = 0;
            }
        }

        foreach ($topWinners3 as $topWinner3) {
            if (isset($topWinners[$topWinner3['armstrong_2_salespersons_id']])) {
                $topWinners[$topWinner3['armstrong_2_salespersons_id']]['mega_prize_count'] = $topWinner3['mega_prize_count'];
            } else {
                $topWinners[$topWinner3['armstrong_2_salespersons_id']] = $topWinner2;
                $topWinners[$topWinner3['armstrong_2_salespersons_id']]['first_prize_count'] = 0;
                $topWinners[$topWinner3['armstrong_2_salespersons_id']]['second_prize_count'] = 0;
            }
        }

        usort($topWinners, function ($a, $b) {
            $rdiff = $b['first_prize_count'] - $a['first_prize_count'];
            if ($rdiff) return $rdiff;
            return $b['second_prize_count'] - $a['second_prize_count'];
        });

        return $topWinners;
    }

    protected function _getFilterConditions($filter, $field = null)
    {
        // Store filter to session
        // $this->session->set_userdata(array('spin_and_win_filter_condition' => $filter));

        if (is_null($field)) {
            $field = $this->_table . '.last_updated';
        }
        $_filter = $filter['filter'];
        switch ($_filter) {
            case 'day':
                $conditions = array(
                    "DATE({$field})" => 'CURDATE()',
                );
                break;

            case 'week':
                $conditions = array(
                    "{$field} >= " => $this->now->startOfWeek()->toDateTimeString(),
                    "{$field} <= " => $this->now->endOfWeek()->toDateTimeString(),
                );
                break;

            case 'season':
//            case 'season1':
//            case 'season2':
                list($startDate, $endDate) = $this->_parseSeason($this->_getSeasonDate($filter['season']));
                $conditions = array(
                    "{$field} >= " => $startDate->toDateTimeString(),
                    "{$field} <= " => $endDate->toDateTimeString(),
                );
                //dd($conditions);
                break;

            default:
                $conditions = array();
                break;
        }

        return $conditions;
    }

    // Winner page
    public function index()
    {

        $this->data['winner'] = $this->_getWinner();
        if ($this->data['winner']) {
            $callRecordsTable = $this->call_records_m->getTableName();
            $routePlansTable = $this->route_plan_m->getTableName();
            $salespersonsTable = $this->salespersons_m->getTableName();
            $where = array(
                "{$this->_table}.armstrong_2_salespersons_id" => $this->data['winner']['armstrong_2_salespersons_id'],
                "{$this->_table}.armstrong_2_call_records_id != " => '',
                "{$this->_table}.is_valid" => 1,
                "{$salespersonsTable}.is_draft" => 0,
                "{$salespersonsTable}.active" => 1,
            );
            $where = array_merge($where, $this->_getFilterConditions($this->_getFilter()));
            $coins = $this->db
                ->select("count(*) as total")
                ->from($this->_table)
                ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                ->where($where)
                ->get()->row_array();
            $where_call = array(
                "{$callRecordsTable}.is_draft" => 0,
                "{$routePlansTable}.is_draft" => 0,
                "{$callRecordsTable}.armstrong_2_salespersons_id" => $this->data['winner']['armstrong_2_salespersons_id'],
                "DATE({$callRecordsTable}.datetime_call_start) = DATE({$routePlansTable}.shifted_date)" => null,
            );
            $where_call = array_merge($where_call, $this->_getFilterConditions($this->_getFilter(), $callRecordsTable . '.datetime_call_start'));
            $calls = $this->db->select("count(*) as total")
                ->from($callRecordsTable)
                ->join($routePlansTable, "{$callRecordsTable}.armstrong_2_route_plan_id = {$routePlansTable}.armstrong_2_route_plan_id", 'inner')
                ->where($where_call)
                ->get()->row_array();

            $this->data['winner']['coins'] = isset($coins['total']) ? $coins['total'] : 0;
            $this->data['winner']['calls'] = isset($calls['total']) ? $calls['total'] : 0;
        }

        $this->data['topWinners'] = $this->_getTopWinners();
        $this->data['season'] = $this->_getPrevSeason();
        $this->data['filter'] = $this->_getFilter();
        return $this->render('spinwin/winner');
    }

    public function country_stats()
    {

        $this->data['stats'] = array();

        $conditions = $this->_getFilterConditions($this->_getFilter());

        $callRecordsTable = $this->call_records_m->getTableName();
        $routePlansTable = $this->route_plan_m->getTableName();
        $salespersonsTable = $this->salespersons_m->getTableName();

        foreach ($this->countries as $country) {
            $prizeTypes = $this->db->select('prize_type, count(prize_type) as prize_type_num')
                ->where(array(
                        'is_draft' => 0,
                        'is_spined' => 1,
                        'is_valid' => 1,
                        'country_id' => $country['id'],
                    ) + $conditions)->group_by('prize_type')
                ->get($this->_table)->result_array();

            $_prizeTypes = array();

            foreach ($prizeTypes as $prize) {
                switch ($prize['prize_type']) {
                    case 0:
                        $key = 'na';
                        break;

                    case 1:
                        $key = 'special';
                        break;

                    case 2:
                        $key = 'medium';
                        break;

                    case 3:
                        $key = 'small';
                        break;

                    default:
                        $key = 'unknown';
                        break;
                }
                $_prizeTypes[$key] = $prize['prize_type_num'];
            }

            $this->data['prizeTypes'][$country['id']] = $_prizeTypes;
        }

        $where_call = array(
            "{$this->_table}.is_draft" => 0,
            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
            "{$this->_table}.armstrong_2_call_records_id != " => '',
            "{$this->_table}.is_valid" => 1,
            "{$salespersonsTable}.is_draft" => 0,
            "{$salespersonsTable}.active" => 1,
        );
        $where_call = array_merge($where_call, $this->_getFilterConditions($this->_getFilter()));
        $calls = $this->db->select("{$this->_table}.country_id, count(*) as total")
            ->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where($where_call)->group_by("{$this->_table}.country_id")
            ->order_by('total', 'desc')
            ->get()->result_array();

        $this->data['calls'] = array_rewrite($calls, 'country_id');

        $players = $this->db->select("count(distinct {$this->_table}.armstrong_2_salespersons_id) as total_players, {$this->_table}.country_id")
            ->from($this->_table)
            ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
            ->where(array(
                    "{$this->_table}.is_draft" => 0,
                    "{$this->_table}.is_valid" => 1,
                    "{$salespersonsTable}.is_draft" => 0,
                    "{$salespersonsTable}.active" => 1,
                ) + $conditions)->group_by("{$this->_table}.country_id")
            ->get()->result_array();

        $this->data['players'] = array_rewrite($players, 'country_id');
        $this->data['season'] = $this->_getPrevSeason();

        return $this->render('spinwin/country_stats');
    }

    public function perfect_calls()
    {
        $filter = $this->_getFilter();

        // if ($filter == 'season')
        if ($filter['filter'] == 'season') {
            return $this->_perfect_calls_season();
        }

        return $this->_perfect_calls_filter($filter);
    }

    protected function _perfect_calls_filter()
    {

        $callRecordsTable = $this->call_records_m->getTableName();
        $routePlansTable = $this->route_plan_m->getTableName();
        $salespersonsTable = $this->salespersons_m->getTableName();
        $rolesTable = $this->roles_m->getTableName();

        $results = array();

        foreach ($this->countries as $country) {
            $where = array(
                "{$this->_table}.is_draft" => 0,
                "{$this->_table}.is_valid" => 1,
                "{$this->_table}.country_id" => $country['id'],
                "{$salespersonsTable}.is_draft" => 0,
                "{$salespersonsTable}.active" => 1,
            );
            $where = array_merge($where, $this->_getFilterConditions($this->_getFilter()));

            $players = $this->db->select("
				distinct({$this->_table}.armstrong_2_salespersons_id), 
				{$salespersonsTable}.first_name, {$salespersonsTable}.last_name,
				{$rolesTable}.name as role_name
			")
                ->from($this->_table)
                ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                ->join($rolesTable, "{$rolesTable}.id = {$salespersonsTable}.roles_id", 'inner')
                ->where($where)
                ->get()->result_array();

            $players = array_rewrite($players, 'armstrong_2_salespersons_id');

            $where_call = array(
                "{$callRecordsTable}.is_draft" => 0,
                "{$routePlansTable}.is_draft" => 0,
                "{$callRecordsTable}.country_id" => $country['id'],
                "DATE({$callRecordsTable}.datetime_call_start) = DATE({$routePlansTable}.shifted_date)" => null,
                // "{$callRecordsTable}.datetime_call_start >= " => $cycleNext->toDateTimeString(),
                // "{$callRecordsTable}.datetime_call_start <= " => $endDate->toDateTimeString(),
            );
            $where_call = array_merge($where_call, $this->_getFilterConditions($this->_getFilter(), $callRecordsTable . '.datetime_call_start'));
            $countryCalls = $this->db->select("{$callRecordsTable}.armstrong_2_salespersons_id, count(*) as total")
                ->from($callRecordsTable)
                ->join($routePlansTable, "{$callRecordsTable}.armstrong_2_route_plan_id = {$routePlansTable}.armstrong_2_route_plan_id", 'inner')
                ->where($where_call)->group_by("{$callRecordsTable}.armstrong_2_salespersons_id")
                ->order_by('total', 'desc')
                ->get()->result_array();

            foreach ($countryCalls as $countryCall) {
                if (isset($players[$countryCall['armstrong_2_salespersons_id']])) {
                    $players[$countryCall['armstrong_2_salespersons_id']]['calls'] = $countryCall['total'];
                }
            }

            $where_coin = array(
                "{$this->_table}.is_draft" => 0,
                "{$this->_table}.is_valid" => 1,
                "{$this->_table}.country_id" => $country['id'],
                "{$salespersonsTable}.is_draft" => 0,
                "{$salespersonsTable}.active" => 1,
                // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                "{$this->_table}.armstrong_2_call_records_id != " => '',
            );
            $where_coin = array_merge($where_coin, $this->_getFilterConditions($this->_getFilter()));
            $countryCoins = $this->db->select("*, count(*) as total")
                ->from($this->_table)
                ->join($salespersonsTable, "{$this->_table}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                ->where($where_coin)->group_by("{$this->_table}.armstrong_2_salespersons_id")
                ->order_by('total', 'desc')
                ->get()->result_array();


            foreach ($countryCoins as $countryCoin) {
                if (isset($players[$countryCoin['armstrong_2_salespersons_id']])) {
                    $players[$countryCoin['armstrong_2_salespersons_id']]['coins'] = $countryCoin['total'];
                    $players[$countryCoin['armstrong_2_salespersons_id']]['percentage'] = !empty($players[$countryCoin['armstrong_2_salespersons_id']]['calls'])
                        ? ($countryCoin['total'] / $players[$countryCoin['armstrong_2_salespersons_id']]['calls']) * 100
                        : 0;
                }
            }
        }

        foreach ($results as $countryId => &$result) {
            foreach ($result as $key => &$_result) {
                $_result['priority'] = $this->_setOrderPriority($_result);
                $_result['name'] = $_result['first_name'] . ' ' . $_result['last_name'];
                if (!isset($_result['percentage'])) {
                    // unset($result[$key]);
                    $_result['percentage'] = 0;
                }
            }

            usort($result, function ($a, $b) {
                $rdiff = $b['priority'] - $a['priority'];
                if ($rdiff) return $rdiff;
                return (float)$b['percentage'] - (float)$a['percentage'];
            });
        }

        $this->data['results'] = $results;
        $this->data['season'] = $this->_getPrevSeason();

        return $this->render('spinwin/perfect_calls');
    }

    protected function _perfect_calls_season()
    {
        $callRecordsTable = $this->call_records_m->getTableName();
        $routePlansTable = $this->route_plan_m->getTableName();
        $salespersonsTable = $this->salespersons_m->getTableName();
        $rolesTable = $this->roles_m->getTableName();
        $prizeSecondTable = 'prizes_second';

        // $conditions = $this->_getFilterConditions($this->_getFilter());

        $calls = array();
        $coins = array();
        $results = array();
        $total = array();
        $filter = $this->_getFilter();
        list($startDate, $endDate) = $this->_parseSeason($this->_getSeasonDate($filter['season']));
        $cycle = $this->_getCycle($startDate, $endDate);
        $cycleNext = $cycle->copy()->addDay()->startOfDay();

        $this->data['cycle'] = array(
            'first' => $startDate,
            'middle' => $cycle,
            'middle_next' => $cycleNext,
            'end' => $endDate,
        );


        foreach ($this->countries as $country) {
            $where_player = array(
                "{$prizeSecondTable}.is_draft" => 0,
                "{$salespersonsTable}.is_draft" => 0,
                "{$salespersonsTable}.active" => 1,
                "{$prizeSecondTable}.country_id" => $country['id'],
            );
            $where_player = array_merge($where_player, $this->_getFilterConditions($this->_getFilter(), "{$prizeSecondTable}.date_sync"));

            $players = $this->db->select("
				distinct({$prizeSecondTable}.armstrong_2_salespersons_id),
				{$salespersonsTable}.first_name, {$salespersonsTable}.last_name,{$salespersonsTable}.email,
				{$rolesTable}.name as role_name, {$salespersonsTable}.type, {$salespersonsTable}.sub_type
			")
                ->from($prizeSecondTable)
                ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                ->join($rolesTable, "{$rolesTable}.id = {$salespersonsTable}.roles_id", 'inner')
                ->where($where_player)
                ->get()->result_array();
            $players = $this->filter_dummy_data($players);
            $players = array_rewrite($players, 'armstrong_2_salespersons_id');

            $countryCalls2 = $this->db->select("{$callRecordsTable}.armstrong_2_salespersons_id, count(*) as total")
                ->from($callRecordsTable)
                ->join($routePlansTable, "{$callRecordsTable}.armstrong_2_route_plan_id = {$routePlansTable}.armstrong_2_route_plan_id", 'inner')
                ->where(array(
                    "{$callRecordsTable}.is_draft" => 0,
                    "{$routePlansTable}.is_draft" => 0,
                    "{$callRecordsTable}.country_id" => $country['id'],
                    "DATE({$callRecordsTable}.datetime_call_start) = DATE({$routePlansTable}.shifted_date)" => null,
                    "{$callRecordsTable}.datetime_call_start >= " => $cycleNext->toDateTimeString(),
                    "{$callRecordsTable}.datetime_call_start <= " => $endDate->toDateTimeString(),
                ))->group_by("{$callRecordsTable}.armstrong_2_salespersons_id")
                ->order_by('total', 'desc')
                ->get()->result_array();

            $countryCalls1 = $this->db->select("{$callRecordsTable}.armstrong_2_salespersons_id, count(*) as total")
                ->from($callRecordsTable)
                ->join($routePlansTable, "{$callRecordsTable}.armstrong_2_route_plan_id = {$routePlansTable}.armstrong_2_route_plan_id", 'inner')
                ->where(array(
                    "{$callRecordsTable}.is_draft" => 0,
                    "{$routePlansTable}.is_draft" => 0,
                    "{$callRecordsTable}.country_id" => $country['id'],
                    "DATE({$callRecordsTable}.datetime_call_start) = DATE({$routePlansTable}.shifted_date)" => null,
                    "{$callRecordsTable}.datetime_call_start >= " => $startDate->toDateTimeString(),
                    "{$callRecordsTable}.datetime_call_start <= " => $cycle->endOfDay()->toDateTimeString(),
                ))->group_by("{$callRecordsTable}.armstrong_2_salespersons_id")
                ->order_by('total', 'desc')
                ->get()->result_array();

            $total[$country['id']]['calls']['first_cycle'] = $total[$country['id']]['calls']['second_cycle'] = 0;
            foreach ($countryCalls1 as $countryCall1) {
                if (isset($players[$countryCall1['armstrong_2_salespersons_id']])) {
                    $players[$countryCall1['armstrong_2_salespersons_id']]['calls']['first_cycle'] = $countryCall1['total'];
                    $total[$country['id']]['calls']['first_cycle'] = (isset($total[$country['id']]['calls']['first_cycle']) ? $total[$country['id']]['calls']['first_cycle'] : 0) + $countryCall1['total'];
                }
            }
            foreach ($countryCalls2 as $countryCall2) {
                if (isset($players[$countryCall2['armstrong_2_salespersons_id']])) {
                    $players[$countryCall2['armstrong_2_salespersons_id']]['calls']['second_cycle'] = $countryCall2['total'];
                    $total[$country['id']]['calls']['second_cycle'] = (isset($total[$country['id']]['calls']['second_cycle']) ? $total[$country['id']]['calls']['second_cycle'] : 0) + $countryCall2['total'];
                }
            }
            $total[$country['id']]['coins']['first_cycle'] = $total[$country['id']]['coins']['second_cycle'] = 0;
            foreach ($players as $id => &$player) {

                if ($player['type'] == 'PUSH') {
                    $countryCoins2 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.app_type" => 1,
                            "call_records_map_only_route_plan.`datetime_call_start` >= " => $cycleNext->toDateTimeString(),
                            "call_records_map_only_route_plan.`datetime_call_start` <= " => $endDate->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();

                    $countryCoins1 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            //"{$this->_table}.is_valid" => 1,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.app_type" => 1,
                            "call_records_map_only_route_plan.`datetime_call_start` >= " => $startDate->toDateTimeString(),
                            "call_records_map_only_route_plan.`datetime_call_start` <= " => $cycle->endOfDay()->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();

                } elseif ($player['type'] == 'PULL') {
                    $countryCoins2 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.app_type" => 0,
                            "call_records_map_only_route_plan.datetime_call_start >= " => $cycleNext->toDateTimeString(),
                            "call_records_map_only_route_plan.datetime_call_start <= " => $endDate->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();

                    $countryCoins1 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            //"{$this->_table}.is_valid" => 1,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.app_type" => 0,
                            "call_records_map_only_route_plan.datetime_call_start >= " => $startDate->toDateTimeString(),
                            "call_records_map_only_route_plan.datetime_call_start <= " => $cycle->endOfDay()->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();
                    if ($id == 'SALA0015TH') {
                        //dd($this->db->last_query());
                    }

                } elseif ($player['type'] == 'MIX') {
                    $countryCoins2 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.`datetime_call_start` >= " => $cycleNext->toDateTimeString(),
                            "call_records_map_only_route_plan.`datetime_call_start` <= " => $endDate->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();

                    $countryCoins1 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->join('call_records_map_only_route_plan', "{$prizeSecondTable}.armstrong_2_call_records_id = call_records_map_only_route_plan.armstrong_2_call_records_id", "inner")
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            //"{$this->_table}.is_valid" => 1,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            "{$prizeSecondTable}.armstrong_2_call_records_id != " => '',
                            "call_records_map_only_route_plan.`datetime_call_start` >= " => $startDate->toDateTimeString(),
                            "call_records_map_only_route_plan.`datetime_call_start` <= " => $cycle->endOfDay()->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();
                } elseif ($player['type'] == 'SL') {
                    $countryCoins2 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            //"{$this->_table}.armstrong_2_call_records_id != " => '',
                            "{$prizeSecondTable}.date_created >= " => $cycleNext->toDateTimeString(),
                            "{$prizeSecondTable}.date_created <= " => $endDate->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();

                    $countryCoins1 = $this->db->select("*, count(*) as total")
                        ->from($prizeSecondTable)
                        ->join($salespersonsTable, "{$prizeSecondTable}.armstrong_2_salespersons_id = {$salespersonsTable}.armstrong_2_salespersons_id", 'inner')
                        ->where(array(
                            "{$prizeSecondTable}.is_draft" => 0,
                            //"{$prizeSecondTable}.is_valid" => 1,
                            "{$prizeSecondTable}.country_id" => $country['id'],
                            "{$salespersonsTable}.is_draft" => 0,
                            "{$salespersonsTable}.active" => 1,
                            // "{$this->_table}.armstrong_2_call_records_id IS NOT NULL" => null,
                            //"{$this->_table}.armstrong_2_call_records_id != " => '',
                            "{$prizeSecondTable}.date_created >= " => $startDate->toDateTimeString(),
                            "{$prizeSecondTable}.date_created <= " => $cycle->endOfDay()->toDateTimeString(),
                            "{$prizeSecondTable}.armstrong_2_salespersons_id" => $id
                        ))->group_by("{$prizeSecondTable}.armstrong_2_salespersons_id")
                        ->order_by('total', 'desc')
                        ->get()->row();
                }
                if ($countryCoins1) {
                    $player['coins']['first_cycle'] = $countryCoins1->total;
                    $player['percentage']['first_cycle'] = !empty($player['calls']['first_cycle'])
                        ? ($countryCoins1->total / $player['calls']['first_cycle']) * 100
                        : 0;
                    $total[$country['id']]['coins']['first_cycle'] = (isset($total[$country['id']]['coins']['first_cycle']) ? $total[$country['id']]['coins']['first_cycle'] : 0) + $countryCoins1->total;
                }

                if ($countryCoins2) {
                    $player['coins']['second_cycle'] = $countryCoins2->total;
                    $player['percentage']['second_cycle'] = !empty($player['calls']['second_cycle'])
                        ? ($countryCoins2->total / $player['calls']['second_cycle']) * 100
                        : 0;
                    $total[$country['id']]['coins']['second_cycle'] = (isset($total[$country['id']]['coins']['second_cycle']) ? $total[$country['id']]['coins']['second_cycle'] : 0) + $countryCoins2->total;
                }
            }

            // Total percontage
            if ($total[$country['id']]['coins']['first_cycle'] > 0 && $total[$country['id']]['calls']['first_cycle'] > 0) {
                $total[$country['id']]['percentage']['first_cycle'] = $total[$country['id']]['coins']['first_cycle'] / $total[$country['id']]['calls']['first_cycle'] * 100;
            }
            if ($total[$country['id']]['coins']['second_cycle'] > 0 && $total[$country['id']]['calls']['second_cycle'] > 0) {
                $total[$country['id']]['percentage']['second_cycle'] = $total[$country['id']]['coins']['second_cycle'] / $total[$country['id']]['calls']['second_cycle'] * 100;
            }
            $results[$country['id']] = $players;

        }
        //Total percentage diff
        foreach ($total as $country_id => &$result) {
            if (!isset($result['percentage']['first_cycle']) || !isset($result['percentage']['second_cycle'])) {
                $result['percentage']['diff'] = 0;
            } else {
                $result['percentage']['diff'] = $result['percentage']['second_cycle'] - $result['percentage']['first_cycle'];
            }
        }
        foreach ($results as $countryId => &$result) {
            foreach ($result as $key => &$_result) {
                $_result['priority'] = $this->_setOrderPriority($_result);
                $_result['name'] = $_result['first_name'] . ' ' . $_result['last_name'];

                if (!isset($_result['percentage']['first_cycle']) || !isset($_result['percentage']['second_cycle'])) {
                    // unset($result[$key]);
                    $_result['percentage']['diff'] = 0;
                } else {
                    $_result['percentage']['diff'] = $_result['percentage']['second_cycle'] - $_result['percentage']['first_cycle'];
                }
            }

            usort($result, function ($a, $b) {
                // if (isset($a['percentage']['diff'], $b['percentage']['diff']))
                // {
                $rdiff = $b['priority'] - $a['priority'];
                if ($rdiff) return $rdiff;
                return (float)$b['percentage']['diff'] - (float)$a['percentage']['diff'];
                // }
            });
        }

        $this->data['results'] = $results;
        $this->data['total'] = $total;
        $this->data['season'] = $this->_getPrevSeason();
        return $this->render('spinwin/perfect_calls_season');
    }

    protected function _setOrderPriority(array $data)
    {
        if (!isset($data['role_name'])) {
            return 0;
        }

        switch ($data['role_name']) {
            case 'Salesperson':
                $priority = 1;
                break;

            case 'Sales Manager':
                $priority = 2;
                break;

            default:
                $priority = 0;
                break;
        }

        return $priority;
    }

    public function filter($filter, $season = null)
    {

        $this->_setFilter($filter, $season);
        redirect($this->agent->referrer());
    }
}