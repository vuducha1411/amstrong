<?php if ( ! defined( 'BASEPATH' ) ) {
	exit( 'No direct script access allowed' );
}
/**
 * Date: 8/8/14
 * Time: 1:35 PM
 */

/*
|--------------------------------------------------------------------------
| Development Helper
|--------------------------------------------------------------------------
|
|These function help developer work with CI much more easier
|Author: Viet-Thang NGUYEN
|
*/
/**
 * Dump helper. Functions to dump variables to the screen, in a nicely formatted manner.
 */
// if ( ! function_exists( 'dump' ) ) {
// 	function dump( $var, $label = 'Dump', $echo = true ) {
// 		// Store dump in variable
// 		ob_start();
// 		var_dump( $var );
// 		$output = ob_get_clean();
// 		// Add formatting
// 		$output = preg_replace( "/\]\=\>\n(\s+)/m", "] => ", $output );
// 		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 30px 0px 250px; text-align: left;">' . $label . ' => ' . $output . '</pre>';
// 		// Output
// 		if ( $echo == true ) {
// 			echo $output;
// 		} else {
// 			return $output;
// 		}
// 	}
// }

/**
 * Dump helper, exit application after finish. Functions to dump variables to the screen, in a nicely formatted manner.
 */
// if ( ! function_exists( 'dd' ) ) {
// 	function dd( $var, $label = 'Dump', $echo = true ) {
// 		dump( $var, $label, $echo );
// 		exit;
// 	}
// }

/**
 * @param $var
 * @param string $label
 * @param bool $echo
 *
 * @return mixed|string
 */
function pr($var, $label = 'Print', $echo = true)
{
    ob_start();
    print_r( $var );
    $output = ob_get_clean();
    // Add formatting
    $output = preg_replace( "/\]\=\>\n(\s+)/m", "] => ", $output );
    $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 30px 0px 250px; text-align: left;">' . $label . ' => ' . $output . '</pre>';
    // Output
    if ( $echo == true ) {
        echo $output;
    } else {
        return $output;
    }
}

/**
 * Dump helper, exit application after finish. Functions to dump variables to the screen, in a nicely formatted manner.
 */
if ( ! function_exists( 'prd' ) ) {
    function prd( $var, $label = 'Print', $echo = true ) {
        pr( $var, $label, $echo );
        exit;
    }
}

if ( ! function_exists( 'random_token' ) ) {
	function random_token() {
		$rand128 = bin2hex( openssl_random_pseudo_bytes( 16 ) );

		return $rand128;
	}
}


if ( ! function_exists( 'wordTrim' ) ) {
	function wordTrim( $string, $maxLength, $offset = 0, $elipses = '...' ) {
		if ( $offset ) {
			$string = preg_replace( '/^\S*\s+/s', '', utf8_substr( $string, $offset ) );
		}

		$strLength = utf8_strlen( $string );

		if ( $maxLength > 0 && $strLength > $maxLength ) {
			$string = utf8_substr( $string, 0, $maxLength );
			$string = strrev( preg_replace( '/^\S*\s+/s', '', strrev( $string ) ) ) . $elipses;
		}

		if ( $offset ) {
			$string = $elipses . $string;
		}

		return $string;
	}
}

/**
 * Shorthand for date funtion with default format Y-m-d H:i:s
 *
 * @param mixed|$format @see http://php.net/manual/en/function.date.php
 *
 * @return string
 */
if ( ! function_exists( 'get_date' ) ) {
	function get_date( $format = 'Y-m-d H:i:s' ) {
		return date( $format );
	}
}

/**
 * Return only the specified key / value pairs from the array
 *
 * @param array|$data
 * @param array|$keys list of keys to keep from $data
 *
 * @return array
 */
// if ( ! function_exists( 'array_only' ) ) {
// 	function array_only( array $data, array $keys ) {
// 		return array_intersect_key( $data, array_flip( $keys ) );
// 	}
// }

/**
 * Removes the given key / value pairs from the array
 *
 * @param array|$data
 * @param array|$keys list of keys to removes from $data
 *
 * @return array
 */
// if ( ! function_exists( 'array_except' ) ) {
// 	function array_except( array $data, array $keys ) {
// 		return array_diff_key( $data, array_flip( $keys ) );
// 	}
// }

/**
 * Replace root array key with child array key
 * 
 * Note that the specified key must exist in the query result, or it will be ignored.
 *
 * @param array|$data
 * @param string|$key
 *
 * @return array
 */
if ( ! function_exists( 'array_rewrite' ) ) {
    function array_rewrite( array $data, $key ) {
        $output = array();

        foreach ($data as $_key => $value) {
            $output[(isset($value[$key])) ? $value[$key] : $_key] = $value;
        }

        return $output;
    }
}

if ( ! function_exists( 'lq' ) ) {
    function lq($die = true) {
        $CI =& get_instance();
        $die ? prd($CI->db->last_query()) : print_r($CI->db->last_query());
    }
}

if ( ! function_exists( 'elq' ) ) {
    function elq($last = true, $die = true) {
        $queryLog = Capsule::getQueryLog();
        $query = $last ? end($queryLog) : $queryLog;
        $die ? dd($query) : print_r($query);
    }
}

/**
 * Set page title with format: {title} | {site name}
 *
 * @param string|$string
 *
 * @return string
 */
if ( ! function_exists( 'page_title' ) ) {
	function page_title( $string ) {
		return $string .= ' | ' . config_item( 'site_name' );
	}
}

/**
 * Create html button
 *
 * @param string|$uri use for href attribute
 * @param string|$content button content
 * @param array|$attributes
 *
 * @return string
 */
if ( ! function_exists( 'html_btn' ) ) {
	function html_btn( $uri, $content, $attributes = array() ) {
		$class = ( empty( $attributes['class'] ) ? '' : ' ' . htmlspecialchars( $attributes['class'] ) );

		unset( $attributes['class'] );

		$attribs = getAttributes( $attributes );

		return "<a href=\"{$uri}\" class=\"btn{$class}\"  {$attribs}>{$content}</a>";
	}
}

/**
 * Create year options that used for dropdown select
 *
 * @param int , string|$from start year
 * @param int , string|$to end year, current year will be used if this was not set
 *
 * @return array
 */
if ( ! function_exists( 'select_year' ) ) {
	function select_year( $from, $to = null ) {
		if ( ! $to ) {
			$to = date( "Y" );
		}

		$output = array();

		$from = intval( $from );
		$to   = intval( $to );

		for ( $i = $from; $i <= $to; $i ++ ) {
			$output[ $i ] = $i;
		}

		return $output;
	}
}

/**
 * Create month options that used for dropdown select
 *
 * @return array
 */
if ( ! function_exists( 'select_month' ) ) {
	function select_month($month = null) {
		$months = array(
			1  => 'January',
			2  => 'February',
			3  => 'March',
			4  => 'April',
			5  => 'May',
			6  => 'June',
			7  => 'July',
			8  => 'August',
			9  => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December',
		);

        if ($month)
        {
            return isset($months[$month]) ? $months[$month] : false;
        }

        return $months;
	}
}

/**
 * Builds a string of attributes for insertion into an HTML tag.
 *
 * @param array $attributes
 *
 * @return string ' attr1="abc" attr2="def" attr3="ghi"'
 */
if ( ! function_exists( 'getAttributes' ) ) {
	function getAttributes( array $attributes = array() ) {
		$attributesHtml = '';

		foreach ( $attributes AS $attribute => $value ) {
			$attributesHtml .= ' ' . htmlspecialchars( $attribute ) . "=\"{$value}\"";
		}

		return $attributesHtml;
	}
}

/**
 * Encrypting string for security purpose
 *
 * @param $string   String that needed to ben encrypted
 * @param $hash     Hash string that used as key for encryption
 *
 * @return string   Encrypted String
 */
if ( ! function_exists( 'encrypt_password' ) ) {
	function encrypt_password( $string, $hash ) {
		$encrypted_string = crypt( $string, md5( $hash ) );

		return $encrypted_string;
	}
}

/**
 * Return only the specified key / value pairs from the array
 *
 * @param array|$data
 * @param array|$keys list of keys to keep from $data
 *
 * @return array
 */
if ( ! function_exists( 'array_only' ) ) {
    function array_only( array $data, array $keys ) {
        return array_intersect_key( $data, array_flip( $keys ) );
    }
}

/**
 * Removes the given key / value pairs from the array
 *
 * @param array|$data
 * @param array|$keys list of keys to removes from $data
 *
 * @return array
 */
if ( ! function_exists( 'array_except' ) ) {
    function array_except( array $data, array $keys ) {
        return array_diff_key( $data, array_flip( $keys ) );
    }
}

/**
 * Convert array to Object
 *
 * @param array|$data
 *
 * @return stdClass Object
 */
if (!function_exists('arrayToObject')) {
    function arrayToObject($array)
    {
        if (!is_array($array) && is_object($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = arrayToObject($value);
                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('objectToArray')) {
    function objectToArray($obj) {
        if(!is_object($obj) && !is_array($obj))
            return $obj;

        return array_map('objectToArray', (array) $obj);
    }
}

if (!function_exists('echo_isset')) {
    function echo_isset($value, $default = null) {
        echo isset($value) ? $value : $default;
    }
}

/**
 * Parse datetime with Carbon
 * 
 * @param mixed|$time Carbon supported time
 * @param string|$timezone Output timezone, try to catch from users table if not set
 * 
 * @return Carbon
 */ 
if (!function_exists('parse_datetime')) 
{
    function parse_datetime($time, $timezone = null)
    {
        $defaultTz = 'Asia/Singapore';
        
        if (!in_array($timezone, timezone_identifiers_list()))
        {
            $timezone = false;
        }
        if ($timezone)
        {
            return Carbon::parse($time, $defaultTz)->setTimezone($timezone);
        }
        return Carbon::parse($time, $defaultTz); 
    }
}

if (!function_exists('get_product_label')) 
{
    function get_product_label($product = array(), $title = 'sku_name')
    {
        if(!$product) return '';
        $label = $product['sku_number'] . ' - ';
        $label .= $product[$title];
        $label .= " ({$product['quantity_case']}x{$product['weight_pc']}) - ";
        $label .= number_format($product['price'], 2);

        return $label;
    }
}

/**
 * @param array $countries
 * @param null $country_id
 * @return bool
 */
if (!function_exists('get_country_name')) {
    function get_country_name($countries = null, $country_id = null) {
        if (!$country_id || !is_array($countries)) {
            return false;
        }

        $countries = array_rewrite($countries, 'id');

        return isset($countries[$country_id]['name']) ? $countries[$country_id]['name'] : false;
    }
}

/**
 *
 * generate armstrong_2_ssd_id
 *
 **/
if (!function_exists('generateSsdId')) {
    function generateSsdId($id, $country_code)
    {
        if (strlen($id) < 8) {
            $compare_str = 8 - strlen($id);
            for ($j = 1; $j <= $compare_str; $j++) {
                $id = '0' . $id;
            }
        }
        $id = 'SSDA' . $id . strtoupper($country_code);
        return $id;
    }
}