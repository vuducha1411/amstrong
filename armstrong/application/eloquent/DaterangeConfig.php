<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class DaterangeConfig extends Base
{
    protected $table = 'daterange_config';

    //protected $guarded = array('id');

    public function country()
    {
        return $this->belongsTo('Country');
    }
}