<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Formula extends Base
{
	protected $table = 'formula';

	protected $guarded = ['id'];

    public function scopeFormulaByCountry($query, $countryOTM){
        $where = array(
            'id' => $countryOTM,
            'type' => 0
        );
        return $query->select('formula')->where($where);
    }
}