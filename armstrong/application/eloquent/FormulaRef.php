<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

use Carbon;

class FormulaRef extends Base
{
	protected $table = 'formula_ref';

	protected $guarded = ['id'];

	protected $appends = ['date_range'];

	public function getDateStartAttribute($value)
	{
		return Carbon::parse($value);
	}

	public function getDateEndAttribute($value)
	{
		return Carbon::parse($value);
	}

	public function getDateRangeAttribute($value)
	{
		return $this->getAttribute('date_start')->toDateString() . ' - ' . $this->getAttribute('date_end')->toDateString();
	}

    public function scopeRefTable($query, $id){
        return $query->select('name','ref_table')->where('id', '=', $id);
    }
}