<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Country extends Base
{
	protected $table = 'country';

	protected $guarded = array('id');

    public function country_config()
    {
        return $this->hasOne('DaterangeConfig');
    }
}