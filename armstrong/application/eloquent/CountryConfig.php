<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class CountryConfig extends Base
{
    protected $table = 'country_config';

    //protected $guarded = array('id');

    public function country()
    {
        return $this->belongsTo('Country');
    }
}