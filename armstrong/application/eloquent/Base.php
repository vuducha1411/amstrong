<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Base extends Eloquent
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'last_updated';

	public function scopeActive($query)
	{
		return $query->where('active', '=', 1);
	}

	public function scopeIsDraft($query, $isDraft = 0)
	{
		return $query->where('is_draft', '=', $isDraft);
	}

	public function scopeCountry($query, $country)
	{
		return $query->where('country_id', '=', $country);
	}

	public function scopeFilter($query, $country, $isDraft = 0)
	{
		return $query->where('country_id', '=', $country)
			->where('is_draft', '=', $isDraft);
	}

	public function softDelete()
	{
		return $this->update([
			'is_draft' => 1
		]);
	}
}