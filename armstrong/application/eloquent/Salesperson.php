<?php namespace Eloquent;

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Salesperson extends Base
{
	protected $table = 'salespersons';

	protected $guarded = array('id');

	protected $appends  = ['full_name'];

	public function getFullNameAttribute($value)
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function country()
	{
		return $this->belongsTo('Eloquent\Country');
	}
}