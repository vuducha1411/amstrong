<?php

/* ami/call_records/preview.html.twig */
class __TwigTemplate_0c57dbe21141df59b2ad5a20fae6983326e155c2d4ae0dee8aaf89ae823206e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_call_records_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <div class=\"list-group\">
                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-user pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Salespersons</h4>
                            <p class=\"list-group-item-text\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_salespersons_id", array()), array(), "array"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-user pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Customers</h4>
                            <p class=\"list-group-item-text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_customers_id", array()), array(), "array"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-phone pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Call time</h4>
                            <p class=\"list-group-item-text\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_start", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_end", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 38
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "call_records"))) {
            // line 39
            echo "                ";
            echo html_btn(site_url(("ami/call_records/edit/" . $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_call_records_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 41
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/call_records/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 41,  74 => 39,  72 => 38,  59 => 30,  48 => 22,  37 => 14,  25 => 5,  19 => 1,);
    }
}
