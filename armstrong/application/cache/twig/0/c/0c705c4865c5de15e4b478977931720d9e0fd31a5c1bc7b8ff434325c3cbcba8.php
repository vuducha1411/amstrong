<?php

/* ami/perfect_store/template.html.twig */
class __TwigTemplate_0c705c4865c5de15e4b478977931720d9e0fd31a5c1bc7b8ff434325c3cbcba8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/perfect_store/template.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_css($context, array $blocks = array())
    {
        // line 8
        $this->displayParentBlock("css", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "
    ";
        // line 13
        echo form_open(site_url("ami/perfect_store/save_template"), array("class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Perfect Store Template</h1>
                <div class=\"text-right\">
                    ";
        // line 20
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/perfect_store/template.html.twig", 20)->display(array_merge($context, array("url" => "ami/perfect_store", "id" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()), "permission" => "perfect_store")));
        // line 21
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">              
                        
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["storeContent"]) ? $context["storeContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 32
            echo "                    <li role=\"presentation\" class=\"";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\"><a href=\"#tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\" role=\"tab\" data-toggle=\"tab\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</a></li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            </ul>

            <div class=\"tab-content m-t\">
                ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["storeContent"]) ? $context["storeContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 38
            echo "                    <div role=\"tabpanel\" class=\"tab-pane ";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\" id=\"tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">

                        ";
            // line 40
            if ((($context["id"] == 0) && (isset($context["products"]) ? $context["products"] : null))) {
                echo "                
                            ";
                // line 41
                if ($this->getAttribute($context["content"], "content", array())) {
                    // line 42
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                        // line 43
                        echo "                                    <div class=\"form-group ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\">
                                        ";
                        // line 44
                        echo form_label("", "", array("class" => "control-label col-sm-3"));
                        echo "
                                        <div class=\"col-sm-6\">
                                            ";
                        // line 46
                        echo form_dropdown((("product[" . $context["key"]) . "][title]"), (isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["data"], "title", array()), "class=\"form-control\"");
                        echo "
                                        </div>
                                        <div class=\"col-sm-3\">
                                            <button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".";
                        // line 49
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-plus\"></i></button>
                                            <button class=\"btn btn-default remove-clone-btn\" data-clone=\".";
                        // line 50
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-minus\"></i></button>
                                        </div>
                                        <input type=\"hidden\" class=\"clone-remove\" name=\"product[";
                        // line 52
                        echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                        echo "][id]\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\" />
                                        <input type=\"hidden\" class=\"clone-move clone-remove\" data-target=\"#deleteTemp\" data-name=\"product_delete[]\" value=\"";
                        // line 53
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\" />
                                    </div>                    
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 56
                    echo "                            ";
                } else {
                    // line 57
                    echo "                                <div class=\"form-group ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\">
                                    ";
                    // line 58
                    echo form_label("", "", array("class" => "control-label col-sm-3"));
                    echo "
                                    <div class=\"col-sm-6\">
                                        ";
                    // line 60
                    echo form_dropdown("product[0][title]", (isset($context["products"]) ? $context["products"] : null), null, "class=\"form-control\"");
                    echo "
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".";
                    // line 63
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i class=\"fa fa-plus\"></i></button>
                                        <button class=\"btn btn-default btn-clone remove-clone-btn\" data-clone=\".";
                    // line 64
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\" style=\"display: none;\"><i class=\"fa fa-minus\"></i></button>
                                    </div>
                                </div>
                            ";
                }
                // line 68
                echo "                        ";
            } else {
                // line 69
                echo "                            ";
                if ($this->getAttribute($context["content"], "content", array())) {
                    // line 70
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                        // line 71
                        echo "                                    <div class=\"form-group ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\">
                                        ";
                        // line 72
                        echo form_label("", "", array("class" => "control-label col-sm-3"));
                        echo "
                                        <div class=\"col-sm-6\">
                                            ";
                        // line 74
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title]"), "value" => $this->getAttribute($context["data"], "title", array()), "class" => "form-control"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-3\">
                                            <button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".";
                        // line 77
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-plus\"></i></button>
                                            <button class=\"btn btn-default remove-clone-btn\" data-clone=\".";
                        // line 78
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-minus\"></i></button>
                                        </div>
                                        <input type=\"hidden\" class=\"clone-remove\" name=\"";
                        // line 80
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "[";
                        echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                        echo "][id]\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\" />
                                        <input type=\"hidden\" class=\"clone-move clone-remove\" data-target=\"#deleteTemp\" data-name=\"";
                        // line 81
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "_delete[]\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\" />
                                    </div>                    
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 84
                    echo "                            ";
                } else {
                    // line 85
                    echo "                                <div class=\"form-group ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\">
                                    ";
                    // line 86
                    echo form_label("", "", array("class" => "control-label col-sm-3"));
                    echo "
                                    <div class=\"col-sm-6\">
                                        ";
                    // line 88
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][title]"), "value" => null, "class" => "form-control"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".";
                    // line 91
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i class=\"fa fa-plus\"></i></button>
                                        <button class=\"btn btn-default btn-clone remove-clone-btn\" data-clone=\".";
                    // line 92
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\" style=\"display: none;\"><i class=\"fa fa-minus\"></i></button>
                                    </div>
                                </div>
                            ";
                }
                // line 96
                echo "                        ";
            }
            // line 97
            echo "                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "            </div>

        </div>
    </div> 

    <div id=\"deleteTemp\"></div>

    ";
        // line 106
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/perfect_store/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 106,  330 => 99,  315 => 97,  312 => 96,  305 => 92,  301 => 91,  295 => 88,  290 => 86,  285 => 85,  282 => 84,  271 => 81,  263 => 80,  258 => 78,  254 => 77,  248 => 74,  243 => 72,  238 => 71,  233 => 70,  230 => 69,  227 => 68,  220 => 64,  216 => 63,  210 => 60,  205 => 58,  200 => 57,  197 => 56,  188 => 53,  182 => 52,  177 => 50,  173 => 49,  167 => 46,  162 => 44,  157 => 43,  152 => 42,  150 => 41,  146 => 40,  138 => 38,  121 => 37,  116 => 34,  95 => 32,  78 => 31,  66 => 21,  64 => 20,  54 => 13,  51 => 12,  48 => 11,  42 => 8,  39 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
