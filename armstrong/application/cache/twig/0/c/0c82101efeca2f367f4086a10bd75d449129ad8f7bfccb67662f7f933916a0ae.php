<?php

/* ami/components/uploader_cert.html.twig */
class __TwigTemplate_0c82101efeca2f367f4086a10bd75d449129ad8f7bfccb67662f7f933916a0ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\" id=\"certUpload\" data-button=\"uploader_cert_pickfiles\" data-container=\"uploader_cert\"
    data-url=\"";
        // line 2
        echo twig_escape_filter($this->env, site_url("ami/media/upload"), "html", null, true);
        echo "\" 
    data-maxfilesize=\"100mb\" 
    data-params=\"";
        // line 4
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array("entry_type" => (isset($context["entry_type"]) ? $context["entry_type"] : null), "class" => "cert")), "html", null, true);
        echo "\" 
    data-mimetypes=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array(0 => array("title" => "PDF Files", "extensions" => "pdf"))), "html", null, true);
        echo "\">
    ";
        // line 6
        echo form_label("Cert", "inputCerts", array("class" => "control-label col-sm-3"));
        echo "
    <div id=\"uploader_cert\" class=\"controls uploader col-sm-6\">
        <div class=\"upload-actions\">
            <a class=\"btn btn-default pick-gallery Tooltip\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url(("ami/media/view/cert/" . (isset($context["entry_type"]) ? $context["entry_type"] : null))), "html", null, true);
        echo "\" data-toggle=\"ajaxModal\" title=\"Select from Media\"><i class=\"fa fa-photo\"></i> Media</a>&nbsp;
            <a class=\"btn btn-default btn-upload\" id=\"uploader_cert_pickfiles\" href=\"javascript:;\"><i class=\"fa fa-upload\"></i> Upload file</a>
        </div>
            <div class=\"filelist\"></div>
        <div class=\"uploaded row\" style=\"margin-top: 10px\">
            ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["certs"]) ? $context["certs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["cert"]) {
            // line 15
            echo "                <div class=\"col-md-4\">
                    <div class=\"thumbnail\">
                        <img src=\"";
            // line 17
            echo twig_escape_filter($this->env, site_url("res/img/pdf.png"), "html", null, true);
            echo "\" width=\"100%\" class=\"img-responsive\">
                        <div class=\"caption\">
                            <span class=\"label label-primary\" title=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["cert"], "filename_old", array()), "html", null, true);
            echo "\">";
            echo wordTrim($this->getAttribute($context["cert"], "filename_old", array()), 15);
            echo "</span>
                            <ul class=\"list-inline\" style=\"margin: 10px 0 0 0;\">
                                <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, (((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/cert/") . $this->getAttribute($context["cert"], "path", array())), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn btn-xs btn-default\"><i class=\"fa fa-eye\"></i> View</a> 
                                <a href=\"#\" data-id=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["cert"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cert"], "class", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-default pull-right delete-media\"><i class=\"fa fa-close\"></i> Delete</a>
                            </ul>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/uploader_cert.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 28,  73 => 22,  69 => 21,  62 => 19,  57 => 17,  53 => 15,  49 => 14,  41 => 9,  35 => 6,  31 => 5,  27 => 4,  22 => 2,  19 => 1,);
    }
}
