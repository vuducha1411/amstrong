<?php

/* ami/reports/custom/filter.twig.html */
class __TwigTemplate_0478a56d43127e8c1b59bf0047985b0f71b3b9d4203df04995c32d8b9bf2b050 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["region"]) ? $context["region"] : null)) {
            // line 2
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryHierachy", array()));
            foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
                // line 3
                echo "\t\t<div class=\"form-group select-change-container\">
\t\t\t";
                // line 4
                echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
                echo "
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t";
                // line 6
                $context["_values"] = ((($context["key"] == "root")) ? ($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "rootValues", array())) : (array()));
                // line 7
                echo "\t\t\t\t";
                echo form_dropdown($context["countryField"], (isset($context["_values"]) ? $context["_values"] : null), null, "class=\"form-control select-change\"");
                echo "
\t\t\t</div>
\t\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 12
        echo "
";
        // line 13
        if ((isset($context["regionOnly"]) ? $context["regionOnly"] : null)) {
            // line 14
            echo "\t<div class=\"form-group\">
\t\t";
            // line 15
            echo form_label("Region", "region", array("class" => "control-label col-sm-3"));
            echo "
\t\t<div class=\"col-sm-6\">
\t\t\t";
            // line 17
            echo form_dropdown("region[]", $this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "regions", array()), "ALL", "class=\"form-control\" multiple=\"multiple\" style=\"min-height: 300px;\"");
            echo "
\t\t</div>
\t</div>
";
        }
        // line 21
        echo "
";
        // line 22
        if ((isset($context["channel"]) ? $context["channel"] : null)) {
            // line 23
            echo "\t<div class=\"form-group\">
\t\t";
            // line 24
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-3"));
            echo "
\t\t<div class=\"col-sm-6\">
\t\t\t<select name=\"channel[]\" class=\"form-control\" id=\"ChannelSelect\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t\t<option value=\"-1\" selected=\"selected\">ALL</option>
\t\t\t\t";
            // line 28
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryChannels", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 29
                echo "\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "\t\t\t</select>
\t\t</div>
\t</div>
";
        }
        // line 35
        echo "
";
        // line 36
        if ((isset($context["otm"]) ? $context["otm"] : null)) {
            // line 37
            echo "\t<div class=\"form-group\">
\t\t";
            // line 38
            echo form_label("OTM", "otm", array("class" => "control-label col-sm-3"));
            echo "
\t\t<div class=\"col-sm-6\">
\t\t\t";
            // line 40
            echo form_dropdown("otm[]", array("ALL" => "ALL", "A" => "A", "B" => "B", "C" => "C", "D" => "D", "O" => "Other"), "ALL", "class=\"form-control\" id=\"InputOTM\" multiple=\"multiple\" style=\"min-height: 120px;\"");
            echo "
\t\t</div>
\t</div>
";
        }
        // line 44
        echo "
";
        // line 45
        if ((isset($context["leaderAndPerson"]) ? $context["leaderAndPerson"] : null)) {
            // line 46
            echo "\t<div class=\"form-group\">
\t\t";
            // line 47
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-3"));
            echo "
\t\t<div class=\"col-sm-6\">
\t\t\t<select name=\"sales_leader[]\" class=\"form-control\" id=\"SalesLeaderSelect\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t\t<option value=\"ALL\" selected=\"selected\">ALL</option>
\t\t\t\t";
            // line 51
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 52
                echo "\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "\t\t\t</select>
\t\t</div>
\t</div>

\t<div class=\"form-group\">
\t\t";
            // line 59
            echo form_label("Armstrong 2 salespersons id", "salesperson", array("class" => "control-label col-sm-3"));
            echo "
\t\t<div class=\"col-sm-6\">
\t\t\t<select name=\"armstrong_2_salespersons_id[]\" class=\"form-control SalespersonsSelect\" id=\"armstrong_2_salespersons_id\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t\t<option value=\"\" selected=\"selected\" data-manager=\"\">ALL</option>
\t\t\t\t";
            // line 63
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 64
                echo "\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\" data-manager=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "\t\t\t</select>
\t\t</div>
\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/filter.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 66,  174 => 64,  170 => 63,  163 => 59,  156 => 54,  143 => 52,  139 => 51,  132 => 47,  129 => 46,  127 => 45,  124 => 44,  117 => 40,  112 => 38,  109 => 37,  107 => 36,  104 => 35,  98 => 31,  87 => 29,  83 => 28,  76 => 24,  73 => 23,  71 => 22,  68 => 21,  61 => 17,  56 => 15,  53 => 14,  51 => 13,  48 => 12,  36 => 7,  34 => 6,  29 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
