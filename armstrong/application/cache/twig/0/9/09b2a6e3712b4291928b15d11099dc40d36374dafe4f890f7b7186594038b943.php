<?php

/* spinwin/winner.html.twig */
class __TwigTemplate_09b2a6e3712b4291928b15d11099dc40d36374dafe4f890f7b7186594038b943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("spinwin/layout.html.twig", "spinwin/winner.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "spinwin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script type=\"text/javascript\">
\$(function() {
\t\$('#CountrySelector').on('change', function(e) {
\t\tvar \$this = \$(this),
\t\t\t\$selected = \$this.find(\":selected\"),
\t\t\tval = \$selected.val();

\t\t\$('.country-filter').each(function() {
\t\t\tvar \$this = \$(this);
\t\t\t\$this.hide();
\t\t\tif (val == 0) {
\t\t\t\t\$this.show();
\t\t\t} else {
\t\t\t\tif (\$this.hasClass('country-' + val)) {
\t\t\t\t\t\$this.show();
\t\t\t\t}
\t\t\t}
\t\t});

\t});
});
</script>
";
    }

    // line 29
    public function block_content($context, array $blocks = array())
    {
        // line 30
        echo "<div class=\"content winner\">
\t<div class=\"content-background-four center-block\">
\t\t";
        // line 32
        if ((isset($context["winner"]) ? $context["winner"] : null)) {
            // line 33
            echo "\t\t\t<div class=\"row ";
            echo "\" style=\"margin-bottom: 30px;\">
\t\t\t\t";
            // line 45
            echo "
\t\t\t\t<div class=\"col-sm-offset-3 col-sm-6\">
\t\t\t\t\t<div class=\"img_winner\">
\t\t\t\t\t\t <img class=\"img-responsive\" src=\"";
            // line 48
            echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "season", array()) == 1)) ? (site_url("res/img/spinwin/ss1_winer.png")) : (((($this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "season", array()) == 2)) ? (site_url("res/img/spinwin/ss2_winer.png")) : ("")))), "html", null, true);
            echo "\" alt=\"\"/>
\t\t\t\t\t\t<div class=\"member-info font-bold\">
\t\t\t\t\t\t\t";
            // line 51
            echo "\t\t\t\t\t\t\t";
            // line 52
            echo "\t\t\t\t\t\t\t<span class=\"text_row_man2\">Calls Made: ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["winner"]) ? $context["winner"] : null), "calls", array())), "html", null, true);
            echo " - Perfect Calls: ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["winner"]) ? $context["winner"] : null), "coins", array())), "html", null, true);
            echo "</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
        } else {
            // line 58
            echo "\t\t\t<div style=\"height: 300px;\"></div>
\t\t";
        }
        // line 60
        echo "\t\t<div class=\"row table_row2\">
\t\t\t<div class=\"content-background-footer center-block\">
\t\t\t\t<div class=\"table-content table-responsive\">
\t\t\t\t\t<table class=\"table win_text\">      
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td colspan=\"6\"><p class=\"text_bd_table upper-bold\">Prize WINNERS</p></td>
\t\t\t\t\t\t\t</tr>  
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t\t\t\t\t<select name=\"country_id\" id=\"CountrySelector\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"0\">ALL</option>
\t\t\t\t\t\t\t\t\t";
        // line 72
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 73
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td colspan=\"4\"></td>
\t\t\t\t\t\t\t</tr>                           
\t\t\t\t\t\t\t<tr class=\"border_bottom upper-bold\">
\t\t\t\t\t\t\t\t<td class=\"mn_td1\">no.</td>
\t\t\t\t\t\t\t\t<td class=\"mn_td2\">country</td>
\t\t\t\t\t\t\t\t<td class=\"mn_td3\">salesperson</td>
\t\t\t\t\t\t\t\t<td class=\"mn_td4\">Medium Prize</td>
\t\t\t\t\t\t\t\t<td class=\"mn_td5\">Small Prize</td>
\t\t\t\t\t\t\t\t<td class=\"mn_td6\">Total Prize</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        // line 87
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topWinners"]) ? $context["topWinners"] : null));
        foreach ($context['_seq'] as $context["i"] => $context["topWinner"]) {
            // line 88
            echo "\t\t\t\t\t\t\t\t<tr class=\"border_bottom1 country-filter country-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["topWinner"], "country_id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t<td class=\"td1\">";
            // line 89
            echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"td2\">";
            // line 90
            echo twig_escape_filter($this->env, get_country_name((isset($context["countries"]) ? $context["countries"] : null), $this->getAttribute($context["topWinner"], "country_id", array())), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"td3\">";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["topWinner"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["topWinner"], "last_name", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"td4\">";
            // line 92
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["topWinner"], "first_prize_count", array())), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"td5\">";
            // line 93
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["topWinner"], "second_prize_count", array())), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"td6\">";
            // line 94
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (($this->getAttribute($context["topWinner"], "first_prize_count", array()) + $this->getAttribute($context["topWinner"], "second_prize_count", array())) + $this->getAttribute($context["topWinner"], "mega_prize_count", array()))), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['topWinner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>    
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "spinwin/winner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 97,  175 => 94,  171 => 93,  167 => 92,  161 => 91,  157 => 90,  153 => 89,  148 => 88,  144 => 87,  130 => 75,  119 => 73,  115 => 72,  101 => 60,  97 => 58,  85 => 52,  83 => 51,  78 => 48,  73 => 45,  69 => 33,  67 => 32,  63 => 30,  60 => 29,  32 => 4,  29 => 3,  11 => 1,);
    }
}
