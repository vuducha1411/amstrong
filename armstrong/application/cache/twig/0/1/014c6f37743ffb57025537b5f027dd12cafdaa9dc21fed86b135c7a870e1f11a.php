<?php

/* ami/contacts/amended.html.twig */
class __TwigTemplate_014c6f37743ffb57025537b5f027dd12cafdaa9dc21fed86b135c7a870e1f11a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">

    ";
        // line 3
        echo form_open(site_url(("ami/contact/amended/" . (isset($context["contactId"]) ? $context["contactId"] : null))), array("class" => "modal-content"));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["contactId"]) ? $context["contactId"] : null), "html", null, true);
        echo " - Amended Data</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"tab-content m-t-md\">
                <div id=\"amended\">
                    <table class=\"table table-striped table-bordered table-hover\">
                        <thead>
                        <tr>
                            <th>Field</th>
                            <th class=\"danger\">Old Value</th>
                            <th class=\"success\">New Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 21
            echo "                            <tr>
                                <td>";
            // line 22
            echo twig_escape_filter($this->env, $context["field"], "html", null, true);
            echo "</td>
                                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "old_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "new_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            ";
        // line 33
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "contact"))) {
            // line 34
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, site_url((("ami/contacts/edit/" . (isset($context["contactId"]) ? $context["contactId"] : null)) . "?filter=1")), "html", null, true);
            echo "\" class=\"btn btn-primary\"><i class=\"fa fa-fw fa-edit\"></i> Edit</a></a>
            ";
        }
        // line 36
        echo "            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    ";
        // line 38
        echo form_close();
        echo "
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/contacts/amended.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 38,  86 => 36,  80 => 34,  78 => 33,  70 => 27,  61 => 24,  57 => 23,  53 => 22,  50 => 21,  46 => 20,  29 => 6,  23 => 3,  19 => 1,);
    }
}
