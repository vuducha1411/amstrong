<?php

/* ami/call_records/edit.html.twig */
class __TwigTemplate_01f7cacbc758d7757be36fefe38f0708c569a4fd7d11fc000f9235bd4d41828d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/call_records/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#DateTimeCallInput').daterangepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        timePicker: true,
        timePicker12Hour: false,
        timePickerIncrement: 10
    }).on('apply.daterangepicker', function(ev, picker) {
        \$('input[name=datetime_call_start]').val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
        \$('input[name=datetime_call_end]').val(picker.endDate.format('YYYY-MM-DD HH:mm:ss'));
    });
});
</script>
";
    }

    // line 22
    public function block_css($context, array $blocks = array())
    {
        // line 23
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        // line 28
        echo "
    ";
        // line 29
        echo form_open(site_url("ami/call_records/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_call_records_id" => $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_call_records_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Call Records</h1>
                <div class=\"text-right\">
                    ";
        // line 36
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/call_records/edit.html.twig", 36)->display(array_merge($context, array("url" => "ami/call_records", "id" => $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_call_records_id", array()), "permission" => "call_records")));
        // line 37
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">  

                ";
        // line 46
        if ((isset($context["plans"]) ? $context["plans"] : null)) {
            // line 47
            echo "                    <div class=\"form-group\">
                        ";
            // line 48
            echo form_label("Route Plans", "armstrong_2_route_plan_id", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 50
            echo form_dropdown("armstrong_2_route_plan_id", (isset($context["plans"]) ? $context["plans"] : null), $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_route_plan_id", array()), (("
                                class=\"form-control AutoInputTrigger\"
                                data-parsley-required=\"true\" 
                                data-url=\"" . site_url("ami/route_plan/info")) . "\"
                            "));
            // line 54
            echo "
                        </div>
                    </div>
                ";
        }
        // line 58
        echo "
                <div class=\"form-group\">
                    ";
        // line 60
        echo form_label("Salespersons", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 62
        echo form_input(array("name" => "armstrong_2_salespersons_id", "value" => $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_salespersons_id", array()), "class" => "form-control AutoInput", "readonly" => "readonly"));
        // line 65
        echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 70
        echo form_label("Customers", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 72
        echo form_input(array("name" => "armstrong_2_customers_id", "value" => $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_customers_id", array()), "class" => "form-control AutoInput", "readonly" => "readonly"));
        // line 75
        echo "
                    </div>
                </div>    

                <div class=\"form-group\">
                    ";
        // line 80
        echo form_label("Date Time Call", "date_time_call", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 82
        echo form_input(array("name" => "date_time_call", "value" => (($this->getAttribute((isset($context["record"]) ? $context["record"] : null), "armstrong_2_call_records_id", array())) ? ((($this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_start", array()) . " - ") . $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_end", array()))) : (null)), "class" => "form-control", "id" => "DateTimeCallInput", "data-parsley-required" => "true"));
        // line 86
        echo "
                        <input type=\"hidden\" name=\"datetime_call_start\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_start", array()), "html", null, true);
        echo "\" />
                        <input type=\"hidden\" name=\"datetime_call_end\" value=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "datetime_call_end", array()), "html", null, true);
        echo "\" />
                    </div>
                </div>            

                ";
        // line 109
        echo "        </div>
    </div> 

    ";
        // line 112
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/call_records/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 112,  177 => 109,  170 => 88,  166 => 87,  163 => 86,  161 => 82,  156 => 80,  149 => 75,  147 => 72,  142 => 70,  135 => 65,  133 => 62,  128 => 60,  124 => 58,  118 => 54,  112 => 50,  107 => 48,  104 => 47,  102 => 46,  91 => 37,  89 => 36,  79 => 29,  76 => 28,  73 => 27,  67 => 24,  63 => 23,  60 => 22,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
