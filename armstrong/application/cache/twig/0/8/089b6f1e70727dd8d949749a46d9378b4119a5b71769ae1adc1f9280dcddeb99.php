<?php

/* ami/salespersons/preview.html.twig */
class __TwigTemplate_089b6f1e70727dd8d949749a46d9378b4119a5b71769ae1adc1f9280dcddeb99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Email</h4>
                            <p class=\"list-group-item-text\"><a href=\"mailto:";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "email", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "email", array()), "html", null, true);
        echo "</a></p>
                        </div>
                    </div>

                    ";
        // line 19
        if ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "territory", array())) {
            // line 20
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-compass pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Territory</h4>
                                <p class=\"list-group-item-text\">";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "territory", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 28
        echo "
                    ";
        // line 29
        if ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "handphone", array())) {
            // line 30
            echo "                        <div class=\"list-group-item\">  
                            <i class=\"fa fa-2x fa-mobile pull-left\"></i>
                            <div class=\"m-l-xl\">                       
                                <h4 class=\"list-group-item-heading text-muted\">Phone</h4>
                                <p class=\"list-group-item-text\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "handphone", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 38
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 43
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "salespersons"))) {
            // line 44
            echo "                ";
            echo html_btn(site_url(("ami/salespersons/edit/" . $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 46
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/salespersons/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 46,  91 => 44,  89 => 43,  82 => 38,  75 => 34,  69 => 30,  67 => 29,  64 => 28,  57 => 24,  51 => 20,  49 => 19,  40 => 15,  25 => 5,  19 => 1,);
    }
}
