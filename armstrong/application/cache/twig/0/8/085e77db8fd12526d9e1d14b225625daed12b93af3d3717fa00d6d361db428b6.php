<?php

/* ami/out_of_trade/edit.html.twig */
class __TwigTemplate_085e77db8fd12526d9e1d14b225625daed12b93af3d3717fa00d6d361db428b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/out_of_trade/edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        echo form_open(site_url("ami/out_of_trade/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["out_of_trade"]) ? $context["out_of_trade"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Out of trade</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 14
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 16
        echo html_btn(site_url("ami/out_of_trade"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "

                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 28
        echo "            <div class=\"form-group hi\">
                ";
        // line 29
        echo form_label("Target calls per day", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 31
        echo form_input(array("name" => "average_calls_per_day_target", "value" => $this->getAttribute((isset($context["out_of_trade"]) ? $context["out_of_trade"] : null), "average_calls_per_day_target", array()), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker"))));
        echo "
                </div>
            </div>
            <div class=\"form-group hi\">
                ";
        // line 35
        echo form_label("No. of working hours per day", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 37
        echo form_input(array("name" => "no_of_working_hours_per_day", "value" => $this->getAttribute((isset($context["out_of_trade"]) ? $context["out_of_trade"] : null), "no_of_working_hours_per_day", array()), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 38
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 39
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 40
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 43
        echo "                </div>
            </div>
        </div>
    </div>

    ";
        // line 48
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/out_of_trade/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 48,  98 => 43,  92 => 40,  89 => 39,  87 => 38,  83 => 37,  78 => 35,  71 => 31,  66 => 29,  63 => 28,  49 => 16,  44 => 14,  31 => 5,  28 => 4,  11 => 1,);
    }
}
