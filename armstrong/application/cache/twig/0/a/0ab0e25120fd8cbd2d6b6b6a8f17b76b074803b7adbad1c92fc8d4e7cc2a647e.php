<?php

/* ami/global_ssd/viewAllData.html.twig */
class __TwigTemplate_0ab0e25120fd8cbd2d6b6b6a8f17b76b074803b7adbad1c92fc8d4e7cc2a647e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/viewAllData.html.twig", 6);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_css($context, array $blocks = array())
    {
        // line 10
        echo "\t";
        // line 11
        echo "\t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/jquery.dataTables-1.10.16.min.css"), "html", null, true);
        echo "\">
\t";
        // line 12
        $this->displayParentBlock("css", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
\t<style>
\t\t#mainContent {
\t\t\tmargin-top: 20px;
\t\t}
\t</style>
";
    }

    // line 21
    public function block_js($context, array $blocks = array())
    {
        // line 22
        echo "\t";
        $this->displayParentBlock("js", $context, $blocks);
        echo " 
\t<script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables-1.10.16.min.js"), "html", null, true);
        echo "\"></script>
\t";
        // line 25
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.buttons.1.5.1.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.buttons.1.5.1.html5.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/jszip.3.1.3.min.js"), "html", null, true);
        echo "\"></script>
\t";
        // line 29
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.checkboxes.min.js"), "html", null, true);
        echo "\"></script>
\t";
        // line 31
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, site_url("res/js/jquery.table2excel-1.1.10.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/functions.js"), "html", null, true);
        echo "\"></script>

\t<script>
\t\t\$(document).ready(function(){ GLOBAL_SSD_MASTER(\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/entrySSDDataShowAll"), "html", null, true);
        echo "\"); });
\t</script>
";
    }

    // line 40
    public function block_content($context, array $blocks = array())
    {
        // line 41
        echo "\t<div id=\"loadingOverlay\" class=\"overlay load-overlay\">
\t\t";
        // line 43
        echo "\t\t<div class=\"overlay-content\">
\t\t\t<p id=\"overlay-message\"></p>
\t\t</div>
\t</div>

\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1>Global SSD</h1>
\t\t\t\t";
        // line 53
        echo "\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div id=\"mainContent\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-4 col-md-offset-2\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"dotFrom_datepicker\">(Date of transaction) From *</label>
\t\t\t\t\t\t\t<div class='input-group date datepicker'>
\t\t\t\t\t\t\t\t<input
\t\t\t\t\t\t\t\t\tvalue=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array()), "from", array(), "array"), "html", null, true);
        echo "\"
\t\t\t\t\t\t\t\t\ttype='text' 
\t\t\t\t\t\t\t\t\tclass=\"form-control datepicker\" 
\t\t\t\t\t\t\t\t\tid=\"dotFrom_datepicker\" 
\t\t\t\t\t\t\t\t\tname=\"dotFrom_datepicker\" 
\t\t\t\t\t\t\t\t\tplaceholder=\"Select date\"
\t\t\t\t\t\t\t\t\tdata-target=\"#dotTo_datepicker\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\" data-caltarget=\"#dotFrom_datepicker\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-calendar\"></span>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"dot_datepicker\">(Date of transaction) To *</label>
\t\t\t\t\t\t\t<div class='input-group date datepicker'>
\t\t\t\t\t\t\t\t<input
\t\t\t\t\t\t\t\t\tvalue=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array()), "to", array(), "array"), "html", null, true);
        echo "\"
\t\t\t\t\t\t\t\t\ttype='text' 
\t\t\t\t\t\t\t\t\tclass=\"form-control datepicker datepicker-today\" 
\t\t\t\t\t\t\t\t\tid=\"dotTo_datepicker\" 
\t\t\t\t\t\t\t\t\tname=\"dotTo_datepicker\" 
\t\t\t\t\t\t\t\t\tplaceholder=\"Select date\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\" data-caltarget=\"#dotTo_datepicker\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-calendar\"></span>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-default pull-right\" id=\"filterDateOfTrans\"><i class=\"fa fa-eye\"></i> View</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"acknowledged\">
\t\t\t\t\t\t\t<input id=\"acknowledged\" name=\"acknowledged\" type=\"checkbox\">
\t\t\t\t\t\t\tDisplay acknowledged SSDs
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<label for=\"\">Actions</label>
\t\t\t\t\t\t<ul class=\"list-group en-hover\">
\t\t\t\t\t\t\t<li class=\"list-group-item\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-eye\"></i>
\t\t\t\t\t\t\t\t<a class=\"ml5\" href=\"";
        // line 111
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/viewAcknowledged"), "html", null, true);
        echo "\">View Acknowledged Data</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li id=\"ssdExportBtn\" class=\"list-group-item export\"> 
\t\t\t\t\t\t\t\t<i class=\"fa fa-download\"></i>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"list-group-item export\"> 
\t\t\t\t\t\t\t\t";
        // line 118
        echo "\t\t\t\t\t\t\t\t<form method=\"POST\" action=\"";
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/acknowledge"), "html", null, true);
        echo "\" id=\"form_acknowledge\">
\t\t\t\t\t\t\t\t\t<button disabled=\"disabled\" type=\"submit\" class=\"btn btn-primary btn-block\">ACKNOWLEDGE SELECTED DATA</button>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-12 mb15 scroll-f\">
\t\t\t<hr/>
\t\t\t";
        // line 130
        echo "\t\t\t\t<table cellspacing=\"0\" width=\"100%\" id=\"tbl_global_ssd\" class=\"mb15 table-ssd table table-bordered table-condensed table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t<th>ID</th>
\t\t\t\t\t\t\t<th>DISTRIBUTOR</th>
\t\t\t\t\t\t\t<th>DT_CUSTOMER_CODE</th>
\t\t\t\t\t\t\t<th>DT_CUSTOMER_NAME</th>
\t\t\t\t\t\t\t<th>CUSTOMERS</th>
\t\t\t\t\t\t\t<th>GROUPING_CUSTOMER</th>
\t\t\t\t\t\t\t<th>TEAM</th>
\t\t\t\t\t\t\t<th>SALESMAN</th>
\t\t\t\t\t\t\t<th>CHANNELS</th>
\t\t\t\t\t\t\t<th>SALESMAN_CODE</th>
\t\t\t\t\t\t\t<th>WHOLESALE_SUB_CHANNEL</th>
\t\t\t\t\t\t\t<th>GOLD_PLUS</th>
\t\t\t\t\t\t\t<th>CPU</th>
\t\t\t\t\t\t\t<th>OEM</th>
\t\t\t\t\t\t\t<th>DT_PRODUCT_CODE</th>
\t\t\t\t\t\t\t<th>DT_PRODUCT_NAME</th>
\t\t\t\t\t\t\t<th>PRODUCT</th>
\t\t\t\t\t\t\t<th>PRODUCT_CODE</th>
\t\t\t\t\t\t\t<th>PRODUCT_GROUPING</th>
\t\t\t\t\t\t\t<th>CATEGORY</th>
\t\t\t\t\t\t\t<th>MARKET</th>
\t\t\t\t\t\t\t<th>HURRICANE_10</th>
\t\t\t\t\t\t\t<th>TOP_SKUS</th>
\t\t\t\t\t\t\t<th>BRAND</th>
\t\t\t\t\t\t\t<th>YEAR</th>
\t\t\t\t\t\t\t<th>MONTH</th>
\t\t\t\t\t\t\t<th>DATE_OF_TRANSACTION</th>
\t\t\t\t\t\t\t<th>SUM_OF_TOTAL_PURCHASE_PRODUCT</th>
\t\t\t\t\t\t\t<th>SUM_OF_TOTAL_QTY_PURCHASE</th>
\t\t\t\t\t\t\t<th>UOM</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t";
        // line 170
        echo "\t\t\t\t</table>
\t\t\t";
        // line 172
        echo "\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/viewAllData.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 172,  252 => 170,  214 => 130,  199 => 118,  190 => 111,  160 => 84,  140 => 67,  124 => 53,  113 => 43,  110 => 41,  107 => 40,  100 => 36,  94 => 33,  90 => 32,  85 => 31,  80 => 29,  76 => 27,  72 => 26,  67 => 25,  63 => 23,  58 => 22,  55 => 21,  44 => 13,  40 => 12,  35 => 11,  33 => 10,  30 => 9,  11 => 6,);
    }
}
