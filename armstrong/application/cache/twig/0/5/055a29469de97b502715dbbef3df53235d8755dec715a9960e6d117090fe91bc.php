<?php

/* ami/reports/custom/penetration_data_report.twig.html */
class __TwigTemplate_055a29469de97b502715dbbef3df53235d8755dec715a9960e6d117090fe91bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("ami/reports/custom/filter.twig.html", "ami/reports/custom/penetration_data_report.twig.html", 1)->display(array_merge($context, array("channel" => 1, "otm" => 1, "leaderAndPerson" => 1)));
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/penetration_data_report.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
