<?php

/* ami/objective_records/edit.html.twig */
class __TwigTemplate_02da50aa673cc6d8b3ea80507269fd7e84ab96e9d11b904f66e254ddb13eb802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/objective_records/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {
            \$('#DateTimeCallInput').daterangepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                timePicker: true,
                timePicker12Hour: false,
                timePickerIncrement: 10
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('input[name=datetime_start]').val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
                \$('input[name=datetime_end]').val(picker.endDate.format('YYYY-MM-DD HH:mm:ss'));
            });
        });
        \$(\".type_objective input:radio\").change(function () {
            val = \$(this).val();
            if (val == 0) {
                \$('.country_channels').hide();
            } else if (val == 1) {
                \$('.country_channels').show();
            }
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
    <style>
        select[multiple].ref_id{
            height: 250px;
        }
    </style>
";
    }

    // line 38
    public function block_content($context, array $blocks = array())
    {
        // line 39
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 54
        echo form_open(site_url("ami/objective_records/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_objective_records_id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_objective_records_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Objective Records</h1>

                <div class=\"text-right\">
                    ";
        // line 62
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/objective_records/edit.html.twig", 62)->display(array_merge($context, array("url" => "ami/objective_records", "armstrong_2_objective_records_id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_objective_records_id", array()), "permission" => "objective_records")));
        // line 63
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 72
        echo form_label("Title", "description", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 74
        echo form_input(array("name" => "description", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "description", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 78
        echo form_label("Date Time Call", "date_time_call", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 80
        echo form_input(array("name" => "date_time_call", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_objective_records_id", array())) ? ((($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "datetime_start", array()) . " - ") . $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "datetime_end", array()))) : (null)), "class" => "form-control", "id" => "DateTimeCallInput", "data-parsley-required" => "true"));
        // line 84
        echo "
                    <input type=\"hidden\" name=\"datetime_start\" value=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "datetime_start", array()), "html", null, true);
        echo "\"/>
                    <input type=\"hidden\" name=\"datetime_end\" value=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "datetime_end", array()), "html", null, true);
        echo "\"/>
                </div>
            </div>
\t\t\t<div class=\"form-group\">
                ";
        // line 90
        echo form_label("App type", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley col-sm\">
                    <label class=\"radio-inline \">";
        // line 92
        echo form_radio("app_type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 0));
        echo " Pull</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 93
        echo form_radio("app_type", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 1));
        echo " Push</label>
                    <label class=\"radio-inline\">";
        // line 94
        echo form_radio("app_type", 2, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 2));
        echo " Mix</label>
                </div>
            </div>
            <div class=\"form-group type_objective\">
                ";
        // line 98
        echo form_label("Objective", "type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 100
        echo form_radio("type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()) == 0));
        echo " Country</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 101
        echo form_radio("type", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()) == 1));
        echo " Channel</label>
                </div>
            </div>
            <div class=\"form-group country_channels\" style=\"display:";
        // line 104
        echo ((($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()) == 1)) ? ("block") : ("none"));
        echo "\">
                ";
        // line 105
        echo form_label("Country Channels", "country_channels_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 107
        echo form_dropdown("ref_id[]", (isset($context["country_channels"]) ? $context["country_channels"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "ref_id", array()), "class=\"form-control ref_id\" multiple data-parsley-required=\"false\"");
        echo "
                    <div><p><strong>Select multiple by holding CTRL or SHIFT while clicking.</strong></p></div>
                </div>
            </div>
            ";
        // line 111
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>
    ";
        // line 114
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/objective_records/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 114,  216 => 111,  209 => 107,  204 => 105,  200 => 104,  194 => 101,  190 => 100,  185 => 98,  178 => 94,  174 => 93,  170 => 92,  165 => 90,  158 => 86,  154 => 85,  151 => 84,  149 => 80,  144 => 78,  137 => 74,  132 => 72,  121 => 63,  119 => 62,  108 => 54,  91 => 39,  88 => 38,  77 => 31,  72 => 30,  69 => 29,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
