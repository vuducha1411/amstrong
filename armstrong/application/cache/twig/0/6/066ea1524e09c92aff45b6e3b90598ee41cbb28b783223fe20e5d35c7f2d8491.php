<?php

/* ami/distributors/pending_action.html.twig */
class __TwigTemplate_066ea1524e09c92aff45b6e3b90598ee41cbb28b783223fe20e5d35c7f2d8491 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">

    ";
        // line 3
        echo form_open(site_url(("ami/distributors/pending" . $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()))), array("class" => "modal-content"));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            ";
        // line 9
        if ((isset($context["amendedData"]) ? $context["amendedData"] : null)) {
            // line 10
            echo "                ";
            $this->loadTemplate("ami/distributors/pending_action_amended_data.html.twig", "ami/distributors/pending_action.html.twig", 10)->display($context);
            // line 11
            echo "            ";
        } else {
            // line 12
            echo "                ";
            $this->loadTemplate("ami/distributors/pending_action_data.html.twig", "ami/distributors/pending_action.html.twig", 12)->display($context);
            // line 13
            echo "            ";
        }
        // line 14
        echo "        </div>
        <div class=\"modal-footer\">
            ";
        // line 17
        echo "                ";
        // line 18
        echo "            ";
        // line 19
        echo "            ";
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
            // line 20
            echo "            ";
            if (((isset($context["app"]) ? $context["app"] : null) == "app")) {
                // line 21
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/distributors/approve/" . $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()))), "html", null, true);
                echo "\" class=\"btn btn-success btn-pending-submit\"><i class=\"fa fa-fw fa-thumbs-up\"></i> Approve</a></a>
           ";
            }
            // line 23
            echo "            ";
            if (((isset($context["app"]) ? $context["app"] : null) == "rej")) {
                // line 24
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/distributors/reject/" . $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()))), "html", null, true);
                echo "\" class=\"btn btn-danger btn-pending-submit\"><i class=\"fa fa-fw fa-thumbs-down\"></i> Reject</a></a>
           ";
            }
            // line 26
            echo "            ";
        }
        // line 27
        echo "            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    ";
        // line 29
        echo form_close();
        echo "
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/distributors/pending_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 29,  83 => 27,  80 => 26,  74 => 24,  71 => 23,  65 => 21,  62 => 20,  59 => 19,  57 => 18,  55 => 17,  51 => 14,  48 => 13,  45 => 12,  42 => 11,  39 => 10,  37 => 9,  29 => 6,  23 => 3,  19 => 1,);
    }
}
