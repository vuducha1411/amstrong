<?php

/* ami/login.html.twig */
class __TwigTemplate_60790b36bc00964cb723cdbaddbc1fc08d6c3e552478ce37b414c380dac06651 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/login.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "

<script>
\$(function() {
    \$('.dropdown-toggle').dropdown();
    
    \$(document).on('click', '.country_select', function (e) {
        var \$this = \$(this),
            countryId = \$this.data('countryid');

        if (countryId)
        {
            \$('#countrySelect').empty().html(\$this.html());
            \$('#countryId').val(countryId);
        }
    });

});
</script>
";
    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        // line 26
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-4 col-md-offset-4\">

                <div class=\"login-panel panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">Please Sign In</h3>
                    </div>
                    <div class=\"panel-body\">
                        ";
        // line 35
        if ((validation_errors() || (isset($context["alert_failed"]) ? $context["alert_failed"] : null))) {
            // line 36
            echo "                            <div class=\"alert alert-danger\">
                                ";
            // line 37
            echo validation_errors();
            echo "
                                ";
            // line 38
            echo twig_escape_filter($this->env, (isset($context["alert_failed"]) ? $context["alert_failed"] : null), "html", null, true);
            echo "
                            </div>
                        ";
        }
        // line 41
        echo "
                        ";
        // line 42
        echo form_open("ami/dashboard/check_login");
        echo "
                        <fieldset>
                            <div class=\"form-group\">
                                <?php //echo form_dropdown('country_id', \$countries, \$country_id, 'class=\"form-control\"') ?>
                                <div class=\"dropdown\">
                                    <button class=\"btn btn-default dropdown-toggle full-width\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\">
                                        <span id=\"countrySelect\">
                                            ";
        // line 49
        if ((isset($context["country_id"]) ? $context["country_id"] : null)) {
            // line 50
            echo "                                                ";
            echo $this->getAttribute((isset($context["countries"]) ? $context["countries"] : null), (isset($context["country_id"]) ? $context["country_id"] : null), array(), "array");
            echo "
                                            ";
        } else {
            // line 52
            echo "                                                Select a Country
                                            ";
        }
        // line 53
        echo "                                        
                                        </span>
                                        <span class=\"caret\"></span>
                                    </button>
                                    <ul class=\"dropdown-menu full-width text-center\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">
                                        ";
        // line 58
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["country"]) {
            // line 59
            echo "                                            <li role=\"presentation\"><a class=\"country_select\" data-countryid=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" role=\"menuitem\" tabindex=\"-1\" href=\"#\">";
            echo $context["country"];
            echo "</a></li>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                                    </ul>
                                </div>
                            </div>
                            <div class=\"form-group\" for=\"country\">
                                <label class=\"control-label\" for=\"email\">Please key in your Armstrong user ID</label>
                                ";
        // line 66
        echo form_input("email", set_value("email", ""), "id=\"email\" class=\"form-control\" placeholder=\"E-mail\" autofocus");
        echo "
                            </div>
                            <div class=\"form-group\">
                                <label class=\"control-label\" for=\"password\">Please key in your Armstrong user password</label>
                                ";
        // line 70
        echo form_password("password", set_value("password", ""), "id=\"password\" class=\"form-control\" placeholder=\"Password\" autofocus");
        echo "
                            </div>
                            <input type=\"hidden\" name=\"country_id\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, (isset($context["country_id"]) ? $context["country_id"] : null), "html", null, true);
        echo "\" id=\"countryId\">
                            <!-- Change this to a button or input when using this as a form -->
                            ";
        // line 74
        echo form_submit("submit", "Login", "class=\"btn btn-lg btn-success btn-block\"");
        echo "
                        </fieldset>
                        ";
        // line 76
        echo form_close();
        echo "
                    </div>
                </div>

            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 76,  156 => 74,  151 => 72,  146 => 70,  139 => 66,  132 => 61,  121 => 59,  117 => 58,  110 => 53,  106 => 52,  100 => 50,  98 => 49,  88 => 42,  85 => 41,  79 => 38,  75 => 37,  72 => 36,  70 => 35,  59 => 26,  56 => 25,  32 => 4,  29 => 3,  11 => 1,);
    }
}
