<?php

/* ami/daterange_config/index.html.twig */
class __TwigTemplate_67c7026d3d10e55c1c765d26d952b0432a60a1293ef0363c50dc8c607c60a1a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/daterange_config/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {

            var arr_loop = ['mighty_10_', 'channel_basket_penetration_', 'active_sku_', 'sampling_history_', 'listing_and_availability_'];
            \$.each(arr_loop, function (k, v) {
                var object = \$('.date-picker[name=\"' + v + 'start\"]');
                var object_end = \$('.date-picker[name=\"' + v + 'end\"]');
                if (!object.val()) {
                    object_end.prop('disabled', true);
                }else{
                    object_end.datepicker({
                        startDate: object.val()
                    });
                }

                object.on('change', function () {
                    if (!object.val()) {
                        object_end.val('');
                        object_end.prop('disabled', true);
                    }
                });

                object_end.on('change', function(){
                    object_end.closest('.form-group').removeClass('has-error');
                });

                object.datepicker().on('changeDate', function () {
                    object_end.prop('disabled', false);
                    var startDate = new Date(\$(this).val());
                    var endDate = (object_end.val()) ? new Date(object_end.val()) : startDate;
                    if (startDate >= endDate) {
                        object_end.val('');
                    }
                    startDate.setDate(startDate.getDate());
                    object_end.datepicker('remove');
                    object_end.datepicker({
                        startDate: startDate
                    });
                });
            });

            \$('button[type=\"submit\"]').on('click',function(e){
                \$.each(arr_loop, function (k, v) {
                    var object = \$('.date-picker[name=\"' + v + 'start\"]');
                    var object_end = \$('.date-picker[name=\"' + v + 'end\"]');
                    if (object.val() && !object_end.val()) {
                        object_end.closest('.form-group').addClass('has-error');
                    }
                })
                if(\$('.has-error').length < 1){
                    \$('form').submit();
                }else{
                    e.preventDefault();
                }
            });
        });
    </script>
";
    }

    // line 64
    public function block_css($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
";
    }

    // line 68
    public function block_content($context, array $blocks = array())
    {
        // line 69
        echo "    ";
        echo form_open(site_url("ami/daterange_config/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Customer Analysis Configuration</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        <button name=\"\" type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-fw fa-check\"></i> Save
                        </button>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 90
        echo form_label("Mighty 10", "mighty_10", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 92
        echo form_dropdown("mighty_10_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "mighty_10_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 97
        echo form_label("Channel Basket Penetration", "channel_basket_penetration", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 99
        echo form_dropdown("channel_basket_penetration_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "channel_basket_penetration_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 104
        echo form_label("Active SKUe", "active_sku", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 106
        echo form_dropdown("active_sku_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "active_sku_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 111
        echo form_label("Sampling History", "sampling_history", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 113
        echo form_dropdown("sampling_history_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_history_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 118
        echo form_label("Listing And Availability", "listing_and_availability", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 120
        echo form_dropdown("listing_and_availability_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "listing_and_availability_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 125
        echo form_label("New Grip", "new_grip", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 127
        echo form_dropdown("new_grip_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "new_grip_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 132
        echo form_label("New Grab", "new_grab", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 134
        echo form_dropdown("new_grab_rolling", (isset($context["rolling"]) ? $context["rolling"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "new_grab_rolling", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
        </div>
    </div>

    ";
        // line 140
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/daterange_config/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 140,  230 => 134,  225 => 132,  217 => 127,  212 => 125,  204 => 120,  199 => 118,  191 => 113,  186 => 111,  178 => 106,  173 => 104,  165 => 99,  160 => 97,  152 => 92,  147 => 90,  122 => 69,  119 => 68,  113 => 66,  108 => 65,  105 => 64,  42 => 5,  38 => 4,  33 => 3,  30 => 2,  11 => 1,);
    }
}
