<?php

/* ami/admin_management/index.html.twig */
class __TwigTemplate_6fdd954b67aec20551eefff07a0558fbd6b380b9038dc25d69e13e6b96b24497 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/admin_management/index.html.twig", 2);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_js($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(document).ready(function() {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/admin_management/ajaxData"), "html", null, true);
        echo "\",*/
                \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "
    ";
        // line 36
        echo form_open(site_url("ami/admin_management/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 41
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Admin Management - Draft") : ("Admin Management"));
        echo "</h1>
                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 44
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 46
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/admin_management/index.html.twig", 46)->display(array_merge($context, array("url" => "ami/admin_management", "icon" => "fa-user", "title" => "Admins", "permission" => "admin_management")));
        // line 47
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 63
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "admin_management"))) {
            // line 64
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 66
        echo "                                <th>Id</th>
                                <th>Name</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["admins"]) ? $context["admins"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
            // line 75
            echo "                                <tr>
                                    ";
            // line 76
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "admin_management"))) {
                // line 77
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "id", array())));
                echo "</td>
                                    ";
            }
            // line 79
            echo "                                    <td>";
            echo wordTrim($this->getAttribute($context["admin"], "id", array()), 100);
            echo "</td>
                                    <td><a href=\"";
            // line 80
            echo twig_escape_filter($this->env, site_url(("ami/admin_management/edit/" . $this->getAttribute($context["admin"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["admin"], "first_name", array()) . " ") . $this->getAttribute($context["admin"], "last_name", array())), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["admin"], "first_name", array()) . " ") . $this->getAttribute($context["admin"], "last_name", array())), "html", null, true);
            echo "</a></td>
                                    <td class=\"center\">";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 84
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "admin_management")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "admin_management")))) {
                // line 85
                echo "                                            ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/admin_management/index.html.twig", 85)->display(array_merge($context, array("url" => "ami/admin_management", "id" => $this->getAttribute($context["admin"], "id", array()), "permission" => "admin_management")));
                // line 86
                echo "                                        ";
            }
            // line 87
            echo "                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 94
        $context["paginationUrl"] = (((isset($context["draft"]) ? $context["draft"] : null)) ? ("ami/admin_management/draft") : ("ami/admin_management"));
        // line 95
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => site_url((isset($context["paginationUrl"]) ? $context["paginationUrl"] : null)), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 109
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/admin_management/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 109,  227 => 95,  225 => 94,  219 => 90,  203 => 87,  200 => 86,  197 => 85,  195 => 84,  190 => 82,  186 => 81,  178 => 80,  173 => 79,  167 => 77,  165 => 76,  162 => 75,  145 => 74,  135 => 66,  131 => 64,  129 => 63,  111 => 47,  109 => 46,  104 => 44,  98 => 41,  90 => 36,  87 => 35,  84 => 34,  78 => 31,  73 => 30,  70 => 29,  54 => 16,  43 => 8,  39 => 7,  33 => 5,  30 => 4,  11 => 2,);
    }
}
