<?php

/* ami/components/form_btn.html.twig */
class __TwigTemplate_64d2220e24a240e1de1769a79f6e9ae8b0df66ba974ce9b76e29a278438021e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<div class=\"btn-header-toolbar\">
    ";
        // line 4
        if (((isset($context["show_by_salespersons"]) ? $context["show_by_salespersons"] : null) == 1)) {
            // line 5
            echo "        <a  onclick=\"getSalesperson()\" title=\"Delete\" class=\"btn btn-info\" ><i class=\"fa fa-users\"></i> Show</a>
        ";
            // line 6
            echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-check\"></i> Transfer", "onclick" => "myConfirm()", "class" => "btn btn-success"));
            echo "
        ";
            // line 7
            echo html_btn(site_url((isset($context["url"]) ? $context["url"] : null)), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
            echo "
    ";
        } else {
            // line 9
            echo "        ";
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null))) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", (isset($context["permission"]) ? $context["permission"] : null))))) {
                // line 10
                echo "            ";
                echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
                echo "
        ";
            }
            // line 12
            echo "
        ";
            // line 13
            echo html_btn(site_url((isset($context["url"]) ? $context["url"] : null)), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
            echo "
        ";
            // line 14
            if (((isset($context["id"]) ? $context["id"] : null) && call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", (isset($context["permission"]) ? $context["permission"] : null))))) {
                // line 15
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, site_url((((isset($context["url"]) ? $context["url"] : null) . "/delete/") . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
                echo "\" title=\"Delete\" class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
        ";
            }
            // line 17
            echo "    ";
        }
        // line 18
        echo "

</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/form_btn.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  62 => 17,  56 => 15,  54 => 14,  50 => 13,  47 => 12,  41 => 10,  38 => 9,  33 => 7,  29 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
