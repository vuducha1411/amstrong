<?php

/* ami/perfect_store/preview.html.twig */
class __TwigTemplate_64f4526f2ba8e23cabf68ed4aaa4e143e2d3c541e8ec9ebc68a9a47dd613a9a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">Perfect Store</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-user pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Salespersons</h4>
                            <p class=\"list-group-item-text\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "armstrong_2_salespersons_id", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">   
                        <i class=\"fa fa-2x fa-coffee pull-left\"></i>
                        <div class=\"m-l-xl\">                    
                            <h4 class=\"list-group-item-heading text-muted\">Call Records</h4>
                            <p class=\"list-group-item-text\">";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "armstrong_2_call_records_id", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 31
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "salespersons"))) {
            // line 32
            echo "                ";
            echo html_btn(site_url(("ami/salespersons/edit/" . $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 34
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/perfect_store/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 34,  59 => 32,  57 => 31,  46 => 23,  35 => 15,  19 => 1,);
    }
}
