<?php

/* spinwin/layout.html.twig */
class __TwigTemplate_6b456891da68264b06e9cc3212b9cc3b22aa0d3d758928451a143df08d3b037f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "spinwin/layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "SPIN & WIN";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        $this->displayParentBlock("head", $context, $blocks);
        echo "
<script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "\"></script>
<script>
var BASE_URL = '";
        // line 10
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "',
    CURRENT_URL = '";
        // line 11
        echo twig_escape_filter($this->env, current_url(), "html", null, true);
        echo "';
function base_url(){ return BASE_URL; }
function current_url(){ return CURRENT_URL; }
</script>
";
    }

    // line 17
    public function block_css($context, array $blocks = array())
    {
        // line 18
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
<link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("res/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\"/>
<link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/css/ami.css"), "html", null, true);
        echo "\"/>
<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/css/spinwin/style.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        // line 26
        echo "
    <div class=\"container-fluid\">
        <div class=\"wrapper\">
            ";
        // line 29
        $this->loadTemplate("spinwin/header.html.twig", "spinwin/layout.html.twig", 29)->display($context);
        // line 30
        echo "
            ";
        // line 31
        $this->displayBlock('content', $context, $blocks);
        // line 32
        echo "        </div>
    </div>

";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "spinwin/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 31,  104 => 32,  102 => 31,  99 => 30,  97 => 29,  92 => 26,  89 => 25,  83 => 21,  79 => 20,  75 => 19,  70 => 18,  67 => 17,  58 => 11,  54 => 10,  49 => 8,  45 => 7,  41 => 6,  38 => 5,  32 => 3,  11 => 1,);
    }
}
