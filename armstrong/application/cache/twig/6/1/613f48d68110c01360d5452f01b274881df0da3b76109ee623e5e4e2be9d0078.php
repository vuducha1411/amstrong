<?php

/* ami/setting_game/play_setting.html.twig */
class __TwigTemplate_613f48d68110c01360d5452f01b274881df0da3b76109ee623e5e4e2be9d0078 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_game/play_setting.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {
            \$('#DateActive').daterangepicker({
                format: 'YYYY-MM-DD',
                timePicker: true,
                timePicker12Hour: false,
                timePickerIncrement: 10
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('input[name=date_start]').val(picker.startDate.format('YYYY-MM-DD'));
                \$('input[name=date_expired]').val(picker.endDate.format('YYYY-MM-DD'));
            });
            \$('#DateActiveSpecial').daterangepicker({
                format: 'YYYY-MM-DD',
                timePicker: true,
                timePicker12Hour: false,
                timePickerIncrement: 10
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('input[name=\"date_prize[prize_special][from]\"]').val(picker.startDate.format('YYYY-MM-DD'));
                \$('input[name=\"date_prize[prize_special][to]\"]').val(picker.endDate.format('YYYY-MM-DD'));
            });
            \$('#DateActiveFirst').daterangepicker({
                format: 'YYYY-MM-DD',
                timePicker: true,
                timePicker12Hour: false,
                timePickerIncrement: 10
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('input[name=\"date_prize[prize_first][from]\"]').val(picker.startDate.format('YYYY-MM-DD'));
                \$('input[name=\"date_prize[prize_first][to]\"]').val(picker.endDate.format('YYYY-MM-DD'));
            });
            \$('#DateActiveSecond').daterangepicker({
                format: 'YYYY-MM-DD',
                timePicker: true,
                timePicker12Hour: false,
                timePickerIncrement: 10
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('input[name=\"date_prize[prize_second][from]\"]').val(picker.startDate.format('YYYY-MM-DD'));
                \$('input[name=\"date_prize[prize_second][to]\"]').val(picker.endDate.format('YYYY-MM-DD'));
            });
            \$('.group_pull input').keyup(function(e){
                var dInput = \$(this).val();
                \$('.title_pull').html(dInput);
            });
            \$('.group_push input').keyup(function(e){
                var dInput = \$(this).val();
                \$('.title_push').html(dInput);
            });
        });

//        function get_number(divClass){
//            alert(count_checkbox(divClass));
//        }
//        function count_checkbox(divClass){
//            var \$b = \$('.'+ divClass +' input[type=checkbox]');
//            return \$b.filter(':checked').length;
//            //alert(\$b.filter(':checked').length);
//        }
    </script>
";
    }

    // line 64
    public function block_css($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 68
    public function block_content($context, array $blocks = array())
    {
        // line 69
        echo "    ";
        echo form_open(site_url("ami/setting_game/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Game Play Settings</h1>

                <div class=\"text-right\">
                    ";
        // line 77
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/setting_game/play_setting.html.twig", 77)->display(array_merge($context, array("url" => "ami/setting_game", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "setting_game")));
        // line 78
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <h4>Game plan configuration</h4>

        <div class=\"form-group\">
            ";
        // line 88
        echo form_label("Date Active", "date_active", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 90
        echo form_input(array("name" => "date_active", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())) ? ((($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_start", array()) . " - ") . $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_expired", array()))) : (null)), "class" => "form-control", "id" => "DateActive", "data-parsley-required" => "true"));
        // line 94
        echo "
                <input type=\"hidden\" name=\"date_start\" value=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_start", array()), "html", null, true);
        echo "\"/>
                <input type=\"hidden\" name=\"date_expired\" value=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_expired", array()), "html", null, true);
        echo "\"/>
            </div>
        </div>
        <hr/>
        <h4>Prizes configuration</h4>

        <div class=\"form-group\">
            ";
        // line 103
        echo form_label("Special Prize", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 105
        echo form_input(array("name" => "config_prize[prize_special]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_special", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
            ";
        // line 107
        echo form_label("Steps number", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 109
        echo form_input(array("name" => "limit_prize[prize_special]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_special", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 113
        echo form_label("1nd Prize", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 115
        echo form_input(array("name" => "config_prize[prize_first]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_first", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
            ";
        // line 117
        echo form_label("Steps number", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 119
        echo form_input(array("name" => "limit_prize[prize_first]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_first", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 123
        echo form_label("2nd Prize", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 125
        echo form_input(array("name" => "config_prize[prize_second]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_second", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
            ";
        // line 127
        echo form_label("Steps number", "description", array("class" => "control-label col-sm-2"));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 129
        echo form_input(array("name" => "limit_prize[prize_second]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_second", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
            </div>
        </div>
        <hr/>
        <h4>PULL SR – PULL SR must conduct ALL <span class=\"title_pull\">";
        // line 133
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "min_number_of_criterias", array())) ? ($this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "min_number_of_criterias", array())) : (1)), "html", null, true);
        echo "</span> out of 6 items from the followings during a sales call to qualify for a
            drawing chance</h4>

        <div class=\"form-group group_pull\">
            <div class=\"col-sm-12\">
                <div class=\"col-sm-3\">";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "min_number_of_criterias", array()), "html", null, true);
        echo ": </div>
                <div class=\"col-sm-3\">
                    ";
        // line 140
        echo form_input(array("name" => "config_pull_new[min_number_of_criterias]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "min_number_of_criterias", array()), "class" => "form-control number_pull", "data-parsley-required" => "true"));
        echo "
                </div>
                <div class=\"col-sm-6\"> </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 146
        echo form_checkbox("config_pull_new[objectives]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "objectives", array()));
        echo "
                        ";
        // line 147
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "objectives", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 148
        echo form_checkbox("config_pull_new[pantryCheck]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "pantryCheck", array()));
        echo "
                        ";
        // line 149
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "pantryCheck", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 154
        echo form_checkbox("config_pull_new[material]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "material", array()));
        echo "
                        ";
        // line 155
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "material", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 156
        echo form_checkbox("config_pull_new[sampling]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "sampling", array()));
        echo "
                        ";
        // line 157
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "sampling", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 162
        echo form_checkbox("config_pull_new[tfos]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "tfos", array()));
        echo "
                        ";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "tfos", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 164
        echo form_checkbox("config_pull_new[competitors]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "competitors", array()));
        echo "
                        ";
        // line 165
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "pull", array()), "competitors", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
        </div>
        <hr/>
        <h4>PUSH SR – PUSH SR must conduct ALL <span class=\"title_push\">";
        // line 170
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "min_number_of_criterias", array())) ? ($this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "min_number_of_criterias", array())) : (1)), "html", null, true);
        echo "</span> out of 6 items from the followings during a sales call to qualify for a
            drawing chance</h4>

        <div class=\"form-group group_push\">
            <div class=\"col-sm-12\">
                <div class=\"col-sm-3\">";
        // line 175
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "min_number_of_criterias", array()), "html", null, true);
        echo ": </div>
                <div class=\"col-sm-3\">
                    ";
        // line 177
        echo form_input(array("name" => "config_push_new[min_number_of_criterias]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "min_number_of_criterias", array()), "class" => "form-control number_push", "data-parsley-required" => "true"));
        echo "
                </div>
                <div class=\"col-sm-6\"> </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 183
        echo form_checkbox("config_push_new[objectives]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "objectives", array()));
        echo "
                        ";
        // line 184
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "objectives", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 185
        echo form_checkbox("config_push_new[pantryCheck]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "pantryCheck", array()));
        echo "
                        ";
        // line 186
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "pantryCheck", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 191
        echo form_checkbox("config_push_new[material]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "material", array()));
        echo "
                        ";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "material", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 193
        echo form_checkbox("config_push_new[perfectStore]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "perfectStore", array()));
        echo "
                        ";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "perfectStore", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 199
        echo form_checkbox("config_push_new[tfos]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "tfos", array()));
        echo "
                        ";
        // line 200
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "tfos", array()), "html", null, true);
        echo "</label>
                    <label class=\"\">";
        // line 201
        echo form_checkbox("config_push_new[competitors]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "competitors", array()));
        echo "
                        ";
        // line 202
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["key_map_title"]) ? $context["key_map_title"] : null), "push", array()), "competitors", array()), "html", null, true);
        echo "</label>
                </div>
            </div>
        </div>
        <hr/>
        <h4>SL – SL must conduct any 1 of the followings during sales call review to qualify for a drawing chance</h4>

        <div class=\"form-group\">
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 212
        echo form_checkbox("config_leader[view_ufs_organization]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_leader", array()), "view_ufs_organization", array()));
        echo "
                        View UFS organization</label>
                    <label class=\"\">";
        // line 214
        echo form_checkbox("config_leader[approve_customers]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_leader", array()), "approve_customers", array()));
        echo "
                        Approval of customer
                        information changes</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"checkbox\">
                    <label class=\"\">";
        // line 221
        echo form_checkbox("config_leader[view_ufs_route_plan]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_leader", array()), "view_ufs_route_plan", array()));
        echo "
                        Conduct SR call
                        coaching</label>
                    <label class=\"\">";
        // line 224
        echo form_checkbox("config_leader[set_sales_target]", 1, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_leader", array()), "set_sales_target", array()));
        echo "
                        Setting of SR sales
                        targets</label>
                </div>
            </div>
            <div class=\"col-sm-4\">
            </div>
        </div>
        <br/>
    </div>

    ";
        // line 235
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/play_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  442 => 235,  428 => 224,  422 => 221,  412 => 214,  407 => 212,  394 => 202,  390 => 201,  386 => 200,  382 => 199,  374 => 194,  370 => 193,  366 => 192,  362 => 191,  354 => 186,  350 => 185,  346 => 184,  342 => 183,  333 => 177,  328 => 175,  320 => 170,  312 => 165,  308 => 164,  304 => 163,  300 => 162,  292 => 157,  288 => 156,  284 => 155,  280 => 154,  272 => 149,  268 => 148,  264 => 147,  260 => 146,  251 => 140,  246 => 138,  238 => 133,  231 => 129,  226 => 127,  221 => 125,  216 => 123,  209 => 119,  204 => 117,  199 => 115,  194 => 113,  187 => 109,  182 => 107,  177 => 105,  172 => 103,  162 => 96,  158 => 95,  155 => 94,  153 => 90,  148 => 88,  136 => 78,  134 => 77,  122 => 69,  119 => 68,  113 => 66,  108 => 65,  105 => 64,  42 => 5,  38 => 4,  33 => 3,  30 => 2,  11 => 1,);
    }
}
