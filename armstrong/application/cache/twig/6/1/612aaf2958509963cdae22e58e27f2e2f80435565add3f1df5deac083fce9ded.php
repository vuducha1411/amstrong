<?php

/* ami/news/preview.html.twig */
class __TwigTemplate_612aaf2958509963cdae22e58e27f2e2f80435565add3f1df5deac083fce9ded extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "title", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            ";
        // line 8
        if ($this->getAttribute((isset($context["images"]) ? $context["images"] : null), 0, array())) {
            // line 9
            echo "                <div class=\"row\">
                    <div class=\"col-lg-4\"><img src=\"";
            // line 10
            echo twig_escape_filter($this->env, ((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/image/thumb") . "/") . $this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), 0, array()), "path", array())), "html", null, true);
            echo "\" class=\"img-responsive\"></div>
                    <div class=\"col-lg-8\"><p>";
            // line 11
            echo $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "content", array());
            echo "</p></div>
                </div>   
            ";
        } else {
            // line 13
            echo "                 
                <p>";
            // line 14
            echo $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "content", array());
            echo "</p>
            ";
        }
        // line 16
        echo "        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 19
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "news"))) {
            // line 20
            echo "                ";
            echo html_btn(site_url(("ami/news/edit/" . $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 22
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/news/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 22,  61 => 20,  59 => 19,  54 => 16,  49 => 14,  46 => 13,  40 => 11,  36 => 10,  33 => 9,  31 => 8,  25 => 5,  19 => 1,);
    }
}
