<?php

/* ami/perfect_store/cc_templates.html.twig */
class __TwigTemplate_62bf80bddfc50d7a1913eb2ec00288f767f0d523d43bc512c14719bbc624b0cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/perfect_store/cc_templates.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_css($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "
    ";
        // line 13
        echo form_open(site_url("ami/perfect_store/save_cc_template"), array("id" => "formCCPerfectStore", "class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Cash and Carry Perfect Store Template</h1>
                <div class=\"text-right\">
                    ";
        // line 20
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/perfect_store/cc_templates.html.twig", 20)->display(array_merge($context, array("url" => "ami/perfect_store", "id" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()), "permission" => "perfect_store")));
        // line 21
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ccContent"]) ? $context["ccContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 32
            echo "                    <li role=\"presentation\" class=\"";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\"><a href=\"#tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\" role=\"tab\"
                                                                                        data-toggle=\"tab\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</a>
                    </li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "            </ul>

            <div class=\"tab-content m-t\">
                ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ccContent"]) ? $context["ccContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 40
            echo "                    <div role=\"tabpanel\" class=\"tab-pane ";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\" id=\"tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">

                        ";
            // line 42
            if ((($context["id"] == 0) && (isset($context["products"]) ? $context["products"] : null))) {
                // line 43
                echo "                            ";
                if ($this->getAttribute($context["content"], "content", array())) {
                    // line 44
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                        // line 45
                        echo "                                    <div class=\"form-group ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\">
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 47
                        echo form_label("Group", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 48
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control group\" data-parsley-required=\"true\">
                                                ";
                        // line 50
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 51
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "group_1", array()))) ? ("selected") : (""));
                            echo " data-id=\"";
                            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 53
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-2\">
                                            ";
                        // line 56
                        echo form_label("Group 2", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 57
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_2]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control\">
                                                ";
                        // line 59
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["brands"]) ? $context["brands"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 60
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "group_2", array()))) ? ("selected") : (""));
                            echo ">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 62
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 65
                        echo form_label("Product", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 66
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control title\" data-parsley-required=\"true\">
                                                ";
                        // line 68
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 69
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\"
                                                            data-id=\"";
                            // line 70
                            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "title", array()))) ? ("selected") : (""));
                            echo ">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 72
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 75
                        echo form_label("Weight", "", array("class" => ""));
                        echo "
                                            ";
                        // line 76
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][weight]"), "value" => $this->getAttribute($context["data"], "weight", array()), "class" => "form-control", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 79
                        echo form_label("Order", "", array("class" => ""));
                        echo "
                                            ";
                        // line 80
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][order_number]"), "value" => $this->getAttribute($context["data"], "order_number", array()), "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-2\">
                                            <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 84
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                            </button>
                                            <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 87
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i
                                                        class=\"fa fa-minus\"></i></button>
                                        </div>
                                        <input type=\"hidden\" class=\"title_id\"
                                               name=\"";
                        // line 91
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"group_id\"
                                               name=\"";
                        // line 93
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"clone-remove\"
                                               name=\"";
                        // line 95
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][id]"), "html", null, true);
                        echo "\"
                                               value=\"";
                        // line 96
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                        <input type=\"hidden\" class=\"clone-move clone-remove\" data-target=\"#deleteTemp\"
                                               data-name=\"product_delete[]\" value=\"";
                        // line 98
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                    </div>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 101
                    echo "                            ";
                } else {
                    // line 102
                    echo "                                <div class=\"form-group ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\">
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 104
                    echo form_label("Group", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 105
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control group\" data-parsley-required=\"true\">
                                            ";
                    // line 107
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 108
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\" }} data-id=\"";
                        echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 110
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
                    // line 113
                    echo form_label("Group 2", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 114
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_2]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control\">
                                            ";
                    // line 116
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["brands"]) ? $context["brands"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 117
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 119
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 122
                    echo form_label("Product", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 123
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][title]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control title\" data-parsley-required=\"true\">
                                            ";
                    // line 125
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 126
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\"
                                                        data-id=\"";
                        // line 127
                        echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 129
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 132
                    echo form_label("Weight", "", array("class" => ""));
                    echo "
                                        ";
                    // line 133
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][weight]"), "value" => null, "class" => "form-control", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 136
                    echo form_label("Order", "", array("class" => ""));
                    echo "
                                        ";
                    // line 137
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][order_number]"), "value" => null, "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-2\">
                                        <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 141
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                        </button>
                                        <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 144
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i
                                                    class=\"fa fa-minus\"></i></button>
                                    </div>
                                    <input type=\"hidden\" class=\"title_id\"
                                           name=\"";
                    // line 148
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][title_id]"), "html", null, true);
                    echo "\">
                                    <input type=\"hidden\" class=\"group_id\"
                                           name=\"";
                    // line 150
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1_id]"), "html", null, true);
                    echo "\">
                                </div>
                            ";
                }
                // line 153
                echo "                        ";
            } elseif (twig_in_filter($context["id"], array(0 => 1, 1 => 4))) {
                // line 154
                echo "                            ";
                if ($this->getAttribute($context["content"], "content", array())) {
                    // line 155
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                        // line 156
                        echo "                                    <div class=\"form-group ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\">
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 158
                        echo form_label("Group", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 159
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control group\" data-parsley-required=\"true\">
                                                ";
                        // line 161
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 162
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "group_1", array()))) ? ("selected") : (""));
                            echo " data-id=\"";
                            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 164
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 167
                        echo form_label("Group 2", "", array("class" => ""));
                        echo "
                                            ";
                        // line 168
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_2]"), "value" => $this->getAttribute($context["data"], "group_2", array()), "class" => "form-control"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-2\">
                                            ";
                        // line 171
                        echo form_label("Brand", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 172
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control title\" data-parsley-required=\"true\">
                                                ";
                        // line 174
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["brands"]) ? $context["brands"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 175
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\"
                                                            data-id=\"";
                            // line 176
                            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "title", array()))) ? ("selected") : (""));
                            echo ">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 178
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 181
                        echo form_label("Weight", "", array("class" => ""));
                        echo "
                                            ";
                        // line 182
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][weight]"), "value" => $this->getAttribute($context["data"], "weight", array()), "class" => "form-control", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 185
                        echo form_label("Order", "", array("class" => ""));
                        echo "
                                            ";
                        // line 186
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][order_number]"), "value" => $this->getAttribute($context["data"], "order_number", array()), "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-2\">
                                            <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 190
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                            </button>
                                            <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 193
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i
                                                        class=\"fa fa-minus\"></i></button>
                                        </div>
                                        <input type=\"hidden\" class=\"title_id\"
                                               name=\"";
                        // line 197
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"group_id\"
                                               name=\"";
                        // line 199
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"clone-remove\"
                                               name=\"";
                        // line 201
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][id]"), "html", null, true);
                        echo "\"
                                               value=\"";
                        // line 202
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                        <input type=\"hidden\" class=\"clone-move clone-remove\" data-target=\"#deleteTemp\"
                                               data-name=\"";
                        // line 204
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "_delete[]\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                    </div>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 207
                    echo "                            ";
                } else {
                    // line 208
                    echo "                                <div class=\"form-group ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\">
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 210
                    echo form_label("Group", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 211
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control group\" data-parsley-required=\"true\">
                                            ";
                    // line 213
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 214
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\" data-id=\"";
                        echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 216
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 219
                    echo form_label("Group 2", "", array("class" => ""));
                    echo "
                                        ";
                    // line 220
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][group_2]"), "value" => null, "class" => "form-control"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
                    // line 223
                    echo form_label("Brand", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 224
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][title]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control title\" data-parsley-required=\"true\">
                                            ";
                    // line 226
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["brands"]) ? $context["brands"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 227
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\"
                                                        data-id=\"";
                        // line 228
                        echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                        echo "\" ";
                        echo ((($context["val"] == $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title", array()))) ? ("selected") : (""));
                        echo ">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 230
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 233
                    echo form_label("Weight", "", array("class" => ""));
                    echo "
                                        ";
                    // line 234
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][weight]"), "value" => null, "class" => "form-control", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 237
                    echo form_label("Order", "", array("class" => ""));
                    echo "
                                        ";
                    // line 238
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][order_number]"), "value" => null, "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-2\">
                                        <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 242
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                        </button>
                                        <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 245
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i
                                                    class=\"fa fa-minus\"></i></button>
                                    </div>
                                    <input type=\"hidden\" class=\"title_id\"
                                           name=\"";
                    // line 249
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][title_id]"), "html", null, true);
                    echo "\">
                                    <input type=\"hidden\" class=\"group_id\"
                                           name=\"";
                    // line 251
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1_id]"), "html", null, true);
                    echo "\">
                                </div>
                            ";
                }
                // line 254
                echo "                        ";
            } else {
                // line 255
                echo "                            ";
                if ($this->getAttribute($context["content"], "content", array())) {
                    // line 256
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                        // line 257
                        echo "                                    <div class=\"form-group ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\">
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 259
                        echo form_label("Group", "", array("class" => ""));
                        echo "
                                            <select name=\"";
                        // line 260
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1]"), "html", null, true);
                        echo "\" id=\"\"
                                                    class=\"form-control group\" data-parsley-required=\"true\">
                                                ";
                        // line 262
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                        foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                            // line 263
                            echo "                                                    <option value=\"";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "\" ";
                            echo ((($context["val"] == $this->getAttribute($context["data"], "group_1", array()))) ? ("selected") : (""));
                            echo " data-id=\"";
                            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                            echo "</option>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 265
                        echo "                                            </select>
                                        </div>
                                        <div class=\"col-sm-3\">
                                            ";
                        // line 268
                        echo form_label("Product", "", array("class" => ""));
                        echo "
                                            ";
                        // line 269
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title]"), "value" => $this->getAttribute($context["data"], "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 272
                        echo form_label("Weight", "", array("class" => ""));
                        echo "
                                            ";
                        // line 273
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][weight]"), "value" => $this->getAttribute($context["data"], "weight", array()), "class" => "form-control", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-1\">
                                            ";
                        // line 276
                        echo form_label("Order", "", array("class" => ""));
                        echo "
                                            ";
                        // line 277
                        echo form_input(array("name" => ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][order_number]"), "value" => $this->getAttribute($context["data"], "order_number", array()), "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                        echo "
                                        </div>
                                        <div class=\"col-sm-2\">
                                            <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 281
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                            </button>
                                            <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                    data-clone=\".";
                        // line 284
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "-base-block\"><i
                                                        class=\"fa fa-minus\"></i></button>
                                        </div>
                                        <input type=\"hidden\" class=\"title_id\"
                                               name=\"";
                        // line 288
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][title_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"group_id\"
                                               name=\"";
                        // line 290
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][group_1_id]"), "html", null, true);
                        echo "\">
                                        <input type=\"hidden\" class=\"clone-remove\"
                                               name=\"";
                        // line 292
                        echo twig_escape_filter($this->env, ((($this->getAttribute($context["content"], "id", array()) . "[") . $context["key"]) . "][id]"), "html", null, true);
                        echo "\"
                                               value=\"";
                        // line 293
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                        <input type=\"hidden\" class=\"clone-move clone-remove\" data-target=\"#deleteTemp\"
                                               data-name=\"";
                        // line 295
                        echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                        echo "_delete[]\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "id", array()), "html", null, true);
                        echo "\"/>
                                    </div>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 298
                    echo "                            ";
                } else {
                    // line 299
                    echo "                                <div class=\"form-group ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\">
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 301
                    echo form_label("Group", "", array("class" => ""));
                    echo "
                                        <select name=\"";
                    // line 302
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1]"), "html", null, true);
                    echo "\" id=\"\"
                                                class=\"form-control group\" data-parsley-required=\"true\">
                                            ";
                    // line 304
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
                    foreach ($context['_seq'] as $context["index"] => $context["val"]) {
                        // line 305
                        echo "                                                <option value=\"";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\" ";
                        echo ((($context["val"] == $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "group_1", array()))) ? ("selected") : (""));
                        echo " data-id=\"";
                        echo twig_escape_filter($this->env, $context["index"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['index'], $context['val'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 307
                    echo "                                        </select>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        ";
                    // line 310
                    echo form_label("Product", "", array("class" => ""));
                    echo "
                                        ";
                    // line 311
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][title]"), "value" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 314
                    echo form_label("Weight", "", array("class" => ""));
                    echo "
                                        ";
                    // line 315
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][weight]"), "value" => null, "class" => "form-control", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-1\">
                                        ";
                    // line 318
                    echo form_label("Order", "", array("class" => ""));
                    echo "
                                        ";
                    // line 319
                    echo form_input(array("name" => ($this->getAttribute($context["content"], "id", array()) . "[0][order_number]"), "value" => null, "class" => "form-control p-l-r-10", "data-parsley-required" => "true"));
                    echo "
                                    </div>
                                    <div class=\"col-sm-2\">
                                        <button class=\"btn btn-default btn-clone clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 323
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i class=\"fa fa-plus\"></i>
                                        </button>
                                        <button class=\"btn btn-default remove-clone-btn m-t-25\"
                                                data-clone=\".";
                    // line 326
                    echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "id", array()), "html", null, true);
                    echo "-base-block\"><i
                                                    class=\"fa fa-minus\"></i></button>
                                    </div>
                                    <input type=\"hidden\" class=\"title_id\"
                                           name=\"";
                    // line 330
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][title_id]"), "html", null, true);
                    echo "\">
                                    <input type=\"hidden\" class=\"group_id\"
                                           name=\"";
                    // line 332
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["content"], "id", array()) . "[0][group_1_id]"), "html", null, true);
                    echo "\">
                                </div>
                            ";
                }
                // line 335
                echo "                        ";
            }
            // line 336
            echo "                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 338
        echo "            </div>
        </div>
    </div>

    <div id=\"deleteTemp\"></div>

    ";
        // line 344
        echo form_close();
        echo "
    <script type=\"application/javascript\">
        \$(document).ready(function () {
            \$('button[type=\"submit\"]').click(function (e) {
                e.preventDefault();
                var titleInput = \$('.title'),
                        groupInput = \$('.group');
                \$(titleInput).each(function () {
                    var titleIDInput = \$(this).closest('.form-group').find('.title_id'),
                            titleID = \$(this).find('option:selected').data('id');
                    titleIDInput.val(titleID);
                });
                \$(groupInput).each(function () {
                    var groupIDInput = \$(this).closest('.form-group').find('.group_id'),
                            groupID = \$(this).find('option:selected').data('id');
                    groupIDInput.val(groupID);
                });
                \$('#formCCPerfectStore').submit();
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "ami/perfect_store/cc_templates.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1011 => 344,  1003 => 338,  988 => 336,  985 => 335,  979 => 332,  974 => 330,  967 => 326,  961 => 323,  954 => 319,  950 => 318,  944 => 315,  940 => 314,  934 => 311,  930 => 310,  925 => 307,  910 => 305,  906 => 304,  901 => 302,  897 => 301,  891 => 299,  888 => 298,  877 => 295,  872 => 293,  868 => 292,  863 => 290,  858 => 288,  851 => 284,  845 => 281,  838 => 277,  834 => 276,  828 => 273,  824 => 272,  818 => 269,  814 => 268,  809 => 265,  794 => 263,  790 => 262,  785 => 260,  781 => 259,  775 => 257,  770 => 256,  767 => 255,  764 => 254,  758 => 251,  753 => 249,  746 => 245,  740 => 242,  733 => 238,  729 => 237,  723 => 234,  719 => 233,  714 => 230,  702 => 228,  697 => 227,  693 => 226,  688 => 224,  684 => 223,  678 => 220,  674 => 219,  669 => 216,  656 => 214,  652 => 213,  647 => 211,  643 => 210,  637 => 208,  634 => 207,  623 => 204,  618 => 202,  614 => 201,  609 => 199,  604 => 197,  597 => 193,  591 => 190,  584 => 186,  580 => 185,  574 => 182,  570 => 181,  565 => 178,  553 => 176,  548 => 175,  544 => 174,  539 => 172,  535 => 171,  529 => 168,  525 => 167,  520 => 164,  505 => 162,  501 => 161,  496 => 159,  492 => 158,  486 => 156,  481 => 155,  478 => 154,  475 => 153,  469 => 150,  464 => 148,  457 => 144,  451 => 141,  444 => 137,  440 => 136,  434 => 133,  430 => 132,  425 => 129,  415 => 127,  410 => 126,  406 => 125,  401 => 123,  397 => 122,  392 => 119,  381 => 117,  377 => 116,  372 => 114,  368 => 113,  363 => 110,  350 => 108,  346 => 107,  341 => 105,  337 => 104,  331 => 102,  328 => 101,  319 => 98,  314 => 96,  310 => 95,  305 => 93,  300 => 91,  293 => 87,  287 => 84,  280 => 80,  276 => 79,  270 => 76,  266 => 75,  261 => 72,  249 => 70,  244 => 69,  240 => 68,  235 => 66,  231 => 65,  226 => 62,  213 => 60,  209 => 59,  204 => 57,  200 => 56,  195 => 53,  180 => 51,  176 => 50,  171 => 48,  167 => 47,  161 => 45,  156 => 44,  153 => 43,  151 => 42,  143 => 40,  126 => 39,  121 => 36,  104 => 33,  97 => 32,  80 => 31,  68 => 21,  66 => 20,  56 => 13,  53 => 12,  50 => 11,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
