<?php

/* ami/global_ssd/newMappingTable.html.twig */
class __TwigTemplate_6200e70b798880149c788ac85f1f8c92ed9a73c3f936b0fda940fce8ecf82a96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/newMappingTable.html.twig", 6);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_js($context, array $blocks = array())
    {
        // line 9
        echo "\t";
        $this->displayParentBlock("js", $context, $blocks);
        echo " 
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/functions.js"), "html", null, true);
        echo "\"></script>
    <script>\$(document).ready(MAPPING_TABLE_MODIFY());</script>
";
    }

    // line 16
    public function block_css($context, array $blocks = array())
    {
        // line 17
        echo "\t";
        $this->displayParentBlock("css", $context, $blocks);
        echo "

    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">

\t<style>
        #mainContent {
            margin-top: 20px;
        }
\t</style>
";
    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "\t<input type=\"hidden\" id=\"rawMapJson\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["map"]) ? $context["map"] : null), "rawvalue", array(), "array"), "html", null, true);
        echo "\">
\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1>
\t\t\t\t\t";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "
\t\t\t\t\t<small>
\t\t\t\t\t\t<span
\t\t\t\t\t\t\tdata-ufsid=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entityRef"]) ? $context["entityRef"] : null), "dbData", array(), "array"), "ufs_id", array(), "array"), "html", null, true);
        echo "\"
\t\t\t\t\t\t\tid=\"distributor\"
\t\t\t\t\t\t\tclass=\"highlight\"
\t\t\t\t\t\t>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entityRef"]) ? $context["entityRef"] : null), "dbData", array(), "array"), "name", array(), "array"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["mapType"]) ? $context["mapType"] : null), "html", null, true);
        echo "</span>

\t\t\t\t\t\t";
        // line 42
        echo (($this->getAttribute((isset($context["map"]) ? $context["map"] : null), "is_draft", array(), "array")) ? ("(Draft)") : (""));
        echo "
\t\t\t\t\t</small>
\t\t\t\t</h1>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div id=\"mainContent\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
                        ";
        // line 55
        if (($this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "flash_type", array()) == "save_true")) {
            // line 56
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-success\">
\t\t\t\t\t\t\t\t<strong>Success!</strong> ";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close\"><i class=\"fa fa-close\"></i></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        } elseif (($this->getAttribute(        // line 60
(isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "flash_type", array()) == "save_false")) {
            // line 61
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-danger\">
\t\t\t\t\t\t\t\t<strong>Warning!</strong> ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "messages", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close\"><i class=\"fa fa-close\"></i></a>
\t\t\t\t\t\t\t</div>
                        ";
        }
        // line 66
        echo "\t\t\t\t\t\t<p id=\"selectCount\"></p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group pull-right\">
\t\t\t\t\t\t\t";
        // line 74
        echo "\t\t\t\t\t\t\t<button title=\"Delete selected rows\" id=\"bulk_delete_row\" disabled=\"disabled\" class=\"btn-standard btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i> Delete Selected</button>
\t\t\t\t\t\t\t<button title=\"Add a new row\" id=\"add_row\" class=\"btn-standard btn btn-xs btn-primary\"><i class=\"fa fa-plus\"></i> Add Row</button>
\t\t\t\t\t\t\t<button title=\"Save and finalize changes\" id=\"save_row\" class=\"btn-standard btn btn-xs btn-success\"><i class=\"fa fa-save\"></i> Save</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12 scroll-x scroll-y div-edit-mapping\">
\t\t\t\t\t\t<form id=\"form_doAddNewMap\" method=\"POST\" action=\"";
        // line 83
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/doAddNewMap"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"map_id\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["map"]) ? $context["map"] : null), "id", array(), "array"), "html", null, true);
        echo "\" >
\t\t\t\t\t\t\t<input type=\"hidden\" id=\"is_draft\" name=\"is_draft\" value=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["map"]) ? $context["map"] : null), "is_draft", array(), "array"), "html", null, true);
        echo "\" >
\t\t\t\t\t\t\t<table id=\"tbl_map\" class=\"table-ssd table table-condensed table-bordered table-striped\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th class=\"in-chkbx more no-input\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"masterCheck\">
\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t";
        // line 92
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
            // line 93
            echo "\t\t\t\t\t\t\t\t\t\t\t<th>";
            echo twig_escape_filter($this->env, $context["col"], "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "\t\t\t\t\t\t\t\t\t\t<th class=\"more no-input min-width\">ACTION</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t";
        // line 99
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["map"]) ? $context["map"] : null), "value", array(), "array"));
        foreach ($context['_seq'] as $context["key"] => $context["val"]) {
            // line 100
            echo "\t\t\t\t\t\t\t\t\t\t<tr class=\"datarow\">
\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"input-chckbx\" value=\"";
            // line 102
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 104
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                // line 105
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"dyna-inpt\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control no-show existing\" type=\"text\" name=\"";
                // line 106
                echo twig_escape_filter($this->env, twig_lower_filter($this->env, $context["col"]), "html", null, true);
                echo "[]\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["val"], $context["col"], array(), "array"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"def-val\">";
                // line 107
                echo $this->getAttribute($context["val"], $context["col"], array(), "array");
                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\" edit-row btn btn-warning btn-xs\" title=\"Edit row\"><i class=\"fa fa-pencil\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\" del-row btn btn-danger btn-xs\" title=\"Delete row\"><i class=\"fa fa-trash\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\" saveEdit no-show btn btn-success btn-xs\" title=\"Save edit\"><i class=\"fa fa-save\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "\t\t\t\t\t\t\t\t\t";
        // line 118
        echo "\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"modal bs-modal-md\" id=\"addColumnModal\" tabindex=\"-1\" role=\"dialog\">
\t    <div class=\"modal-dialog modal-md\" role=\"document\">
\t        <div class=\"modal-content\">
\t            <div class=\"modal-header\">
\t                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t                <h4 class=\"modal-title\">Ádd new column</h4>
\t            </div>
\t            <div class=\"modal-body\">
\t            \t<div class=\"row\">
\t            \t\t<div class=\"col-md-6\">
\t\t\t                <ul class=\"list-group\">
\t\t\t\t\t\t\t\t";
        // line 138
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
            // line 139
            echo "\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\">
\t\t\t\t\t\t\t\t\t\t";
            // line 140
            echo twig_escape_filter($this->env, $context["col"], "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-href=\"";
            // line 141
            echo twig_escape_filter($this->env, site_url("ami/global_ssd/deleteCol?="), "html", null, true);
            echo "\" class=\"close\"><i class=\"fa fa-trash\"></i></a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "\t\t\t                </ul>
\t\t\t            </div>
\t            \t\t<div class=\"col-md-6\">
\t            \t\t\t<h4><span class=\"highlight\">";
        // line 147
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["mapColumnsDefault"]) ? $context["mapColumnsDefault"] : null)), "html", null, true);
        echo "</span> columns in total.</h4>
\t            \t\t\t<hr/>
\t            \t\t\t<div class=\"form-group\">
\t            \t\t\t\t<label for=\"newColumn\">Column name</label>
\t            \t\t\t\t<input placeholder=\"Add new column name. No duplicates allowed\" type=\"text\" name=\"newColumn\" class=\"form-control\" required=\"required\">
\t            \t\t\t</div>
\t            \t\t</div>
\t            \t</div>
\t            </div>
\t            <div class=\"modal-footer\">
\t            \t<button class=\"btn btn-primary\" type=\"button\">Save</button>
\t            </div>
\t        </div><!-- /.modal-content -->
\t    </div><!-- /.modal-dialog -->
\t</div><!-- /.modal -->
";
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/newMappingTable.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  291 => 147,  286 => 144,  277 => 141,  273 => 140,  270 => 139,  266 => 138,  244 => 118,  242 => 117,  230 => 110,  221 => 107,  215 => 106,  212 => 105,  208 => 104,  203 => 102,  199 => 100,  195 => 99,  189 => 95,  180 => 93,  176 => 92,  166 => 85,  162 => 84,  158 => 83,  147 => 74,  141 => 66,  134 => 62,  131 => 61,  129 => 60,  123 => 57,  120 => 56,  118 => 55,  102 => 42,  95 => 40,  89 => 37,  83 => 34,  74 => 29,  71 => 28,  59 => 19,  53 => 17,  50 => 16,  43 => 12,  38 => 10,  33 => 9,  30 => 8,  11 => 6,);
    }
}
