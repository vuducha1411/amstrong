<?php

/* ami/objective_records/index.html.twig */
class __TwigTemplate_6e919eefad74f30f5212991c87608a4ef9fe1155ed01872ae7eecf4d90f63f9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/objective_records/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 14
        echo twig_escape_filter($this->env, site_url("ami/objective_records/ajaxData"), "html", null, true);
        echo "\",*/
//                \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
//                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
            function updateOrder()
            {
                \$.ajax({
                    url: \"";
        // line 35
        echo twig_escape_filter($this->env, site_url("ami/objective_records/update/"), "html", null, true);
        echo "/\",
                    type: 'POST',
                    beforeSend: function () {
                        \$('.loaddingicon').show();
                    },
                    complete: function () {
                        \$('.loaddingicon').hide();
                    },
                    dataType: 'json',
                    data: \$('.sortquestion input[type=\"checkbox\"]'),
                    success: function (json)
                    {

                    }
                });
            }
            \$('.saveaction').click(function(){
                updateOrder();
                return false;
            });
            \$(\".sortquestion\").sortable({
                update: function(event, ui) {
                  //  updateOrder();
                    \$('.saveaction').removeClass('hide');
                }
            }).disableSelection();
        });
    </script>
";
    }

    // line 65
    public function block_css($context, array $blocks = array())
    {
        // line 66
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "
    ";
        // line 72
        echo form_open(site_url("ami/objective_records/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 77
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Objective Records Draft") : ("Objective Records"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar m-r\">
                        <button type=\"button\" class=\"btn btn-sm btn-success saveaction\">
                            <i class=\"fa fw fa-floppy-o\"></i> Save
                        </button>
                    </span>
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 86
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 88
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/objective_records/index.html.twig", 88)->display(array_merge($context, array("url" => "ami/objective_records", "icon" => "fa-user", "title" => "Objective Records", "permission" => "objective_records", "showImport" => 0)));
        // line 89
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 99
        echo ((((isset($context["active"]) ? $context["active"] : null) == 1)) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                            href=\"";
        // line 100
        echo twig_escape_filter($this->env, site_url("ami/objective_records?active=1"), "html", null, true);
        echo "\">Active</a>
                </li>
                <li role=\"presentation\" ";
        // line 102
        echo ((((isset($context["active"]) ? $context["active"] : null) == 0)) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                              href=\"";
        // line 103
        echo twig_escape_filter($this->env, site_url("ami/objective_records?active=0"), "html", null, true);
        echo "\">Inactive</a>
                </li>
            </ul>
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 111
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 112
            echo "                                <div class=\"row table-filter\">
                                    <div class=\"col-md-12\">
                                        <div class=\"dropdown\">
                                            <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                                    id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                                Filter: ";
            // line 117
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                                        class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                                <li role=\"presentation\" ";
            // line 121
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                                            role=\"menuitem\" tabindex=\"-1\"
                                                            href=\"";
            // line 123
            echo twig_escape_filter($this->env, site_url("ami/objective_records"), "html", null, true);
            echo "\">All</a></li>
                                                <li role=\"presentation\" ";
            // line 124
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "country")) ? ("class=\"active\"") : (""));
            echo ">
                                                    <a role=\"menuitem\" tabindex=\"-1\"
                                                       href=\"";
            // line 126
            echo twig_escape_filter($this->env, site_url("ami/objective_records?filter=country"), "html", null, true);
            echo "\">Country
                                                        Objective</a>
                                                </li>
                                                <li role=\"presentation\" ";
            // line 129
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "channel")) ? ("class=\"active\"") : (""));
            echo ">
                                                    <a role=\"menuitem\" tabindex=\"-1\"
                                                       href=\"";
            // line 131
            echo twig_escape_filter($this->env, site_url("ami/objective_records?filter=channel"), "html", null, true);
            echo "\">
                                                       Channel Objective</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ";
        }
        // line 139
        echo "                            <thead>
                            <tr>
                                ";
        // line 141
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "objective_records"))) {
            // line 142
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 146
        echo "                                <th class=\"nosort\">ID</th>
                                <th class=\"nosort\">Description</th>
                                <th class=\"nosort\">Type</th>
                                <th class=\"nosort\">App type</th>
                                <th class=\"nosort\">Datetime Start</th>
                                <th class=\"nosort\">Datetime End</th>
                                <th class=\"nosort text-center\">
                                    <img width=\"30px\" style=\"display: none; \" class=\"loaddingicon\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, site_url("res/img"), "html", null, true);
        echo "/loading.gif\">
                                </th>
                            </tr>
                            </thead>
                            <tbody class=\"sortquestion\">
                            ";
        // line 158
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 159
            echo "                                <tr>
                                    ";
            // line 160
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "objective_records"))) {
                // line 161
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "armstrong_2_objective_records_id", array())));
                echo "</td>
                                    ";
            }
            // line 163
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "armstrong_2_objective_records_id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 164
            echo wordTrim($this->getAttribute($context["post"], "description", array()), 120);
            echo "</td>
                                    <td class=\"center\">";
            // line 165
            if (($this->getAttribute($context["post"], "type", array()) == 0)) {
                echo " Country
                                        ";
            } elseif (($this->getAttribute(            // line 166
$context["post"], "type", array()) == 1)) {
                echo "    Channel
                                        ";
            } elseif (($this->getAttribute(            // line 167
$context["post"], "type", array()) == 2)) {
                echo "    Person
                                        ";
            } else {
                // line 168
                echo "  Business
                                        ";
            }
            // line 170
            echo "                                    </td>
\t\t\t\t\t\t\t\t\t<td class=\"center\">";
            // line 171
            if (($this->getAttribute($context["post"], "app_type", array()) == 0)) {
                echo " Pull
                                        ";
            } elseif (($this->getAttribute(            // line 172
$context["post"], "app_type", array()) == 1)) {
                echo " Push
                                        ";
            }
            // line 174
            echo "                                    </td>
                                    ";
            // line 176
            echo "                                        ";
            // line 177
            echo "                                        ";
            // line 178
            echo "                                    ";
            // line 179
            echo "                                    <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "datetime_start", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 180
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "datetime_end", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 184
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "objective_records"))) {
                // line 185
                echo "                                                ";
                echo html_btn(site_url(((("ami/objective_records/edit/" . $this->getAttribute($context["post"], "armstrong_2_objective_records_id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                                            ";
            }
            // line 187
            echo "                                            ";
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "objective_records")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 188
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 189
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/objective_records/restore/" . $this->getAttribute($context["post"], "armstrong_2_objective_records_id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 191
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/objective_records/delete/" . $this->getAttribute($context["post"], "armstrong_2_objective_records_id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 193
                echo "                                            ";
            }
            // line 194
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 198
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 202
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 203
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/objective_records/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/objective_records/draft")));
            // line 204
            echo "                                ";
        } else {
            // line 205
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/objective_records/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/objective_records")));
            // line 206
            echo "                                ";
        }
        // line 207
        echo "                                ";
        // line 208
        echo "                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 221
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/objective_records/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  423 => 221,  408 => 208,  406 => 207,  403 => 206,  400 => 205,  397 => 204,  394 => 203,  392 => 202,  386 => 198,  377 => 194,  374 => 193,  368 => 191,  362 => 189,  359 => 188,  356 => 187,  350 => 185,  348 => 184,  341 => 180,  336 => 179,  334 => 178,  332 => 177,  330 => 176,  327 => 174,  322 => 172,  318 => 171,  315 => 170,  311 => 168,  306 => 167,  302 => 166,  298 => 165,  294 => 164,  289 => 163,  283 => 161,  281 => 160,  278 => 159,  274 => 158,  266 => 153,  257 => 146,  251 => 142,  249 => 141,  245 => 139,  234 => 131,  229 => 129,  223 => 126,  218 => 124,  214 => 123,  209 => 121,  202 => 117,  195 => 112,  193 => 111,  182 => 103,  178 => 102,  173 => 100,  169 => 99,  157 => 89,  155 => 88,  150 => 86,  138 => 77,  130 => 72,  127 => 71,  124 => 70,  118 => 67,  113 => 66,  110 => 65,  77 => 35,  53 => 14,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
