<?php

/* ami/setting_module_fields/modules.html.twig */
class __TwigTemplate_63d60b8b7d571b8ea15e1aee47d6904c82e76c9944d8cbaa2fc6f2325110fae1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_module_fields/modules.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                'order': [[0, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 24
    public function block_css($context, array $blocks = array())
    {
        // line 25
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 29
    public function block_content($context, array $blocks = array())
    {
        // line 30
        echo "
    ";
        // line 31
        echo form_open(site_url("ami/setting_module_fields/modules_process"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 36
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Modules Draft") : ("Modules"));
        echo "</h1>
                ";
        // line 38
        echo "                ";
        // line 39
        echo "                ";
        // line 40
        echo "                ";
        // line 41
        echo "                ";
        // line 42
        echo "                ";
        // line 43
        echo "                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 56
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 57
            echo "                                ";
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null)))) {
                // line 58
                echo "                                    <div class=\"row table-filter\">
                                        <div class=\"col-md-12\">
                                            <div class=\"col-sm-8\" style=\"padding-left:0px\">
                                                ";
                // line 61
                echo form_input(array("name" => "name", "value" => "", "class" => "form-control input-sm"));
                echo "
                                            </div>
                                            <div class=\"col-sm-2\">
                                                ";
                // line 64
                echo form_button(array("type" => "submit", "content" => "<i class=\"fa fw fa-plus\"></i> Add", "class" => "btn btn-success btn-sm"));
                echo "
                                            </div>
                                        </div>
                                    </div>
                                ";
            }
            // line 69
            echo "                            ";
        }
        // line 70
        echo "                            <thead>
                            <tr>
                                ";
        // line 73
        echo "                                ";
        // line 74
        echo "                                ";
        // line 75
        echo "                                ";
        // line 76
        echo "                                ";
        // line 77
        echo "                                <th>ID</th>
                                <th>Title</th>
                                <th>Created</th>
                                <th>Updated</th>
                                ";
        // line 82
        echo "                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 85
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 86
            echo "                                <tr>
                                    ";
            // line 88
            echo "                                    ";
            // line 89
            echo "                                    ";
            // line 90
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "name", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    ";
            // line 95
            echo "                                    ";
            // line 96
            echo "
                                    ";
            // line 98
            echo "                                    ";
            // line 99
            echo "
                                    ";
            // line 101
            echo "
                                    ";
            // line 103
            echo "                                    ";
            // line 104
            echo "                                    ";
            // line 105
            echo "                                    ";
            // line 106
            echo "                                    ";
            // line 107
            echo "                                    ";
            // line 108
            echo "
                                    ";
            // line 110
            echo "                                    ";
            // line 111
            echo "                                    ";
            // line 112
            echo "                                    ";
            // line 113
            echo "                                    ";
            // line 114
            echo "                                    ";
            // line 115
            echo "                                    ";
            // line 116
            echo "                                    ";
            // line 117
            echo "                                    ";
            // line 118
            echo "                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 124
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 125
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/news_feed/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/news_feed/draft")));
            // line 126
            echo "                                ";
        } else {
            // line 127
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/news_feed/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/news_feed")));
            // line 128
            echo "                                ";
        }
        // line 129
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 143
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/setting_module_fields/modules.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 143,  270 => 129,  267 => 128,  264 => 127,  261 => 126,  258 => 125,  256 => 124,  250 => 120,  243 => 118,  241 => 117,  239 => 116,  237 => 115,  235 => 114,  233 => 113,  231 => 112,  229 => 111,  227 => 110,  224 => 108,  222 => 107,  220 => 106,  218 => 105,  216 => 104,  214 => 103,  211 => 101,  208 => 99,  206 => 98,  203 => 96,  201 => 95,  197 => 93,  193 => 92,  189 => 91,  184 => 90,  182 => 89,  180 => 88,  177 => 86,  173 => 85,  168 => 82,  162 => 77,  160 => 76,  158 => 75,  156 => 74,  154 => 73,  150 => 70,  147 => 69,  139 => 64,  133 => 61,  128 => 58,  125 => 57,  123 => 56,  108 => 43,  106 => 42,  104 => 41,  102 => 40,  100 => 39,  98 => 38,  94 => 36,  86 => 31,  83 => 30,  80 => 29,  74 => 26,  69 => 25,  66 => 24,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
