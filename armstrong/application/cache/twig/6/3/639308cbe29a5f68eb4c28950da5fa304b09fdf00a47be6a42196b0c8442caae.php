<?php

/* ami/reports/custom/customer_profiling_transaction.twig.html */
class __TwigTemplate_639308cbe29a5f68eb4c28950da5fa304b09fdf00a47be6a42196b0c8442caae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("ami/reports/custom/filter.twig.html", "ami/reports/custom/customer_profiling_transaction.twig.html", 1)->display(array_merge($context, array("channel" => 0, "otm" => 0, "leaderAndPerson" => 0)));
        // line 6
        echo "
<div class=\"form-group\">
\t";
        // line 8
        echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-3"));
        echo "
\t<div class=\"col-sm-6\">
\t\t<select name=\"sales_leader[]\" class=\"form-control\" id=\"SalesLeaderSelect\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t<option value=\"ALL\" selected=\"selected\">ALL</option>
\t\t\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
            // line 13
            echo "\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-sub_type=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "sub_type", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
            echo "</option>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t\t</select>
\t</div>
</div>

<div class=\"form-group\">
\t";
        // line 20
        echo form_label("Armstrong 2 salespersons id", "salesperson", array("class" => "control-label col-sm-3"));
        echo "
\t<div class=\"col-sm-6\">
\t\t<select name=\"armstrong_2_salespersons_id[]\" class=\"form-control SalespersonsSelect\" id=\"armstrong_2_salespersons_id\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t<option value=\"\" selected=\"selected\" data-manager=\"\">ALL</option>
\t\t\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
            // line 25
            echo "\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-manager=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
            echo "</option>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "\t\t\t<option value=\"unassigned\" data-manager=\"\">Unassigned</option>
\t\t</select>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/customer_profiling_transaction.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 27,  69 => 25,  65 => 24,  58 => 20,  51 => 15,  36 => 13,  32 => 12,  25 => 8,  21 => 6,  19 => 1,);
    }
}
