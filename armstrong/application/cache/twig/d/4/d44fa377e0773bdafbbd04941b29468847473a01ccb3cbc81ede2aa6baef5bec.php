<?php

/* ami/promotional_mechanics/items/promotion_2.html.twig */
class __TwigTemplate_d44fa377e0773bdafbbd04941b29468847473a01ccb3cbc81ede2aa6baef5bec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-panel active\">
    <table class=\"table table-striped table-bordered table-hover\" id=\"promotional_2\">
        <thead>
        <tr>
            ";
        // line 5
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
            // line 6
            echo "                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                      data-target=\"tbody\"
                                                      data-description=\"#CheckAllBtn\"></th>
            ";
        }
        // line 10
        echo "            <th>Promotion Description</th>
            <th>Comparison Period</th>
            <th>Grow percentage Start</th>
            <th>Grow percentage End</th>
            <th>Promotion applicable</th>
            <th>Promotion Free Gift</th>
            <th>From date</th>
            <th>To date</th>
            <th class=\"nosort text-center\"></th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promotinal"]) {
            // line 23
            echo "            <tr>
                ";
            // line 24
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
                // line 25
                echo "                    <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["promotinal"], "id", array())));
                echo "</td>
                ";
            }
            // line 27
            echo "                <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "sub_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "comparison_period", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "percentage_start_range", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "percentage_end_range", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "free_sku_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "promotion_free_gift", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "from_date", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "to_date", array()), "html", null, true);
            echo "</td>
                <td class=\"center text-center\" style=\"min-width: 80px;\">
                    ";
            // line 36
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotional_mechanics")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics")))) {
                // line 37
                echo "                        <div class=\"btn-group\">
                            ";
                // line 38
                echo html_btn(site_url(((("ami/promotional_mechanics/edit/" . $this->getAttribute($context["promotinal"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                            ";
                // line 39
                echo html_btn(site_url(("ami/promotional_mechanics/delete/" . $this->getAttribute($context["promotinal"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                        </div>
                    ";
            }
            // line 42
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotinal'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/items/promotion_2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 45,  111 => 42,  105 => 39,  101 => 38,  98 => 37,  96 => 36,  91 => 34,  87 => 33,  83 => 32,  79 => 31,  75 => 30,  71 => 29,  67 => 28,  62 => 27,  56 => 25,  54 => 24,  51 => 23,  47 => 22,  33 => 10,  27 => 6,  25 => 5,  19 => 1,);
    }
}
