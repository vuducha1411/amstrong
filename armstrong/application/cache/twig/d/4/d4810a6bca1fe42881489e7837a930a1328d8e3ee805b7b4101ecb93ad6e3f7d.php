<?php

/* ami/competitor_products_country/edit.html.twig */
class __TwigTemplate_d4810a6bca1fe42881489e7837a930a1328d8e3ee805b7b4101ecb93ad6e3f7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/competitor_products_country/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/jquery-number/jquery.number.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function initProductAutocomplete(destroy) {
            var products_cat_web_id = '?products_cat_web_id='+\$('#cat_web').val();
            \$('.product-tokenfield').autocomplete({
                name: 'Enter to search',
                source: \"";
        // line 11
        echo twig_escape_filter($this->env, site_url("ami/products/tokenfield"), "html", null, true);
        echo "\"+products_cat_web_id,
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    var \$this = \$(this),
                            \$base = \$this.closest('.sku-base'),
                            \$input = \$base.find('.InputProductId');
                    \$this.val(ui.item.label);
                    \$input.val(ui.item.value);
                }
            }).on('focus', function () {
                this.select();
            }).attr('placeholder', 'Enter to search');
        }
        \$(document).on('change', '#cat_web', function (e) {
            initProductAutocomplete();
        });
        \$(document).on('change', '#brand_id', function (e) {
            \$('input[name=\"brand\"]').val(\$('#brand_id option:selected').html());
        });
        \$(function () {
            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        \$clone = \$(this).closest('.sku-base').clone(),
                        nextCounter = \$('.product-tokenfield').length;
                \$clone.find('.hide').removeClass('hide').show();
                if (\$this.hasClass('clone-add')) {
                    \$new = \$clone.clone();

                    \$new.find('input[name], select[name]').each(function () {
                        var \$this = \$(this);
                        \$this.attr('name', \$this.attr('name').replace(/\\[(\\d+)\\]/, '[' + nextCounter + ']'));

                        if (\$this.is('input:text')) {
                            \$this.val('');
                        }
                        else {
                            \$this.val(0);
                        }
                    });
                    \$new.appendTo(\$('#sku-base-block'));
                }
                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.sku-base').remove();
                }
                initProductAutocomplete();
            });
            initProductAutocomplete();
        });
    </script>
";
    }

    // line 65
    public function block_content($context, array $blocks = array())
    {
        // line 66
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 81
        echo form_open(site_url("ami/competitor_products_country/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Competitor Products Master List</h1>

                <div class=\"text-right\">
                    ";
        // line 89
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/competitor_products_country/edit.html.twig", 89)->display(array_merge($context, array("url" => "ami/competitor_products_country", "id" => $this->getAttribute((isset($context["ppostt"]) ? $context["ppostt"] : null), "id", array()), "permission" => "competitor_products")));
        // line 90
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 99
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 101
        echo form_radio("active", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "active", array()) == 1));
        echo "
                        Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 103
        echo form_radio("active", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "active", array()) == 0));
        echo "
                        No</label>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 108
        echo form_label("App Type", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 110
        echo form_dropdown("app_type", (isset($context["app_list"]) ? $context["app_list"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()), "class=\"form-control fetch-sl\" id=\"input-app-type\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 114
        echo form_label("Brands", "brand_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 116
        echo form_dropdown("brand_id", (isset($context["brands"]) ? $context["brands"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "brand_id", array()), "class=\"form-control fetch-sl\" id=\"brand_id\"");
        echo "
                </div>
                <input type=\"hidden\" value=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "brand", array()), "html", null, true);
        echo "\" name=\"brand\">
            </div>
            <div class=\"form-group\">
                ";
        // line 121
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 123
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "name", array()), "class" => "form-control"));
        echo "
                    ";
        // line 125
        echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 128
        echo form_label("Competitor Products Name", "product_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 130
        echo form_input(array("name" => "product_name", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "product_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 134
        echo form_label("Quantity", "quantity", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 136
        echo form_input(array("name" => "quantity", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "quantity", array()), "class" => "form-control", "data-parsley-required" => "false"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 140
        echo form_label("Price", "price", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 142
        echo form_input(array("name" => "price", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "price", array()), "class" => "form-control", "data-parsley-required" => "false"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 146
        echo form_label("Weight Per PC", "weight_per_pc", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 148
        echo form_input(array("name" => "weight_per_pc", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "weight_per_pc", array()), "class" => "form-control", "data-parsley-required" => "false"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 152
        echo form_label("Product Cat Web", "product_cat_web_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 154
        echo form_dropdown("product_cat_web_id", (isset($context["cat_web"]) ? $context["cat_web"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "product_cat_web_id", array()), "id=\"cat_web\" class=\"form-control\"");
        echo "
                </div>
            </div>
            ";
        // line 165
        echo "            ";
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 166
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "products_array", array()));
        foreach ($context['_seq'] as $context["key"] => $context["product"]) {
            // line 167
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 168
            echo form_label("Ufs Skus", "ufs_skus", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 170
            echo form_input(array("name" => "sku_number_", "value" => get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null)), "class" => "form-control product-tokenfield"));
            // line 172
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 173
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"";
            // line 174
            echo twig_escape_filter($this->env, $context["product"], "html", null, true);
            echo "\"
                               data-price=\"";
            // line 175
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "price", array()), "html", null, true);
            echo "\"
                               data-pcsprice=\"";
            // line 176
            echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "price", array()) / $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "quantity_case", array())), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add hide\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 184
        echo "            ";
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 185
            echo "                ";
            $context["counter"] = twig_length_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "products_array", array()));
            // line 186
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 187
            echo form_label("Ufs Skus", "ufs_skus", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 189
            echo form_input(array("name" => "sku_number_", "value" => null, "class" => "form-control product-tokenfield"));
            // line 191
            echo "
                        <input placeholder=\"Enter to search\" type=\"hidden\" name=\"products[";
            // line 192
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"\"
                               data-price=\"\"
                               data-pcsprice=\"\" />
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
                <div id=\"sku-base-block\"></div>
            ";
        }
        // line 204
        echo "
            ";
        // line 205
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 209
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/competitor_products_country/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 209,  349 => 205,  346 => 204,  331 => 192,  328 => 191,  326 => 189,  321 => 187,  318 => 186,  315 => 185,  312 => 184,  298 => 176,  294 => 175,  290 => 174,  286 => 173,  283 => 172,  281 => 170,  276 => 168,  273 => 167,  268 => 166,  265 => 165,  259 => 154,  254 => 152,  247 => 148,  242 => 146,  235 => 142,  230 => 140,  223 => 136,  218 => 134,  211 => 130,  206 => 128,  201 => 125,  197 => 123,  192 => 121,  186 => 118,  181 => 116,  176 => 114,  169 => 110,  164 => 108,  156 => 103,  151 => 101,  146 => 99,  135 => 90,  133 => 89,  122 => 81,  105 => 66,  102 => 65,  46 => 11,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
