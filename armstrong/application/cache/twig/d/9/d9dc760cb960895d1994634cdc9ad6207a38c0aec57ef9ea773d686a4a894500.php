<?php

/* ami/module_field_countries/index.html.twig */
class __TwigTemplate_d9dc760cb960895d1994634cdc9ad6207a38c0aec57ef9ea773d686a4a894500 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/module_field_countries/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/module_field_countries/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'asc'],[5,'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 36
    public function block_css($context, array $blocks = array())
    {
        // line 37
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 41
    public function block_content($context, array $blocks = array())
    {
        // line 42
        echo "
    ";
        // line 43
        echo form_open(site_url("ami/module_field_countries/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 48
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Module Field Countries Draft") : ("Module Field Countries"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 52
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 54
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/module_field_countries/index.html.twig", 54)->display(array_merge($context, array("url" => "ami/module_field_countries", "icon" => "fa-user", "title" => "Module Field Countries", "permission" => "module_field_countries", "showImport" => 0)));
        // line 55
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 69
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 70
            echo "                                <div class=\"row table-filter\">
                                    <div class=\"col-md-12\">
                                        <div class=\"dropdown\">
                                            <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                                    id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                                Filter: ";
            // line 75
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                                        class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                                <li role=\"presentation\" ";
            // line 79
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                                            role=\"menuitem\" tabindex=\"-1\"
                                                            href=\"";
            // line 81
            echo twig_escape_filter($this->env, site_url("ami/module_field_countries"), "html", null, true);
            echo "\">All</a>
                                                </li>
                                                ";
            // line 83
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 84
                echo "                                                    <li role=\"presentation\" ";
                echo ((((isset($context["filter"]) ? $context["filter"] : null) == $this->getAttribute($context["module"], "name", array()))) ? ("class=\"active\"") : (""));
                echo ">
                                                        <a role=\"menuitem\" tabindex=\"-1\"
                                                           href=\"";
                // line 86
                echo twig_escape_filter($this->env, site_url(("ami/module_field_countries?filter=" . $this->getAttribute($context["module"], "name", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["module"], "name", array()), "html", null, true);
                echo "</a>
                                                    </li>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ";
        }
        // line 94
        echo "                            <thead>
                            <tr>
                                ";
        // line 96
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "module_field_countries"))) {
            // line 97
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 101
        echo "                                <th>ID</th>
                                <th>Title</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Section</th>
                                <th>Row</th>
                                <th>Required</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 115
            echo "                                <tr>
                                    ";
            // line 116
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "module_field_countries"))) {
                // line 117
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 119
            echo "                                    <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "field", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">
                                        ";
            // line 123
            if (($this->getAttribute($context["post"], "type", array()) == 1)) {
                // line 124
                echo "                                            View
                                        ";
            } elseif (($this->getAttribute(            // line 125
$context["post"], "type", array()) == 2)) {
                // line 126
                echo "                                            Input
                                        ";
            } elseif (($this->getAttribute(            // line 127
$context["post"], "type", array()) == 3)) {
                // line 128
                echo "                                            Dropdown List
                                        ";
            } else {
                // line 130
                echo "                                            Click Button
                                        ";
            }
            // line 132
            echo "                                    </td>
                                    <td class=\"center\">";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "section", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "row", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 135
            echo ((($this->getAttribute($context["post"], "required", array()) == 1)) ? ("Yes") : ("No"));
            echo "</td>
                                    <td class=\"center\">";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 141
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "module_field_countries"))) {
                // line 142
                echo "                                                ";
                echo html_btn(site_url(((("ami/module_field_countries/edit/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "

                                            ";
            }
            // line 145
            echo "
                                            ";
            // line 146
            if (((isset($context["filter"]) ? $context["filter"] : null) == "pending")) {
                // line 147
                echo "                                                ";
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
                    // line 148
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_countries/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                    // line 149
                    echo html_btn(site_url(("ami/module_field_countries/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 151
                echo "                                            ";
            }
            // line 152
            echo "
                                            ";
            // line 153
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "module_field_countries")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 154
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 155
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_countries/restore/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 157
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_countries/delete/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 159
                echo "                                            ";
            }
            // line 160
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 168
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 169
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/module_field_countries/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/module_field_countries/draft")));
            // line 170
            echo "                                ";
        } else {
            // line 171
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/module_field_countries/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/module_field_countries")));
            // line 172
            echo "                                ";
        }
        // line 173
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 187
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/module_field_countries/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  395 => 187,  377 => 173,  374 => 172,  371 => 171,  368 => 170,  365 => 169,  363 => 168,  357 => 164,  348 => 160,  345 => 159,  339 => 157,  333 => 155,  330 => 154,  328 => 153,  325 => 152,  322 => 151,  317 => 149,  312 => 148,  309 => 147,  307 => 146,  304 => 145,  297 => 142,  295 => 141,  288 => 137,  284 => 136,  280 => 135,  276 => 134,  272 => 133,  269 => 132,  265 => 130,  261 => 128,  259 => 127,  256 => 126,  254 => 125,  251 => 124,  249 => 123,  244 => 121,  240 => 120,  235 => 119,  229 => 117,  227 => 116,  224 => 115,  220 => 114,  205 => 101,  199 => 97,  197 => 96,  193 => 94,  186 => 89,  175 => 86,  169 => 84,  165 => 83,  160 => 81,  155 => 79,  148 => 75,  141 => 70,  139 => 69,  123 => 55,  121 => 54,  116 => 52,  109 => 48,  101 => 43,  98 => 42,  95 => 41,  89 => 38,  84 => 37,  81 => 36,  57 => 15,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
