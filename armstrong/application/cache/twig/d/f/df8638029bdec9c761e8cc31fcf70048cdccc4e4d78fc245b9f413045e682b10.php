<?php

/* ami/contacts/index.html.twig */
class __TwigTemplate_df8638029bdec9c761e8cc31fcf70048cdccc4e4d78fc245b9f413045e682b10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/contacts/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 18
        echo twig_escape_filter($this->env, site_url(("ami/contacts/ajaxData?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData : \"c.id\" },
                    {mData : \"c.armstrong_2_customers_id\" },
                    {mData : \"\$.armstrong_2_customers_name\" },
                    {mData : \"c.first_name\"},
                    {mData : \"c.last_name\"},
                    {mData : \"c.primary_contact\"},
                    {mData : \"c.signature_base64\"},
                    {mData : \"c.date_created\", \"bSearchable\": false},
                    {mData : \"c.last_updated\", \"bSearchable\": false}
                ],
                \"fnRowCallback\": function( nRow, aData, iDisplayIndex ) {
                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"'+ aData['DT_RowId'] +'\"></td>');
                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">'+ aData['buttons'] +'</td>');
                },
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 58
    public function block_css($context, array $blocks = array())
    {
        // line 59
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 60
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 63
    public function block_content($context, array $blocks = array())
    {
        // line 64
        echo "
    ";
        // line 65
        echo form_open(site_url("ami/contacts/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 70
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Contacts Draft") : ("Contacts"));
        echo "</h1>
                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 73
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 76
        echo twig_escape_filter($this->env, site_url("ami/contacts/add"), "html", null, true);
        echo "\"><i class=\"fa fw fa-plus\"></i> Add</a>
                        ";
        // line 77
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 78
            echo "                            <a class=\"btn btn-sm btn-default\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/contacts"), "html", null, true);
            echo "\"><i class=\"fa fw fa-tags\"></i> Contacts</a>
                        ";
        } else {
            // line 80
            echo "                            ";
            // line 81
            echo "                        ";
        }
        // line 82
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 93
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "active")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"  href=\"";
        echo twig_escape_filter($this->env, site_url("ami/contacts?filter=active"), "html", null, true);
        echo "\">Active</a></li>
                <li role=\"presentation\" ";
        // line 94
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"  href=\"";
        echo twig_escape_filter($this->env, site_url("ami/contacts?filter=inactive"), "html", null, true);
        echo "\">Inactive</a></li>
             </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 104
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "contacts"))) {
            // line 105
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 107
        echo "                                    <th> ID</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Primary Contact</th>
                                    <th>Signature</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 132
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/contacts/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 132,  201 => 107,  197 => 105,  195 => 104,  180 => 94,  174 => 93,  161 => 82,  158 => 81,  156 => 80,  150 => 78,  148 => 77,  144 => 76,  138 => 73,  132 => 70,  124 => 65,  121 => 64,  118 => 63,  112 => 60,  108 => 59,  105 => 58,  62 => 18,  50 => 9,  46 => 8,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
