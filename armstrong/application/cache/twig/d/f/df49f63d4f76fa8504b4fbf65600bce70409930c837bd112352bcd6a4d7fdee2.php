<?php

/* ami/promotional_mechanics/items/promotion_2_edit.html.twig */
class __TwigTemplate_df49f63d4f76fa8504b4fbf65600bce70409930c837bd112352bcd6a4d7fdee2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    ";
        // line 2
        echo form_label("Date Range", "range_id", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 4
        echo form_dropdown("range_id", (isset($context["range"]) ? $context["range"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "range_id", array()), "id=\"range_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 8
        echo form_label("Comparison Period", "comparison_period", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 10
        echo form_dropdown("comparison_period", (isset($context["comparison_period"]) ? $context["comparison_period"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "comparison_period", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 14
        echo form_label("Promotion Name", "name", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 16
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 20
        echo form_label("Promotion Description", "sub_name", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 22
        echo form_input(array("name" => "sub_name", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "sub_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 26
        echo form_label("Grow percentage Start", "percentage_start_range", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 28
        echo form_input(array("name" => "percentage_start_range", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "percentage_start_range", array()), "class" => "form-control", "data-parsley-required" => "true", "min" => 1, "data-parsley-type" => "digits"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 32
        echo form_label("Grow percentage End", "percentage_end_range", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 34
        echo form_input(array("name" => "percentage_end_range", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "percentage_end_range", array()), "class" => "form-control", "min" => 1, "data-parsley-type" => "digits"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 38
        echo form_label("Promotion applicable", "free_sku_name", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 40
        echo form_dropdown("free_sku_name", (isset($context["product_applicable"]) ? $context["product_applicable"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "free_sku_name", array()), "id=\"applicable\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 44
        echo form_label("Promotion Free Gift", "promotion_free_gift", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 46
        echo form_input(array("name" => "promotion_free_gift", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "promotion_free_gift", array()), "class" => "form-control"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 50
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6 no-parsley\">
        <label class=\"radio-inline\">";
        // line 52
        echo form_radio("active", 0, ($this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "active", array()) == 0));
        echo " No</label>&nbsp;
        <label class=\"radio-inline\">";
        // line 53
        echo form_radio("active", 1, ($this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "active", array()) == 1));
        echo " Yes</label>&nbsp;
    </div>
</div>

";
        // line 57
        echo form_input(array("name" => "type", "value" => 2, "type" => "hidden", "class" => "hide"));
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/items/promotion_2_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 57,  127 => 53,  123 => 52,  118 => 50,  111 => 46,  106 => 44,  99 => 40,  94 => 38,  87 => 34,  82 => 32,  75 => 28,  70 => 26,  63 => 22,  58 => 20,  51 => 16,  46 => 14,  39 => 10,  34 => 8,  27 => 4,  22 => 2,  19 => 1,);
    }
}
