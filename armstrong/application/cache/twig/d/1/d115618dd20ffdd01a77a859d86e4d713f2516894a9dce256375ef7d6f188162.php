<?php

/* ami/kpi_target_in_sales_cycle/edit1.html.twig */
class __TwigTemplate_d115618dd20ffdd01a77a859d86e4d713f2516894a9dce256375ef7d6f188162 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/kpi_target_in_sales_cycle/edit1.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script type=\"application/javascript\">
        var counter = 0;
        function setClone(\$clone, counter, base){
            \$clone.removeClass('hide clone-step').addClass('step');
            var closestStep = \$clone.closest('.step');
            if(base.hasClass('focused_skus')){
                closestStep.find('.select-product').attr('name', 'focused_skus_target[' + counter + '][sku_number]').attr('data-parsley-type', 'number');
                closestStep.find('.input-amount').attr('name', 'focused_skus_target[' + counter + '][amount]').attr('data-parsley-type', 'digits');
            }else if(base.hasClass('special_tasks')){
                closestStep.find('.input-title').attr('name', 'special_tasks_target[' + counter + '][special_task_title]');
                closestStep.find('.input-amount').attr('name', 'special_tasks_target[' + counter + '][amount]').attr('data-parsley-type', 'digits');
            }
        }

        \$(document).on('click', '.btn-clone', function (e) {
            e.preventDefault();
            var \$this = \$(this),
                    base = \$this.closest('.base'),
                    \$clone = base.find('.clone-step').clone();

            counter = counter > 0 ? counter + 1 : \$('.step').length;

            if (\$this.hasClass('clone-add')) {
                setClone(\$clone, counter, base);
                \$clone.appendTo(base);

            }

            if (\$this.hasClass('clone-remove')) {
                \$(this).closest('.step').remove();
                if(base.find('.step').length == 0){
                    \$clone.appendTo(base);
                    setClone(\$clone, counter, base);
                }
            }
        });

        \$(document).on('change', '.input-amount, .select-product, .input-title', function(){
            var base = \$(this).closest('.base'),
                    stepElement = base.find('.step'),
                    total = 0;

            \$.each(stepElement, function(){
                var \$val = \$(this).find('.input-amount').val(),
                        amount;
                if(base.hasClass('focused_skus')){
                    if(\$(this).find('.select-product').val() == '' || \$(this).find('.select-product').val() == null )
                        \$val = 0;
                }else if(base.hasClass('special_tasks')){
                    if(\$(this).find('.input-title').val() == '' || \$(this).find('.input-title').val() == null )
                        \$val = 0;
                }
                amount = \$val ? parseInt(\$val) : 0;
                total = total + amount;
            });

            if(base.hasClass('focused_skus')){
                \$('#sku_amount').val(total);
            }else if(base.hasClass('special_tasks')){
                \$('#special_task_amount').val(total);
            }
        });
    </script>
";
    }

    // line 68
    public function block_css($context, array $blocks = array())
    {
        // line 69
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style>
        select[multiple] {
            min-height: 200px;
        }
    </style>
";
    }

    // line 76
    public function block_content($context, array $blocks = array())
    {
        // line 77
        echo "    ";
        echo form_open(site_url("ami/kpi_target_in_sales_cycle/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">KPI Target In Sales Cycle</h1>

                <div class=\"text-right\">
                    ";
        // line 85
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/kpi_target_in_sales_cycle/edit1.html.twig", 85)->display(array_merge($context, array("url" => "ami/kpi_target_in_sales_cycle", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "kpi_target_in_sales_cycle")));
        // line 86
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 95
        echo form_label("Sales cycle", "sales_cycle_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 97
        echo form_dropdown("sales_cycle_id", (isset($context["range"]) ? $context["range"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sales_cycle_id", array()), "id=\"range_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 102
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 104
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" id=\"salespersons_dropdown\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 109
        echo form_label("Turnover", "turnover_target", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 111
        echo form_input(array("name" => "turnover_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "turnover_target", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 116
        echo form_label("Touchpoint", "touchpoint_target", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 118
        echo form_input(array("name" => "touchpoint_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "touchpoint_target", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 123
        echo form_label("Focused SKU", "focused_skus_target", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"base col-sm-9 focused_skus\">
                    ";
        // line 125
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["focus_skus"]) ? $context["focus_skus"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["index"] => $context["sku"]) {
            // line 126
            echo "                        <div class=\"form-group step\">
                            <div class=\"col-sm-5\">
                                ";
            // line 128
            echo form_dropdown((("focused_skus_target[" . $context["index"]) . "][sku_number]"), (isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["sku"], "sku_number", array()), "class=\"form-control select-product\"");
            echo "
                            </div>
                            ";
            // line 130
            echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
            echo "
                            <div class=\"col-sm-2\">
                                ";
            // line 132
            echo form_input(array("name" => (("focused_skus_target[" . $context["index"]) . "][amount]"), "value" => $this->getAttribute($context["sku"], "amount", array()), "class" => "form-control input-amount", "data-parsley-type" => "digits"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 141
            echo "                        <div class=\"form-group step\">
                            <div class=\"col-sm-5\">
                                ";
            // line 143
            echo form_dropdown("focused_skus_target[0][sku_number]", (isset($context["products"]) ? $context["products"] : null), null, "class=\"form-control select-product\"");
            echo "
                            </div>
                            ";
            // line 145
            echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
            echo "
                            <div class=\"col-sm-2\">
                                ";
            // line 147
            echo form_input(array("name" => "focused_skus_target[0][amount]", "value" => null, "class" => "form-control input-amount", "data-parsley-type" => "digits"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['sku'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "                    <div class=\"form-group clone-step hide\">
                        <div class=\"col-sm-5\">
                            ";
        // line 158
        echo form_dropdown(null, (isset($context["products"]) ? $context["products"] : null), null, "class=\"form-control select-product\"");
        echo "
                        </div>
                        ";
        // line 160
        echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
        echo "
                        <div class=\"col-sm-2\">
                            ";
        // line 162
        echo form_input(array("name" => null, "value" => null, "class" => "form-control input-amount", "data-parsley-type" => "digits"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 174
        echo form_label("Special Tasks", "special_tasks_target", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"base col-sm-9 special_tasks\">
                    ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["special_tasks"]) ? $context["special_tasks"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["index"] => $context["sku"]) {
            // line 177
            echo "                        <div class=\"form-group step\">
                            <div class=\"col-sm-5\">
                                ";
            // line 179
            echo form_input(array("name" => (("special_tasks_target[" . $context["index"]) . "][special_task_title]"), "value" => $this->getAttribute($context["sku"], "special_task_title", array()), "class" => "form-control input-title"));
            echo "
                            </div>
                            ";
            // line 181
            echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
            echo "
                            <div class=\"col-sm-2\">
                                ";
            // line 183
            echo form_input(array("name" => (("special_tasks_target[" . $context["index"]) . "][amount]"), "value" => $this->getAttribute($context["sku"], "amount", array()), "class" => "form-control input-amount", "data-parsley-type" => "digits"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 192
            echo "                        <div class=\"form-group step\">
                            <div class=\"col-sm-5\">
                                ";
            // line 194
            echo form_input(array("name" => "special_tasks_target[0][special_task_title]", "value" => null, "class" => "form-control input-title"));
            echo "
                            </div>
                            ";
            // line 196
            echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
            echo "
                            <div class=\"col-sm-2\">
                                ";
            // line 198
            echo form_input(array("name" => "special_tasks_target[0][amount]", "value" => null, "class" => "form-control input-amount", "data-parsley-type" => "digits"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['sku'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 207
        echo "                    <div class=\"form-group clone-step hide\">
                        <div class=\"col-sm-5\">
                            ";
        // line 209
        echo form_input(array("name" => null, "value" => $this->getAttribute((isset($context["sku"]) ? $context["sku"] : null), "sku_number", array()), "class" => "form-control input-title"));
        echo "
                        </div>
                        ";
        // line 211
        echo form_label("Amount", "amount", array("class" => "control-label col-sm-1"));
        echo "
                        <div class=\"col-sm-2\">
                            ";
        // line 213
        echo form_input(array("name" => null, "value" => null, "class" => "form-control input-amount", "data-parsley-type" => "digits"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 226
        echo form_input(array("name" => "focused_skus_amount_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "focused_skus_amount_target", array()), "type" => "hidden", "class" => "hide", "id" => "sku_amount"));
        echo "
    ";
        // line 227
        echo form_input(array("name" => "special_tasks_amount_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "special_tasks_amount_target", array()), "type" => "hidden", "class" => "hide", "id" => "special_task_amount"));
        echo "
    ";
        // line 228
        echo form_close();
        echo "

    <link href=\"";
        // line 230
        echo twig_escape_filter($this->env, site_url("res/css/jquery-confirm.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <script src=\"";
        // line 232
        echo twig_escape_filter($this->env, site_url("res/js/jquery-confirm.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "ami/kpi_target_in_sales_cycle/edit1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  409 => 232,  404 => 230,  399 => 228,  395 => 227,  391 => 226,  375 => 213,  370 => 211,  365 => 209,  361 => 207,  346 => 198,  341 => 196,  336 => 194,  332 => 192,  318 => 183,  313 => 181,  308 => 179,  304 => 177,  299 => 176,  294 => 174,  279 => 162,  274 => 160,  269 => 158,  265 => 156,  250 => 147,  245 => 145,  240 => 143,  236 => 141,  222 => 132,  217 => 130,  212 => 128,  208 => 126,  203 => 125,  198 => 123,  190 => 118,  185 => 116,  177 => 111,  172 => 109,  164 => 104,  159 => 102,  151 => 97,  146 => 95,  135 => 86,  133 => 85,  121 => 77,  118 => 76,  106 => 69,  103 => 68,  33 => 3,  30 => 2,  11 => 1,);
    }
}
