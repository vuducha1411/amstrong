<?php

/* ami/components/nav.html.twig */
class __TwigTemplate_d76f9153cce98bc4eb0e858e64da604160b88350c0e42bbfc664f14d735c3058 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"navbar-header\">
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
    </button>
    <a class=\"navbar-brand\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("ami/dashboard"), "html", null, true);
        echo "\">
        ";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["site_name"]) ? $context["site_name"] : null), "html", null, true);
        echo "
    </a>
</div>

<ul class=\"nav navbar-top-links navbar-right\">
    ";
        // line 204
        echo "    ";
        if ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "type", array()) == "SL") || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "type", array()) == "ADMIN"))) {
            // line 205
            echo "        ";
            if ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw")) && ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Sales Manager"))) {
                // line 206
                echo "            <li>
                <a href=\"";
                // line 207
                echo twig_escape_filter($this->env, site_url("ami/ssd/pending"), "html", null, true);
                echo "\">
                    <i class=\"fa fa-money fa-fw\"></i> Pending Consolidated Orders <span
                            class=\"badge ";
                // line 209
                echo (((isset($context["pendingSsd"]) ? $context["pendingSsd"] : null)) ? ("progress-bar-danger") : (""));
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["pendingSsd"]) ? $context["pendingSsd"] : null), "html", null, true);
                echo "</span>
                </a>
            </li>
        ";
            }
            // line 213
            echo "
        ";
            // line 214
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw", 1 => "my", 2 => "hk", 3 => "sa", 4 => "ph", 5 => "id", 6 => "vn", 7 => "th"))) {
                // line 215
                echo "            ";
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")))) {
                    // line 216
                    echo "                <li class=\"dropdown dropdown-reports\">
                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                        <i class=\"fa fa-group fa-fw\"></i> Pending Customer <span
                                class=\"badge ";
                    // line 219
                    echo (((isset($context["pendingCustomers"]) ? $context["pendingCustomers"] : null)) ? ("progress-bar-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["totalpendingCustomers"]) ? $context["totalpendingCustomers"] : null), "html", null, true);
                    echo "</span>
                        <i class=\"fa fa-caret-down\"></i>
                    </a>
                    <ul class=\"dropdown-menu multi-level\">
                        <li>
                            <a href=\"";
                    // line 224
                    echo twig_escape_filter($this->env, site_url("ami/customers?filter=pending"), "html", null, true);
                    echo "\">
                                Pending Customer <span
                                        class=\"badge ";
                    // line 226
                    echo (((isset($context["pendingCustomers"]) ? $context["pendingCustomers"] : null)) ? ("progress-bar-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["pendingCustomers"]) ? $context["pendingCustomers"] : null), "html", null, true);
                    echo "</span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
                    // line 230
                    echo twig_escape_filter($this->env, site_url("ami/distributors?filter=pending"), "html", null, true);
                    echo "\">
                                Pending Distributor <span
                                        class=\"badge ";
                    // line 232
                    echo (((isset($context["pendingDistributors"]) ? $context["pendingDistributors"] : null)) ? ("progress-bar-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["pendingDistributors"]) ? $context["pendingDistributors"] : null), "html", null, true);
                    echo "</span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
                    // line 236
                    echo twig_escape_filter($this->env, site_url("ami/wholesalers?filter=pending"), "html", null, true);
                    echo "\">
                                Pending Wholesalers <span
                                        class=\"badge ";
                    // line 238
                    echo (((isset($context["pendingWholesalers"]) ? $context["pendingWholesalers"] : null)) ? ("progress-bar-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["pendingWholesalers"]) ? $context["pendingWholesalers"] : null), "html", null, true);
                    echo "</span>
                            </a>
                        </li>
                    </ul>
                </li>
            ";
                }
                // line 244
                echo "        ";
            }
            // line 245
            echo "    ";
        }
        // line 246
        echo "
    <li class=\"dropdown dropdown-reports\">
        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
            <i class=\"fa fa-bar-chart fa-fw\"></i> Reports
            <i class=\"fa fa-caret-down\"></i>
        </a>
        <ul class=\"dropdown-menu multi-level\">
            ";
        // line 253
        if ((($this->getAttribute((isset($context["report_perm"]) ? $context["report_perm"] : null), "summary_report_view", array()) == 1) || ((isset($context["super_admin"]) ? $context["super_admin"] : null) == true))) {
            // line 254
            echo "                <li class=\"dropdown-submenu\">
                    <a tabindex=\"-1\" href=\"#\">Summary Reports</a>
                    <ul class=\"dropdown-menu\">
                        ";
            // line 259
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/report_call"), "html", null, true);
            echo "\">
                                01. Call Report</a></li>
                        ";
            // line 263
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/report_pantry_check"), "html", null, true);
            echo "\">
                                02. Pantry Report</a></li>
                        ";
            // line 267
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/report_otm"), "html", null, true);
            echo "\">
                                03. OTM Report</a></li>
                        ";
            // line 271
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/report_customers"), "html", null, true);
            echo "\">
                                04. Customer Report</a></li>
                        ";
            // line 274
            echo "                        ";
            // line 275
            echo "                        ";
            // line 281
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/get_app_versions_report"), "html", null, true);
            echo "\">
                                05. App Versions Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 283
            echo twig_escape_filter($this->env, site_url("ami/new_reports/penetration_data_report"), "html", null, true);
            echo "\">
                                06. Penetration Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 285
            echo twig_escape_filter($this->env, site_url("ami/reports/penetration_customers_report"), "html", null, true);
            echo "\">
                                07. Customers Penetration Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 287
            echo twig_escape_filter($this->env, site_url("ami/reports/get_perfect_call_report_tracking"), "html", null, true);
            echo "\">
                                08. Perfect Call Report</a></li>
                        ";
            // line 290
            echo "                                ";
            // line 291
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/rich_media"), "html", null, true);
            echo "\">
                                09. Rich Media Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 293
            echo twig_escape_filter($this->env, site_url("ami/reports/grip_grab_report"), "html", null, true);
            echo "\">
                                10. GGG Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 295
            echo twig_escape_filter($this->env, site_url("ami/reports/promotions_activity_report"), "html", null, true);
            echo "\">
                                11. Promotion Activities</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 297
            echo twig_escape_filter($this->env, site_url("ami/reports/report_last_pantry_check"), "html", null, true);
            echo "\">
                                12. Last Pantry Check Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 299
            echo twig_escape_filter($this->env, site_url("ami/reports/rich_media_demonstration"), "html", null, true);
            echo "\">
                                13. Rich media demostration Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 301
            echo twig_escape_filter($this->env, site_url("ami/reports/summary_out_of_trade_report"), "html", null, true);
            echo "\">
                                14. Out Of Trade Reports</a></li>
                    </ul>
                </li>
            ";
        }
        // line 306
        echo "            ";
        if ((($this->getAttribute((isset($context["report_perm"]) ? $context["report_perm"] : null), "transaction_report_view", array()) == 1) || ((isset($context["super_admin"]) ? $context["super_admin"] : null) == true))) {
            // line 307
            echo "                <li class=\"dropdown-submenu\">
                    <a tabindex=\"-1\" href=\"#\">Transaction Data</a>
                    <ul class=\"dropdown-menu\">
                        <li><a tabindex=\"-1\" href=\"";
            // line 310
            echo twig_escape_filter($this->env, site_url("ami/reports/data_call"), "html", null, true);
            echo "\">
                                01. Call and Route Data</a></li>
                        ";
            // line 314
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/data_tfo"), "html", null, true);
            echo "\">
                                02. TFO - PSD</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 316
            echo twig_escape_filter($this->env, site_url("ami/reports/data_ssd"), "html", null, true);
            echo "\">
                                03. SSD Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 318
            echo twig_escape_filter($this->env, site_url("ami/reports/data_pantry_check"), "html", null, true);
            echo "\">
                                04. Pantry/Inventory Check Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 320
            echo twig_escape_filter($this->env, site_url("ami/reports/data_sampling"), "html", null, true);
            echo "\">
                                05. Sampling Data</a></li>
                        ";
            // line 324
            echo "                        ";
            // line 325
            echo "                        ";
            // line 326
            echo "                        ";
            // line 338
            echo "                        ";
            // line 339
            echo "                        ";
            // line 340
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/competitor_reprot"), "html", null, true);
            echo "\">
                                06. Competitor Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 342
            echo twig_escape_filter($this->env, site_url("ami/reports/objective_record_data"), "html", null, true);
            echo "\">
                                07. Objective Record Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 344
            echo twig_escape_filter($this->env, site_url("ami/reports/activities_log_data"), "html", null, true);
            echo "\">
                                08. Activities Log Data</a></li>
                        ";
            // line 347
            echo "                        ";
            // line 348
            echo "                        ";
            // line 349
            echo "                        ";
            // line 350
            echo "                        <li><a tabindex=\"-1\"
                               href=\"";
            // line 351
            echo twig_escape_filter($this->env, site_url("ami/reports/get_perfect_call_report_tracking_detail"), "html", null, true);
            echo "\">
                                09. Perfect Call Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 353
            echo twig_escape_filter($this->env, site_url("ami/reports/get_perfect_store_report"), "html", null, true);
            echo "\">
                                10. Perfect Store Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 355
            echo twig_escape_filter($this->env, site_url("ami/reports/coaching_note"), "html", null, true);
            echo "\">
                                11. Coaching Note Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 357
            echo twig_escape_filter($this->env, site_url("ami/reports/customer_profiling_transaction"), "html", null, true);
            echo "\">
                                12. Customer Profiling Transaction Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 359
            echo twig_escape_filter($this->env, site_url("ami/reports/rap_report"), "html", null, true);
            echo "\">
                                13. RAP Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 361
            echo twig_escape_filter($this->env, site_url("ami/reports/dish_penetration_report"), "html", null, true);
            echo "\">
                                14. Dish Penetration Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 363
            echo twig_escape_filter($this->env, site_url("ami/reports/free_gift_report"), "html", null, true);
            echo "\">
                                15. Free Gift Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 365
            echo twig_escape_filter($this->env, site_url("ami/reports/ggg_report"), "html", null, true);
            echo "\">
                                16. GGG Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 367
            echo twig_escape_filter($this->env, site_url("ami/reports/notes_report"), "html", null, true);
            echo "\">
                                17. Notes Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 369
            echo twig_escape_filter($this->env, site_url("ami/reports/email_report"), "html", null, true);
            echo "\">
                                18. Email Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 371
            echo twig_escape_filter($this->env, site_url("ami/reports/uplift_report"), "html", null, true);
            echo "\">
                                19. UPLIFT Transactional Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 373
            echo twig_escape_filter($this->env, site_url("ami/reports/qc_report"), "html", null, true);
            echo "\">
                                20. QC Transactional Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 375
            echo twig_escape_filter($this->env, site_url("ami/reports/avp_point_report"), "html", null, true);
            echo "\">
                                21. AVP Point Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 377
            echo twig_escape_filter($this->env, site_url("ami/reports/gift_voucher_report"), "html", null, true);
            echo "\">
                                22. Gift Voucher Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 379
            echo twig_escape_filter($this->env, site_url("ami/reports/assessment_report"), "html", null, true);
            echo "\">
                                23. Assessment Reports</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 381
            echo twig_escape_filter($this->env, site_url("ami/reports/out_of_trade_report"), "html", null, true);
            echo "\">
                                24. Out of Trade Reports</a></li>
                        ";
            // line 384
            echo "                        ";
            // line 385
            echo "                        <li><a tabindex=\"-1\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/reports/transaction_rich_media_demonstration"), "html", null, true);
            echo "\">
                                25. Rich media demostration Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 387
            echo twig_escape_filter($this->env, site_url("ami/reports/ttl_report"), "html", null, true);
            echo "\">
                                26. TTL Report</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 389
            echo twig_escape_filter($this->env, site_url("ami/reports/bo_file"), "html", null, true);
            echo "\">
                                27. BO File</a></li>
                    </ul>
                </li>
            ";
        }
        // line 394
        echo "            ";
        // line 395
        echo "            ";
        if ((($this->getAttribute((isset($context["report_perm"]) ? $context["report_perm"] : null), "master_report_view", array()) == 1) || ((isset($context["super_admin"]) ? $context["super_admin"] : null) == true))) {
            // line 396
            echo "                <li class=\"dropdown-submenu\">
                    <a tabindex=\"-1\" href=\"#\">Master Data</a>
                    <ul class=\"dropdown-menu\">
                        <li><a tabindex=\"-1\" href=\"";
            // line 399
            echo twig_escape_filter($this->env, site_url("ami/reports/data_customers"), "html", null, true);
            echo "\">
                                01. Customer Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 401
            echo twig_escape_filter($this->env, site_url("ami/reports/data_product"), "html", null, true);
            echo "\">
                                02. Product Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 403
            echo twig_escape_filter($this->env, site_url("ami/reports/data_route_plan"), "html", null, true);
            echo "\">
                                03. Route Plan Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 405
            echo twig_escape_filter($this->env, site_url("ami/reports/data_organization"), "html", null, true);
            echo "\">
                                04. Organization Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 407
            echo twig_escape_filter($this->env, site_url("ami/reports/data_region_master"), "html", null, true);
            echo "\">
                                05. Region Master Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 409
            echo twig_escape_filter($this->env, site_url("ami/reports/data_ssd_mt"), "html", null, true);
            echo "\">
                                06. SSD Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 411
            echo twig_escape_filter($this->env, site_url("ami/reports/data_distributors"), "html", null, true);
            echo "\">
                                07. Distributors Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 413
            echo twig_escape_filter($this->env, site_url("ami/reports/data_wholesalers"), "html", null, true);
            echo "\">
                                08. Wholesalers Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 415
            echo twig_escape_filter($this->env, site_url("ami/reports/data_recipes"), "html", null, true);
            echo "\">
                                09. Recipes Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 417
            echo twig_escape_filter($this->env, site_url("ami/reports/route_master_temp"), "html", null, true);
            echo "\">
                                10. Route Master Template</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 419
            echo twig_escape_filter($this->env, site_url("ami/reports/customer_contact_data"), "html", null, true);
            echo "\">
                                11. Customer Contact Data</a></li>
                        <li><a tabindex=\"-1\" href=\"";
            // line 421
            echo twig_escape_filter($this->env, site_url("ami/reports/perfect_store_master_data"), "html", null, true);
            echo "\">
                                12. Perfect Store Master Data</a></li>
                    </ul>
                </li>
            ";
        }
        // line 426
        echo "            ";
        if ((($this->getAttribute((isset($context["report_perm"]) ? $context["report_perm"] : null), "business_report_view", array()) == 1) || ((isset($context["super_admin"]) ? $context["super_admin"] : null) == true))) {
            // line 427
            echo "                <li class=\"dropdown-submenu\">
                    <a tabindex=\"-1\" href=\"#\">Business Reports</a>
                    <ul class=\"dropdown-menu\">
                        <li><a tabindex=\"-1\" href=\"";
            // line 430
            echo twig_escape_filter($this->env, site_url("ami/reports/customer_profiling?filter=regional"), "html", null, true);
            echo "\">
                                01. Customer Profiling Report</a></li>
                    </ul>
                </li>
            ";
        }
        // line 435
        echo "        </ul>
    </li>
    ";
        // line 437
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["hiddenMenus"]) ? $context["hiddenMenus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sub_menu"]) {
            // line 438
            echo "        ";
            if ((twig_in_filter($this->getAttribute($context["sub_menu"], "id", array()), array(0 => $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "sub_roles_id", array()))) || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) ==  -1))) {
                // line 439
                echo "            ";
                if (($this->getAttribute($context["sub_menu"], "name", array()) == "regional")) {
                    // line 440
                    echo "                ";
                    // line 441
                    echo "                <li class=\"dropdown dropdown-reports\">
                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                        <i class=\"fa fa-bar-chart fa-fw\"></i> Regional
                        <i class=\"fa fa-caret-down\"></i>
                    </a>

                    <ul class=\"dropdown-menu multi-level\">
                        <li class=\"dropdown-submenu\">
                            <a tabindex=\"-1\" href=\"#\">Summary Reports</a>
                            <ul class=\"dropdown-menu\">
                                ";
                    // line 453
                    echo "                                <li><a tabindex=\"-1\" href=\"";
                    echo twig_escape_filter($this->env, site_url("ami/reports/report_call?filter=regional"), "html", null, true);
                    echo "\">
                                        01. Call Report</a></li>
                                ";
                    // line 457
                    echo "                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 458
                    echo twig_escape_filter($this->env, site_url("ami/reports/report_pantry_check?filter=regional"), "html", null, true);
                    echo "\">
                                        02. Pantry Report</a></li>
                                ";
                    // line 462
                    echo "                                <li><a tabindex=\"-1\" href=\"";
                    echo twig_escape_filter($this->env, site_url("ami/reports/report_otm?filter=regional"), "html", null, true);
                    echo "\">
                                        03. OTM Report</a></li>
                                ";
                    // line 466
                    echo "                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 467
                    echo twig_escape_filter($this->env, site_url("ami/reports/report_customers?filter=regional"), "html", null, true);
                    echo "\">
                                        04. Customer Report</a></li>
                                ";
                    // line 470
                    echo "                                ";
                    // line 471
                    echo "                                ";
                    // line 477
                    echo "                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 478
                    echo twig_escape_filter($this->env, site_url("ami/reports/get_app_versions_report?filter=regional"), "html", null, true);
                    echo "\">
                                        05. App Versions Report</a></li>
                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 481
                    echo twig_escape_filter($this->env, site_url("ami/reports/penetration_data_report?filter=regional"), "html", null, true);
                    echo "\">
                                        06. Penetration Report</a></li>
                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 484
                    echo twig_escape_filter($this->env, site_url("ami/reports/penetration_customers_report?filter=regional"), "html", null, true);
                    echo "\">
                                        07. Customers Penetration Report</a></li>
                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 487
                    echo twig_escape_filter($this->env, site_url("ami/reports/get_perfect_call_report_tracking?filter=regional"), "html", null, true);
                    echo "\">
                                        08. Perfect Call Report</a></li>
                                <li><a tabindex=\"-1\"
                                       href=\"";
                    // line 490
                    echo twig_escape_filter($this->env, site_url("ami/reports/regional_sync_report?filter=regional"), "html", null, true);
                    echo "\">
                                        09. Regional Sync Report</a></li>
                            </ul>
                        </li>


                        ";
                    // line 496
                    if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()), array(0 => "Admin"))) {
                        // line 497
                        echo "                            ";
                        // line 498
                        echo "                            ";
                        // line 499
                        echo "                            ";
                        // line 500
                        echo "                            ";
                        // line 501
                        echo "                            ";
                        // line 502
                        echo "                            ";
                        // line 503
                        echo "                            ";
                        // line 504
                        echo "                            ";
                        // line 505
                        echo "                            ";
                        // line 506
                        echo "                            ";
                        // line 507
                        echo "                            ";
                        // line 508
                        echo "                            ";
                        // line 509
                        echo "                            ";
                        // line 510
                        echo "                            ";
                        // line 511
                        echo "                            ";
                        // line 512
                        echo "                            ";
                        // line 513
                        echo "                            ";
                        // line 514
                        echo "                            ";
                        // line 515
                        echo "                            ";
                        // line 516
                        echo "                            ";
                        // line 517
                        echo "                            ";
                        // line 518
                        echo "                            ";
                        // line 519
                        echo "                            ";
                        // line 520
                        echo "                            ";
                        // line 521
                        echo "                            ";
                        // line 522
                        echo "                            ";
                        // line 523
                        echo "                            ";
                        // line 524
                        echo "                            ";
                        // line 525
                        echo "                            ";
                        // line 526
                        echo "                            ";
                        // line 527
                        echo "                            ";
                        // line 528
                        echo "                            ";
                        // line 529
                        echo "                            ";
                        // line 530
                        echo "                            ";
                        // line 531
                        echo "                            ";
                        // line 532
                        echo "                        ";
                    }
                    // line 533
                    echo "                    </ul>
                </li>
            ";
                }
                // line 536
                echo "        ";
            }
            // line 537
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 538
        echo "

    ";
        // line 540
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()), array(0 => "Admin", 1 => "Sales Manager"))) {
            // line 541
            echo "        <li>
            <a href=\"";
            // line 542
            echo twig_escape_filter($this->env, site_url("ami/dashboard/infographic"), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">
                <i class=\"fa fa-line-chart fa-fw\"></i> Infographic
            </a>
        </li>
    ";
        }
        // line 547
        echo "
    ";
        // line 549
        echo "    <li>
        <a href=\"";
        // line 550
        echo twig_escape_filter($this->env, site_url("ami/dashboard/infographic_new"), "html", null, true);
        echo "\" data-toggle=\"ajaxModal\">
            <i class=\"fa fa-line-chart fa-fw\"></i> Infographic New
        </a>
    </li>
    ";
        // line 555
        echo "
    ";
        // line 556
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) < 0)) {
            // line 557
            echo "        <li class=\"dropdown\">
            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                <img src=\"";
            // line 559
            echo twig_escape_filter($this->env, site_url((("res/img/flags/" . twig_lower_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()))) . ".png")), "html", null, true);
            echo "\"/> ";
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array())), "html", null, true);
            echo "
                <i class=\"fa fa-caret-down\"></i>
            </a>
            <ul class=\"dropdown-menu dropdown-contries\">
                ";
            // line 563
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                // line 564
                echo "                    <li ";
                echo (((twig_lower_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array())) == twig_lower_filter($this->env, $this->getAttribute($context["country"], "code", array())))) ? ("class=\"active\"") : (""));
                echo ">
                        <a href=\"";
                // line 565
                echo twig_escape_filter($this->env, site_url(("ami/dashboard/country/" . twig_lower_filter($this->env, $this->getAttribute($context["country"], "code", array())))), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 566
                echo twig_escape_filter($this->env, site_url((("res/img/flags/" . twig_lower_filter($this->env, $this->getAttribute($context["country"], "code", array()))) . ".png")), "html", null, true);
                echo "\"/> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
                echo "
                        </a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 570
            echo "            </ul>
        </li>
    ";
        }
        // line 573
        echo "    <li class=\"dropdown\">
        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
            <i class=\"fa fa-user fa-fw\"></i> ";
        // line 575
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
        echo " <i class=\"fa fa-caret-down\"></i>
        </a>
        <ul class=\"dropdown-menu dropdown-user\">
            <li><a><i class=\"fa fa-info fa-fw\"></i> ";
        // line 578
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
        echo "</a></li>
            <li class=\"divider\"></li>
            ";
        // line 581
        echo "            <li><a href=\"#\"><i class=\"fa fa-gear fa-fw\"></i> Settings</a></li>
            <li class=\"divider\"></li>
            <li>";
        // line 583
        echo anchor("ami/dashboard/logout", "<i class=\"fa fa-sign-out fa-fw\"></i> Logout");
        echo "</a></li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>";
    }

    public function getTemplateName()
    {
        return "ami/components/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  778 => 583,  774 => 581,  769 => 578,  763 => 575,  759 => 573,  754 => 570,  742 => 566,  738 => 565,  733 => 564,  729 => 563,  720 => 559,  716 => 557,  714 => 556,  711 => 555,  704 => 550,  701 => 549,  698 => 547,  690 => 542,  687 => 541,  685 => 540,  681 => 538,  675 => 537,  672 => 536,  667 => 533,  664 => 532,  662 => 531,  660 => 530,  658 => 529,  656 => 528,  654 => 527,  652 => 526,  650 => 525,  648 => 524,  646 => 523,  644 => 522,  642 => 521,  640 => 520,  638 => 519,  636 => 518,  634 => 517,  632 => 516,  630 => 515,  628 => 514,  626 => 513,  624 => 512,  622 => 511,  620 => 510,  618 => 509,  616 => 508,  614 => 507,  612 => 506,  610 => 505,  608 => 504,  606 => 503,  604 => 502,  602 => 501,  600 => 500,  598 => 499,  596 => 498,  594 => 497,  592 => 496,  583 => 490,  577 => 487,  571 => 484,  565 => 481,  559 => 478,  556 => 477,  554 => 471,  552 => 470,  547 => 467,  544 => 466,  538 => 462,  533 => 458,  530 => 457,  524 => 453,  512 => 441,  510 => 440,  507 => 439,  504 => 438,  500 => 437,  496 => 435,  488 => 430,  483 => 427,  480 => 426,  472 => 421,  467 => 419,  462 => 417,  457 => 415,  452 => 413,  447 => 411,  442 => 409,  437 => 407,  432 => 405,  427 => 403,  422 => 401,  417 => 399,  412 => 396,  409 => 395,  407 => 394,  399 => 389,  394 => 387,  388 => 385,  386 => 384,  381 => 381,  376 => 379,  371 => 377,  366 => 375,  361 => 373,  356 => 371,  351 => 369,  346 => 367,  341 => 365,  336 => 363,  331 => 361,  326 => 359,  321 => 357,  316 => 355,  311 => 353,  306 => 351,  303 => 350,  301 => 349,  299 => 348,  297 => 347,  292 => 344,  287 => 342,  281 => 340,  279 => 339,  277 => 338,  275 => 326,  273 => 325,  271 => 324,  266 => 320,  261 => 318,  256 => 316,  250 => 314,  245 => 310,  240 => 307,  237 => 306,  229 => 301,  224 => 299,  219 => 297,  214 => 295,  209 => 293,  203 => 291,  201 => 290,  196 => 287,  191 => 285,  186 => 283,  180 => 281,  178 => 275,  176 => 274,  170 => 271,  164 => 267,  158 => 263,  152 => 259,  147 => 254,  145 => 253,  136 => 246,  133 => 245,  130 => 244,  119 => 238,  114 => 236,  105 => 232,  100 => 230,  91 => 226,  86 => 224,  76 => 219,  71 => 216,  68 => 215,  66 => 214,  63 => 213,  54 => 209,  49 => 207,  46 => 206,  43 => 205,  40 => 204,  32 => 9,  28 => 8,  19 => 1,);
    }
}
