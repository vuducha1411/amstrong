<?php

/* ami/ssd/pending_action.html.twig */
class __TwigTemplate_d5a3745205b2bc993de7a2e586e1689371aaf12c83f52ec24bd5b7678282368e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">

    ";
        // line 3
        $context["url"] = (((isset($context["approve"]) ? $context["approve"] : null)) ? ("approve") : ("reject"));
        // line 4
        echo "
    ";
        // line 5
        echo form_open(site_url(((("ami/ssd/" . (isset($context["url"]) ? $context["url"] : null)) . "/") . $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array()))), array("class" => "modal-content pending-action-form"));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 8
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["url"]) ? $context["url"] : null)), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "sku_number", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <div class=\"list-group\">
                    ";
        // line 13
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array())) {
            // line 14
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-group pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Customer</h4>
                                <p class=\"list-group-item-text\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array()), "armstrong_2_customers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array()), "armstrong_2_customers_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 22
        echo "
                    ";
        // line 23
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array())) {
            // line 24
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-male pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Salesperson</h4>
                                <p class=\"list-group-item-text\">";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "armstrong_2_salespersons_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "last_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 32
        echo "
                    ";
        // line 33
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array())) {
            // line 34
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-shopping-cart pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Wholesaler</h4>
                                <p class=\"list-group-item-text\">";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array()), "armstrong_2_wholesalers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 42
        echo "
                    ";
        // line 43
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array())) {
            // line 44
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-bus pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Distributor</h4>
                                <p class=\"list-group-item-text\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array()), "armstrong_2_distributors_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 52
        echo "
                    ";
        // line 53
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "call_record", array())) {
            // line 54
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-bus pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Distributor</h4>
                                <p class=\"list-group-item-text\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "call_record", array()), "armstrong_2_call_records_id", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 62
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer ";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()), "html", null, true);
        echo "\">
            ";
        // line 66
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Sales Manager")) {
            // line 67
            echo "                <button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-fw ";
            echo (((isset($context["approve"]) ? $context["approve"] : null)) ? ("fa-thumbs-up") : ("fa-thumbs-down"));
            echo "\"></i> ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["url"]) ? $context["url"] : null)), "html", null, true);
            echo "</a></button>
            ";
        }
        // line 69
        echo "            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    ";
        // line 71
        echo form_close();
        echo "
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/ssd/pending_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 71,  162 => 69,  154 => 67,  152 => 66,  148 => 65,  143 => 62,  136 => 58,  130 => 54,  128 => 53,  125 => 52,  116 => 48,  110 => 44,  108 => 43,  105 => 42,  96 => 38,  90 => 34,  88 => 33,  85 => 32,  74 => 28,  68 => 24,  66 => 23,  63 => 22,  54 => 18,  48 => 14,  46 => 13,  34 => 8,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
