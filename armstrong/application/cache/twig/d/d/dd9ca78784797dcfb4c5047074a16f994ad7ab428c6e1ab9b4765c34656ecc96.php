<?php

/* ami/call_records/record.html.twig */
class __TwigTemplate_dd9ca78784797dcfb4c5047074a16f994ad7ab428c6e1ab9b4765c34656ecc96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["records"]) ? $context["records"] : null)) {
            echo " 

    <div class=\"panel panel-default\">                            
        <div class=\"panel-body\">                
            <div class=\"\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                    <thead>
                        <tr>
                            ";
            // line 9
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "call_records"))) {
                // line 10
                echo "                                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                            ";
            }
            // line 12
            echo "                            <th>ID</th>
                            <th>Salesperson Name</th>
                            <th>Operator</th>
                            <th>Call Start</th>
                            <th>Call End</th>
                            <th>Time Taken</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 22
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
                // line 23
                echo "                            <tr>
                                ";
                // line 24
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "call_records"))) {
                    // line 25
                    echo "                                    <td class=\"text-center\">";
                    echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["record"], "armstrong_2_call_records_id", array())));
                    echo "</td>
                                ";
                }
                // line 27
                echo "                                <td><a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/call_records/edit/" . $this->getAttribute($context["record"], "armstrong_2_call_records_id", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "armstrong_2_call_records_id", array()), "html", null, true);
                echo "\" data-toggle=\"ajaxModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "armstrong_2_call_records_id", array()), "html", null, true);
                echo "</a></td>
                                <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "last_name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "armstrong_2_customers_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "armstrong_2_customers_name", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "datetime_call_start", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "datetime_call_end", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "time_taken", array()), "html", null, true);
                echo "</td>
                                <td class=\"center text-center\" style=\"min-width: 80px;\">
                                    ";
                // line 34
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "call_records")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "call_records")))) {
                    // line 35
                    echo "                                        ";
                    $this->loadTemplate("ami/components/table_btn.html.twig", "ami/call_records/record.html.twig", 35)->display(array_merge($context, array("url" => "ami/call_records", "id" => $this->getAttribute($context["record"], "armstrong_2_call_records_id", array()), "permission" => "call_records")));
                    // line 36
                    echo "                                    ";
                }
                // line 37
                echo "                                </td>
                            </tr>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "                    </tbody>
                </table>
            </div>
            <!-- /. -->
        </div>
        <!-- /.panel-body -->

    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('#dataTables').dataTable({
            \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
            'order': [[1, 'asc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });        
    });  
    </script>
";
        } else {
            // line 65
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no call records</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/call_records/record.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 65,  136 => 40,  120 => 37,  117 => 36,  114 => 35,  112 => 34,  107 => 32,  103 => 31,  99 => 30,  93 => 29,  85 => 28,  76 => 27,  70 => 25,  68 => 24,  65 => 23,  48 => 22,  36 => 12,  32 => 10,  30 => 9,  19 => 1,);
    }
}
