<?php

/* ami/promotions/preview.html.twig */
class __TwigTemplate_23a299c43dde739e893480d6be39261a99df036374e7945bca7e6e19eadf031d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sku_name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">               
            <p>";
        // line 8
        echo $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "description", array());
        echo "</p>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>            
            ";
        // line 12
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "post"))) {
            // line 13
            echo "                ";
            echo html_btn(site_url(("ami/promotions/edit/" . $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 15
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/promotions/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 15,  40 => 13,  38 => 12,  31 => 8,  25 => 5,  19 => 1,);
    }
}
