<?php

/* ami/perfect_store/edit.html.twig */
class __TwigTemplate_276562f04d2d6c320c0c4c198876d5d5cf1c5e0cadfd9f2ffd3a748676e82632 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/perfect_store/edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        echo form_open(site_url("ami/perfect_store/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Perfect Store</h1>
                <div class=\"text-right\">
                    ";
        // line 12
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/perfect_store/edit.html.twig", 12)->display(array_merge($context, array("url" => "ami/perfect_store", "id" => $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "id", array()), "permission" => "perfect_store")));
        // line 13
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 23
        echo form_label("Call Records", "armstrong_2_call_records_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 25
        echo form_dropdown("armstrong_2_call_records_id", (isset($context["callRecords"]) ? $context["callRecords"] : null), $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "armstrong_2_call_records_id", array()), (("
                        class=\"form-control AutoInputTrigger\" 
                        data-parsley-required=\"true\" 
                        data-url=\"" . site_url("ami/call_records/info")) . "\"
                    "));
        // line 29
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 34
        echo form_label("Salespersons", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 36
        echo form_input(array("name" => "armstrong_2_salespersons_id", "value" => $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "armstrong_2_salespersons_id", array()), "class" => "form-control AutoInput", "readonly" => "readonly"));
        // line 39
        echo "
                </div>
            </div>

            <ul class=\"nav nav-tabs m-t-lg\" role=\"tablist\">
                ";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["storeContent"]) ? $context["storeContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 45
            echo "                    <li role=\"presentation\" class=\"";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\"><a href=\"#tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\" role=\"tab\" data-toggle=\"tab\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</a></li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </ul>

            <div class=\"tab-content m-t\">
                ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["storeContent"]) ? $context["storeContent"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["id"] => $context["content"]) {
            // line 51
            echo "                    <div role=\"tabpanel\" class=\"tab-pane ";
            echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
            echo "\" id=\"tb_";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">

                        ";
            // line 53
            if ((($context["id"] == 0) && (isset($context["products"]) ? $context["products"] : null))) {
                echo "                                        
                            <table class=\"table store-content\">
                                <tbody>                                   
                                    ";
                // line 56
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                    // line 57
                    echo "                                        <tr>
                                            <td>";
                    // line 58
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["data"], "title", array()), array(), "array"), "html", null, true);
                    echo "</td>
                                            <td>";
                    // line 59
                    echo form_checkbox("active[]", $this->getAttribute($context["data"], "id", array()), twig_in_filter($this->getAttribute($context["data"], "id", array()), $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "active", array())));
                    echo "</td>
                                        </tr>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 61
                echo "   
                                </tbody>
                            </table>                                 
                        ";
            } else {
                // line 65
                echo "                            <table class=\"table store-content\">
                                <tbody>                                   
                                    ";
                // line 67
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["content"], "content", array()));
                foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                    // line 68
                    echo "                                        <tr>
                                            <td>";
                    // line 69
                    echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "title", array()), "html", null, true);
                    echo "</td>
                                            <td>";
                    // line 70
                    echo form_checkbox("active[]", $this->getAttribute($context["data"], "id", array()), twig_in_filter($this->getAttribute($context["data"], "id", array()), $this->getAttribute((isset($context["perfectStore"]) ? $context["perfectStore"] : null), "active", array())));
                    echo "</td>
                                        </tr>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 72
                echo "   
                                </tbody>
                            </table>
                        ";
            }
            // line 76
            echo "                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "            </div>
        </div>
    </div> 

    ";
        // line 82
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/perfect_store/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 82,  235 => 78,  220 => 76,  214 => 72,  205 => 70,  201 => 69,  198 => 68,  194 => 67,  190 => 65,  184 => 61,  175 => 59,  171 => 58,  168 => 57,  164 => 56,  158 => 53,  150 => 51,  133 => 50,  128 => 47,  107 => 45,  90 => 44,  83 => 39,  81 => 36,  76 => 34,  69 => 29,  63 => 25,  58 => 23,  46 => 13,  44 => 12,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
