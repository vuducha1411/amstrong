<?php

/* ami/accessibility/edit.html.twig */
class __TwigTemplate_27eeca8a5f81059960f3afc9d8465cc807fa1c2351b7e70d7c36e9f5cb386f16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/accessibility/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "

    <script type=\"text/javascript\"
            src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/tokenfield/bootstrap-tokenfield.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\">
        \$(function () {
            \$('#tokenfield')
                    .on('tokenfield:createtoken', function (e) {
                        var existingTokens = \$(this).tokenfield('getTokens');
                        // console.log(existingTokens);
                        \$.each(existingTokens, function (index, token) {
                            if (token.value === e.attrs.value) {
                                e.preventDefault();
                                return;
                            }
                        });
                    })
                    .tokenfield({
                        autocomplete: {
                            source: \"";
        // line 26
        echo twig_escape_filter($this->env, site_url("ami/accessibility/tokenfield"), "html", null, true);
        echo "\",
                            delay: 100
                        }
                    });

            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 34
        echo twig_escape_filter($this->env, site_url("ami/accessibility/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            \$('.checkbox-manage').click(function (e) {
                // \$('.checkbox-manage').prop('checked', false);
                // \$(this).prop('checked', true);
                var \$this = \$(this),
                        \$manage = \$('.checkbox-manage.manage-all'),
                        \$manageTeam = \$('.checkbox-manage.manage-staff')

                if (\$this.hasClass('manage-all')) {
                    \$manageTeam.prop('checked', false);
                }

                if (\$this.hasClass('manage-staff')) {
                    \$manage.prop('checked', false);
                }

                \$('#perm_customer').find('.checkbox-view').prop('checked', true);
            });
        });
    </script>
";
    }

    // line 64
    public function block_css($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("res/css/plugins/tokenfield/bootstrap-tokenfield.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "
    ";
        // line 72
        echo form_open(site_url("ami/accessibility/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Accessibility</h1>

                <div class=\"text-right\">
                    ";
        // line 87
        echo "                    ";
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/accessibility/edit.html.twig", 87)->display(array_merge($context, array("url" => "ami/accessibility", "id" => $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "id", array()), "permission" => "accessibility")));
        // line 88
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#permissions\" role=\"tab\"
                                                          data-toggle=\"tab\">Permissions</a></li>
                <li role=\"presentation\"><a href=\"#users\" role=\"tab\" data-toggle=\"tab\">Salespersons</a></li>
            </ul>

            <div class=\"tab-content\">

                <div role=\"tabpanel\" class=\"tab-pane active\" id=\"permissions\">
                    <div class=\"form-group m-t-md\">
                        ";
        // line 107
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 109
        if (twig_in_filter($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "name", array()), array(0 => "Admin", 1 => "Sales Manager"))) {
            // line 110
            echo "                                ";
            echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true", "readonly" => "readonly"));
            // line 113
            echo "
                            ";
        } else {
            // line 115
            echo "                                ";
            echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
            // line 117
            echo "
                            ";
        }
        // line 119
        echo "                        </div>
                    </div>
                    <div class=\"form-group m-t-md sr-only\">
                        ";
        // line 122
        echo form_label("Global", "global", array("class" => "text-right col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 124
        echo form_checkbox(array("name" => "global", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "global", array())));
        echo "
                        </div>
                    </div>
                    <table class=\"table no-parsley permissions m-b-lg\" id=\"md_report }}\">
                        <tr>
                            <th class=\"group-title text-ellipsis\"><i
                                        class=\"fa fa-bar-chart fa-fw\"></i> Report
                            </th>
                            <th class=\"text-center\"><a class=\"\"
                                                       data-target=\"#md_report input.checkbox-view\">View</a>
                                ";
        // line 135
        echo "                                ";
        // line 136
        echo "                                ";
        // line 137
        echo "                                ";
        // line 138
        echo "                                ";
        // line 139
        echo "                                ";
        // line 140
        echo "                        </tr>
                        <tr>
                            <th class=\"permission-title text-ellipsis\">Summary Report</th>
                            <td class=\"text-center\">";
        // line 143
        echo form_checkbox(array("name" => "restrictions[summary_report_view][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "summary_report_view", array())));
        echo "</td>
                        </tr>
                        <tr>
                            <th class=\"permission-title text-ellipsis\">Transaction Data Report</th>
                            <td class=\"text-center\">";
        // line 147
        echo form_checkbox(array("name" => "restrictions[transaction_report_view][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "transaction_report_view", array())));
        echo "</td>
                        </tr>
                        <tr>
                            <th class=\"permission-title text-ellipsis\">Master Data Report</th>
                            <td class=\"text-center\">";
        // line 151
        echo form_checkbox(array("name" => "restrictions[master_report_view][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "master_report_view", array())));
        echo "</td>
                        </tr>
                        <tr>
                            <th class=\"permission-title text-ellipsis\">Business Report</th>
                            <td class=\"text-center\">";
        // line 155
        echo form_checkbox(array("name" => "restrictions[business_report_view][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "business_report_view", array())));
        echo "</td>
                        </tr>
                        <tr>
                            <th class=\"permission-title text-ellipsis\">Show/hide Button</th>
                            <td class=\"text-center\">
                                ";
        // line 160
        echo form_checkbox(array("name" => "restrictions[report_view][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "report_view", array())));
        echo "
                                View Button
                                &nbsp;
                                ";
        // line 163
        echo form_checkbox(array("name" => "restrictions[report_export][]", "value" => 1, "class" => "checkbox-view", "checked" => $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "report_export", array())));
        echo "
                                Export Button
                            </td>
                        </tr>
                    </table>
                    ";
        // line 168
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menuItems"]) ? $context["menuItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menuItem"]) {
            // line 169
            echo "                        ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["menuItem"], "child_nodes", array())) > 0)) {
                // line 170
                echo "                            <table class=\"table no-parsley permissions m-b-lg\" id=\"md_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo "\">
                                <tr>
                                    <th class=\"group-title text-ellipsis\"><i
                                                class=\"fa ";
                // line 173
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "icon", array()), "html", null, true);
                echo " fa-fw\"></i> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "display_name", array()), "html", null, true);
                echo "
                                    </th>
                                    <th class=\"text-center\"><a class=\"CheckAll\"
                                                               data-target=\"#md_";
                // line 176
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo " input.checkbox-view\">View</a>
                                    </th>
                                    <th class=\"text-center\"><a class=\"CheckAll\"
                                                               data-target=\"#md_";
                // line 179
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo " input.checkbox-add\">Add</a>
                                    </th>
                                    <th class=\"text-center\"><a class=\"CheckAll\"
                                                               data-target=\"#md_";
                // line 182
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo " input.checkbox-edit\">Edit</a>
                                    </th>
                                    <th class=\"text-center\"><a class=\"CheckAll\"
                                                               data-target=\"#md_";
                // line 185
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo " input.checkbox-delete\">Delete</a>
                                    </th>
                                    ";
                // line 187
                if (twig_in_filter($this->getAttribute($context["menuItem"], "name", array()), array(0 => "sales_management"))) {
                    // line 188
                    echo "                                        <th class=\"text-center\"><a class=\"\">Manage</a></th>
                                    ";
                }
                // line 190
                echo "                                </tr>

                                ";
                // line 192
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menuItem"], "child_nodes", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["childNode"]) {
                    // line 193
                    echo "                                    <tr id=\"perm_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childNode"], "name", array()), "html", null, true);
                    echo "\">
                                        <th class=\"permission-title text-ellipsis\">";
                    // line 194
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childNode"], "display_name", array()), "html", null, true);
                    echo "</th>
                                        <td class=\"text-center\">";
                    // line 195
                    echo form_checkbox(array("name" => "restrictions[view][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-view", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "view", array()))));
                    echo "</td>
                                        <td class=\"text-center\">";
                    // line 196
                    echo form_checkbox(array("name" => "restrictions[add][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-add", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "add", array()))));
                    echo "</td>
                                        <td class=\"text-center\">";
                    // line 197
                    echo form_checkbox(array("name" => "restrictions[edit][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-edit", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "edit", array()))));
                    echo "</td>
                                        <td class=\"text-center\">";
                    // line 198
                    echo form_checkbox(array("name" => "restrictions[delete][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-delete", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "delete", array()))));
                    echo "</td>
                                        ";
                    // line 199
                    if (twig_in_filter($this->getAttribute($context["menuItem"], "name", array()), array(0 => "sales_management", 1 => "customers_management"))) {
                        // line 200
                        echo "                                            <td class=\"text-center\">
                                                ";
                        // line 201
                        if (twig_in_filter($this->getAttribute($context["childNode"], "name", array()), array(0 => "salespersons"))) {
                            // line 202
                            echo "                                                    ";
                            echo form_checkbox(array("name" => "restrictions[manage_all][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-manage manage-all Tooltip", "title" => "Manage All", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "manage_all", array()))));
                            echo " All
                                                    &nbsp;
                                                    ";
                            // line 204
                            echo form_checkbox(array("name" => "restrictions[manage_staff][]", "value" => $this->getAttribute($context["childNode"], "id", array()), "class" => "checkbox-manage manage-staff Tooltip", "title" => "Manage Team", "checked" => twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "manage_staff", array()))));
                            echo " Staff
                                                ";
                        }
                        // line 206
                        echo "                                            </td>
                                        ";
                    }
                    // line 208
                    echo "                                    </tr>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['childNode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 210
                echo "                            </table>
                        ";
            }
            // line 212
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menuItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 213
        echo "                    <table class=\"table no-parsley permissions m-b-lg\" id=\"md_report }}\">
                        <tr>
                            <th class=\"group-title text-ellipsis\"><i
                                        class=\"fa ";
        // line 216
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "icon", array()), "html", null, true);
        echo " fa-fw\"></i> Webshop
                            </th>
                            <th class=\"text-center\"><a class=\"CheckAll\"
                                                       data-target=\"#md_";
        // line 219
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "name", array()), "html", null, true);
        echo " input.checkbox-view\">View</a>
                            </th>
                            <th class=\"text-center\"><a class=\"CheckAll\"
                                                       data-target=\"#md_";
        // line 222
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "name", array()), "html", null, true);
        echo " input.checkbox-add\">Add</a>
                            </th>
                            <th class=\"text-center\"><a class=\"CheckAll\"
                                                       data-target=\"#md_";
        // line 225
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "name", array()), "html", null, true);
        echo " input.checkbox-edit\">Edit</a>
                            </th>
                            <th class=\"text-center\"><a class=\"CheckAll\"
                                                       data-target=\"#md_";
        // line 228
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "name", array()), "html", null, true);
        echo " input.checkbox-delete\">Delete</a>
                            </th>
                            ";
        // line 230
        if (twig_in_filter($this->getAttribute((isset($context["menuItem"]) ? $context["menuItem"] : null), "name", array()), array(0 => "sales_management"))) {
            // line 231
            echo "                                <th class=\"text-center\"><a class=\"\">Manage</a></th>
                            ";
        }
        // line 233
        echo "                        </tr>
                        <tr>
                            <td></td>
                            <td class=\"text-center\">";
        // line 236
        echo form_checkbox(array("name" => "restrictions[view][]", "value" => 92, "class" => "checkbox-view", "checked" => twig_in_filter(92, $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "view", array()))));
        echo "</td>
                            <td class=\"text-center\">";
        // line 237
        echo form_checkbox(array("name" => "restrictions[add][]", "value" => 92, "class" => "checkbox-view", "checked" => twig_in_filter(92, $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "add", array()))));
        echo "</td>
                            <td class=\"text-center\">";
        // line 238
        echo form_checkbox(array("name" => "restrictions[edit][]", "value" => 92, "class" => "checkbox-view", "checked" => twig_in_filter(92, $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "edit", array()))));
        echo "</td>
                            <td class=\"text-center\">";
        // line 239
        echo form_checkbox(array("name" => "restrictions[delete][]", "value" => 92, "class" => "checkbox-view", "checked" => twig_in_filter(92, $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "delete", array()))));
        echo "</td>
                        </tr>
                    </table>
                </div>

                <div role=\"tabpanel\" class=\"tab-pane\" id=\"users\">
                    <div class=\"form-group m-t-md\">
                        ";
        // line 246
        echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 248
        echo form_input(array("name" => "salespersons", "class" => "form-control", "id" => "tokenfield"));
        echo "
                        </div>
                    </div>

                    <table class=\"table table-striped table-bordered table-hover no-parsley\" id=\"dataTables\">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th class=\"nosort\"></th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 263
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["salespersons"]) ? $context["salespersons"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["salesperson"]) {
            // line 264
            echo "                            <tr>
                                <td>
                                    <a href=\"";
            // line 266
            echo twig_escape_filter($this->env, site_url(("ami/salespersons/edit/" . $this->getAttribute($context["salesperson"], "armstrong_2_salespersons_id", array()))), "html", null, true);
            echo "\"
                                       title=\"";
            // line 267
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "last_name", array()), "html", null, true);
            echo "\"
                                       data-toggle=\"ajaxModal\">";
            // line 268
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "</a></td>
                                <td>";
            // line 269
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "last_name", array()), "html", null, true);
            echo "</td>
                                <td class=\"center\">";
            // line 270
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "date_created", array()), "html", null, true);
            echo "</td>
                                <td class=\"center\">";
            // line 271
            echo twig_escape_filter($this->env, $this->getAttribute($context["salesperson"], "last_updated", array()), "html", null, true);
            echo "</td>
                                <td class=\"center\" style=\"min-width: 80px;\">
                                    <div class=\"btn-group\">
                                        ";
            // line 274
            echo html_btn(site_url(("ami/accessibility/salespersons/edit/" . $this->getAttribute($context["salesperson"], "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit", "data-toggle" => "ajaxModal"));
            echo "
                                        ";
            // line 275
            echo html_btn(site_url(("ami/accessibility/salespersons/delete/" . $this->getAttribute($context["salesperson"], "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
            echo "
                                    </div>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['salesperson'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 280
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 287
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/accessibility/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  548 => 287,  539 => 280,  528 => 275,  524 => 274,  518 => 271,  514 => 270,  508 => 269,  504 => 268,  498 => 267,  494 => 266,  490 => 264,  486 => 263,  468 => 248,  463 => 246,  453 => 239,  449 => 238,  445 => 237,  441 => 236,  436 => 233,  432 => 231,  430 => 230,  425 => 228,  419 => 225,  413 => 222,  407 => 219,  401 => 216,  396 => 213,  390 => 212,  386 => 210,  379 => 208,  375 => 206,  370 => 204,  364 => 202,  362 => 201,  359 => 200,  357 => 199,  353 => 198,  349 => 197,  345 => 196,  341 => 195,  337 => 194,  332 => 193,  328 => 192,  324 => 190,  320 => 188,  318 => 187,  313 => 185,  307 => 182,  301 => 179,  295 => 176,  287 => 173,  280 => 170,  277 => 169,  273 => 168,  265 => 163,  259 => 160,  251 => 155,  244 => 151,  237 => 147,  230 => 143,  225 => 140,  223 => 139,  221 => 138,  219 => 137,  217 => 136,  215 => 135,  202 => 124,  197 => 122,  192 => 119,  188 => 117,  185 => 115,  181 => 113,  178 => 110,  176 => 109,  171 => 107,  150 => 88,  147 => 87,  136 => 72,  133 => 71,  130 => 70,  124 => 67,  120 => 66,  115 => 65,  112 => 64,  79 => 34,  68 => 26,  48 => 9,  44 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}
