<?php

/* ami/promotional_mechanics/items/promotion_1_edit.html.twig */
class __TwigTemplate_21a4c3bcbc720876d8fee33c11276315503532e2a4ea2dc2fb33cc5a8cda0655 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    ";
        // line 2
        echo form_label("Date Range", "range_id", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 4
        echo form_dropdown("range_id", (isset($context["range"]) ? $context["range"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "range_id", array()), "id=\"range_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 8
        echo form_label("Promotion Name", "name", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 10
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 14
        echo form_label("Promotion Description", "sub_name", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 16
        echo form_input(array("name" => "sub_name", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "sub_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 20
        echo form_label("Months since last bought", "n_months", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 22
        echo form_input(array("name" => "n_months", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "n_months", array()), "class" => "form-control", "data-parsley-required" => "true", "min" => 1, "data-parsley-type" => "digits"));
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 26
        echo form_label("Purchased SKU", "purchased_sku_number", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 28
        echo form_dropdown("purchased_sku_number", (isset($context["products"]) ? $context["products"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "purchased_sku_number", array()), "class=\"form-control\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 32
        echo form_label("Purchased SKU quantity", "purchased_sku_quantity", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-2\">
        ";
        // line 34
        echo form_input(array("name" => "purchased_sku_quantity", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "purchased_sku_quantity", array()), "class" => "form-control", "data-parsley-required" => "true", "min" => 1, "data-parsley-type" => "digits"));
        echo "
    </div>
    ";
        // line 36
        echo form_label("Type", "purchased_sku_quantity_type", array("class" => "control-label col-sm-1"));
        echo "
    <div class=\"col-sm-3\">
        ";
        // line 38
        echo form_dropdown("purchased_sku_quantity_type", (isset($context["quantity_type"]) ? $context["quantity_type"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "purchased_sku_quantity_type", array()), "class=\"form-control\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 42
        echo form_label("Free SKU", "free_sku_number", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        ";
        // line 44
        echo form_dropdown("free_sku_number", (isset($context["products"]) ? $context["products"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "free_sku_number", array()), "class=\"form-control\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 48
        echo form_label("Free SKU quantity", "free_sku_quantity", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-2\">
        ";
        // line 50
        echo form_input(array("name" => "free_sku_quantity", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "free_sku_quantity", array()), "class" => "form-control", "data-parsley-required" => "true", "min" => 1, "data-parsley-type" => "digits"));
        echo "
    </div>
    ";
        // line 52
        echo form_label("Type", "free_sku_quantity_type", array("class" => "control-label col-sm-1"));
        echo "
    <div class=\"col-sm-3\">
        ";
        // line 54
        echo form_dropdown("free_sku_quantity_type", (isset($context["quantity_type"]) ? $context["quantity_type"] : null), $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "free_sku_quantity_type", array()), "class=\"form-control\"");
        echo "
    </div>
</div>
<div class=\"form-group\">
    ";
        // line 58
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6 no-parsley\">
        <label class=\"radio-inline\">";
        // line 60
        echo form_radio("active", 0, ($this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "active", array()) == 0));
        echo " No</label>&nbsp;
        <label class=\"radio-inline\">";
        // line 61
        echo form_radio("active", 1, ($this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "active", array()) == 1));
        echo " Yes</label>&nbsp;
    </div>
</div>
";
        // line 64
        echo form_input(array("name" => "type", "value" => 1, "type" => "hidden", "class" => "hide"));
        echo "

<script type=\"text/javascript\">
    /**
    function autochoosewhenchange(){
        if(\$('select[name=\"purchased_sku_number\"]').val() != '')
        {
            \$('select[name=\"free_sku_number\"]').removeAttr('disabled');
            \$('select[name=\"free_sku_number\"] option:nth-child(1)').attr('disabled', 'disabled');
            if(\$('select[name=\"free_sku_number\"]').val() == null || \$('select[name=\"free_sku_number\"]').val() == '')
            {
                \$('select[name=\"free_sku_number\"]').val(\$('select[name=\"purchased_sku_number\"]').val());
            }
        }
        else
        {
            \$('select[name=\"free_sku_number\"] option:nth-child(1)').removeAttr('disabled');
            \$('select[name=\"free_sku_number\"]').val('');
            \$('select[name=\"free_sku_number\"]').attr('disabled', 'disabled');
        }
    }
    \$(document).ready(function(){
        autochoosewhenchange();
        \$('select[name=\"purchased_sku_number\"]').change(function(){
            autochoosewhenchange();
        });
    });
    */
</script>";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/items/promotion_1_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 64,  147 => 61,  143 => 60,  138 => 58,  131 => 54,  126 => 52,  121 => 50,  116 => 48,  109 => 44,  104 => 42,  97 => 38,  92 => 36,  87 => 34,  82 => 32,  75 => 28,  70 => 26,  63 => 22,  58 => 20,  51 => 16,  46 => 14,  39 => 10,  34 => 8,  27 => 4,  22 => 2,  19 => 1,);
    }
}
