<?php

/* ami/route_plan/index.html.twig */
class __TwigTemplate_2190b580f8c76ce03c30cbd6c2dc812c901d03288350be4af609a3b2132dccd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/route_plan/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/call_modules.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 14
    public function block_css($context, array $blocks = array())
    {
        // line 15
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "
    ";
        // line 23
        echo form_open(site_url("ami/route_plan/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 28
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Route Plan Draft") : ("Route Plan"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 31
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 33
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/route_plan/index.html.twig", 33)->display(array_merge($context, array("url" => "ami/route_plan", "icon" => "fa-calendar", "title" => "Route Plan", "permission" => "route_plan")));
        // line 34
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">                        
            ";
        // line 62
        echo "
            ";
        // line 63
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 64
            echo "                <div class=\"form-group\">
                    ";
            // line 65
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 67
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"SalespersonsSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 71
        echo "
            <div class=\"form-group\">
                ";
        // line 73
        echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 75
        echo form_input(array("name" => "date_time", "value" => null, "class" => "form-control", "id" => "InputDateRange"));
        // line 78
        echo "
                </div>

                <input type=\"hidden\" id=\"InputStartDate\" value=\"\" />
                <input type=\"hidden\" id=\"InputEndDate\" value=\"\" />
            </div>

            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\" 
                        data-url=\"";
        // line 88
        echo twig_escape_filter($this->env, site_url("ami/route_plan/plans"), "html", null, true);
        echo "\"
                        data-container=\"#PlansContainer\"
                        data-draft=\"";
        // line 90
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo "\">Generate</a>
                </div>
            </div>

            <div id=\"PlansContainer\"></div>
        
        </div>
    </div>
    
    ";
        // line 99
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/route_plan/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 99,  171 => 90,  166 => 88,  154 => 78,  152 => 75,  147 => 73,  143 => 71,  136 => 67,  131 => 65,  128 => 64,  126 => 63,  123 => 62,  112 => 34,  110 => 33,  105 => 31,  99 => 28,  91 => 23,  88 => 22,  85 => 21,  79 => 18,  75 => 17,  71 => 16,  67 => 15,  64 => 14,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
