<?php

/* ami/customers/pending_action_amended_data.html.twig */
class __TwigTemplate_284552f5764d2a0c010569b39bcc201baa3e3827e5958fe8ee57fe1ba1eb22ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs\" role=\"tablist\">
    <li role=\"presentation\" class=\"active\"><a href=\"#data\" aria-controls=\"data\" role=\"tab\" data-toggle=\"tab\">Customer Data</a></li>
    <li role=\"presentation\"><a href=\"#amended\" aria-controls=\"amended\" role=\"tab\" data-toggle=\"tab\">Amended Data</a></li>
</ul>

<div class=\"tab-content m-t-md\">
    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"data\">
        ";
        // line 8
        $this->loadTemplate("ami/customers/pending_action_data.html.twig", "ami/customers/pending_action_amended_data.html.twig", 8)->display($context);
        // line 9
        echo "    </div>
    <div role=\"tabpanel\" class=\"tab-pane\" id=\"amended\">
        <table class=\"table table-striped table-bordered table-hover tabletvo\">
            <thead>
                <tr>
                    <th>Field</th>
                    <th class=\"danger\">Old Value</th>
                    <th class=\"success\">New Value</th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 21
            echo "                    <tr>
                        <td>
                            ";
            // line 23
            if (($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "country_id", array(), "array") == 6)) {
                // line 24
                echo "                                ";
                if (($context["field"] == "cuisine_channels_id")) {
                    // line 25
                    echo "                                    ";
                    echo "Sub OG_name";
                    echo "
                                ";
                } elseif ((                // line 26
$context["field"] == "cuisine_channels_name")) {
                    // line 27
                    echo "                                    ";
                    echo "Sub OG_name";
                    echo "
                                ";
                } elseif ((                // line 28
$context["field"] == "cuisine_groups_id")) {
                    // line 29
                    echo "                                    ";
                    echo "OG_name";
                    echo "
                                ";
                } else {
                    // line 31
                    echo "                                    ";
                    echo twig_escape_filter($this->env, $context["field"], "html", null, true);
                    echo "
                                ";
                }
                // line 33
                echo "                            ";
            } else {
                // line 34
                echo "                                ";
                echo twig_escape_filter($this->env, $context["field"], "html", null, true);
                echo "
                            ";
            }
            // line 36
            echo "                        </td>
                        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "old_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "new_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "            </tbody>
        </table>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/customers/pending_action_amended_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 41,  98 => 38,  94 => 37,  91 => 36,  85 => 34,  82 => 33,  76 => 31,  70 => 29,  68 => 28,  63 => 27,  61 => 26,  56 => 25,  53 => 24,  51 => 23,  47 => 21,  43 => 20,  30 => 9,  28 => 8,  19 => 1,);
    }
}
