<?php

/* ami/tfo/edit.html.twig */
class __TwigTemplate_28b8e2c6226a4cff7b3320705c3f7a5c1bfa8cc8cf256bd7f96b3e7243d0f34b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/tfo/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/jquery-number/jquery.number.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/salespersons.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/stop_key.js"), "html", null, true);
        echo "\"></script>
    <script>

    function initProductAutocomplete(destroy) {

        if (destroy) {
            \$('.product-tokenfield').autocomplete(\"destroy\");
        }

        \$('.product-tokenfield').autocomplete({
            source: \"";
        // line 19
        echo twig_escape_filter($this->env, site_url("ami/products/tokenfield"), "html", null, true);
        echo "\",
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                var \$this = \$(this),
                        \$base = \$this.closest('.sku-base'),
                        \$input = \$base.find('.InputProductId'),
                        price = ui.item.price || 0,
                        pcsPrice = ui.item.pcsprice || 0,
                        \$qtyCase = \$base.find('.qty-case'),
                        \$qtyPcs = \$base.find('.qty-pcs'),
                        qtyCase = parseInt(\$qtyCase.val()) || 0,
                        qtyPcs = parseInt(\$qtyPcs.val()) || 0,
                        \$subtotal = \$base.find('.subtotal');

                if (price && pcsPrice) {
                    var subtotal = (price * qtyCase + pcsPrice * qtyPcs);
                    \$subtotal.val(\$.number(subtotal, 2));

                    caculateTotal();
                }

                \$this.val(ui.item.label);
                \$input.val(ui.item.value)
                        .attr('data-price', price)
                        .attr('data-pcsprice', pcsPrice);
            }
        }).on('focus', function () {
            this.select();
        });
    }

    function caculateTotal() {
        var \$total = \$('#total'),
        //total = \$.number(\$total.data('total'), 2) || 0;
                total = 0;

        \$('.subtotal').each(function () {
            var \$this = \$(this).number(true, 2);

            total += \$this.val() * 1;
        });

        if (\$total.data('init')) {
            \$total.data('init', 0);
        }
        else {
            \$total.val(\$.number(total, 2));
        }
    }


    \$(function () {
        document.onkeypress = stopRKey;
        \$(document).on('click', '.btn-clone', function (e) {
            e.preventDefault();

            var \$this = \$(this),
                    \$clone = \$(this).closest('.sku-base').clone(),
                    nextCounter = \$('.product-tokenfield').length;

            \$clone.find('.hide').removeClass('hide').show();

            if (\$this.hasClass('clone-add')) {
                \$new = \$clone.clone();

                \$new.find('input[name], select[name]').each(function () {
                    var \$this = \$(this);
                    \$this.attr('name', \$this.attr('name').replace(/\\[(\\d+)\\]/, '[' + nextCounter + ']'));

                    if (\$this.is('input:text')) {
                        \$this.val('');
                    }
                    else {
                        \$this.val(0);
                    }
                });

                \$new.appendTo(\$('#sku-base-block'));
            }

            if (\$this.hasClass('clone-remove')) {
                \$(this).closest('.sku-base').remove();

                caculateTotal();
            }

            initProductAutocomplete();
        });

        initProductAutocomplete();

        \$('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        \$('#InputWholesalers').autocomplete({
            source: \"";
        // line 116
        echo twig_escape_filter($this->env, site_url("ami/wholesalers/tokenfield"), "html", null, true);
        echo "\",
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                \$(this).val(ui.item.label);
                \$('#InputWholesalersId').val(ui.item.value);

                \$('#InputDistributors').val('');
                \$('#InputDistributorsId').val('');
            }
        }).on('focus', function () {
            this.select();
        });

        \$('#InputDistributors').autocomplete({
            source: \"";
        // line 131
        echo twig_escape_filter($this->env, site_url("ami/distributors/tokenfield"), "html", null, true);
        echo "\",
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                \$(this).val(ui.item.label);
                \$('#InputDistributorsId').val(ui.item.value);

                \$('#InputWholesalers').val('');
                \$('#InputWholesalersId').val('');
            }
        }).on('focus', function () {
            this.select();
        });

        \$('#InputCallRecords').autocomplete({
            source: \"";
        // line 146
        echo twig_escape_filter($this->env, site_url("ami/call_records/tokenfield"), "html", null, true);
        echo "\",
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                \$(this).val(ui.item.label);
                \$('#InputCallRecordsId').val(ui.item.value);
            }
        }).on('focus', function () {
            this.select();
        });

        \$('#InputCustomers').autocomplete({
            source: function (request, response) {
                \$.ajax({
                    url: \"";
        // line 160
        echo twig_escape_filter($this->env, site_url("ami/master_template/tokenfield"), "html", null, true);
        echo "\",
                    dataType: \"json\",
                    data: {
                        term: request.term,
                        salespersons: \$('#SelectSalesperson').find(\":selected\").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                \$(this).val(ui.item.label);
                \$('#InputCustomersId').val(ui.item.value);
            }
        }).on('focus', function () {
            this.select();
        });

        \$('#SelectSalesperson').on('change', function () {
            \$(\"#InputCustomers\").autocomplete(\"search\", \"\");
        });

        \$.listen('parsley:form:validate', function (formInstance) {

            window.ParsleyUI.removeError(\$('#InputWholesalers').parsley(), 'required', 'Either this value or Distributor value is required');
            window.ParsleyUI.removeError(\$('#InputDistributors').parsley(), 'required', 'Either this value or Wholesaler value is required');

            if (!\$('#InputWholesalers').val() && !\$('#InputDistributors').val()) {

                window.ParsleyUI.addError(\$('#InputWholesalers').parsley(), 'required', 'Either this value or Distributor value is required');
                window.ParsleyUI.addError(\$('#InputDistributors').parsley(), 'required', 'Either this value or Wholesaler value is required');

                formInstance.submitEvent.preventDefault();
            }
        });

        \$(document).on('change input', '.qty-case, .qty-pcs', function (e) {
            var \$this = \$(this),
                    \$base = \$this.closest('.sku-base'),
                    \$input = \$base.find('.InputProductId'),
                    price = \$input.data('price') || 0,
                    pcsPrice = \$input.data('pcsprice') || 0,
                    \$qtyCase = \$base.find('.qty-case'),
                    \$qtyPcs = \$base.find('.qty-pcs'),
                    qtyCase = parseInt(\$qtyCase.val()) || 0,
                    qtyPcs = parseInt(\$qtyPcs.val()) || 0,
                    \$subtotal = \$base.find('.subtotal');

            if (price && pcsPrice) {
                var subtotal = (price * qtyCase + pcsPrice * qtyPcs);
                \$subtotal.val(\$.number(subtotal, 2));

                caculateTotal();
            }
        });

        ";
        // line 219
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "products_array", array())) {
            // line 220
            echo "        \$('.product-tokenfield').each(function () {
            var \$this = \$(this),
                    \$base = \$this.closest('.sku-base'),
                    \$input = \$base.find('.InputProductId'),
                    price = \$input.data('price') || 0,
                    pcsPrice = \$input.data('pcsprice') || 0,
                    \$qtyCase = \$base.find('.qty-case'),
                    \$qtyPcs = \$base.find('.qty-pcs'),
                    qtyCase = parseInt(\$qtyCase.val()) || 0,
                    qtyPcs = parseInt(\$qtyPcs.val()) || 0,
                    \$subtotal = \$base.find('.subtotal');

            if (price && pcsPrice) {
                var subtotal = (price * qtyCase + pcsPrice * qtyPcs);
                \$subtotal.val(\$.number(subtotal, 2));

                caculateTotal();
            }
        });
        ";
        }
        // line 240
        echo "
        \$(document).on('change input', 'input[name=\"sku_number_\"], input[name=\"armstrong_2_customers_id_\"], input[name=\"armstrong_2_wholesalers_id_\"], input[name=\"armstrong_2_distributors_id_\"], input[name=\"armstrong_2_call_records_id_\"]', function (e) {
            var \$this = \$(this),
                    input_hidden = \$this.closest('div').find('input[type=\"hidden\"]');
            input_hidden.val(\$this.val());
        })

    });

    </script>
";
    }

    // line 252
    public function block_css($context, array $blocks = array())
    {
        // line 253
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\"
          href=\"";
        // line 255
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
";
    }

    // line 258
    public function block_content($context, array $blocks = array())
    {
        // line 259
        echo "
    ";
        // line 260
        echo form_open(site_url("ami/tfo/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_tfo_id" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_tfo_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Tfo</h1>

                <div class=\"text-right\">
                    ";
        // line 268
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/tfo/edit.html.twig", 268)->display(array_merge($context, array("url" => "ami/tfo", "id" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_tfo_id", array()), "permission" => "tfo")));
        // line 269
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 278
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 280
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_salespersons_id", array()), (("
                            class=\"form-control\" 
                            data-parsley-required=\"true\" 
                            id=\"SelectSalesperson\" 
                            data-url=\"" . site_url("ami/salespersons/fetch_customer")) . "\"
                        "));
        // line 285
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 290
        echo form_label("Customer", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 292
        echo form_input(array("name" => "armstrong_2_customers_id_", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputCustomers", "data-parsley-required" => "true"));
        // line 296
        echo "
                    <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"InputCustomersId\"
                           value=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_customers_id", array()), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 303
        echo form_label("Wholesaler", "armstrong_2_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 306
        echo "                    ";
        echo form_input(array("name" => "armstrong_2_wholesalers_id_", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_wholesalers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputWholesalers"));
        // line 309
        echo "
                    <input type=\"hidden\" name=\"armstrong_2_wholesalers_id\" id=\"InputWholesalersId\" value=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_wholesalers_id", array()), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 315
        echo form_label("Distributor", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 318
        echo "                    ";
        echo form_input(array("name" => "armstrong_2_distributors_id_", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_distributors_id", array()), array(), "array"), "class" => "form-control", "id" => "InputDistributors"));
        // line 320
        echo "
                    <input type=\"hidden\" name=\"armstrong_2_distributors_id\" id=\"InputDistributorsId\"
                           value=\"";
        // line 322
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_distributors_id", array()), "html", null, true);
        echo "\"/>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 326
        echo form_label("Call Record", "armstrong_2_call_records_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 329
        echo "                    ";
        echo form_input(array("name" => "armstrong_2_call_records_id_", "value" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_call_records_id", array()), "class" => "form-control", "id" => "InputCallRecords"));
        // line 332
        echo "
                    <input type=\"hidden\" name=\"armstrong_2_call_records_id\" id=\"InputCallRecordsId\"
                           value=\"";
        // line 334
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_call_records_id", array()), "html", null, true);
        echo "\"/>
                </div>
            </div>

            ";
        // line 338
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 339
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "products_array", array()));
        foreach ($context['_seq'] as $context["key"] => $context["tfo_product"]) {
            // line 340
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 341
            echo form_label("Product", "products", array("class" => "control-label col-sm-3"));
            echo "
                    <input type=\"hidden\" value=\"";
            // line 342
            echo twig_escape_filter($this->env, twig_jsonencode_filter($context["tfo_product"]), "html", null, true);
            echo "\" name=\"products[";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][productencode]\">
                    <div class=\"col-sm-6\">
                        ";
            // line 350
            echo "
                        ";
            // line 351
            echo form_input(array("name" => "sku_number_", "value" => (($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array")) ? (get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null))) : ("")), "class" => "form-control product-tokenfield"));
            // line 353
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 354
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"";
            // line 355
            echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "sku_number", array()), "html", null, true);
            echo "\"
                               data-price=\"";
            // line 356
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), "price", array()), "html", null, true);
            echo "\"
                               data-pcsprice=\"";
            // line 357
            echo twig_escape_filter($this->env, (((($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), "price", array()) > 0) && ($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), "quantity_case", array()) > 0))) ? (($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), "price", array()) / $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), "quantity_case", array()))) : (0)), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add hide\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                    ";
            // line 363
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 2, 1 => 9))) {
                // line 364
                echo "                        <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley checkbox\">
                            <label>
                                <input class=\"new-sku ";
                // line 366
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "newSKU", array()), "html", null, true);
                echo "\" ";
                if (((($this->getAttribute($context["tfo_product"], "newSKU", array()) . "") == "true") || (($this->getAttribute($context["tfo_product"], "newSKU", array()) . "") == "1"))) {
                    echo "checked=\"checked\"";
                }
                echo " type=\"checkbox\" name=\"products[";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "][newSKU]\"> NEW SKU
                            </label>
                        </div>
                    ";
            }
            // line 370
            echo "                    <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
                        Qty
                        case ";
            // line 372
            echo form_input(array("name" => (("products[" . $context["key"]) . "][qty_case]"), "value" => $this->getAttribute($context["tfo_product"], "qty_case", array()), "class" => "form-control sub-input qty-case", "maxlength" => 3));
            echo "
                        &nbsp;
                        Qty
                        pcs ";
            // line 375
            echo form_input(array("name" => (("products[" . $context["key"]) . "][qty_piece]"), "value" => $this->getAttribute($context["tfo_product"], "qty_piece", array()), "class" => "form-control sub-input qty-pcs", "maxlength" => 3));
            echo "
                        &nbsp;
                        Free
                        case ";
            // line 378
            echo form_input(array("name" => (("products[" . $context["key"]) . "][free_case]"), "value" => $this->getAttribute($context["tfo_product"], "free_case", array()), "class" => "form-control sub-input", "maxlength" => 3));
            echo "
                        &nbsp;
                        Free
                        pcs ";
            // line 381
            echo form_input(array("name" => (("products[" . $context["key"]) . "][free_piece]"), "value" => $this->getAttribute($context["tfo_product"], "free_piece", array()), "class" => "form-control sub-input", "maxlength" => 3));
            echo "
                        &nbsp;
                        Subtotal ";
            // line 383
            echo form_input(array("name" => (("products[" . $context["key"]) . "][price]"), "class" => "form-control sub-input subtotal", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['tfo_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 387
        echo "
            ";
        // line 388
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 389
            echo "                ";
            $context["counter"] = twig_length_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "products_array", array()));
            // line 390
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 391
            echo form_label("Product", "products", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 399
            echo "                        ";
            echo form_input(array("name" => "sku_number_", "value" => null, "class" => "form-control product-tokenfield"));
            // line 401
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 402
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\" value=\"\"
                               data-price=\"0\" data-pcsprice=\"0\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                    ";
            // line 409
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 2, 1 => 9))) {
                // line 410
                echo "                        <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley checkbox\">
                            <label>
                                <input class=\"new-sku\" type=\"checkbox\" name=\"products[";
                // line 412
                echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
                echo "][newSKU]\"> NEW SKU
                            </label>
                        </div>
                    ";
            }
            // line 416
            echo "                    <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
                        ";
            // line 417
            if (((isset($context["function_method"]) ? $context["function_method"] : null) == "add")) {
                // line 418
                echo "                            Qty case ";
                echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][qty_case]"), "class" => "form-control sub-input qty-case", "maxlength" => 3, "data-parsley-required" => "true"));
                echo "&nbsp;
                            Qty pcs ";
                // line 419
                echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][qty_piece]"), "class" => "form-control sub-input qty-pcs", "maxlength" => 3, "data-parsley-required" => "true"));
                echo "&nbsp;
                        ";
            } else {
                // line 421
                echo "                            Qty case ";
                echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][qty_case]"), "class" => "form-control sub-input qty-case", "maxlength" => 3));
                echo "&nbsp;
                            Qty pcs ";
                // line 422
                echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][qty_piece]"), "class" => "form-control sub-input qty-pcs", "maxlength" => 3));
                echo "&nbsp;
                        ";
            }
            // line 424
            echo "                        Free
                        case ";
            // line 425
            echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][free_case]"), "class" => "form-control sub-input", "maxlength" => 3));
            echo "
                        &nbsp;
                        Free
                        pcs ";
            // line 428
            echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][free_piece]"), "class" => "form-control sub-input", "maxlength" => 3));
            echo "
                        &nbsp;
                        Subtotal ";
            // line 430
            echo form_input(array("name" => "", "class" => "form-control sub-input subtotal", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
                <div id=\"sku-base-block\"></div>
            ";
        }
        // line 435
        echo "
            <div class=\"form-group\">
                ";
        // line 437
        echo form_label("Total", "total", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 439
        echo form_input(array("name" => "total", "value" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "total", array()), "class" => "form-control", "id" => "total", "readonly" => "readonly", "data-total" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "total", array()), "data-init" => 1));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 444
        echo form_label("Method", "method", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 446
        echo form_dropdown("method", (isset($context["tfo_method"]) ? $context["tfo_method"] : null), $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "method", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 451
        echo form_label("Delivery Date", "delivery_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 453
        echo form_input(array("name" => "delivery_date", "value" => $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "delivery_date", array())), "method"), "toDateString", array(), "method"), "class" => "form-control datetimepicker"));
        // line 455
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 460
        echo form_label("Remarks", "remarks", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 462
        echo form_textarea(array("name" => "remarks", "value" => $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "remarks", array()), "class" => "form-control", "rows" => 3));
        echo "
                </div>
            </div>
        </div>
    </div>

    ";
        // line 468
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/tfo/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  716 => 468,  707 => 462,  702 => 460,  695 => 455,  693 => 453,  688 => 451,  680 => 446,  675 => 444,  667 => 439,  662 => 437,  658 => 435,  650 => 430,  645 => 428,  639 => 425,  636 => 424,  631 => 422,  626 => 421,  621 => 419,  616 => 418,  614 => 417,  611 => 416,  604 => 412,  600 => 410,  598 => 409,  588 => 402,  585 => 401,  582 => 399,  577 => 391,  574 => 390,  571 => 389,  569 => 388,  566 => 387,  556 => 383,  551 => 381,  545 => 378,  539 => 375,  533 => 372,  529 => 370,  516 => 366,  512 => 364,  510 => 363,  501 => 357,  497 => 356,  493 => 355,  489 => 354,  486 => 353,  484 => 351,  481 => 350,  474 => 342,  470 => 341,  467 => 340,  462 => 339,  460 => 338,  453 => 334,  449 => 332,  446 => 329,  441 => 326,  434 => 322,  430 => 320,  427 => 318,  422 => 315,  414 => 310,  411 => 309,  408 => 306,  403 => 303,  395 => 298,  391 => 296,  389 => 292,  384 => 290,  377 => 285,  370 => 280,  365 => 278,  354 => 269,  352 => 268,  341 => 260,  338 => 259,  335 => 258,  329 => 255,  323 => 253,  320 => 252,  306 => 240,  284 => 220,  282 => 219,  220 => 160,  203 => 146,  185 => 131,  167 => 116,  67 => 19,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
