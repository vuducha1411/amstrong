<?php

/* ami/call_records/index.html.twig */
class __TwigTemplate_258988e0651b53e4a29153e15ace19fad615733e57a40aa842e51834af5a5dc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/call_records/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/call_modules.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 17
        echo twig_escape_filter($this->env, site_url("ami/call_records/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    // \$('#Generate').click(function(e) {
    //     xhr = null;
    //     var \$startDate = \$('#InputStartDate'),
    //         \$endDate = \$('#InputEndDate'),
    //         \$container = \$('#CallRecordsContainer');

    //     if (xhr) {
    //         xhr.abort();
    //     }

    //     if (!xhr && \$startDate.val() && \$endDate.val()) {
            
    //         \$container.html('').empty().html('<div class=\"col-sm-6 col-sm-offset-3\"><i class=\"fa fa-spinner fa-spin\"></i> Loading...</div>');

    //         \$.ajax({
    //             url: \"";
        // line 41
        echo twig_escape_filter($this->env, site_url("ami/call_records/records"), "html", null, true);
        echo "\",
    //             type: 'post',
    //             data: { start_date: \$startDate.val(), end_date: \$endDate.val() , draft: ";
        // line 43
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo " },
    //             success: function(res) {
    //                 try {
    //                     var resJson = JSON.parse(res);
    //                     if (resJson._session_expried) {
    //                         window.location.reload();
    //                         return;
    //                     }
                        
    //                 } catch(e) {}
                    
    //                 \$container.html('').empty();
    //                 if (res) {
    //                     \$container.append(res);
    //                     CheckAll(\$('input.CheckAll'));
    //                 }

    //                 xhr = null;
    //             }
    //         });
    //     }
    // });

    \$('#InputDateRange').daterangepicker({
        format: 'YYYY-MM-DD'
    }).on('apply.daterangepicker', function(ev, picker) {
        \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD'));
        \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD'));
    });
});
</script>
";
    }

    // line 76
    public function block_css($context, array $blocks = array())
    {
        // line 77
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 82
    public function block_content($context, array $blocks = array())
    {
        // line 83
        echo "
    ";
        // line 84
        echo form_open(site_url("ami/call_records/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 89
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Call Records Draft") : ("Call Records"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 92
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 94
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/call_records/index.html.twig", 94)->display(array_merge($context, array("url" => "ami/call_records", "icon" => "fa-coffee", "title" => "Call Records", "permission" => "call_records", "showImport" => 1)));
        // line 95
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            ";
        // line 147
        echo "
            ";
        // line 148
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 149
            echo "                <div class=\"form-group\">
                    ";
            // line 150
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 152
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"SalespersonsSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 156
        echo "            
            <div class=\"form-group\">
                ";
        // line 158
        echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 160
        echo form_input(array("name" => "date_time", "value" => null, "class" => "form-control", "id" => "InputDateRange"));
        // line 163
        echo "
                </div>

                <input type=\"hidden\" id=\"InputStartDate\" value=\"\" />
                <input type=\"hidden\" id=\"InputEndDate\" value=\"\" />
            </div>

            ";
        // line 175
        echo "
            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\" 
                        data-url=\"";
        // line 179
        echo twig_escape_filter($this->env, site_url("ami/call_records/records"), "html", null, true);
        echo "\"
                        data-container=\"#CallRecordsContainer\"
                        data-draft=\"";
        // line 181
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo "\">Generate</a>
                </div>
            </div>

            <div id=\"CallRecordsContainer\"></div>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 191
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/call_records/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 191,  244 => 181,  239 => 179,  233 => 175,  224 => 163,  222 => 160,  217 => 158,  213 => 156,  206 => 152,  201 => 150,  198 => 149,  196 => 148,  193 => 147,  182 => 95,  180 => 94,  175 => 92,  169 => 89,  161 => 84,  158 => 83,  155 => 82,  149 => 79,  145 => 78,  141 => 77,  138 => 76,  102 => 43,  97 => 41,  70 => 17,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
