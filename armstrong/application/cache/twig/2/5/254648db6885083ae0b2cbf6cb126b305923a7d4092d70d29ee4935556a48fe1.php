<?php

/* ami/setting_game/setting_spin.html.twig */
class __TwigTemplate_254648db6885083ae0b2cbf6cb126b305923a7d4092d70d29ee4935556a48fe1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_game/setting_spin.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 13
        echo twig_escape_filter($this->env, site_url("ami/setting_game/integrate_with_applications/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'desc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 25
    public function block_css($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "    Total spin: ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prizes"]) ? $context["prizes"] : null), "totalspin", array(), "array"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/setting_spin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 31,  81 => 30,  75 => 27,  70 => 26,  67 => 25,  52 => 13,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
