<?php

/* ami/distributors/preview.html.twig */
class __TwigTemplate_2f507aebe6628f230f410b2317c818ca4ed6e4ef1256880ac872d6ce7e3d5df8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-map-marker pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <div class=\"list-group-item-text\">
                                <h4 class=\"list-group-item-heading text-muted\">Location</h4>
                                <p><span class=\"text-muted\">Street Address:</span> ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "street_address", array()), "html", null, true);
        echo "</p>
                                <p><span class=\"text-muted\">City:</span> ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "city", array()), "html", null, true);
        echo "</p>
                                ";
        // line 18
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "postal_code", array())) {
            // line 19
            echo "                                    <p><span class=\"text-muted\">Postal Code:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "postal_code", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 21
        echo "                                ";
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "region", array())) {
            // line 22
            echo "                                    <p><span class=\"text-muted\">Region:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "region", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 24
        echo "                                ";
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "state", array())) {
            // line 25
            echo "                                    <p><span class=\"text-muted\">State:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "state", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 27
        echo "                                ";
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "district", array())) {
            // line 28
            echo "                                    <p><span class=\"text-muted\">District:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "district", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 30
        echo "                                ";
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "suburb", array())) {
            // line 31
            echo "                                    <p><span class=\"text-muted\">Suburb:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "suburb", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 33
        echo "                                ";
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "province", array())) {
            // line 34
            echo "                                    <p><span class=\"text-muted\">Province:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "province", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 36
        echo "                            </div>
                        </div>
                    </div>

                    ";
        // line 40
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email", array())) {
            // line 41
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Email</h4>
                                <p class=\"list-group-item-text\"><a href=\"mailto:";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email", array()), "html", null, true);
            echo "</a></p>
                            </div>
                        </div>
                    ";
        }
        // line 49
        echo "
                    ";
        // line 50
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "salesperson_name", array())) {
            // line 51
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-usd pull-left\"></i>
                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Salesperson Name</h4>
                                <p class=\"list-group-item-text\">";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "salesperson_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 59
        echo "
                    ";
        // line 60
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone", array())) {
            // line 61
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-phone pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Phone</h4>
                                <p class=\"list-group-item-text\">";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone_2", array())) ? ((" - " . $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone_2", array()))) : ("")), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 69
        echo "
                    ";
        // line 70
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "fax", array())) {
            // line 71
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-fax pull-left\"></i>
                            <div class=\"m-l-lg\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Fax</h4>
                                <p class=\"list-group-item-text\">";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "fax", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 79
        echo "
                    ";
        // line 80
        if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "contact_details", array())) {
            // line 81
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-info-circle pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Contact Details</h4>
                                <p class=\"list-group-item-text\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "contact_details", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 89
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 94
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "distributor"))) {
            // line 95
            echo "                ";
            echo html_btn(site_url(("ami/distributors/edit/" . $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 97
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/distributors/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 97,  208 => 95,  206 => 94,  199 => 89,  192 => 85,  186 => 81,  184 => 80,  181 => 79,  174 => 75,  168 => 71,  166 => 70,  163 => 69,  154 => 65,  148 => 61,  146 => 60,  143 => 59,  136 => 55,  130 => 51,  128 => 50,  125 => 49,  116 => 45,  110 => 41,  108 => 40,  102 => 36,  96 => 34,  93 => 33,  87 => 31,  84 => 30,  78 => 28,  75 => 27,  69 => 25,  66 => 24,  60 => 22,  57 => 21,  51 => 19,  49 => 18,  45 => 17,  41 => 16,  25 => 5,  19 => 1,);
    }
}
