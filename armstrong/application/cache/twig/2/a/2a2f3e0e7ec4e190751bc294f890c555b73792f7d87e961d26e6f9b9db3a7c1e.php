<?php

/* ami/master_template/template.html.twig */
class __TwigTemplate_2a2f3e0e7ec4e190751bc294f890c555b73792f7d87e961d26e6f9b9db3a7c1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["templates"]) ? $context["templates"] : null)) {
            echo " 

    <ul class=\"nav nav-tabs\" role=\"tablist\">
        ";
            // line 4
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["templates"]) ? $context["templates"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["masterId"] => $context["master"]) {
                // line 5
                echo "            <li role=\"presentation\" class=\"";
                echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
                echo "\"><a href=\"#tb_";
                echo twig_escape_filter($this->env, $context["masterId"], "html", null, true);
                echo "\" role=\"tab\" data-toggle=\"tab\">Template ";
                echo twig_escape_filter($this->env, $context["masterId"], "html", null, true);
                echo "</a></li>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['masterId'], $context['master'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "    </ul>

    <div class=\"tab-content m-t\">
        ";
            // line 10
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["templates"]) ? $context["templates"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["masterId"] => $context["master"]) {
                // line 11
                echo "            <div role=\"tabpanel\" class=\"tab-pane ";
                echo (($this->getAttribute($context["loop"], "first", array())) ? ("active") : (""));
                echo "\" id=\"tb_";
                echo twig_escape_filter($this->env, $context["masterId"], "html", null, true);
                echo "\">

                <div class=\"panel panel-default\">                                    
                    <div class=\"panel-body\">      
                        <div class=\"\">
                            <table class=\"table table-striped table-bordered table-hover data-tables\">
                                <thead>
                                    <tr>
                                        ";
                // line 19
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan_master_template"))) {
                    // line 20
                    echo "                                            <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                        ";
                }
                // line 22
                echo "                                        <th>ID</th>
                                        <th>Salesperson Name</th>
                                        <th>Operator</th>
                                        <th>Planned Date</th>
                                        <th>Created</th>
                                        <th>Date Sync</th>
                                        <th class=\"nosort text-center\"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    ";
                // line 32
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($context["master"]);
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["template"]) {
                    // line 33
                    echo "                                        <tr>
                                            ";
                    // line 34
                    if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan_master_template"))) {
                        // line 35
                        echo "                                                <td class=\"text-center\">";
                        echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["template"], "id", array())));
                        echo "</td>
                                            ";
                    }
                    // line 37
                    echo "                                            <td><a href=\"";
                    echo twig_escape_filter($this->env, site_url(("ami/master_template/edit/" . $this->getAttribute($context["template"], "id", array()))), "html", null, true);
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "id", array()), "html", null, true);
                    echo "\" data-toggle=\"ajaxModal\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "id", array()), "html", null, true);
                    echo "</a></td>
                                            <td>";
                    // line 38
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "armstrong_2_salespersons_id", array()), "html", null, true);
                    echo " - ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "first_name", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "last_name", array()), "html", null, true);
                    echo "</td>
                                            <td>";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "armstrong_2_customers_id", array()), "html", null, true);
                    echo " - ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "armstrong_2_customers_name", array()), "html", null, true);
                    echo "</td>
                                            <td class=\"text-center\">";
                    // line 40
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "planned_day", array()), "html", null, true);
                    echo "</td>
                                            <td class=\"text-center\">";
                    // line 41
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "date_created", array()), "html", null, true);
                    echo "</td>
                                            <td class=\"text-center\">";
                    // line 42
                    echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "date_sync", array()), "html", null, true);
                    echo "</td>
                                            <td class=\"text-center\" style=\"min-width: 80px;\">
                                                ";
                    // line 44
                    if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "route_plan_master_template")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan_master_template")))) {
                        // line 45
                        echo "                                                    ";
                        $this->loadTemplate("ami/components/table_btn.html.twig", "ami/master_template/template.html.twig", 45)->display(array_merge($context, array("url" => "ami/master_template", "id" => $this->getAttribute($context["template"], "id", array()), "permission" => "route_plan")));
                        // line 46
                        echo "                                                ";
                    }
                    // line 47
                    echo "                                            </td>
                                        </tr>
                                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['template'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 50
                echo "                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['masterId'], $context['master'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('.data-tables').dataTable({
            'order': [[1, 'asc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });        
    });  
    </script>
";
        } else {
            // line 75
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no route plan master template</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/master_template/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 75,  226 => 58,  205 => 50,  189 => 47,  186 => 46,  183 => 45,  181 => 44,  176 => 42,  172 => 41,  168 => 40,  162 => 39,  154 => 38,  145 => 37,  139 => 35,  137 => 34,  134 => 33,  117 => 32,  105 => 22,  101 => 20,  99 => 19,  85 => 11,  68 => 10,  63 => 7,  42 => 5,  25 => 4,  19 => 1,);
    }
}
