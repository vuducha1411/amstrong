<?php

/* ami/questions/index.html.twig */
class __TwigTemplate_2928ad91b267309814edd90dc97c9341b8474da438a859ed9691dea2a0cb50be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/questions/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 12
        echo twig_escape_filter($this->env, site_url("ami/questions/ajaxData"), "html", null, true);
        echo "\",*/
              //  \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
               // 'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
        var flagsord = true;
        \$(document).on('keyup', '#dataTables_filter input[type=\"search\"]', function(){
            if(\$(this).val() != '')
            {
                \$('.saveaction').hide();
                flagsord = false;
                \$('.sortquestion').sortable('disable');
            }
            else
            {
                \$('.saveaction').show();
                flagsord = true;
                \$( \".sortquestion\" ).sortable();
                \$( \".sortquestion\" ).sortable( \"option\", \"disabled\", false );
                // ^^^ this is required otherwise re-enabling sortable will not work!
                \$( \".sortquestion\" ).disableSelection();
                return false;
            }
        });
        \$('.editquestion').click(function(){
            var idquestion = \$(this).data('id');
            \$.ajax({
                url: \"";
        // line 53
        echo twig_escape_filter($this->env, site_url("ami/questions/edit/"), "html", null, true);
        echo "/\"+idquestion,
                type: 'get',
                dataType: 'json',
                success: function (json)
                {
                    if(json.id)
                    {
                        \$('#myModal input[name=\"title\"]').val(json.title);
                        \$('#myModal input[name=\"id\"]').val(json.id);
                        \$('#myModal input[name=\"app_type\"][value=\"'+json.app_type+'\"]').click();
                        \$('#myModal input[name=\"parent_id\"]').val(json.parent_id);
                        \$('#myModal input[name=\"type_question\"]').val(json.type_question);
                        \$('#myModal input[name=\"require\"][value=\"'+json.require+'\"]').click();
                        \$('#myModal').modal('show');
                    }
                    else
                    {
                        alert(json)
                    }
                }
            });
            return false;
        });
        \$('.addaction').click(function(){
            \$('#myModal input[name=\"title\"]').val('');
            \$('#myModal input[name=\"id\"]').val('');
            \$('#myModal input[name=\"app_type\"][value=\"0\"]').click();
            \$('#myModal input[name=\"parent_id\"]').val('";
        // line 80
        echo twig_escape_filter($this->env, (isset($context["parent_id"]) ? $context["parent_id"] : null), "html", null, true);
        echo "');
            \$('#myModal input[name=\"type_question\"]').val('";
        // line 81
        echo (((isset($context["parent_id"]) ? $context["parent_id"] : null)) ? (1) : (0));
        echo "');
            \$('#myModal input[name=\"require\"][value=\"0\"]').click();
            \$('#myModal').modal('show');
        });
        function updateOrder()
        {
            \$.ajax({
                url: \"";
        // line 88
        echo twig_escape_filter($this->env, site_url("ami/questions/update/"), "html", null, true);
        echo "/\",
                type: 'POST',
                beforeSend: function () {
                    \$('.loaddingicon').show();
                },
                complete: function () {
                    \$('.loaddingicon').hide();
                },
                dataType: 'json',
                data: \$('.sortquestion input[type=\"checkbox\"]'),
                success: function (json)
                {

                }
            });
        }
        \$(\".sortquestion\").sortable({
            update: function(event, ui) {
                updateOrder();
            }
        }).disableSelection();
    </script>
";
    }

    // line 112
    public function block_css($context, array $blocks = array())
    {
        // line 113
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 114
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <style>
        .sortquestion .center {
            cursor: move;
        }
        .ui-sortable-disabled .center {
            cursor: default !important;
        }
        .nosort::after{
            display: none;
        }
    </style>
";
    }

    // line 128
    public function block_content($context, array $blocks = array())
    {
        // line 129
        echo "
    ";
        // line 130
        echo form_open(site_url("ami/questions/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 135
        echo twig_escape_filter($this->env, (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Questions Draft") : ((isset($context["tittle_page"]) ? $context["tittle_page"] : null))), "html", null, true);
        echo "</h1>

                <div class=\"text-right\">
                    ";
        // line 138
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("view", (isset($context["permission"]) ? $context["permission"] : null)))) {
            // line 139
            echo "                        <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                            ";
            // line 140
            echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger hide\" nam=\"delete\"  id=\"CheckAllBtn\""));
            echo "
                            ";
            // line 141
            if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
                // line 142
                echo "                                <a class=\"btn btn-sm btn-default\" href=\"";
                echo twig_escape_filter($this->env, site_url("ami/questions/index"), "html", null, true);
                echo "\"><i class=\"fa fa-fw fa-ban\"></i> Cancel</a>
                            ";
            }
            // line 144
            echo "                            ";
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null)))) {
                // line 145
                echo "                                <button type=\"button\" class=\"btn btn-sm btn-success addaction\">
                                    <i class=\"fa fw fa-plus\"></i> Add
                                </button>
                                ";
                // line 149
                echo "                                ";
                // line 150
                echo "                            ";
            }
            // line 151
            echo "                        </div>
                    ";
        }
        // line 153
        echo "
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    ";
        // line 160
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 161
            echo "        <!--div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"dropdown\">
                    <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                            id=\"dropdownFilter\" data-toggle=\"dropdown\">
                        Filter: ";
            // line 166
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                        <li role=\"presentation\" ";
            // line 170
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                    role=\"menuitem\" tabindex=\"-1\"
                                    href=\"";
            // line 172
            echo twig_escape_filter($this->env, site_url("ami/questions"), "html", null, true);
            echo "\">All</a></li>
                        <li role=\"presentation\" ";
            // line 173
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "approved")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 175
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=pull"), "html", null, true);
            echo "\">Pull</a>
                        </li>
                        <li role=\"presentation\" ";
            // line 177
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 179
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=push"), "html", null, true);
            echo "\">Push</a>
                        </li>
                        <li role=\"presentation\" ";
            // line 181
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "reject")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 183
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=leader"), "html", null, true);
            echo "\">Leader</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div-->
    ";
        }
        // line 190
        echo "    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">

                            <thead>
                            <tr>
                                ";
        // line 200
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions"))) {
            // line 201
            echo "                                    <th style=\"width:30px;\" class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 205
        echo "                                <th style=\"width:30px;\" class=\"nosort\">Order</th>
                                <th class=\"nosort\">Title</th>
                                <th style=\"width:80px;\" class=\"nosort\">App Type</th>
                                ";
        // line 208
        if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
            // line 209
            echo "                                ";
        } else {
            // line 210
            echo "                                    <th style=\"width:80px;\" class=\"nosort\">Total question</th>
                                ";
        }
        // line 212
        echo "                                <th style=\"width:80px;\" class=\"nosort\">Type</th>
                                ";
        // line 214
        echo "                                <th style=\"width:120px;\" class=\"nosort\">Updated</th>
                                <th style=\"width:80px;\" class=\"nosort text-center\">
                                    <img width=\"30px\" style=\"display: none; \" class=\"loaddingicon\" src=\"";
        // line 216
        echo twig_escape_filter($this->env, site_url("res/img"), "html", null, true);
        echo "/loading.gif\">
                                </th>
                            </tr>
                            </thead>
                            <tbody class=\"sortquestion\">
                            ";
        // line 221
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 222
            echo "                                <tr>
                                    ";
            // line 223
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions"))) {
                // line 224
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 226
            echo "                                    ";
            // line 227
            echo "                                    <td class=\"center\">
                                        <span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
                                    </td>
                                    <td class=\"center\">
                                        ";
            // line 231
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "questions"))) {
                // line 232
                echo "                                            ";
                if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
                    // line 233
                    echo "                                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                    echo "
                                            ";
                } else {
                    // line 235
                    echo "                                                <a href=\"";
                    echo twig_escape_filter($this->env, site_url(((("ami/questions/index/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                    echo "</a>
                                            ";
                }
                // line 237
                echo "                                        ";
            } else {
                // line 238
                echo "                                            ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "
                                        ";
            }
            // line 239
            echo "</td>
                                    <td class=\"center\">";
            // line 240
            if (($this->getAttribute($context["post"], "app_type", array()) == 0)) {
                echo "  Pull
                                        ";
            } elseif (($this->getAttribute(            // line 241
$context["post"], "app_type", array()) == 1)) {
                echo " Push
                                        ";
            } else {
                // line 242
                echo "  Leader
                                        ";
            }
            // line 244
            echo "                                    </td>
                                    ";
            // line 245
            if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
                // line 246
                echo "                                    ";
            } else {
                // line 247
                echo "                                        <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "total_question", array()), "html", null, true);
                echo " questions</td>
                                    ";
            }
            // line 249
            echo "                                    <td class=\"center\">";
            echo ((($this->getAttribute($context["post"], "parent_id", array()) == 0)) ? ("Category") : ("Question"));
            echo "</td>
                                    ";
            // line 251
            echo "                                    <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 255
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "questions"))) {
                // line 256
                echo "                                                ";
                echo html_btn(site_url(((("ami/questions/index/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit editquestion", "data-id" => $this->getAttribute($context["post"], "id", array()), "title" => "Edit"));
                echo "

                                            ";
            }
            // line 259
            echo "
                                            ";
            // line 260
            if (((isset($context["filter"]) ? $context["filter"] : null) == "pending")) {
                // line 261
                echo "                                                ";
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
                    // line 262
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/questions/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                    // line 263
                    echo html_btn(site_url(("ami/questions/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 265
                echo "                                            ";
            }
            // line 266
            echo "
                                            ";
            // line 267
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 268
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 269
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/questions/restore/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 271
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/questions/delete/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 273
                echo "                                            ";
            }
            // line 274
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 278
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 282
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 283
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/questions/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/questions/draft")));
            // line 284
            echo "                                ";
        } else {
            // line 285
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/questions/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/questions")));
            // line 286
            echo "                                ";
        }
        // line 287
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    ";
        // line 300
        echo form_close();
        echo "
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Add ";
        // line 306
        echo twig_escape_filter($this->env, (isset($context["tittle_page"]) ? $context["tittle_page"] : null), "html", null, true);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    ";
        // line 309
        echo form_open(site_url("ami/questions/save"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "
                    <div class=\"form-group\">
                        ";
        // line 311
        echo form_label("Title", "title", array("class" => "control-label"));
        echo "
                        ";
        // line 312
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                    </div>
                    <div class=\"form-group select_app_type ";
        // line 314
        if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
            echo "hide";
        }
        echo "\">
                        ";
        // line 315
        echo form_label("Type", "Type", array("class" => "control-label"));
        echo "
                        <div class=\"no-parsley\">
                            <label class=\"radio-inline\">";
        // line 317
        echo form_radio("app_type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 0));
        echo " Pull</label>&nbsp;
                            <label class=\"radio-inline\">";
        // line 318
        echo form_radio("app_type", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 1));
        echo " Push</label>&nbsp;
                            <label class=\"radio-inline\">";
        // line 319
        echo form_radio("app_type", 2, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 2));
        echo " Leader</label>
                        </div>
                    </div>
                    <div class=\"form-group ";
        // line 322
        if ((isset($context["isquestion"]) ? $context["isquestion"] : null)) {
        } else {
            echo "hide";
        }
        echo "\">
                        ";
        // line 323
        echo form_label("Require", "Require", array("class" => "control-label"));
        echo "
                        <div class=\"no-parsley\">
                            <label class=\"radio-inline\">";
        // line 325
        echo form_radio("require", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "require", array()) == 1));
        echo " Yes</label>&nbsp;
                            <label class=\"radio-inline\">";
        // line 326
        echo form_radio("require", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "require", array()) == 0));
        echo " No</label>&nbsp;
                        </div>
                    </div>
                    <input type=\"hidden\" name=\"type_question\" value=\"";
        // line 329
        echo (((isset($context["parent_id"]) ? $context["parent_id"] : null)) ? (1) : (0));
        echo "\">
                    <input type=\"hidden\" name=\"parent_id\" value=\"";
        // line 330
        echo twig_escape_filter($this->env, (isset($context["parent_id"]) ? $context["parent_id"] : null), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"id\" value=\"\">
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>
                </div>
                ";
        // line 337
        echo form_close();
        echo "
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/questions/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  646 => 337,  636 => 330,  632 => 329,  626 => 326,  622 => 325,  617 => 323,  610 => 322,  604 => 319,  600 => 318,  596 => 317,  591 => 315,  585 => 314,  580 => 312,  576 => 311,  571 => 309,  565 => 306,  556 => 300,  539 => 287,  536 => 286,  533 => 285,  530 => 284,  527 => 283,  525 => 282,  519 => 278,  510 => 274,  507 => 273,  501 => 271,  495 => 269,  492 => 268,  490 => 267,  487 => 266,  484 => 265,  479 => 263,  474 => 262,  471 => 261,  469 => 260,  466 => 259,  459 => 256,  457 => 255,  449 => 251,  444 => 249,  438 => 247,  435 => 246,  433 => 245,  430 => 244,  426 => 242,  421 => 241,  417 => 240,  414 => 239,  408 => 238,  405 => 237,  397 => 235,  391 => 233,  388 => 232,  386 => 231,  380 => 227,  378 => 226,  372 => 224,  370 => 223,  367 => 222,  363 => 221,  355 => 216,  351 => 214,  348 => 212,  344 => 210,  341 => 209,  339 => 208,  334 => 205,  328 => 201,  326 => 200,  314 => 190,  304 => 183,  299 => 181,  294 => 179,  289 => 177,  284 => 175,  279 => 173,  275 => 172,  270 => 170,  263 => 166,  256 => 161,  254 => 160,  245 => 153,  241 => 151,  238 => 150,  236 => 149,  231 => 145,  228 => 144,  222 => 142,  220 => 141,  216 => 140,  213 => 139,  211 => 138,  205 => 135,  197 => 130,  194 => 129,  191 => 128,  174 => 114,  169 => 113,  166 => 112,  139 => 88,  129 => 81,  125 => 80,  95 => 53,  51 => 12,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
