<?php

/* ami/reports/custom/penetration_customers_report.twig.html */
class __TwigTemplate_26e04e7c7896d576931add2bcbe0cc08e93c209ff8e15b5912d5c61729939f63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("ami/reports/custom/filter.twig.html", "ami/reports/custom/penetration_customers_report.twig.html", 1)->display(array_merge($context, array("channel" => 1, "otm" => 1, "leaderAndPerson" => 1)));
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/penetration_customers_report.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 6,  19 => 1,);
    }
}
