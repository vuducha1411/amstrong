<?php

/* ami/reports/index.html.twig */
class __TwigTemplate_265142447944f7022c16ece77d1fa72af7b7313abd86d9b076433de7318fe089 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/reports/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportLabels"]) ? $context["reportLabels"] : null), (isset($context["action"]) ? $context["action"] : null), array(), "array"), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_js($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/date.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/generateDataReport.js?"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(twig_date_converter($this->env), "timestamp", array()), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/generateDataReport2.js?"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(twig_date_converter($this->env), "timestamp", array()), "html", null, true);
        echo "\"></script>
    <script src=\"https://www.gstatic.com/charts/loader.js\"></script>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/js/googlechartsapi.js"), "html", null, true);
        echo "\"></script>


    <script>
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(loadCustomerProfilingReport);

        function getInternetExplorerVersion()
        // Returns the version of Internet Explorer or a -1
        // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp(\"MSIE ([0-9]{1,}[\\.0-9]{0,})\");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.\$1);
            }
            // 7 - ie7
            // 8 - ie 8
            // 9 -ie 9
            // 10 -ie 10
            //>-1 >10
            return rv;
        }
        function checkVersion() {
            var msg = \"You're not using Internet Explorer.\";
            var ver = getInternetExplorerVersion();

            if (ver > -1) {
                if (ver >= 8.0)
                    msg = \"You're using a recent copy of Internet Explorer.\"
                else
                    msg = \"You should upgrade your copy of Internet Explorer.\";
            }
            alert(msg + ' ' + ver);
        }

        function zoom_report() {
            \$('#ReportsGeneration').toggle();
            \$(\"#reporting-view #iframe_report\").toggleClass('height_zoom');
        }
        function get_sale(value) {
            \$('#armstrong_2_salespersons_id').html('');
            \$.ajax({
                type: 'POST',
                url: base_url() + 'ami/reports/penetration_customers_report/' + value,
                data: {},
                success: function (val) {
                    \$('#armstrong_2_salespersons_id').html(val);
                }
            })

        }

        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        ";
        // line 75
        if ((($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking_detail") || ($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking"))) {
            // line 76
            echo "            \$('input[name=from]').parents('.form-group').hide();
            \$('input[name=to]').parents('.form-group').hide();
            \$('#input-date-type').on('change',function(){
                if(\$(this).val() == 0){
                    \$('input[name=from]').parents('.form-group').hide();
                    \$('input[name=to]').parents('.form-group').hide();
                    \$('select[name=season]').parents('.form-group').show();
                }else{
                    \$('input[name=from]').parents('.form-group').show();
                    \$('input[name=to]').parents('.form-group').show();
                    \$('select[name=season]').parents('.form-group').hide();
                }
            })
        ";
        }
        // line 90
        echo "

        ";
        // line 92
        if ((isset($context["showtopsku"]) ? $context["showtopsku"] : null)) {
            // line 93
            echo "        \$('input[name=\"from\"], input[name=\"to\"], #input-app-type').on('change', function (e) {
            var \$this = \$(this),
            fromtime = \$('input[name=\"from\"]').val(),
            totime = \$('input[name=\"to\"]').val(),
            app_type = \$('#input-app-type').val(),
            countryID = ";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
            echo ",
            productWrap = \$('#top-products-sku'),
            otherProductWrap = \$('#sku');
            \$.ajax({
                url: \"";
            // line 102
            echo twig_escape_filter($this->env, site_url("ami/products/getTopProducts"), "html", null, true);
            echo "\",
                data: {from: fromtime, to: totime, country_id: countryID, app_type: app_type,all:true},
                type: 'POST',
                dataType: 'json',
                success: function (res) {
                    var productWrapper = productWrap.closest('div').find('.ms-drop.bottom ul'),
                            otherProductWrapper = otherProductWrap.closest('div').find('.ms-drop.bottom ul');
                    productWrap.html(res.topProducts);
                 //   otherProductWrap.html(res.otherProducts);
                }
            });

        });
        ";
        }
        // line 116
        echo "        \$(function () {
            // ajax get list customers when choose salesperson and channel
            if (\$('#armstrong_2_customers_id').length > 0) {
                \$('#armstrong_2_salespersons_id, #customer_channel').on('change', function () {
                    var salespersons_id = \$('#armstrong_2_salespersons_id').val(),
                            data,
                            cus_channel = (\$('#customer_channel').length > 0) ? \$('#customer_channel').val() : '';
                    if (cus_channel != '' && salespersons_id != '') {
                        data = {
                            armstrong_2_salespersons_id: salespersons_id,
                            channel: cus_channel
                        };
                    } else if (salespersons_id != '' && cus_channel == '') {
                        data = {
                            armstrong_2_salespersons_id: salespersons_id
                        };
                    } else if (salespersons_id == '' && cus_channel != '') {
                        data = {
                            channel: cus_channel
                        };
                    }
                    data.action = '";
        // line 137
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                    \$.ajax({
                        url: \"";
        // line 139
        echo twig_escape_filter($this->env, site_url("ami/customers/getCustomerBySalespersons"), "html", null, true);
        echo "\",
                        data: data,
                        type: 'POST',
                        dataType: 'html',
                        success: function (res) {
                            \$('#armstrong_2_customers_id').html(res);
                        }
                    });
                });
            }

            \$('#multiCountriesList').on('change', function (e) {
                var \$this = \$(this),
                        \$val = \$this.val(),
                        option = \$this.find('option');
                if (\$val == 0) {
                    \$.each(option, function () {
                        var op_val = \$(this).val();
                        \$val.push(op_val);
                    });
                    \$val.splice(0, 2);
                }
                \$this.val(\$val);
            });
            var monthNames = [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\",
                \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"
            ];
            function genareMonthSelect(id, maxmonth){
                var optionstring = '';
                var oldvalue = \$('#'+id).val();
                for(m = 1; m <= maxmonth; m++)
                {
                    if(oldvalue == m)
                    {
                        optionstring += \"<option value='\"+m+\"' selected='selected'>\"+monthNames[m-1]+\"</option>\";
                    }
                    else
                    {
                        optionstring += \"<option value='\"+m+\"'>\"+monthNames[m-1]+\"</option>\";
                    }
                }
                \$('#'+id).html(optionstring);
            }
            function loadMonth(){
                var datecurrent = new Date();
                var maxmonth = 12;
                if(\$('#input-year').val() >= datecurrent.getFullYear())
                {
                    var maxmonth = datecurrent.getMonth();
                }
                genareMonthSelect('monthfrom', maxmonth);
                genareMonthSelect('monthto', maxmonth);
            }
            function loadMonthTo(){
                var maxmonth = \$('#monthto').val();
                genareMonthSelect('monthfrom', maxmonth);
            }
            \$('#monthto').on('change', function (e) {
                loadMonthTo();
            });
            \$('#input-year').on('change', function (e) {
                loadMonth();
            });
            \$('#gripCustomers').on('change', function () {
                var \$val = \$(this).val();
                if (\$val == 1) {
                    var unassigned = '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                    \$('#armstrong_2_salespersons_id').find('option').end().append(unassigned);
                } else {
                    \$('#armstrong_2_salespersons_id').find('option[value=\"unassigned\"]').remove();
                }
            });
            //chon customer thi an from va to nguoc lai hien
            ";
        // line 212
        if (((isset($context["action"]) ? $context["action"] : null) == "dish_penetration_report")) {
            // line 213
            echo "            \$('#customer_channel, #customer-status').closest('.form-group').hide();
            ";
        }
        // line 215
        echo "            \$('#report_type').on('change', function (e) {
                if(\$(this).val() == 'channel')
                {
                    \$('input[name=\"from\"], input[name=\"to\"]').closest('.form-group').hide();
                    \$('#customer_channel, #customer-status').closest('.form-group').show();
                }
                else
                {
                    \$('input[name=\"from\"], input[name=\"to\"]').closest('.form-group').show();
                    \$('#customer_channel, #customer-status').closest('.form-group').hide();
                }
            });
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 235
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + toTitleCase(res['value'][key]) + '\">' + toTitleCase(res['value'][key]) + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            \$('.select-change').trigger('change');

            var salesLeaderSelect = \$('.SalespersonsSelect').html();

            \$('#SalesLeaderSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        sub_type = optionSelected.data('sub_type'),
                        \$target = \$('.SalespersonsSelect'),
                        val = \$this.val();
                var list = '<option value=\"\" selected=\"selected\">ALL</option>';
                if (\$.inArray('ALL', val) !== -1) {
                    \$target.html('').html(salesLeaderSelect);
                    return;
                }

                \$target.html(salesLeaderSelect).find('option').each(function () {
                    var \$option = \$(this);
                    slId = \$option.data('manager') || 0;

                    if (\$.inArray(slId, val) !== -1) {
                        list += \$option.prop('outerHTML');
                    }
                });
                \$target.html('').html(list);
                var arr_sub_type = [];
                \$.each(optionSelected, function () {
                    var sub_type = \$(this).data('sub_type');
                    arr_sub_type.push(sub_type);
                    if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                        \$target.html('').html(salesLeaderSelect);
                        return;
                    }
                });
                \$('input[name=\"sub_type\"]').val(arr_sub_type);
            });

            \$('.fetch-sku').on('change', function () {
                var month = \$('#input-month').val(),
                        year = \$('#input-year').val();
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/products/countNumberTopSku',
                    data: {
                        country_id: '";
        // line 306
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
        echo "',
                        month: month,
                        year: year
                    },
                    success: function (val) {
                        \$('#input-number-sku').html(val);
                    }
                });
            });

            \$('.fetch-sl').on('change', function () {
                var \$appType = \$('#input-app-type'),
                        \$sl = \$('#input-sale-leader'),
                        \$slStatus = \$('#input-sl-status'),
                        appType = \$appType.val() || '',
                        sl = \$sl.find(':selected').val() || '',
                        slStatus = \$slStatus.find(':selected').val() || '',
                        season = \$('select[name=\"season\"]').val(),
                        date_from = \$('input[name=\"from\"]').val(),
                        date_to = \$('input[name=\"to\"]').val();
                if (season) {
                    var split_season = season.split('.');
                    date_from = split_season[0];
                    date_to = split_season[1];
                }
                \$('#armstrong_2_salespersons_id').html('');
                month = \$('#input-month').find(':selected').val();
                year = \$('#input-year').find(':selected').val();
                if (month === undefined) month = 0;
                if (year === undefined) year = 0;
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/salespersons/fetch_salesperson',
                    data: {
                        app_type: appType,
                        active: slStatus,
                        salespersons_manager_id: sl,
                        date_from: date_from,
                        date_to: date_to,
                        month: month,
                        year: year,
                        action: '";
        // line 347
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "',
                    },
                    success: function (val) {
                        salesLeaderSelect = val;
                        ";
        // line 351
        if ((((isset($context["action"]) ? $context["action"] : null) == "report_customers") || ((isset($context["action"]) ? $context["action"] : null) == "customer_profiling_transaction"))) {
            // line 352
            echo "                        val += '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                        ";
        }
        // line 354
        echo "                        \$('#armstrong_2_salespersons_id').html(val);
                        \$('#armstrong_2_salespersons_id').trigger('change');
                    }
                });
            });

            \$('.fetch-sl').trigger('change');

            \$('.fetch-sl2').on('change', function () {
                var \$appType = \$('#input-app-type'),
                        \$sl = \$('#input-sale-leader'),
                        \$slStatus = \$('#input-sl-status'),
                        appType = \$appType.val() || '',
                        sl = \$sl.find(':selected').val() || '',
                        slStatus = \$slStatus.find(':selected').val() || '',
                        season = \$('select[name=\"season\"]').val(),
                        date_from = \$('input[name=\"from\"]').val(),
                        country_id = \$('select[name=\"country_list\"]').val(),
                        date_to = \$('input[name=\"to\"]').val();
                if (season) {
                    var split_season = season.split('.');
                    date_from = split_season[0];
                    date_to = split_season[1];
                }
                \$('#armstrong_2_salespersons_id').html('');
                month = \$('#input-month').find(':selected').val();
                year = \$('#input-year').find(':selected').val();
                if (month === undefined) month = 0;
                if (year === undefined) year = 0;
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/salespersons/fetch_salesperson2',
                    data: {
                        app_type: appType,
                        active: slStatus,
                        salespersons_manager_id: sl,
                        date_from: date_from,
                        date_to: date_to,
                        month: month,
                        year: year,
                        country_id: country_id,
                        action: '";
        // line 395
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "',
                    },
                    success: function (val) {
                        salesLeaderSelect = val;
                        ";
        // line 399
        if (((isset($context["action"]) ? $context["action"] : null) == "customer_profiling_transaction")) {
            // line 400
            echo "                        val += '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                        ";
        }
        // line 402
        echo "                        \$('#armstrong_2_salespersons_id').html(val);
                        \$('#armstrong_2_salespersons_id').trigger('change');
                    }
                });
            });

            \$('.fetch-sl2').trigger('change');

            \$('.fetch-sl3').on('change', function () {
                var \$appType = \$('#input-app-type'),
                        \$sl = \$('#input-sale-leader'),
                        \$slStatus = \$('#input-sl-status'),
                        appType = \$appType.val() || '',
                        sl = \$sl.find(':selected').val() || '',
                        slStatus = \$slStatus.find(':selected').val() || '',
                        season = \$('select[name=\"season\"]').val(),
                        date_from = \$('input[name=\"from\"]').val(),
                        country_id = \$('select[name=\"country_list\"]').val(),
                        date_to = \$('input[name=\"to\"]').val();

                \$('#SalesLeaderSelect').html('');
                month = \$('#input-month').find(':selected').val();
                year = \$('#input-year').find(':selected').val();
                if (month === undefined) month = 0;
                if (year === undefined) year = 0;
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/salespersons/fetch_salesleader',
                    data: {
                        app_type: appType,
                        active: slStatus,
                        salespersons_manager_id: sl,
                        date_from: date_from,
                        date_to: date_to,
                        month: month,
                        year: year,
                        country_id: country_id
                    },
                    success: function (val) {
                        salesLeaderSelect2 = val;
                        ";
        // line 442
        if (((isset($context["action"]) ? $context["action"] : null) == "customer_profiling_transaction")) {
            // line 443
            echo "                        val += '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                        ";
        }
        // line 445
        echo "                        \$('#SalesLeaderSelect').html(val);
                    }
                });
            });

            \$('.fetch-sl3').trigger('change');

            \$('.btn-clear').click(function (e) {
                e.preventDefault();
                \$(this).closest('.form-group').find(':text').val('');
            });

            var \$iFrame = \$('#iframe_report');
            var \$iFrame_zoom = \$('#iframe_report_zoom');
            // \$('#iframe_report').hide();
            // \$('#zoom_rp').hide();

            \$iFrame.load(function () {
                \$('#reporting-view .loading').html('');
                \$(\"#iframe_report\").show();
                \$(\"#zoom_rp\").show();
            });

            \$('form').on('submit', function (e) {
                if (\$(this).find('input[name=\"from\"]').val() === '' || \$(this).find('input[name=\"to\"]').val() === '')
                    e.preventDefault();
            });

            /*\$('button[type=\"reset\"]').click(function(){
             \$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/report_tfo');
             });*/

            /*\$('select#inputTable').on('change', function(){
             window.location = base_url() + 'management/reports?type=' + \$(this).val().replace('report_', '');;
             //\$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/' + \$(this).val());
             });

             \$('select#inputTable ul li a').on('click', function(){
             var mode =\$(this).attr('href').replace('<?=base_url()?>management/reports?type=', '');
             alert(mode);
             });*/

            \$('button.btn-view-report:not(.disabled)').click(function (e) {
                e.preventDefault();
                var mode = '";
        // line 489
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                var spElement = \$('#armstrong_2_salespersons_id');
                var getall = 0;
                var sr_manage = '";
        // line 492
        echo twig_escape_filter($this->env, (((isset($context["sr_manage"]) ? $context["sr_manage"] : null)) ? ((isset($context["sr_manage"]) ? $context["sr_manage"] : null)) : ("staff")), "html", null, true);
        echo "';
                var slElement = \$('#SalesLeaderSelect');
                var sl = null;
                var sub_type = null;
                if (slElement.length > 0) {
                    sl = slElement.val();
                    var sl_op_selected = slElement.find('option:selected');
                    \$.each(sl_op_selected, function () {
                        sub_type = \$(this).data('sub_type');
                        if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                            sl = null;
                            return false;
                        }
                    });
                }

                if (spElement.length > 0) {
                    var sp = spElement.val();
                    if (sp === null || sp[0] == '' || sp[0] === null || sp[0] === 'all') {
                        if (sl == undefined || sl === null || sl[0] == '' || sl[0] === null || sl[0] == 'ALL') {
                            getall = 1;
                            ";
        // line 514
        echo "                            ";
        // line 515
        echo "                            ";
        // line 516
        echo "                            sp = 0;
                            ";
        // line 518
        echo "                            if(sr_manage == 'staff') sp = '";
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) > 0)) ? ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array())) : (0)), "html", null, true);
        echo "';
                        } else {
                            getall = 2;
                            sp = sl;
                        }
                    }else{
                        if (sp[0] == 'unassigned'){
                            getall = 9;
                            sp = \"0\";
                        }
                    }
                } else {
                    if (mode == 'data_customers' || mode == 'customer_profiling_transaction'){
                        if (sl[0] == 'unassigned') {
                            getall = 9;
                            sp = '";
        // line 533
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) > 0)) ? ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array())) : (0)), "html", null, true);
        echo "';
                        }else{
                            getall = 0;
                            sp = '";
        // line 536
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) > 0)) ? ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array())) : (0)), "html", null, true);
        echo "';
                        }
                    }else{
                        getall = 0;
                        sp = '";
        // line 540
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roles_id", array()) > 0)) ? ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array())) : (0)), "html", null, true);
        echo "';
                    }
                }

                var otmElement = \$('#InputOTM');
                var otm = '';
                if (otmElement.length > 0) {
                    otm = otmElement.val();
                }

                var channelElement = \$('#ChannelSelect');
                var channel = '';
                if (channelElement.length > 0) {
                    channel = channelElement.val();
                }

                ";
        // line 556
        if ((($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking_detail") || ($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking"))) {
            // line 557
            echo "                    if(\$('#input-date-type').val()== 0){
                        if (\$('select[name=\"season\"]').length > 0) {
                            var season = \$('select[name=\"season\"]').val();
                            var season_arr = season.split('.');
                            fromdate = season_arr['0'] + ' 00:00:00';
                            todate = season_arr['1'] + ' 23:59:59';
                        }
                    }else{
                        if (\$('input[name=\"from\"]').length > 0) {
                            var fromdate = \$('input[name=\"from\"]').val() + ' 00:00:00';
                        }
                        if (\$('input[name=\"to\"]').length > 0) {
                            var todate = \$('input[name=\"to\"]').val() + ' 23:59:59';
                        }
                    }

                ";
        } else {
            // line 574
            echo "                    if (\$('input[name=\"from\"]').length > 0) {
                        var fromdate = \$('input[name=\"from\"]').val() + ' 00:00:00';
                    }
                    if (\$('input[name=\"to\"]').length > 0) {
                        var todate = \$('input[name=\"to\"]').val() + ' 23:59:59';
                    }

                    if (\$('select[name=\"season\"]').length > 0) {
                        var season = \$('select[name=\"season\"]').val();
                        var season_arr = season.split('.');
                        fromdate = season_arr['0'] + ' 00:00:00';
                        todate = season_arr['1'] + ' 23:59:59';
                    }
                ";
        }
        // line 588
        echo "                if (\$('select[name=\"number_sku\"]').length > 0) {
                    var number_sku = \$('select[name=\"number_sku\"]').val();
                }
                if (\$('select[name=\"country_list\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list\"]').val();
                    var countryId = country_list;
                } else if (\$('select[name=\"country_list[]\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list[]\"]').val();
                    var countryId = country_list;
//                if(\$.isArray(countryId)){
//                    countryId = countryId.join('&country=');
//                }
                } else {
                    var countryId = '";
        // line 601
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
        echo "';
                }

                if (\$('#input-app-type').length > 0) {
                    var app_type = \$('#input-app-type').val();
                }
                if (\$('#report_type').length > 0) {
                    var report_type = \$('#report_type').val();
                }
                else
                {
                    var report_type = '';
                }
                if (\$('#note_type').length > 0) {
                    var note_type = \$('#note_type').val();
                }
                else
                {
                    var note_type = '';
                }
                if (\$('select[name=\"active\"]') != null) {
                    var active = \$('select[name=\"active\"]').val();
                }


                if (\$('#gripCustomers').length > 0) {
                    var isGripCustomer = \$('#gripCustomers').val();
                }
                // var fromdate = '2014-05-01 00:00:01';
                // var todate = '2014-05-31 23:59:59';

                var elementMoth = \$('#input-month');
                var elementYear = \$('#input-year');
                var elementMothFrom = \$('select[name=\"monthfrom\"]');
                var elementMothTo = \$('select[name=\"monthto\"]');
                if (elementMoth.length > 0 && elementYear.length > 0) {
                    var year = elementYear.val();
                    var month = elementMoth.val();
                    var dateStartMonth = year + '-' + month + '-01 00:00:00';
                    var lastday = (new Date(year, month, 0)).getDate();
                    var dateEndMonth = year + '-' + month + '-' + lastday + ' 23:59:59';
                }
                if (elementYear.length > 0 && elementMothFrom.length > 0 && elementMothTo.length > 0) {
                    var year = elementYear.val();
                    var monthfrom = elementMothFrom.val();
                    var monthto = elementMothTo.val();
                    var dateStartMonth = year + '-' + monthfrom + '-01 00:00:00';
                    var lastday = (new Date(year, monthto, 0)).getDate();
                    console.log(lastday);
                    var dateEndMonth = year + '-' + monthto + '-' + lastday + ' 23:59:59';
                    if(dateStartMonth > dateEndMonth)
                    {
                        alert(\"Date report isn't valid\");
                        return false;
                    }
                }
                var sp_too = (typeof sp !== 'undefined') ? \$('select[name=\"armstrong_2_salespersons_id\"] option[value=\"' + sp + '\"]').html() : \"";
        // line 657
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
        echo " [";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
        echo "]\";

                var urlGetReport = '";
        // line 659
        echo twig_escape_filter($this->env, (isset($context["reportEndpoint"]) ? $context["reportEndpoint"] : null), "html", null, true);
        echo "';
                var isMultiCountry = 0;
                var regional = 0;
                ";
        // line 662
        if (((isset($context["filter"]) ? $context["filter"] : null) == "regional")) {
            // line 663
            echo "                regional = 1;
                getall = 1;
                sp = 0;
                channel = -1;
                otm = 'ALL';
                if (\$('select[name=\"country_list[]\"]').length > 0) {
                    isMultiCountry = 1;
                }
                ";
        }
        // line 672
        echo "                var versionIe = getInternetExplorerVersion();
                if (versionIe >= 7 && versionIe <= 9) {
                    getdataForTableOnIe(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm);
                } else {
                    getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, isGripCustomer, isMultiCountry, regional, report_type, note_type);
                }
            });

            function getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, isGripCustomer, isMultiCountry, regional, report_type, note_type) {
                \$('#reporting-view').html('<p class=\"loader-dotted\" style=\"text-align:center;\">&nbsp;&nbsp;<span class=\"status\"></span><br /><img src=\"";
        // line 681
        echo twig_escape_filter($this->env, site_url("res/img/load-generic.gif"), "html", null, true);
        echo "\" /></p>');
                \$('#g_chart').html('<p class=\"loader-dotted\" style=\"text-align:center;\">&nbsp;&nbsp;<span class=\"status\"></span><br /><img src=\"";
        // line 682
        echo twig_escape_filter($this->env, site_url("res/img/load-generic.gif"), "html", null, true);
        echo "\" /></p>');
                urlGetReport += 'json/';
                var mode = '";
        // line 684
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                var filter = \$('#filter_listed').val();
                var salesperson_status = \$('#input-sl-status').val();
                var cStatus = \$('#customer-status').val();
                var tfoSource = \$('#tfoSource').val();
                var productStatus = \$('#productStatus').val();
                var templateType = \$('#templateType').val();
                var syncReportType = \$('#syncReportType').val();
                var syncReportView = \$('#syncReportView').val();
                var sr_manage = '";
        // line 693
        echo twig_escape_filter($this->env, (((isset($context["sr_manage"]) ? $context["sr_manage"] : null)) ? ((isset($context["sr_manage"]) ? $context["sr_manage"] : null)) : ("staff")), "html", null, true);
        echo "';
                var customer_channel = \$('#customer_channel').val();
                var customers = \$('#armstrong_2_customers_id').val();
                console.log(customers);
                if(typeof customers != 'undefined'){
                    if(customers[0] === 'all'){
                        customers = 0;
                    }
                }
                var sku;
                if (\$('#sku').val()){
                    if(\$.inArray(\"\", \$('#sku').val()) != -1){
                        sku = 0;
                    }else{
                        sku = \$('#sku').val();
                    }
                }else{
                    sku = [];
                }
                ";
        // line 712
        if ((isset($context["showtopsku"]) ? $context["showtopsku"] : null)) {
            // line 713
            echo "                if (sku !== 0) {
                    topsku = \$('#top-products-sku').val();
                    if (topsku) {
                        if (\$.inArray('ALL', topsku) >= 0) {
                            \$('#top-products-sku option').each(function () {
                                if (\$(this).val() != 'ALL') {
                                    if (\$.inArray(\$(this).val(), sku) == -1) {
                                        sku.push(\$(this).val());
                                    }
                                }
                            });
                        }
                        else {
                            for (x in topsku) {
                                if (\$.inArray(topsku[x], sku) == -1) {
                                    sku.push(topsku[x]);
                                }
                            }
                        }
                    }
                }
                ";
        }
        // line 735
        echo "                var datapost = {
                    note_type: note_type,
                    from_date: fromdate,
                    to_date: todate,
                    spId: sp,
                    custId: customers,
                    country: countryId,
                    getall: getall,
                    app_type: app_type,
                };
                var from = \$('input[name=\"from\"]').val();
                var to = \$('input[name=\"to\"]').val();
                var listNewReport = ['ilovephp','avp_point_report', 'out_of_trade_report', 'rich_media_demonstration' ,'summary_out_of_trade_report', 'transaction_rich_media_demonstration', 'ttl_report', 'bo_file'];
                if(listNewReport.indexOf(mode) > 0)
                {
                    getNewReport(urlGetReport, mode, sp, sku, customer_channel, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, isGripCustomer, isMultiCountry, regional, report_type, note_type, salesperson_status ,sr_manage);
                }
                else
                {
                    switch (mode) {
                        case 'assessment_report':
                            getAssesmentReport(urlGetReport, fromdate, todate, active, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'out_of_trade_report':
                            getOutOfTradeReport(urlGetReport, fromdate, todate, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'gift_voucher_report':
                            getGiftVoucherReport(urlGetReport, fromdate, todate, customers, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'avp_point_report':
                            getAvpPointReport(urlGetReport, fromdate, todate, customer_channel, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'ggg_report':
                            getGGGReport(urlGetReport, sp, app_type, dateStartMonth, dateEndMonth, customer_channel, sku, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'notes_report':
                            getNoteReport(urlGetReport, datapost);
                            break;
                        case 'get_perfect_store_report':
                            getPerfectStoreReport(urlGetReport, countryId, getall, sp, month, year, app_type, active, isMultiCountry, regional);
                            break;
                        case 'get_perfect_call_report_tracking':
                            getPerfectCallReportTracking(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'get_perfect_call_report_tracking_detail':
                            getPerfectCallReportTrackingDetail(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'penetration_customers_report':
                            penetrationCustomersReport(urlGetReport, month, year, countryId, number_sku, sp, getall, app_type, channel, otm, sr_manage, isMultiCountry, regional);
                            break;
                        case 'penetration_data_report':
                            top_products = '';
                            view_by = 0;
                            penetrationDataReport(urlGetReport, month, year, countryId, app_type, channel, otm, getall, sp, top_products, isGripCustomer, view_by, isMultiCountry, regional);
                            break;
                        case 'get_perfect_call_report':
                            getPerfectCallReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'get_detail_app_versions_report':
                            getDetailAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                            break;
                        case 'get_app_versions_report':
                            getAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, salesperson_status, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_all_in_one':
                            getReportAllInOne(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'report_pantry_check':
                            loadReportPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_tfo':
                            switch (countryId) {
                                case '3':
                                    dataTfoHk(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sku, tfoSource, productStatus, sr_manage, isMultiCountry, regional);
                                    break;
                                case '6':
                                    dataTfoTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sku, tfoSource, productStatus, sr_manage, isMultiCountry, regional);
                                    break;
                                default:
                                    dataTfoAll(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sku, tfoSource, productStatus, sr_manage, isMultiCountry, regional);
                                    break;
                            }
                            break;
                        case 'data_ssd':
                            if (countryId != 6) {
                                dataSsdReprotAll(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sku, sr_manage, isMultiCountry, regional);
                            } else {
                                dataSsdReprotTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sku, sr_manage, isMultiCountry, regional);
                            }
                            // loadDataSsd(urlGetReport, sp, month, year, countryId, getall);
                            break;
                        case 'data_call':
                            loadDataCall(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'coaching_note':
                            coachingNoteDataReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'coaching_summary':
                            coachingSummaryDataReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'route_master_temp':
                            routeMasterTempDataReport(urlGetReport, sp, countryId, getall, app_type, salesperson_status, sr_manage, isMultiCountry, regional);
                            break;
                        case 'customer_contact_data':
                            customerContactReport(urlGetReport, sp, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_sampling':
                            if (countryId != 6) {
                                dataSamplingAll(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            } else {
                                dataSamplingTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            }
                            // loadDataSampling(urlGetReport, sp, fromdate, todate, countryId,getall,active);
                            break;
                        case 'data_pantry_check':
                            loadDataPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sku, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_last_pantry_check':
                            reportLastPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_otm':
                            loadReportOtm(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'email_report':
                                var sent = \$('#sent').val();
                            loadEmailReport(urlGetReport, sp, fromdate, todate, countryId, getall, sent, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'qc_report':
                            loadQcReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'uplift_report':
                            loadUpliftReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_call':
                            loadReportCall(urlGetReport, sp, fromdate, todate, countryId, channel, otm, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_customers':
                            loadReportCustomer(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'report_summary':
                            allInOneTransactionsReprot(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'report_tfo':
                            tfoReprotSr(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'report_sampling':
                            samplingReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                            break;
                        case 'report_approval':
                            approvalReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'top_ten_penetration_sr':
                            topTenPenetrationSrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'report_route_plan':
                            routePlanReprot(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'report_grip_and_grab_sr':
                            gripAndGrabReportSR(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'data_otm':
                            dataOtm(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'new_customer_report':
                            newCustomerReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'top_ten_penetration_calculation_tr':
                            topTenPenetrationCalculationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'new_grip_report':
                            newGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'new_grab_report':
                            newGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'lost_grip_report':
                            lostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'lost_grab_report':
                            lostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'potential_lost_grip_report':
                            potentialLostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'potential_lost_grab_report':
                            potentialLostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'top_ten_penetration_tr':
                            topTenPenetrationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                            break;
                        case 'competitor_reprot':
                            competitorReprot(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'objective_record_data':
                            objectiveRecordData(urlGetReport, sp, fromdate, todate, countryId, getall, sr_manage, isMultiCountry, regional, active, app_type);
                            break;
                        case 'activities_log_data':
                            activitiesLogData(urlGetReport, sp, fromdate, todate, countryId, getall, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_product':
                            productData(urlGetReport, countryId, sr_manage, filter, isMultiCountry, regional);
                            break;
                        case 'data_recipes':
                            getRecipesData(urlGetReport, countryId, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_route_plan':
                            dataRoutePlan(urlGetReport, sp, countryId, getall, fromdate, todate, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_organization':
                            salespersonsData(urlGetReport, sp, countryId, sr_manage, isMultiCountry, regional , active);
                            break;
                        case 'data_region_master':
                            regionReport(urlGetReport, sp, countryId, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_ssd_mt':
                            ssData(urlGetReport, sp, countryId, getall, month, year, fromdate, todate, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_distributors':
                            dataDistributors(urlGetReport, sp, countryId, getall, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_wholesalers':
                            dataWholesalers(urlGetReport, sp, countryId, getall, sr_manage, isMultiCountry, regional);
                            break;
                        case 'data_customers':
                            if (countryId != 6) {
                                dataCustomersReprotAll(urlGetReport, sp, countryId, getall, cStatus, isMultiCountry, regional);
                            } else {
                                dataCustomersReprotTw(urlGetReport, sp, countryId, getall, cStatus, isMultiCountry, regional);
                            }

                            break;
                        case 'customer_profiling':
                            loadCustomerProfilingReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, sr_manage, isMultiCountry, regional);
                            break;
                        case 'customer_profiling_transaction':
                            loadCustomerProfilingTransactionReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, cStatus, sr_manage, isMultiCountry, regional);
                            break;
                        case 'regional_sync_report':
                            loadRegionalSyncReport(urlGetReport, sp, month, year, countryId, getall, app_type, active, syncReportType, syncReportView, isMultiCountry, regional);
                            break;
                        case 'rap_report':
                            rapReport(urlGetReport, countryId, sp, getall, fromdate, todate, app_type, active, customer_channel, sr_manage, isMultiCountry, regional);
                            break;
                        case 'dish_penetration_report':
                            dishPenetrationReport(urlGetReport, countryId, sp, getall, fromdate, todate, customer_channel, customers, sku, sr_manage, isMultiCountry, regional, report_type, cStatus);
                            break;
                        case 'free_gift_report':
                            freeGiftReport(urlGetReport, countryId, sp, getall, fromdate, todate, app_type, active, sr_manage, isMultiCountry, regional);
                            break;
                        case 'potential_customers_report':
                            potentialCustomersReport(urlGetReport, countryId, getall, fromdate, todate, active, regional);
                            break;
                        case 'perfect_store_master_data':
                            perfectStoreMasterData(urlGetReport, sp, countryId, getall, templateType, isMultiCountry, regional);
                            break;
                        default:
                            loadDataOld();
                            break;

                    }
                }
            }

            function getdataForTableOnIe(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm) {
                var mode = '";
        // line 999
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                var \$iFrame = \$('#iframe_report');
                var \$iFrame_zoom = \$('#iframe_report_zoom');

                var cStatus = \$('#customer-status').val();
                var salesperson_status = \$('#input-sl-status').val();

                \$('#reporting-view .loading').html('<p class=\"loader-dotted\" style=\"text-align:center;\">&nbsp;&nbsp;<span class=\"status\"></span><br /><img src=\"";
        // line 1006
        echo twig_escape_filter($this->env, site_url("res/img/load-generic.gif"), "html", null, true);
        echo "\" /> Loading...</p>');
                \$('#iframe_report').hide();
                \$('#zoom_rp').hide();
                urlGetReport = urlGetReport + 'html/';
                var dataSend = '';
                var url = '';
                var urlget = '';
                switch (mode) {
                    case 'get_perfect_store_report':
                        urlget = urlGetReport + 'transactiondata/get_perfect_store_report';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        dataSend += '&app_type=' + app_type + '&active=' + active;
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'get_perfect_call_report_tracking':
                        urlget = urlGetReport + 'summaryreports/get_perfect_call_report_tracking';
                        dataSend = generateParamHaveBetweenDateAndApptype(urlGetReport, sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'get_perfect_call_report_tracking_detail':
                        urlget = urlGetReport + 'transactiondata/get_perfect_call_report_tracking_detail';
                        dataSend = generateParamHaveBetweenDateAndApptype(urlGetReport, sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'penetration_customers_report':
                        urlget = urlGetReport + 'summaryreports/penetration_customers_report';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        dataSend += '&number_sku=' + number_sku;
                        dataSend += '&app_type=' + app_type + '&channel=' + channel + '&otm=' + otm;
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'penetration_data_report':
                        urlget = urlGetReport + 'summaryreports/penetration_data_report';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        dataSend += '&app_type=' + app_type + '&channel=' + channel + '&otm=' + otm;
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'get_perfect_call_report':
                        urlget = urlGetReport + 'transactiondata/get_perfect_call_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'get_detail_app_versions_report':
                        urlget = urlGetReport + 'transactiondata/get_detail_app_versions_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'get_app_versions_report':
                        urlget = urlGetReport + 'summaryreports/get_app_versions_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_all_in_one':
                        urlget = urlGetReport + 'summaryreports/get_all_in_one';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_call':
                        urlget = urlGetReport + 'summaryreports/reportcall';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        url = urlget + '?' + dataSend + '&channel=' + channel + '&otm=' + otm + '&app_type=' + app_type;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_pantry_check':
                        urlget = urlGetReport + 'summaryreports/report_pantry_check';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_otm':
                        urlget = urlGetReport + 'summaryreports/report_otm';
                        dataSend = generateParamHaveBetweenDate(sp, dateStartMonth, dateEndMonth, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_customers':
                        urlget = urlGetReport + 'summaryreports/customers';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_call':
                        urlget = urlGetReport + 'transactiondata/data_call';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'coaching_note':
                        urlget = urlGetReport + 'transactiondata/coaching_note';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'route_master_temp':
                        urlget = urlGetReport + 'masterdata/route_master_temp';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend + '&app_type=' + app_type;
                        \$iFrame.attr('src', url);
                        break;
                    case 'customer_contact_data':
                        urlget = urlGetReport + 'masterdata/customer_contact_data';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend + '&app_type=' + app_type + '&active=' + active;
                        ;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_tfo':
                        urlget = urlGetReport + 'transactiondata/data_tfo';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_ssd':
                        urlget = urlGetReport + 'transactiondata/data_ssd';
                        if (countryId == 6) {
                            dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        } else {
                            dataSend = generateParamHaveBetweenDate(sp, dateStartMonth, dateEndMonth, countryId, getall);
                        }

                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_pantry_check':
                        urlget = urlGetReport + 'transactiondata/data_pantry_check';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_last_pantry_check':
                        urlget = urlGetReport + 'summaryreports/report_last_pantry_check';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_sampling':
                        urlget = urlGetReport + 'transactiondata/data_sampling';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_summary':
                        urlget = urlGetReport + 'summaryreports/all_in_one_transactions';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_tfo':
                        urlget = urlGetReport + 'summaryreports/tfo_reprot_sr';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_sampling':
                        urlget = urlGetReport + 'summaryreports/sampling_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_approval':
                        urlget = urlGetReport + 'summaryreports/approval_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'top_ten_penetration_sr':
                        urlget = urlGetReport + 'summaryreports/top_ten_penetration_sr';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_route_plan':
                        urlget = urlGetReport + 'summaryreports/route_plan_reprot';
                        dataSend = generateParamWitchMonthYear(sp, month, year, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'report_grip_and_grab_sr':
                        urlget = urlGetReport + 'summaryreports/grip_and_grab_report_sr';
                        dataSend = generateParamHaveBetweenDate(sp, dateStartMonth, dateEndMonth, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_otm':
                        urlget = urlGetReport + 'transactiondata/data_otm';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'new_customer_report':
                        urlget = urlGetReport + 'transactiondata/new_customer_report';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'top_ten_penetration_calculation_tr':
                        urlget = urlGetReport + 'transactiondata/top_ten_penetration_calculation_tr';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'new_grip_report':
                        urlget = urlGetReport + 'transactiondata/new_grip_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'new_grab_report':
                        urlget = urlGetReport + 'transactiondata/new_grab_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'lost_grip_report':
                        urlget = urlGetReport + 'transactiondata/lost_grip_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'lost_grab_report':
                        urlget = urlGetReport + 'transactiondata/lost_grab_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'potential_lost_grip_report':
                        urlget = urlGetReport + 'transactiondata/potential_lost_grip_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'potential_lost_grab_report':
                        urlget = urlGetReport + 'transactiondata/potential_lost_grab_report';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'top_ten_penetration_tr':
                        urlget = urlGetReport + 'transactiondata/top_ten_penetration_tr';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'competitor_reprot':
                        urlget = urlGetReport + 'transactiondata/competitor_reprot';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall, app_type);
                        url = urlget + '?' + dataSend + '&active=' + active;
                        \$iFrame.attr('src', url);
                        break;
                    case 'objective_record_data':
                        urlget = urlGetReport + 'trantransactiondata/objective_record_data';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'activities_log_data':
                        urlget = urlGetReport + 'transactiondata/activities_log_data';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_product':
                        urlget = urlGetReport + 'masterdata/product_data';
                        url = urlget + '?country=' + countryId;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_recipes':
                        urlget = urlGetReport + 'masterdata/data_recipes';
                        url = urlget + '?country=' + countryId;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_route_plan':
                        urlget = urlGetReport + 'masterdata/data_route_plan';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_organization':
                        urlget = urlGetReport + 'masterdata/salespersons';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_region_master':
                        urlget = urlGetReport + 'masterdata/region';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_ssd_mt':
                        urlget = urlGetReport + 'masterdata/ssd';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_distributors':
                        urlget = urlGetReport + 'masterdata/distributors';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_wholesalers':
                        urlget = urlGetReport + 'masterdata/wholesalers';
                        dataSend = generateParamNotTime(sp, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'data_customers':
                        urlget = urlGetReport + 'masterdata/data_customers';
                        dataSend = generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall);
                        url = urlget + '?' + dataSend;
                        \$iFrame.attr('src', url);
                        break;
                    case 'customer_profiling_transaction':
                        loadCustomerProfilingTransactionReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, cStatus, sr_manage, isMultiCountry, regional);
                        break;
                    default:
                        break;
                }
            }

            function loadDataOld() {
                var mode = '";
        // line 1337
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                var sp = \$('select#inputArmstrong_2_salespersons_id').val();
                var sp_too = (typeof sp !== 'undefined') ? \$('select[name=\"armstrong_2_salespersons_id\"] option[value=\"' + sp + '\"]').html() : \"";
        // line 1339
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
        echo " [";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
        echo "]\";
                \$.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: base_url() + 'manual_reports/' + mode,
                    data: \$('form').serialize(),
                    //data: {armstrong_2_salespersons_id : sp, form : 1, from : \$('input[name=\"from\"]').val(), to : \$('input[name=\"to\"]').val()},
                    success: function (result) {
                        var html = '';
                        var i = 1;
                        switch (mode) {
                            /*case 'report_penetration':
                             html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"5\">Top 10 Penetration</th></tr>';
                             html += '<tr style=\"background: #95b3d7;\"><th colspan=\"2\">Salesperson: ' + sp_too + '</th><th>Top 10</th><th>%</th><th>Target</th></tr></thead><tbody>';
                             \$.each(result, function(key, val){
                             html += '<tr><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customer_name'] + '</td><td>' + val['top_ten'] + '</td><td>' + val['percent'] + '</td><td>' + val['target'] + '</td></tr>';
                             });
                             html += '</tbody></table>';
                             break;*/
                            case 'data_tfo':
                                var istaiwan = false;
                                if (result.length > 0 && result[0]['price_per_case'] !== undefined) {
                                    istaiwan = true;
                                }
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">TFO Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 TFO ID</th><th>AS2 Customer ID</th><th>Customer Name</th><th>AS2 Salesperson ID</th><th>Salesperson</th><th>AS2 Distributors ID</th><th>Distributors Name</th><th>Distributor Group</th><th>AS2 Wholesalers ID</th><th>Wholesalers Name</th>';

                                if (istaiwan) {
                                    html += '<th>Sales Hierarchy</th>';
                                }
                                html += '<th>AS2 Call Records ID</th><th>SKU number</th><th>SKU name</th><th>Packing Size</th><th>Top Ten</th><th>Qty case</th><th>Qty pcs</th><th>Free cases</th><th>Free pieces</th><th>New SKU</th><th>Total</th>';

                                if (istaiwan) {
                                    html += '<th>Price per case</th><th>Customer Type</th>';
                                }
                                html += '<th>Method</th><th>Date created</th><th>Time Created</th><th>Delivery date</th><th>Delivery Time</th><th>Country ID</th><th>Bussiness Type</th><th>Remarks</th><th>Signature</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_tfo_id'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_distributors_id'] + '</td><td>' + val['distributors_name'] + '</td><td>' + val['distributor_group'] + '</td><td>' + val['armstrong_2_wholesalers_id'] + '</td><td>' + val['wholesalers_name'] + '</td>';
                                    if (istaiwan) {
                                        html += '<td>' + val['sales_hierarchy'] + '</td>';
                                    }
                                    html += '<td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['sku_number'] + '</td><td>' + val['sku_name'] + '</td><td>' + val['packing_size'] + '</td><td>' + val['is_top_ten'] + '</td><td>' + val['qty_case'] + '</td><td>' + val['qty_pcs'] + '<td>' + val['free_case'] + '</td><td>' + val['free_pcs'] + '</td><td>' + val['new_sku'] + '</td><td>' + val['total'] + '</td>';
                                    if (istaiwan) {
                                        html += '<td>' + val['price_per_case'] + '</td><td>' + val['customer_type'] + '</td>';
                                    }
                                    html += '<td>' + val['method'] + '</td><td>' + val['date_created'] + '</td><td>' + val['time_created'] + '</td><td>' + val['delivery_date'] + '</td><td>' + val['delivery_time'] + '</td><td>' + val['country_id'] + '</td><td>' + val['business_type'] + '</td><td>' + val['remarks'] + '</td><td>' + val['signature'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;

                            case 'data_ssd':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">SSD Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">OTM</th><th style=\"text-align: center;\"># of visited</th><th style=\"text-align: center;\">Total</th><th style=\"text-align: center;\">%</th></tr></thead><tbody>';
                                var cols = [];
                                var cols = [];
                                if (result.length > 0) {
                                    html += '<tr style=\"background: #95b3d7;\"><th>#</th>';
                                    \$.each(result[0], function (key, val) {
                                        html += '<th>' + key + '</th>';
                                        cols.push(key);
                                    });
                                    html += '</tr>';
                                }
                                html += '</thead><tbody>';
                                \$.each(result, function (key, val) {
                                    //html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td style=\"text-align: center;\">' + val['otm_A_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_B_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_C_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_D_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_A_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_B_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_C_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_D_fulfillment_rate'] + '</td></tr>';
                                    html += '<tr><td>' + i + '</td>';
                                    \$.each(cols, function (k, v) {
                                        html += '<td';
                                        if (k > 2) html += ' style=\"text-align: center;\"';
                                        html += '>' + val[v] + '</td>';
                                    });
                                    html += '</tr>';
                                    i++;
                                });

                                html += '</tbody></table>';
                                break;
                            case 'data_call':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Call Records Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>Country Channel</th><th>OTM</th><th>Date</th><th>Time Call Start</th><th>Time Call End</th><th>Call Length</th><th>Customer Type</th><th>Bussiness Type</th><th>Month Week</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['country_channels'] + '</td><td>' + val['otm'] + '</td><td>' + val['date'] + '<td>' + val['time_call_start'] + '</td><td>' + val['time_call_end'] + '</td><td>' + val['call_length'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td><td>' + val['month_week'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'coaching_note':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Coaching Note Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>Country Channel</th><th>OTM</th><th>Date</th><th>Time Call Start</th><th>Time Call End</th><th>Call Length</th><th>Customer Type</th><th>Bussiness Type</th><th>Month Week</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['country_channels'] + '</td><td>' + val['otm'] + '</td><td>' + val['date'] + '<td>' + val['time_call_start'] + '</td><td>' + val['time_call_end'] + '</td><td>' + val['call_length'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td><td>' + val['month_week'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'route_master_temp':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Coaching Note Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>Country Channel</th><th>OTM</th><th>Date</th><th>Time Call Start</th><th>Time Call End</th><th>Call Length</th><th>Customer Type</th><th>Bussiness Type</th><th>Month Week</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['country_channels'] + '</td><td>' + val['otm'] + '</td><td>' + val['date'] + '<td>' + val['time_call_start'] + '</td><td>' + val['time_call_end'] + '</td><td>' + val['call_length'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td><td>' + val['month_week'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'customer_contact_data':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Coaching Note Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>Country Channel</th><th>OTM</th><th>Date</th><th>Time Call Start</th><th>Time Call End</th><th>Call Length</th><th>Customer Type</th><th>Bussiness Type</th><th>Month Week</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['country_channels'] + '</td><td>' + val['otm'] + '</td><td>' + val['date'] + '<td>' + val['time_call_start'] + '</td><td>' + val['time_call_end'] + '</td><td>' + val['call_length'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td><td>' + val['month_week'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'data_sampling':
                                var istaiwan = false;
                                if (result.length > 0 && result[0]['sales_hierarchy'] !== undefined) {
                                    istaiwan = true;
                                }
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Sampling Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Pantry Check ID</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th>';
                                if (istaiwan) {
                                    html += '<th>Sales Hierarchy</th>';
                                }
                                html += '<th>AS2 Customers ID</th><th>Customer Name</th><th>SKU number</th><th>SKU name</th><th>Packing Size</th><th>Country Channel</th><th>Type</th><th>Comments</th><th>Date Call Start</th><th>Time Call Start</th>';
                                html += '<th>Customer Type</th><th>Bussiness Type</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_sampling_id'] + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td>';
                                    if (istaiwan) {
                                        html += '<td>' + val['sales_hierarchy'] + '</td>';
                                    }
                                    html += '<td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['sku_number'] + '</td><td>' + val['sku_name'] + '</td><td>' + val['packing_size'] + '</td<td>' + val['country_channel'] + '</td><td>' + val['type'] + '</td><td>' + val['comments'] + '</td><td>' + val['date_call_start'] + '</td><td>' + val['time_call_start'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'data_pantry_check':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Pantry Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Pantry Check ID</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>SKU number</th><th>SKU name</th><th>Packing Size</th><th>Current Qty Case</th><th>Current Qty Pc</th><th>Consumption Case</th><th>Consumption Pc</th><th>Country Channel</th><th>Notes</th><th>Date Call Start</th><th>Time Call Start</th><th>Customer Type</th><th>Bussiness Type</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_pantry_check_id'] + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['sku_number'] + '</td><td>' + val['sku_name'] + '</td><td>' + val['packing_size'] + '</td><td>' + val['current_qty_cases'] + '<td>' + val['current_qty_pcs'] + '</td><td>' + val['consumption_cases'] + '</td><td>' + val['consumption_pcs'] + '</td><td>' + val['country_channel'] + '</td><td>' + val['notes'] + '</td><td>' + val['date_call_start'] + '</td><td>' + val['time_call_start'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_last_pantry_check':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Pantry Data</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Pantry Check ID</th><th>AS2 Call Records ID</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>AS2 Customers ID</th><th>Customer Name</th><th>SKU number</th><th>SKU name</th><th>Packing Size</th><th>Current Qty Case</th><th>Current Qty Pc</th><th>Consumption Case</th><th>Consumption Pc</th><th>Country Channel</th><th>Notes</th><th>Date Call Start</th><th>Time Call Start</th><th>Customer Type</th><th>Bussiness Type</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_pantry_check_id'] + '</td><td>' + val['armstrong_2_call_records_id'] + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['armstrong_2_customers_id'] + '</td><td>' + val['customers_name'] + '</td><td>' + val['sku_number'] + '</td><td>' + val['sku_name'] + '</td><td>' + val['packing_size'] + '</td><td>' + val['current_qty_cases'] + '<td>' + val['current_qty_pcs'] + '</td><td>' + val['consumption_cases'] + '</td><td>' + val['consumption_pcs'] + '</td><td>' + val['country_channel'] + '</td><td>' + val['notes'] + '</td><td>' + val['date_call_start'] + '</td><td>' + val['time_call_start'] + '</td><td>' + val['customer_type'] + '</td><td>' + val['business_type'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_otm':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">OTM Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">OTM</th><th style=\"text-align: center;\"># of visited</th><th style=\"text-align: center;\">Total</th><th style=\"text-align: center;\">%</th></tr></thead><tbody>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>OTM A Customer Count</th><th>OTM B Customer Count</th><th>OTM C Customer Count</th><th>OTM D Customer Count</th><th>OTM A Fulfillment Rate</th><th>OTM B Fulfillment Rate</th><th>OTM C Fulfillment Rate</th><th>OTM D Fulfillment Rate</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td style=\"text-align: center;\">' + val['otm_A_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_B_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_C_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_D_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_A_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_B_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_C_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_D_fulfillment_rate'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_pantry_check':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Pantry Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">OTM</th><th style=\"text-align: center;\"># of visited</th><th style=\"text-align: center;\">Total</th><th style=\"text-align: center;\">%</th></tr></thead><tbody>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>Total Calls</th><th>Total Pantry Checks</th><th>Average Pantry Check Per Call</th><th>Total Pantry Checks Items</th><th>Average Pantry Check Items Per Call</th><th>Total Unique Calls</th><th>Total Unique Pantry Checks</th><th>Average Unique Pantry Check Per Unique Call</th><th>Total Unique Pantry Checks Items</th><th>Average Unique Pantry Check Items Per Unique Call</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td style=\"text-align: center;\">' + val['total_calls'] + '</td><td style=\"text-align: center;\">' + val['total_pantry_checks'] + '</td><td style=\"text-align: center;\">' + val['average_pantry_check_per_call'] + '</td><td style=\"text-align: center;\">' + val['total_pantry_checks_items'] + '</td><td style=\"text-align: center;\">' + val['average_pantry_check_items_per_call'] + '</td><td style=\"text-align: center;\">' + val['total_unique_calls'] + '</td><td style=\"text-align: center;\">' + val['total_unique_pantry_checks'] + '</td><td style=\"text-align: center;\">' + val['average_unique_pantry_check_per_unique_call'] + '</td><td style=\"text-align: center;\">' + val['total_unique_pantry_checks_items'] + '</td><td style=\"text-align: center;\">' + val['average_unique_pantry_check_items_per_unique_call'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_customers':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Customer Report</th></tr>';
                                //html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">OTM</th><th style=\"text-align: center;\"># of visited</th><th style=\"text-align: center;\">Total</th><th style=\"text-align: center;\">%</th></tr></thead><tbody>';
                                var cols = [];
                                if (result.length > 0) {
                                    html += '<tr style=\"background: #95b3d7;\"><th>#</th>';
                                    \$.each(result[0], function (key, val) {
                                        html += '<th>' + key + '</th>';
                                        cols.push(key);
                                    });
                                    html += '</tr>';
                                }
                                html += '</thead><tbody>';
                                \$.each(result, function (key, val) {
                                    //html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td style=\"text-align: center;\">' + val['otm_A_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_B_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_C_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_D_customer_count'] + '</td><td style=\"text-align: center;\">' + val['otm_A_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_B_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_C_fulfillment_rate'] + '</td><td style=\"text-align: center;\">' + val['otm_D_fulfillment_rate'] + '</td></tr>';
                                    html += '<tr><td>' + i + '</td>';
                                    \$.each(cols, function (k, v) {
                                        html += '<td';
                                        if (k > 2) html += ' style=\"text-align: center;\"';
                                        html += '>' + val[v] + '</td>';
                                    });
                                    html += '</tr>';
                                    i++;
                                });

                                html += '</tbody></table>';
                                break;
                            case 'report_call':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"40\">Call Report</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>#</th><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th>Call Made</th><th>Working Day</th><th>Actual Working Day</th><th>Average Call per Working Day</th><th>Average Call per Actual Working Day</th><th>TFO Made Inside Call</th><th>TFO Made Inside Call Value</th><th>TFO Made Outside Call</th><th>TFO Made Outside Call Value</th><th>Strike Rate</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + i + '</td><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td style=\"text-align: center;\">' + val['call_made'] + '</td><td style=\"text-align: center;\">' + val['working_day'] + '</td><td style=\"text-align: center;\">' + val['actual_working_day'] + '</td><td style=\"text-align: center;\">' + val['ave_call_per_working_day'] + '</td><td style=\"text-align: center;\">' + val['ave_call_per_actual_working_day'] + '</td><td style=\"text-align: center;\">' + val['tfo_made_inside_call'] + '</td><td style=\"text-align: center;\">' + val['tfo_made_inside_call_value'] + '</td><td style=\"text-align: center;\">' + val['tfo_made_outside_call'] + '</td><td style=\"text-align: center;\">' + val['tfo_made_outside_call_value'] + '</td><td style=\"text-align: center;\">' + val['strike_rate'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                                break;
                            /*case 'report_new_sku':
                             html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"5\">Report New SKU</th></tr>';
                             html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">OTM</th><th style=\"text-align: center;\"># of visited</th><th style=\"text-align: center;\">Total</th><th style=\"text-align: center;\">%</th></tr></thead><tbody>';
                             \$.each(result, function(key, val){
                             html += '<tr><td>&nbsp;</td><td style=\"text-align: center;\">' + val['customer_otm'] + '</td><td style=\"text-align: center;\">' + val['no_visited'] + '</td><td style=\"text-align: center;\">' + val['total'] + '</td><td style=\"text-align: center;\">' + val['percent'] + '</td></tr>';
                             });
                             html += '</tbody></table>';
                             break;*/
                            case 'report_calls_tracking':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"20\">TFO &amp; Call Records Tracking</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th># of TFO\\'s</th><th># of Calls</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['num_tfo'] + '</td><td>' + val['num_call'] + '</td></tr>';
                                    i++
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_sampling_tracking':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"20\">Sampling Tracking</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th># of Wet</th><th># of Dry</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['wet'] + '</td><td>' + val['dry'] + '</td></tr>';
                                    i++;
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_pantry_check_tracking':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"20\">Pantry Check Tracking</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th colspan=\"20\">Salesperson: ' + sp_too + '</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>AS2 Salespersons ID</th><th>Salesperson Name</th><th># of Pantry</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>' + val['armstrong_2_salespersons_id'] + '</td><td>' + val['salespersons_name'] + '</td><td>' + val['pantry'] + '</td></tr>';
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_strike_rate':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"5\">Strike Rate</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\"># of TFO\\'s</th><th style=\"text-align: center;\"># of TFO\\'s with Call Record</th><th style=\"text-align: center;\">Strike Rate</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>&nbsp;</td><td style=\"text-align: center;\">' + val['num_tfo'] + '</td><td style=\"text-align: center;\">' + val['num_tfo_calls'] + '</td><td style=\"text-align: center;\">' + val['strike_rate'] + '</td></tr>';
                                });
                                html += '</tbody></table>';
                                break;
                            case 'report_gripgrab':
                                html += '<table class=\"table table-report\"><thead><tr style=\"background: #e26b0a;\"><th colspan=\"5\">Grip n\\' Grab</th></tr>';
                                html += '<tr style=\"background: #95b3d7;\"><th>Salesperson: ' + sp_too + '</th><th style=\"text-align: center;\">New Grip</th><th style=\"text-align: center;\">New Grab</th></tr></thead><tbody>';
                                \$.each(result, function (key, val) {
                                    html += '<tr><td>&nbsp;</td><td style=\"text-align: center;\">' + val['new_grip'] + '</td><td style=\"text-align: center;\">' + val['new_grab'] + '</td></tr>';
                                });
                                html += '</tbody></table>';
                                break;

                        }
                        \$('#reporting-view').html(html);
                    }
                });
            }

        });
    </script>
";
    }

    // line 1623
    public function block_css($context, array $blocks = array())
    {
        // line 1624
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 1625
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 1626
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <style type=\"text/css\">
        #iframe_report, #iframe_report_zoom {
            width: 100%;
            border: none;
        }

        #reporting-view #iframe_report {
            min-height: 400px;
            display: none
        }

        #zoom_rp {
            display: none
        }

        #groups_btn_export {
            display: none;
        }

        #reporting-view .height_zoom {
            height: 700px
        }

        #mainContent {
            margin-top: 20px;
        }
    </style>
";
    }

    // line 1656
    public function block_content($context, array $blocks = array())
    {
        // line 1657
        echo "
    ";
        // line 1658
        echo form_open(site_url("ami/reports/generate"), array("class" => "form-horizontal"));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 1663
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "regional")) ? ("Regional ") : (""));
        echo "
                    Report ";
        // line 1664
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["reportLabels"]) ? $context["reportLabels"] : null), (isset($context["action"]) ? $context["action"] : null), array(), "array")) ? (("- " . $this->getAttribute((isset($context["reportLabels"]) ? $context["reportLabels"] : null), (isset($context["action"]) ? $context["action"] : null), array(), "array"))) : ("")), "html", null, true);
        echo "</h1>

                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div id=\"mainContent\">

                ";
        // line 1676
        if ((($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking_detail") || ($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "get_perfect_call_report_tracking"))) {
            // line 1677
            echo "                <div class=\"form-group\">
                    ";
            // line 1678
            echo form_label("Date Type", "input-year", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 1680
            echo form_dropdown("date_type", array("0" => "Season", "1" => "Date range"), 0, "class=\"form-control fetch-date-type\" id=\"input-date-type\"");
            echo "
                    </div>
                </div>
                ";
        }
        // line 1684
        echo "
                ";
        // line 1685
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["report"]) ? $context["report"] : null));
        foreach ($context['_seq'] as $context["_action"] => $context["data"]) {
            // line 1686
            echo "                    ";
            // line 1687
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "years")) {
                // line 1688
                echo "                        <div class=\"form-group\">
                            ";
                // line 1689
                echo form_label("Year", "input-year", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1691
                echo form_dropdown("input-year", select_year(2010), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "Y"), "method"), "class=\"form-control fetch-sl fetch-sku\" id=\"input-year\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1695
            echo "                    ";
            // line 1696
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "month_ranger")) {
                // line 1697
                echo "                        <div class=\"form-group\">
                            ";
                // line 1698
                echo form_label("Month ", "monthfrom", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-3\">
                                <select name=\"monthfrom\" id=\"monthfrom\" class=\"form-control\">
                                    ";
                // line 1701
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 12));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 1702
                    echo "                                        <option value=\"";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "\" >";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["months"]) ? $context["months"] : null), ($context["i"] - 1), array(), "array"), "html", null, true);
                    echo "</option>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1704
                echo "                                </select>
                            </div>
                            <div class=\"col-sm-3\">
                                <select name=\"monthto\" id=\"monthto\" class=\"form-control\">
                                    ";
                // line 1708
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 12));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 1709
                    echo "                                        <option value=\"";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "\" >";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["months"]) ? $context["months"] : null), ($context["i"] - 1), array(), "array"), "html", null, true);
                    echo "</option>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1711
                echo "                                </select>
                            </div>
                        </div>
                    ";
            }
            // line 1715
            echo "                    ";
            // line 1729
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "country_list")) {
                // line 1730
                echo "                         <div class=\"form-group\">
                            ";
                // line 1731
                echo form_label("Country ", "country_list", array("class" => "control-label col-sm-3"));
                echo "

                            <div class=\"col-sm-6\">
                                <select name=\"country_list\" id=\"input-country_list\"
                                        class=\"form-control fetch-sl fetch-sl3\">
                                    ";
                // line 1736
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["c_tries"]) ? $context["c_tries"] : null));
                foreach ($context['_seq'] as $context["c_id"] => $context["c_name"]) {
                    // line 1737
                    echo "                                        <option value=\"";
                    echo twig_escape_filter($this->env, $context["c_id"], "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) == $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "id", array()))) ? ("selected") : (""));
                    echo ">";
                    echo twig_escape_filter($this->env, $context["c_name"], "html", null, true);
                    echo "</option>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['c_id'], $context['c_name'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1739
                echo "                                </select>
                            </div>
                        </div>
                    ";
            }
            // line 1743
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "multi_country_list")) {
                // line 1744
                echo "                        <div class=\"form-group\">
                            ";
                // line 1745
                echo form_label("Country ", "country_list", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1748
                echo "                                <select name=\"country_list[]\" class=\"form-control\" id=\"multiCountriesList\"
                                        multiple=\"multiple\" style=\"min-height: 250px;\">
                                    ";
                // line 1750
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["c_tries"]) ? $context["c_tries"] : null));
                foreach ($context['_seq'] as $context["c_id"] => $context["c_name"]) {
                    // line 1751
                    echo "                                        <option value=\"";
                    echo twig_escape_filter($this->env, $context["c_id"], "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) == $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "id", array()))) ? ("selected") : (""));
                    echo ">";
                    echo twig_escape_filter($this->env, $context["c_name"], "html", null, true);
                    echo "</option>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['c_id'], $context['c_name'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1753
                echo "                                </select>
                            </div>
                        </div>
                    ";
            }
            // line 1757
            echo "

                    ";
            // line 1759
            if (($this->getAttribute($context["data"], "type", array()) == "season")) {
                // line 1760
                echo "                        <div class=\"form-group\">
                            ";
                // line 1761
                echo form_label("Season", "active", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1763
                echo form_dropdown("season", $this->getAttribute($context["data"], "prizes_setting", array()), null, "class=\"form-control fetch-sl\" ");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1767
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "datepicker")) {
                // line 1768
                echo "                        <div class=\"form-group ";
                echo ((((isset($context["action"]) ? $context["action"] : null) == "get_app_versions_report")) ? ("hide") : (""));
                echo "\">
                            ";
                // line 1769
                echo form_label("From *", "from", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1771
                echo form_input(array("name" => "from", "value" => $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "class" => "form-control fetch-sl", "data-provide" => "datepicker", "data-date-format" => "yyyy-mm-dd"));
                // line 1775
                echo "
                            </div>

                            <div class=\"com-sm-1\">
                                <button type=\"button\" class=\"btn btn-default btn-small btn-clear\">Clear</button>
                            </div>
                        </div>

                        <div class=\"form-group ";
                // line 1783
                echo ((((isset($context["action"]) ? $context["action"] : null) == "get_app_versions_report")) ? ("hide") : (""));
                echo "\">
                            ";
                // line 1784
                echo form_label("To *", "to", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1786
                echo form_input(array("name" => "to", "value" => $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "class" => "form-control fetch-sl", "data-provide" => "datepicker", "data-date-format" => "yyyy-mm-dd"));
                // line 1790
                echo "
                            </div>

                            <div class=\"com-sm-1\">
                                <button type=\"button\" class=\"btn btn-default btn-small btn-clear\">Clear</button>
                            </div>
                        </div>
                    ";
            }
            // line 1798
            echo "
                    ";
            // line 1799
            if (($this->getAttribute($context["data"], "type", array()) == "multi_select")) {
                // line 1800
                echo "                        ";
                if ($this->getAttribute($context["data"], "list", array())) {
                    // line 1801
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1802
                    echo form_label(twig_capitalize_string_filter($this->env, strtr($this->getAttribute($context["data"], "col", array()), array("_" => " "))), $this->getAttribute($context["data"], "col", array()), array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1804
                    echo form_dropdown(($this->getAttribute($context["data"], "col", array()) . "[]"), $this->getAttribute($context["data"], "list", array()), $this->getAttribute($context["data"], "selected", array()), (("class=\"form-control\" id=\"" . $this->getAttribute(                    // line 1805
$context["data"], "col", array())) . "\" multiple=\"multiple\" style=\"height: 250px;\""));
                    // line 1806
                    echo "
                                    <p class=\"help-block\">Select multiple by holding CTRL or SHIFT while
                                        clicking.</p>
                                </div>
                            </div>
                        ";
                }
                // line 1812
                echo "                    ";
            }
            // line 1813
            echo "
                    ";
            // line 1814
            if (($this->getAttribute($context["data"], "type", array()) == "month-year")) {
                // line 1815
                echo "                        ";
                if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "data_ssd_mt")) {
                    // line 1816
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1817
                    echo form_label("SSD Month", "month", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1819
                    echo form_dropdown("month", select_month(), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "m"), "method"), "class=\"form-control fetch-sl fetch-sku\" id=\"input-month\"");
                    echo "
                                </div>
                            </div>

                            <div class=\"form-group\">
                                ";
                    // line 1824
                    echo form_label("SSD Year", "year", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1826
                    echo form_dropdown("year", select_year(2014), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "Y"), "method"), "class=\"form-control fetch-sl fetch-sku\" id=\"input-year\"");
                    echo "
                                </div>
                            </div>
                        ";
                } else {
                    // line 1830
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1831
                    echo form_label("Month", "month", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1833
                    echo form_dropdown("month", select_month(), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "m"), "method"), "class=\"form-control fetch-sl fetch-sku\" id=\"input-month\"");
                    echo "
                                </div>
                            </div>

                            <div class=\"form-group\">
                                ";
                    // line 1838
                    echo form_label("Year", "year", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1840
                    echo form_dropdown("year", select_year(2010), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "Y"), "method"), "class=\"form-control fetch-sl fetch-sku\" id=\"input-year\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1844
                echo "                    ";
            }
            // line 1845
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "number_sku")) {
                // line 1846
                echo "                        <div class=\"form-group\">
                            ";
                // line 1847
                echo form_label("Number Sku", "number_sku", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1849
                echo form_dropdown("number_sku", array(), 0, "class=\"form-control\" id=\"input-number-sku\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1853
            echo "
                    ";
            // line 1854
            if (($this->getAttribute($context["data"], "type", array()) == "note_type")) {
                // line 1855
                echo "                        <div class=\"form-group\">
                            ";
                // line 1856
                echo form_label("Note Types", "note_type", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1858
                echo form_dropdown("note_type", $this->getAttribute($context["data"], "list", array()), 1, "class=\"form-control\" id=\"note_type\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1862
            echo "
                    ";
            // line 1863
            if (($this->getAttribute($context["data"], "type", array()) == "report_type")) {
                // line 1864
                echo "                        <div class=\"form-group\">
                            ";
                // line 1865
                echo form_label("Report Type", "report_type", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1867
                echo form_dropdown("report_type", $this->getAttribute($context["data"], "list", array()), 1, "class=\"form-control\" id=\"report_type\"");
                echo "
                            </div>
                        </div>

                        <div class=\"form-group\" style=\"display: none;\">
                            ";
                // line 1872
                echo form_label("Customer Status", "c_status", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1874
                echo form_dropdown("c_status", array("" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control\" id=\"customer-status\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1878
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "salespersons_status")) {
                // line 1879
                echo "                        <div class=\"form-group\">
                            ";
                // line 1880
                echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1882
                echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1886
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "app_type")) {
                // line 1887
                echo "
                        ";
                // line 1888
                if ((isset($context["slStatus"]) ? $context["slStatus"] : null)) {
                    // line 1889
                    echo "                            ";
                    if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "customer_profiling_transaction")) {
                        // line 1890
                        echo "                                <div class=\"form-group\">
                                    ";
                        // line 1891
                        echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                        echo "
                                    <div class=\"col-sm-6\">
                                        ";
                        // line 1893
                        echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                        echo "
                                    </div>
                                </div>
                            ";
                    } elseif (($this->getAttribute(                    // line 1896
(isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "free_gift_report")) {
                        // line 1897
                        echo "                                <div class=\"form-group\">
                                    ";
                        // line 1898
                        echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                        echo "
                                    <div class=\"col-sm-6\">
                                        ";
                        // line 1900
                        echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"),  -1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                        echo "
                                    </div>
                                </div>
                            ";
                    } else {
                        // line 1904
                        echo "                                <div class=\"form-group\">
                                    ";
                        // line 1905
                        echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                        echo "
                                    <div class=\"col-sm-6\">
                                        ";
                        // line 1907
                        echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                        echo "
                                    </div>
                                </div>
                            ";
                    }
                    // line 1911
                    echo "                        ";
                }
                // line 1912
                echo "
                        ";
                // line 1913
                if ((isset($context["cStatus"]) ? $context["cStatus"] : null)) {
                    // line 1914
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1915
                    echo form_label("Customer Status", "c_status", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1917
                    echo form_dropdown("c_status", array("" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control\" id=\"customer-status\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1921
                echo "
                        ";
                // line 1922
                if ((isset($context["syncReportType"]) ? $context["syncReportType"] : null)) {
                    // line 1923
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1924
                    echo form_label("Type", "syncReportType", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1926
                    echo form_dropdown("syncReportType", array("1" => "Sales Team", "000" => "Calls"), 1, "class=\"form-control\" id=\"syncReportType\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1930
                echo "
                        ";
                // line 1931
                if ((isset($context["syncReportView"]) ? $context["syncReportView"] : null)) {
                    // line 1932
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1933
                    echo form_label("View", "syncReportView", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1935
                    echo form_dropdown("syncReportView", array("1" => "Yearly", "2" => "Quarterly", "0" => "Monthly"), 0, "class=\"form-control\" id=\"syncReportView\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1939
                echo "
                        ";
                // line 1940
                if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "customer_profiling_transaction")) {
                    // line 1941
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1942
                    echo form_label("App Type", "app_type", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1944
                    echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\"form-control fetch-sl\" id=\"input-app-type\"");
                    echo "
                                </div>
                            </div>
                        ";
                } else {
                    // line 1948
                    echo "                            ";
                    if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "email_report")) {
                        // line 1949
                        echo "                                <div class=\"form-group\">
                                    ";
                        // line 1950
                        echo form_label("Sent status", "sent", array("class" => "control-label col-sm-3"));
                        echo "
                                    <div class=\"col-sm-6\">
                                        ";
                        // line 1952
                        echo form_dropdown("sent", array("all" => "All", "0" => "Sent", "1" => "Wait", "-1" => "Error"), null, "class=\"form-control fetch-sl\" id=\"sent\"");
                        echo "
                                    </div>
                                </div>
                            ";
                    }
                    // line 1956
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1957
                    echo form_label("App Type", "app_type", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1959
                    echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\"form-control fetch-sl\" id=\"input-app-type\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1963
                echo "
                        ";
                // line 1964
                if ((isset($context["viewFilter"]) ? $context["viewFilter"] : null)) {
                    // line 1965
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1966
                    echo form_label("View", "viewFilter", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1968
                    echo form_dropdown("viewFilter", array("0" => "Checklist", "1" => "Details"), 0, "class=\"form-control\" id=\"viewFilter\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1972
                echo "                    ";
            }
            // line 1973
            echo "
                    ";
            // line 1974
            if (($this->getAttribute($context["data"], "type", array()) == "multi_app_type")) {
                // line 1975
                echo "
                        ";
                // line 1976
                if ((isset($context["slStatus"]) ? $context["slStatus"] : null)) {
                    // line 1977
                    echo "                            <div class=\"form-group\">
                                ";
                    // line 1978
                    echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                    echo "
                                <div class=\"col-sm-6\">
                                    ";
                    // line 1980
                    echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                    echo "
                                </div>
                            </div>
                        ";
                }
                // line 1984
                echo "
                        <div class=\"form-group\">
                            ";
                // line 1986
                echo form_label("App Type", "multi_app_type", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 1988
                if (((isset($context["action"]) ? $context["action"] : null) == "coaching_note")) {
                    // line 1989
                    echo "                                ";
                    echo form_dropdown("multi_app_type[]", array("PULL" => "PULL", "PUSH" => "PUSH"), "PULL", "class=\"form-control fetch-sl\" id=\"input-app-type\" ");
                    echo "
                                ";
                } else {
                    // line 1991
                    echo "                                ";
                    echo form_dropdown("multi_app_type[]", (isset($context["app_type"]) ? $context["app_type"] : null), "PULL", "class=\"form-control fetch-sl\" id=\"input-app-type\" multiple");
                    echo "
                                ";
                }
                // line 1993
                echo "                            </div>
                        </div>
                    ";
            }
            // line 1996
            echo "
                    ";
            // line 1997
            if (($this->getAttribute($context["data"], "type", array()) == "grip_customers")) {
                // line 1998
                echo "                        <div class=\"form-group\">
                            ";
                // line 1999
                echo form_label("Customers", "grip_customers", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2001
                echo form_dropdown("customers", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "grip_customers", array()), "list", array()), null, "class=\"form-control\" id=\"gripCustomers\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2005
            echo "
                    ";
            // line 2006
            if (($this->getAttribute($context["data"], "type", array()) == "show_graph")) {
                // line 2007
                echo "                        <div class=\"form-group\">
                            ";
                // line 2008
                echo form_label("Output Format", "show_graph", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2010
                echo form_dropdown("showGraph", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "show_graph", array()), "list", array()), null, "class=\"form-control\" id=\"showGraph\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2014
            echo "
                    ";
            // line 2015
            if (($this->getAttribute($context["data"], "type", array()) == "managerId")) {
                // line 2016
                echo "                        <div class=\"form-group\">
                            ";
                // line 2017
                echo form_label("Sale Leader", "managerId", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2020
                echo "                                ";
                echo form_dropdown("managerId", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "managers", array()), "list", array()), null, "class=\"form-control fetch-sl\" id=\"input-sale-leader\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2024
            echo "
                    ";
            // line 2025
            if (($this->getAttribute($context["data"], "type", array()) == "customer_channel")) {
                // line 2026
                echo "                        <div class=\"form-group\">
                            ";
                // line 2027
                echo form_label("Customer Channel", "customer_channel", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2029
                echo form_dropdown("customer_channel", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "customer_channel", array()), "list", array()), null, "class=\"form-control\" id=\"customerChannel\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2033
            echo "                    ";
            // line 2034
            echo "                    ";
            if (($this->getAttribute($context["data"], "type", array()) == "top_sku")) {
                // line 2035
                echo "                        <div class=\"topProduct form-group\">
                            ";
                // line 2036
                echo form_label("Top SKU", "top_products", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2038
                echo form_dropdown("top_products[]", array(), 0, "class=\"form-control\" id=\"top-products-sku\"  multiple=\"multiple\" style=\"height: 200px;\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2042
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_action'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2043
        echo "

                ";
        // line 2045
        if ((isset($context["tfoSource"]) ? $context["tfoSource"] : null)) {
            // line 2046
            echo "                    <div class=\"form-group\">
                        ";
            // line 2047
            echo form_label("Source", "tfoSource", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 2049
            if (((isset($context["action"]) ? $context["action"] : null) != "data_tfo")) {
                // line 2050
                echo "                                ";
                echo form_dropdown("tfoSource", array("1" => "TFO"), 1, "class=\"form-control\" id=\"tfoSource\"");
                echo "
                            ";
            } else {
                // line 2052
                echo "                            ";
                echo form_dropdown("tfoSource", array("1" => "TFO", "2" => "Pantry", "3" => "TFO + Pantry"), 1, "class=\"form-control\" id=\"tfoSource\"");
                echo "
                            ";
            }
            // line 2054
            echo "                        </div>
                    </div>
                ";
        }
        // line 2057
        echo "
                ";
        // line 2058
        if ((isset($context["templateType"]) ? $context["templateType"] : null)) {
            // line 2059
            echo "                    <div class=\"form-group\">
                        ";
            // line 2060
            echo form_label("Source", "templateType", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 2062
            echo form_dropdown("templateType", array("1" => "Cash & Carry Template", "2" => "Default Perfect Store", "3" => "MY Perfect Store Template", "4" => "MY Cash & Carry Template"), 1, "class=\"form-control\" id=\"templateType\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 2066
        echo "
                ";
        // line 2067
        if ((isset($context["productStatus"]) ? $context["productStatus"] : null)) {
            // line 2068
            echo "                    <div class=\"form-group\">
                        ";
            // line 2069
            echo form_label("SKU Status", "productStatus", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 2071
            echo form_dropdown("productStatus", array("-1" => "All", "1" => "Active", "0" => "Inactive"),  -1, "class=\"form-control\" id=\"productStatus\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 2075
        echo "
                ";
        // line 2084
        echo "
                ";
        // line 2085
        if (twig_in_filter((isset($context["action"]) ? $context["action"] : null), $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "custom", array(), "array"))) {
            // line 2086
            echo "                    ";
            $this->loadTemplate((("ami/reports/custom/" . (isset($context["action"]) ? $context["action"] : null)) . ".twig.html"), "ami/reports/index.html.twig", 2086)->display($context);
            // line 2087
            echo "                ";
        }
        // line 2088
        echo "
                ";
        // line 2089
        if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "free_gift_report")) {
            // line 2090
            echo "                    <div class=\"form-group\">
                        ";
            // line 2091
            echo form_label("Export Type", "typeFile", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 2093
            echo form_dropdown("typeFile", array("csv" => "CSV", "pdf" => "PDF"), null, "class=\"form-control\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 2097
        echo "
                ";
        // line 2098
        if ((isset($context["slStatus"]) ? $context["slStatus"] : null)) {
            // line 2099
            echo "                    ";
            if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "potential_customers_report")) {
                // line 2100
                echo "                        <div class=\"form-group\">
                            ";
                // line 2101
                echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-3"));
                echo "
                            <div class=\"col-sm-6\">
                                ";
                // line 2103
                echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"), 1, "class=\"form-control fetch-sl\" id=\"input-sl-status\"");
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 2107
            echo "                ";
        }
        // line 2108
        echo "
                ";
        // line 2130
        echo "                ";
        // line 2131
        echo "                ";
        // line 2132
        echo "                ";
        // line 2133
        echo "                ";
        // line 2134
        echo "                ";
        // line 2135
        echo "                ";
        // line 2136
        echo "                ";
        // line 2137
        echo "                ";
        // line 2138
        echo "                ";
        if (($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) == "data_product")) {
            // line 2139
            echo "                    <div class=\"form-group\">
                        ";
            // line 2140
            echo form_label("Filter", "filter", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 2142
            echo form_dropdown("filter_listed", $this->getAttribute((isset($context["report"]) ? $context["report"] : null), "filter", array()), null, "class=\"form-control fetch-sl\" id=\"filter_listed\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 2146
        echo "            </div>

            <input type=\"hidden\" name=\"type\" value=\"";
        // line 2148
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" name=\"sub_type\" value=\"\"/>
            <input type=\"hidden\" name=\"filter\" value=\"";
        // line 2150
        echo twig_escape_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null), "html", null, true);
        echo "\"/>

            <div class=\"form-group\">
                <div class=\"col-sm-19 ";
        // line 2153
        echo ((($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "action_url", array()) != "data_product")) ? ("col-sm-offset-3") : ("col-sm-offset-3"));
        echo "\">

                    <button type=\"reset\" class=\"btn btn-default\"><i class=\"fa fw fa-times\"></i> Reset</button>
                    <button class=\"btn btn-primary btn-view-report\" ";
        // line 2156
        echo (((!twig_in_filter((isset($context["action"]) ? $context["action"] : null), array(0 => "data_customers")) && $this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_view", array()))) ? ("") : ("disabled"));
        echo ">
                        <i class=\"fa fw fa-eye\"></i> View
                    </button>
                    <button type=\"submit\" class=\"btn btn-primary\" ";
        // line 2159
        echo (($this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_export", array())) ? ("") : ("disabled"));
        echo "><i
                                class=\"fa fw fa-download\"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 2167
        echo form_close();
        echo "

    <button id=\"zoom_rp\" class=\"btn btn-mini btn-zoom-report\" onclick=\"zoom_report()\"><i class=\"icon-zoom-in\"></i>&nbsp;Zoom
    </button>
    <div id=\"g_chart\" style=\"padding-left: 20px; border:1px solid black; display: none; margin: 0 auto;\">
        <div class=\"loading\"></div>
    </div>

    <div id=\"reporting-view\" style=\"padding-left: 20px;\">
        <!-- reports get generated here -->
        <div class=\"loading\"></div>
        <iframe id=\"iframe_report\"></iframe>
    </div>



";
    }

    public function getTemplateName()
    {
        return "ami/reports/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2888 => 2167,  2877 => 2159,  2871 => 2156,  2865 => 2153,  2859 => 2150,  2854 => 2148,  2850 => 2146,  2843 => 2142,  2838 => 2140,  2835 => 2139,  2832 => 2138,  2830 => 2137,  2828 => 2136,  2826 => 2135,  2824 => 2134,  2822 => 2133,  2820 => 2132,  2818 => 2131,  2816 => 2130,  2813 => 2108,  2810 => 2107,  2803 => 2103,  2798 => 2101,  2795 => 2100,  2792 => 2099,  2790 => 2098,  2787 => 2097,  2780 => 2093,  2775 => 2091,  2772 => 2090,  2770 => 2089,  2767 => 2088,  2764 => 2087,  2761 => 2086,  2759 => 2085,  2756 => 2084,  2753 => 2075,  2746 => 2071,  2741 => 2069,  2738 => 2068,  2736 => 2067,  2733 => 2066,  2726 => 2062,  2721 => 2060,  2718 => 2059,  2716 => 2058,  2713 => 2057,  2708 => 2054,  2702 => 2052,  2696 => 2050,  2694 => 2049,  2689 => 2047,  2686 => 2046,  2684 => 2045,  2680 => 2043,  2674 => 2042,  2667 => 2038,  2662 => 2036,  2659 => 2035,  2656 => 2034,  2654 => 2033,  2647 => 2029,  2642 => 2027,  2639 => 2026,  2637 => 2025,  2634 => 2024,  2626 => 2020,  2621 => 2017,  2618 => 2016,  2616 => 2015,  2613 => 2014,  2606 => 2010,  2601 => 2008,  2598 => 2007,  2596 => 2006,  2593 => 2005,  2586 => 2001,  2581 => 1999,  2578 => 1998,  2576 => 1997,  2573 => 1996,  2568 => 1993,  2562 => 1991,  2556 => 1989,  2554 => 1988,  2549 => 1986,  2545 => 1984,  2538 => 1980,  2533 => 1978,  2530 => 1977,  2528 => 1976,  2525 => 1975,  2523 => 1974,  2520 => 1973,  2517 => 1972,  2510 => 1968,  2505 => 1966,  2502 => 1965,  2500 => 1964,  2497 => 1963,  2490 => 1959,  2485 => 1957,  2482 => 1956,  2475 => 1952,  2470 => 1950,  2467 => 1949,  2464 => 1948,  2457 => 1944,  2452 => 1942,  2449 => 1941,  2447 => 1940,  2444 => 1939,  2437 => 1935,  2432 => 1933,  2429 => 1932,  2427 => 1931,  2424 => 1930,  2417 => 1926,  2412 => 1924,  2409 => 1923,  2407 => 1922,  2404 => 1921,  2397 => 1917,  2392 => 1915,  2389 => 1914,  2387 => 1913,  2384 => 1912,  2381 => 1911,  2374 => 1907,  2369 => 1905,  2366 => 1904,  2359 => 1900,  2354 => 1898,  2351 => 1897,  2349 => 1896,  2343 => 1893,  2338 => 1891,  2335 => 1890,  2332 => 1889,  2330 => 1888,  2327 => 1887,  2324 => 1886,  2317 => 1882,  2312 => 1880,  2309 => 1879,  2306 => 1878,  2299 => 1874,  2294 => 1872,  2286 => 1867,  2281 => 1865,  2278 => 1864,  2276 => 1863,  2273 => 1862,  2266 => 1858,  2261 => 1856,  2258 => 1855,  2256 => 1854,  2253 => 1853,  2246 => 1849,  2241 => 1847,  2238 => 1846,  2235 => 1845,  2232 => 1844,  2225 => 1840,  2220 => 1838,  2212 => 1833,  2207 => 1831,  2204 => 1830,  2197 => 1826,  2192 => 1824,  2184 => 1819,  2179 => 1817,  2176 => 1816,  2173 => 1815,  2171 => 1814,  2168 => 1813,  2165 => 1812,  2157 => 1806,  2155 => 1805,  2154 => 1804,  2149 => 1802,  2146 => 1801,  2143 => 1800,  2141 => 1799,  2138 => 1798,  2128 => 1790,  2126 => 1786,  2121 => 1784,  2117 => 1783,  2107 => 1775,  2105 => 1771,  2100 => 1769,  2095 => 1768,  2092 => 1767,  2085 => 1763,  2080 => 1761,  2077 => 1760,  2075 => 1759,  2071 => 1757,  2065 => 1753,  2052 => 1751,  2048 => 1750,  2044 => 1748,  2039 => 1745,  2036 => 1744,  2033 => 1743,  2027 => 1739,  2014 => 1737,  2010 => 1736,  2002 => 1731,  1999 => 1730,  1996 => 1729,  1994 => 1715,  1988 => 1711,  1977 => 1709,  1973 => 1708,  1967 => 1704,  1956 => 1702,  1952 => 1701,  1946 => 1698,  1943 => 1697,  1940 => 1696,  1938 => 1695,  1931 => 1691,  1926 => 1689,  1923 => 1688,  1920 => 1687,  1918 => 1686,  1914 => 1685,  1911 => 1684,  1904 => 1680,  1899 => 1678,  1896 => 1677,  1894 => 1676,  1879 => 1664,  1875 => 1663,  1867 => 1658,  1864 => 1657,  1861 => 1656,  1828 => 1626,  1824 => 1625,  1819 => 1624,  1816 => 1623,  1527 => 1339,  1522 => 1337,  1188 => 1006,  1178 => 999,  912 => 735,  888 => 713,  886 => 712,  864 => 693,  852 => 684,  847 => 682,  843 => 681,  832 => 672,  821 => 663,  819 => 662,  813 => 659,  806 => 657,  747 => 601,  732 => 588,  716 => 574,  697 => 557,  695 => 556,  676 => 540,  669 => 536,  663 => 533,  644 => 518,  641 => 516,  639 => 515,  637 => 514,  613 => 492,  607 => 489,  561 => 445,  557 => 443,  555 => 442,  513 => 402,  509 => 400,  507 => 399,  500 => 395,  457 => 354,  453 => 352,  451 => 351,  444 => 347,  400 => 306,  326 => 235,  304 => 215,  300 => 213,  298 => 212,  222 => 139,  217 => 137,  194 => 116,  177 => 102,  170 => 98,  163 => 93,  161 => 92,  157 => 90,  141 => 76,  139 => 75,  75 => 14,  69 => 12,  64 => 11,  60 => 10,  56 => 9,  52 => 8,  48 => 7,  43 => 6,  40 => 5,  34 => 3,  31 => 2,  11 => 1,);
    }
}
