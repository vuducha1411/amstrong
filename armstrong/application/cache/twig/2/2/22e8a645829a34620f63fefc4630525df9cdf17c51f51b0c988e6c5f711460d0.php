<?php

/* ami/setting_game/setting_index.html.twig */
class __TwigTemplate_22e8a645829a34620f63fefc4630525df9cdf17c51f51b0c988e6c5f711460d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_game/setting_index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 13
        echo twig_escape_filter($this->env, site_url("ami/setting_game/integrate_with_applications/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'desc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 25
    public function block_css($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "    <form action=\"\">
        <div class=\"row row sticky sticky-h1 bg-white\">
            <div class=\"col-lg-12\">
                <div class=\"page-header nm\">
                    <h1 class=\"pull-left\">Game Play</h1>

                    <div class=\"text-right\">
                        <div class=\"btn-header-toolbar\">
                            ";
        // line 39
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", "setting_game"))) {
            // line 40
            echo "                                ";
            echo html_btn(site_url("ami/setting_game/play_setting"), "<i class=\"fa fa-fw fa-plus\"></i> Add</a>", array("class" => "btn-default"));
            echo "
                            ";
        }
        // line 42
        echo "                        </div>
                    </div>
                    <div class=\"clearfix\"></div>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class=\"row m-t-md\">
            <div class=\"col-lg-12\">
                <div class=\"panel panel-default\">

                    <div class=\"panel-body\">
                        <div class=\"\">
                            <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">

                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Season</th>
                                    <th>Date Start</th>
                                    <th>Date Expired</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort\"></th>
                                </tr>
                                </thead>
                                <tbody>
                                ";
        // line 69
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["seasons"]) ? $context["seasons"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["season"]) {
            // line 70
            echo "                                    <tr>
                                        <td class=\"center\">";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "id", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">
                                            <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, site_url(("ami/setting_game/play_setting/" . $this->getAttribute($context["season"], "id", array()))), "html", null, true);
            echo "\"
                                               title=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "season", array()), "html", null, true);
            echo "\"
                                               data-toggle=\"ajaxModal\">";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "season", array()), "html", null, true);
            echo "</a>
                                        </td>
                                        <td class=\"center\">";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "date_start", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "date_expired", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"text-center\">
                                            <div class=\"btn-group\">
                                                ";
            // line 83
            if ($this->getAttribute($context["season"], "is_current_season", array())) {
                // line 84
                echo "                                                    <a href=\"play_setting/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["season"], "id", array()), "html", null, true);
                echo "\"
                                                       class=\"btn btn-default edit\" title=\"Edit\"><i
                                                                class=\"fa fa-edit\"></i></a>
                                                ";
            }
            // line 88
            echo "                                            </div>
                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['season'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "                                </tbody>
                            </table>
                        </div>
                        <!-- /. -->
                    </div>
                    <!-- /.panel-body -->

                </div>
            </div>
        </div>
    </form>
";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/setting_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 92,  184 => 88,  176 => 84,  174 => 83,  168 => 80,  164 => 79,  160 => 78,  156 => 77,  151 => 75,  147 => 74,  143 => 73,  138 => 71,  135 => 70,  131 => 69,  102 => 42,  96 => 40,  94 => 39,  84 => 31,  81 => 30,  75 => 27,  70 => 26,  67 => 25,  52 => 13,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
