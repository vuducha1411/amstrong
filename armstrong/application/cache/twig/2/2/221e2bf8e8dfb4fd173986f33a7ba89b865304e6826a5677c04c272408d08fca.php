<?php

/* ami/distributors/edit.html.twig */
class __TwigTemplate_221e2bf8e8dfb4fd173986f33a7ba89b865304e6826a5677c04c272408d08fca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/distributors/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/customer-chain.js"), "html", null, true);
        echo "\"></script>
    <script xmlns=\"http://www.w3.org/1999/html\">
        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        //mother
        var armstrong_2_related_mother_id = '";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_related_mother_id", array()), "html", null, true);
        echo "';
        \$('select[name=\"armstrong_2_related_mother_id\"]').change(function(){
            armstrong_2_related_mother_id = \$('select[name=\"armstrong_2_related_mother_id\"]').val();
        });
        function getRelatedMother()
        {
            var armstrong_2_salespersons = '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_salespersons_id\"]').val();
            armstrong_2_salespersons += '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_secondary_salespersons_id\"]').val();
            armstrong_2_salespersons += '&is_mother=1';
            armstrong_2_salespersons += '&type_cus=distributors';
            armstrong_2_salespersons += '&action=related_mother';
            armstrong_2_salespersons += '&id_current='+\$('input[name=\"armstrong_2_distributors_id\"]').val();
            \$.ajax({
                url: \"";
        // line 26
        echo twig_escape_filter($this->env, site_url("ami/customers/getCustomerBySalespersons"), "html", null, true);
        echo "\",
                data: armstrong_2_salespersons,
                type: 'POST',
                dataType: 'html',
                success: function (html) {
                    \$('select[name=\"armstrong_2_related_mother_id\"]').html(html);
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[selected=\"selected\"]').removeAttr('selected');
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[value=\"'+armstrong_2_related_mother_id+'\"]').attr('selected', 'selected');
                }
            });
        }
        \$('select[name=\"armstrong_2_salespersons_id\"], select[name=\"armstrong_2_secondary_salespersons_id').change(function(){
            getRelatedMother();
        });
        getRelatedMother();
        \$('input[name=\"is_mother\"]').change(function(){
            console.log(\$('input[name=\"is_mother\"]').val());
            if(\$('input[name=\"is_mother\"]:checked').val() == 1)
            {
                \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').hide();
            }
            else
            {
                \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').show();
            }
        });
        \$('input[name=\"is_mother\"]').trigger('change');
        //end mother
        \$(function () {
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 63
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + toTitleCase(res['value'][key]) + '\">' + toTitleCase(res['value'][key]) + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            function toggle(obj) {
                var \$input = \$(obj);

                if (\$input.prop('checked')){
                    document.getElementById(\"disableLine\").value = \"1\";
                } 
                else {
                    document.getElementById(\"disableLine\").value = \"0\";
                };
            }

            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        weightage = \$this.find('option:selected').data('weightage') || 0;

                \$('#ChannelWeightage').val(weightage);
            });

            \$('#InputSalespersons').autocomplete({
                source: \"";
        // line 109
        echo twig_escape_filter($this->env, site_url("ami/salespersons/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputSalespersonsId').val(ui.item.value);
                }
            }).on('focus', function () {
                this.select();
            });
            \$(document).ready(function () {
                var approved = '";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()), "html", null, true);
        echo "',
                        active = '";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "active", array()), "html", null, true);
        echo "',
                        optionCloseDown = \$('#approved option[value=\"-2\"]'),
                        optionReject = \$('#approved option[value=\"-1\"]'),
                        optionApproved = \$('#approved option[value=\"1\"]');

                if (active == 1) {
                    if (approved == -2 || approved == -1) {
                        \$('#approved').val('');
                    }
                    optionApproved.show();
                    optionCloseDown.hide();
                    optionReject.hide();
                } else {
                    if (approved == 1) {
                        \$('#approved').val('');
                    }
                    optionApproved.hide();
                    optionCloseDown.show();
                    optionReject.show();
                }

                \$('input[name=\"active\"]').on('change', function () {
                    var \$val = \$(this).val();
                    \$('#approved').val('');
                    if (\$val == 1) {
                        optionApproved.show();
                        optionCloseDown.hide();
                        optionReject.hide();
                    } else {
                        optionApproved.hide();
                        optionCloseDown.show();
                        optionReject.show();
                    }
                });
            });
        });
    </script>
";
    }

    // line 160
    public function block_content($context, array $blocks = array())
    {
        // line 161
        echo "
    ";
        // line 162
        echo form_open(site_url("ami/distributors/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_distributors_id" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Distributors</h1>

                <div class=\"text-right\">
                    ";
        // line 170
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/distributors/edit.html.twig", 170)->display(array_merge($context, array("url" => "ami/distributors", "id" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()), "permission" => "distributor")));
        // line 171
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 181
        echo form_label("Armstrong 1 distributors id", "armstrong_1_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 183
        echo form_input(array("name" => "armstrong_1_distributors_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_1_distributors_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 188
        echo form_label("Armstrong 2 distributors id", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 190
        echo form_input(array("name" => "armstrong_2_distributors_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()), "class" => "form-control", "disabled" => "disabled"));
        echo "
                </div>
            </div>
            ";
        // line 194
        echo "            ";
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) == 4)) {
            // line 195
            echo "            <div class=\"form-group\">
                ";
            // line 196
            echo form_label("SIFU ID", "sifu_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 198
            echo form_input(array("name" => "sifu_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "sifu_id", array()), "class" => "form-control"));
            echo "
                </div>
            </div>
            ";
        }
        // line 202
        echo "
            <div class=\"form-group\">
                ";
        // line 204
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 206
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

                <div class=\"form-group\">
                    ";
        // line 211
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 213
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" ");
        echo "
                    </div>
                </div>

            <div class=\"form-group\">
                ";
        // line 218
        echo form_label("Armstrong 2 Secondary Salespersons", "armstrong_2_secondary_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 220
        echo form_dropdown("armstrong_2_secondary_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_secondary_salespersons_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 224
        echo form_label("Is Mother", "is_mother", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 226
        echo form_radio("is_mother", 1, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "is_mother", array()) == 1));
        echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 227
        echo form_radio("is_mother", 0, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "is_mother", array()) == 0));
        echo " No</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 231
        echo form_label("Related Mother", "armstrong_2_related_mother_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 233
        echo form_dropdown("armstrong_2_related_mother_id", (isset($context["related_mother"]) ? $context["related_mother"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_related_mother_id", array()), "class=\"form-control\" data-type=\"distributors\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 237
        echo form_label("Armstrong 2 Chains Id", "armstrong_2_chains_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 239
        echo form_dropdown("armstrong_2_chains_id", (isset($context["chains"]) ? $context["chains"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_chains_id", array()), "class=\"form-control\"  data-type=\"distributors\"");
        echo "
                </div>
            </div>
            ";
        // line 242
        if ((twig_length_filter($this->env, (isset($context["groups"]) ? $context["groups"] : null)) > 1)) {
            // line 243
            echo "                <div class=\"form-group\">
                    ";
            // line 244
            echo form_label("Group", "groups_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 246
            echo form_dropdown("groups_id", (isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "groups_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 250
        echo "
            <div class=\"form-group\">
                ";
        // line 252
        echo form_label("Asst Manager", "asst_manager", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 254
        echo form_input(array("name" => "asst_manager", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "asst_manager", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 266
        echo "


            <div class=\"form-group\">
                ";
        // line 270
        echo form_label("Customers Types", "customers_types_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <label class=\"control-label\">";
        // line 272
        echo ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "customers_types_id", array()) == 3)) ? ("Wholesalers") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "customers_types_id", array()) == 1)) ? ("Customer") : ("Distributor"))));
        echo "</label>
                </div>
            </div>


            ";
        // line 278
        echo "            ";
        // line 279
        echo "            ";
        // line 280
        echo "            ";
        // line 281
        echo "            ";
        // line 282
        echo "            ";
        // line 283
        echo "            ";
        // line 284
        echo "            ";
        // line 285
        echo "            ";
        // line 286
        echo "            ";
        // line 287
        echo "            ";
        // line 288
        echo "            ";
        // line 289
        echo "            <div class=\"form-group\">
                ";
        // line 290
        echo form_label("SSD ID", "ssd_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 292
        echo form_input(array("name" => "ssd_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "ssd_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 296
        echo form_label("SSD Name", "sap_cust_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 298
        echo form_input(array("name" => "sap_cust_name", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "sap_cust_name", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 303
        echo form_label("Sap ID", "sap_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 305
        echo form_input(array("name" => "sap_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "sap_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 310
        echo form_label("Street Address", "street_address", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 312
        echo form_input(array("name" => "street_address", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "street_address", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            ";
        // line 316
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countryHierachy"]) ? $context["countryHierachy"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
            // line 317
            echo "                <div class=\"form-group select-change-container\">
                    ";
            // line 318
            echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 320
            if ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array())) {
                // line 321
                echo "                            ";
                echo form_dropdown($context["countryField"], $this->getAttribute((isset($context["countryValues"]) ? $context["countryValues"] : null), $context["countryField"], array(), "array"), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            } else {
                // line 323
                echo "                            ";
                echo form_dropdown($context["countryField"], ((($context["key"] == "root")) ? ((isset($context["rootValues"]) ? $context["rootValues"] : null)) : (array())), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            }
            // line 325
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 328
        echo "
            <div class=\"form-group\">
                ";
        // line 330
        echo form_label("Province", "province", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 332
        echo form_input(array("name" => "province", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "province", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 337
        echo form_label("Postal Code", "postal_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 339
        echo form_input(array("name" => "postal_code", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "postal_code", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 344
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 346
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 351
        echo form_label("Phone 2", "phone_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 353
        echo form_input(array("name" => "phone_2", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "phone_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 358
        echo form_label("SMS Number", "sms_number", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 360
        echo form_input(array("name" => "sms_number", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "sms_number", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 365
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 367
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 372
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 374
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 379
        echo form_label("Email 2", "email_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 381
        echo form_input(array("name" => "email_2", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email_2", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 386
        echo form_label("Email 3", "email_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 388
        echo form_input(array("name" => "email_3", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "email_3", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            ";
        // line 392
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw"))) {
            // line 393
            echo "                <div class=\"form-group\">
                    ";
            // line 394
            echo form_label("LINE ID", "line_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 396
            echo form_input(array("name" => "line_id", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "line_id", array()), "class" => "form-control"));
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 401
            echo form_label("", "disable_line", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-5\">
                        <input type=\"checkbox\" value=\"1\" name=\"disable_line\" id=\"disableLine\" onclick=\"toggle(this)\" ";
            // line 403
            echo ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "disable_line", array()) != "0")) ? ("checked") : (""));
            echo ">&nbsp;Disable LINE Messages</input>
                    </div>
                </div>
            ";
        }
        // line 407
        echo "
            <div class=\"form-group\">
                ";
        // line 409
        echo form_label("Account Number", "account_number", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 411
        echo form_input(array("name" => "account_number", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "account_number", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-maxlength" => "4"));
        echo "
                </div>
            </div>
            ";
        // line 420
        echo "
            ";
        // line 429
        echo "            <div class=\"form-group\">
                ";
        // line 430
        echo form_label("Current Approval Status", "current_status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 432
        echo form_input(array("name" => "current_status", "value" => ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) ==  -2)) ? ("Close down") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) ==  -1)) ? ("Rejected") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) == 0)) ? ("Pending") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) == 1)) ? ("Approved") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) == 2)) ? ("Edit") : ("")))))))))), "class" => "form-control", "readonly" => "readonly"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 436
        echo form_label("", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 438
        echo form_radio("active", 0, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 439
        echo form_radio("active", 1, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>
            ";
        // line 442
        if (((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw")) && ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin"))) {
            // line 443
            echo "                <div class=\"form-group\">
                    ";
            // line 444
            echo form_label("Approved", "approved", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <select name=\"approved\" id=\"approved\" class=\"form-control\" data-parsley-required=\"true\">
                            <option value=\"\">--Select--</option>
                            <option value=\"-2\" ";
            // line 448
            echo ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) ==  -2)) ? ("selected") : (""));
            echo ">Close down</option>
                            <option value=\"-1\" ";
            // line 449
            echo ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) ==  -1)) ? ("selected") : (""));
            echo ">Rejected</option>
                            ";
            // line 451
            echo "                            <option value=\"1\" ";
            echo ((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "approved", array()) == 1)) ? ("selected") : (""));
            echo ">Approved</option>
                            ";
            // line 453
            echo "                        </select>
                    </div>
                </div>
            ";
        }
        // line 457
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["country_channels"]) ? $context["country_channels"] : null)) > 1)) {
            // line 458
            echo "                <div class=\"form-group\">
                    ";
            // line 459
            echo form_label("Country Channel", "country_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 461
            echo form_dropdown("country_channels_id", (isset($context["country_channels"]) ? $context["country_channels"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "country_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 465
        echo "
            ";
        // line 466
        if ((twig_length_filter($this->env, (isset($context["country_sub_channels"]) ? $context["country_sub_channels"] : null)) > 1)) {
            // line 467
            echo "                <div class=\"form-group\">
                    ";
            // line 468
            echo form_label("Country Sub Channel", "country_sub_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 470
            echo form_dropdown("country_sub_channels_id", (isset($context["country_sub_channels"]) ? $context["country_sub_channels"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "country_sub_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 474
        echo "            <div class=\"form-group\">
                ";
        // line 475
        echo form_label("DTM", "otm", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 477
        echo form_dropdown("otm", (isset($context["otm"]) ? $context["otm"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "otm", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            ";
        // line 480
        if ((twig_length_filter($this->env, (isset($context["business_types"]) ? $context["business_types"] : null)) > 1)) {
            // line 481
            echo "                <div class=\"form-group\">
                    ";
            // line 482
            echo form_label("Business Types", "business_types_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 484
            echo form_dropdown("business_types_id", (isset($context["business_types"]) ? $context["business_types"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "business_types_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 488
        echo "
            ";
        // line 489
        if ((twig_length_filter($this->env, (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null)) > 1)) {
            // line 490
            echo "                <div class=\"form-group\">
                    ";
            // line 491
            echo form_label("Assigned Distributor 1", "assigned_distributor_1", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 493
            echo form_dropdown("assigned_distributor_1", (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "assigned_distributor_1", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 497
        echo "
            ";
        // line 498
        if ((twig_length_filter($this->env, (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null)) > 1)) {
            // line 499
            echo "                <div class=\"form-group\">
                    ";
            // line 500
            echo form_label("Assigned Distributor 2", "assigned_distributor_2", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 502
            echo form_dropdown("assigned_distributor_2", (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "assigned_distributor_2", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 506
        echo "
            <div class=\"form-group\">
                ";
        // line 508
        echo form_label("Contact Details", "contact_details", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 510
        echo form_textarea(array("name" => "contact_details", "value" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "contact_details", array()), "class" => "form-control", "rows" => 3));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 514
        echo form_label("Is Perfect Store", "is_perfect_store", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 516
        echo form_radio("is_perfect_store", 0, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "is_perfect_store", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 517
        echo form_radio("is_perfect_store", 1, ($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "is_perfect_store", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>
        </div>
    </div>

    ";
        // line 523
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/distributors/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  900 => 523,  891 => 517,  887 => 516,  882 => 514,  875 => 510,  870 => 508,  866 => 506,  859 => 502,  854 => 500,  851 => 499,  849 => 498,  846 => 497,  839 => 493,  834 => 491,  831 => 490,  829 => 489,  826 => 488,  819 => 484,  814 => 482,  811 => 481,  809 => 480,  803 => 477,  798 => 475,  795 => 474,  788 => 470,  783 => 468,  780 => 467,  778 => 466,  775 => 465,  768 => 461,  763 => 459,  760 => 458,  757 => 457,  751 => 453,  746 => 451,  742 => 449,  738 => 448,  731 => 444,  728 => 443,  726 => 442,  720 => 439,  716 => 438,  711 => 436,  704 => 432,  699 => 430,  696 => 429,  693 => 420,  687 => 411,  682 => 409,  678 => 407,  671 => 403,  666 => 401,  658 => 396,  653 => 394,  650 => 393,  648 => 392,  641 => 388,  636 => 386,  628 => 381,  623 => 379,  615 => 374,  610 => 372,  602 => 367,  597 => 365,  589 => 360,  584 => 358,  576 => 353,  571 => 351,  563 => 346,  558 => 344,  550 => 339,  545 => 337,  537 => 332,  532 => 330,  528 => 328,  520 => 325,  514 => 323,  508 => 321,  506 => 320,  501 => 318,  498 => 317,  494 => 316,  487 => 312,  482 => 310,  474 => 305,  469 => 303,  461 => 298,  456 => 296,  449 => 292,  444 => 290,  441 => 289,  439 => 288,  437 => 287,  435 => 286,  433 => 285,  431 => 284,  429 => 283,  427 => 282,  425 => 281,  423 => 280,  421 => 279,  419 => 278,  411 => 272,  406 => 270,  400 => 266,  393 => 254,  388 => 252,  384 => 250,  377 => 246,  372 => 244,  369 => 243,  367 => 242,  361 => 239,  356 => 237,  349 => 233,  344 => 231,  337 => 227,  333 => 226,  328 => 224,  321 => 220,  316 => 218,  308 => 213,  303 => 211,  295 => 206,  290 => 204,  286 => 202,  279 => 198,  274 => 196,  271 => 195,  268 => 194,  262 => 190,  257 => 188,  249 => 183,  244 => 181,  232 => 171,  230 => 170,  219 => 162,  216 => 161,  213 => 160,  171 => 121,  167 => 120,  153 => 109,  104 => 63,  64 => 26,  48 => 13,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
