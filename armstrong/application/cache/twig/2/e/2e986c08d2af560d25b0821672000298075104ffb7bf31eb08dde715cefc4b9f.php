<?php

/* ami/sampling/preview.html.twig */
class __TwigTemplate_2e986c08d2af560d25b0821672000298075104ffb7bf31eb08dde715cefc4b9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">                    

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Call Records</h4>
                            <p class=\"list-group-item-text\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_call_records_id", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    ";
        // line 20
        if ($this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "products", array())) {
            // line 21
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-barcode pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <div class=\"list-group-item-text\">
                                    <h4 class=\"list-group-item-heading text-muted\">Products</h4>
                                    ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "products", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 27
                echo "                                        <p><span class=\"text-muted\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
                echo ":</span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_name", array()), "html", null, true);
                echo "</p>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                                   
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 33
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 38
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "sampling"))) {
            // line 39
            echo "                ";
            echo html_btn(site_url(("ami/sampling/edit/" . $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 41
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/sampling/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 41,  86 => 39,  84 => 38,  77 => 33,  70 => 28,  59 => 27,  55 => 26,  48 => 21,  46 => 20,  39 => 16,  25 => 5,  19 => 1,);
    }
}
