<?php

/* ami/master_template/edit.html.twig */
class __TwigTemplate_2ef5f881c1885482fb304fa03fe9ad873efc52220e926abdfc2eb9f01a222a6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/master_template/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/salespersons.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(function() {
            \$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD',
                // pickTime: false
            });
            \$('#InputCustomers').autocomplete({
                // source: \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield?salespersons="), "html", null, true);
        echo "\" + \$('#SelectSalesperson').find(\":selected\").val(),
                source: function(request, response) {
                    \$.ajax({
                        url: \"";
        // line 18
        echo twig_escape_filter($this->env, site_url("ami/master_template/tokenfield"), "html", null, true);
        echo "\",
                        dataType: \"json\",
                        data: {
                            term : request.term,
                            salespersons : \$('#SelectSalesperson').find(\":selected\").val(),
                            action : 'master_template'
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                delay: 100,
                select: function( event, ui ) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputCustomersId').val(ui.item.value);
                }
            }).on('focus', function() {
                this.select();
            });
        });
    </script>
";
    }

    // line 43
    public function block_css($context, array $blocks = array())
    {
        // line 44
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
";
    }

    // line 48
    public function block_content($context, array $blocks = array())
    {
        // line 49
        echo "
    ";
        // line 50
        echo form_open(site_url("ami/master_template/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Master Template</h1>
                <div class=\"text-right\">
                    ";
        // line 57
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/master_template/edit.html.twig", 57)->display(array_merge($context, array("url" => "ami/master_template", "id" => $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "id", array()), "permission" => "route_plan")));
        // line 58
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 68
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 70
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_salespersons_id", array()), (("
                            class=\"form-control\" 
                            data-parsley-required=\"true\" 
                            id=\"SelectSalesperson\" 
                            data-url=\"" . site_url("ami/salespersons/fetch_customer")) . "\"
                        "));
        // line 75
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 80
        echo form_label("Customer", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 87
        echo "                    ";
        echo form_input(array("name" => "armstrong_2_customers_id_", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputCustomers", "data-parsley-required" => "true"));
        // line 91
        echo "
                    <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"InputCustomersId\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_customers_id", array()), "html", null, true);
        echo "\" />
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 97
        echo form_label("Planned Day", "planned_day", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 99
        echo form_input(array("name" => "planned_day", "value" => $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "planned_day", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "number", "data-parsley-range" => "[1, 100]"));
        // line 104
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 109
        echo form_label("Master Id", "master_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 111
        echo form_radio("master_id", 1, ($this->getAttribute((isset($context["template"]) ? $context["template"] : null), "master_id", array()) == 1));
        echo " Template 1</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 112
        echo form_radio("master_id", 2, ($this->getAttribute((isset($context["template"]) ? $context["template"] : null), "master_id", array()) == 2));
        echo " Template 2</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 113
        echo form_radio("master_id", 3, ($this->getAttribute((isset($context["template"]) ? $context["template"] : null), "master_id", array()) == 3));
        echo " Template 3</label>
                </div>
            </div>

            ";
        // line 123
        echo "        </div>
    </div>

    ";
        // line 126
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/master_template/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 126,  207 => 123,  200 => 113,  196 => 112,  192 => 111,  187 => 109,  180 => 104,  178 => 99,  173 => 97,  165 => 92,  162 => 91,  159 => 87,  154 => 80,  147 => 75,  140 => 70,  135 => 68,  123 => 58,  121 => 57,  111 => 50,  108 => 49,  105 => 48,  99 => 45,  94 => 44,  91 => 43,  63 => 18,  57 => 15,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
