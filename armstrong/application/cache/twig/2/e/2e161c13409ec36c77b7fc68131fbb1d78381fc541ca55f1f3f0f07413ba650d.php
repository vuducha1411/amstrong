<?php

/* ami/wholesalers/pending_action_amended_data.html.twig */
class __TwigTemplate_2e161c13409ec36c77b7fc68131fbb1d78381fc541ca55f1f3f0f07413ba650d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs\" role=\"tablist\">
    ";
        // line 3
        echo "    <li role=\"presentation\" class=\"active\"><a href=\"#amended\" aria-controls=\"amended\" role=\"tab\" data-toggle=\"tab\">Amended Data</a></li>
</ul>

<div class=\"tab-content m-t-md\">
    ";
        // line 10
        echo "    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"amended\">
        <table class=\"table table-striped table-bordered table-hover\">
            <thead>
                <tr>
                    <th>Field</th>
                    <th class=\"danger\">Old Value</th>
                    <th class=\"success\">New Value</th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 21
            echo "                    <tr>
                        <td>";
            // line 22
            echo twig_escape_filter($this->env, $context["field"], "html", null, true);
            echo "</td>
                        <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "old_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                        <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "new_data", array()), $context["field"], array(), "array"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "            </tbody>
        </table>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/pending_action_amended_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 27,  55 => 24,  51 => 23,  47 => 22,  44 => 21,  40 => 20,  28 => 10,  22 => 3,  19 => 1,);
    }
}
