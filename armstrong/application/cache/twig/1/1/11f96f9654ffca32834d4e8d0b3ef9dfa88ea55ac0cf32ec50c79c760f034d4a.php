<?php

/* ami/wholesalers/pending_action.html.twig */
class __TwigTemplate_11f96f9654ffca32834d4e8d0b3ef9dfa88ea55ac0cf32ec50c79c760f034d4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">

    ";
        // line 3
        echo form_open(site_url(("ami/wholesalers/pending" . $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()))), array("class" => "modal-content"));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            ";
        // line 9
        if ((isset($context["amendedData"]) ? $context["amendedData"] : null)) {
            // line 10
            echo "                ";
            $this->loadTemplate("ami/wholesalers/pending_action_amended_data.html.twig", "ami/wholesalers/pending_action.html.twig", 10)->display($context);
            // line 11
            echo "            ";
        } else {
            // line 12
            echo "                ";
            $this->loadTemplate("ami/wholesalers/pending_action_data.html.twig", "ami/wholesalers/pending_action.html.twig", 12)->display($context);
            // line 13
            echo "            ";
        }
        // line 14
        echo "        </div>
        <div class=\"modal-footer\">
            ";
        // line 16
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
            // line 17
            echo "            ";
            if (((isset($context["app"]) ? $context["app"] : null) == "app")) {
                // line 18
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/wholesalers/approve/" . $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()))), "html", null, true);
                echo "\" class=\"btn btn-success btn-pending-submit\"><i class=\"fa fa-fw fa-thumbs-up\"></i> Approve</a></a>
            ";
            }
            // line 20
            echo "            ";
            if (((isset($context["app"]) ? $context["app"] : null) == "rej")) {
                // line 21
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/wholesalers/reject/" . $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()))), "html", null, true);
                echo "\" class=\"btn btn-danger btn-pending-submit\"><i class=\"fa fa-fw fa-thumbs-down\"></i> Reject</a></a>
            ";
            }
            // line 23
            echo "            ";
        }
        // line 24
        echo "            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    ";
        // line 26
        echo form_close();
        echo "
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/pending_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 26,  78 => 24,  75 => 23,  69 => 21,  66 => 20,  60 => 18,  57 => 17,  55 => 16,  51 => 14,  48 => 13,  45 => 12,  42 => 11,  39 => 10,  37 => 9,  29 => 6,  23 => 3,  19 => 1,);
    }
}
