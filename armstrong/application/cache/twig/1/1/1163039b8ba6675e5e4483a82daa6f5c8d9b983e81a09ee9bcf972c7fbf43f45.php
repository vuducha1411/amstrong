<?php

/* ami/salespersons/edit.html.twig */
class __TwigTemplate_1163039b8ba6675e5e4483a82daa6f5c8d9b983e81a09ee9bcf972c7fbf43f45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/salespersons/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script type=\"text/javascript\"
            src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/tokenfield/bootstrap-tokenfield.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/salespersons.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/parsley/parsley.remote.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(function () {
            sales_type = \"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "type", array()), "html", null, true);
        echo "\";
            sub_type = \"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "sub_type", array()), "html", null, true);
        echo "\";
            \$('#RolesSelector').trigger('change');
        });
    </script>
";
    }

    // line 18
    public function block_css($context, array $blocks = array())
    {
        // line 19
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/css/plugins/tokenfield/bootstrap-tokenfield.min.css"), "html", null, true);
        echo "\"/>
    <style>
        select[multiple] {
            min-height: 250px;
        }
    </style>
";
    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "
    ";
        // line 30
        echo form_open(site_url("ami/salespersons/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_salespersons_id" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Salespersons</h1>

                <div class=\"text-right\">
                    ";
        // line 38
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/salespersons/edit.html.twig", 38)->display(array_merge($context, array("url" => "ami/salespersons", "id" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()), "permission" => "salespersons")));
        // line 39
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 47
        if ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "id", array())) {
            // line 48
            echo "            <div class=\"form-group\">
                ";
            // line 49
            echo form_label("Armstrong 1 salespersons ID", "first_name", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 51
            echo form_input(array("name" => "armstrong_1_salespersons_id", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_1_salespersons_id", array()), "class" => "form-control"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 55
            echo form_label("Armstrong 2 salespersons Id", "first_name", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 57
            echo form_input(array("value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()), "class" => "form-control", "data-parsley-required" => "true", "disabled" => "disabled"));
            echo "
                </div>
            </div>
            ";
        }
        // line 61
        echo "            <div class=\"form-group\">
                ";
        // line 62
        echo form_label("First Name", "first_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 64
        echo form_input(array("name" => "first_name", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "first_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 69
        echo form_label("Last Name", "last_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 71
        echo form_input(array("name" => "last_name", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "last_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 76
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <!--";
        // line 78
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "email", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "email", "data-parsley-remote" => site_url("ami/salespersons/validate"), "data-parsley-remote-message" => "Duplicated email", "data-parsley-remote-options" => (($this->getAttribute(        // line 83
(isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array())) ? (twig_escape_filter($this->env, twig_jsonencode_filter(array("data" => array("armstrong_2_salespersons_id" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array())))))) : ("")), "data-parsley-trigger" => "focusout"));
        // line 85
        echo "-->
                    ";
        // line 86
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "email", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "email", "data-parsley-trigger" => "focusout"));
        // line 89
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 94
        echo form_label("Password", "password", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 96
        echo form_password(array("name" => "password", "value" => null, "class" => "form-control", "id" => "inputPassword", "data-parsley-required" => (($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "password", array())) ? ("false") : ("true")), "data-parsley-minlength" => 6));
        echo "
                </div>
            </div>

            ";
        // line 100
        if ( !$this->getAttribute((isset($context["person"]) ? $context["person"] : null), "password", array())) {
            // line 101
            echo "                <div class=\"form-group\">
                    ";
            // line 102
            echo form_label("Confirm Password", "confirm_password", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 104
            echo form_password(array("name" => "confirm_password", "value" => null, "class" => "form-control", "data-parsley-equalto" => "#inputPassword"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 108
        echo "
            <div class=\"form-group\">
                ";
        // line 110
        echo form_label("Alias", "alias", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 112
        echo form_input(array("name" => "alias", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "alias", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 116
        if ((twig_length_filter($this->env, (isset($context["roles"]) ? $context["roles"] : null)) > 1)) {
            // line 117
            echo "                <div class=\"form-group\">
                    ";
            // line 118
            echo form_label("User Role", "roles_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 121
            echo "                        <select name=\"roles_id\" id=\"RolesSelector\" class=\"form-control\"
                                data-fetchUrl=\"";
            // line 122
            echo twig_escape_filter($this->env, site_url("ami/salespersons/roles"), "html", null, true);
            echo "\"
                                data-parsley-required=\"true\" ";
            // line 123
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array())) ? (("data-edit=" . $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "armstrong_2_salespersons_id", array()))) : ("")), "html", null, true);
            echo ">
                            ";
            // line 124
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["roles"]) ? $context["roles"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 125
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["role"], "id", array()), "html", null, true);
                echo "\" data-depth=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["role"], "depth", array()), "html", null, true);
                echo "\" data-role=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["role"], "name", array()), "html", null, true);
                echo "\"
                                        data-parent=\"";
                // line 126
                echo twig_escape_filter($this->env, $this->getAttribute($context["role"], "parent_id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute($context["role"], "id", array()) == $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "roles_id", array()))) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["role"], "name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 128
            echo "                        </select>
                    </div>
                </div>
            ";
        }
        // line 132
        echo "
            ";
        // line 133
        if ((twig_length_filter($this->env, (isset($context["types"]) ? $context["types"] : null)) > 1)) {
            // line 134
            echo "                <div class=\"form-group\">
                    ";
            // line 135
            echo form_label("User Type", "type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        <select name=\"type\" class=\"form-control\">
                            ";
            // line 138
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["types"]) ? $context["types"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
                // line 139
                echo "                                ";
                // line 140
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "type", array()) == $context["type"])) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 142
            echo "                        </select>
                        ";
            // line 144
            echo "                        ";
            // line 145
            echo "                    </div>
                </div>
            ";
        }
        // line 148
        echo "
            ";
        // line 149
        if ((twig_length_filter($this->env, (isset($context["sub_types"]) ? $context["sub_types"] : null)) > 1)) {
            // line 150
            echo "                <div class=\"form-group\">
                    ";
            // line 151
            echo form_label("Sub Type", "sub_type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        <select name=\"sub_type\" class=\"form-control\">
                            ";
            // line 154
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["sub_types"]) ? $context["sub_types"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_type"]) {
                // line 155
                echo "                                ";
                // line 156
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $context["sub_type"], "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "sub_type", array()) == $context["sub_type"])) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $context["sub_type"], "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_type'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 158
            echo "                        </select>
                        ";
            // line 160
            echo "                        ";
            // line 161
            echo "                    </div>
                </div>
            ";
        }
        // line 164
        echo "
            <div class=\"form-group\">
                ";
        // line 166
        echo form_label("Countries Management", "country_management", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 168
        if ((isset($context["countries_id"]) ? $context["countries_id"] : null)) {
            // line 169
            echo "                        ";
            echo form_dropdown("country_management[]", (isset($context["countries_list"]) ? $context["countries_list"] : null), (isset($context["countries_id"]) ? $context["countries_id"] : null), "id=\"countriesSelector\" class=\"form-control\" data-parsley-required=\"true\" multiple");
            echo "
                    ";
        } elseif (twig_in_filter($this->getAttribute(        // line 170
(isset($context["person"]) ? $context["person"] : null), "sub_type", array()), array(0 => "vp", 1 => "svp"))) {
            // line 171
            echo "                        ";
            echo form_dropdown("country_management[]", (isset($context["countries_list"]) ? $context["countries_list"] : null), null, "id=\"countriesSelector\" class=\"form-control\" data-parsley-required=\"true\" multiple");
            echo "
                    ";
        } else {
            // line 173
            echo "                        ";
            echo form_dropdown("country_management[]", (isset($context["countries_list"]) ? $context["countries_list"] : null), (isset($context["country_id"]) ? $context["country_id"] : null), "id=\"countriesSelector\" class=\"form-control\" data-parsley-required=\"true\" multiple");
            echo "
                    ";
        }
        // line 175
        echo "                </div>
            </div>

            ";
        // line 178
        if ((twig_length_filter($this->env, (isset($context["managers"]) ? $context["managers"] : null)) >= 1)) {
            // line 179
            echo "                <div class=\"form-group\">
                    ";
            // line 180
            echo form_label("Sales Leader", "salespersons_manager_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        <select name=\"salespersons_manager_id\" class=\"form-control\">
                            <option value=\"\">-- Default --</option>
                            ";
            // line 184
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["managers"]) ? $context["managers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["manager"]) {
                // line 185
                echo "                                ";
                // line 186
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "salespersons_manager_id", array()) == $this->getAttribute($context["manager"], "armstrong_2_salespersons_id", array()))) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "last_name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manager'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 188
            echo "                        </select>
                        ";
            // line 190
            echo "                    </div>
                </div>
            ";
        }
        // line 193
        echo "
            <div class=\"form-group\">
                ";
        // line 195
        echo form_label("Territory", "territory", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 197
        echo form_input(array("name" => "territory", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "territory", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 202
        echo form_label("Handphone", "handphone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 204
        echo form_input(array("name" => "handphone", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "handphone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 209
        echo form_label("Did", "did", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 211
        echo form_input(array("name" => "did", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "did", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 216
        echo "            <div class=\"form-group\">
                ";
        // line 217
        echo form_label("", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 219
        echo form_radio("active", 0, ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 220
        echo form_radio("active", 1, ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>
            ";
        // line 224
        echo "            <div class=\"form-group\">
                ";
        // line 225
        echo form_label("Test Acc", "test_acc", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 227
        echo form_radio("test_acc", 1, ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "test_acc", array()) == 1));
        echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 228
        echo form_radio("test_acc", 0, ($this->getAttribute((isset($context["person"]) ? $context["person"] : null), "test_acc", array()) == 0));
        echo " No</label>&nbsp;
                </div>
            </div>
            <input type=\"hidden\" id=\"RoleDepth\" name=\"depth\" class=\"hide\"/>

            ";
        // line 241
        echo "        </div>
    </div>

    ";
        // line 244
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw"))) {
            // line 245
            echo "        <div class=\"form-group\">
            ";
            // line 246
            echo form_label("LINE ID", "line_id", array("class" => "control-label col-sm-3"));
            echo "
            <div class=\"col-sm-6\">
                ";
            // line 248
            echo form_input(array("name" => "line_id", "value" => $this->getAttribute((isset($context["person"]) ? $context["person"] : null), "line_id", array()), "class" => "form-control"));
            echo "
            </div>
        </div>
    ";
        }
        // line 252
        echo "
    ";
        // line 253
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/salespersons/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  556 => 253,  553 => 252,  546 => 248,  541 => 246,  538 => 245,  536 => 244,  531 => 241,  523 => 228,  519 => 227,  514 => 225,  511 => 224,  505 => 220,  501 => 219,  496 => 217,  493 => 216,  486 => 211,  481 => 209,  473 => 204,  468 => 202,  460 => 197,  455 => 195,  451 => 193,  446 => 190,  443 => 188,  428 => 186,  426 => 185,  422 => 184,  415 => 180,  412 => 179,  410 => 178,  405 => 175,  399 => 173,  393 => 171,  391 => 170,  386 => 169,  384 => 168,  379 => 166,  375 => 164,  370 => 161,  368 => 160,  365 => 158,  352 => 156,  350 => 155,  346 => 154,  340 => 151,  337 => 150,  335 => 149,  332 => 148,  327 => 145,  325 => 144,  322 => 142,  309 => 140,  307 => 139,  303 => 138,  297 => 135,  294 => 134,  292 => 133,  289 => 132,  283 => 128,  271 => 126,  262 => 125,  258 => 124,  254 => 123,  250 => 122,  247 => 121,  242 => 118,  239 => 117,  237 => 116,  230 => 112,  225 => 110,  221 => 108,  214 => 104,  209 => 102,  206 => 101,  204 => 100,  197 => 96,  192 => 94,  185 => 89,  183 => 86,  180 => 85,  178 => 83,  177 => 78,  172 => 76,  164 => 71,  159 => 69,  151 => 64,  146 => 62,  143 => 61,  136 => 57,  131 => 55,  124 => 51,  119 => 49,  116 => 48,  114 => 47,  104 => 39,  102 => 38,  91 => 30,  88 => 29,  85 => 28,  74 => 20,  69 => 19,  66 => 18,  57 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
