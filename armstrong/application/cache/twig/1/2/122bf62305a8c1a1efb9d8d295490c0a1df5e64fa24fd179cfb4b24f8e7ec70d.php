<?php

/* ami/competitor_products_country/index.html.twig */
class __TwigTemplate_122bf62305a8c1a1efb9d8d295490c0a1df5e64fa24fd179cfb4b24f8e7ec70d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/competitor_products_country/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(document).ready(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/competitor_products_country/ajaxData"), "html", null, true);
        echo "\",*/
                \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
                'order': [[1, 'asc']],
                'bPaginate': true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }],
                \"iDisplayLength\": 50
            });
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "
    ";
        // line 36
        echo form_open(site_url("ami/competitor_products_country/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 41
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Competitor Products Master List Draft") : ("Competitor Products Master List"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 45
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 47
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/competitor_products_country/index.html.twig", 47)->display(array_merge($context, array("url" => "ami/competitor_products_country", "icon" => "fa-line-chart", "title" => "Competitor Products Master List", "permission" => "competitor_products_country", "export" => 1)));
        // line 48
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 64
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "competitor_products"))) {
            // line 65
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 69
        echo "                                <th>Name</th>
                                <th>Product Name</th>
                                <th>Weight Per PC</th>
                                <th>Brand</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 81
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 82
            echo "                                <tr>
                                    ";
            // line 83
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "competitor_products"))) {
                // line 84
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 86
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "name", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "product_name", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "weight_per_pc", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "brand", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "quantity", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "price", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 95
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "competitor_products")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "competitor_products")))) {
                // line 96
                echo "                                            ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/competitor_products_country/index.html.twig", 96)->display(array_merge($context, array("url" => "ami/competitor_products_country", "id" => $this->getAttribute($context["post"], "id", array()), "permission" => "competitor_products")));
                // line 97
                echo "                                        ";
            }
            // line 98
            echo "                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 105
        $context["paginationUrl"] = (((isset($context["draft"]) ? $context["draft"] : null)) ? ("ami/competitor_products_country/draft") : ("ami/competitor_products_country"));
        // line 106
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => site_url((isset($context["paginationUrl"]) ? $context["paginationUrl"] : null)), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 119
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/competitor_products_country/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 119,  247 => 106,  245 => 105,  239 => 101,  223 => 98,  220 => 97,  217 => 96,  215 => 95,  210 => 93,  206 => 92,  202 => 91,  198 => 90,  194 => 89,  190 => 88,  186 => 87,  181 => 86,  175 => 84,  173 => 83,  170 => 82,  153 => 81,  139 => 69,  133 => 65,  131 => 64,  113 => 48,  111 => 47,  106 => 45,  99 => 41,  91 => 36,  88 => 35,  85 => 34,  79 => 31,  74 => 30,  71 => 29,  54 => 15,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
