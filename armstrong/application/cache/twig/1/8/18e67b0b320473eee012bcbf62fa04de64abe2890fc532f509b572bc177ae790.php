<?php

/* ami/module_field_countries/process.html.twig */
class __TwigTemplate_18e67b0b320473eee012bcbf62fa04de64abe2890fc532f509b572bc177ae790 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/module_field_countries/process.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script language=\"javascript\" type=\"text/javascript\">
        \$(\".select_list ul\").selectable();
        function move_list_items(sourceid, destinationid) {
            var ids = '0';
            \$(\"#\" + sourceid + \" li.ui-selected\").each(function () {
                if (sourceid == 'from_select_list') {
                    type = 'add';
                } else {
                    type = 'remove';
                }
                ids = ids + ',' + \$(this).attr('data');
            });
            add_remove_field(ids, type);
            \$(\"#\" + sourceid + \" li.ui-selected\").appendTo(\"#\" + destinationid);
            \$(\".select_list ul li\").removeClass('ui-selected');
        }

        function move_list_items_all(sourceid, destinationid) {
            var ids = '0';
            \$(\"#\" + sourceid + \" li\").each(function () {
                if (sourceid == 'from_select_list') {
                    type = 'add';
                } else {
                    type = 'remove';
                }
                ids = ids + ',' + \$(this).attr('data');
            });
            add_remove_field(ids, type);
            \$(\"#\" + sourceid + \" li\").appendTo(\"#\" + destinationid);
            \$(\".select_list ul li\").removeClass('ui-selected');
        }

        function add_remove_field(ids, type) {
            \$.ajax({
                type: \"POST\",
                dataType: 'json',
                url: '";
        // line 41
        echo twig_escape_filter($this->env, site_url("ami/module_field_countries/ajax_add_remove_field"), "html", null, true);
        echo "',
                data: {
                    ids: ids,
                    type: type
                }
            });
        }

        \$(document).ready(function () {
            // Get GET parameter
            \$.urlParam = function urlParam(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };

            \$('.header a span').append(' <i class=\"fa fa-sort\"></i>');

            if (\$.urlParam('sort_by') == null) {
                \$('.header a.filter_field').addClass('asc');
                \$('.header a.filter_field').attr('href', '";
        // line 70
        echo site_url((("ami/module_field_countries/process?filter=" . (isset($context["filter"]) ? $context["filter"] : null)) . "&sort_by=field&order=desc"));
        echo "');
            }else{
                var sortBy = \$.urlParam('sort_by');
                var href;
                ";
        // line 74
        $context["href"] = ((("ami/module_field_countries/process?filter=" . (isset($context["filter"]) ? $context["filter"] : null)) . "&sort_by=") . (isset($context["sort_by"]) ? $context["sort_by"] : null));
        // line 75
        echo "                if(\$.urlParam('order') == 'desc'){
                    href = \"";
        // line 76
        echo site_url(((isset($context["href"]) ? $context["href"] : null) . "&order=asc"));
        echo "\";
                    \$('.header a.filter_'+sortBy).addClass('desc');
                }else{
                    href = \"";
        // line 79
        echo site_url(((isset($context["href"]) ? $context["href"] : null) . "&order=desc"));
        echo "\";
                    \$('.header a.filter_'+sortBy).addClass('asc');
                }

                \$('.header a.filter_'+sortBy).attr('href', href);
            }
            \$('.header a.asc i').attr('class', 'fa fa-sort-down');
            \$('.header a.desc i').attr('class', 'fa fa-sort-up');
        });
    </script>
";
    }

    // line 91
    public function block_css($context, array $blocks = array())
    {
        // line 92
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .select_list {
            overflow: auto;
            height: 400px !important;
            margin: 10px;
            border: 1px solid #ddd
        }

        .select_list .ui-selected {
            background: #5cb85c;
            color: white;
        }

        .select_list ul {
            padding: 1px;
            cursor: pointer;
        }

        .select_list ul li {
            list-style: none;
            border-bottom: 1px solid #e9e9e9;
            padding-left: 5px;
            position: relative
        }

        #from_select_list li button {
            display: none;
            float: right
        }

        #to_select_list li:hover button {
            display: inline-block;
        }

        .btn_edit_field {
            position: absolute;
            right: 0px;
            padding: 0px;
            margin: 0px
        }
        .header span{
            font-weight: bold;
        }
    </style>
";
    }

    // line 139
    public function block_content($context, array $blocks = array())
    {
        // line 140
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Add Field To Country</h1>

                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"col-lg-12\">
                        <div class=\"col-md-12\">
                            <div class=\"dropdown\">
                                <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                        id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                    Filter: ";
        // line 160
        echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
        echo " <span
                                            class=\"caret\"></span>
                                </button>
                                <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                    <!--li role=\"presentation\" ";
        // line 164
        echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
        echo "><a
                                                role=\"menuitem\" tabindex=\"-1\"
                                                href=\"";
        // line 166
        echo twig_escape_filter($this->env, site_url("ami/module_field_countries/process"), "html", null, true);
        echo "\">All</a>
                                    </li-->
                                    ";
        // line 168
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 169
            echo "                                        <li role=\"presentation\" ";
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == $this->getAttribute($context["module"], "name", array()))) ? ("class=\"active\"") : (""));
            echo ">
                                            <a role=\"menuitem\" tabindex=\"-1\"
                                               href=\"";
            // line 171
            echo twig_escape_filter($this->env, site_url(("ami/module_field_countries/process?filter=" . $this->getAttribute($context["module"], "name", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["module"], "name", array()), "html", null, true);
            echo "</a>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 174
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-lg-6\">
                        <h4>Fields</h4>

                        <div class=\"col-lg-12 select_list\">
                            <ul id=\"from_select_list\">
                                ";
        // line 183
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 184
            echo "                                    <li data='";
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "id", array()), "html", null, true);
            echo "'>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "field", array()), "html", null, true);
            echo "
                                        ";
            // line 185
            echo form_button(array("type" => "button", "content" => (("<i class=\"fa fa-fw fa-pencil-square-o\"></i> <a href=\"" . site_url(((((("ami/module_field_countries/edit/" . $this->getAttribute(            // line 187
$context["field"], "id", array())) . "/") . $this->getAttribute($context["field"], "field", array())) . "/") . (isset($context["filter"]) ? $context["filter"] : null)))) . "\"
                                           data-toggle=\"ajaxModal\">Edit</a>"), "class" => "btn btn-outline btn-link btn_edit_field"));
            // line 190
            echo "
                                    </li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 193
        echo "                            </ul>
                        </div>
                    </div>
                    <div class=\"col-lg-6\">
                        <h4>Added</h4>

                        <div class=\"col-lg-12 select_list\">

                            ";
        // line 202
        echo "                            <div class=\"header\">
                                <a href=\"";
        // line 203
        echo twig_escape_filter($this->env, site_url((("ami/module_field_countries/process?filter=" . (isset($context["filter"]) ? $context["filter"] : null)) . "&sort_by=field&order=asc")), "html", null, true);
        echo "\" class=\"filter_field\">
                                    <span class=\"col-sm-6\">Field</span>
                                </a>
                                <a href=\"";
        // line 206
        echo twig_escape_filter($this->env, site_url((("ami/module_field_countries/process?filter=" . (isset($context["filter"]) ? $context["filter"] : null)) . "&sort_by=section&order=asc")), "html", null, true);
        echo "\" class=\"filter_section\">
                                    <span class=\"col-sm-3\">Section</span>
                                </a>
                                <a href=\"";
        // line 209
        echo twig_escape_filter($this->env, site_url((("ami/module_field_countries/process?filter=" . (isset($context["filter"]) ? $context["filter"] : null)) . "&sort_by=row&order=asc")), "html", null, true);
        echo "\" class=\"filter_row\">
                                    <span class=\"col-sm-3\">Row</span>
                                </a>
                                <span class=\"col-sm-1\"></span>
                            </div>
                            <hr/>
                            <ul id=\"to_select_list\">
                                ";
        // line 216
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["module_fields_country"]) ? $context["module_fields_country"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 217
            echo "                                    <li data='";
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "module_field_id", array()), "html", null, true);
            echo "'>
                                        <div class=\"row\">
                                            <span class=\"col-sm-7\">";
            // line 219
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "field", array()), "html", null, true);
            echo "</span>
                                            <span class=\"col-sm-2\">";
            // line 220
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "section", array()), "html", null, true);
            echo "</span>
                                            <span class=\"col-sm-1\">";
            // line 221
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "row", array()), "html", null, true);
            echo "</span>
                                            <span class=\"col-sm-2\">
                                                ";
            // line 223
            echo form_button(array("type" => "button", "content" => (("<i class=\"fa fa-fw fa-pencil-square-o\"></i> <a href=\"" . site_url(((((("ami/module_field_countries/edit/" . $this->getAttribute(            // line 225
$context["field"], "module_field_id", array())) . "/") . $this->getAttribute($context["field"], "field", array())) . "/") . (isset($context["filter"]) ? $context["filter"] : null)))) . "\"
                                               data-toggle=\"ajaxModal\">Edit</a>"), "class" => "btn btn-outline btn-link btn_edit_field"));
            // line 228
            echo "
                                            </span>
                                        </div>
                                    </li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 233
        echo "                            </ul>
                        </div>
                    </div>
                    <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">

                        <tr>
                            <td>
                                ";
        // line 240
        echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-chevron-circle-right\"></i> Move Right All", "class" => "btn btn-success", "id" => "moverightall", "onclick" => "move_list_items_all('from_select_list','to_select_list')"));
        // line 246
        echo "
                            </td>
                            <td>
                                ";
        // line 249
        echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-chevron-right\"></i> Move Right", "class" => "btn btn-success", "id" => "moveright", "onclick" => "move_list_items('from_select_list','to_select_list')"));
        // line 255
        echo "
                            </td>
                            <td>
                                ";
        // line 258
        echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-chevron-left\"></i> Move Left", "class" => "btn btn-success", "id" => "moveleft", "onclick" => "move_list_items('to_select_list','from_select_list')"));
        // line 264
        echo "
                            </td>
                            <td>
                                ";
        // line 267
        echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-chevron-circle-left\"></i> Move Left All", "class" => "btn btn-success", "id" => "moveleftall", "onclick" => "move_list_items_all('to_select_list','from_select_list')"));
        // line 273
        echo "
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/module_field_countries/process.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 273,  392 => 267,  387 => 264,  385 => 258,  380 => 255,  378 => 249,  373 => 246,  371 => 240,  362 => 233,  352 => 228,  349 => 225,  348 => 223,  343 => 221,  339 => 220,  335 => 219,  329 => 217,  325 => 216,  315 => 209,  309 => 206,  303 => 203,  300 => 202,  290 => 193,  282 => 190,  279 => 187,  278 => 185,  271 => 184,  267 => 183,  256 => 174,  245 => 171,  239 => 169,  235 => 168,  230 => 166,  225 => 164,  218 => 160,  196 => 140,  193 => 139,  142 => 92,  139 => 91,  124 => 79,  118 => 76,  115 => 75,  113 => 74,  106 => 70,  74 => 41,  33 => 4,  30 => 3,  11 => 1,);
    }
}
