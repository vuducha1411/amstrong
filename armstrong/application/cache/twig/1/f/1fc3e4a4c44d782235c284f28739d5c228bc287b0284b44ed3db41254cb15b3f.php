<?php

/* ami/setting_game/integrate_with_applications.html.twig */
class __TwigTemplate_1fc3e4a4c44d782235c284f28739d5c228bc287b0284b44ed3db41254cb15b3f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_game/integrate_with_applications.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 14
        echo twig_escape_filter($this->env, site_url("ami/setting_game/integrate_with_applications/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'desc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <style type=\"text/css\">
        .bg_disabled {
            background: #d9edf7
        }
    </style>
";
    }

    // line 36
    public function block_content($context, array $blocks = array())
    {
        // line 37
        echo "    ";
        echo form_open(site_url(("ami/setting_game/integrate_with_applications_save?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))));
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 41
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Integrate With Applications Draft") : ("Integrate With Applications"));
        echo "</h1>

                <div class=\"text-right\">
                    <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                        ";
        // line 45
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 61
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 62
            echo "                                <div class=\"row table-filter\">
                                    <div class=\"col-md-12\">
                                        <div class=\"dropdown\">
                                            <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                                    id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                                Filter: ";
            // line 67
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                                        class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                                <li role=\"presentation\" ";
            // line 71
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                                            role=\"menuitem\" tabindex=\"-1\"
                                                            href=\"";
            // line 73
            echo twig_escape_filter($this->env, site_url("ami/setting_game/integrate_with_applications"), "html", null, true);
            echo "\">All</a>
                                                </li>
                                                ";
            // line 75
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                // line 76
                echo "                                                    <li role=\"presentation\" ";
                echo ((((isset($context["filter"]) ? $context["filter"] : null) == $this->getAttribute($context["country"], "code", array()))) ? ("class=\"active\"") : (""));
                echo ">
                                                        <a role=\"menuitem\" tabindex=\"-1\"
                                                           href=\"";
                // line 78
                echo twig_escape_filter($this->env, site_url(("ami/setting_game/integrate_with_applications?filter=" . $this->getAttribute($context["country"], "code", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
                echo "</a>
                                                    </li>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ";
        }
        // line 86
        echo "                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Salespersons</th>
                                <th>SR</th>
                                <th>Leader</th>
                                <th>Created</th>
                                <th>Updated</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["post"]) {
            // line 98
            echo "                                <tr>
                                    <th align=\"center\">";
            // line 99
            echo twig_escape_filter($this->env, ($context["key"] + 1), "html", null, true);
            echo "</th>
                                    <td>";
            // line 100
            echo twig_escape_filter($this->env, (($this->getAttribute($context["post"], "email", array()) . "--") . $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array())), "html", null, true);
            echo "</td>
                                    <td class=\"center ";
            // line 101
            echo ((($this->getAttribute($context["post"], "app_type", array()) != 0)) ? ("bg_disabled") : (""));
            echo "\">
                                        ";
            // line 102
            if (($this->getAttribute($context["post"], "app_type", array()) == 0)) {
                // line 103
                echo "                                            ";
                echo form_checkbox((("cf[pull][" . $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array())) . "]"), 1, $this->getAttribute($context["post"], "is_pull", array()));
                echo "
                                        ";
            }
            // line 105
            echo "                                    </td>
                                    <td class=\"center ";
            // line 106
            echo ((($this->getAttribute($context["post"], "app_type", array()) != 2)) ? ("bg_disabled") : (""));
            echo "\">
                                        ";
            // line 107
            if (($this->getAttribute($context["post"], "app_type", array()) == 2)) {
                // line 108
                echo "                                            ";
                echo form_checkbox((("cf[leader][" . $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array())) . "]"), 1, $this->getAttribute($context["post"], "is_leader", array()));
                echo "
                                        ";
            }
            // line 110
            echo "                                    </td>
                                    <td class=\"center\">";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "
                                        ";
            // line 112
            echo form_hidden(array("sales_id[]" => $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array())));
            echo "
                                    </td>
                                    <td class=\"center\">";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "                            </tbody>
                        </table>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    ";
        // line 129
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/integrate_with_applications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 129,  254 => 117,  245 => 114,  240 => 112,  236 => 111,  233 => 110,  227 => 108,  225 => 107,  221 => 106,  218 => 105,  212 => 103,  210 => 102,  206 => 101,  202 => 100,  198 => 99,  195 => 98,  191 => 97,  178 => 86,  171 => 81,  160 => 78,  154 => 76,  150 => 75,  145 => 73,  140 => 71,  133 => 67,  126 => 62,  124 => 61,  105 => 45,  98 => 41,  90 => 37,  87 => 36,  76 => 28,  71 => 27,  68 => 26,  53 => 14,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
