<?php

/* ami/promotional_mechanics/range.html.twig */
class __TwigTemplate_1e95235e496949eb9cd088abf30d8a608164015706c18f78257e554fdf5564e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotional_mechanics/range.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 18
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics/ajaxRangeData"), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"id\"},
                    {mData: \"title\"},
                    {mData: \"from_date\"},
                    {mData: \"to_date\"},
                    {mData: \"date_created\", \"bSearchable\": false},
                    {mData: \"last_updated\", \"bSearchable\": false}
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" name=\"ids[]\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });

        });
    </script>
";
    }

    // line 47
    public function block_css($context, array $blocks = array())
    {
        // line 48
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 52
    public function block_content($context, array $blocks = array())
    {
        // line 53
        echo "
    ";
        // line 54
        echo form_open(site_url("ami/promotional_mechanics/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Promotional Mechanics Range</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 63
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>

                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics/add_range"), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 86
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
            // line 87
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 91
        echo "                                <th>ID</th>
                                <th>Title</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 115
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/range.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 115,  164 => 91,  158 => 87,  156 => 86,  134 => 67,  127 => 63,  115 => 54,  112 => 53,  109 => 52,  103 => 49,  98 => 48,  95 => 47,  63 => 18,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
