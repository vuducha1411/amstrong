<?php

/* ami/promotional_mechanics/overview.html.twig */
class __TwigTemplate_1dea2f441bd4c33afe50312348a593562a3ffadc0f85f6950a0a3b87932bf174 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotional_mechanics/overview.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

";
    }

    // line 11
    public function block_css($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 21
        echo "Promotional Mechanics Range & Setting";
        echo "</h1>

                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-primary\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-cogs fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        <div>Manager</div>
                                        <div>Range</div>
                                    </div>
                                </div>
                            </div>
                            <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, site_url("/ami/promotional_mechanics/range"), "html", null, true);
        echo "\">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-green\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-check-square-o fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        <div>Manager</div>
                                        <div>Setting</div>
                                    </div>
                                </div>
                            </div>
                            <a href=\"";
        // line 70
        echo twig_escape_filter($this->env, site_url("/ami/promotional_mechanics"), "html", null, true);
        echo "\">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/overview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 70,  102 => 47,  73 => 21,  67 => 17,  64 => 16,  58 => 13,  53 => 12,  50 => 11,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
