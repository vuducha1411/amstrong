<?php

/* ami/check_list/index.html.twig */
class __TwigTemplate_1daf1c6dee5401b1c84f8330dec345326886a357b5a653f7ebc69811eb95092b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/check_list/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/check_list/ajaxData"), "html", null, true);
        echo "\",*/
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        // 'bPaginate': false,
        \"iDisplayLength\": 20,
        'aoColumnDefs': [{
            // 'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
});
</script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "
    ";
        // line 36
        echo form_open(site_url("ami/check_list/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 41
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Check List Draft") : ("Check List"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 44
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 46
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/check_list/index.html.twig", 46)->display(array_merge($context, array("url" => "ami/check_list", "icon" => "fa-list-alt", "title" => "Check List", "permission" => "check_list")));
        // line 47
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">  

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#listed\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Active</a></li>
                <li role=\"presentation\"><a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Inactive</a></li>
            </ul>

            <div class=\"panel panel-default m-t\">                

                <div class=\"panel-body tab-content\">                
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 69
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
            // line 70
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 72
        echo "                                    <th>Title</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 79
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "active", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["check"]) {
            // line 80
            echo "                                    <tr>
                                        ";
            // line 81
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "check_list"))) {
                // line 82
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["check"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 84
            echo "                                        <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 88
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "check_list")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "check_list")))) {
                // line 89
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/check_list/index.html.twig", 89)->display(array_merge($context, array("url" => "ami/check_list", "id" => $this->getAttribute($context["check"], "id", array()), "permission" => "check_list")));
                // line 90
                echo "                                            ";
            }
            // line 91
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['check'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                            </tbody>
                        </table>                        
                    </div>

                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 102
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
            // line 103
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 105
        echo "                                    <th>Title</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 112
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "inactive", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["check"]) {
            // line 113
            echo "                                    <tr>
                                        ";
            // line 114
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
                // line 115
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["check"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 117
            echo "                                        <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["check"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 121
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "check_list")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "check_list")))) {
                // line 122
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/check_list/index.html.twig", 122)->display(array_merge($context, array("url" => "ami/check_list", "id" => $this->getAttribute($context["check"], "id", array()), "permission" => "check_list")));
                // line 123
                echo "                                            ";
            }
            // line 124
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['check'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "                            </tbody>
                        </table>                        
                    </div>
                </div>             
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 137
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/check_list/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 137,  306 => 127,  290 => 124,  287 => 123,  284 => 122,  282 => 121,  277 => 119,  273 => 118,  268 => 117,  262 => 115,  260 => 114,  257 => 113,  240 => 112,  231 => 105,  227 => 103,  225 => 102,  215 => 94,  199 => 91,  196 => 90,  193 => 89,  191 => 88,  186 => 86,  182 => 85,  177 => 84,  171 => 82,  169 => 81,  166 => 80,  149 => 79,  140 => 72,  136 => 70,  134 => 69,  110 => 47,  108 => 46,  103 => 44,  97 => 41,  89 => 36,  86 => 35,  83 => 34,  77 => 31,  73 => 30,  70 => 29,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
