<?php

/* ami/setting_country/coaching_form_setting.html.twig */
class __TwigTemplate_1d1fe7e5eebfadded0f8639dfdae6d9d7c9905d3b1c1fdf708464497e79f77b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-pane\" id=\"coaching_forms\">
    <ul class=\"nav nav-tabs\" role=\"tablist\">
        <li role=\"presentation\" class=\"active\">
            <a href=\"#app_pull\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Pull</a>
        </li>
        <li role=\"presentation\">
            <a href=\"#app_push\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Push</a>
        </li>
    </ul>

    <div class=\"panel panel-default m-t\">
        <div class=\"panel-body tab-content\">
            <div role=\"tabpanel\" class=\"tab-pane active\" id=\"app_pull\">
                <div class=\"form-group\">
                    ";
        // line 15
        echo form_label("Summary of the visits Today", "visit_summary", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 17
        echo form_textarea(array("name" => "application_config[pull][coaching_forms][visit_summary]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "visit_summary", array()), "class" => "form-control input-xxlarge", "id" => "visitSummaryPull"));
        // line 19
        echo "
                    </div>
                </div>

                ";
        // line 23
        $context["excellence_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        // line 24
        echo "                ";
        $context["improvement_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        // line 25
        echo "                ";
        $context["development_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "development_plan_new", array()));
        // line 26
        echo "                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 28
        echo form_label("Areas of Excellence (selling skills, selling tools, chefmanship)", "areas_of_excellence_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["excellence_new_array"]) {
            // line 31
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 32
            echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 34
            echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_excellence_new][" . $context["key"]) . "]"), "value" => $context["excellence_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['excellence_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 45
        echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 47
        echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_excellence_new][" . (isset($context["excellence_counter_pull"]) ? $context["excellence_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 61
        echo form_label("Areas for Improvement", "areas_of_improvement_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["improvement_new_array"]) {
            // line 64
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 65
            echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 67
            echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_improvement_new][" . $context["key"]) . "]"), "value" => $context["improvement_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['improvement_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 78
        echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 80
        echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_improvement_new][" . (isset($context["improvement_counter_pull"]) ? $context["improvement_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 94
        echo form_label("Development Plan", "development_plan_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 96
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "development_plan_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["development_new_array"]) {
            // line 97
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 98
            echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 100
            echo form_input(array("name" => (("application_config[pull][coaching_forms][development_plan_new][" . $context["key"]) . "]"), "value" => $context["development_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['development_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 111
        echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 113
        echo form_input(array("name" => (("application_config[pull][coaching_forms][development_plan_new][" . (isset($context["development_counter_pull"]) ? $context["development_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
            </div>
            <div role=\"tabpanel\" class=\"tab-pane\" id=\"app_push\">
                <div class=\"form-group\">
                    ";
        // line 127
        echo form_label("Summary of the visits Today", "visit_summary", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 129
        echo form_textarea(array("name" => "application_config[push][coaching_forms][visit_summary]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "visit_summary", array()), "class" => "form-control input-xxlarge", "id" => "visitSummaryPush"));
        // line 131
        echo "
                    </div>
                </div>

                ";
        // line 135
        $context["excellence_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        // line 136
        echo "                ";
        $context["improvement_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        // line 137
        echo "                ";
        $context["development_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "development_plan_new", array()));
        // line 138
        echo "                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 140
        echo form_label("Areas of Excellence (selling skills, selling tools, chefmanship)", "areas_of_excellence_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 142
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["excellence_new_array"]) {
            // line 143
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 144
            echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 146
            echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_excellence_new][" . $context["key"]) . "]"), "value" => $context["excellence_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['excellence_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 157
        echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 159
        echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_excellence_new][" . (isset($context["excellence_counter_push"]) ? $context["excellence_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 173
        echo form_label("Areas for Improvement", "areas_of_improvement_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 175
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["improvement_new_array"]) {
            // line 176
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 177
            echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 179
            echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_improvement_new][" . $context["key"]) . "]"), "value" => $context["improvement_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['improvement_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 190
        echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 192
        echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_improvement_new][" . (isset($context["improvement_counter_push"]) ? $context["improvement_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 206
        echo form_label("Development Plan", "development_plan_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    ";
        // line 208
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "development_plan_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["development_new_array"]) {
            // line 209
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 210
            echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 212
            echo form_input(array("name" => (("application_config[push][coaching_forms][development_plan_new][" . $context["key"]) . "]"), "value" => $context["development_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['development_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 222
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 223
        echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 225
        echo form_input(array("name" => (("application_config[push][coaching_forms][development_plan_new][" . (isset($context["development_counter_push"]) ? $context["development_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/setting_country/coaching_form_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 225,  411 => 223,  408 => 222,  392 => 212,  387 => 210,  384 => 209,  380 => 208,  375 => 206,  358 => 192,  353 => 190,  350 => 189,  334 => 179,  329 => 177,  326 => 176,  322 => 175,  317 => 173,  300 => 159,  295 => 157,  292 => 156,  276 => 146,  271 => 144,  268 => 143,  264 => 142,  259 => 140,  255 => 138,  252 => 137,  249 => 136,  247 => 135,  241 => 131,  239 => 129,  234 => 127,  217 => 113,  212 => 111,  209 => 110,  193 => 100,  188 => 98,  185 => 97,  181 => 96,  176 => 94,  159 => 80,  154 => 78,  151 => 77,  135 => 67,  130 => 65,  127 => 64,  123 => 63,  118 => 61,  101 => 47,  96 => 45,  93 => 44,  77 => 34,  72 => 32,  69 => 31,  65 => 30,  60 => 28,  56 => 26,  53 => 25,  50 => 24,  48 => 23,  42 => 19,  40 => 17,  35 => 15,  19 => 1,);
    }
}
