<?php

/* ami/ssd/pending.html.twig */
class __TwigTemplate_15f942f6357f3a825d908b263b8146b6ef73124619b53137f346a40d21ac1e3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/ssd/pending.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 14
        echo twig_escape_filter($this->env, site_url("ami/news/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    \$('#BtnApproveAll').click(function(e) {
        e.preventDefault();

        var \$this = \$(this),
            \$form = \$this.closest('form');

        bootbox.confirm(\"Please confirm approval?\", function(result) {
            if (result) {  
                \$.ajax({
                    url: \"";
        // line 31
        echo twig_escape_filter($this->env, site_url("ami/ssd/approve_all"), "html", null, true);
        echo "\",
                    type: 'POST',
                    dataType: 'json',
                    data: \$form.serialize(),
                    beforeSend: function() {
                        \$('#BtnApproveAll').html('<i style=\"font-size: 20px\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i>')
                    },
                    success: function(res) {
                        window.location.reload();
                    }
                });
            }
        });
    });
});
</script>
";
    }

    // line 49
    public function block_css($context, array $blocks = array())
    {
        // line 50
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 54
    public function block_content($context, array $blocks = array())
    {
        // line 55
        echo "
    ";
        // line 56
        echo form_open(site_url("ami/ssd/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Pending Consolidated Orders (";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["pendingSsd"]) ? $context["pendingSsd"] : null), "html", null, true);
        echo ")</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 64
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                     ";
        // line 66
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/ssd/pending.html.twig", 66)->display(array_merge($context, array("url" => "ami/ssd", "icon" => "fa-money", "title" => "SSD", "permission" => "ssd")));
        // line 67
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ssd"]) ? $context["ssd"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["_ssd"]) {
            // line 75
            echo "        <input type=\"hidden\" name=\"ids[]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "id", array()), "html", null, true);
            echo "\" />
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_ssd'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            <div class=\"panel panel-default\">                                
                <div class=\"panel-body\">                
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">                        
                            <thead>
                                <tr>
                                    ";
        // line 87
        echo "                                    <th>ID</th>
                                    <th>Salesperson Name</th>
                                    <th>Operator</th>
                                    <th>Created</th>
                                    <th>Updated</th>                                    
                                    <th class=\"nosort text-center\">
                                        ";
        // line 93
        if ((isset($context["ssd"]) ? $context["ssd"] : null)) {
            // line 94
            echo "                                            ";
            echo html_btn("#", "<i class=\"fa fa-thumbs-up\"></i> Approve All", array("class" => "btn-success approve", "title" => "Approve All", "id" => "BtnApproveAll"));
            // line 98
            echo "
                                        ";
        }
        // line 100
        echo "                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 104
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ssd"]) ? $context["ssd"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["_ssd"]) {
            // line 105
            echo "                                    <tr>
                                        ";
            // line 107
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url((("ami/ssd/edit/" . $this->getAttribute($context["_ssd"], "id", array())) . "?pending=1")), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "id", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_ssd_id", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "last_name", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_customers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_customers_name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 113
            if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()) == "tw")) {
                // line 114
                echo "                                                <div class=\"btn-group\">
                                                    ";
                // line 115
                if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Sales Manager")) {
                    // line 116
                    echo "                                                        ";
                    echo html_btn(site_url((("ami/ssd/edit/" . $this->getAttribute($context["_ssd"], "id", array())) . "?pending=1")), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                    echo "
                                                        ";
                    // line 117
                    echo html_btn(site_url(("ami/ssd/approve/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                    echo "
                                                        ";
                    // line 118
                    echo html_btn(site_url(("ami/ssd/reject/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                }
                // line 120
                echo "                                                </div>
                                            ";
            } else {
                // line 122
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/ssd/pending.html.twig", 122)->display(array_merge($context, array("url" => "ami/ssd", "id" => $this->getAttribute($context["_ssd"], "id", array()), "permission" => "ssd")));
                // line 123
                echo "                                            ";
            }
            // line 124
            echo "                                        </td>                                        
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_ssd'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "                            </tbody>
                        </table>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 140
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/ssd/pending.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 140,  294 => 127,  278 => 124,  275 => 123,  272 => 122,  268 => 120,  263 => 118,  259 => 117,  254 => 116,  252 => 115,  249 => 114,  247 => 113,  242 => 111,  238 => 110,  232 => 109,  224 => 108,  215 => 107,  212 => 105,  195 => 104,  189 => 100,  185 => 98,  182 => 94,  180 => 93,  172 => 87,  161 => 77,  152 => 75,  148 => 74,  139 => 67,  137 => 66,  132 => 64,  126 => 61,  118 => 56,  115 => 55,  112 => 54,  106 => 51,  102 => 50,  99 => 49,  78 => 31,  58 => 14,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
