<?php

/* ami/holidays/index.html.twig */
class __TwigTemplate_15e6049a5e498b6e799e594b4f44bb14af201ab4567be6cd45cdb674208b3343 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/holidays/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                ";
        // line 16
        echo "                ";
        // line 17
        echo "                ";
        // line 18
        echo "                ";
        // line 19
        echo "                ";
        // line 20
        echo "                ";
        // line 21
        echo "                ";
        // line 22
        echo "                ";
        // line 23
        echo "                ";
        // line 24
        echo "                ";
        // line 25
        echo "                ";
        // line 26
        echo "                ";
        // line 27
        echo "                ";
        // line 28
        echo "                ";
        // line 29
        echo "                ";
        // line 30
        echo "                ";
        // line 31
        echo "                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"100px\", \"targets\": [0,1]},
                    {\"width\": \"250px\", \"targets\": [2]}
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 55
    public function block_css($context, array $blocks = array())
    {
        // line 56
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 60
    public function block_content($context, array $blocks = array())
    {
        // line 61
        echo "
    ";
        // line 62
        echo form_open(site_url("ami/holidays/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 67
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Holidays Draft") : ("Holidays"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 71
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>

                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 75
        echo twig_escape_filter($this->env, site_url("ami/holidays/add"), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                        ";
        // line 77
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 78
            echo "                            <a class=\"btn btn-sm btn-default\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/holidays"), "html", null, true);
            echo "\"><i
                                        class=\"fa fw fa-tags\"></i> Holidays</a>
                        ";
        } else {
            // line 81
            echo "                            ";
            // line 82
            echo "                        ";
        }
        // line 83
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 100
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "holiday_settings"))) {
            // line 101
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 105
        echo "                                <th>Holiday ID</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["holidays"]) ? $context["holidays"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["holiday"]) {
            // line 115
            echo "                                <tr>
                                    ";
            // line 116
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "holiday_settings"))) {
                // line 117
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["holiday"], "armstrong_2_holiday_id", array())));
                echo "</td>
                                    ";
            }
            // line 119
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["holiday"], "armstrong_2_holiday_id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["holiday"], "title", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["holiday"], "date", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["holiday"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["holiday"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 125
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "holiday_settings")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "holiday_settings")))) {
                // line 126
                echo "                                            <div class=\"btn-group\">
                                                ";
                // line 127
                echo html_btn(site_url(("ami/holidays/edit/" . $this->getAttribute($context["holiday"], "armstrong_2_holiday_id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                                                ";
                // line 128
                echo html_btn(site_url(("ami/holidays/delete/" . $this->getAttribute($context["holiday"], "armstrong_2_holiday_id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                                            </div>
                                            ";
                // line 131
                echo "                                        ";
            }
            // line 132
            echo "                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['holiday'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "                            </tbody>
                        </table>
                    </div>

                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 148
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/holidays/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 148,  277 => 135,  269 => 132,  266 => 131,  261 => 128,  257 => 127,  254 => 126,  252 => 125,  247 => 123,  243 => 122,  239 => 121,  235 => 120,  230 => 119,  224 => 117,  222 => 116,  219 => 115,  215 => 114,  204 => 105,  198 => 101,  196 => 100,  177 => 83,  174 => 82,  172 => 81,  165 => 78,  163 => 77,  158 => 75,  151 => 71,  144 => 67,  136 => 62,  133 => 61,  130 => 60,  124 => 57,  119 => 56,  116 => 55,  90 => 31,  88 => 30,  86 => 29,  84 => 28,  82 => 27,  80 => 26,  78 => 25,  76 => 24,  74 => 23,  72 => 22,  70 => 21,  68 => 20,  66 => 19,  64 => 18,  62 => 17,  60 => 16,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
