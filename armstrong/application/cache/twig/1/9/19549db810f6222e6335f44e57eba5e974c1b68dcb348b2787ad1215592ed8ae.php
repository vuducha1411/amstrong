<?php

/* ami/salespersons/index.html.twig */
class __TwigTemplate_19549db810f6222e6335f44e57eba5e974c1b68dcb348b2787ad1215592ed8ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/salespersons/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                ";
        // line 14
        echo "                ";
        // line 15
        echo "                ";
        // line 16
        echo "                ";
        // line 17
        echo "                ";
        // line 18
        echo "                ";
        // line 19
        echo "//                'aoColumns': [
//                    {mData: \"armstrong_2_salespersons_id\"},
//                    {mData: \"first_name\"},
//                    {mData: \"last_name\"},
//                    {mData: \"email\"},
//                    {mData: \"date_created\", \"bSearchable\": false},
//                    {mData: \"last_updated\", \"bSearchable\": false}
//                ],
                ";
        // line 28
        echo "                ";
        // line 29
        echo "                ";
        // line 30
        echo "                ";
        // line 31
        echo "                ";
        // line 32
        echo "                ";
        // line 33
        echo "                'order': [[2, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
//                    ,{
//                        \"targets\": [3],
//                        \"bSearchable\": false
//                    }
//                    ,{
//                        \"targets\": [4],
//                        \"bSearchable\": false
//                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 64
    public function block_css($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 69
    public function block_content($context, array $blocks = array())
    {
        // line 70
        echo "
    ";
        // line 71
        echo form_open(site_url("ami/salespersons/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 76
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Salespersons Draft") : ("Salespersons"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 80
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 82
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/salespersons/index.html.twig", 82)->display(array_merge($context, array("url" => "ami/salespersons", "icon" => "fa-user", "title" => "Salespersons", "permission" => "salespersons", "export" => 1, "showImport" => 1)));
        // line 83
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"";
        // line 94
        echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("active"));
        echo "\"><a role=\"menuitem\"
                                                                                href=\"";
        // line 95
        echo twig_escape_filter($this->env, site_url("ami/salespersons"), "html", null, true);
        echo "\">Active</a>
                </li>
                <li role=\"presentation\" class=\"";
        // line 97
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("active") : (""));
        echo "\"><a role=\"menuitem\"
                                                                                              href=\"";
        // line 98
        echo twig_escape_filter($this->env, site_url("ami/salespersons?filter=inactive"), "html", null, true);
        echo "\">Inactive</a>
                </li>
            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 109
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "salespersons"))) {
            // line 110
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 114
        echo "                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>User Role</th>
                                <th>User Type</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Updated By</th>
                                <th>Is Test acc</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 128
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["salespersons"]) ? $context["salespersons"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sales"]) {
            // line 129
            echo "                                <tr>
                                    ";
            // line 130
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "salespersons"))) {
                // line 131
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["sales"], "armstrong_2_salespersons_id", array())));
                echo "</td>
                                    ";
            }
            // line 133
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "first_name", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "last_name", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "email", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "roles_id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "type", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($context["sales"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 141
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["sales"], "updated_name", array()) != "")) ? ($this->getAttribute($context["sales"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 142
            echo ((($this->getAttribute($context["sales"], "test_acc", array()) == 1)) ? ("Yes") : ("No"));
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 144
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "salespersons")))) {
                // line 145
                echo "                                            <div class=\"btn-group\">
                                                ";
                // line 146
                echo html_btn(site_url(("ami/salespersons/edit/" . $this->getAttribute($context["sales"], "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                                                ";
                // line 147
                echo html_btn(site_url(("ami/salespersons/delete/" . $this->getAttribute($context["sales"], "armstrong_2_salespersons_id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                                            </div>
                                            ";
                // line 150
                echo "                                        ";
            }
            // line 151
            echo "                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sales'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 167
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/salespersons/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 167,  309 => 154,  301 => 151,  298 => 150,  293 => 147,  289 => 146,  286 => 145,  284 => 144,  279 => 142,  275 => 141,  271 => 140,  267 => 139,  263 => 138,  259 => 137,  255 => 136,  251 => 135,  247 => 134,  242 => 133,  236 => 131,  234 => 130,  231 => 129,  227 => 128,  211 => 114,  205 => 110,  203 => 109,  189 => 98,  185 => 97,  180 => 95,  176 => 94,  163 => 83,  161 => 82,  156 => 80,  149 => 76,  141 => 71,  138 => 70,  135 => 69,  129 => 66,  124 => 65,  121 => 64,  88 => 33,  86 => 32,  84 => 31,  82 => 30,  80 => 29,  78 => 28,  68 => 19,  66 => 18,  64 => 17,  62 => 16,  60 => 15,  58 => 14,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
