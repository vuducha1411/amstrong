<?php

/* ami/customers/pending_action_data.html.twig */
class __TwigTemplate_133a993647c9607c8f59b7f82c896e50f5bedb43494a241f3b892f7b3d0d72f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"panel\">
    ";
        // line 71
        echo "    <table class=\"table table-striped table-bordered table-hover tabletvo\">
        <thead>
        <tr>
            <th>Field</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 80
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["customer"]) ? $context["customer"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 81
            echo "            ";
            if ((($context["key"] == "cuisine_name") || ($context["key"] == "serving_name"))) {
                // line 82
                echo "            ";
            } else {
                // line 83
                echo "
                <tr class=\"";
                // line 84
                echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                echo "\">
                    <td>
                        ";
                // line 86
                if (($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "country_id", array(), "array") == 6)) {
                    // line 87
                    echo "                            ";
                    if (($context["key"] == "cuisine_channels_id")) {
                        // line 88
                        echo "                                ";
                        echo "Sub OG_id";
                        echo "
                            ";
                    } elseif ((                    // line 89
$context["key"] == "cuisine_channels_name")) {
                        // line 90
                        echo "                                ";
                        echo "Sub OG_name";
                        echo "
                            ";
                    } elseif ((                    // line 91
$context["key"] == "cuisine_groups_id")) {
                        // line 92
                        echo "                                ";
                        echo "OG_name";
                        echo "
                            ";
                    } else {
                        // line 94
                        echo "                                ";
                        echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                        echo "
                            ";
                    }
                    // line 96
                    echo "                        ";
                } else {
                    // line 97
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "
                        ";
                }
                // line 99
                echo "                    </td>
                    <td>";
                // line 100
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</td>
                </tr>
                ";
                // line 102
                if (($context["key"] == "cuisine_channels_id")) {
                    // line 103
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 104
                    echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "country_id", array(), "array") == 6)) ? ("Sub OG_name") : ("cuisine_channels_name"));
                    echo "</td>
                        <td>";
                    // line 105
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "cuisine_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 108
                echo "                ";
                if (($context["key"] == "serving_types_id")) {
                    // line 109
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 110
                    echo "serving_types_name";
                    echo "</td>
                        <td>";
                    // line 111
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "serving_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 114
                echo "
            ";
            }
            // line 116
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/customers/pending_action_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 117,  138 => 116,  134 => 114,  128 => 111,  124 => 110,  119 => 109,  116 => 108,  110 => 105,  106 => 104,  101 => 103,  99 => 102,  94 => 100,  91 => 99,  85 => 97,  82 => 96,  76 => 94,  70 => 92,  68 => 91,  63 => 90,  61 => 89,  56 => 88,  53 => 87,  51 => 86,  46 => 84,  43 => 83,  40 => 82,  37 => 81,  32 => 80,  22 => 71,  19 => 1,);
    }
}
