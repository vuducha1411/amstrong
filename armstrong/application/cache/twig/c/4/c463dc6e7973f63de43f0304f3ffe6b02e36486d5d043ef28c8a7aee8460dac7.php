<?php

/* ami/news/index.html.twig */
class __TwigTemplate_c463dc6e7973f63de43f0304f3ffe6b02e36486d5d043ef28c8a7aee8460dac7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/news/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/news/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        // 'bPaginate': false,
        \"iDisplayLength\": 20,
        'aoColumnDefs': [{
            // 'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
});
</script>
";
    }

    // line 28
    public function block_css($context, array $blocks = array())
    {
        // line 29
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 33
    public function block_content($context, array $blocks = array())
    {
        // line 34
        echo "
    ";
        // line 35
        echo form_open(site_url("ami/news/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 40
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("News Draft") : ("News"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 43
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 45
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/news/index.html.twig", 45)->display(array_merge($context, array("url" => "ami/news", "icon" => "fa-list-alt", "title" => "News", "permission" => "news")));
        // line 46
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">  

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#listed\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Listed</a></li>
                <li role=\"presentation\"><a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Delisted</a></li>
            </ul>

            <div class=\"panel panel-default m-t\">                

                <div class=\"panel-body tab-content\">                
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 68
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
            // line 69
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 71
        echo "                                    <th>Title</th>
                                    <th>Summary</th>
                                    <th>Global</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["news"]) ? $context["news"] : null), "active", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
            // line 81
            echo "                                    <tr>
                                        ";
            // line 82
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
                // line 83
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["new"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 85
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/news/edit/" . $this->getAttribute($context["new"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "title", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim($this->getAttribute($context["new"], "title", array()), 50);
            echo "</a></td>
                                        <td>";
            // line 86
            echo wordTrim($this->getAttribute($context["new"], "summary", array()), 100);
            echo "</td>
                                        <td>";
            // line 87
            echo ((($this->getAttribute($context["new"], "global", array()) == 1)) ? ("Yes") : ("No"));
            echo "</td>
                                        <td class=\"center\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 91
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "news")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news")))) {
                // line 92
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/news/index.html.twig", 92)->display(array_merge($context, array("url" => "ami/news", "id" => $this->getAttribute($context["new"], "id", array()), "permission" => "news")));
                // line 93
                echo "                                            ";
            }
            // line 94
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "                            </tbody>
                        </table>                        
                    </div>

                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 105
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
            // line 106
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 108
        echo "                                    <th>Title</th>
                                    <th>Summary</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 116
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["news"]) ? $context["news"] : null), "inactive", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
            // line 117
            echo "                                    <tr>
                                        ";
            // line 118
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news"))) {
                // line 119
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["new"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 121
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/news/edit/" . $this->getAttribute($context["new"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "title", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim($this->getAttribute($context["new"], "title", array()), 50);
            echo "</a></td>
                                        <td>";
            // line 122
            echo wordTrim($this->getAttribute($context["new"], "summary", array()), 100);
            echo "</td>
                                        <td class=\"center\">";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 126
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "news")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news")))) {
                // line 127
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/news/index.html.twig", 127)->display(array_merge($context, array("url" => "ami/news", "id" => $this->getAttribute($context["new"], "id", array()), "permission" => "news")));
                // line 128
                echo "                                            ";
            }
            // line 129
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "                            </tbody>
                        </table>                        
                    </div>
                </div>             
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 142
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/news/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  340 => 142,  328 => 132,  312 => 129,  309 => 128,  306 => 127,  304 => 126,  299 => 124,  295 => 123,  291 => 122,  282 => 121,  276 => 119,  274 => 118,  271 => 117,  254 => 116,  244 => 108,  240 => 106,  238 => 105,  228 => 97,  212 => 94,  209 => 93,  206 => 92,  204 => 91,  199 => 89,  195 => 88,  191 => 87,  187 => 86,  178 => 85,  172 => 83,  170 => 82,  167 => 81,  150 => 80,  139 => 71,  135 => 69,  133 => 68,  109 => 46,  107 => 45,  102 => 43,  96 => 40,  88 => 35,  85 => 34,  82 => 33,  76 => 30,  72 => 29,  69 => 28,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
