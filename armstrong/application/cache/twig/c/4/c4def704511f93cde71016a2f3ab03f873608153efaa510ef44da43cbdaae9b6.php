<?php

/* ami/promotional_mechanics/edit.html.twig */
class __TwigTemplate_c4def704511f93cde71016a2f3ab03f873608153efaa510ef44da43cbdaae9b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotional_mechanics/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function applicable_reset() {
            \$('#discount_applicable, #money_applicable').attr('disabled', true);
            \$('#discount_applicable, #money_applicable').attr('data-parsley-required', false);
        }

        \$(function () {
            createUploader(\$('#imageUpload'));
            createUploader(\$('#videoUpload'));
            createUploader(\$('#brochureUpload'));

            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });

            /*\$('#applicable').on('change', function () {
                var \$val = \$(this).val(),
                        money_element = \$('#money_applicable'),
                        discount_element = \$('#discount_applicable');
                applicable_reset();
                if(\$val == 'money'){
                    money_element.removeAttr('disabled');
                    money_element.attr('data-parsley-required', true);
                    if(discount_element.val() == 0) discount_element.val('');
                }else if(\$val == 'discount'){
                    discount_element.removeAttr('disabled');
                    discount_element.attr('data-parsley-required', true);
                    if(money_element.val() == 0) money_element.val('');
                }else{
                    if(discount_element.val() == 0) discount_element.val('');
                    if(money_element.val() == 0) money_element.val('');
                }
            });*/
        });

        \$(document).ready(function(){
            /*var \$val = \$('#applicable').val(),
                    money_element = \$('#money_applicable'),
                    discount_element = \$('#discount_applicable');
            applicable_reset();
            if(\$val == 'money'){
                money_element.removeAttr('disabled');
                money_element.attr('data-parsley-required', true);
                if(discount_element.val() == 0) discount_element.val('');
            }else if(\$val == 'discount'){
                discount_element.removeAttr('disabled');
                discount_element.attr('data-parsley-required', true);
                if(money_element.val() == 0) money_element.val('');
            }else{
                if(discount_element.val() == 0) discount_element.val('');
                if(money_element.val() == 0) money_element.val('');
            }*/
        });
    </script>
";
    }

    // line 76
    public function block_css($context, array $blocks = array())
    {
        // line 77
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 81
    public function block_content($context, array $blocks = array())
    {
        // line 82
        echo "    ";
        echo form_open(site_url("ami/promotional_mechanics/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Promotional Mechanics</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 91
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 93
        echo html_btn(site_url("ami/promotional_mechanics"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 94
        if ($this->getAttribute((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null), "id", array())) {
            // line 95
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/promotional_mechanics/delete/" . $this->getAttribute((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null), "id", array()))), "html", null, true);
            echo "\"
                               title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 99
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 108
        if (((isset($context["filter"]) ? $context["filter"] : null) == "promo_1")) {
            // line 109
            echo "                ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_1_edit.html.twig", "ami/promotional_mechanics/edit.html.twig", 109)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 110
            echo "            ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_2")) {
            // line 111
            echo "                ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_2_edit.html.twig", "ami/promotional_mechanics/edit.html.twig", 111)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 112
            echo "            ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_3")) {
            // line 113
            echo "                ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_3_edit.html.twig", "ami/promotional_mechanics/edit.html.twig", 113)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 114
            echo "            ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_4")) {
            // line 115
            echo "                ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_4_edit.html.twig", "ami/promotional_mechanics/edit.html.twig", 115)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 116
            echo "            ";
        } else {
            // line 117
            echo "                ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_5_edit.html.twig", "ami/promotional_mechanics/edit.html.twig", 117)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 118
            echo "            ";
        }
        // line 119
        echo "
            ";
        // line 120
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/promotional_mechanics/edit.html.twig", 120)->display(array_merge($context, array("entry_type" => "promotional_mechanics")));
        // line 121
        echo "
            ";
        // line 122
        $this->loadTemplate("ami/components/uploader_video.html.twig", "ami/promotional_mechanics/edit.html.twig", 122)->display(array_merge($context, array("entry_type" => "promotional_mechanics")));
        // line 123
        echo "
            ";
        // line 124
        $this->loadTemplate("ami/components/uploader_brochure.html.twig", "ami/promotional_mechanics/edit.html.twig", 124)->display(array_merge($context, array("entry_type" => "promotional_mechanics")));
        // line 125
        echo "
            ";
        // line 126
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["promotional"]) ? $context["promotional"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "
            ";
        // line 127
        echo form_input(array("name" => "filter", "value" => (isset($context["filter"]) ? $context["filter"] : null), "type" => "hidden", "class" => "hide"));
        echo "
            ";
        // line 128
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 132
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 132,  242 => 128,  238 => 127,  234 => 126,  231 => 125,  229 => 124,  226 => 123,  224 => 122,  221 => 121,  219 => 120,  216 => 119,  213 => 118,  210 => 117,  207 => 116,  204 => 115,  201 => 114,  198 => 113,  195 => 112,  192 => 111,  189 => 110,  186 => 109,  184 => 108,  173 => 99,  165 => 95,  163 => 94,  159 => 93,  154 => 91,  141 => 82,  138 => 81,  132 => 78,  127 => 77,  124 => 76,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
