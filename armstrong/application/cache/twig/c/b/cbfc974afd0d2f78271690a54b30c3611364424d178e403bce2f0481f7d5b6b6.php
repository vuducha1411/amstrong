<?php

/* ami/distributors/pending_action_data.html.twig */
class __TwigTemplate_cbfc974afd0d2f78271690a54b30c3611364424d178e403bce2f0481f7d5b6b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"panel\">

    <table class=\"table table-striped table-bordered table-hover\">
        <thead>
            <tr>
                <th>Field</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 12
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["distributor"]) ? $context["distributor"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 13
            echo "                ";
            if ((($context["key"] == "cuisine_name") || ($context["key"] == "serving_name"))) {
                // line 14
                echo "            ";
            } else {
                // line 15
                echo "
                <tr class=\"";
                // line 16
                echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                echo "\">
                    <td>";
                // line 17
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</td>
                    <td>";
                // line 18
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</td>
                </tr>
                ";
                // line 20
                if (($context["key"] == "cuisine_channels_id")) {
                    // line 21
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 22
                    echo "cuisine_channels_name";
                    echo "</td>
                        <td>";
                    // line 23
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "cuisine_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 26
                echo "                ";
                if (($context["key"] == "serving_types_id")) {
                    // line 27
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 28
                    echo "serving_types_name";
                    echo "</td>
                        <td>";
                    // line 29
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "serving_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 32
                echo "
            ";
            }
            // line 34
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/distributors/pending_action_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 35,  97 => 34,  93 => 32,  87 => 29,  83 => 28,  78 => 27,  75 => 26,  69 => 23,  65 => 22,  60 => 21,  58 => 20,  53 => 18,  49 => 17,  45 => 16,  42 => 15,  39 => 14,  36 => 13,  31 => 12,  19 => 1,);
    }
}
