<?php

/* ami/components/modal_import.html.twig */
class __TwigTemplate_c720a2dc95425a999a1b2a2f4132a8dbb2d9ac082ce3d265cf745571ffad6040 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a class=\"btn btn-primary hide\" id=\"ImportModalTrigger\" data-toggle=\"modal\" data-target=\"#ImportModal\">Button</a>

<div class=\"modal fade\" id=\"ImportModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"ImportModalLabel\" aria-hidden=\"true\" data-backdrop=\"static\">
    <div class=\"modal-dialog\">
        ";
        // line 5
        echo form_open((isset($context["action"]) ? $context["action"] : null), array("class" => "modal-content"), array("redirect" => (isset($context["redirect"]) ? $context["redirect"] : null)));
        echo "
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                <h3 id=\"ImportModalLabel\">Import</h3>
            </div>
            <div class=\"modal-body\">
                
            </div>
            <div class=\"modal-footer\">
                <div class=\"modal-footer-message\"></div>    
            </div>
        ";
        // line 16
        echo form_close();
        echo "
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/modal_import.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 16,  25 => 5,  19 => 1,);
    }
}
