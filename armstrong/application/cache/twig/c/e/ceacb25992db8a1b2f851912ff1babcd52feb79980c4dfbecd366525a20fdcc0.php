<?php

/* ami/products/preview.html.twig */
class __TwigTemplate_ceacb25992db8a1b2f851912ff1babcd52feb79980c4dfbecd366525a20fdcc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sku_name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <p>";
        // line 10
        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array());
        echo "</p>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>            
            ";
        // line 16
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "product"))) {
            // line 17
            echo "                ";
            echo html_btn(site_url(("ami/products/edit/" . $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 19
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/products/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 19,  44 => 17,  42 => 16,  33 => 10,  25 => 5,  19 => 1,);
    }
}
