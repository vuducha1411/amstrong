<?php

/* ami/discount_model/setting.html.twig */
class __TwigTemplate_c1ad5a2d72994bc809592d6c175fe8b79afefb2292da9ea5d87d9dbd9ed184df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/discount_model/setting.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                ";
        // line 18
        echo "                ";
        // line 19
        echo "                ";
        // line 20
        echo "                ";
        // line 21
        echo "                ";
        // line 22
        echo "                ";
        // line 23
        echo "                ";
        // line 24
        echo "                ";
        // line 25
        echo "                ";
        // line 26
        echo "                ";
        // line 27
        echo "                ";
        // line 28
        echo "                ";
        // line 29
        echo "                ";
        // line 30
        echo "                ";
        // line 31
        echo "                ";
        // line 32
        echo "                ";
        // line 33
        echo "                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });
        });
    </script>
";
    }

    // line 46
    public function block_css($context, array $blocks = array())
    {
        // line 47
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 51
    public function block_content($context, array $blocks = array())
    {
        // line 52
        echo "
    ";
        // line 53
        echo form_open(site_url("ami/discount_model/update_setting"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Discount Setting</h1>

                <div class=\"text-right\">
                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-warning\" href=\"javascript:;\" data-toggle=\"modal\"
                           data-target=\"#upload_template\"><i
                                    class=\"fa fw fa-upload\"></i> Upload</a>
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, site_url("ami/discount_model/add_setting"), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        ";
        // line 77
        echo "        ";
        if ($this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array())) {
            // line 78
            echo "            <div class=\"alert ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "flash_type", array()), "html", null, true);
            echo " alert-dismissible col-lg-12\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                            aria-hidden=\"true\">&times;</span></button>
                <strong>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array()), "html", null, true);
            echo "</strong>
            </div>
        ";
        }
        // line 84
        echo "        <div class=\"col-lg-12\">
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 91
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "discount_model"))) {
            // line 92
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 96
        echo "                                <th>Cat Web Name</th>
                                <th>Sku number list</th>
                                <th>Type</th>
                                <th>Tier 1 Case</th>
                                <th>Tier 1 Discount</th>
                                <th>Tier 2 Case</th>
                                <th>Tier 2 Discount</th>
                                ";
        // line 104
        echo "                                ";
        // line 105
        echo "                                <th>From Date</th>
                                <th>To Date</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 111
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["settings"]) ? $context["settings"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["setting"]) {
            // line 112
            echo "                                <tr>
                                    ";
            // line 113
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "discount_model"))) {
                // line 114
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["setting"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 116
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "cat_web_name", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 117
            echo $this->getAttribute($context["setting"], "sku_number", array());
            echo "</td>
                                    <td>";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "type", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "tier_1_case", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "tier_1_discount", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "tier_2_case", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["setting"], "tier_2_discount", array()), "html", null, true);
            echo "</td>
                                    ";
            // line 124
            echo "                                    ";
            // line 125
            echo "                                    <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["setting"], "range", array()), "from_date", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["setting"], "range", array()), "to_date", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 128
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "discount_model")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "discount_model")))) {
                // line 129
                echo "                                            <div class=\"btn-group\">
                                                ";
                // line 130
                echo html_btn(site_url(("ami/discount_model/edit_setting/" . $this->getAttribute($context["setting"], "id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                                                ";
                // line 131
                echo html_btn(site_url(("ami/discount_model/delete_setting/" . $this->getAttribute($context["setting"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                                            </div>
                                        ";
            }
            // line 134
            echo "                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['setting'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 137
        echo "                            </tbody>
                        </table>
                    </div>

                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 150
        echo form_close();
        echo "
    <div class=\"modal fade\" id=\"upload_template\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog\">
            ";
        // line 153
        echo form_open(site_url("ami/discount_model/upload_template"), array("class" => "modal-content", "id" => "upload_form", "enctype" => "multipart/form-data"));
        echo "
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                <h3 id=\"myModalLabel\">Upload Template</h3>
            </div>
            <div class=\"modal-body\">
                ";
        // line 160
        echo "                    <div class=\"col-sm-12\">
                        <div class=\"form-group row\">
                            ";
        // line 162
        echo form_label("Date Range", "range_id", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-9\">
                                ";
        // line 164
        echo form_dropdown("range_id", (isset($context["range"]) ? $context["range"] : null), null, "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            ";
        // line 168
        echo form_label("File", "file", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-9\">
                                <input type=\"file\" name=\"file\">
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-sm-offset-3\">
                                <a href=\"";
        // line 175
        echo twig_escape_filter($this->env, site_url("res/up/template.xlsx"), "html", null, true);
        echo "\" class=\"bg-info\">Click here to download example template.</a>
                            </div>
                        </div>
                    </div>
                ";
        // line 180
        echo "            </div>
            <div class=\"modal-footer\">
                <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close
                </button>
                ";
        // line 184
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Proceed", "class" => "btn btn-danger"));
        echo "
            </div>
            ";
        // line 186
        echo form_close();
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/discount_model/setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 186,  356 => 184,  350 => 180,  343 => 175,  333 => 168,  326 => 164,  321 => 162,  317 => 160,  308 => 153,  302 => 150,  287 => 137,  279 => 134,  273 => 131,  269 => 130,  266 => 129,  264 => 128,  259 => 126,  254 => 125,  252 => 124,  248 => 122,  244 => 121,  240 => 120,  236 => 119,  232 => 118,  228 => 117,  223 => 116,  217 => 114,  215 => 113,  212 => 112,  208 => 111,  200 => 105,  198 => 104,  189 => 96,  183 => 92,  181 => 91,  172 => 84,  166 => 81,  159 => 78,  156 => 77,  142 => 65,  127 => 53,  124 => 52,  121 => 51,  115 => 48,  110 => 47,  107 => 46,  92 => 33,  90 => 32,  88 => 31,  86 => 30,  84 => 29,  82 => 28,  80 => 27,  78 => 26,  76 => 25,  74 => 24,  72 => 23,  70 => 22,  68 => 21,  66 => 20,  64 => 19,  62 => 18,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
