<?php

/* ami/ssd/ssd.html.twig */
class __TwigTemplate_cdbe283d5cc8aa844fcc60670d19e2d74c903ae4a4a8e010f4044f754a9ae3ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["ssd"]) ? $context["ssd"] : null)) {
            echo " 

    <div class=\"panel panel-default\">                            
        <div class=\"panel-body\">                
            <div class=\"\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">                    
                    <thead>
                        <tr>
                            ";
            // line 9
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "ssd"))) {
                // line 10
                echo "                                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                            ";
            }
            // line 12
            echo "                            <th>ID</th>
                            <th>SSD ID</th>
                            <th>Salesperson </th>
                            <th>Operator</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>SSD date</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 24
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["ssd"]) ? $context["ssd"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["_ssd"]) {
                // line 25
                echo "                            <tr>
                                ";
                // line 26
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "ssd"))) {
                    // line 27
                    echo "                                    <td class=\"text-center\">";
                    echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["_ssd"], "id", array())));
                    echo "</td>
                                ";
                }
                // line 29
                echo "                                <td><a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/ssd/edit/" . $this->getAttribute($context["_ssd"], "id", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "id", array()), "html", null, true);
                echo "\" data-toggle=\"ajaxModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "id", array()), "html", null, true);
                echo "</a></td>
                                <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_ssd_id", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "last_name", array()), "html", null, true);
                echo " </td>
                                <td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_customers_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "armstrong_2_customers_name", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 33
                echo ((($this->getAttribute($context["_ssd"], "status", array()) == 1)) ? ("Approved") : (((($this->getAttribute($context["_ssd"], "status", array()) == 0)) ? ("Pending") : ("Rejected"))));
                echo "</td>
                                <td class=\"center\">";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "date_created", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["_ssd"], "last_updated", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => $this->getAttribute($context["_ssd"], "ssd_date", array())), "method"), "toDateTimeString", array(), "method"), "html", null, true);
                echo "</td>
                                <td class=\"center text-center\" style=\"min-width: 80px;\">
                                    ";
                // line 38
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "ssd")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "ssd")))) {
                    // line 39
                    echo "                                        ";
                    if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()) == "tw")) {
                        // line 40
                        echo "                                            <div class=\"btn-group\">
                                                ";
                        // line 41
                        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "ssd"))) {
                            echo "                                            
                                                    ";
                            // line 42
                            echo html_btn(site_url(("ami/ssd/edit/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                            echo "

                                                    ";
                            // line 44
                            if (((isset($context["filter"]) ? $context["filter"] : null) == "pending")) {
                                // line 45
                                echo "                                                        ";
                                echo html_btn(site_url(("ami/ssd/approve/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                                echo "
                                                        ";
                                // line 46
                                echo html_btn(site_url(("ami/ssd/reject/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                                echo "
                                                    ";
                            }
                            // line 48
                            echo "                                                ";
                        }
                        // line 49
                        echo "
                                                ";
                        // line 50
                        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "ssd"))) {
                            // line 51
                            echo "                                                    ";
                            if ((isset($context["draft"]) ? $context["draft"] : null)) {
                                // line 52
                                echo "                                                        ";
                                echo html_btn(site_url(("ami/ssd/restore/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                                echo "        
                                                    ";
                            } else {
                                // line 54
                                echo "                                                        ";
                                echo html_btn(site_url(("ami/ssd/delete/" . $this->getAttribute($context["_ssd"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                                echo "
                                                    ";
                            }
                            // line 56
                            echo "                                                ";
                        }
                        // line 57
                        echo "                                            </div>
                                        ";
                    } else {
                        // line 59
                        echo "                                            ";
                        $this->loadTemplate("ami/components/table_btn.html.twig", "ami/ssd/ssd.html.twig", 59)->display(array_merge($context, array("url" => "ami/ssd", "id" => $this->getAttribute($context["_ssd"], "id", array()), "permission" => "ssd")));
                        // line 60
                        echo "                                        ";
                    }
                    // line 61
                    echo "                                    ";
                }
                // line 62
                echo "                                </td>
                            </tr>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_ssd'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                    </tbody>
                </table>
            </div>
            <!-- /. -->
        </div>
        <!-- /.panel-body -->

    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('#dataTables').dataTable({
            'order': [[1, 'asc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }],
            \"iDisplayLength\": 50
        });        
    });  
    </script>
";
        } else {
            // line 90
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no SSD</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/ssd/ssd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 90,  206 => 65,  190 => 62,  187 => 61,  184 => 60,  181 => 59,  177 => 57,  174 => 56,  168 => 54,  162 => 52,  159 => 51,  157 => 50,  154 => 49,  151 => 48,  146 => 46,  141 => 45,  139 => 44,  134 => 42,  130 => 41,  127 => 40,  124 => 39,  122 => 38,  117 => 36,  113 => 35,  109 => 34,  105 => 33,  99 => 32,  91 => 31,  87 => 30,  78 => 29,  72 => 27,  70 => 26,  67 => 25,  50 => 24,  36 => 12,  32 => 10,  30 => 9,  19 => 1,);
    }
}
