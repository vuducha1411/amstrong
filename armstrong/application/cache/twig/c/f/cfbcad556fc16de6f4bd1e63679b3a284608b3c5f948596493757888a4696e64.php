<?php

/* ami/products/index.html.twig */
class __TwigTemplate_cfbcad556fc16de6f4bd1e63679b3a284608b3c5f948596493757888a4696e64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/products/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(document).ready(function () {
            \$('.dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/products/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'asc']],
                'aoColumnDefs': [{
                    'aTargets': ['nosort']
                }],
                \"iDisplayLength\": 50
            });
        });
    </script>
";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
        // line 32
        echo "
    ";
        // line 33
        echo form_open(site_url("ami/products/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 38
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Products Draft") : ("Products"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 42
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 44
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/products/index.html.twig", 44)->display(array_merge($context, array("url" => "ami/products", "icon" => "fa-barcode", "title" => "Products", "permission" => "product")));
        // line 45
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#listed\" aria-controls=\"home\" role=\"tab\"
                                                          data-toggle=\"tab\">Listed</a></li>
                <li role=\"presentation\"><a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Delisted</a>
                </li>
            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 69
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product"))) {
            // line 70
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 74
        echo "                                <th>SKU name</th>
                                <th>SKU number</th>
                                <th>App type</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Updated By</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products"]) ? $context["products"] : null), "active", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 85
            echo "                                <tr>
                                    ";
            // line 86
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product"))) {
                // line 87
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["product"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 89
            echo "                                    <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/products/edit/" . $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\"
                                           title=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_name", array()), "html", null, true);
            echo "\"
                                           data-toggle=\"ajaxModal\">";
            // line 91
            echo wordTrim(((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ($this->getAttribute($context["product"], "name_alt", array())) : ($this->getAttribute($context["product"], "sku_name", array()))), 50);
            echo "</a>
                                    </td>
                                    <td>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 94
            echo ((($this->getAttribute($context["product"], "app_type", array()) == 0)) ? ("PULL") : (((($this->getAttribute($context["product"], "app_type", array()) == 1)) ? ("PUSH") : ("ALL"))));
            echo "</td>
                                    <td class=\"center\">";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 97
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["product"], "updated_name", array()) != "")) ? ($this->getAttribute($context["product"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 99
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "product")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product")))) {
                // line 100
                echo "                                            ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/products/index.html.twig", 100)->display(array_merge($context, array("url" => "ami/products", "id" => $this->getAttribute($context["product"], "id", array()), "permission" => "product")));
                // line 101
                echo "                                        ";
            }
            // line 102
            echo "                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 105
        echo "                            </tbody>
                        </table>
                    </div>

                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 113
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product"))) {
            // line 114
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 118
        echo "                                <th>SKU name</th>
                                <th>SKU number</th>
                                <th>Created</th>
                                <th>Updated</th>
                                ";
        // line 122
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "product")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product")))) {
            // line 123
            echo "                                    <th class=\"nosort text-center\"></th>
                                ";
        }
        // line 125
        echo "                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 128
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products"]) ? $context["products"] : null), "inactive", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 129
            echo "                                <tr>
                                    ";
            // line 130
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product"))) {
                // line 131
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["product"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 133
            echo "                                    <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/products/edit/" . $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\"
                                           title=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_name", array()), "html", null, true);
            echo "\"
                                           data-toggle=\"ajaxModal\">";
            // line 135
            echo wordTrim(((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ($this->getAttribute($context["product"], "name_alt", array())) : ($this->getAttribute($context["product"], "sku_name", array()))), 50);
            echo "</a>
                                    </td>
                                    <td>";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    ";
            // line 140
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "product")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "product")))) {
                // line 141
                echo "                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
                // line 142
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/products/index.html.twig", 142)->display(array_merge($context, array("url" => "ami/products", "id" => $this->getAttribute($context["product"], "id", array()), "permission" => "product")));
                // line 143
                echo "                                        </td>
                                    ";
            }
            // line 145
            echo "                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 158
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/products/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  371 => 158,  358 => 147,  343 => 145,  339 => 143,  337 => 142,  334 => 141,  332 => 140,  328 => 139,  324 => 138,  320 => 137,  315 => 135,  311 => 134,  306 => 133,  300 => 131,  298 => 130,  295 => 129,  278 => 128,  273 => 125,  269 => 123,  267 => 122,  261 => 118,  255 => 114,  253 => 113,  243 => 105,  227 => 102,  224 => 101,  221 => 100,  219 => 99,  214 => 97,  210 => 96,  206 => 95,  202 => 94,  198 => 93,  193 => 91,  189 => 90,  184 => 89,  178 => 87,  176 => 86,  173 => 85,  156 => 84,  144 => 74,  138 => 70,  136 => 69,  110 => 45,  108 => 44,  103 => 42,  96 => 38,  88 => 33,  85 => 32,  82 => 31,  76 => 28,  71 => 27,  68 => 26,  54 => 15,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
