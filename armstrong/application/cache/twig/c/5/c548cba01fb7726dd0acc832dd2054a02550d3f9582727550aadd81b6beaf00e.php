<?php

/* ami/master_template/transfer_data.html.twig */
class __TwigTemplate_c548cba01fb7726dd0acc832dd2054a02550d3f9582727550aadd81b6beaf00e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/master_template/transfer_data.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script>
        function checkTargetSalesperson() {
            var currentVal = \$('select[name=\"armstrong_2_salespersons_id\"]').val();
            var targetVal = \$('select[name=\"armstrong_2_salespersons_id_new\"]').val();
            if (!targetVal && currentVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Target Salesperson]'
                });
                return false;
            } else if (!currentVal && targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Current Salesperson]'
                });
                return false;
            } else if (!currentVal && !targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: \"Please chose [Target Salesperson] <br />and [Current Salesperson]\"
                });
                return false;
            } else if (currentVal == targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: \"[Current Salesperson] is the same <br />with [Target Salesperson] <br />Can't transfer\"
                });
                return false;
            }
            return true;
        }

        function getDatas() {
            var targetElement = \$('.datas_assigned'),
                    inputCustomers = \$('input[name=\"datas\"]'),
                    items = targetElement.find('li div[data-id]'),
                    array_val = [];
            \$.each(items, function () {
                var \$val = \$(this).data('id');
                array_val.push(\$val);
            });
            inputCustomers.val(array_val);
        }

        function checkSelectAllCheckBox(selector) {
            var itemVisible = selector.find('ul li.fillable:visible'),
                    itemSelected = selector.find('ul .ui-selected:visible'),
                    selectAllItem = selector.find('ul li.selectall');

            if (itemVisible.length > 0) {
                selectAllItem.removeClass('hidden');
            } else {
                selectAllItem.addClass('hidden');
            }
            if (itemSelected.length == itemVisible.length) {
                selectAllItem.find('.checkAll').prop('checked', true);
            } else {
                selectAllItem.find('.checkAll').prop('checked', false);
            }
        }

        \$(document).ready(function () {
            \$('.datas_block').perfectScrollbar();
            \$('#armstrong_2_salespersons_id').on('change', function () {
                var \$val = \$(this).val(),
                        datas_block_wrapper = \$('.datas_unassigned');
                datas_block_wrapper.perfectScrollbar('destroy');
                \$.ajax({
                    url: \"";
        // line 73
        echo twig_escape_filter($this->env, site_url("ami/master_template/getMasterTemplateBySalesperson"), "html", null, true);
        echo "\",
                    data: {armstrong_2_salespersons_id: \$val},
                    type: 'POST',
                    dataType: 'html',
                    success: function (res) {
                        datas_block_wrapper.html(res);
                        var input_datas = \$('input[name=\"datas\"]').val();
                        if(input_datas){
                            var \$datas_array = input_datas.split(',');
                            \$.each(datas_block_wrapper.find('li:not(.selectall) div'), function () {
                                var data_id = \$(this).data('id').toString();
                                if (\$.inArray(data_id, \$datas_array) != -1) {
                                    \$(this).closest('li').remove();
                                }
                            });
                        }
                        checkSelectAllCheckBox(datas_block_wrapper);
                        datas_block_wrapper.perfectScrollbar();
                    }
                });
            });
            \$('.transfer-right').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = \$('.datas_unassigned ul li.ui-selected'),
                            selectedElementClone = selectedElement.clone(),
                            targetBlock = \$('.datas_assigned > ul');
                    selectedElement.remove();
                    selectedElementClone.appendTo(targetBlock);
                    checkSelectAllCheckBox(\$('.datas_unassigned'));
                    checkSelectAllCheckBox(\$('.datas_assigned'));
                    getDatas();
                }
            });
            \$('.transfer-left').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = \$('.datas_assigned ul li.ui-selected'),
                            selectedElementClone = selectedElement.clone(),
                            targetBlock = \$('.datas_unassigned > ul');
                    selectedElement.remove();
                    selectedElementClone.appendTo(targetBlock);
                    checkSelectAllCheckBox(\$('.datas_unassigned'));
                    checkSelectAllCheckBox(\$('.datas_assigned'));
                    getDatas();
                }
            });

            \$(document).on('click', 'input.checkAll', function () {
                var checkbox = \$(this).closest('.datas_block').find('input.check-box:visible');
                if (\$(this).is(':checked')) {
                    checkbox.prop('checked', true);
                    checkbox.closest('li').addClass('ui-selected');
                } else {
                    checkbox.prop('checked', false);
                    checkbox.closest('li').removeClass('ui-selected');
                }
            });
            \$(document).on('click', 'input.check-box', function () {
                var currentBlock = \$(this).closest('.datas_block'),
                        checkAll = currentBlock.find('input.checkAll'),
                        flagCheck = true;
                if (\$(this).is(':checked')) {
                    \$(this).closest('li').addClass('ui-selected');
                    currentBlock.find('input.check-box:visible').each(function (k, e) {
                        var checked = \$(this).prop('checked');
                        if (checked == false) {
                            flagCheck = false;
                            return false;
                        }
                    });
                    if (flagCheck) {
                        checkAll.prop('checked', true);
                    } else {
                        checkAll.prop('checked', false);
                    }
                } else {
                    \$(this).closest('li').removeClass('ui-selected');
                    checkAll.prop('checked', false);
                }
            });

            // Filter
            \$('.filter').on('input', function () {
                var \$val = \$(this).val(),
                        idAttr = \$(this).attr('id'),
                        wrapClass = (idAttr == 'unAssignedFilter') ? '.datas_unassigned' : '.datas_assigned',
                        options = \$(wrapClass + ' ul li.fillable');
                if (\$val == '') {
                    \$(wrapClass + ' ul li.fillable').removeClass('hidden');
                } else {
                    options.each(function () {
                        var str = \$(this).find('div').text(),
                                regex = new RegExp(\$val, 'gi'),
                                res = str.match(regex);
                        if (res) {
                            \$(this).removeClass('hidden');
                        } else {
                            \$(this).addClass('hidden');
                        }
                    });
                }
                checkSelectAllCheckBox(\$('.datas_unassigned'));
                checkSelectAllCheckBox(\$('.datas_assigned'));
            });
            \$('button[type=\"submit\"]').on('click', function (e) {
                e.preventDefault();
                var input_datas = \$('input[name=\"datas\"]').val();
                if (!input_datas) {
                    \$.alert({
                        title: 'Alert!',
                        content: \"Nothing to transfer\"
                    });
                    return false;
                }
                \$.confirm({
                    title: 'Confirm!',
                    content: 'All current Master Template of [Current Salesperson] will be shift to [Target Salesperson]. <br />Are you sure?',
                    confirm: function () {
                        \$('button[type=\"submit\"]').attr('disabled', 'disabled');
                        \$('#myform').submit();
                    },
                    cancel: function () {

                    }
                });
            });
        });
    </script>
";
    }

    // line 202
    public function block_css($context, array $blocks = array())
    {
        // line 203
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .datas_block {
            height: 300px !important;
            overflow: hidden;
            position: relative;
            border: 1px solid #ddd;
            border-radius: 10px;
            padding: 10px;
        }

        .datas_block ul {
            list-style: none;
            padding: 0;
        }

        .datas_block ul li {
            cursor: pointer;
        }

        .datas_block ul li.selected {
            background-color: #337ab7;
            color: #fff;
        }

        .btn_transfer {
            margin-top: 100px;
        }
    </style>
";
    }

    // line 234
    public function block_content($context, array $blocks = array())
    {
        // line 235
        echo "    ";
        echo form_open(site_url("ami/master_template/transfer_do"), array("class" => "", "id" => "myform", "data-parsley-validate" => "false"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">

            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Transfer of Master Template</h1>

                <div class=\"text-right\">
                    ";
        // line 244
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/master_template/transfer_data.html.twig", 244)->display(array_merge($context, array("url" => "ami/master_template", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "permission" => "customer")));
        // line 245
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"left-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 255
        echo form_label("Current Salesperson", "armstrong_2_salespersons_id", array("class" => ""));
        echo "
                    ";
        // line 256
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "id=\"armstrong_2_salespersons_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    <input type=\"text\" id=\"unAssignedFilter\" class=\"form-control filter\"
                           placeholder=\"Input text to search\">
                </div>
                <div class=\"form-group\">
                    ";
        // line 263
        echo form_label("Unassigned", "datas_unassigned", array("class" => ""));
        echo "
                    <div class=\"datas_unassigned datas_block\">
                        ";
        // line 266
        echo "                        ";
        // line 267
        echo "                        ";
        // line 268
        echo "                        ";
        // line 269
        echo "                        ";
        // line 270
        echo "                        ";
        // line 271
        echo "                        ";
        // line 272
        echo "                        ";
        // line 273
        echo "                        ";
        // line 274
        echo "                        ";
        // line 275
        echo "                        ";
        // line 276
        echo "                        ";
        // line 277
        echo "                        ";
        // line 278
        echo "                        ";
        // line 279
        echo "                        ";
        // line 280
        echo "                        ";
        // line 281
        echo "                        ";
        // line 282
        echo "                        ";
        // line 283
        echo "                    </div>
                </div>
            </div>
            <div class=\"btn_transfer col-sm-2\">
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-right\">
                    <i class=\"fa fa-long-arrow-right\"></i>
                    Left to Right
                </a>
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-left\">
                    <i class=\"fa fa-long-arrow-left\"></i>
                    Right to Left
                </a>
            </div>
            <div class=\"right-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 298
        echo form_label("Target Salesperson", "armstrong_2_salespersons_id_new", array("class" => ""));
        echo "
                    ";
        // line 299
        echo form_dropdown("armstrong_2_salespersons_id_new", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id_new", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    <input type=\"text\" name=\"\" id=\"assignedFilter\" class=\"form-control filter\"
                           placeholder=\"Input text to search\">
                </div>
                <div class=\"form-group\">
                    ";
        // line 306
        echo form_label("Assigned", "datas_assigned", array("class" => ""));
        echo "
                    <div class=\"datas_assigned datas_block\">
                        <ul>
                            <li class=\"selectall hidden\">
                                <div>
                                    <input name=\"checkall\" class=\"checkAll\" type=\"checkbox\"> Select All
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type=\"hidden\" name=\"datas\">
    ";
        // line 321
        echo form_close();
        echo "
    <link href=\"";
        // line 322
        echo twig_escape_filter($this->env, site_url("res/css/plugins/perfect-scrollbar/perfect-scrollbar.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <link href=\"";
        // line 324
        echo twig_escape_filter($this->env, site_url("res/css/jquery-confirm.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <link href=\"";
        // line 326
        echo twig_escape_filter($this->env, site_url("res/css/multiple-select.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <script src=\"";
        // line 328
        echo twig_escape_filter($this->env, site_url("res/js/jquery-confirm.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 329
        echo twig_escape_filter($this->env, site_url("res/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 331
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/jquery.multiple.select.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "ami/master_template/transfer_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 331,  431 => 329,  427 => 328,  422 => 326,  417 => 324,  412 => 322,  408 => 321,  390 => 306,  380 => 299,  376 => 298,  359 => 283,  357 => 282,  355 => 281,  353 => 280,  351 => 279,  349 => 278,  347 => 277,  345 => 276,  343 => 275,  341 => 274,  339 => 273,  337 => 272,  335 => 271,  333 => 270,  331 => 269,  329 => 268,  327 => 267,  325 => 266,  320 => 263,  310 => 256,  306 => 255,  294 => 245,  292 => 244,  279 => 235,  276 => 234,  241 => 203,  238 => 202,  106 => 73,  33 => 4,  30 => 3,  11 => 1,);
    }
}
