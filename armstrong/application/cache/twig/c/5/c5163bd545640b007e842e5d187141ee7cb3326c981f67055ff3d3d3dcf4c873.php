<?php

/* ami/kpi_target/index1.html.twig */
class __TwigTemplate_c5163bd545640b007e842e5d187141ee7cb3326c981f67055ff3d3d3dcf4c873 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/kpi_target/index1.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/kpi_target/ajaxData"), "html", null, true);
        echo "\",*/
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
});
</script>
";
    }

    // line 28
    public function block_css($context, array $blocks = array())
    {
        // line 29
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 33
    public function block_content($context, array $blocks = array())
    {
        // line 34
        echo "
    ";
        // line 35
        echo form_open(site_url("ami/kpi_target/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 40
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("KPI Settings Draft") : ("KPI Settings"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 43
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 45
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/kpi_target/index1.html.twig", 45)->display(array_merge($context, array("url" => "ami/kpi_target", "icon" => "fa-line-chart", "title" => "KPI Settings", "permission" => "kpi_target")));
        // line 46
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            <div class=\"panel panel-default\">                
                
                <div class=\"panel-body\">                
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 62
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target"))) {
            // line 63
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 65
        echo "                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 73
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 74
            echo "                                    <tr>
                                        ";
            // line 75
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target"))) {
                // line 76
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 78
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/kpi_target/edit/" . $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 83
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "kpi_target")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target")))) {
                // line 84
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/kpi_target/index1.html.twig", 84)->display(array_merge($context, array("url" => "ami/kpi_target", "id" => $this->getAttribute($context["post"], "id", array()), "permission" => "kpi_target")));
                // line 85
                echo "                                            ";
            }
            // line 86
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 93
        $context["paginationUrl"] = (((isset($context["draft"]) ? $context["draft"] : null)) ? ("ami/kpi_target/draft") : ("ami/kpi_target"));
        // line 94
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => site_url((isset($context["paginationUrl"]) ? $context["paginationUrl"] : null)), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 108
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/kpi_target/index1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 108,  227 => 94,  225 => 93,  219 => 89,  203 => 86,  200 => 85,  197 => 84,  195 => 83,  190 => 81,  186 => 80,  180 => 79,  171 => 78,  165 => 76,  163 => 75,  160 => 74,  143 => 73,  133 => 65,  129 => 63,  127 => 62,  109 => 46,  107 => 45,  102 => 43,  96 => 40,  88 => 35,  85 => 34,  82 => 33,  76 => 30,  72 => 29,  69 => 28,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
