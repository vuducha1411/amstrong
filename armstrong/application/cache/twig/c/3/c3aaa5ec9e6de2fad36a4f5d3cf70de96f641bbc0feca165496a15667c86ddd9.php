<?php

/* ami/contacts/preview.html.twig */
class __TwigTemplate_c3aaa5ec9e6de2fad36a4f5d3cf70de96f641bbc0feca165496a15667c86ddd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <ul class=\"list-unstyled\">
                    <li class=\"text-center\">
                        ";
        // line 11
        $context["avatar"] = (($this->getAttribute((isset($context["images"]) ? $context["images"] : null), 0, array())) ? (((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/image/thumb") . "/") . $this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), 0, array()), "path", array()))) : (site_url("res/img/m-av_s.png")));
        // line 12
        echo "                        <img class=\"img-bordered-danger\" src=\"";
        echo twig_escape_filter($this->env, (isset($context["avatar"]) ? $context["avatar"] : null), "html", null, true);
        echo "\" alt=\"\" width=\"100px\" height=\"100px\">
                        <br>
                        <h5 class=\"semibold mb0\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "name", array()), "html", null, true);
        echo "</h5>
                        <p class=\"nm text-muted\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "position", array()), "html", null, true);
        echo "</p>
                    </li>
                </ul>
                <div class=\"list-group\">
                    ";
        // line 19
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array())) {
            // line 20
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Email</h4>
                                <p class=\"list-group-item-text\"><a href=\"mailto:";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array()), "html", null, true);
            echo "</a>  ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email_two", array())) ? (((" - <a href=\"mailto:{{ contacts.email_two }}\">" . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email_two", array())) . "</a>")) : ("")), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 28
        echo "
                    ";
        // line 29
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone", array())) {
            // line 30
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-phone pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Phone</h4>
                                <p class=\"list-group-item-text\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_2", array())) ? ((" - " . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_2", array()))) : ("")), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 38
        echo "
                    ";
        // line 39
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "fax", array())) {
            // line 40
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-fax pull-left\"></i>
                            <div class=\"m-l-lg\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Fax</h4>
                                <p class=\"list-group-item-text\">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "fax", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 48
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 53
        echo html_btn(site_url(("ami/contacts/edit/" . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
        echo "
        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/contacts/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 53,  110 => 48,  103 => 44,  97 => 40,  95 => 39,  92 => 38,  83 => 34,  77 => 30,  75 => 29,  72 => 28,  61 => 24,  55 => 20,  53 => 19,  46 => 15,  42 => 14,  36 => 12,  34 => 11,  25 => 5,  19 => 1,);
    }
}
