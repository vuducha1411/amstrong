<?php

/* ami/reports/custom/report_call.twig.html */
class __TwigTemplate_c21d62b9db57fd7cf20ac996c41a804f90f27eea2521fd74833d0ed6577dfb25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("ami/reports/custom/filter.twig.html", "ami/reports/custom/report_call.twig.html", 1)->display(array_merge($context, array("channel" => 1, "otm" => 1)));
        // line 5
        echo "
";
        // line 12
        echo "
<div class=\"form-group\">
\t";
        // line 14
        echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-3"));
        echo "
\t<div class=\"col-sm-6\">
\t\t<select name=\"sales_leader[]\" class=\"form-control\" id=\"SalesLeaderSelect\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t<option value=\"ALL\" selected=\"selected\">ALL</option>
\t\t\t";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
            // line 19
            echo "\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-sub_type=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "sub_type", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
            echo "</option>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "\t\t</select>
\t</div>
</div>

<div class=\"form-group\">
\t";
        // line 26
        echo form_label("Armstrong 2 salespersons id", "salesperson", array("class" => "control-label col-sm-3"));
        echo "
\t<div class=\"col-sm-6\">
\t\t<select name=\"armstrong_2_salespersons_id[]\" class=\"form-control SalespersonsSelect\" id=\"armstrong_2_salespersons_id\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t<option value=\"\" selected=\"selected\" data-manager=\"\">ALL</option>
\t\t\t";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
            // line 31
            echo "\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-manager=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
            echo "</option>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "\t\t</select>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/report_call.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 33,  72 => 31,  68 => 30,  61 => 26,  54 => 21,  39 => 19,  35 => 18,  28 => 14,  24 => 12,  21 => 5,  19 => 1,);
    }
}
