<?php

/* ami/products/edit.html.twig */
class __TwigTemplate_c6bc831a0820849b7c429206e13b835f16799df645f8c72e491261efbb00a3db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/products/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            createUploader(\$('#imageUpload'));
            createUploader(\$('#videoUpload'));
            createUploader(\$('#brochureUpload'));
            createUploader(\$('#specUpload'));
            createUploader(\$('#certUpload'));
        });

        CKEDITOR.replace('inputContent', {
            customConfig: '";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.replace('inputDescription', {
            customConfig: '";
        // line 24
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.replace('inputNutrition', {
            customConfig: '";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.on('instanceReady', function () {
            \$.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on(\"change\", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                });
            });
        });


        \$('button[type=\"submit\"]').on('click', function (e) {
            var sku_number = \$('input[name=\"sku_number\"]').val(),
                    sku_name = \$('input[name=\"sku_name\"]').val(),
                    price = \$('input[name=\"price\"]').val(),
                    quantity_case = \$('input[name=\"quantity_case\"]').val(),
                    weight_pc = \$('input[name=\"weight_pc\"]').val(),
                    product_id = '";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sku_number", array()), "html", null, true);
        echo "';
            if (\$.inArray('', [sku_number, sku_name, price, quantity_case, weight_pc]) == -1) {
                if ((product_id && (sku_number !== product_id)) || product_id == '') {
                    e.preventDefault();
                    \$.ajax({
                        type: 'POST',
                        url: base_url() + 'ami/products/checkExistSkuNumber',
                        data: {sku_number: sku_number},
                        success: function (res) {
                            res = \$.parseJSON(res);
                            var input_sku = \$('input[name=\"sku_number\"]'),
                                    parsley_id = input_sku.attr('data-parsley-id'),
                                    error_wrap = \$('ul.parsley-errors-list[id=\"parsley-id-' + parsley_id + '\"]');
                            if (res['error'] == 1) {
                                input_sku.removeClass('parsley-success');
                                input_sku.addClass('parsley-error');
                                if (\$('ul.filled.parsley-errors-list[id=\"parsley-id-' + parsley_id + '\"]').length == 0) {
                                    error_wrap.addClass('filled').append('<li class=\"parsley-required\">' + res.message + '</li>');
                                }

                            } else if (res['error'] == 0) {
                                input_sku.removeClass('parsley-error');
                                input_sku.addClass('parsley-success');
                                error_wrap.removeClass('filled').empty();
                                \$('form')[0].submit();
                            }
                        }
                    })
                }
            }
        });
    </script>
";
    }

    // line 82
    public function block_content($context, array $blocks = array())
    {
        // line 83
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 98
        echo form_open(site_url("ami/products/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Products</h1>

                <div class=\"text-right\">
                    ";
        // line 106
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/products/edit.html.twig", 106)->display(array_merge($context, array("url" => "ami/products", "id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id", array()), "permission" => "product")));
        // line 107
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 116
        echo form_label("SKU Number", "sku_number", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 118
        echo form_input(array("name" => "sku_number", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sku_number", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 123
        echo form_label("SKU Name", "sku_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 125
        echo form_input(array("name" => "sku_name", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "sku_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 130
        echo form_label("SKU Name (Other Language)", "name_alt", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 132
        echo form_input(array("name" => "name_alt", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name_alt", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 137
        echo form_label("Description", "description", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 139
        echo form_textarea(array("name" => "description", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "class" => "form-control input-xxlarge", "id" => "inputDescription"));
        // line 141
        echo "
                </div>
            </div>

            ";
        // line 145
        if ((twig_length_filter($this->env, (isset($context["groups"]) ? $context["groups"] : null)) > 1)) {
            // line 146
            echo "                <div class=\"form-group\">
                    ";
            // line 147
            echo form_label("Group", "groups_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 149
            echo form_dropdown("groups_id", (isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "groups_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 153
        echo "
            <div class=\"form-group\">
                ";
        // line 155
        echo form_label("Brand", "brands_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 157
        echo form_dropdown("brands_id", (isset($context["brands"]) ? $context["brands"] : null), $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "brands_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 161
        echo form_label("App Type", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 163
        echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "app_type", array())) ? ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "app_type", array())) : (0)), "class=\"form-control\"");
        echo "
                </div>
            </div>
            ";
        // line 166
        if ((twig_length_filter($this->env, (isset($context["products_cat_web"]) ? $context["products_cat_web"] : null)) > 1)) {
            // line 167
            echo "                <div class=\"form-group\">
                    ";
            // line 168
            echo form_label("Products Cat Web", "products_cat_web_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 170
            echo form_dropdown("products_cat_web_id", (isset($context["products_cat_web"]) ? $context["products_cat_web"] : null), $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "products_cat_web_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 174
        echo "
            <div class=\"form-group\">
                ";
        // line 176
        echo form_label("Ingredients", "ingredients", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 178
        echo form_textarea(array("name" => "ingredients", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "ingredients", array()), "class" => "form-control input-xxlarge", "id" => "inputContent", "data-parsley" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 183
        echo form_label("Barcode/Origin", "barcode", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 185
        echo form_input(array("name" => "barcode", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "barcode", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 190
        echo form_label("Price", "price", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 192
        echo form_input(array("name" => "price", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            ";
        // line 196
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 12, 1 => 13, 2 => 14, 3 => 15, 4 => 16, 5 => 17))) {
            // line 197
            echo "                <div class=\"form-group\">
                    ";
            // line 198
            echo form_label("Gross Margin", "gross_margin", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 200
            echo form_input(array("name" => "gross_margin", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "gross_margin", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "number"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 204
        echo "
            <div class=\"form-group\">
                ";
        // line 206
        echo form_label("Quantity in Case", "quantity_case", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 208
        echo form_input(array("name" => "quantity_case", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quantity_case", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "integer"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 213
        echo form_label("Weight per Pc", "weight_pc", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 215
        echo form_input(array("name" => "weight_pc", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "weight_pc", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            ";
        // line 225
        echo "
            ";
        // line 234
        echo "
            <div class=\"form-group\">
                ";
        // line 236
        echo form_label("Categories", "categories_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 238
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["categories"]) ? $context["categories"] : null), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "glob_pro_hierarchy_array", array()), 0, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 243
        echo form_label("Markets", "markets_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 245
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["markets"]) ? $context["markets"] : null), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "glob_pro_hierarchy_array", array()), 1, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 250
        echo form_label("Sectors", "sectors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 252
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["sectors"]) ? $context["sectors"] : null), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "glob_pro_hierarchy_array", array()), 2, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 257
        echo form_label("Sub Sectors", "sub_sectors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 259
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["sub_sectors"]) ? $context["sub_sectors"] : null), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "glob_pro_hierarchy_array", array()), 3, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 264
        echo form_label("Segment", "segments_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 266
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["segments"]) ? $context["segments"] : null), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "glob_pro_hierarchy_array", array()), 4, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 271
        echo form_label("Shelf Life", "shelf_life", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 273
        echo form_input(array("name" => "shelf_life", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "shelf_life", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            ";
        // line 276
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 4))) {
            // line 277
            echo "                <div class=\"form-group\">
                    ";
            // line 278
            echo form_label("Pallet Configuration", "pallet_configuration", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 280
            echo form_input(array("name" => "pallet_configuration", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pallet_configuration", array()), "class" => "form-control"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 284
        echo "            <div class=\"form-group\">
                ";
        // line 285
        echo form_label("Product Claims", "pro_claims", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 287
        echo form_input(array("name" => "pro_claims", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pro_claims", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 292
        echo form_label("Is Top Ten", "is_top_ten", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <div class=\"checkbox\">
                        <label class=\"\">";
        // line 295
        echo form_checkbox("is_top_ten", 1, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "is_top_ten", array()));
        echo "</label>
                    </div>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 301
        echo form_label("Listing", "listing_availability", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <div class=\"checkbox\">
                        <label class=\"\">";
        // line 304
        echo form_checkbox("listing_availability", 1, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "listing_availability", array()));
        echo "</label>
                    </div>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 310
        echo form_label("Main SKU", "main_sku", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 312
        echo form_dropdown("main_sku", (isset($context["main_sku"]) ? $context["main_sku"] : null), $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "main_sku", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            ";
        // line 316
        if ((twig_length_filter($this->env, (isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null)) > 1)) {
            // line 317
            echo "                <div class=\"form-group\">
                    ";
            // line 318
            echo form_label("Cuisine Channels", "cuisine_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 320
            echo form_dropdown("cuisine_channels_id", (isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null), $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "cuisine_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 324
        echo "
            ";
        // line 338
        echo "
            <div class=\"form-group\">
                ";
        // line 340
        echo form_label("Nutrition Name", "nutrition_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 342
        echo form_textarea(array("name" => "nutrition_name", "value" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "nutrition_name", array()), "class" => "form-control input-xxlarge", "id" => "inputNutrition"));
        echo "
                </div>
            </div>

            ";
        // line 354
        echo "
            <div class=\"form-group\">
                ";
        // line 356
        echo form_label("", "listed", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 358
        echo form_radio("listed", 0, ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "listed", array()) == 0));
        echo " Delisted</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 359
        echo form_radio("listed", 1, ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "listed", array()) == 1));
        echo " Listed</label>&nbsp;
                </div>
            </div>

            ";
        // line 363
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/products/edit.html.twig", 363)->display(array_merge($context, array("entry_type" => "products")));
        // line 364
        echo "
            ";
        // line 365
        $this->loadTemplate("ami/components/uploader_video.html.twig", "ami/products/edit.html.twig", 365)->display(array_merge($context, array("entry_type" => "products")));
        // line 366
        echo "
            ";
        // line 367
        $this->loadTemplate("ami/components/uploader_brochure.html.twig", "ami/products/edit.html.twig", 367)->display(array_merge($context, array("entry_type" => "products")));
        // line 368
        echo "
            ";
        // line 369
        $this->loadTemplate("ami/components/uploader_spec.html.twig", "ami/products/edit.html.twig", 369)->display(array_merge($context, array("entry_type" => "products")));
        // line 370
        echo "
            ";
        // line 371
        $this->loadTemplate("ami/components/uploader_cert.html.twig", "ami/products/edit.html.twig", 371)->display(array_merge($context, array("entry_type" => "products")));
        // line 372
        echo "
            ";
        // line 373
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 377
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/products/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  629 => 377,  622 => 373,  619 => 372,  617 => 371,  614 => 370,  612 => 369,  609 => 368,  607 => 367,  604 => 366,  602 => 365,  599 => 364,  597 => 363,  590 => 359,  586 => 358,  581 => 356,  577 => 354,  570 => 342,  565 => 340,  561 => 338,  558 => 324,  551 => 320,  546 => 318,  543 => 317,  541 => 316,  534 => 312,  529 => 310,  520 => 304,  514 => 301,  505 => 295,  499 => 292,  491 => 287,  486 => 285,  483 => 284,  476 => 280,  471 => 278,  468 => 277,  466 => 276,  460 => 273,  455 => 271,  447 => 266,  442 => 264,  434 => 259,  429 => 257,  421 => 252,  416 => 250,  408 => 245,  403 => 243,  395 => 238,  390 => 236,  386 => 234,  383 => 225,  376 => 215,  371 => 213,  363 => 208,  358 => 206,  354 => 204,  347 => 200,  342 => 198,  339 => 197,  337 => 196,  330 => 192,  325 => 190,  317 => 185,  312 => 183,  304 => 178,  299 => 176,  295 => 174,  288 => 170,  283 => 168,  280 => 167,  278 => 166,  272 => 163,  267 => 161,  260 => 157,  255 => 155,  251 => 153,  244 => 149,  239 => 147,  236 => 146,  234 => 145,  228 => 141,  226 => 139,  221 => 137,  213 => 132,  208 => 130,  200 => 125,  195 => 123,  187 => 118,  182 => 116,  171 => 107,  169 => 106,  158 => 98,  141 => 83,  138 => 82,  101 => 48,  78 => 28,  71 => 24,  64 => 20,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
