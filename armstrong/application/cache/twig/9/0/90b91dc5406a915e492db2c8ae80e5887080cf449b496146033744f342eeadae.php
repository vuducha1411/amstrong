<?php

/* ami/sales_cycle/index.html.twig */
class __TwigTemplate_90b91dc5406a915e492db2c8ae80e5887080cf449b496146033744f342eeadae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/sales_cycle/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/sales_cycle/ajaxData"), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"id\", \"sWidth\": \"10%\"},
                    {mData: \"from_date\"},
                    {mData: \"to_date\"}
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    ";
        // line 25
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sales_cycle"))) {
            // line 26
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    ";
        }
        // line 28
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 120px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });
        });
    </script>
";
    }

    // line 44
    public function block_css($context, array $blocks = array())
    {
        // line 45
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 49
    public function block_content($context, array $blocks = array())
    {
        // line 50
        echo "
    ";
        // line 51
        echo form_open(site_url("ami/sales_cycle/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

       <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Sales Cycle</h1>

                <div class=\"text-right\">
\t\t\t\t\t<span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
\t\t\t\t\t\t";
        // line 60
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
\t\t\t\t\t</span>
                    ";
        // line 62
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/sales_cycle/index.html.twig", 62)->display(array_merge($context, array("url" => "ami/sales_cycle", "icon" => "fa-user", "title" => "Sales cycle", "permission" => "sales_cycle", "showImport" => 0)));
        // line 63
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 80
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sales_cycle"))) {
            // line 81
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 85
        echo "                                <th>ID</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 103
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/sales_cycle/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 103,  163 => 85,  157 => 81,  155 => 80,  136 => 63,  134 => 62,  129 => 60,  117 => 51,  114 => 50,  111 => 49,  105 => 46,  100 => 45,  97 => 44,  79 => 28,  75 => 26,  73 => 25,  61 => 16,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
