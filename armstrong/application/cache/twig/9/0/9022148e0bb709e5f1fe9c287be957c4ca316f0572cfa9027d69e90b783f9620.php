<?php

/* ami/classification/edit.html.twig */
class __TwigTemplate_9022148e0bb709e5f1fe9c287be957c4ca316f0572cfa9027d69e90b783f9620 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/classification/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });
        });
        var maxcustomer = 999;
        function addCustomer(ui)
        {
            maxcustomer++;
            var html = '<tr class=\"c_'+maxcustomer+'\">';
            html += '<td width=\"20%\">';
            html += ui.value;
            html += '<input type=\"hidden\" name=\"customer[]\" value=\"'+ui.value+'\" >';
            html += '</td>';
            html += '<td>';
            html += ui.label;
            html += '</td>';
            html += '<td>';
            html += '<span class=\"btn btn-danger removepointst\"  data-value=\"'+ui.value+'\" data-nbconfig=\"'+maxcustomer+'\">Remove</span>';
            html += '</td>';
            html += '</tr>';
            \$('#customerlist').after(html);
        }
       // var except = '';
        \$('#customer_search').autocomplete({
            source: function(request, response) {
                \$.ajax({
                    url: \"";
        // line 48
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield"), "html", null, true);
        echo "\",
                    dataType: \"json\",
                    data: {
                        term : request.term,
                        except : except,
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            delay: 100,
            select: function (event, ui) {
                event.preventDefault();
                addCustomer(ui.item);
                except += ',\"'+ui.item.value+'\"';
                \$('#customer_search').val('');
            }
        }).on('focus', function () {
            this.select();
        });

        \$(document).on('click', '.removepointst', function () {
           except = except.replace(',\"'+\$(this).data('value')+'\"', '');
           \$('.c_'+\$(this).data('nbconfig')).remove();
        });
    </script>
";
    }

    // line 77
    public function block_css($context, array $blocks = array())
    {
        // line 78
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 82
    public function block_content($context, array $blocks = array())
    {
        // line 83
        echo "    ";
        echo form_open(site_url("ami/classification/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Classification</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 92
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 94
        echo html_btn(site_url("ami/classification"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 95
        if ($this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array())) {
            // line 96
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/classification/delete/" . $this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 99
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 109
        echo form_label("From", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 111
        echo form_input(array("name" => "from_date", "value" => (($this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "from_date", array())) ? ($this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "from_date", array())) : (twig_date_format_filter($this->env, "now", "Y-m-d H:i:s"))), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 115
        echo form_label("To", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 117
        echo form_input(array("name" => "to_date", "value" => (($this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "to_date", array())) ? ($this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "to_date", array())) : ((isset($context["date_end_month"]) ? $context["date_end_month"] : null))), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 118
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 119
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 120
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 123
        echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 126
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 128
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "name", array()), "id" => "name", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control") : ("form-control")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 129
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 130
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 131
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 134
        echo "                </div>
            </div>

            <table class=\"table table-striped\">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody >
                <tr id=\"customerlist\">
                    <td width=\"20%\">
                        ";
        // line 148
        echo form_label("Customer", "customer_search", array("class" => "control-label"));
        echo "</td>
                    <td>
                        <div class=\"form-group\">
                            <div class=\"\">
                                ";
        // line 152
        echo form_input(array("name" => "customer_search", "value" => "", "id" => "customer_search", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control") : ("form-control"))));
        echo "

                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <script type=\"text/javascript\">var except = '';</script>
                ";
        // line 160
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "customer", array()));
        foreach ($context['_seq'] as $context["key"] => $context["customer"]) {
            // line 161
            echo "                    <tr class=\"c_";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
                        <td width=\"20%\">
                            ";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_id", array()), "html", null, true);
            echo "
                            <script type=\"text/javascript\"> except += ',\"";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_id", array()), "html", null, true);
            echo "\"';</script>
                            <input type=\"hidden\" name=\"customer[]\" value=\"";
            // line 165
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_id", array()), "html", null, true);
            echo "\" >
                        </td>
                        <td>
                            ";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_name", array()), "html", null, true);
            echo "
                        </td>
                        <td>
                            <span class=\"btn btn-danger removepointst\" data-value=\"";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($context["customer"], "armstrong_2_customers_id", array()), "html", null, true);
            echo "\"  data-nbconfig=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">Remove</span>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['customer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 175
        echo "                </tbody>
            </table>
            ";
        // line 177
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["classification"]) ? $context["classification"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 182
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/classification/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 182,  327 => 177,  323 => 175,  311 => 171,  303 => 168,  297 => 165,  293 => 164,  289 => 163,  283 => 161,  279 => 160,  268 => 152,  261 => 148,  245 => 134,  239 => 131,  236 => 130,  234 => 129,  230 => 128,  225 => 126,  220 => 123,  214 => 120,  211 => 119,  209 => 118,  205 => 117,  200 => 115,  193 => 111,  188 => 109,  176 => 99,  169 => 96,  167 => 95,  163 => 94,  158 => 92,  145 => 83,  142 => 82,  136 => 79,  131 => 78,  128 => 77,  96 => 48,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
