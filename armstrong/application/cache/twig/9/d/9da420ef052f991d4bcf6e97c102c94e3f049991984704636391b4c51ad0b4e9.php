<?php

/* ami/tfo/preview.html.twig */
class __TwigTemplate_9da420ef052f991d4bcf6e97c102c94e3f049991984704636391b4c51ad0b4e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_tfo_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    ";
        // line 11
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "customer", array())) {
            // line 12
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-group pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Customer</h4>
                                <p class=\"list-group-item-text\">";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "customer", array()), "armstrong_2_customers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "customer", array()), "armstrong_2_customers_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 20
        echo "
                    ";
        // line 21
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "salesperson", array())) {
            // line 22
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-male pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Salesperson</h4>
                                <p class=\"list-group-item-text\">";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "salesperson", array()), "armstrong_2_salespersons_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "salesperson", array()), "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "salesperson", array()), "last_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 30
        echo "
                    ";
        // line 31
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "wholesaler", array())) {
            // line 32
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-shopping-cart pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Wholesaler</h4>
                                <p class=\"list-group-item-text\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "wholesaler", array()), "armstrong_2_wholesalers_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "wholesaler", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 40
        echo "
                    ";
        // line 41
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "distributor", array())) {
            // line 42
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-bus pull-left\"></i>
                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Distributor</h4>
                                <p class=\"list-group-item-text\">";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "distributor", array()), "armstrong_2_distributors_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "distributor", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 50
        echo "
                    ";
        // line 51
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "call_record", array())) {
            // line 52
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-file-text-o pull-left\"></i>
                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Call record</h4>
                                <p class=\"list-group-item-text\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "call_record", array()), "armstrong_2_call_records_id", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 60
        echo "
                    ";
        // line 61
        if ($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "products_array", array())) {
            // line 62
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-shopping-cart pull-left\"></i>
                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Products</h4>
                                ";
            // line 66
            $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
            // line 67
            echo "                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "products_array", array()));
            foreach ($context['_seq'] as $context["key"] => $context["tfo_product"]) {
                // line 68
                echo "                                  ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "sku_number", array()), "html", null, true);
                echo " -
                                    ";
                // line 69
                echo twig_escape_filter($this->env, get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["tfo_product"], "sku_number", array()), array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null)), "html", null, true);
                echo "
                                    <br>
                                    Quantity Pieces: ";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "qty_piece", array()), "html", null, true);
                echo "
                                    Quantity Case: ";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "qty_case", array()), "html", null, true);
                echo "
                                    Free Pieces: ";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "free_piece", array()), "html", null, true);
                echo "
                                    Free Case: ";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($context["tfo_product"], "free_case", array()), "html", null, true);
                echo "
                                    <br><br>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['tfo_product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "                            </div>
                        </div>
                    ";
        }
        // line 80
        echo "
                </div>

            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 87
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "tfo"))) {
            // line 88
            echo "                ";
            echo html_btn(site_url(("ami/tfo/edit/" . $this->getAttribute((isset($context["tfo"]) ? $context["tfo"] : null), "armstrong_2_tfo_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 90
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/tfo/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 90,  196 => 88,  194 => 87,  185 => 80,  180 => 77,  171 => 74,  167 => 73,  163 => 72,  159 => 71,  154 => 69,  149 => 68,  144 => 67,  142 => 66,  136 => 62,  134 => 61,  131 => 60,  124 => 56,  118 => 52,  116 => 51,  113 => 50,  104 => 46,  98 => 42,  96 => 41,  93 => 40,  84 => 36,  78 => 32,  76 => 31,  73 => 30,  62 => 26,  56 => 22,  54 => 21,  51 => 20,  42 => 16,  36 => 12,  34 => 11,  25 => 5,  19 => 1,);
    }
}
