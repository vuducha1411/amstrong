<?php

/* ami/principles/edit.html.twig */
class __TwigTemplate_920383f05fb574c3114e99a787f87ef7cf0ae6b011306bfd5897a28513b0569c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/principles/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        data_arr = [];
        app_type = '';
        column_type = '';
        function resortingArray() {
            \$(\"ul#sortable li:not(.hide)\").each(function () {
                var id = \$(this).find('input').data('id'),
                        status = \$(this).find('input').data('status'),
                        value = \$(this).find('input').val();
                ";
        // line 16
        if (((isset($context["action"]) ? $context["action"] : null) == "add")) {
            // line 17
            echo "                app_type = \$('select[name=\"app_type\"]').val(),
                        column_type = \$('select[name=\"column_type\"]').val();
                ";
        } else {
            // line 20
            echo "                app_type = '";
            echo twig_escape_filter($this->env, (isset($context["app_type"]) ? $context["app_type"] : null), "html", null, true);
            echo "',
                        column_type = '";
            // line 21
            echo twig_escape_filter($this->env, (isset($context["column_type"]) ? $context["column_type"] : null), "html", null, true);
            echo "';
                ";
        }
        // line 23
        echo "                var obj = {'id': id, 'name': value, 'status': status, 'app_type': app_type, 'column_type': column_type};
                data_arr.push(obj);
            });
        }

        \$(function () {
            \$(\"#sortable\").sortable().disableSelection();

            \$('.add').on('click', function () {
                var clone = \$(\"ul#sortable li.hide\").clone();
                clone.removeClass('hide');
                clone.find('input').attr('data-parsley-required', true);
                clone.appendTo(\$('#sortable'));
            });

            \$(document).on('click', '.remove', function () {
                var \$this = \$(this),
                        parent = \$this.closest('li'),
                        id = parent.find('input').data('id');
                if (!id) {
                    parent.remove();
                } else {
                    parent.css('display', 'none');
                    parent.find('input').attr('data-status', 'delete');
                }
            });

            \$('button[type=\"submit\"]').click(function (e) {
                e.preventDefault();
                data_arr = [];
                resortingArray();
                if(data_arr.length > 0){
                    \$(this).addClass('disabled');
                    \$.ajax({
                        type: 'POST',
                        url: base_url() + 'ami/principles/save',
                        data: {
                            action: '";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "',
                            column_type: column_type,
                            app_type: app_type,
                            data_arr
                        },
                        success: function (re) {
                            window.location.replace(base_url() + re);
                        }
                    });
                }else{
                    alert('Please Add at least 1 principle!');
                }
            });
        });
    </script>
";
    }

    // line 77
    public function block_css($context, array $blocks = array())
    {
        // line 78
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        #sortable li {
            margin: 0 3px 10px 3px;
            padding: 4px;
            padding-left: 1.5em;
            font-size: 1em;
            height: 35px;
            color: #333;
            font-weight: normal;
        }

        #sortable li span {
            position: absolute;
            margin-left: -1.3em;
        }

        #sortable li input {
            width: 95%;
        }

        .ui-icon-arrowthick-2-n-s {
            background-position: -128px -45px;
        }

        .add {
            margin-bottom: 20px;
        }

        .remove {
            padding-top: 2px;
            padding-bottom: 2px;
        }
    </style>
";
    }

    // line 122
    public function block_content($context, array $blocks = array())
    {
        // line 123
        echo "    ";
        echo form_open(site_url("ami/principles/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true", "id" => "form-principles"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Principles</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 132
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 134
        echo "                        ";
        echo html_btn(site_url("ami/principles"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 135
        if ($this->getAttribute((isset($context["principles"]) ? $context["principles"] : null), "id", array())) {
            // line 136
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/principles/delete/" . $this->getAttribute((isset($context["principles"]) ? $context["principles"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 139
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 148
        if (((isset($context["action"]) ? $context["action"] : null) == "add")) {
            // line 149
            echo "                <div class=\"form-group\">
                    ";
            // line 150
            echo form_label("App type", "app_type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 152
            echo form_dropdown("app_type", array(0 => "Pull", 1 => "Push", 2 => "Mix"), null, "class=\"form-control\"");
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 157
            echo form_label("Principle type", "column_type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 159
            echo form_dropdown("column_type", (isset($context["column_type"]) ? $context["column_type"] : null), null, "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 163
        echo "            <a class=\"btn btn-default btn-clone add\"><i class=\"fa fa-plus\"></i></a>
            <ul id=\"sortable\">
                <li class=\"ui-state-default hide\">
                    <span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
                    <a class=\"btn btn-default btn-clone remove pull-right\"><i class=\"fa fa-minus\"></i></a>
                    ";
        // line 168
        echo form_input(array("name" => "", "value" => null, "class" => "", "data-parsley-required" => "false", "data-id" => null, "data-status" => ""));
        echo "
                </li>
                ";
        // line 170
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["principles"]) ? $context["principles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["principle"]) {
            // line 171
            echo "                    <li class=\"ui-state-default\">
                        <span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
                        <a class=\"btn btn-default btn-clone remove pull-right\"><i class=\"fa fa-minus\"></i></a>
                        ";
            // line 174
            echo form_input(array("name" => "", "value" => $this->getAttribute($context["principle"], "name", array()), "class" => "", "data-parsley-required" => "true", "data-id" => $this->getAttribute($context["principle"], "id", array()), "data-status" => ""));
            echo "
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['principle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 177
        echo "            </ul>
            ";
        // line 178
        echo form_input(array("name" => "data[]", "value" => $this->getAttribute((isset($context["principles"]) ? $context["principles"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 182
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/principles/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 182,  293 => 178,  290 => 177,  281 => 174,  276 => 171,  272 => 170,  267 => 168,  260 => 163,  253 => 159,  248 => 157,  240 => 152,  235 => 150,  232 => 149,  230 => 148,  219 => 139,  212 => 136,  210 => 135,  205 => 134,  201 => 132,  188 => 123,  185 => 122,  139 => 79,  134 => 78,  131 => 77,  111 => 60,  72 => 23,  67 => 21,  62 => 20,  57 => 17,  55 => 16,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
