<?php

/* ami/kpi_target/preview1.html.twig */
class __TwigTemplate_9c70ad1dd48eca4d6add5ca70339c0849091a51bca994178770f8e3124e0bab5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_salespersons_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-line-chart pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Call Target</h4>
                            <p class=\"list-group-item-text\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-bar-chart pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">New Grab Target</h4>
                            <p class=\"list-group-item-text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-area-chart pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">New Grip Target</h4>
                            <p class=\"list-group-item-text\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-dollar pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Sale Target</h4>
                            <p class=\"list-group-item-text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-calendar pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Month/Year</h4>
                            <p class=\"list-group-item-text\">";
        // line 46
        echo select_month($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "month", array()));
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "year", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 54
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "kpi_settings"))) {
            // line 55
            echo "                ";
            echo html_btn(site_url(("ami/kpi_target/edit/" . $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 57
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/kpi_target/preview1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 57,  96 => 55,  94 => 54,  81 => 46,  70 => 38,  59 => 30,  48 => 22,  37 => 14,  25 => 5,  19 => 1,);
    }
}
