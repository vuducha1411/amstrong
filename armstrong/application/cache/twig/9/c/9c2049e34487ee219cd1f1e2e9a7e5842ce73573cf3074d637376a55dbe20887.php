<?php

/* ami/ssd/preview.html.twig */
class __TwigTemplate_9c2049e34487ee219cd1f1e2e9a7e5842ce73573cf3074d637376a55dbe20887 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_ssd_id", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "sku_number", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    ";
        // line 11
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array())) {
            // line 12
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-group pull-left\"></i>

                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Customer</h4>

                                <p class=\"list-group-item-text\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array()), "armstrong_2_customers_id", array()), "html", null, true);
            echo "
                                    - ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "customer", array()), "armstrong_2_customers_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 23
        echo "
                    ";
        // line 24
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array())) {
            // line 25
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-male pull-left\"></i>

                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Salesperson</h4>

                                <p class=\"list-group-item-text\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "
                                    - ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "salesperson", array()), "last_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 36
        echo "
                    ";
        // line 37
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array())) {
            // line 38
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-shopping-cart pull-left\"></i>

                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Wholesaler</h4>

                                <p class=\"list-group-item-text\">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array()), "armstrong_2_wholesalers_id", array()), "html", null, true);
            echo "
                                    - ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "wholesaler", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 49
        echo "
                    ";
        // line 50
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array())) {
            // line 51
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-bus pull-left\"></i>

                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Distributor</h4>

                                <p class=\"list-group-item-text\">";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array()), "armstrong_2_distributors_id", array()), "html", null, true);
            echo "
                                    - ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "distributor", array()), "name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 62
        echo "
                    ";
        // line 63
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "call_record", array())) {
            // line 64
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-coffee pull-left\"></i>

                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Call Records</h4>

                                <p class=\"list-group-item-text\">";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "call_record", array()), "armstrong_2_call_records_id", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 74
        echo "
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-calendar pull-left\"></i>

                        <div class=\"m-l-xl\">
                            <h4 class=\"list-group-item-heading text-muted\">Date</h4>

                            <p class=\"list-group-item-text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "ssd_date", array())), "method"), "toDateString", array(), "method"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-money pull-left\"></i>

                        <div class=\"m-l-xl\">
                            <h4 class=\"list-group-item-heading text-muted\">Total</h4>

                            <p class=\"list-group-item-text\">";
        // line 91
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["total"]) ? $context["total"] : null), 2), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-info pull-left\"></i>

                        <div class=\"m-l-xl\">
                            <h4 class=\"list-group-item-heading text-muted\">Method</h4>

                            <p class=\"list-group-item-text\">";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["methods"]) ? $context["methods"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "method", array()), array(), "array"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-shopping-cart pull-left\"></i>

                        <div class=\"m-l-xl\">
                            <h4 class=\"list-group-item-heading text-muted\">Products</h4>
                            ";
        // line 110
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 111
        echo "                            ";
        echo twig_escape_filter($this->env, get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "sku_number", array()), array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null)), "html", null, true);
        echo "
                            <br>
                            Quantity Pieces: ";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "qty_pcs", array()), "html", null, true);
        echo "
                            Quantity Case: ";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "qty_cases", array()), "html", null, true);
        echo "
                            Free Pieces: ";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "free_qty_pcs", array()), "html", null, true);
        echo "
                            Free Case: ";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "free_qty_cases", array()), "html", null, true);
        echo "
                            
                            <!-- ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ssdGroups"]) ? $context["ssdGroups"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["ssd_product"]) {
            // line 119
            echo "                                ";
            if (($this->getAttribute($context["ssd_product"], "sku_number", array()) != "")) {
                // line 120
                echo "                                    <br><br>
                                    ";
                // line 121
                echo twig_escape_filter($this->env, get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["ssd_product"], "sku_number", array()), array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null)), "html", null, true);
                echo "
                                    <br>
                                    Quantity Pieces: ";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "qty_pcs", array()), "html", null, true);
                echo "
                                    Quantity Case: ";
                // line 124
                echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "qty_cases", array()), "html", null, true);
                echo "
                                    Free Pieces: ";
                // line 125
                echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "free_qty_pcs", array()), "html", null, true);
                echo "
                                    Free Case: ";
                // line 126
                echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "free_qty_cases", array()), "html", null, true);
                echo "
                                ";
            }
            // line 128
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['ssd_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 136
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "ssd"))) {
            // line 137
            echo "                ";
            $context["editUrl"] = (((isset($context["pending"]) ? $context["pending"] : null)) ? ((("ami/ssd/edit/" . $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array())) . "?pending=1")) : (("ami/ssd/edit/" . $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array()))));
            // line 138
            echo "                ";
            echo html_btn(site_url((isset($context["editUrl"]) ? $context["editUrl"] : null)), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 140
        echo "        </div>
    </div>
</div>

<script>
    \$(document).ready(function () {
        \$('.modal-body').slimScroll({
            height: '400px'
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "ami/ssd/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 140,  275 => 138,  272 => 137,  270 => 136,  255 => 128,  250 => 126,  246 => 125,  242 => 124,  238 => 123,  233 => 121,  230 => 120,  227 => 119,  223 => 118,  218 => 116,  214 => 115,  210 => 114,  206 => 113,  200 => 111,  198 => 110,  186 => 101,  173 => 91,  160 => 81,  151 => 74,  144 => 70,  136 => 64,  134 => 63,  131 => 62,  124 => 58,  120 => 57,  112 => 51,  110 => 50,  107 => 49,  100 => 45,  96 => 44,  88 => 38,  86 => 37,  83 => 36,  74 => 32,  70 => 31,  62 => 25,  60 => 24,  57 => 23,  50 => 19,  46 => 18,  38 => 12,  36 => 11,  25 => 5,  19 => 1,);
    }
}
