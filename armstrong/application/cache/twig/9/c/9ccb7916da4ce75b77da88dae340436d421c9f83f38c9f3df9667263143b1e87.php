<?php

/* ami/news_feed/index.html.twig */
class __TwigTemplate_9ccb7916da4ce75b77da88dae340436d421c9f83f38c9f3df9667263143b1e87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/news_feed/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/news_feed/ajaxData"), "html", null, true);
        echo "\",*/
                \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 37
    public function block_css($context, array $blocks = array())
    {
        // line 38
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 42
    public function block_content($context, array $blocks = array())
    {
        // line 43
        echo "
    ";
        // line 44
        echo form_open(site_url("ami/news_feed/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 49
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("News Feed Draft") : ("News Feed"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 53
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 55
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/news_feed/index.html.twig", 55)->display(array_merge($context, array("url" => "ami/news_feed", "icon" => "fa-user", "title" => "News Feed", "permission" => "news_feed", "showImport" => 0)));
        // line 56
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 70
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 71
            echo "                                <div class=\"row table-filter\">
                                    <div class=\"col-md-12\">
                                        <div class=\"dropdown\">
                                            <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                                    id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                                Filter: ";
            // line 76
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                                        class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                                <li role=\"presentation\" ";
            // line 80
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                                            role=\"menuitem\" tabindex=\"-1\"
                                                            href=\"";
            // line 82
            echo twig_escape_filter($this->env, site_url("ami/news_feed"), "html", null, true);
            echo "\">All</a></li>
                                                <li role=\"presentation\" ";
            // line 83
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "approved")) ? ("class=\"active\"") : (""));
            echo ">
                                                    <a role=\"menuitem\" tabindex=\"-1\"
                                                       href=\"";
            // line 85
            echo twig_escape_filter($this->env, site_url("ami/news_feed?filter=approved"), "html", null, true);
            echo "\">Approved</a>
                                                </li>
                                                <li role=\"presentation\" ";
            // line 87
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo ">
                                                    <a role=\"menuitem\" tabindex=\"-1\"
                                                       href=\"";
            // line 89
            echo twig_escape_filter($this->env, site_url("ami/news_feed?filter=pending"), "html", null, true);
            echo "\">Pending</a>
                                                </li>
                                                <li role=\"presentation\" ";
            // line 91
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "reject")) ? ("class=\"active\"") : (""));
            echo ">
                                                    <a role=\"menuitem\" tabindex=\"-1\"
                                                       href=\"";
            // line 93
            echo twig_escape_filter($this->env, site_url("ami/news_feed?filter=reject"), "html", null, true);
            echo "\">Reject</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ";
        }
        // line 100
        echo "                            <thead>
                            <tr>
                                ";
        // line 102
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news_feed"))) {
            // line 103
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 107
        echo "                                <th>ID</th>
                                <th>Content</th>
                                <th>Salespersons</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_feed"]) ? $context["news_feed"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
            // line 119
            echo "                                <tr>
                                    ";
            // line 120
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news_feed"))) {
                // line 121
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["new"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 123
            echo "                                    <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/news_feed/edit/" . $this->getAttribute($context["new"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "id", array()), "html", null, true);
            echo "\"
                                           data-toggle=\"ajaxModal\">";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "id", array()), "html", null, true);
            echo "</a></td>
                                    <td>";
            // line 125
            echo wordTrim($this->getAttribute($context["new"], "content", array()), 120);
            echo "</td>
                                    <td class=\"center\">";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 127
            echo ((($this->getAttribute($context["new"], "newsfeed_type", array()) == 0)) ? ("Local") : ("Global"));
            echo "</td>
                                    <td class=\"center\">";
            // line 128
            if (($this->getAttribute($context["new"], "status", array()) == 0)) {
                echo " Pending
                                        ";
            } elseif (($this->getAttribute(            // line 129
$context["new"], "status", array()) == 1)) {
                echo "    Approved
                                        ";
            } else {
                // line 130
                echo "  Reject
                                        ";
            }
            // line 132
            echo "                                    </td>
                                    <td class=\"center\">";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["new"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 138
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "news_feed"))) {
                // line 139
                echo "                                                ";
                echo html_btn(site_url(((("ami/news_feed/edit/" . $this->getAttribute($context["new"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "

                                            ";
            }
            // line 142
            echo "
                                            ";
            // line 143
            if (((isset($context["filter"]) ? $context["filter"] : null) == "pending")) {
                // line 144
                echo "                                                ";
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
                    // line 145
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/news_feed/pending/" . $this->getAttribute($context["new"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                    // line 146
                    echo html_btn(site_url(("ami/news_feed/pending/" . $this->getAttribute($context["new"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 148
                echo "                                            ";
            }
            // line 149
            echo "
                                            ";
            // line 150
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "news_feed")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 151
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 152
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/news_feed/restore/" . $this->getAttribute($context["new"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 154
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/news_feed/delete/" . $this->getAttribute($context["new"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 156
                echo "                                            ";
            }
            // line 157
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 161
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 165
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 166
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/news_feed/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/news_feed/draft")));
            // line 167
            echo "                                ";
        } else {
            // line 168
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/news_feed/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/news_feed")));
            // line 169
            echo "                                ";
        }
        // line 170
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 184
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/news_feed/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  390 => 184,  372 => 170,  369 => 169,  366 => 168,  363 => 167,  360 => 166,  358 => 165,  352 => 161,  343 => 157,  340 => 156,  334 => 154,  328 => 152,  325 => 151,  323 => 150,  320 => 149,  317 => 148,  312 => 146,  307 => 145,  304 => 144,  302 => 143,  299 => 142,  292 => 139,  290 => 138,  283 => 134,  279 => 133,  276 => 132,  272 => 130,  267 => 129,  263 => 128,  259 => 127,  255 => 126,  251 => 125,  247 => 124,  240 => 123,  234 => 121,  232 => 120,  229 => 119,  225 => 118,  212 => 107,  206 => 103,  204 => 102,  200 => 100,  190 => 93,  185 => 91,  180 => 89,  175 => 87,  170 => 85,  165 => 83,  161 => 82,  156 => 80,  149 => 76,  142 => 71,  140 => 70,  124 => 56,  122 => 55,  117 => 53,  110 => 49,  102 => 44,  99 => 43,  96 => 42,  90 => 39,  85 => 38,  82 => 37,  57 => 15,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
