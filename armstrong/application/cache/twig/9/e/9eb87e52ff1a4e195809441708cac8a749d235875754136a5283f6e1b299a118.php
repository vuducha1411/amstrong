<?php

/* ami/master_template/preview.html.twig */
class __TwigTemplate_9eb87e52ff1a4e195809441708cac8a749d235875754136a5283f6e1b299a118 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_salespersons_id", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_customers_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-user pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Customer</h4>
                            <p class=\"list-group-item-text\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_customers_id", array()), array(), "array"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-user pull-left\"></i>
                        <div class=\"m-l-xl\">                          
                            <h4 class=\"list-group-item-heading text-muted\">Salespersons</h4>
                            <p class=\"list-group-item-text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "armstrong_2_salespersons_id", array()), array(), "array"), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-calendar pull-left\"></i>
                        <div class=\"m-l-xl\">                          
                            <h4 class=\"list-group-item-heading text-muted\">Planned Day</h4>
                            <p class=\"list-group-item-text\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "planned_day", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 38
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "route_plan_master_template"))) {
            // line 39
            echo "                ";
            echo html_btn(site_url(("ami/master_template/edit/" . $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 41
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/master_template/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 41,  74 => 39,  72 => 38,  61 => 30,  50 => 22,  39 => 14,  25 => 5,  19 => 1,);
    }
}
