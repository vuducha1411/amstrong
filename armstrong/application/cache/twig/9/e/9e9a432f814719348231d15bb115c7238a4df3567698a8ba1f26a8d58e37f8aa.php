<?php

/* ami/wholesalers/edit.html.twig */
class __TwigTemplate_9e9a432f814719348231d15bb115c7238a4df3567698a8ba1f26a8d58e37f8aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/wholesalers/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/customer-chain.js"), "html", null, true);
        echo "\"></script>
    <script>
        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        //mother
        var armstrong_2_related_mother_id = '";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_related_mother_id", array()), "html", null, true);
        echo "';
        \$('select[name=\"armstrong_2_related_mother_id\"]').change(function(){
            armstrong_2_related_mother_id = \$('select[name=\"armstrong_2_related_mother_id\"]').val();
        });
        function getRelatedMother()
        {
            var armstrong_2_salespersons = '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_salespersons_id\"]').val();
            armstrong_2_salespersons += '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_secondary_salespersons_id\"]').val();
            armstrong_2_salespersons += '&is_mother=1';
            armstrong_2_salespersons += '&type_cus=wholesalers';
            armstrong_2_salespersons += '&action=related_mother';
            armstrong_2_salespersons += '&id_current='+\$('input[name=\"armstrong_2_wholesalers_id\"]').val();
            \$.ajax({
                url: \"";
        // line 26
        echo twig_escape_filter($this->env, site_url("ami/customers/getCustomerBySalespersons"), "html", null, true);
        echo "\",
                data: armstrong_2_salespersons,
                type: 'POST',
                dataType: 'html',
                success: function (html) {
                    \$('select[name=\"armstrong_2_related_mother_id\"]').html(html);
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[selected=\"selected\"]').removeAttr('selected');
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[value=\"'+armstrong_2_related_mother_id+'\"]').attr('selected', 'selected');
                }
            });
        }
        \$('select[name=\"armstrong_2_salespersons_id\"], select[name=\"armstrong_2_secondary_salespersons_id').change(function(){
            getRelatedMother();
        });
        getRelatedMother();
        \$('input[name=\"is_mother\"]').change(function(){
            console.log(\$('input[name=\"is_mother\"]').val());
            if(\$('input[name=\"is_mother\"]:checked').val() == 1)
            {
                \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').hide();
            }
            else
            {
                \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').show();
            }
        });
        \$('input[name=\"is_mother\"]').trigger('change');
        //end mother
        \$(function () {
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 63
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + toTitleCase(res['value'][key]) + '\">' + toTitleCase(res['value'][key]) + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        weightage = \$this.find('option:selected').data('weightage') || 0;

                \$('#ChannelWeightage').val(weightage);
            });

            \$('#InputSalespersons').autocomplete({
                source: \"";
        // line 98
        echo twig_escape_filter($this->env, site_url("ami/salespersons/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputSalespersonsId').val(ui.item.value);
                }
            }).on('focus', function () {
                this.select();
            });
            \$(document).ready(function () {
                var approved = '";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()), "html", null, true);
        echo "',
                        active = '";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "active", array()), "html", null, true);
        echo "',
                        optionCloseDown = \$('#approved option[value=\"-2\"]'),
                        optionReject = \$('#approved option[value=\"-1\"]'),
                        optionApproved = \$('#approved option[value=\"1\"]');

                if (active == 1) {
                    if (approved == -2 || approved == -1) {
                        \$('#approved').val('');
                    }
                    optionApproved.show();
                    optionCloseDown.hide();
                    optionReject.hide();
                } else {
                    if (approved == 1) {
                        \$('#approved').val('');
                    }
                    optionApproved.hide();
                    optionCloseDown.show();
                    optionReject.show();
                }

                \$('input[name=\"active\"]').on('change', function () {
                    var \$val = \$(this).val();
                    \$('#approved').val('');
                    if (\$val == 1) {
                        optionApproved.show();
                        optionCloseDown.hide();
                        optionReject.hide();
                    } else {
                        optionApproved.hide();
                        optionCloseDown.show();
                        optionReject.show();
                    }
                });
            });
        });
    </script>
";
    }

    // line 149
    public function block_content($context, array $blocks = array())
    {
        // line 150
        echo "
    ";
        // line 151
        echo form_open(site_url("ami/wholesalers/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_wholesalers_id" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Wholesalers</h1>
                <div class=\"text-right\">
                    ";
        // line 158
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/wholesalers/edit.html.twig", 158)->display(array_merge($context, array("url" => "ami/wholesalers", "id" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()), "permission" => "wholesalers")));
        // line 159
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 169
        echo form_label("Armstrong 1 wholesalers id", "armstrong_1_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 171
        echo form_input(array("name" => "armstrong_1_wholesalers_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_1_wholesalers_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 176
        echo form_label("Armstrong 2 wholesalers id", "armstrong_2_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 178
        echo form_input(array("name" => "armstrong_2_wholesalers_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array()), "class" => "form-control", "disabled" => "disabled"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 183
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 185
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

                <div class=\"form-group\">
                    ";
        // line 190
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 192
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" ");
        echo "
                    </div>
                </div>

            <div class=\"form-group\">
                ";
        // line 197
        echo form_label("Armstrong 2 Secondary Salespersons", "armstrong_2_secondary_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 199
        echo form_dropdown("armstrong_2_secondary_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_secondary_salespersons_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 203
        echo form_label("Is Mother", "is_mother", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 205
        echo form_radio("is_mother", 1, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_mother", array()) == 1));
        echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 206
        echo form_radio("is_mother", 0, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_mother", array()) == 0));
        echo " No</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 210
        echo form_label("Related Mother", "armstrong_2_related_mother_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 212
        echo form_dropdown("armstrong_2_related_mother_id", (isset($context["related_mother"]) ? $context["related_mother"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_related_mother_id", array()), "class=\"form-control\" data-type=\"wholesalers\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 217
        echo form_label("Armstrong 2 Chains Id", "armstrong_2_chains_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 219
        echo form_dropdown("armstrong_2_chains_id", (isset($context["chains"]) ? $context["chains"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_chains_id", array()), "class=\"form-control\" data-type=\"wholesalers\"");
        echo "
                </div>
            </div>
            ";
        // line 222
        if ((twig_length_filter($this->env, (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null)) > 1)) {
            // line 223
            echo "                <div class=\"form-group\">
                    ";
            // line 224
            echo form_label("Distributor", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 226
            echo form_dropdown("armstrong_2_distributors_id", (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_distributors_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 230
        echo "


            <div class=\"form-group\">
                ";
        // line 234
        echo form_label("Customers Types", "customers_types_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <label class=\"control-label\">";
        // line 236
        echo ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "customers_types_id", array()) == 2)) ? ("Distributor") : (((($this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "customers_types_id", array()) == 1)) ? ("Customer") : ("Wholesalers"))));
        echo "</label>
                </div>
            </div>

            ";
        // line 241
        echo "            ";
        // line 242
        echo "            ";
        // line 243
        echo "            ";
        // line 244
        echo "            ";
        // line 245
        echo "            ";
        // line 246
        echo "            ";
        // line 247
        echo "            ";
        // line 248
        echo "            ";
        // line 249
        echo "            ";
        // line 250
        echo "            ";
        // line 251
        echo "            ";
        // line 252
        echo "            <div class=\"form-group\">
                ";
        // line 253
        echo form_label("SSD ID", "ssd_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 255
        echo form_input(array("name" => "ssd_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "ssd_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 259
        echo form_label("SSD Name", "sap_cust_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 261
        echo form_input(array("name" => "sap_cust_name", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "sap_cust_name", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 265
        echo form_label("Sap ID", "sap_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 267
        echo form_input(array("name" => "sap_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "sap_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 271
        echo form_label("Street Address", "street_address", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 273
        echo form_input(array("name" => "street_address", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "street_address", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            ";
        // line 277
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countryHierachy"]) ? $context["countryHierachy"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
            // line 278
            echo "                <div class=\"form-group select-change-container\">
                    ";
            // line 279
            echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 281
            if ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "armstrong_2_wholesalers_id", array())) {
                // line 282
                echo "                            ";
                echo form_dropdown($context["countryField"], $this->getAttribute((isset($context["countryValues"]) ? $context["countryValues"] : null), $context["countryField"], array(), "array"), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            } else {
                // line 284
                echo "                            ";
                echo form_dropdown($context["countryField"], ((($context["key"] == "root")) ? ((isset($context["rootValues"]) ? $context["rootValues"] : null)) : (array())), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            }
            // line 286
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 289
        echo "
            <div class=\"form-group\">
                ";
        // line 291
        echo form_label("Postal Code", "postal_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 293
        echo form_input(array("name" => "postal_code", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "postal_code", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 298
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 300
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "phone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 305
        echo form_label("Phone 2", "phone_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 307
        echo form_input(array("name" => "phone_2", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "phone_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 312
        echo form_label("SMS Number", "sms_number", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 314
        echo form_input(array("name" => "sms_number", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "sms_number", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 319
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 321
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 326
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 328
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 333
        echo form_label("Email 2", "email_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 335
        echo form_input(array("name" => "email_2", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "email_2", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 340
        echo form_label("Email 3", "email_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 342
        echo form_input(array("name" => "email_3", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "email_3", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            ";
        // line 346
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw"))) {
            // line 347
            echo "                <div class=\"form-group\">
                    ";
            // line 348
            echo form_label("LINE ID", "line_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 350
            echo form_input(array("name" => "line_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "line_id", array()), "class" => "form-control"));
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 355
            echo form_label("", "disable_line", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-5\">
                        <input type=\"checkbox\" value=\"1\" name=\"disable_line\" id=\"disableLine\" onclick=\"toggle(this)\" ";
            // line 357
            echo ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "disable_line", array()) != "0")) ? ("checked") : (""));
            echo ">&nbsp;Disable LINE Messages</input>
                    </div>
                </div>
            ";
        }
        // line 361
        echo "
            <div class=\"form-group\">
                ";
        // line 363
        echo form_label("Account Number", "account_number", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 365
        echo form_input(array("name" => "account_number", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "account_number", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-maxlength" => "4"));
        echo "
                </div>
            </div>
            ";
        // line 374
        echo "            <div class=\"form-group\">
                ";
        // line 375
        echo form_label("Current Approval Status", "current_status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 377
        echo form_input(array("name" => "current_status", "value" => ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) ==  -2)) ? ("Close down") : (((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) ==  -1)) ? ("Rejected") : (((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) == 0)) ? ("Pending") : (((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) == 1)) ? ("Approved") : (((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) == 2)) ? ("Edit") : ("")))))))))), "class" => "form-control", "readonly" => "readonly"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 381
        echo form_label("", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 383
        echo form_radio("active", 0, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 384
        echo form_radio("active", 1, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>
            ";
        // line 387
        if (((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw")) && ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin"))) {
            // line 388
            echo "                <div class=\"form-group\">
                    ";
            // line 389
            echo form_label("Approved", "approved", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <select name=\"approved\" id=\"approved\" class=\"form-control\" data-parsley-required=\"true\">
                            <option value=\"\">--Select--</option>
                            <option value=\"-2\" ";
            // line 393
            echo ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) ==  -2)) ? ("selected") : (""));
            echo ">Close down</option>
                            <option value=\"-1\" ";
            // line 394
            echo ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) ==  -1)) ? ("selected") : (""));
            echo ">Rejected</option>
                            ";
            // line 396
            echo "                            <option value=\"1\" ";
            echo ((($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "approved", array()) == 1)) ? ("selected") : (""));
            echo ">Approved</option>
                            ";
            // line 398
            echo "                        </select>
                    </div>
                </div>
            ";
        }
        // line 402
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["country_channels"]) ? $context["country_channels"] : null)) > 1)) {
            // line 403
            echo "                <div class=\"form-group\">
                    ";
            // line 404
            echo form_label("Country Channel", "country_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 406
            echo form_dropdown("country_channels_id", (isset($context["country_channels"]) ? $context["country_channels"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "country_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 410
        echo "
            ";
        // line 411
        if ((twig_length_filter($this->env, (isset($context["country_sub_channels"]) ? $context["country_sub_channels"] : null)) > 1)) {
            // line 412
            echo "                <div class=\"form-group\">
                    ";
            // line 413
            echo form_label("Country Sub Channel", "country_sub_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 415
            echo form_dropdown("country_sub_channels_id", (isset($context["country_sub_channels"]) ? $context["country_sub_channels"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "country_sub_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 419
        echo "            <div class=\"form-group\">
                ";
        // line 420
        echo form_label("DTM", "otm", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 422
        echo form_dropdown("otm", (isset($context["otm"]) ? $context["otm"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "otm", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            ";
        // line 425
        if ((twig_length_filter($this->env, (isset($context["business_types"]) ? $context["business_types"] : null)) > 1)) {
            // line 426
            echo "                <div class=\"form-group\">
                    ";
            // line 427
            echo form_label("Business Type", "business_types_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 429
            echo form_dropdown("business_types_id", (isset($context["business_types"]) ? $context["business_types"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "business_types_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 433
        echo "
            ";
        // line 434
        if ((twig_length_filter($this->env, (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null)) > 1)) {
            // line 435
            echo "                <div class=\"form-group\">
                    ";
            // line 436
            echo form_label("Assigned Distributor 1", "assigned_distributor_1", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 438
            echo form_dropdown("assigned_distributor_1", (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "assigned_distributor_1", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 442
        echo "
            ";
        // line 443
        if ((twig_length_filter($this->env, (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null)) > 1)) {
            // line 444
            echo "                <div class=\"form-group\">
                    ";
            // line 445
            echo form_label("Assigned Distributor 2", "assigned_distributor_2", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 447
            echo form_dropdown("assigned_distributor_2", (isset($context["assigned_distributors"]) ? $context["assigned_distributors"] : null), $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "assigned_distributor_2", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 451
        echo "
            <div class=\"form-group\">
                ";
        // line 453
        echo form_label("Contact Details", "contact_details", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 455
        echo form_textarea(array("name" => "contact_details", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "contact_details", array()), "class" => "form-control", "rows" => 3));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 460
        echo form_label("Is plus", "is_plus", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 462
        echo form_radio("is_plus", 0, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_plus", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 463
        echo form_radio("is_plus", 1, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_plus", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 467
        echo form_label("Is Perfect Store", "is_perfect_store", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 469
        echo form_radio("is_perfect_store", 0, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_perfect_store", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 470
        echo form_radio("is_perfect_store", 1, ($this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "is_perfect_store", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>
        </div>
    </div>

    ";
        // line 476
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  851 => 476,  842 => 470,  838 => 469,  833 => 467,  826 => 463,  822 => 462,  817 => 460,  809 => 455,  804 => 453,  800 => 451,  793 => 447,  788 => 445,  785 => 444,  783 => 443,  780 => 442,  773 => 438,  768 => 436,  765 => 435,  763 => 434,  760 => 433,  753 => 429,  748 => 427,  745 => 426,  743 => 425,  737 => 422,  732 => 420,  729 => 419,  722 => 415,  717 => 413,  714 => 412,  712 => 411,  709 => 410,  702 => 406,  697 => 404,  694 => 403,  691 => 402,  685 => 398,  680 => 396,  676 => 394,  672 => 393,  665 => 389,  662 => 388,  660 => 387,  654 => 384,  650 => 383,  645 => 381,  638 => 377,  633 => 375,  630 => 374,  624 => 365,  619 => 363,  615 => 361,  608 => 357,  603 => 355,  595 => 350,  590 => 348,  587 => 347,  585 => 346,  578 => 342,  573 => 340,  565 => 335,  560 => 333,  552 => 328,  547 => 326,  539 => 321,  534 => 319,  526 => 314,  521 => 312,  513 => 307,  508 => 305,  500 => 300,  495 => 298,  487 => 293,  482 => 291,  478 => 289,  470 => 286,  464 => 284,  458 => 282,  456 => 281,  451 => 279,  448 => 278,  444 => 277,  437 => 273,  432 => 271,  425 => 267,  420 => 265,  413 => 261,  408 => 259,  401 => 255,  396 => 253,  393 => 252,  391 => 251,  389 => 250,  387 => 249,  385 => 248,  383 => 247,  381 => 246,  379 => 245,  377 => 244,  375 => 243,  373 => 242,  371 => 241,  364 => 236,  359 => 234,  353 => 230,  346 => 226,  341 => 224,  338 => 223,  336 => 222,  330 => 219,  325 => 217,  317 => 212,  312 => 210,  305 => 206,  301 => 205,  296 => 203,  289 => 199,  284 => 197,  276 => 192,  271 => 190,  263 => 185,  258 => 183,  250 => 178,  245 => 176,  237 => 171,  232 => 169,  220 => 159,  218 => 158,  208 => 151,  205 => 150,  202 => 149,  160 => 110,  156 => 109,  142 => 98,  104 => 63,  64 => 26,  48 => 13,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
