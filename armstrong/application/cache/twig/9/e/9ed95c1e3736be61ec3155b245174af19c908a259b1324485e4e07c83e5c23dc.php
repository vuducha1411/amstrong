<?php

/* ami/chains/edit.html.twig */
class __TwigTemplate_9ed95c1e3736be61ec3155b245174af19c908a259b1324485e4e07c83e5c23dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/chains/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        \$('input[name=\"type\"]').change(function(){
            console.log(\$('input[name=\"type\"]').val());
            if(\$('input[name=\"type\"]:checked').val() == 2)
            {
                \$('select[name=\"armstrong_2_grandmother_id\"]').closest('.form-group').hide();
            }
            else
            {
                \$('select[name=\"armstrong_2_grandmother_id\"]').closest('.form-group').show();
            }
        });
        \$('input[name=\"is_mother\"]').trigger('change');
        \$(function () {

            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 35
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + res['value'][key] + '\">' + res['value'][key] + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            ";
        // line 62
        if ( !$this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
            // line 63
            echo "            \$('.select-change').trigger('change');
            ";
        }
        // line 65
        echo "
            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        weightage = \$this.find('option:selected').data('weightage') || 0,
                        convenience_factor = \$this.find('option:selected').data('convenience_lvl') || 0,
                        ufs_share = \$this.find('option:selected').data('ufs_food_share') || 0;
                \$('#ChannelWeightage, #FoodCostNew').val(weightage);
                ";
        // line 72
        $context["au_nz"] = array(0 => 2, 1 => 9);
        // line 73
        echo "                ";
        $context["id_sa"] = array(0 => 10, 1 => 4);
        // line 74
        echo "                ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
            // line 75
            echo "                \$('#convenienceFactor').val(convenience_factor);
                ";
        }
        // line 77
        echo "                ";
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 78
            echo "                \$('.id-ufs-share').val(ufs_share);
                ";
        }
        // line 80
        echo "                \$('input[name=\"ufs_share\"]').val(ufs_share);
            });

            \$('.fetch-otm').on('change', function () {
                var foodOverElement = \$('input[name=\"food_turnover_new\"]'),
                        foodPurchaseElement = \$('input[name=\"food_purchase_value_new\"]'),
                        otmFoodCostElement = \$('input[name=\"ufs_share_food_cost_new\"]'),
                        otmPotentialElement = \$('input[name=\"otm_potential_value_new\"]'),
                        otmNewElement = \$('input[name=\"otm_new\"]'),
                        aFoodCost = \$('#FoodCostNew').val(),
                        aUfsShare = \$('input[name=\"assumption_ufs_share_new\"]').val(),
                        convenience = \$('#convenienceFactor').val();
                if (\$('.convenience_other_country').length > 0) {
                    convenience = \$('.convenience_other_country').val() == 0 ? 0.5 : 1;
                }
                var foodOver,
                        foodPurchase,
                        otmFoodCost,
                        otmNew
                \$.each(\$('.fetch-food'), function () {
                    var \$val = \$(this).val();

                    if (!foodOver) {
                        foodOver = \$val;
                    } else {
                        foodOver = foodOver * \$val;
                    }
                });

//                foodOver = Math.round(foodOver).toFixed(3);
                foodPurchase = foodOver * aFoodCost;
                otmFoodCost = foodPurchase * aUfsShare;
                otmNew = otmFoodCost * convenience;

                foodOverElement.val(foodOver);
                foodPurchaseElement.val(foodPurchase);
                otmFoodCostElement.val(otmFoodCost);
                otmPotentialElement.val(otmNew);

                \$.ajax({
                    url: \"";
        // line 120
        echo twig_escape_filter($this->env, site_url("ami/customers/getOtmBandwidth"), "html", null, true);
        echo "\",
                    data: {otm: otmNew},
                    type: 'POST',
                    success: function (res) {
                        otmNewElement.val(res);
                    }
                });

            });

//            \$('.fetch-otm').trigger('change');

            \$('#SellingInput').on('change input blur', function (e) {
                \$('#BudgetInput').val('');
            });

            \$('#BudgetInput').on('change input blur', function (e) {
                \$('#SellingInput').val('');
            });

            \$('#InputBestTimeCall, .timepicker').datetimepicker({
                format: 'h:mm A'
            });

            ";
        // line 144
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 145
            echo "            \$('#subChannelSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        country_channels_id = optionSelected.data('country_channel'),
                        global_channels_id = \$('#ChannelSelect').find('option[value=' + country_channels_id + ']').data('global_channels_id');
                \$('#ChannelSelect, #channelHidden').val(country_channels_id);
                \$('#globalChannels, #globalChannelHidden').val(global_channels_id);
                \$('#ChannelSelect').trigger('change');
            });
            ";
        } else {
            // line 155
            echo "            \$('#globalChannels').on('change', function () {
                var \$this = \$(this),
                        global_channel_id = \$this.val(),
                        channel_option = \$('#ChannelSelect option');

                \$.each(channel_option, function () {
                    var data_global_channel_id = \$(this).data('global_channels_id');
                    if (data_global_channel_id == global_channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#ChannelSelect option:first-child').show().prop('selected', true);
                \$('#subChannelSelect option').hide();
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });

            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        channel_id = \$this.val(),
                        sub_channel_option = \$('#subChannelSelect option');
                \$.each(sub_channel_option, function () {
                    var country_channel_id = \$(this).data('country_channel');
                    if (country_channel_id == channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });
            ";
        }
        // line 188
        echo "        });

        \$(document).ready(function () {
            var approved = '";
        // line 191
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()), "html", null, true);
        echo "',
                    active = '";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()), "html", null, true);
        echo "',
                    optionCloseDown = \$('#approved option[value=\"-2\"]'),
                    optionReject = \$('#approved option[value=\"-1\"]'),
                    optionApproved = \$('#approved option[value=\"1\"]');

            if (active == 1) {
                if (approved == -2 || approved == -1) {
                    \$('#approved').val('');
                }
                optionApproved.show();
                optionCloseDown.hide();
                optionReject.hide();
            } else {
                if (approved == 1) {
                    \$('#approved').val('');
                }
                optionApproved.hide();
                optionCloseDown.show();
                optionReject.show();
            }

            \$('input[name=\"active\"]').on('change', function () {
                var \$val = \$(this).val();
                \$('#approved').val('');
                if (\$val == 1) {
                    optionApproved.show();
                    optionCloseDown.hide();
                    optionReject.hide();
                } else {
                    optionApproved.hide();
                    optionCloseDown.show();
                    optionReject.show();
                }
            });

            // init sub channel
            ";
        // line 228
        if (!twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 229
            echo "            \$.each(\$('#subChannelSelect option'), function () {
                var country_channel_id = \$(this).data('country_channel');
                if (country_channel_id == \$('#ChannelSelect').val()) {
                    \$(this).show();
                } else {
                    \$(this).hide();
                }
            });
            ";
        }
        // line 238
        echo "
            ";
        // line 239
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 240
            echo "            \$('input[name=\"assumption_ufs_share_new\"]').val(\$('#ChannelSelect').find('option:selected').data('ufs_food_share'));
            ";
        }
        // line 242
        echo "        });

        \$(function () {
            \$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD hh:mm:ss'
            });
        });
    </script>
";
    }

    // line 252
    public function block_css($context, array $blocks = array())
    {
        // line 253
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\"
          href=\"";
        // line 255
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">

";
    }

    // line 259
    public function block_content($context, array $blocks = array())
    {
        // line 260
        echo "
    ";
        // line 261
        echo form_open(site_url("ami/chains/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_chains_id" => $this->getAttribute(        // line 262
(isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_chains_id", array()), "filter" =>         // line 263
(isset($context["filter"]) ? $context["filter"] : null)));
        // line 264
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Chains</h1>

                <div class=\"text-right\">
                    ";
        // line 272
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/chains/edit.html.twig", 272)->display(array_merge($context, array("url" => "ami/chains", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_chains_id", array()), "permission" => "chains")));
        // line 273
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 283
        echo form_label("Name", "armstrong_2_chains_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 285
        echo form_input(array("name" => "armstrong_2_chains_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_chains_name", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-maxlength" => 100));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 290
        echo form_label("Armstrong 1 Chains Id", "armstrong_1_chains_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 292
        echo form_input(array("name" => "armstrong_1_chains_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_1_chains_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 297
        echo form_label("Armstrong 2 Chains Id", "armstrong_2_chains_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 299
        echo form_input(array("name" => "armstrong_2_chains_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_chains_id", array()), "class" => "form-control", "disabled" => "disabled"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 303
        echo form_label("Type", "type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 305
        echo form_radio("type", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "type", array()) == 1));
        echo " Mother</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 306
        echo form_radio("type", 2, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "type", array()) == 2));
        echo " GrandMother</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 310
        echo form_label("Grand Mother ID", "armstrong_2_grandmother_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 312
        echo form_dropdown("armstrong_2_grandmother_id", (isset($context["chains"]) ? $context["chains"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_grandmother_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 317
        echo form_label("Current Approval Status", "current_status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 319
        echo form_input(array("name" => "current_status", "value" => ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -2)) ? ("Close down") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -1)) ? ("Rejected") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 0)) ? ("Pending") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 1)) ? ("Approved") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 2)) ? ("Edit") : ("")))))))))), "class" => "form-control", "readonly" => "readonly"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 324
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 326
        echo form_radio("active", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 327
        echo form_radio("active", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                </div>
            </div>

            ";
        // line 331
        if (((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw")) && ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin"))) {
            // line 332
            echo "                <div class=\"form-group\">
                    ";
            // line 333
            echo form_label("Approved", "approved", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <select name=\"approved\" id=\"approved\" class=\"form-control\" data-parsley-required=\"true\">
                            <option value=\"\">--Select--</option>
                            <option value=\"-2\" ";
            // line 337
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -2)) ? ("selected") : (""));
            echo ">Close down</option>
                            <option value=\"-1\" ";
            // line 338
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -1)) ? ("selected") : (""));
            echo ">Rejected</option>
                            ";
            // line 340
            echo "                            <option value=\"1\" ";
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 1)) ? ("selected") : (""));
            echo ">Approved</option>
                            ";
            // line 342
            echo "                        </select>
                    </div>
                </div>
            ";
        }
        // line 346
        echo "
            ";
        // line 347
        if ((twig_length_filter($this->env, (isset($context["groups"]) ? $context["groups"] : null)) > 1)) {
            // line 348
            echo "                <div class=\"form-group\">
                    ";
            // line 349
            echo form_label("Group", "groups_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 351
            echo form_dropdown("groups_id", (isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "groups_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 355
        echo "
            <div class=\"form-group\">
                ";
        // line 357
        echo form_label("SSD Name", "sap_cust_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 359
        echo form_input(array("name" => "sap_cust_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 364
        echo form_label("SSD Name 2", "sap_cust_name_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 366
        echo form_input(array("name" => "sap_cust_name_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 371
        echo form_label("SSD ID", "ssd_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 373
        echo form_input(array("name" => "ssd_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 378
        echo form_label("SSD ID 2", "ssd_id_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 380
        echo form_input(array("name" => "ssd_id_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 385
        echo form_label("Sap ID", "sap_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 387
        echo form_input(array("name" => "sap_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 392
        echo form_label("Street Address", "street_address", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 394
        echo form_input(array("name" => "street_address", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "street_address", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 425
        echo "
            ";
        // line 426
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countryHierachy"]) ? $context["countryHierachy"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
            // line 427
            echo "                <div class=\"form-group select-change-container\">
                    ";
            // line 428
            echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 430
            if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
                // line 431
                echo "                            ";
                echo form_dropdown($context["countryField"], $this->getAttribute((isset($context["countryValues"]) ? $context["countryValues"] : null), $context["countryField"], array(), "array"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            } else {
                // line 433
                echo "                            ";
                $context["_values"] = ((($context["key"] == "root")) ? ((isset($context["rootValues"]) ? $context["rootValues"] : null)) : (array()));
                // line 434
                echo "                            ";
                echo form_dropdown($context["countryField"], (isset($context["_values"]) ? $context["_values"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            }
            // line 436
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 439
        echo "
            <div class=\"form-group\">
                ";
        // line 441
        echo form_label("Postal Code", "postal_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 443
        echo form_input(array("name" => "postal_code", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "postal_code", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 448
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 450
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 455
        echo form_label("Phone 2", "phone_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 457
        echo form_input(array("name" => "phone_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone_two", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 462
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 464
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 469
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 471
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 476
        echo form_label("Email 2", "email_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 478
        echo form_input(array("name" => "email_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email_two", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            ";
        // line 482
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
            // line 483
            echo "                <div class=\"form-group\">
                    ";
            // line 484
            echo form_label("Primary Contact", "primary_contact", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 486
            echo form_dropdown("primary_contact", (isset($context["contactListOptions"]) ? $context["contactListOptions"] : null), (isset($context["primaryContact"]) ? $context["primaryContact"] : null), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 490
        echo "            <div class=\"form-group\">
                ";
        // line 491
        echo form_label("Acct Opened", "acct_opened", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 493
        echo form_input(array("name" => "acct_opened", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "acct_opened", array()), "class" => "form-control datepicker"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 497
        echo form_label("Web Url", "web_url", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 499
        echo form_input(array("name" => "web_url", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "web_url", array()), "class" => "form-control", "data-parsley-type" => "url"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 504
        echo form_label("Global Channel", "global_channels", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 506
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 507
            echo "                        ";
            echo form_dropdown("hidden_global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\" disabled=\"disabled\"");
            echo "
                    ";
        } else {
            // line 509
            echo "                        ";
            echo form_dropdown("global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    ";
        }
        // line 511
        echo "                </div>
            </div>
            ";
        // line 513
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 514
            echo "                <input id=\"globalChannelHidden\" type=\"hidden\" name=\"global_channels_id\"
                       value=\"";
            // line 515
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "html", null, true);
            echo "\">
            ";
        }
        // line 517
        echo "
            ";
        // line 518
        if ((isset($context["country_channels"]) ? $context["country_channels"] : null)) {
            // line 519
            echo "                <div class=\"form-group\">
                    ";
            // line 520
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 523
            echo "                        <select name=\"";
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disable_channel") : ("channel"));
            echo "\"
                                class=\"form-control fetch-otm ";
            // line 524
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 2, 1 => 9))) ? ("channellv") : (""));
            echo "\"
                                id=\"ChannelSelect\"
                                data-parsley-required=\"true\" ";
            // line 526
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disabled=\"disabled\"") : (""));
            echo ">
                            <option value=\"\" data-weightage=\"\">- Select -</option>
                            ";
            // line 528
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["country_channels"]) ? $context["country_channels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 529
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute($context["country_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()))) ? ("selected=\"selected\"") : (""));
                echo "
                                        data-global_channels_id=\"";
                // line 530
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "global_channels_id", array()), "html", null, true);
                echo "\"
                                        data-weightage=\"";
                // line 531
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "channel_weightage", array()), "html", null, true);
                echo "\"
                                        data-convenience_lvl=\"";
                // line 532
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "convenience_lvl", array()), "html", null, true);
                echo "\"
                                        data-ufs_food_share=\"";
                // line 533
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "ufs_food_share", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 535
            echo "                        </select>
                    </div>
                </div>
                ";
            // line 538
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 539
                echo "                    <input id=\"channelHidden\" type=\"hidden\" name=\"channel\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()), "html", null, true);
                echo "\">
                ";
            }
            // line 541
            echo "            ";
        }
        // line 542
        echo "
            ";
        // line 543
        if ((twig_length_filter($this->env, (isset($context["sub_channels"]) ? $context["sub_channels"] : null)) > 1)) {
            // line 544
            echo "                <div class=\"form-group\">
                    ";
            // line 545
            echo form_label("Sub Channel", "sub_channel", array("class" => "control-label col-sm-3"));
            echo "
                    ";
            // line 546
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 547
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\" data-parsley-required=\"true\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 552
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 553
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 554
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 556
                echo "                            </select>
                        </div>
                    ";
            } else {
                // line 559
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 564
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 565
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 566
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 568
                echo "                            </select>
                        </div>
                    ";
            }
            // line 571
            echo "                </div>
            ";
        }
        // line 573
        echo "
            <div class=\"form-group\">
                ";
        // line 575
        echo form_label("Channel Weightage", "channel_weightage", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 577
        echo form_input(array("name" => "channel_weightage", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_weightage", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "ChannelWeightage"));
        echo "
                </div>
            </div>

            ";
        // line 581
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 10))) {
            // line 582
            echo "                <div class=\"form-group\">
                    ";
            // line 583
            echo form_label("Real Otm", "real_otm", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 585
            echo form_input(array("name" => "real_otm", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "real_otm", array()), "class" => "form-control", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 589
        echo "
            <div class=\"form-group\">
                ";
        // line 591
        echo form_label("Otm", "otm", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 593
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 5))) {
            // line 594
            echo "                        ";
            echo form_dropdown("otm", array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "N/A" => "N/A"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm", array()), "class=\"form-control\"");
            echo "
                    ";
        } else {
            // line 596
            echo "                        ";
            echo form_input(array("name" => "otm", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm", array()), "class" => "form-control", "readonly" => "readonly"));
            echo "
                    ";
        }
        // line 598
        echo "                </div>
            </div>


        </div>

        <div class=\"form-group\">
            ";
        // line 605
        echo form_label("Key Manager", "key_acct", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 607
        echo form_input(array("name" => "key_acct", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "key_acct", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        ";
        // line 611
        if ((twig_length_filter($this->env, (isset($context["business_types"]) ? $context["business_types"] : null)) > 1)) {
            // line 612
            echo "            <div class=\"form-group\">
                ";
            // line 613
            echo form_label("Business Type", "business_type", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 615
            echo form_dropdown("business_type", (isset($context["business_types"]) ? $context["business_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "business_type", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 619
        echo "
        ";
        // line 626
        echo "
        ";
        // line 628
        echo "        ";
        if ((twig_length_filter($this->env, (isset($context["suppliers"]) ? $context["suppliers"] : null)) > 1)) {
            // line 629
            echo "            <div class=\"form-group\">
                ";
            // line 630
            echo form_label("Primary Supplier", "primary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 632
            echo form_dropdown("primary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "primary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
            // line 637
            echo form_label("Secondary Supplier", "secondary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 639
            echo form_dropdown("secondary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "secondary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 643
        echo "
        <div class=\"form-group\">
            ";
        // line 645
        echo form_label("Trading Days Per Year", "trading_days_per_yr", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 647
        echo form_input(array("name" => "trading_days_per_yr", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "trading_days_per_yr", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-range" => "[1,366]"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 652
        echo form_label("Trading Weeks Per Year", "trading_wks_per_yr", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 654
        echo form_input(array("name" => "trading_wks_per_yr", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "trading_wks_per_yr", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-range" => "[1,53]"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 659
        echo form_label("Turnover", "turnover", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 661
        echo form_input(array("name" => "turnover", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "turnover", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 666
        echo form_label("Capacity", "capacity", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 668
        echo form_input(array("name" => "capacity", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "capacity", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 673
        echo form_label("No Of Outlets", "no_of_outlets", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 675
        echo form_input(array("name" => "no_of_outlets", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "no_of_outlets", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        ";
        // line 680
        echo "        ";
        // line 681
        echo "        ";
        // line 682
        echo "        ";
        // line 683
        echo "        ";
        // line 684
        echo "        ";
        // line 685
        echo "        ";
        // line 686
        echo "        ";
        // line 687
        echo "        ";
        // line 688
        echo "        ";
        // line 689
        echo "        ";
        // line 690
        echo "
        <div class=\"form-group\">
            ";
        // line 692
        echo form_label("Meals Per Day", "meals_per_day", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 694
        echo form_input(array("name" => "meals_per_day", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "meals_per_day", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 699
        echo form_label("Selling Price", "selling_price", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 701
        echo form_input(array("name" => "selling_price", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "selling_price", array()), "class" => "form-control", "data-parsley-type" => "number", "id" => "SellingInput"));
        // line 704
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 709
        echo form_label("Budget Price", "budget_price", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 711
        echo form_input(array("name" => "budget_price", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "budget_price", array()), "class" => "form-control", "data-parsley-type" => "number", "id" => "BudgetInput"));
        // line 714
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 719
        echo form_label("Food Cost", "food_cost", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 721
        echo form_input(array("name" => "food_cost", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_cost", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 726
        echo form_label("Best Time To Call", "best_time_to_call", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 728
        echo form_input(array("name" => "best_time_to_call", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "best_time_to_call", array()), "class" => "form-control", "id" => "InputBestTimeCall"));
        // line 731
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 736
        echo form_label("Notes", "notes", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 738
        echo form_input(array("name" => "notes", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "notes", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 743
        echo form_label("Latitude", "latitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 745
        echo form_input(array("name" => "latitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "latitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 750
        echo form_label("Longitude", "longitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 752
        echo form_input(array("name" => "longitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "longitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 757
        echo form_label("Display Name", "display_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 759
        echo form_input(array("name" => "display_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "display_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 764
        echo form_label("Registered Name", "registered_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 766
        echo form_input(array("name" => "registered_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "registered_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 771
        echo form_label("Opening Hours", "opening_hours", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-3\">
                ";
        // line 773
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_from", array()), "class" => "form-control timepicker"));
        // line 775
        echo "
            </div>
            <div class=\"col-sm-3\">
                ";
        // line 778
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_to", array()), "class" => "form-control timepicker"));
        // line 780
        echo "
            </div>
        </div>

        ";
        // line 799
        echo "
        <div class=\"form-group\">
            ";
        // line 801
        echo form_label("Ufs Share", "ufs_share", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 803
        echo form_input(array("name" => "ufs_share", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly"));
        echo "
            </div>
        </div>

        ";
        // line 807
        if ((twig_length_filter($this->env, (isset($context["customers_types"]) ? $context["customers_types"] : null)) > 1)) {
            // line 808
            echo "            <div class=\"form-group\">
                ";
            // line 809
            echo form_label("Customers Types", "customers_types_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 811
            echo form_dropdown("customers_types_id", (isset($context["customers_types"]) ? $context["customers_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "customers_types_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 815
        echo "        <div class=\"form-group\">
            ";
        // line 816
        echo form_label("Buy Frequency", "buy_frequency", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                <select name=\"buy_frequency\" class=\"form-control\" id=\"ChannelSelect\">
                    <option value=\"0\" data-weightage=\"\">- Select -</option>
                    <option value=\"1\" ";
        // line 820
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 1)) ? ("selected=\"selected\"") : (""));
        echo " >Once a month
                    </option>
                    <option value=\"2\" ";
        // line 822
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 2)) ? ("selected=\"selected\"") : (""));
        echo " >Twice a month
                    </option>
                    <option value=\"3\" ";
        // line 824
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 3)) ? ("selected=\"selected\"") : (""));
        echo " >Three times a
                        month
                    </option>
                    <option value=\"4\" ";
        // line 827
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 4)) ? ("selected=\"selected\"") : (""));
        echo " >Once a week
                    </option>
                </select>
            </div>
        </div>

        ";
        // line 833
        if ((twig_length_filter($this->env, (isset($context["channel_groups"]) ? $context["channel_groups"] : null)) > 1)) {
            // line 834
            echo "            <div class=\"form-group\">
                ";
            // line 835
            echo form_label("Channel Groups", "channel_groups_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 837
            echo form_dropdown("channel_groups_id", (isset($context["channel_groups"]) ? $context["channel_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 841
        echo "
        ";
        // line 842
        if ((twig_length_filter($this->env, (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null)) > 1)) {
            // line 843
            echo "            <div class=\"form-group\">
                ";
            // line 844
            echo form_label("Cuisine Groups", "cuisine_groups_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 846
            echo form_dropdown("cuisine_groups_id", (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "cuisine_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 850
        echo "        ";
        // line 851
        echo "        ";
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) != 3)) {
            // line 852
            echo "            <div class=\"form-group\">
                ";
            // line 853
            echo form_label("Meals / Days", "meals_per_day_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 855
            echo form_input(array("name" => "meals_per_day_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "meals_per_day_new", array()), "class" => "form-control fetch-food fetch-otm", "data-parsley-type" => "number"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 859
            echo form_label("Avarage Price / Meal", "selling_price_per_meal_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 861
            echo form_input(array("name" => "selling_price_per_meal_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "selling_price_per_meal_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 865
            echo form_label("Weeks / Year", "weeks_per_year_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 867
            echo form_input(array("name" => "weeks_per_year_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "weeks_per_year_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,52]"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 871
            echo form_label("Days / Week", "days_per_week_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 873
            echo form_input(array("name" => "days_per_week_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "days_per_week_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,7]"));
            echo "
                </div>
            </div>
            ";
            // line 876
            echo form_input(array("name" => "food_turnover_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_turnover_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 878
            echo form_label("Assumption: Food Cost %", "assumption_food_cost_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 880
            echo form_input(array("name" => "assumption_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "assumption_food_cost_new", array()), "class" => "form-control fetch-otm fetch-food-purchase", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "FoodCostNew"));
            echo "
                </div>
            </div>
            ";
            // line 883
            echo form_input(array("name" => "food_purchase_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_purchase_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 885
            echo form_label("Assumption: potential UFS Share of Food Cost %", "assumption_ufs_share_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 887
            echo form_input(array("name" => "assumption_ufs_share_new", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control id-ufs-share", "data-parsley-type" => "number", "readonly" => "readonly"));
            echo "
                </div>
            </div>
            ";
            // line 890
            echo form_input(array("name" => "ufs_share_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ufs_share_food_cost_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            ";
            // line 891
            echo form_input(array("name" => "otm_potential_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_potential_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 893
            echo form_label("Convenience Factor", "convenience_factor_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 895
            $context["au_nz"] = array(0 => 2, 1 => 9);
            // line 896
            echo "                    ";
            $context["mer"] = array(0 => 12, 1 => 13, 2 => 14, 3 => 15, 4 => 16, 5 => 17);
            // line 897
            echo "                    ";
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
                // line 898
                echo "                        ";
                echo form_input(array("name" => "convenience_factor_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class" => "form-control fetch-otm convenience_sp_country", "id" => "convenienceFactor", "data-parsley-type" => "number", "readonly" => "readonly"));
                echo "
                    ";
            } elseif (twig_in_filter($this->getAttribute(            // line 899
(isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["mer"]) ? $context["mer"] : null))) {
                // line 900
                echo "                        ";
                echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1", "1.5" => "1.5"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm\" id=\"convenienceFactor\"");
                echo "
                    ";
            } else {
                // line 902
                echo "                        ";
                echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm convenience_other_country\" id=\"convenienceFactor\"");
                echo "
                    ";
            }
            // line 904
            echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 907
            echo form_label("OTM New", "otm_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 909
            echo form_input(array("name" => "otm_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_new", array()), "class" => "form-control", "readonly" => "readonly", "id" => "OTMNew"));
            echo "
                </div>
            </div>
        ";
        }
        // line 913
        echo "    </div>
    </div>

    ";
        // line 916
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/chains/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1607 => 916,  1602 => 913,  1595 => 909,  1590 => 907,  1585 => 904,  1579 => 902,  1573 => 900,  1571 => 899,  1566 => 898,  1563 => 897,  1560 => 896,  1558 => 895,  1553 => 893,  1548 => 891,  1544 => 890,  1538 => 887,  1533 => 885,  1528 => 883,  1522 => 880,  1517 => 878,  1512 => 876,  1506 => 873,  1501 => 871,  1494 => 867,  1489 => 865,  1482 => 861,  1477 => 859,  1470 => 855,  1465 => 853,  1462 => 852,  1459 => 851,  1457 => 850,  1450 => 846,  1445 => 844,  1442 => 843,  1440 => 842,  1437 => 841,  1430 => 837,  1425 => 835,  1422 => 834,  1420 => 833,  1411 => 827,  1405 => 824,  1400 => 822,  1395 => 820,  1388 => 816,  1385 => 815,  1378 => 811,  1373 => 809,  1370 => 808,  1368 => 807,  1361 => 803,  1356 => 801,  1352 => 799,  1346 => 780,  1344 => 778,  1339 => 775,  1337 => 773,  1332 => 771,  1324 => 766,  1319 => 764,  1311 => 759,  1306 => 757,  1298 => 752,  1293 => 750,  1285 => 745,  1280 => 743,  1272 => 738,  1267 => 736,  1260 => 731,  1258 => 728,  1253 => 726,  1245 => 721,  1240 => 719,  1233 => 714,  1231 => 711,  1226 => 709,  1219 => 704,  1217 => 701,  1212 => 699,  1204 => 694,  1199 => 692,  1195 => 690,  1193 => 689,  1191 => 688,  1189 => 687,  1187 => 686,  1185 => 685,  1183 => 684,  1181 => 683,  1179 => 682,  1177 => 681,  1175 => 680,  1168 => 675,  1163 => 673,  1155 => 668,  1150 => 666,  1142 => 661,  1137 => 659,  1129 => 654,  1124 => 652,  1116 => 647,  1111 => 645,  1107 => 643,  1100 => 639,  1095 => 637,  1087 => 632,  1082 => 630,  1079 => 629,  1076 => 628,  1073 => 626,  1070 => 619,  1063 => 615,  1058 => 613,  1055 => 612,  1053 => 611,  1046 => 607,  1041 => 605,  1032 => 598,  1026 => 596,  1020 => 594,  1018 => 593,  1013 => 591,  1009 => 589,  1002 => 585,  997 => 583,  994 => 582,  992 => 581,  985 => 577,  980 => 575,  976 => 573,  972 => 571,  967 => 568,  957 => 566,  950 => 565,  946 => 564,  939 => 559,  934 => 556,  924 => 554,  917 => 553,  913 => 552,  906 => 547,  904 => 546,  900 => 545,  897 => 544,  895 => 543,  892 => 542,  889 => 541,  883 => 539,  881 => 538,  876 => 535,  866 => 533,  862 => 532,  858 => 531,  854 => 530,  847 => 529,  843 => 528,  838 => 526,  833 => 524,  828 => 523,  823 => 520,  820 => 519,  818 => 518,  815 => 517,  810 => 515,  807 => 514,  805 => 513,  801 => 511,  795 => 509,  789 => 507,  787 => 506,  782 => 504,  774 => 499,  769 => 497,  762 => 493,  757 => 491,  754 => 490,  747 => 486,  742 => 484,  739 => 483,  737 => 482,  730 => 478,  725 => 476,  717 => 471,  712 => 469,  704 => 464,  699 => 462,  691 => 457,  686 => 455,  678 => 450,  673 => 448,  665 => 443,  660 => 441,  656 => 439,  648 => 436,  642 => 434,  639 => 433,  633 => 431,  631 => 430,  626 => 428,  623 => 427,  619 => 426,  616 => 425,  609 => 394,  604 => 392,  596 => 387,  591 => 385,  583 => 380,  578 => 378,  570 => 373,  565 => 371,  557 => 366,  552 => 364,  544 => 359,  539 => 357,  535 => 355,  528 => 351,  523 => 349,  520 => 348,  518 => 347,  515 => 346,  509 => 342,  504 => 340,  500 => 338,  496 => 337,  489 => 333,  486 => 332,  484 => 331,  477 => 327,  473 => 326,  468 => 324,  460 => 319,  455 => 317,  447 => 312,  442 => 310,  435 => 306,  431 => 305,  426 => 303,  419 => 299,  414 => 297,  406 => 292,  401 => 290,  393 => 285,  388 => 283,  376 => 273,  374 => 272,  364 => 264,  362 => 263,  361 => 262,  360 => 261,  357 => 260,  354 => 259,  347 => 255,  341 => 253,  338 => 252,  326 => 242,  322 => 240,  320 => 239,  317 => 238,  306 => 229,  304 => 228,  265 => 192,  261 => 191,  256 => 188,  221 => 155,  209 => 145,  207 => 144,  180 => 120,  138 => 80,  134 => 78,  131 => 77,  127 => 75,  124 => 74,  121 => 73,  119 => 72,  110 => 65,  106 => 63,  104 => 62,  74 => 35,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
