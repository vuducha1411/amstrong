<?php

/* ami/webshop/modal.html.twig */
class __TwigTemplate_96ce65ca73fc85d7eb837b6e517fed38eadf4c1d767b6e38e7c9be44b7c63345 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal bs-modal-md\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-md\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\">Sync Webshop</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 24
        echo "                <div class=\"col-md-12 form-group\" id=\"webshop-daterange\">
                    ";
        // line 25
        echo form_label("Select Date Range", "", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"input-daterange input-group\" id=\"datepicker\">
                        <input type=\"text\" class=\"input-sm form-control\" name=\"date_from\" id=\"date_from\" />
                        <span class=\"input-group-addon\">to</span>
                        <input type=\"text\" class=\"input-sm form-control\" name=\"date_to\" id=\"date_to\"/>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" onclick=\"downloadWebshop('";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["session_id"]) ? $context["session_id"] : null), "html", null, true);
        echo "')\" class=\"btn btn-primary\" id=\"btnDownload\">Start Download</button>
            </div>
        </div><!-- /.modal-content -->
        <div id=\"loading\" align=\"center\"><span style=\"margin-top: 130px; display: block\" id=\"message\"></span></div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
";
        // line 41
        echo "<div class=\"modal bs-modal-sm\" id=\"successModal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\">Sync Webshop</h4>
            </div>
            <div class=\"modal-body\">
                Import Successful
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-primary\" onclick=\"hideModal()\">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
";
        // line 58
        echo "<div class=\"modal bs-modal-sm\" id=\"warningModal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-primary\" onclick=\"hideModal()\">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->";
    }

    public function getTemplateName()
    {
        return "ami/webshop/modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 58,  53 => 41,  44 => 34,  32 => 25,  29 => 24,  19 => 1,);
    }
}
