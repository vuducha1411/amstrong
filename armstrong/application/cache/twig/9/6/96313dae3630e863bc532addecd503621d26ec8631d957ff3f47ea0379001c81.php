<?php

/* ami/components/uploader_image.html.twig */
class __TwigTemplate_96313dae3630e863bc532addecd503621d26ec8631d957ff3f47ea0379001c81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["folder_image"] = ((((isset($context["entry_type"]) ? $context["entry_type"] : null) == "promotions")) ? ("promo_image") : ("image"));
        // line 2
        $context["folder_thumb"] = ((((isset($context["entry_type"]) ? $context["entry_type"] : null) == "promotions")) ? ("") : ("thumb/"));
        // line 3
        echo "<div class=\"form-group\" id=\"imageUpload\" data-button=\"uploader_image_pickfiles\" data-container=\"uploader_image\"
     data-url=\"";
        // line 4
        echo twig_escape_filter($this->env, site_url("ami/media/upload"), "html", null, true);
        echo "\"
     data-multi=\"true\"
     data-params=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array("entry_type" => (isset($context["entry_type"]) ? $context["entry_type"] : null), "class" => (isset($context["folder_image"]) ? $context["folder_image"] : null))), "html", null, true);
        echo "\"
     data-mimetypes=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array(0 => array("title" => "Image files", "extensions" => "jpg,jpeg,gif,png"))), "html", null, true);
        echo "\">
    ";
        // line 8
        echo form_label("Image", "inputImages", array("class" => "control-label col-sm-3"));
        echo "
    <div id=\"uploader_image\" class=\"controls uploader col-sm-6\">
        <div class=\"filelist\"></div>
        <div class=\"upload-actions\">
            <a class=\"btn btn-default pick-gallery Tooltip\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url(((("ami/media/view/" . (isset($context["folder_image"]) ? $context["folder_image"] : null)) . "/") . (isset($context["entry_type"]) ? $context["entry_type"] : null))), "html", null, true);
        echo "\"
               data-toggle=\"ajaxModal\" title=\"Select from Media\"><i class=\"fa fa-photo\"></i> Media</a>&nbsp;
            <a class=\"btn btn-default btn-upload\" id=\"uploader_image_pickfiles\" href=\"javascript:;\"><i
                        class=\"fa fa-upload\"></i> Upload file</a>
        </div>
        <div class=\"uploaded row\" style=\"margin-top: 10px\">
            ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 19
            echo "                <div class=\"col-md-4\">
                    <div class=\"thumbnail\">
                        <img src=\"";
            // line 21
            echo twig_escape_filter($this->env, (((((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["folder_image"]) ? $context["folder_image"] : null)) . "/") . (isset($context["MEDIA_FOLDER"]) ? $context["MEDIA_FOLDER"] : null)) . (isset($context["folder_thumb"]) ? $context["folder_thumb"] : null)) . $this->getAttribute($context["image"], "path", array())), "html", null, true);
            echo "\"
                             style=\"width: 100%; height: 138px;\" class=\"img-responsive\">

                        <div class=\"caption\">
                            <span class=\"label label-primary\"
                                  title=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "filename_old", array()), "html", null, true);
            echo "\">";
            echo wordTrim($this->getAttribute($context["image"], "filename_old", array()), 15);
            echo "</span>
                            <ul class=\"list-inline\" style=\"margin: 10px 0 0 0;\">
                                <a href=\"#ModalView\" data-src=\"";
            // line 28
            echo twig_escape_filter($this->env, ((((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["folder_image"]) ? $context["folder_image"] : null)) . "/") . (isset($context["MEDIA_FOLDER"]) ? $context["MEDIA_FOLDER"] : null)) . $this->getAttribute($context["image"], "path", array())), "html", null, true);
            echo "\"
                                   class=\"btn btn-xs btn-default modal-view\" data-toggle=\"modal\"><i
                                            class=\"fa fa-eye\"></i> View</a>
                                <a href=\"#\" data-id=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "class", array()), "html", null, true);
            echo "\"
                                   class=\"btn btn-xs btn-default pull-right delete-media\"><i class=\"fa fa-close\"></i>
                                    Delete</a>
                            </ul>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/uploader_image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 39,  84 => 31,  78 => 28,  71 => 26,  63 => 21,  59 => 19,  55 => 18,  46 => 12,  39 => 8,  35 => 7,  31 => 6,  26 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
