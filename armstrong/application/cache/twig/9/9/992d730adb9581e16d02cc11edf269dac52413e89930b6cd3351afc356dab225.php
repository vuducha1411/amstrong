<?php

/* ami/contact_strategy_daterange/edit.html.twig */
class __TwigTemplate_992d730adb9581e16d02cc11edf269dac52413e89930b6cd3351afc356dab225 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/contact_strategy_daterange/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "    ";
        echo form_open(site_url("ami/contact_strategy_daterange/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Contact Strategy</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 44
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 46
        echo html_btn(site_url("ami/contact_strategy_daterange"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 47
        if ($this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "id", array())) {
            // line 48
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/contact_strategy_daterange/delete/" . $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 51
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 61
        echo form_label("From", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 63
        echo form_input(array("name" => "from_date", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "from_date", array()), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 67
        echo form_label("To", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 69
        echo form_input(array("name" => "to_date", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "to_date", array()), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 70
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 71
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 72
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 75
        echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 78
        echo form_label("OTM A Customers Visited", "otm_A_customers_visited", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 80
        echo form_input(array("name" => "otm_A_customers_visited", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_A_customers_visited", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 82
        echo form_label("In", "", array("class" => "control-label col-sm-1"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 84
        echo form_input(array("name" => "otm_A_customers_visited_in_weeks", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_A_customers_visited_in_weeks", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 86
        echo form_label("Weeks", "", array("class" => "control-label"));
        echo "
            </div>
            <div class=\"form-group\">
                ";
        // line 89
        echo form_label("OTM B Customers Visited", "otm_B_customers_visited", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 91
        echo form_input(array("name" => "otm_B_customers_visited", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_B_customers_visited", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 93
        echo form_label("In", "", array("class" => "control-label col-sm-1"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 95
        echo form_input(array("name" => "otm_B_customers_visited_in_weeks", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_B_customers_visited_in_weeks", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 97
        echo form_label("Weeks", "", array("class" => "control-label"));
        echo "
            </div>
            <div class=\"form-group\">
                ";
        // line 100
        echo form_label("OTM C Customers Visited", "otm_C_customers_visited", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 102
        echo form_input(array("name" => "otm_C_customers_visited", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_C_customers_visited", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 104
        echo form_label("In", "", array("class" => "control-label col-sm-1"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 106
        echo form_input(array("name" => "otm_C_customers_visited_in_weeks", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_C_customers_visited_in_weeks", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 108
        echo form_label("Weeks", "", array("class" => "control-label"));
        echo "
            </div>
            <div class=\"form-group\">
                ";
        // line 111
        echo form_label("OTM D Customers Visited", "otm_D_customers_visited", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 113
        echo form_input(array("name" => "otm_D_customers_visited", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_D_customers_visited", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 115
        echo form_label("In", "", array("class" => "control-label col-sm-1"));
        echo "
                <div class=\"col-sm-2\">
                    ";
        // line 117
        echo form_input(array("name" => "otm_D_customers_visited_in_weeks", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "otm_D_customers_visited_in_weeks", array()), "class" => "form-control", "min" => 1, "data-parsley-required" => "true", "data-parsley-type" => "digits"));
        echo "
                </div>
                ";
        // line 119
        echo form_label("Weeks", "", array("class" => "control-label"));
        echo "
            </div>
            ";
        // line 121
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["contact_strategy_daterange"]) ? $context["contact_strategy_daterange"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 126
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/contact_strategy_daterange/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 126,  277 => 121,  272 => 119,  267 => 117,  262 => 115,  257 => 113,  252 => 111,  246 => 108,  241 => 106,  236 => 104,  231 => 102,  226 => 100,  220 => 97,  215 => 95,  210 => 93,  205 => 91,  200 => 89,  194 => 86,  189 => 84,  184 => 82,  179 => 80,  174 => 78,  169 => 75,  163 => 72,  160 => 71,  158 => 70,  154 => 69,  149 => 67,  142 => 63,  137 => 61,  125 => 51,  118 => 48,  116 => 47,  112 => 46,  107 => 44,  94 => 35,  91 => 34,  85 => 31,  80 => 30,  77 => 29,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
