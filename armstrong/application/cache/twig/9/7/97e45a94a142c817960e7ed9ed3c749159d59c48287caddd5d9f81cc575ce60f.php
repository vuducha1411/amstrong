<?php

/* ami/minitools/migration.html.twig */
class __TwigTemplate_97e45a94a142c817960e7ed9ed3c749159d59c48287caddd5d9f81cc575ce60f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/minitools/migration.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 12
    public function block_css($context, array $blocks = array())
    {
        // line 13
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "
    ";
        // line 19
        echo form_open(site_url("ami/kpi/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "


    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"row row sticky sticky-h1 bg-white\">
                    <div class=\"col-lg-12\">
                        <div class=\"page-header nm\">
                            <h1 class=\"pull-left\"></h1>
                            <div class=\"text-right\">
                                <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                                    <a class=\"btn btn-sm btn-warning Tooltip\" id=\"ImportPickfiles\" href=\"javascript:;\"
                                       data-url=\"";
        // line 32
        echo twig_escape_filter($this->env, site_url("/ami/minitools/import"), "html", null, true);
        echo "?exelname=";
        echo twig_escape_filter($this->env, (isset($context["exelname"]) ? $context["exelname"] : null), "html", null, true);
        echo "\" title=\"Max records 10000 (only csv)\">
                                        <i class=\"fa fw fa-upload\"></i> Import</a>
                                    <a class=\"btn btn-sm btn-primary\" id=\"getTemplateimport\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "res/up/minitools/template/Migration_TW_PULL_to_PUSH.csv\"
                                       target=\"_blank\" title=\"Get template import\"><i
                                                class=\"fa fw fa-download\"></i>Get Template</a>
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class=\"panel-body\">
                    ";
        // line 45
        if ((isset($context["customers"]) ? $context["customers"] : null)) {
            // line 46
            echo "                        <table class=\"table\">
                            <tr>
                                <th>STT</th>
                                <th>Customer</th>
                                <th>Wholesaler</th>
                            </tr>
                        ";
            // line 52
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["customers"]) ? $context["customers"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["table"]) {
                // line 53
                echo "                            <tr>
                                <td>";
                // line 54
                echo twig_escape_filter($this->env, ($context["key"] + 1), "html", null, true);
                echo "</td>
                                <td>";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 0, array(), "array"), "html", null, true);
                echo "</td>
                                <td>";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 1, array(), "array"), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['table'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                        </table>
                        <ul>
                        ";
            // line 61
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tables"]) ? $context["tables"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["table"]) {
                // line 62
                echo "                            <li><a class=\"btn\" href=\"";
                echo twig_escape_filter($this->env, (site_url("ami/minitools/runmigration?table=") . $context["key"]), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</a></li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['table'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                            <li><a class=\"btn\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/minitools/movetfo"), "html", null, true);
            echo "\" >TFO *</a></li>
                        </ul>
                        ";
        } else {
            // line 67
            echo "                        <p>Please upload file exel</p>
                    ";
        }
        // line 69
        echo "                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 78
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/minitools/migration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 78,  178 => 69,  174 => 67,  167 => 64,  156 => 62,  152 => 61,  148 => 59,  139 => 56,  135 => 55,  131 => 54,  128 => 53,  124 => 52,  116 => 46,  114 => 45,  100 => 34,  93 => 32,  77 => 19,  74 => 18,  71 => 17,  65 => 14,  60 => 13,  57 => 12,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
