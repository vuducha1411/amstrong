<?php

/* ami/sampling/edit.html.twig */
class __TwigTemplate_98dbad13d36d5d1dfa40747180f7c70ced71a48bbc03be35c569450ddfa2a70b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/sampling/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script>
        function validateProductInput(){
            var validate = false;
            \$.each(\$('select[name=\"sku_number_\"]'), function(){
                if(\$(this).val() !== ''){
                    validate = true;
                }
            });
            return validate;
        }
        \$(function () {
            var counter = \$('.InputProductId').length - 1;
            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        \$clone = \$(this).closest('.sku-base').clone();
                \$clone.find('.hide').removeClass('hide').show();

                if (\$this.hasClass('clone-add')) {
                    \$new = \$clone.clone();
                    counter ++;
                    \$new.find('input[name], select[name]').each(function () {
                        var \$this = \$(this);
                        \$this.attr('name', \$this.attr('name').replace(/\\[(\\d+)\\]/, '[' + counter + ']'));

                        if (\$this.hasClass('product-tokenfield') || \$this.is('input:text')) {
                            \$this.val('');
                        }
                        else {
                            \$this.val(0);
                        }
                    });
                    \$new.find('select[name=\"sku_number_\"]').attr('data-parsley-required', 'false').removeClass('parsley-error');
                    \$new.find('ul.parsley-errors-list').remove();
                    \$new.appendTo(\$('#sku-base-block'));
                    \$new.find('.item-type-input').trigger('change');
                }

                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.sku-base').remove();
                    var validate = validateProductInput();
                    if(!validate){
                        \$('select[name=\"sku_number_\"]').first().attr('data-parsley-required', 'true');
                    }
                }
            });

            \$('#salesperson_list').change(function () {
                var \$this = \$(this),
                        armstrong_2_salespersons_id = \$this.val();
                \$('#call_record').html('');
                \$.ajax({
                    url: \"";
        // line 57
        echo twig_escape_filter($this->env, site_url("ami/sampling/getCustomerFromSalesperson"), "html", null, true);
        echo "\",
                    data: {armstrong_2_salespersons_id: armstrong_2_salespersons_id},
                    type: 'POST',
                    dataType: 'html',
                    success: function (res) {
                        \$('#customer_list').html(res).trigger('change');
                    }
                });
            });
            \$('#customer_list').change(function () {
                var \$this = \$(this),
                        armstrong_2_customers_id = \$this.val(),
                        armstrong_2_salespersons_id = \$('#salesperson_list').val();
                \$.ajax({
                    url: \"";
        // line 71
        echo twig_escape_filter($this->env, site_url("ami/sampling/getCallFromSalesCus"), "html", null, true);
        echo "\",
                    data: {
                        armstrong_2_salespersons_id: armstrong_2_salespersons_id,
                        armstrong_2_customers_id: armstrong_2_customers_id
                    },
                    type: 'POST',
                    dataType: 'html',
                    success: function (res) {
                        \$('#call_record_list').html(res);
                        if(res){
                            \$('#call_record_list').removeClass('parsley-error');
                            \$('#call_record_list').closest('div').find('ul.parsley-errors-list').hide();
                        }else{
                            if(\$('#call_record_list').closest('div').find('ul.parsley-errors-list li').length > 0){
                                \$('#call_record_list').addClass('parsley-error');
                            }
                            \$('#call_record_list').closest('div').find('ul.parsley-errors-list').show();
                        }
                    }
                });
            });
            \$(document).on('change', '.item-type-input', function () {
                var \$this = \$(this),
                        productSelect = \$this.closest('.sku-base').find('select[name=\"sku_number_\"]'),
                        item_type = \$this.val();
                \$.ajax({
                    url: \"";
        // line 97
        echo twig_escape_filter($this->env, site_url("ami/sampling/getProductByItemType"), "html", null, true);
        echo "\",
                    data: {
                        item_type: item_type
                    },
                    type: 'POST',
                    dataType: 'html',
                    success: function (res) {
                        productSelect.html(res);
                        var validate = validateProductInput(),
                                target = \$('select[name=\"sku_number_\"]').first();
                        if(!validate){
                            target.attr('data-parsley-required', 'true');
                        }else{
                            target.attr('data-parsley-required', 'false').removeClass('parsley-error');
                            target.closest('div').find('ul.parsley-errors-list').remove();
                        }
                    }
                });
            });

            \$(document).on('change', 'select[name=\"sku_number_\"]', function (e) {
                var \$this = \$(this),
                        input_hidden = \$this.closest('div').find('input[type=\"hidden\"]'),
                        target = \$('select[name=\"sku_number_\"]').first();
                        validate = validateProductInput();
                input_hidden.val(\$this.val());
                if(!validate){
                    target.attr('data-parsley-required', 'true');
                }else{
                    target.attr('data-parsley-required', 'false').removeClass('parsley-error');
                    target.closest('div').find('ul.parsley-errors-list').remove();
                }
            })
        });
    </script>
";
    }

    // line 134
    public function block_css($context, array $blocks = array())
    {
        // line 135
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style>
        .sub-input-item {
            width: 13%;
            display: inline-block;
        }

        .feedback-input {
            width: 37%;
        }
    </style>
";
    }

    // line 148
    public function block_content($context, array $blocks = array())
    {
        // line 149
        echo "
    ";
        // line 150
        echo form_open(site_url("ami/sampling/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_sampling_id" => $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Sampling</h1>
                <div class=\"text-right\">
                    ";
        // line 157
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/sampling/edit.html.twig", 157)->display(array_merge($context, array("url" => "ami/sampling", "id" => $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array()), "permission" => "sampling")));
        // line 158
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 166
        if ($this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array())) {
            // line 167
            echo "                <div class=\"form-group\">
                    ";
            // line 168
            echo form_label("Sampling ID", "armstrong_2_sampling_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 170
            echo form_input(array("name" => "armstrong_2_sampling_id", "value" => $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array()), "class" => "form-control", "data-parsley-required" => "true", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 174
        echo "            <div class=\"form-group\">
                ";
        // line 175
        echo form_label("Salespersons", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 177
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_salespersons_id", array()), "id=\"salesperson_list\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 181
        echo form_label("Customers", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 183
        echo form_dropdown("armstrong_2_customers_id", (isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_customers_id", array()), "id=\"customer_list\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 187
        echo form_label("Call Records", "armstrong_2_call_records_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 189
        echo form_dropdown("armstrong_2_call_records_id", (isset($context["call_records"]) ? $context["call_records"] : null), $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_call_records_id", array()), "id=\"call_record_list\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            ";
        // line 193
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 194
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "sampling_array", array()));
        foreach ($context['_seq'] as $context["key"] => $context["sampling_array"]) {
            // line 195
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 196
            echo form_label("Item type", "item_type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 198
            echo form_dropdown((("products[" . $context["key"]) . "][item_type]"), (isset($context["item_type"]) ? $context["item_type"] : null), $this->getAttribute($context["sampling_array"], "item_type", array()), "class=\"form-control item-type-input\"");
            echo "
                    </div>
                    ";
            // line 200
            echo form_label("Product", "products", array("class" => "control-label col-sm-1"));
            echo "
                    <div class=\"col-sm-3\">
                        ";
            // line 202
            if (($this->getAttribute($context["sampling_array"], "item_type", array()) == 1)) {
                // line 203
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["recipes"]) ? $context["recipes"] : null), $this->getAttribute($context["sampling_array"], "sku_number", array()), "class='form-control product-tokenfield'");
                echo "
                        ";
            } elseif (($this->getAttribute(            // line 204
$context["sampling_array"], "item_type", array()) == 2)) {
                // line 205
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["benefits"]) ? $context["benefits"] : null), $this->getAttribute($context["sampling_array"], "sku_number", array()), "class='form-control product-tokenfield'");
                echo "
                        ";
            } elseif (($this->getAttribute(            // line 206
$context["sampling_array"], "item_type", array()) == 3)) {
                // line 207
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["products_video"]) ? $context["products_video"] : null), $this->getAttribute($context["sampling_array"], "sku_number", array()), "class='form-control product-tokenfield'");
                echo "
                        ";
            } elseif (($this->getAttribute(            // line 208
$context["sampling_array"], "item_type", array()) == 4)) {
                // line 209
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["promotions"]) ? $context["promotions"] : null), $this->getAttribute($context["sampling_array"], "sku_number", array()), "class='form-control product-tokenfield'");
                echo "
                        ";
            } else {
                // line 211
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["products"]) ? $context["products"] : null), $this->getAttribute($context["sampling_array"], "sku_number", array()), "class='form-control product-tokenfield'");
                echo "
                        ";
            }
            // line 213
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 214
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"";
            // line 215
            echo twig_escape_filter($this->env, $this->getAttribute($context["sampling_array"], "sku_number", array()), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add hide\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i></button>
                    </div>

                    <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
                        Feedback ";
            // line 223
            echo form_input(array("name" => (("products[" . $context["key"]) . "][feedBack]"), "value" => $this->getAttribute($context["sampling_array"], "feedBack", array()), "class" => "form-control sub-input-item feedback-input"));
            echo "
                        &nbsp;
                        Type ";
            // line 225
            echo form_dropdown((("products[" . $context["key"]) . "][type]"), (isset($context["type"]) ? $context["type"] : null), $this->getAttribute($context["sampling_array"], "type", array()), "class=\"form-control sub-input-item\"");
            echo "
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['sampling_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 229
        echo "            ";
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 230
            echo "                ";
            $context["counter"] = twig_length_filter($this->env, $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "sampling_array", array()));
            // line 231
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 232
            echo form_label("Item type", "item_type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 234
            echo form_dropdown((("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][item_type]"), (isset($context["item_type"]) ? $context["item_type"] : null), $this->getAttribute((isset($context["sampling_array"]) ? $context["sampling_array"] : null), "item_type", array()), "class=\"form-control item-type-input\"");
            echo "
                    </div>
                    ";
            // line 236
            echo form_label("Product", "products", array("class" => "control-label col-sm-1"));
            echo "
                    <div class=\"col-sm-3\">
                        ";
            // line 238
            if ($this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "armstrong_2_sampling_id", array())) {
                // line 239
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["products"]) ? $context["products"] : null), "", "class='form-control product-tokenfield'");
                echo "
                        ";
            } else {
                // line 241
                echo "                            ";
                echo form_dropdown("sku_number_", (isset($context["products"]) ? $context["products"] : null), "", "class='form-control product-tokenfield' data-parsley-required='true'");
                echo "
                        ";
            }
            // line 243
            echo "                        <input type=\"hidden\" name=\"products[";
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                    <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
                        Feedback ";
            // line 251
            echo form_input(array("name" => (("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][feedBack]"), "value" => $this->getAttribute((isset($context["sampling_array"]) ? $context["sampling_array"] : null), "feedBack", array()), "class" => "form-control sub-input-item feedback-input"));
            echo "
                        &nbsp;
                        Type ";
            // line 253
            echo form_dropdown((("products[" . (isset($context["counter"]) ? $context["counter"] : null)) . "][type]"), (isset($context["type"]) ? $context["type"] : null), $this->getAttribute((isset($context["sampling_array"]) ? $context["sampling_array"] : null), "type", array()), "class=\"form-control sub-input-item\"");
            echo "
                    </div>
                </div>
                <div id=\"sku-base-block\"></div>
            ";
        }
        // line 258
        echo "            <div class=\"form-group\">
                ";
        // line 259
        echo form_label("Comments", "comments", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 261
        echo form_textarea(array("name" => "comments", "value" => $this->getAttribute((isset($context["sampling"]) ? $context["sampling"] : null), "comments", array()), "class" => "form-control", "rows" => 3));
        echo "
                </div>
            </div>
        </div>
    </div>

    ";
        // line 267
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/sampling/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  453 => 267,  444 => 261,  439 => 259,  436 => 258,  428 => 253,  423 => 251,  411 => 243,  405 => 241,  399 => 239,  397 => 238,  392 => 236,  387 => 234,  382 => 232,  379 => 231,  376 => 230,  373 => 229,  363 => 225,  358 => 223,  347 => 215,  343 => 214,  340 => 213,  334 => 211,  328 => 209,  326 => 208,  321 => 207,  319 => 206,  314 => 205,  312 => 204,  307 => 203,  305 => 202,  300 => 200,  295 => 198,  290 => 196,  287 => 195,  282 => 194,  280 => 193,  273 => 189,  268 => 187,  261 => 183,  256 => 181,  249 => 177,  244 => 175,  241 => 174,  234 => 170,  229 => 168,  226 => 167,  224 => 166,  214 => 158,  212 => 157,  202 => 150,  199 => 149,  196 => 148,  179 => 135,  176 => 134,  136 => 97,  107 => 71,  90 => 57,  33 => 4,  30 => 3,  11 => 1,);
    }
}
