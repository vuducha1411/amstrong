<?php

/* ami/kpi_target/edit1.html.twig */
class __TwigTemplate_988900d166e93fcc56c62d1ac1d986b2a306d047187163d92d3885a1b6312f2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/kpi_target/edit1.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script type=\"application/javascript\">

        \$(document).on('change input', '.call_target, .new_grab_target, .new_grip_target, .total_grip_target, .total_grab_target, .kpi_sale_target, .average_call_per_working_day_target, .average_call_per_actual_working_day_target, .pantry_check_target, .sampling_target,   .otm_a_call_frequency_target, .otm_b_call_frequency_target, .otm_a_average_call_time_target, .otm_b_average_call_time_target, .total_tfo_target, .total_tfo_value_target, .average_grab_target, .push_calls_made_target, .push_call_achievement_target, .push_route_plan_coverage_target, .push_tfo_made_target, .push_tfo_value_target, .push_strike_rate_target, .push_new_grab_target, .push_new_grip_target, .push_number_sku_per_ws_target, .push_perfect_store_target, .potential_gripped_target', function () {
            total = \$(this).val();
            sub_val = \$(this).attr('class').split(' ')[1];
            \$('.' + sub_val + '_weeks').val(Math.floor(total / 5));
        });
        \$(document).on('change input', '.call_target_weeks, .new_grab_target_weeks, .new_grip_target_weeks, .total_grip_target, .total_grab_target, .strike_rate_target_weeks, .average_call_per_working_day_target_weeks, .kpi_sale_target_weeks, .average_call_per_actual_working_day_target_weeks, .pantry_check_target_weeks, .sampling_target_weeks, .otm_a_fulfilment_rate_target_weeks, .otm_b_fulfilment_rate_target_weeks, .otm_a_call_frequency_target_weeks, .otm_b_call_frequency_target_weeks, .otm_a_average_call_time_target_weeks, .otm_b_average_call_time_target_weeks, .total_tfo_target_weeks, .total_tfo_value_target_weeks, .average_grab_target_weeks, .push_calls_made_target_weeks, .push_call_achievement_target_weeks, .push_route_plan_coverage_target_weeks, .push_tfo_made_target_weeks, .push_tfo_value_target_weeks, .push_strike_rate_target_weeks, .push_new_grab_target_weeks, .push_new_grip_target_weeks, .push_number_sku_per_ws_target_weeks, .push_perfect_store_target_weeks, .potential_gripped_target_weeks', function () {
            total = 0;
            sub_class = \$(this).attr('class').split(' ')[1];
            total_class = sub_class.substr(0, sub_class.length - 6);
            \$('.' + sub_class).each(function () {
                sub_val = parseInt(\$(this).val()) || 0;
                total += sub_val;
            });
            \$('.' + total_class).val(total);
        });
        \$(document).on('change', '#select_type', function () {
            var \$val = \$(this).val(),
                    call_target_label = \$('input[name=\"kpi_call_target\"]').closest('.form-group').find('label'),
                    call_target_weeks_label = \$('input[name=\"kpi_call_target_weeks[]\"]').closest('.form-group').find('label'),
                    total_tfo_target_label = \$('input[name=\"total_tfo_target\"]').closest('.form-group').find('label'),
                    total_tfo_target_weeks_label = \$('input[name=\"total_tfo_target_weeks[]\"]').closest('.form-group').find('label'),
                    total_tfo_value_target_label = \$('input[name=\"kpi_sale_target\"]').closest('.form-group').find('label'),
                    total_tfo_value_target_weeks_label = \$('input[name=\"kpi_sale_target_weeks[]\"]').closest('.form-group').find('label');
            if (\$val == 'PULL') {
                call_target_label.text(\"Call Target\");
                call_target_weeks_label.text(\"Week's Call Target\");
                total_tfo_target_label.text(\"Total TFO Target\");
                total_tfo_target_weeks_label.text(\"Week's Total TFO Target\");
                total_tfo_value_target_label.text(\"Total TFO value Target\");
                total_tfo_value_target_weeks_label.text(\"Week's Total TFO value Target\");

                \$('.pull_element').show();
                \$('.push_element').hide();
            } else if (\$val == 'PUSH') {
                call_target_label.text(\"Call Made\");
                call_target_weeks_label.text(\"Week's Call Made\");
                total_tfo_target_label.text(\"Tfo Made\");
                total_tfo_target_weeks_label.text(\"Week's Total Made\");
                total_tfo_value_target_label.text(\"TFO value\");
                total_tfo_value_target_weeks_label.text(\"Week's TFO value\");

                \$('.pull_element').hide();
                \$('.push_element').show();
            }
        });

         \$(document).on('change input', '.strike_rate_target, .otm_a_fulfilment_rate_target, .otm_b_fulfilment_rate_target', function () {
            
            total = \$(this).val();
            sub_val = \$(this).attr('class').split(' ')[1];
            \$('.' + sub_val + '_weeks').val(total);
           
        });

        \$(document).on('change', '.fecth_sp', function () {
            var type = \$('#select_type').val(),
                    cr_sp = \"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_salespersons_id", array()), "html", null, true);
        echo "\",
                    salespersonsElement = \$('#salespersons_dropdown');
            \$.ajax({
                url: \"";
        // line 65
        echo twig_escape_filter($this->env, site_url("ami/kpi_target/ajaxGetSalespersons"), "html", null, true);
        echo "\",
                type: 'GET',
                data: {
                    'type': type,
                    'salesperson': cr_sp
                },
                success: function (rs) {
                    salespersonsElement.html(rs);
                }
            });
        });

        \$('button[type=\"submit\"]').click(function (e) {
            e.preventDefault();
            var error = false;
            var percent_class = ['strike_rate_target', 'otm_a_fulfilment_rate_target', 'otm_b_fulfilment_rate_target'];
            \$.each(percent_class, function (k, \$class) {
                var target = \$('.' + \$class),
                        val = target.val();
                if (val > 100) {
                    error = true; 
                    \$.alert({
                        title: 'Alert!',
                        content: 'Strike Rate Target, OTM A fulfilment rate and OTM B fulfilment rate maximum is 100%'
                    });

                    return false;
                }
            });
            if (!error) {
                \$('#page-wrapper form').submit();
            }
        });

        \$(document).ready(function () {
            var \$val = \$('#select_type').val();
            if (\$val == 'PULL') {
                \$('.pull_element').show();
                \$('.push_element').hide();
            } else if (\$val == 'PUSH') {
                \$('.pull_element').hide();
                \$('.push_element').show();
            }

            \$('.fecth_sp').trigger('change');
        });

    </script>
";
    }

    // line 114
    public function block_content($context, array $blocks = array())
    {
        // line 115
        echo "    ";
        if (((isset($context["exists"]) ? $context["exists"] : null) == false)) {
            // line 116
            echo "        ";
            echo form_open(site_url("ami/kpi_target/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
            echo "

        <div class=\"row sticky sticky-h1 bg-white\">
            <div class=\"col-lg-12\">
                <div class=\"page-header nm\">
                    <h1 class=\"pull-left\">KPI Target</h1>

                    <div class=\"text-right\">
                        ";
            // line 124
            $this->loadTemplate("ami/components/form_btn.html.twig", "ami/kpi_target/edit1.html.twig", 124)->display(array_merge($context, array("url" => "ami/kpi_target", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "kpi_target")));
            // line 125
            echo "                    </div>
                    <div class=\"clearfix\"></div>
                </div>
            </div>
        </div>

        <div class=\"row m-t-md\">
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    ";
            // line 134
            echo form_label("Type", "type", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-8\">
                        ";
            // line 136
            echo form_dropdown("type", array("PULL" => "PULL", "PUSH" => "PUSH"), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sp_type", array()), "class=\"form-control fecth_sp\" id=\"select_type\"");
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 141
            echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-8\">
                        ";
            // line 143
            echo form_dropdown("armstrong_2_salespersons_id", array(), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" id=\"salespersons_dropdown\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 148
            echo form_label("Month", "month", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-8\">
                        ";
            // line 150
            echo form_dropdown("month", select_month(), twig_number_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "month", array())), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 155
            echo form_label("Year", "year", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-8\">
                        ";
            // line 157
            echo form_dropdown("year", select_year(2010), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "year", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 163
            echo form_label("Call Target", "kpi_call_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 165
            echo form_input(array("name" => "kpi_call_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target", array())) : (0)), "class" => "form-control call_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Call Target Monthly"));
            // line 169
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 173
            echo form_label("Week's Call Target", "kpi_call_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 175
            echo form_input(array("name" => "kpi_call_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target_weeks", array()), 0, array()), "class" => "form-control call_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 179
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 182
            echo form_input(array("name" => "kpi_call_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target_weeks", array()), 1, array()), "class" => "form-control call_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 186
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 189
            echo form_input(array("name" => "kpi_call_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target_weeks", array()), 2, array()), "class" => "form-control call_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 193
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 196
            echo form_input(array("name" => "kpi_call_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target_weeks", array()), 3, array()), "class" => "form-control call_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 200
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 203
            echo form_input(array("name" => "kpi_call_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_call_target_weeks", array()), 4, array()), "class" => "form-control call_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 207
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 213
            echo form_label("Strike Rate Target (%)", "strike_rate_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 215
            echo form_input(array("name" => "strike_rate_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target", array())) : (0)), "class" => "form-control strike_rate_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Strike Rate Target (%) Monthly"));
            // line 219
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 223
            echo form_label("Week's Strike Rate Target (%)", "strike_rate_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 225
            echo form_input(array("name" => "strike_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target_weeks", array()), 0, array()), "class" => "form-control strike_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01", "readonly" => "readonly"));
            // line 230
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 233
            echo form_input(array("name" => "strike_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target_weeks", array()), 1, array()), "class" => "form-control strike_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02", "readonly" => "readonly"));
            // line 238
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 241
            echo form_input(array("name" => "strike_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target_weeks", array()), 2, array()), "class" => "form-control strike_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03", "readonly" => "readonly"));
            // line 246
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 249
            echo form_input(array("name" => "strike_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target_weeks", array()), 3, array()), "class" => "form-control strike_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04", "readonly" => "readonly"));
            // line 254
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 257
            echo form_input(array("name" => "strike_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "strike_rate_target_weeks", array()), 4, array()), "class" => "form-control strike_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05", "readonly" => "readonly"));
            // line 262
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 268
            echo form_label("Total TFO Target", "total_tfo_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 270
            echo form_input(array("name" => "total_tfo_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target", array())) : (0)), "class" => "form-control total_tfo_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Total TFO Target Monthly"));
            // line 274
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 278
            echo form_label("Week's Total TFO Target", "total_tfo_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 280
            echo form_input(array("name" => "total_tfo_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target_weeks", array()), 0, array()), "class" => "form-control total_tfo_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 284
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 287
            echo form_input(array("name" => "total_tfo_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target_weeks", array()), 1, array()), "class" => "form-control total_tfo_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 291
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 294
            echo form_input(array("name" => "total_tfo_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target_weeks", array()), 2, array()), "class" => "form-control total_tfo_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 298
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 301
            echo form_input(array("name" => "total_tfo_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target_weeks", array()), 3, array()), "class" => "form-control total_tfo_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 305
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 308
            echo form_input(array("name" => "total_tfo_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_tfo_target_weeks", array()), 4, array()), "class" => "form-control total_tfo_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 312
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 318
            echo form_label("Total TFO value Target", "kpi_sale_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 320
            echo form_input(array("name" => "kpi_sale_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target", array())) : (0)), "class" => "form-control kpi_sale_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Total TFO value Target Monthly"));
            // line 324
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 328
            echo form_label("Week's Total TFO value Target", "kpi_sale_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 330
            echo form_input(array("name" => "kpi_sale_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target_weeks", array()), 0, array()), "class" => "form-control kpi_sale_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 334
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 337
            echo form_input(array("name" => "kpi_sale_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target_weeks", array()), 1, array()), "class" => "form-control kpi_sale_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 341
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 344
            echo form_input(array("name" => "kpi_sale_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target_weeks", array()), 2, array()), "class" => "form-control kpi_sale_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 348
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 351
            echo form_input(array("name" => "kpi_sale_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target_weeks", array()), 3, array()), "class" => "form-control kpi_sale_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 355
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 358
            echo form_input(array("name" => "kpi_sale_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_sale_target_weeks", array()), 4, array()), "class" => "form-control kpi_sale_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 362
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 368
            echo form_label("New Grib Target", "kpi_new_grip_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 370
            echo form_input(array("name" => "kpi_new_grip_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target", array()), "class" => "form-control new_grip_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "New Grib Target Monthly"));
            // line 374
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 378
            echo form_label("Week's New Grib Target", "kpi_new_grip_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 380
            echo form_input(array("name" => "kpi_new_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target_weeks", array()), 0, array()), "class" => "form-control new_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 384
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 387
            echo form_input(array("name" => "kpi_new_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target_weeks", array()), 1, array()), "class" => "form-control new_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 391
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 394
            echo form_input(array("name" => "kpi_new_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target_weeks", array()), 2, array()), "class" => "form-control new_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 398
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 401
            echo form_input(array("name" => "kpi_new_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target_weeks", array()), 3, array()), "class" => "form-control new_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 405
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 408
            echo form_input(array("name" => "kpi_new_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grip_target_weeks", array()), 4, array()), "class" => "form-control new_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 412
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    <hr/>
                    ";
            // line 418
            echo form_label("New Grab Target", "kpi_new_grab_target", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-10\">
                        ";
            // line 420
            echo form_input(array("name" => "kpi_new_grab_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target", array()), "class" => "form-control new_grab_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "New Grab Target Monthly"));
            // line 424
            echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
            // line 428
            echo form_label("Week's New Grab Target", "kpi_new_grab_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                    <div class=\"col-sm-2\">
                        ";
            // line 430
            echo form_input(array("name" => "kpi_new_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target_weeks", array()), 0, array()), "class" => "form-control new_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 434
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 437
            echo form_input(array("name" => "kpi_new_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target_weeks", array()), 1, array()), "class" => "form-control new_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 441
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 444
            echo form_input(array("name" => "kpi_new_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target_weeks", array()), 2, array()), "class" => "form-control new_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 448
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 451
            echo form_input(array("name" => "kpi_new_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target_weeks", array()), 3, array()), "class" => "form-control new_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 455
            echo "
                    </div>
                    <div class=\"col-sm-2\">
                        ";
            // line 458
            echo form_input(array("name" => "kpi_new_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "kpi_new_grab_target_weeks", array()), 4, array()), "class" => "form-control new_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 462
            echo "
                    </div>
                </div>

                <div class=\"pull_element\">
                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 469
            echo form_label("Average calls per working day", "average_call_per_working_day_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 471
            echo form_input(array("name" => "average_call_per_working_day_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target", array())) : (0)), "class" => "form-control average_call_per_working_day_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Average calls per working day Monthly"));
            // line 475
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 479
            echo form_label("Week's Average calls per working day", "average_call_per_working_day_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 481
            echo form_input(array("name" => "average_call_per_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target_weeks", array()), 0, array()), "class" => "form-control average_call_per_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 485
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 488
            echo form_input(array("name" => "average_call_per_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target_weeks", array()), 1, array()), "class" => "form-control average_call_per_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 492
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 495
            echo form_input(array("name" => "average_call_per_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target_weeks", array()), 2, array()), "class" => "form-control average_call_per_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 499
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 502
            echo form_input(array("name" => "average_call_per_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target_weeks", array()), 3, array()), "class" => "form-control average_call_per_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 506
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 509
            echo form_input(array("name" => "average_call_per_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_working_day_target_weeks", array()), 4, array()), "class" => "form-control average_call_per_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 513
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 519
            echo form_label("Average calls per actual working day", "average_call_per_actual_working_day_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 521
            echo form_input(array("name" => "average_call_per_actual_working_day_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target", array())) : (0)), "class" => "form-control average_call_per_actual_working_day_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Average calls per actual working day Monthly"));
            // line 525
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 529
            echo form_label("Week's Average calls per actual working day", "average_call_per_actual_working_day_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 531
            echo form_input(array("name" => "average_call_per_actual_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target_weeks", array()), 0, array()), "class" => "form-control average_call_per_actual_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 535
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 538
            echo form_input(array("name" => "average_call_per_actual_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target_weeks", array()), 1, array()), "class" => "form-control average_call_per_actual_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 542
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 545
            echo form_input(array("name" => "average_call_per_actual_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target_weeks", array()), 2, array()), "class" => "form-control average_call_per_actual_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 549
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 552
            echo form_input(array("name" => "average_call_per_actual_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target_weeks", array()), 3, array()), "class" => "form-control average_call_per_actual_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 556
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 559
            echo form_input(array("name" => "average_call_per_actual_working_day_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_call_per_actual_working_day_target_weeks", array()), 4, array()), "class" => "form-control average_call_per_actual_working_day_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 563
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 569
            echo form_label("Pantry check Target", "pantry_check_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 571
            echo form_input(array("name" => "pantry_check_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target", array())) : (0)), "class" => "form-control pantry_check_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Pantry check Target Monthly"));
            // line 575
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 579
            echo form_label("Week's Pantry check Target", "pantry_check_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 581
            echo form_input(array("name" => "pantry_check_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target_weeks", array()), 0, array()), "class" => "form-control pantry_check_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 585
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 588
            echo form_input(array("name" => "pantry_check_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target_weeks", array()), 1, array()), "class" => "form-control pantry_check_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 592
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 595
            echo form_input(array("name" => "pantry_check_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target_weeks", array()), 2, array()), "class" => "form-control pantry_check_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 599
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 602
            echo form_input(array("name" => "pantry_check_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target_weeks", array()), 3, array()), "class" => "form-control pantry_check_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 606
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 609
            echo form_input(array("name" => "pantry_check_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pantry_check_target_weeks", array()), 4, array()), "class" => "form-control pantry_check_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 613
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 619
            echo form_label("Sampling Target", "sampling_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 621
            echo form_input(array("name" => "sampling_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target", array())) : (0)), "class" => "form-control sampling_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Sampling Target Monthly"));
            // line 625
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 629
            echo form_label("Week's Sampling Target", "sampling_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 631
            echo form_input(array("name" => "sampling_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target_weeks", array()), 0, array()), "class" => "form-control sampling_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 635
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 638
            echo form_input(array("name" => "sampling_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target_weeks", array()), 1, array()), "class" => "form-control sampling_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 642
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 645
            echo form_input(array("name" => "sampling_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target_weeks", array()), 2, array()), "class" => "form-control sampling_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 649
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 652
            echo form_input(array("name" => "sampling_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target_weeks", array()), 3, array()), "class" => "form-control sampling_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 656
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 659
            echo form_input(array("name" => "sampling_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sampling_target_weeks", array()), 4, array()), "class" => "form-control sampling_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 663
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 669
            echo form_label("OTM A fulfilment rate (%)", "otm_a_fulfilment_rate_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 671
            echo form_input(array("name" => "otm_a_fulfilment_rate_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target", array())) : (0)), "class" => "form-control otm_a_fulfilment_rate_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "OTM A fulfilment rate (%) Monthly"));
            // line 675
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 679
            echo form_label("Week's OTM A fulfilment rate (%)", "otm_a_fulfilment_rate_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 681
            echo form_input(array("name" => "otm_a_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target_weeks", array()), 0, array()), "class" => "form-control otm_a_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01", "readonly" => "readonly"));
            // line 686
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 689
            echo form_input(array("name" => "otm_a_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target_weeks", array()), 1, array()), "class" => "form-control otm_a_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02", "readonly" => "readonly"));
            // line 694
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 697
            echo form_input(array("name" => "otm_a_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target_weeks", array()), 2, array()), "class" => "form-control otm_a_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03", "readonly" => "readonly"));
            // line 702
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 705
            echo form_input(array("name" => "otm_a_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target_weeks", array()), 3, array()), "class" => "form-control otm_a_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04", "readonly" => "readonly"));
            // line 710
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 713
            echo form_input(array("name" => "otm_a_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_fulfilment_rate_target_weeks", array()), 4, array()), "class" => "form-control otm_a_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05", "readonly" => "readonly"));
            // line 718
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 724
            echo form_label("OTM B fulfilment rate (%)", "otm_b_fulfilment_rate_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 726
            echo form_input(array("name" => "otm_b_fulfilment_rate_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target", array())) : (0)), "class" => "form-control otm_b_fulfilment_rate_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "OTM B fulfilment rate (%) Monthly"));
            // line 730
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 734
            echo form_label("Week's OTM B fulfilment rate (%)", "otm_b_fulfilment_rate_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 736
            echo form_input(array("name" => "otm_b_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target_weeks", array()), 0, array()), "class" => "form-control otm_b_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01", "readonly" => "readonly"));
            // line 741
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 744
            echo form_input(array("name" => "otm_b_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target_weeks", array()), 1, array()), "class" => "form-control otm_b_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02", "readonly" => "readonly"));
            // line 749
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 752
            echo form_input(array("name" => "otm_b_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target_weeks", array()), 2, array()), "class" => "form-control otm_b_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03", "readonly" => "readonly"));
            // line 757
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 760
            echo form_input(array("name" => "otm_b_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target_weeks", array()), 3, array()), "class" => "form-control otm_b_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04", "readonly" => "readonly"));
            // line 765
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 768
            echo form_input(array("name" => "otm_b_fulfilment_rate_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_fulfilment_rate_target_weeks", array()), 4, array()), "class" => "form-control otm_b_fulfilment_rate_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05", "readonly" => "readonly"));
            // line 773
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 779
            echo form_label("Otm A call frequency", "otm_a_call_frequency_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 781
            echo form_input(array("name" => "otm_a_call_frequency_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target", array())) : (0)), "class" => "form-control otm_a_call_frequency_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Otm A call frequency Monthly"));
            // line 785
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 789
            echo form_label("Week's Otm A call frequency", "otm_a_call_frequency_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 791
            echo form_input(array("name" => "otm_a_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target_weeks", array()), 0, array()), "class" => "form-control otm_a_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 795
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 798
            echo form_input(array("name" => "otm_a_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target_weeks", array()), 1, array()), "class" => "form-control otm_a_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 802
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 805
            echo form_input(array("name" => "otm_a_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target_weeks", array()), 2, array()), "class" => "form-control otm_a_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 809
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 812
            echo form_input(array("name" => "otm_a_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target_weeks", array()), 3, array()), "class" => "form-control otm_a_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 816
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 819
            echo form_input(array("name" => "otm_a_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_call_frequency_target_weeks", array()), 4, array()), "class" => "form-control otm_a_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 823
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 829
            echo form_label("Otm B call frequency", "otm_b_call_frequency_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 831
            echo form_input(array("name" => "otm_b_call_frequency_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target", array())) : (0)), "class" => "form-control otm_b_call_frequency_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Otm B call frequency Monthly"));
            // line 835
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 839
            echo form_label("Week's Otm B call frequency", "otm_b_call_frequency_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 841
            echo form_input(array("name" => "otm_b_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target_weeks", array()), 0, array()), "class" => "form-control otm_b_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 845
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 848
            echo form_input(array("name" => "otm_b_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target_weeks", array()), 1, array()), "class" => "form-control otm_b_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 852
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 855
            echo form_input(array("name" => "otm_b_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target_weeks", array()), 2, array()), "class" => "form-control otm_b_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 859
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 862
            echo form_input(array("name" => "otm_b_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target_weeks", array()), 3, array()), "class" => "form-control otm_b_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 866
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 869
            echo form_input(array("name" => "otm_b_call_frequency_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_call_frequency_target_weeks", array()), 4, array()), "class" => "form-control otm_b_call_frequency_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 873
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 879
            echo form_label("Otm A average call time (minutes)", "otm_a_average_call_time_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 881
            echo form_input(array("name" => "otm_a_average_call_time_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target", array())) : (0)), "class" => "form-control otm_a_average_call_time_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Otm A average call time (minutes) Monthly"));
            // line 885
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 889
            echo form_label("Week's Otm A average call time (minutes)", "otm_a_average_call_time_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 891
            echo form_input(array("name" => "otm_a_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target_weeks", array()), 0, array()), "class" => "form-control otm_a_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 895
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 898
            echo form_input(array("name" => "otm_a_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target_weeks", array()), 1, array()), "class" => "form-control otm_a_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 902
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 905
            echo form_input(array("name" => "otm_a_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target_weeks", array()), 2, array()), "class" => "form-control otm_a_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 909
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 912
            echo form_input(array("name" => "otm_a_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target_weeks", array()), 3, array()), "class" => "form-control otm_a_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 916
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 919
            echo form_input(array("name" => "otm_a_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_a_average_call_time_target_weeks", array()), 4, array()), "class" => "form-control otm_a_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 923
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 929
            echo form_label("Otm B average call time (minutes)", "otm_b_average_call_time_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 931
            echo form_input(array("name" => "otm_b_average_call_time_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target", array())) : (0)), "class" => "form-control otm_b_average_call_time_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Otm B average call time (minutes) Monthly"));
            // line 935
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 939
            echo form_label("Week's Otm B average call time (minutes)", "otm_b_average_call_time_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 941
            echo form_input(array("name" => "otm_b_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target_weeks", array()), 0, array()), "class" => "form-control otm_b_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 945
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 948
            echo form_input(array("name" => "otm_b_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target_weeks", array()), 1, array()), "class" => "form-control otm_b_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 952
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 955
            echo form_input(array("name" => "otm_b_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target_weeks", array()), 2, array()), "class" => "form-control otm_b_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 959
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 962
            echo form_input(array("name" => "otm_b_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target_weeks", array()), 3, array()), "class" => "form-control otm_b_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 966
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 969
            echo form_input(array("name" => "otm_b_average_call_time_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm_b_average_call_time_target_weeks", array()), 4, array()), "class" => "form-control otm_b_average_call_time_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 973
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 979
            echo form_label("Average Grab Target", "average_grab_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 981
            echo form_input(array("name" => "average_grab_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target", array()), "class" => "form-control average_grab_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Average Grab Target Monthly"));
            // line 985
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 989
            echo form_label("Week's Average Grab Target", "average_grab_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 991
            echo form_input(array("name" => "average_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target_weeks", array()), 0, array()), "class" => "form-control average_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 995
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 998
            echo form_input(array("name" => "average_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target_weeks", array()), 1, array()), "class" => "form-control average_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1002
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1005
            echo form_input(array("name" => "average_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target_weeks", array()), 2, array()), "class" => "form-control average_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1009
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1012
            echo form_input(array("name" => "average_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target_weeks", array()), 3, array()), "class" => "form-control average_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1016
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1019
            echo form_input(array("name" => "average_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "average_grab_target_weeks", array()), 4, array()), "class" => "form-control average_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1023
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1029
            echo form_label("Total Grip Target", "total_grip_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1031
            echo form_input(array("name" => "total_grip_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target", array()), "class" => "form-control total_grip_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Total Grip Target Monthly"));
            // line 1035
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1039
            echo form_label("Week's Total Grip", "total_grip_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1041
            echo form_input(array("name" => "total_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target_weeks", array()), 0, array()), "class" => "form-control total_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1045
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1048
            echo form_input(array("name" => "total_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target_weeks", array()), 1, array()), "class" => "form-control total_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1052
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1055
            echo form_input(array("name" => "total_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target_weeks", array()), 2, array()), "class" => "form-control total_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1059
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1062
            echo form_input(array("name" => "total_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target_weeks", array()), 3, array()), "class" => "form-control total_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1066
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1069
            echo form_input(array("name" => "total_grip_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grip_target_weeks", array()), 4, array()), "class" => "form-control total_grip_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1073
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1079
            echo form_label("Total Grab Target", "total_grab_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1081
            echo form_input(array("name" => "total_grab_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target", array()), "class" => "form-control total_grab_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Total Grab Target Monthly"));
            // line 1085
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1089
            echo form_label("Week's Total Grab", "total_grab_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1091
            echo form_input(array("name" => "total_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target_weeks", array()), 0, array()), "class" => "form-control total_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1095
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1098
            echo form_input(array("name" => "total_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target_weeks", array()), 1, array()), "class" => "form-control total_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1102
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1105
            echo form_input(array("name" => "total_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target_weeks", array()), 2, array()), "class" => "form-control total_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1109
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1112
            echo form_input(array("name" => "total_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target_weeks", array()), 3, array()), "class" => "form-control total_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1116
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1119
            echo form_input(array("name" => "total_grab_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "total_grab_target_weeks", array()), 4, array()), "class" => "form-control total_grab_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1123
            echo "
                        </div>
                    </div>
                    ";
            // line 1127
            echo "                    ";
            if ((isset($context["enablepotential"]) ? $context["enablepotential"] : null)) {
                // line 1128
                echo "                        <div class=\"form-group\">
                            <hr/>
                            ";
                // line 1130
                echo form_label("Potential Gripped Target", "potential_gripped_target", array("class" => "control-label col-sm-2"));
                echo "
                            <div class=\"col-sm-10\">
                                ";
                // line 1132
                echo form_input(array("name" => "potential_gripped_target", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target", array()), "class" => "form-control potential_gripped_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Potential Gripped Target Monthly"));
                // line 1136
                echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            ";
                // line 1140
                echo form_label("Week's Potential Gripped", "potential_gripped_target_weeks[]", array("class" => "control-label col-sm-2"));
                echo "
                            <div class=\"col-sm-2\">
                                ";
                // line 1142
                echo form_input(array("name" => "potential_gripped_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target_weeks", array()), 0, array()), "class" => "form-control potential_gripped_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
                // line 1146
                echo "
                            </div>
                            <div class=\"col-sm-2\">
                                ";
                // line 1149
                echo form_input(array("name" => "potential_gripped_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target_weeks", array()), 1, array()), "class" => "form-control potential_gripped_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
                // line 1153
                echo "
                            </div>
                            <div class=\"col-sm-2\">
                                ";
                // line 1156
                echo form_input(array("name" => "potential_gripped_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target_weeks", array()), 2, array()), "class" => "form-control potential_gripped_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
                // line 1160
                echo "
                            </div>
                            <div class=\"col-sm-2\">
                                ";
                // line 1163
                echo form_input(array("name" => "potential_gripped_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target_weeks", array()), 3, array()), "class" => "form-control potential_gripped_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
                // line 1167
                echo "
                            </div>
                            <div class=\"col-sm-2\">
                                ";
                // line 1170
                echo form_input(array("name" => "potential_gripped_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "potential_gripped_target_weeks", array()), 4, array()), "class" => "form-control potential_gripped_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
                // line 1174
                echo "
                            </div>
                        </div>
                    ";
            }
            // line 1178
            echo "                </div>

                <div class=\"push_element\">
                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1183
            echo form_label("Call Achievement Target", "push_call_achievement_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1185
            echo form_input(array("name" => "push_call_achievement_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target", array())) : (0)), "class" => "form-control push_call_achievement_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Call Achievement Target Monthly"));
            // line 1189
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1193
            echo form_label("Week's Call Achievement Target", "push_call_achievement_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1195
            echo form_input(array("name" => "push_call_achievement_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target_weeks", array()), 0, array()), "class" => "form-control push_call_achievement_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1199
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1202
            echo form_input(array("name" => "push_call_achievement_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target_weeks", array()), 1, array()), "class" => "form-control push_call_achievement_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1206
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1209
            echo form_input(array("name" => "push_call_achievement_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target_weeks", array()), 2, array()), "class" => "form-control push_call_achievement_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1213
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1216
            echo form_input(array("name" => "push_call_achievement_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target_weeks", array()), 3, array()), "class" => "form-control push_call_achievement_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1220
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1223
            echo form_input(array("name" => "push_call_achievement_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_call_achievement_target_weeks", array()), 4, array()), "class" => "form-control push_call_achievement_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1227
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1233
            echo form_label("Route Plan Coverage Target", "push_route_plan_coverage_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1235
            echo form_input(array("name" => "push_route_plan_coverage_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target", array())) : (0)), "class" => "form-control push_route_plan_coverage_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Route Plan Coverage Target Monthly"));
            // line 1239
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1243
            echo form_label("Week's Route Plan Coverage Target", "push_route_plan_coverage_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1245
            echo form_input(array("name" => "push_route_plan_coverage_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target_weeks", array()), 0, array()), "class" => "form-control push_route_plan_coverage_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1249
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1252
            echo form_input(array("name" => "push_route_plan_coverage_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target_weeks", array()), 1, array()), "class" => "form-control push_route_plan_coverage_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1256
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1259
            echo form_input(array("name" => "push_route_plan_coverage_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target_weeks", array()), 2, array()), "class" => "form-control push_route_plan_coverage_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1263
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1266
            echo form_input(array("name" => "push_route_plan_coverage_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target_weeks", array()), 3, array()), "class" => "form-control push_route_plan_coverage_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1270
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1273
            echo form_input(array("name" => "push_route_plan_coverage_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_route_plan_coverage_target_weeks", array()), 4, array()), "class" => "form-control push_route_plan_coverage_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1277
            echo "
                        </div>
                    </div>


                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1284
            echo form_label("Number Sku per ws Target", "push_number_sku_per_ws_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1286
            echo form_input(array("name" => "push_number_sku_per_ws_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target", array())) : (0)), "class" => "form-control push_number_sku_per_ws_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Number Sku per ws Target Monthly"));
            // line 1290
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1294
            echo form_label("Week's Number Sku per ws Target", "push_number_sku_per_ws_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1296
            echo form_input(array("name" => "push_number_sku_per_ws_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target_weeks", array()), 0, array()), "class" => "form-control push_number_sku_per_ws_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1300
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1303
            echo form_input(array("name" => "push_number_sku_per_ws_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target_weeks", array()), 1, array()), "class" => "form-control push_number_sku_per_ws_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1307
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1310
            echo form_input(array("name" => "push_number_sku_per_ws_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target_weeks", array()), 2, array()), "class" => "form-control push_number_sku_per_ws_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1314
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1317
            echo form_input(array("name" => "push_number_sku_per_ws_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target_weeks", array()), 3, array()), "class" => "form-control push_number_sku_per_ws_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1321
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1324
            echo form_input(array("name" => "push_number_sku_per_ws_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_number_sku_per_ws_target_weeks", array()), 4, array()), "class" => "form-control push_number_sku_per_ws_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1328
            echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <hr/>
                        ";
            // line 1334
            echo form_label("Perfect Store Target", "push_perfect_store_target", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-10\">
                            ";
            // line 1336
            echo form_input(array("name" => "push_perfect_store_target", "value" => (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target", array())) : (0)), "class" => "form-control push_perfect_store_target", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Perfect Store Target Monthly"));
            // line 1340
            echo "
                        </div>
                    </div>
                    <div class=\"form-group\">
                        ";
            // line 1344
            echo form_label("Week's Perfect Store Target", "push_perfect_store_target_weeks[]", array("class" => "control-label col-sm-2"));
            echo "
                        <div class=\"col-sm-2\">
                            ";
            // line 1346
            echo form_input(array("name" => "push_perfect_store_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target_weeks", array()), 0, array()), "class" => "form-control push_perfect_store_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 01"));
            // line 1350
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1353
            echo form_input(array("name" => "push_perfect_store_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target_weeks", array()), 1, array()), "class" => "form-control push_perfect_store_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 02"));
            // line 1357
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1360
            echo form_input(array("name" => "push_perfect_store_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target_weeks", array()), 2, array()), "class" => "form-control push_perfect_store_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 03"));
            // line 1364
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1367
            echo form_input(array("name" => "push_perfect_store_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target_weeks", array()), 3, array()), "class" => "form-control push_perfect_store_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 04"));
            // line 1371
            echo "
                        </div>
                        <div class=\"col-sm-2\">
                            ";
            // line 1374
            echo form_input(array("name" => "push_perfect_store_target_weeks[]", "value" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "push_perfect_store_target_weeks", array()), 4, array()), "class" => "form-control push_perfect_store_target_weeks", "data-parsley-type" => "number", "data-parsley-maxlength" => 10, "placeholder" => "Week 05"));
            // line 1378
            echo "
                        </div>
                    </div>
                </div>
            </div>
        </div>

        ";
            // line 1385
            echo form_close();
            echo "
    ";
        } else {
            // line 1387
            echo "        <div class=\"row\">
            <div class=\"col-sm-12\">
                <h3>You can not create new kpi target for this month</h3>
            </div>
        </div>
    ";
        }
        // line 1393
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/jquery-confirm.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <script src=\"";
        // line 1395
        echo twig_escape_filter($this->env, site_url("res/js/jquery-confirm.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "ami/kpi_target/edit1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1613 => 1395,  1607 => 1393,  1599 => 1387,  1594 => 1385,  1585 => 1378,  1583 => 1374,  1578 => 1371,  1576 => 1367,  1571 => 1364,  1569 => 1360,  1564 => 1357,  1562 => 1353,  1557 => 1350,  1555 => 1346,  1550 => 1344,  1544 => 1340,  1542 => 1336,  1537 => 1334,  1529 => 1328,  1527 => 1324,  1522 => 1321,  1520 => 1317,  1515 => 1314,  1513 => 1310,  1508 => 1307,  1506 => 1303,  1501 => 1300,  1499 => 1296,  1494 => 1294,  1488 => 1290,  1486 => 1286,  1481 => 1284,  1472 => 1277,  1470 => 1273,  1465 => 1270,  1463 => 1266,  1458 => 1263,  1456 => 1259,  1451 => 1256,  1449 => 1252,  1444 => 1249,  1442 => 1245,  1437 => 1243,  1431 => 1239,  1429 => 1235,  1424 => 1233,  1416 => 1227,  1414 => 1223,  1409 => 1220,  1407 => 1216,  1402 => 1213,  1400 => 1209,  1395 => 1206,  1393 => 1202,  1388 => 1199,  1386 => 1195,  1381 => 1193,  1375 => 1189,  1373 => 1185,  1368 => 1183,  1361 => 1178,  1355 => 1174,  1353 => 1170,  1348 => 1167,  1346 => 1163,  1341 => 1160,  1339 => 1156,  1334 => 1153,  1332 => 1149,  1327 => 1146,  1325 => 1142,  1320 => 1140,  1314 => 1136,  1312 => 1132,  1307 => 1130,  1303 => 1128,  1300 => 1127,  1295 => 1123,  1293 => 1119,  1288 => 1116,  1286 => 1112,  1281 => 1109,  1279 => 1105,  1274 => 1102,  1272 => 1098,  1267 => 1095,  1265 => 1091,  1260 => 1089,  1254 => 1085,  1252 => 1081,  1247 => 1079,  1239 => 1073,  1237 => 1069,  1232 => 1066,  1230 => 1062,  1225 => 1059,  1223 => 1055,  1218 => 1052,  1216 => 1048,  1211 => 1045,  1209 => 1041,  1204 => 1039,  1198 => 1035,  1196 => 1031,  1191 => 1029,  1183 => 1023,  1181 => 1019,  1176 => 1016,  1174 => 1012,  1169 => 1009,  1167 => 1005,  1162 => 1002,  1160 => 998,  1155 => 995,  1153 => 991,  1148 => 989,  1142 => 985,  1140 => 981,  1135 => 979,  1127 => 973,  1125 => 969,  1120 => 966,  1118 => 962,  1113 => 959,  1111 => 955,  1106 => 952,  1104 => 948,  1099 => 945,  1097 => 941,  1092 => 939,  1086 => 935,  1084 => 931,  1079 => 929,  1071 => 923,  1069 => 919,  1064 => 916,  1062 => 912,  1057 => 909,  1055 => 905,  1050 => 902,  1048 => 898,  1043 => 895,  1041 => 891,  1036 => 889,  1030 => 885,  1028 => 881,  1023 => 879,  1015 => 873,  1013 => 869,  1008 => 866,  1006 => 862,  1001 => 859,  999 => 855,  994 => 852,  992 => 848,  987 => 845,  985 => 841,  980 => 839,  974 => 835,  972 => 831,  967 => 829,  959 => 823,  957 => 819,  952 => 816,  950 => 812,  945 => 809,  943 => 805,  938 => 802,  936 => 798,  931 => 795,  929 => 791,  924 => 789,  918 => 785,  916 => 781,  911 => 779,  903 => 773,  901 => 768,  896 => 765,  894 => 760,  889 => 757,  887 => 752,  882 => 749,  880 => 744,  875 => 741,  873 => 736,  868 => 734,  862 => 730,  860 => 726,  855 => 724,  847 => 718,  845 => 713,  840 => 710,  838 => 705,  833 => 702,  831 => 697,  826 => 694,  824 => 689,  819 => 686,  817 => 681,  812 => 679,  806 => 675,  804 => 671,  799 => 669,  791 => 663,  789 => 659,  784 => 656,  782 => 652,  777 => 649,  775 => 645,  770 => 642,  768 => 638,  763 => 635,  761 => 631,  756 => 629,  750 => 625,  748 => 621,  743 => 619,  735 => 613,  733 => 609,  728 => 606,  726 => 602,  721 => 599,  719 => 595,  714 => 592,  712 => 588,  707 => 585,  705 => 581,  700 => 579,  694 => 575,  692 => 571,  687 => 569,  679 => 563,  677 => 559,  672 => 556,  670 => 552,  665 => 549,  663 => 545,  658 => 542,  656 => 538,  651 => 535,  649 => 531,  644 => 529,  638 => 525,  636 => 521,  631 => 519,  623 => 513,  621 => 509,  616 => 506,  614 => 502,  609 => 499,  607 => 495,  602 => 492,  600 => 488,  595 => 485,  593 => 481,  588 => 479,  582 => 475,  580 => 471,  575 => 469,  566 => 462,  564 => 458,  559 => 455,  557 => 451,  552 => 448,  550 => 444,  545 => 441,  543 => 437,  538 => 434,  536 => 430,  531 => 428,  525 => 424,  523 => 420,  518 => 418,  510 => 412,  508 => 408,  503 => 405,  501 => 401,  496 => 398,  494 => 394,  489 => 391,  487 => 387,  482 => 384,  480 => 380,  475 => 378,  469 => 374,  467 => 370,  462 => 368,  454 => 362,  452 => 358,  447 => 355,  445 => 351,  440 => 348,  438 => 344,  433 => 341,  431 => 337,  426 => 334,  424 => 330,  419 => 328,  413 => 324,  411 => 320,  406 => 318,  398 => 312,  396 => 308,  391 => 305,  389 => 301,  384 => 298,  382 => 294,  377 => 291,  375 => 287,  370 => 284,  368 => 280,  363 => 278,  357 => 274,  355 => 270,  350 => 268,  342 => 262,  340 => 257,  335 => 254,  333 => 249,  328 => 246,  326 => 241,  321 => 238,  319 => 233,  314 => 230,  312 => 225,  307 => 223,  301 => 219,  299 => 215,  294 => 213,  286 => 207,  284 => 203,  279 => 200,  277 => 196,  272 => 193,  270 => 189,  265 => 186,  263 => 182,  258 => 179,  256 => 175,  251 => 173,  245 => 169,  243 => 165,  238 => 163,  229 => 157,  224 => 155,  216 => 150,  211 => 148,  203 => 143,  198 => 141,  190 => 136,  185 => 134,  174 => 125,  172 => 124,  160 => 116,  157 => 115,  154 => 114,  101 => 65,  95 => 62,  32 => 3,  29 => 2,  11 => 1,);
    }
}
