<?php

/* ami/questions/edit.html.twig */
class __TwigTemplate_93a7a2da3ebc22dbceb237b973e688b72a5fa27f13b6cf759944ada528923ede extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/questions/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    <script>
        \$(\".cat_question input:radio\").change(function () {
            val = \$(this).val();
            if (val == 0) {
                \$('.select_cat').hide();
            } else if (val == 1) {
                \$('.select_cat').show();
            }
        });
        \$(\".select_app_type input:radio\").change(function () {
            val = \$(this).val();
            if (val) {
                url_rq = \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/questions/cat_questions"), "html", null, true);
        echo "\";
                \$.ajax({
                    url: url_rq + '/' + val + '/true',
                    type: 'GET',
                    success: function (res) {
                        \$('.form_select_cat').html(res);
                    }
                });
            }
        });
    </script>
";
    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        // line 28
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 43
        echo form_open(site_url("ami/questions/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "filter" => (isset($context["filter"]) ? $context["filter"] : null)));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Questions</h1>

                <div class=\"text-right\">
                    ";
        // line 51
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/questions/edit.html.twig", 51)->display(array_merge($context, array("url" => "ami/questions", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "questions")));
        // line 52
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 61
        echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 63
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group select_app_type\">
                ";
        // line 67
        echo form_label("Type", "Type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 69
        echo form_radio("app_type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 0));
        echo " Pull</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 70
        echo form_radio("app_type", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 1));
        echo " Push</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 71
        echo form_radio("app_type", 2, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 2));
        echo " Leader</label>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 75
        echo form_label("Cat Questions", "cat_questions", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley cat_question\">
                    <label class=\"radio-inline\">";
        // line 77
        echo form_radio("type_question", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type_question", array()) == 1));
        echo "
                        Question</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 79
        echo form_radio("type_question", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type_question", array()) == 0));
        echo "
                        Categories</label>
                </div>
            </div>
            <div class=\"form-group select_cat\" style=\"display:";
        // line 83
        echo ((($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type_question", array()) == 1)) ? ("block") : ("none"));
        echo "\">
                ";
        // line 84
        echo form_label("", "cat_questions", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 86
        echo form_dropdown("parent_id", (isset($context["cat_questions"]) ? $context["cat_questions"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "parent_id", array()), "class=\"form-control form_select_cat\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            ";
        // line 90
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 94
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/questions/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 94,  171 => 90,  164 => 86,  159 => 84,  155 => 83,  148 => 79,  143 => 77,  138 => 75,  131 => 71,  127 => 70,  123 => 69,  118 => 67,  111 => 63,  106 => 61,  95 => 52,  93 => 51,  82 => 43,  65 => 28,  62 => 27,  46 => 15,  32 => 3,  29 => 2,  11 => 1,);
    }
}
