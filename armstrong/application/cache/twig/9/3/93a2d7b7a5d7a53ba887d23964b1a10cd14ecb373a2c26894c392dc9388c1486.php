<?php

/* ami/promotional_mechanics/index.html.twig */
class __TwigTemplate_93a2d7b7a5d7a53ba887d23964b1a10cd14ecb373a2c26894c392dc9388c1486 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotional_mechanics/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#promotional_1').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"80px\", \"targets\": [10]}
                ],
                \"iDisplayLength\": 50
            });

            \$('#promotional_2').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"80px\", \"targets\": [9]}
                ],
                \"iDisplayLength\": 50
            });

            \$('#promotional_3').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"80px\", \"targets\": [8]}
                ],
                \"iDisplayLength\": 50
            });

            \$('#promotional_4').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"80px\", \"targets\": [7]}
                ],
                \"iDisplayLength\": 50
            });

            \$('#promotional_5').dataTable({
                \"processing\": true,
                \"serverSide\": false,
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"80px\", \"targets\": [6]}
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 96
    public function block_css($context, array $blocks = array())
    {
        // line 97
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 98
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 101
    public function block_content($context, array $blocks = array())
    {
        // line 102
        echo "
    ";
        // line 103
        echo form_open(site_url("ami/promotional_mechanics/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 108
        echo "Promotional Mechanics";
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 112
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>

                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-success\"
                           href=\"";
        // line 117
        echo twig_escape_filter($this->env, site_url(("ami/promotional_mechanics/add?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 130
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "promo_1")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                             href=\"";
        // line 131
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics?filter=promo_1"), "html", null, true);
        echo "\">Promotion
                        1</a>
                </li>
                <li role=\"presentation\" ";
        // line 134
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "promo_2")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                             href=\"";
        // line 135
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics?filter=promo_2"), "html", null, true);
        echo "\">Promotion
                        2</a>
                </li>
                <li role=\"presentation\" ";
        // line 138
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "promo_3")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                             href=\"";
        // line 139
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics?filter=promo_3"), "html", null, true);
        echo "\">Promotion
                        3</a>
                </li>
                <li role=\"presentation\" ";
        // line 142
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "promo_4")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                             href=\"";
        // line 143
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics?filter=promo_4"), "html", null, true);
        echo "\">Promotion
                        4</a>
                </li>
                <li role=\"presentation\" ";
        // line 146
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "promo_5")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                             href=\"";
        // line 147
        echo twig_escape_filter($this->env, site_url("ami/promotional_mechanics?filter=promo_5"), "html", null, true);
        echo "\">Promotion
                        5</a>
                </li>
            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">
                    ";
        // line 154
        if (((isset($context["filter"]) ? $context["filter"] : null) == "promo_1")) {
            // line 155
            echo "                        ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_1.html.twig", "ami/promotional_mechanics/index.html.twig", 155)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 156
            echo "                    ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_2")) {
            // line 157
            echo "                        ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_2.html.twig", "ami/promotional_mechanics/index.html.twig", 157)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 158
            echo "                    ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_3")) {
            // line 159
            echo "                        ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_3.html.twig", "ami/promotional_mechanics/index.html.twig", 159)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 160
            echo "                    ";
        } elseif (((isset($context["filter"]) ? $context["filter"] : null) == "promo_4")) {
            // line 161
            echo "                        ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_4.html.twig", "ami/promotional_mechanics/index.html.twig", 161)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 162
            echo "                    ";
        } else {
            // line 163
            echo "                        ";
            $this->loadTemplate("ami/promotional_mechanics/items/promotion_5.html.twig", "ami/promotional_mechanics/index.html.twig", 163)->display(array_merge($context, array("promotional_mechanics" => (isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null))));
            // line 164
            echo "                    ";
        }
        // line 165
        echo "                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 174
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 174,  286 => 165,  283 => 164,  280 => 163,  277 => 162,  274 => 161,  271 => 160,  268 => 159,  265 => 158,  262 => 157,  259 => 156,  256 => 155,  254 => 154,  244 => 147,  240 => 146,  234 => 143,  230 => 142,  224 => 139,  220 => 138,  214 => 135,  210 => 134,  204 => 131,  200 => 130,  184 => 117,  176 => 112,  169 => 108,  161 => 103,  158 => 102,  155 => 101,  149 => 98,  144 => 97,  141 => 96,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
