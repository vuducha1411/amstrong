<?php

/* ami/setting_country/items/coaching_form_setting.html.twig */
class __TwigTemplate_3e3766e5654b754ee9b08131ab4d9c6478b87019a67d985980bacd9720161a6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-pane\" id=\"coaching_forms\">
    <ul class=\"nav nav-tabs\" role=\"tablist\">
        <li role=\"presentation\" class=\"active\">
            <a href=\"#app_pull\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Pull</a>
        </li>
        <li role=\"presentation\">
            <a href=\"#app_push\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Push</a>
        </li>
    </ul>

    <div class=\"panel panel-default m-t\">
        <div class=\"panel-body tab-content\">
            <div role=\"tabpanel\" class=\"tab-pane active\" id=\"app_pull\">
                <div class=\"form-group\">
                    ";
        // line 15
        echo form_label("Summary of the visits Today", "visit_summary", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 17
        echo form_textarea(array("name" => "application_config[pull][coaching_forms][visit_summary]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "visit_summary", array()), "class" => "form-control input-xxlarge", "id" => "visitSummaryPull"));
        // line 19
        echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
        // line 23
        echo form_label("Section Height", "visit_summary_height", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 25
        echo form_input(array("name" => "application_config[pull][coaching_forms][visit_summary_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "visit_summary_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                    </div>
                </div>
                ";
        // line 28
        $context["excellence_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        // line 29
        echo "                ";
        $context["improvement_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        // line 30
        echo "                ";
        $context["development_counter_pull"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "development_plan_new", array()));
        // line 31
        echo "                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 33
        echo form_label("Areas of Excellence (selling skills, selling tools, chefmanship)", "areas_of_excellence_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 36
        echo form_label("Section Height", "areas_of_excellence_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 38
        echo form_input(array("name" => "application_config[pull][coaching_forms][areas_of_excellence_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_excellence_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["excellence_new_array"]) {
            // line 42
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 43
            echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 45
            echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_excellence_new][" . $context["key"]) . "]"), "value" => $context["excellence_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['excellence_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 56
        echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 58
        echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_excellence_new][" . (isset($context["excellence_counter_pull"]) ? $context["excellence_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 72
        echo form_label("Areas for Improvement", "areas_of_improvement_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 75
        echo form_label("Section Height", "areas_of_improvement_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 77
        echo form_input(array("name" => "application_config[pull][coaching_forms][areas_of_improvement_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_improvement_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["improvement_new_array"]) {
            // line 81
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 82
            echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 84
            echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_improvement_new][" . $context["key"]) . "]"), "value" => $context["improvement_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['improvement_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 95
        echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 97
        echo form_input(array("name" => (("application_config[pull][coaching_forms][areas_of_improvement_new][" . (isset($context["improvement_counter_pull"]) ? $context["improvement_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 111
        echo form_label("Development Plan", "development_plan_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 114
        echo form_label("Section Height", "development_plan_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 116
        echo form_input(array("name" => "application_config[pull][coaching_forms][development_plan_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "development_plan_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 119
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "coaching_forms", array()), "development_plan_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["development_new_array"]) {
            // line 120
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 121
            echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 123
            echo form_input(array("name" => (("application_config[pull][coaching_forms][development_plan_new][" . $context["key"]) . "]"), "value" => $context["development_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['development_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 133
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 134
        echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 136
        echo form_input(array("name" => (("application_config[pull][coaching_forms][development_plan_new][" . (isset($context["development_counter_pull"]) ? $context["development_counter_pull"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
            </div>
            <div role=\"tabpanel\" class=\"tab-pane\" id=\"app_push\">
                <div class=\"form-group\">
                    ";
        // line 150
        echo form_label("Summary of the visits Today", "visit_summary", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 152
        echo form_textarea(array("name" => "application_config[push][coaching_forms][visit_summary]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "visit_summary", array()), "class" => "form-control input-xxlarge", "id" => "visitSummaryPush"));
        // line 154
        echo "
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
        // line 158
        echo form_label("Section Height", "visit_summary_height", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 160
        echo form_input(array("name" => "application_config[push][coaching_forms][visit_summary_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "visit_summary_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                    </div>
                </div>
                ";
        // line 163
        $context["excellence_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        // line 164
        echo "                ";
        $context["improvement_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        // line 165
        echo "                ";
        $context["development_counter_push"] = twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "development_plan_new", array()));
        // line 166
        echo "                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 168
        echo form_label("Areas of Excellence (selling skills, selling tools, chefmanship)", "areas_of_excellence_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 171
        echo form_label("Section Height", "areas_of_excellence_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 173
        echo form_input(array("name" => "application_config[push][coaching_forms][areas_of_excellence_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_excellence_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_excellence_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["excellence_new_array"]) {
            // line 177
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 178
            echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 180
            echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_excellence_new][" . $context["key"]) . "]"), "value" => $context["excellence_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['excellence_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 190
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 191
        echo form_label("Title", "areas_of_excellence_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 193
        echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_excellence_new][" . (isset($context["excellence_counter_push"]) ? $context["excellence_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 207
        echo form_label("Areas for Improvement", "areas_of_improvement_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 210
        echo form_label("Section Height", "areas_of_improvement_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 212
        echo form_input(array("name" => "application_config[push][coaching_forms][areas_of_improvement_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_improvement_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 215
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "areas_of_improvement_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["improvement_new_array"]) {
            // line 216
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 217
            echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 219
            echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_improvement_new][" . $context["key"]) . "]"), "value" => $context["improvement_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['improvement_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 229
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 230
        echo form_label("Title", "areas_of_improvement_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 232
        echo form_input(array("name" => (("application_config[push][coaching_forms][areas_of_improvement_new][" . (isset($context["improvement_counter_push"]) ? $context["improvement_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
                <hr>
                <div class=\"areas-block\">
                    <div class=\"form-group\">
                        ";
        // line 246
        echo form_label("Development Plan", "development_plan_new", array("class" => "control-label col-sm-12 title-left"));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 249
        echo form_label("Section Height", "development_plan_new_height", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 251
        echo form_input(array("name" => "application_config[push][coaching_forms][development_plan_new_height]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "development_plan_new_height", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                        </div>
                    </div>
                    ";
        // line 254
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "coaching_forms", array()), "development_plan_new", array()));
        foreach ($context['_seq'] as $context["key"] => $context["development_new_array"]) {
            // line 255
            echo "                        <div class=\"form-group sku-base\">
                            ";
            // line 256
            echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
            echo "
                            <div class=\"col-sm-6\">
                                ";
            // line 258
            echo form_input(array("name" => (("application_config[push][coaching_forms][development_plan_new][" . $context["key"]) . "]"), "value" => $context["development_new_array"], "class" => "form-control InputProductId"));
            echo "
                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add hide\"><i
                                            class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i
                                            class=\"fa fa-minus\"></i></button>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['development_new_array'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 268
        echo "                    <div class=\"form-group sku-base\">
                        ";
        // line 269
        echo form_label("Title", "development_plan_new", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            ";
        // line 271
        echo form_input(array("name" => (("application_config[push][coaching_forms][development_plan_new][" . (isset($context["development_counter_push"]) ? $context["development_counter_push"] : null)) . "]"), "value" => null, "class" => "form-control InputProductId"));
        echo "
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i
                                        class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                        class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>
                    <div class=\"base-block\"></div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/setting_country/items/coaching_form_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  510 => 271,  505 => 269,  502 => 268,  486 => 258,  481 => 256,  478 => 255,  474 => 254,  468 => 251,  463 => 249,  457 => 246,  440 => 232,  435 => 230,  432 => 229,  416 => 219,  411 => 217,  408 => 216,  404 => 215,  398 => 212,  393 => 210,  387 => 207,  370 => 193,  365 => 191,  362 => 190,  346 => 180,  341 => 178,  338 => 177,  334 => 176,  328 => 173,  323 => 171,  317 => 168,  313 => 166,  310 => 165,  307 => 164,  305 => 163,  299 => 160,  294 => 158,  288 => 154,  286 => 152,  281 => 150,  264 => 136,  259 => 134,  256 => 133,  240 => 123,  235 => 121,  232 => 120,  228 => 119,  222 => 116,  217 => 114,  211 => 111,  194 => 97,  189 => 95,  186 => 94,  170 => 84,  165 => 82,  162 => 81,  158 => 80,  152 => 77,  147 => 75,  141 => 72,  124 => 58,  119 => 56,  116 => 55,  100 => 45,  95 => 43,  92 => 42,  88 => 41,  82 => 38,  77 => 36,  71 => 33,  67 => 31,  64 => 30,  61 => 29,  59 => 28,  53 => 25,  48 => 23,  42 => 19,  40 => 17,  35 => 15,  19 => 1,);
    }
}
