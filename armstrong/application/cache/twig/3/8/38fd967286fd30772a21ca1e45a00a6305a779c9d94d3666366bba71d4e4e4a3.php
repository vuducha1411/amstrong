<?php

/* ami/master_template/index.html.twig */
class __TwigTemplate_38fd967286fd30772a21ca1e45a00a6305a779c9d94d3666366bba71d4e4e4a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/master_template/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<script>
\$(document).ready(function() {
    
    \$('#Generate').click(function(e) {
        xhr = null;
        var \$this = \$(this),
            \$selector = \$('#PlanSelector'),
            \$container = \$('#PlansContainer');

            // \$container.html('').empty().html('<div class=\"col-sm-6 col-sm-offset-3\"><i class=\"fa fa-spinner fa-spin\"></i> Loading...</div>');

        if (xhr) {
            xhr.abort();
        }

        if (!xhr && \$selector.val()) {
            
            \$container.html('').empty().html('<div class=\"col-sm-6 col-sm-offset-3\"><i class=\"fa fa-spinner fa-spin\"></i> Loading...</div>');

            \$.ajax({
                url: \"";
        // line 29
        echo twig_escape_filter($this->env, site_url("ami/master_template/templates"), "html", null, true);
        echo "\",
                type: 'post',
                data: { id: \$selector.val(), draft: ";
        // line 31
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo " },
                success: function(res) {
                    try {
                        var resJson = JSON.parse(res);
                        if (resJson._session_expried) {
                            window.location.reload();
                            return;
                        }
                        
                    } catch(e) {}
                    
                    \$container.html('').empty();
                    if (res) {
                        \$container.append(res);
                        CheckAll(\$('input.CheckAll'));
                    }

                    xhr = null;
                }
            });
        }
    });

    // \$('#PlanSelector').trigger('change');
});
</script>
";
    }

    // line 59
    public function block_css($context, array $blocks = array())
    {
        // line 60
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 64
    public function block_content($context, array $blocks = array())
    {
        // line 65
        echo "
    ";
        // line 66
        echo form_open(site_url("ami/master_template/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 71
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Master Template Draft") : ("Master Template"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 74
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 76
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/master_template/index.html.twig", 76)->display(array_merge($context, array("url" => "ami/master_template", "icon" => "fa-retweet", "title" => "Master Template", "permission" => "route_plan_master_template")));
        // line 77
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            

            ";
        // line 87
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 88
            echo "                <div class=\"form-group\">
                    ";
            // line 89
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 91
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"PlanSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 95
        echo "
            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\">Generate</a>
                </div>
            </div>

            <div id=\"PlansContainer\"></div>
        </div>
    </div>
    
    ";
        // line 106
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/master_template/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 106,  172 => 95,  165 => 91,  160 => 89,  157 => 88,  155 => 87,  143 => 77,  141 => 76,  136 => 74,  130 => 71,  122 => 66,  119 => 65,  116 => 64,  110 => 61,  106 => 60,  103 => 59,  72 => 31,  67 => 29,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
