<?php

/* ami/promotional_mechanics/items/promotion_1.html.twig */
class __TwigTemplate_38eb3e9204ff2dbdde40a8b2478b621996c26c4d1f1e579e49258fe1fde0d882 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-panel active\">
    <table class=\"table table-striped table-bordered table-hover\" id=\"promotional_1\">
        <thead>
        <tr>
            ";
        // line 5
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
            // line 6
            echo "                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                      data-target=\"tbody\"
                                                      data-description=\"#CheckAllBtn\"></th>
            ";
        }
        // line 10
        echo "            <th>Promotion Description</th>
            <th>Months since last bought</th>
            <th>Purchased SKU total</th>
            <th>Free SKU total</th>
            <th>Purchased SKU code</th>
            <th>Purchased SKU name</th>
            <th>Free SKU code</th>
            <th>From date</th>
            <th>To date</th>
            <th class=\"nosort text-center\"></th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promotinal"]) {
            // line 24
            echo "            <tr>
                ";
            // line 25
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
                // line 26
                echo "                    <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["promotinal"], "id", array())));
                echo "</td>
                ";
            }
            // line 28
            echo "                <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "sub_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "n_months", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "purchased_sku_quantity", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "free_sku_quantity", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "purchased_sku_number", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "purchased_sku_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "free_sku_number", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "from_date", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "to_date", array()), "html", null, true);
            echo "</td>
                <td class=\"center text-center\" style=\"min-width: 80px;\">
                    ";
            // line 38
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotional_mechanics")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics")))) {
                // line 39
                echo "                        <div class=\"btn-group\">
                            ";
                // line 40
                echo html_btn(site_url(((("ami/promotional_mechanics/edit/" . $this->getAttribute($context["promotinal"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                            ";
                // line 41
                echo html_btn(site_url(("ami/promotional_mechanics/delete/" . $this->getAttribute($context["promotinal"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                        </div>
                    ";
            }
            // line 44
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotinal'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/items/promotion_1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 47,  116 => 44,  110 => 41,  106 => 40,  103 => 39,  101 => 38,  96 => 36,  92 => 35,  88 => 34,  84 => 33,  80 => 32,  76 => 31,  72 => 30,  68 => 29,  63 => 28,  57 => 26,  55 => 25,  52 => 24,  48 => 23,  33 => 10,  27 => 6,  25 => 5,  19 => 1,);
    }
}
