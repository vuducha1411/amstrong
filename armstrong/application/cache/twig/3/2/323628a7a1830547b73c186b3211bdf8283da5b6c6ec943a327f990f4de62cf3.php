<?php

/* ami/setting_module_fields/module_fields.html.twig */
class __TwigTemplate_323628a7a1830547b73c186b3211bdf8283da5b6c6ec943a327f990f4de62cf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_module_fields/module_fields.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>


    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                ";
        // line 14
        echo "                 ";
        // line 15
        echo "                 ";
        // line 16
        echo "                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 27
    public function block_css($context, array $blocks = array())
    {
        // line 28
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 32
    public function block_content($context, array $blocks = array())
    {
        // line 33
        echo "
    ";
        // line 34
        echo form_open(site_url("ami/setting_module_fields/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 38
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Module Fields Draft") : ("Module Fields"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 42
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 44
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/setting_module_fields/module_fields.html.twig", 44)->display(array_merge($context, array("url" => "ami/setting_module_fields", "icon" => "fa-user", "title" => "Module Fields", "permission" => "setting_module_fields", "showImport" => 0)));
        // line 45
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            ";
        // line 59
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 60
            echo "                                <div class=\"row table-filter\">
                                    <div class=\"col-md-12\">
                                        <div class=\"dropdown\">
                                            <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                                                    id=\"dropdownFilter\" data-toggle=\"dropdown\">
                                                Filter: ";
            // line 65
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                                        class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                                                <li role=\"presentation\" ";
            // line 69
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                                            role=\"menuitem\" tabindex=\"-1\"
                                                            href=\"";
            // line 71
            echo twig_escape_filter($this->env, site_url("ami/setting_module_fields/module_fields"), "html", null, true);
            echo "\">All</a>
                                                </li>
                                                ";
            // line 73
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 74
                echo "                                                    <li role=\"presentation\" ";
                echo ((((isset($context["filter"]) ? $context["filter"] : null) == $this->getAttribute($context["module"], "name", array()))) ? ("class=\"active\"") : (""));
                echo ">
                                                        <a role=\"menuitem\" tabindex=\"-1\"
                                                           href=\"";
                // line 76
                echo twig_escape_filter($this->env, site_url(("ami/setting_module_fields/module_fields?filter=" . $this->getAttribute($context["module"], "name", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["module"], "name", array()), "html", null, true);
                echo "</a>
                                                    </li>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ";
        }
        // line 84
        echo "                            <thead>
                            <tr>
                                ";
        // line 86
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "setting_module_fields"))) {
            // line 87
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 91
        echo "                                <th>ID</th>
                                <th>Module</th>
                                <th>Field</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 100
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 101
            echo "                                <tr>
                                    ";
            // line 102
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "setting_module_fields"))) {
                // line 103
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 105
            echo "                                    <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "module", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "field", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 113
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "setting_module_fields"))) {
                // line 114
                echo "                                                ";
                echo html_btn(site_url(((("ami/setting_module_fields/module_fields_edit/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "

                                            ";
            }
            // line 117
            echo "
                                            ";
            // line 118
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "setting_module_fields")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 119
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 120
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/setting_module_fields/restore/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 122
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/setting_module_fields/delete/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 124
                echo "                                            ";
            }
            // line 125
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 133
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 134
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/setting_module_fields/module_fields/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/setting_module_fields/module_fields/draft")));
            // line 135
            echo "                                ";
        } else {
            // line 136
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/setting_module_fields/module_fields/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/setting_module_fields/module_fields")));
            // line 137
            echo "                                ";
        }
        // line 138
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 152
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/setting_module_fields/module_fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 152,  306 => 138,  303 => 137,  300 => 136,  297 => 135,  294 => 134,  292 => 133,  286 => 129,  277 => 125,  274 => 124,  268 => 122,  262 => 120,  259 => 119,  257 => 118,  254 => 117,  247 => 114,  245 => 113,  238 => 109,  234 => 108,  230 => 107,  226 => 106,  221 => 105,  215 => 103,  213 => 102,  210 => 101,  206 => 100,  195 => 91,  189 => 87,  187 => 86,  183 => 84,  176 => 79,  165 => 76,  159 => 74,  155 => 73,  150 => 71,  145 => 69,  138 => 65,  131 => 60,  129 => 59,  113 => 45,  111 => 44,  106 => 42,  99 => 38,  92 => 34,  89 => 33,  86 => 32,  80 => 29,  75 => 28,  72 => 27,  59 => 16,  57 => 15,  55 => 14,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
