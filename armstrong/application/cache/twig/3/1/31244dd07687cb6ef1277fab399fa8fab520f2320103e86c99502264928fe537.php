<?php

/* ami/webshop/tag.html.twig */
class __TwigTemplate_31244dd07687cb6ef1277fab399fa8fab520f2320103e86c99502264928fe537 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/webshop/tag.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/webshop.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 16
    public function block_css($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        echo form_open_multipart(site_url("ami/webshop/add_new"), array("class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Tag Webshop Order</h1>
                <div class=\"text-right\">
                    ";
        // line 28
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/webshop/tag.html.twig", 28)->display(array_merge($context, array("url" => "ami/webshop", "id" => (isset($context["order_id"]) ? $context["order_id"] : null), "permission" => "webshop")));
        // line 29
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
            <div>
                <ul class=\"breadcrumb\">
                    <li><a href=";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo ">Dashboard</a></li>
                    <li><a href=";
        // line 35
        echo twig_escape_filter($this->env, site_url("ami/webshop"), "html", null, true);
        echo ">Webshop</a></li>
                    <li><a href=\"#\">Tag Order</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 46
        echo form_label("Salesperson ID", "salesperson", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 48
        echo form_dropdown("", (isset($context["salespersons"]) ? $context["salespersons"] : null), (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), (("
                            class=\"form-control\"
                            data-parsley-required=\"true\"
                            id=\"armstrong_2_salesperson\"
                            data-url=\"" . site_url("ami/salespersons/fetch_customer")) . "\""));
        // line 52
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_salespersons_id\" id=\"armstrong_2_salespersons_id\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), "html", null, true);
        echo "\">
                    </div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\" id=\"customer\">
                <div class=\"form-group\">
                    ";
        // line 59
        echo form_label("Customer ID", "customer", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 61
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_customer", "data-parsley-required" => "true"));
        // line 64
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"armstrong_2_customers_id\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), "html", null, true);
        echo "\">
                        <div id=\"addPotential\"></div>
                    </div>
                </div>
            </div>
            <div id=\"potential\">
                ";
        // line 92
        echo "            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 95
        echo form_label("Distributor ID", "distributor", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 97
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_distributor"));
        // line 99
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_distributors_id\" id=\"armstrong_2_distributors_id\" value=\"";
        // line 100
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), "html", null, true);
        echo "\">
                    </div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 106
        echo form_label("Wholesaler ID", "wholesaler", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 108
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), (isset($context["armstrong_2_wholesalers_id"]) ? $context["armstrong_2_wholesalers_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_wholesaler"));
        // line 110
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_wholesalers_id\" id=\"armstrong_2_wholesalers_id\" value=\"";
        // line 111
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_wholesalers_id"]) ? $context["armstrong_2_wholesalers_id"] : null), "html", null, true);
        echo "\">
                    </div>
                </div>
            </div>
            ";
        // line 115
        if (((isset($context["country_id"]) ? $context["country_id"] : null) == 2)) {
            // line 116
            echo "                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
            // line 118
            echo form_label("Call Record", "call_record", array("class" => "control-label col-md-3"));
            echo "
                        <div class=\"col-md-6 no-parsley\">";
            // line 119
            echo form_input(array("name" => "armstrong_2_call_record", "value" => "", "class" => "form-control", "id" => "armstrong_2_call_record"));
            // line 121
            echo "</div>
                        <input type=\"hidden\" name=\"armstrong_2_call_records_id\" id=\"armstrong_2_call_records_id\" value=\"";
            // line 122
            echo twig_escape_filter($this->env, (isset($context["armstrong_2_call_records_id"]) ? $context["armstrong_2_call_records_id"] : null), "html", null, true);
            echo "\">
                    </div>
                </div>
            ";
        }
        // line 126
        echo "            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 128
        echo form_label("Webshop Order ID", "order_id", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 129
        echo form_input(array("name" => "webshop_order_id", "value" => (isset($context["order_id"]) ? $context["order_id"] : null), "class" => "form-control", "id" => "webshop_order_id"));
        echo "</div>
                </div>
            </div>
            ";
        // line 132
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["product"]) {
            // line 133
            echo "                <div class=\"col-md-12 form-group sku-base\">
                    <div class=\"form-group\">
                        ";
            // line 135
            echo form_label("Product", "product", array("class" => "control-label col-md-3"));
            echo "
                        <div class=\"col-md-6\">
                            ";
            // line 137
            echo form_input(array("name" => "", "value" => (($this->getAttribute($context["product"], "product_number", array()) . " - ") . $this->getAttribute($context["product"], "product_name", array())), "class" => "form-control", "id" => "product"));
            echo "
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            <b>Webshop : &nbsp;</b>
                            ";
            // line 141
            echo form_hidden((("webshop[" . $context["key"]) . "][sku_number]"), $this->getAttribute($context["product"], "product_number", array()));
            echo "
                            Quantity ";
            // line 142
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_quantity]"), "value" => $this->getAttribute($context["product"], "product_quantity", array()), "class" => "form-control sub-input qty-case", "id" => "product_quantity", "readonly" => "readonly"));
            echo " &nbsp;
                            ";
            // line 143
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_package_type]"), "value" => $this->getAttribute($context["product"], "product_package_type", array()), "class" => "form-control sub-input subtotal", "id" => "product_package_type", "readonly" => "readonly"));
            echo " &nbsp;
                            Price ";
            // line 144
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_price]"), "value" => $this->getAttribute($context["product"], "product_price", array()), "class" => "form-control sub-input qty-case", "id" => "product_price", "readonly" => "readonly"));
            echo " &nbsp;
                            Sub Total ";
            // line 145
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][total_price]"), "value" => $this->getAttribute($context["product"], "product_total_price", array()), "class" => "form-control sub-input subtotal", "id" => "total_price", "readonly" => "readonly"));
            echo "
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            <b>Armstrong : &nbsp;</b>
                            ";
            // line 149
            echo form_hidden((("armstrong[" . $context["key"]) . "][sku_number]"), $this->getAttribute($context["product"], "product_number", array()));
            echo "
                            ";
            // line 150
            echo form_hidden((("armstrong[" . $context["key"]) . "][package_type]"), $this->getAttribute($context["product"], "product_package_type", array()));
            echo "
                            Quantity Per Case ";
            // line 151
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][quantity_case]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_per_case", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_product_quantity", "readonly" => "readonly"));
            echo "
                            Price ";
            // line 152
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][price]"), "value" => $this->getAttribute($context["product"], "armstrong_product_price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_product_price", "readonly" => "readonly"));
            echo " &nbsp;
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            <b>&nbsp;</b>
                            Quantity
                            ";
            // line 157
            if (($this->getAttribute($context["product"], "product_package_type", array()) == "Case")) {
                // line 158
                echo "                                ";
                echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][qty_case]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_case", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_qty_case", "readonly" => "readonly"));
                echo "
                            ";
            } else {
                // line 160
                echo "                                ";
                echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][qty_piece]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_pcs", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_qty_pcs", "readonly" => "readonly"));
                echo "
                            ";
            }
            // line 162
            echo "                            ";
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][package_type]"), "value" => $this->getAttribute($context["product"], "product_package_type", array()), "class" => "form-control sub-input subtotal", "id" => "package_type", "readonly" => "readonly"));
            echo "
                            Sub Total ";
            // line 163
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][total_price]"), "value" => $this->getAttribute($context["product"], "armstrong_total_price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_total_price", "readonly" => "readonly"));
            echo "
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 168
        echo "            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 170
        echo form_label("Total Order", "total_order", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 171
        echo form_input(array("name" => "total", "value" => (isset($context["total_order"]) ? $context["total_order"] : null), "class" => "form-control", "id" => "total"));
        echo "</div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 176
        echo form_label("Order Date", "order_date", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 177
        echo form_input(array("name" => "order_date", "value" => (isset($context["order_date"]) ? $context["order_date"] : null), "class" => "form-control", "id" => "order_date"));
        echo "</div>
                </div>
            </div>
            ";
        // line 180
        echo form_close();
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/webshop/tag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 180,  352 => 177,  348 => 176,  340 => 171,  336 => 170,  332 => 168,  321 => 163,  316 => 162,  310 => 160,  304 => 158,  302 => 157,  294 => 152,  290 => 151,  286 => 150,  282 => 149,  275 => 145,  271 => 144,  267 => 143,  263 => 142,  259 => 141,  252 => 137,  247 => 135,  243 => 133,  239 => 132,  233 => 129,  229 => 128,  225 => 126,  218 => 122,  215 => 121,  213 => 119,  209 => 118,  205 => 116,  203 => 115,  196 => 111,  193 => 110,  191 => 108,  186 => 106,  177 => 100,  174 => 99,  172 => 97,  167 => 95,  162 => 92,  153 => 65,  150 => 64,  148 => 61,  143 => 59,  134 => 53,  131 => 52,  125 => 48,  120 => 46,  106 => 35,  102 => 34,  95 => 29,  93 => 28,  83 => 22,  80 => 21,  74 => 18,  69 => 17,  66 => 16,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
