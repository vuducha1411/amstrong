<?php

/* ami/promotional_mechanics/items/promotion_4.html.twig */
class __TwigTemplate_31a417f2eb600abed8a23e4504208e29ab13af45fbec723bc284e02bf9776050 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-panel active\">
    <table class=\"table table-striped table-bordered table-hover\" id=\"promotional_4\">
        <thead>
        <tr>
            ";
        // line 5
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
            // line 6
            echo "                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                      data-target=\"tbody\"
                                                      data-description=\"#CheckAllBtn\"></th>
            ";
        }
        // line 10
        echo "            <th>Promotion Description</th>
            <th>Purchased SKU code</th>
            <th>Purchased SKU total</th>
            <th>Promotion applicable</th>
            <th>Promotion Free Gift</th>
            <th>From date</th>
            <th>To date</th>
            <th class=\"nosort text-center\"></th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotional_mechanics"]) ? $context["promotional_mechanics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promotinal"]) {
            // line 22
            echo "            <tr>
                ";
            // line 23
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics"))) {
                // line 24
                echo "                    <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["promotinal"], "id", array())));
                echo "</td>
                ";
            }
            // line 26
            echo "                <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "sub_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "purchased_sku_number", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "x_mounts", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "free_sku_name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotinal"], "promotion_free_gift", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "from_date", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["promotinal"], "range", array()), "to_date", array()), "html", null, true);
            echo "</td>
                <td class=\"center text-center\" style=\"min-width: 80px;\">
                    ";
            // line 34
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotional_mechanics")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotional_mechanics")))) {
                // line 35
                echo "                        <div class=\"btn-group\">
                            ";
                // line 36
                echo html_btn(site_url(((("ami/promotional_mechanics/edit/" . $this->getAttribute($context["promotinal"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                            ";
                // line 37
                echo html_btn(site_url(("ami/promotional_mechanics/delete/" . $this->getAttribute($context["promotinal"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete ", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
                        </div>
                    ";
            }
            // line 40
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotinal'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/promotional_mechanics/items/promotion_4.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 43,  106 => 40,  100 => 37,  96 => 36,  93 => 35,  91 => 34,  86 => 32,  82 => 31,  78 => 30,  74 => 29,  70 => 28,  66 => 27,  61 => 26,  55 => 24,  53 => 23,  50 => 22,  46 => 21,  33 => 10,  27 => 6,  25 => 5,  19 => 1,);
    }
}
