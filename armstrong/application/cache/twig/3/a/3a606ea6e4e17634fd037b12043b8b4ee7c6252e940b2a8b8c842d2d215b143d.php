<?php

/* ami/recipes/index.html.twig */
class __TwigTemplate_3a606ea6e4e17634fd037b12043b8b4ee7c6252e940b2a8b8c842d2d215b143d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/recipes/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('.dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/recipes/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        \"iDisplayLength\": 50
    });
});
</script>
";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
        // line 32
        echo "
    ";
        // line 33
        echo form_open(site_url("ami/recipes/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 38
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Recipes Draft") : ("Recipes"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 41
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 43
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/recipes/index.html.twig", 43)->display(array_merge($context, array("url" => "ami/recipes", "icon" => "fa-cutlery", "title" => "Recipes", "permission" => "recipes")));
        // line 44
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#listed\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Listed</a></li>
                <li role=\"presentation\"><a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Delisted</a></li>
            </ul>

            <div class=\"panel panel-default m-t\"> 
                                
                <div class=\"panel-body tab-content\">                
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 65
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes"))) {
            // line 66
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 68
        echo "                                    <th>Recipe Name</th>
                                    <th>Recipe Name (Other Language)</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Updated By</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 77
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["recipes"]) ? $context["recipes"] : null), "active", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["recipe"]) {
            // line 78
            echo "                                    <tr>
                                        ";
            // line 79
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes"))) {
                // line 80
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["recipe"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 82
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/recipes/edit/" . $this->getAttribute($context["recipe"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "sku_name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim($this->getAttribute($context["recipe"], "name", array()), 40);
            echo "</a></td>
                                        <td>";
            // line 83
            echo wordTrim($this->getAttribute($context["recipe"], "name_alt", array()), 30);
            echo "</td>
                                        <td class=\"center\">";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 86
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["recipe"], "updated_name", array()) != "")) ? ($this->getAttribute($context["recipe"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 88
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "recipes")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes")))) {
                // line 89
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/recipes/index.html.twig", 89)->display(array_merge($context, array("url" => "ami/recipes", "id" => $this->getAttribute($context["recipe"], "id", array()), "permission" => "recipes")));
                // line 90
                echo "                                            ";
            }
            // line 91
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                            </tbody>
                        </table>                        
                    </div>

                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 102
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes"))) {
            // line 103
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 105
        echo "                                    <th>Recipe Name</th>
                                    <th>Recipe Name (Other Language)</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Updated By</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["recipes"]) ? $context["recipes"] : null), "inactive", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["recipe"]) {
            // line 115
            echo "                                    <tr>
                                        ";
            // line 116
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes"))) {
                // line 117
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["recipe"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 119
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/recipes/edit/" . $this->getAttribute($context["recipe"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "sku_name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim($this->getAttribute($context["recipe"], "name", array()), 40);
            echo "</a></td>
                                        <td>";
            // line 120
            echo wordTrim($this->getAttribute($context["recipe"], "name_alt", array()), 30);
            echo "</td>
                                        <td class=\"center\">";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["recipe"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 123
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["recipe"], "updated_name", array()) != "")) ? ($this->getAttribute($context["recipe"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 125
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "recipes")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "recipes")))) {
                // line 126
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/recipes/index.html.twig", 126)->display(array_merge($context, array("url" => "ami/recipes", "id" => $this->getAttribute($context["recipe"], "id", array()), "permission" => "recipes")));
                // line 127
                echo "                                            ";
            }
            // line 128
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "                            </tbody>
                        </table>                        
                    </div>
                </div>            
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 140
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/recipes/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 140,  330 => 131,  314 => 128,  311 => 127,  308 => 126,  306 => 125,  301 => 123,  297 => 122,  293 => 121,  289 => 120,  280 => 119,  274 => 117,  272 => 116,  269 => 115,  252 => 114,  241 => 105,  237 => 103,  235 => 102,  225 => 94,  209 => 91,  206 => 90,  203 => 89,  201 => 88,  196 => 86,  192 => 85,  188 => 84,  184 => 83,  175 => 82,  169 => 80,  167 => 79,  164 => 78,  147 => 77,  136 => 68,  132 => 66,  130 => 65,  107 => 44,  105 => 43,  100 => 41,  94 => 38,  86 => 33,  83 => 32,  80 => 31,  74 => 28,  70 => 27,  67 => 26,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
