<?php

/* ami/check_list/edit.html.twig */
class __TwigTemplate_3c9024f9c00bf1ce8a1d945d51866ddf6ea915bceecc53b00f42b9613c36e906 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/check_list/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script>

</script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
<script>
    CKEDITOR.replace('inputContent', {
        customConfig: '";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
    });

    CKEDITOR.on('instanceReady', function(){
        \$.each( CKEDITOR.instances, function(instance) {
            CKEDITOR.instances[instance].on(\"change\", function(e) {
                for ( instance in CKEDITOR.instances ) {    
                    CKEDITOR.instances[instance].updateElement();
                }
            });
        });
    });

</script>
";
    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        // line 28
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 43
        echo form_open(site_url("ami/check_list/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Check List</h1>
                <div class=\"text-right\">
                    ";
        // line 50
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/check_list/edit.html.twig", 50)->display(array_merge($context, array("url" => "ami/check_list", "id" => $this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "id", array()), "permission" => "check_list")));
        // line 51
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    ";
        // line 60
        echo form_label("Title", "name", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 62
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 67
        echo form_label("", "active", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <label class=\"radio-inline\">";
        // line 69
        echo form_radio("active", 1, ($this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                        <label class=\"radio-inline\">";
        // line 70
        echo form_radio("active", 0, ($this->getAttribute((isset($context["check_list"]) ? $context["check_list"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                    </div>
                </div>
                ";
        // line 73
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div> 

    ";
        // line 77
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/check_list/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 77,  135 => 73,  129 => 70,  125 => 69,  120 => 67,  112 => 62,  107 => 60,  96 => 51,  94 => 50,  84 => 43,  67 => 28,  64 => 27,  45 => 11,  39 => 8,  32 => 4,  29 => 3,  11 => 1,);
    }
}
