<?php

/* ami/kpi_target_in_sales_cycle/index1.html.twig */
class __TwigTemplate_3473b5756d2cf73fbb2b12da80942c771e98e5a25201e433d17513faed7ad0ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/kpi_target_in_sales_cycle/index1.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(document).ready(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/kpi_target_in_sales_cycle/ajaxData"), "html", null, true);
        echo "\",*/
                \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
                'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });
    </script>
";
    }

    // line 28
    public function block_css($context, array $blocks = array())
    {
        // line 29
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 33
    public function block_content($context, array $blocks = array())
    {
        // line 34
        echo "
    ";
        // line 35
        echo form_open(site_url("ami/kpi_target_in_sales_cycle/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 40
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("KPI Settings Draft") : ("KPI In Sales Cycle Settings"));
        echo "</h1>
                <div class=\"text-right\">
                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-warning\" href=\"javascript:;\" data-toggle=\"modal\"
                           data-target=\"#upload_template\" title=\"Upload Kpi Actual\"><i
                                    class=\"fa fw fa-upload\"></i> Upload Actual</a>
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, site_url("ami/kpi_target_in_sales_cycle/add"), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                        <a class=\"hide\" id=\"CheckAllBtn\">
                            ";
        // line 49
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                        </a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        ";
        // line 61
        echo "        ";
        if ($this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array())) {
            // line 62
            echo "            <div class=\"alert ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "flash_type", array()), "html", null, true);
            echo " alert-dismissible col-lg-12\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                            aria-hidden=\"true\">&times;</span></button>
                <strong>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session_flash_message"]) ? $context["session_flash_message"] : null), "message", array()), "html", null, true);
            echo "</strong>
            </div>
        ";
        }
        // line 68
        echo "        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 76
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target_in_sales_cycle"))) {
            // line 77
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 81
        echo "                                <th>ID</th>
                                <th>Name</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 91
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 92
            echo "                                <tr>
                                    ";
            // line 93
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target_in_sales_cycle"))) {
                // line 94
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 96
            echo "                                    <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/kpi_target_in_sales_cycle/edit/" . $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\"
                                           title=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\"
                                           data-toggle=\"ajaxModal\">";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "</a></td>
                                    <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_name", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "from_date", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "to_date", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center\">";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        ";
            // line 105
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "kpi_target_in_sales_cycle")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "kpi_target_in_sales_cycle")))) {
                // line 106
                echo "                                            ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/kpi_target_in_sales_cycle/index1.html.twig", 106)->display(array_merge($context, array("url" => "ami/kpi_target_in_sales_cycle", "id" => $this->getAttribute($context["post"], "id", array()), "permission" => "kpi_target_in_sales_cycle")));
                // line 107
                echo "                                        ";
            }
            // line 108
            echo "                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 115
        $context["paginationUrl"] = (((isset($context["draft"]) ? $context["draft"] : null)) ? ("ami/kpi_target_in_sales_cycle/draft") : ("ami/kpi_target_in_sales_cycleƯ"));
        // line 116
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => site_url((isset($context["paginationUrl"]) ? $context["paginationUrl"] : null)), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 130
        echo form_close();
        echo "
    <div class=\"modal fade\" id=\"upload_template\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog\">
            ";
        // line 133
        echo form_open(site_url("ami/kpi_target_in_sales_cycle/upload_template"), array("class" => "modal-content", "id" => "upload_form", "enctype" => "multipart/form-data"));
        echo "
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                <h3 id=\"myModalLabel\">Upload Template</h3>
            </div>
            <div class=\"modal-body\">
                ";
        // line 140
        echo "                <div class=\"col-sm-12\">
                    <div class=\"form-group row\">
                        ";
        // line 142
        echo form_label("Date Range", "range_id", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-9\">
                            ";
        // line 144
        echo form_dropdown("range_id", (isset($context["range"]) ? $context["range"] : null), null, "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                        </div>
                    </div>
                    <div class=\"form-group row\">
                        ";
        // line 148
        echo form_label("File", "file", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-9\">
                            <input type=\"file\" name=\"file\">
                        </div>
                    </div>
                    <div class=\"form-group row\">
                        <div class=\"col-sm-offset-3\">
                            <a href=\"";
        // line 155
        echo twig_escape_filter($this->env, site_url("res/up/kpi_actual_in_sales_cycle_template.xlsx"), "html", null, true);
        echo "\" class=\"bg-info\">Click here to download example template.</a>
                        </div>
                    </div>
                </div>
                ";
        // line 160
        echo "            </div>
            <div class=\"modal-footer\">
                <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close
                </button>
                ";
        // line 164
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Proceed", "class" => "btn btn-danger"));
        echo "
            </div>
            ";
        // line 166
        echo form_close();
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/kpi_target_in_sales_cycle/index1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 166,  341 => 164,  335 => 160,  328 => 155,  318 => 148,  311 => 144,  306 => 142,  302 => 140,  293 => 133,  287 => 130,  269 => 116,  267 => 115,  261 => 111,  245 => 108,  242 => 107,  239 => 106,  237 => 105,  232 => 103,  228 => 102,  224 => 101,  220 => 100,  214 => 99,  210 => 98,  206 => 97,  201 => 96,  195 => 94,  193 => 93,  190 => 92,  173 => 91,  161 => 81,  155 => 77,  153 => 76,  143 => 68,  137 => 65,  130 => 62,  127 => 61,  113 => 49,  107 => 46,  98 => 40,  90 => 35,  87 => 34,  84 => 33,  78 => 30,  73 => 29,  70 => 28,  54 => 15,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
