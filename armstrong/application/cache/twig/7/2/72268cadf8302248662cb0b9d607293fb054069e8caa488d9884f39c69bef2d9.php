<?php

/* ami/potential_contacts/edit.html.twig */
class __TwigTemplate_72268cadf8302248662cb0b9d607293fb054069e8caa488d9884f39c69bef2d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/potential_contacts/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery.signaturepad.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        function removeRequireInput(input){
            \$('#InputCustomers').removeAttr('data-parsley-required');
            \$('#InputWholesalers').removeAttr('data-parsley-required');
            \$('#InputDistributors').removeAttr('data-parsley-required');
            input.attr('data-parsley-required', \"true\");
        }
        \$(function () {
            createUploader(\$('#imageUpload'));

            \$('#InputCustomers').autocomplete({
                source: \"";
        // line 21
        echo twig_escape_filter($this->env, site_url("ami/potential_customers/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    removeRequireInput(\$(this));
                    \$('#InputCustomersId').val(ui.item.value);
                    \$('#InputDistributors').val('');
                    \$('#InputDistributorsId').val('');
                    \$('#InputWholesalers').val('');
                    \$('#InputWholesalersId').val('');
                    check_primary_contact();
                }
            }).on('focus', function () {
                this.select();
            });


            \$.listen('parsley:form:validate', function (formInstance) {

                if (!\$('#InputWholesalers').val()
                        && !\$('#InputDistributors').val()
                        && !\$('#InputCustomers').val()
                ) {
                    // window.ParsleyUI.addError(\$('.parsley-need-one').parsley(), 'name', 'message');
                    formInstance.submitEvent.preventDefault();
                }
            });
            check_primary_contact();
        });

        function check_primary_contact() {
            var val = \$('#InputCustomers').val();
            if (val) {
                \$.ajax({
                    url: \"";
        // line 56
        echo twig_escape_filter($this->env, site_url("ami/potential_contacts/check_primary_contact"), "html", null, true);
        echo "\",
                    data: {customers_id: val},
                    type: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        if (res == true) {
                            \$('#cusAlert').html('');
                        } else {
                            \$('#cusAlert').html('This Customer has\\'nt got contact primary !');
                        }
                    }
                });
            }
        }

        \$(document).ready(function () {
            ";
        // line 72
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 1))) {
            // line 73
            echo "            var canvas = document.getElementById(\"theCanvas\"),
                    sigInput = \$('input[name=\"signature_base64\"]'),
                    signature;

            ";
            // line 77
            if (($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()) == "")) {
                // line 78
                echo "            \$('.show-sig').hide();
            \$('#smoothed').show();
            canvas.width = \$(\"#smoothed\").width() - 4;
            signature = \$('#smoothed').signaturePad({
                drawOnly: false,
                lineTop: 220,
                lineColour: '#111',
                lineWidth: 1,
                autoscale: true,
                drawBezierCurves: true
            });
            ";
            } else {
                // line 90
                echo "            \$('.show-sig').show();
            \$('#smoothed').hide();
            ";
            }
            // line 93
            echo "            \$('.changeButton').click(function () {
                \$('.show-sig').hide();
                \$('#smoothed').show();
                canvas.width = \$(\"#smoothed\").width() - 4;
                signature = \$('#smoothed').signaturePad({
                    lineTop: 220,
                    lineColour: '#111',
                    lineWidth: 1,
                    autoscale: true,
                    drawBezierCurves: true
                });
            });

            \$('button[type=\"submit\"]').click(function (e) {
                e.preventDefault();
                if (\$('.output').val()) {
                    var signatureimg = signature.getSignatureImage();
                    signatureimg = signatureimg.replace('data:image/png;base64,', '');
                    sigInput.val(signatureimg);
                } else {
                    sigInput.val('";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()), "html", null, true);
            echo "');
                }
                \$('.contact-form').submit();
            });
            ";
        }
        // line 118
        echo "        });
    </script>
";
    }

    // line 122
    public function block_css($context, array $blocks = array())
    {
        // line 123
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 124
        echo twig_escape_filter($this->env, site_url("res/css/jquery.signaturepad.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 125
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
    <style>
        #theCanvas, .show-sig img {
            border-radius: 10px;
            border: 2px solid orange;
        }

        .sigPad {
            width: 100%;
        }

        .sigWrapper {
            border: none;
        }

        .show-sig img {
            width: 100%;
        }

        .changeButton {
            display: list-item;
            bottom: 0.2em;
            position: absolute;
            right: 0;
            font-size: 0.75em;
            line-height: 1.375;

        }
    </style>
";
    }

    // line 156
    public function block_content($context, array $blocks = array())
    {
        // line 157
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 172
        echo form_open(site_url("ami/potential_contacts/save"), array("class" => "form-horizontal contact-form", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Potential Contacts</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 181
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 183
        echo "                        ";
        echo html_btn(site_url("ami/potential_contacts"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 184
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())) {
            // line 185
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/potential_contacts/delete/" . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 188
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 199
        echo form_label("Potential Customers", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 201
        echo form_input(array("name" => "armstrong_1_customers_id", "value" => $this->getAttribute((isset($context["potential_customers"]) ? $context["potential_customers"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_1_customers_id", array()), array(), "array"), "class" => "form-control", "data-parsley-required" => "true", "id" => "InputCustomers"));
        // line 205
        echo "
                    <span id=\"cusAlert\"></span>
                </div>
                <input type=\"hidden\" name=\"tag_id[]\" id=\"InputCustomersId\"
                       value=\"";
        // line 209
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 0)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
            </div>

            <div class=\"form-group\">
                ";
        // line 213
        echo form_label("Primary Contact", "primary_contact", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 215
        echo form_radio("primary_contact", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 0));
        echo "
                        No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 217
        echo form_radio("primary_contact", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 1));
        echo "
                        Yes</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 223
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 225
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 230
        echo form_label("Position", "position", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 232
        echo form_input(array("name" => "position", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "position", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 237
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-4\">
                    ";
        // line 239
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
                <div class=\"col-sm-2\">
                    ";
        // line 242
        echo form_input(array("name" => "ext", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "ext", array()), "class" => "form-control", "placeholder=\"Ext\" maxlength=\"5\" data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 247
        echo form_label("Phone 2", "phone_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 249
        echo form_input(array("name" => "phone_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_2", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 254
        echo form_label("Phone 3", "phone_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 256
        echo form_input(array("name" => "phone_3", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_3", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 261
        echo form_label("Mobile", "mobile", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 263
        echo form_input(array("name" => "mobile", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 268
        echo form_label("Mobile 2", "mobile_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 270
        echo form_input(array("name" => "mobile_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile_2", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 275
        echo form_label("Mobile 3", "mobile_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 277
        echo form_input(array("name" => "mobile_3", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile_3", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 282
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 284
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 289
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 291
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 296
        echo form_label("Email 2", "email_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 298
        echo form_input(array("name" => "email_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email_2", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 303
        echo form_label("Birthday", "birthday", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 305
        echo form_input(array("name" => "birthday", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "birthday", array()), "class" => "form-control", "data-provide" => "datepicker", "data-date-format" => "yyyy-mm-dd", "data-date-start-date" => "-50y"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 310
        echo form_label("Interests", "interests", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 312
        echo form_input(array("name" => "interests", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "interests", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 317
        echo form_label("", "status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 319
        echo form_radio("status", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 320
        echo form_radio("status", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 325
        echo form_label("Remarks", "remarks", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 327
        echo form_input(array("name" => "remarks", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "remarks", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 332
        echo form_label("Facebook", "facebook", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 334
        echo form_input(array("name" => "facebook", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "facebook", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 339
        echo form_label("Instagram", "instagram", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 341
        echo form_input(array("name" => "instagram", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "instagram", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 346
        echo form_label("Linkedln", "linkedln", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 348
        echo form_input(array("name" => "linkedln", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "linkedln", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 352
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/potential_contacts/edit.html.twig", 352)->display(array_merge($context, array("entry_type" => "contacts")));
        // line 353
        echo "            ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 1))) {
            // line 354
            echo "                <div class=\"form-group\">
                    ";
            // line 355
            echo form_label("Signature", "signature", array("class" => "control-label col-sm-3"));
            echo "

                    <div class=\"col-sm-6 show-sig\">
                        <ul class=\"sigNav\" style=\"display: block;\">
                            <li class=\"changeButton\"><a href=\"javascript:;\">Change</a></li>
                        </ul>
                        <img src=\"data:image/png;base64,";
            // line 361
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()), "html", null, true);
            echo "\" alt=\"\">
                    </div>

                    <div class=\"col-sm-6\" id=\"smoothed\">
                        <ul class=\"sigNav\">
                            <li class=\"drawIt\"><a href=\"#draw-it\" class=\"current\">Draw Signature</a></li>
                            <li class=\"clearButton\"><a href=\"#clear\">Clear</a></li>
                        </ul>
                        <div class=\"sig sigWrapper \" style=\"height:auto;\">
                            <div class=\"typed\"></div>
                            <canvas id=\"theCanvas\" class=\"pad\" height=\"250\"></canvas>
                            <input type=\"hidden\" name=\"output\" class=\"output\">
                        </div>
                    </div>

                </div>
            ";
        }
        // line 378
        echo "            ";
        echo form_input(array("name" => "signature_base64", "type" => "hidden", "class" => "hide"));
        echo "
            ";
        // line 379
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 383
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/potential_contacts/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  631 => 383,  624 => 379,  619 => 378,  599 => 361,  590 => 355,  587 => 354,  584 => 353,  582 => 352,  575 => 348,  570 => 346,  562 => 341,  557 => 339,  549 => 334,  544 => 332,  536 => 327,  531 => 325,  523 => 320,  519 => 319,  514 => 317,  506 => 312,  501 => 310,  493 => 305,  488 => 303,  480 => 298,  475 => 296,  467 => 291,  462 => 289,  454 => 284,  449 => 282,  441 => 277,  436 => 275,  428 => 270,  423 => 268,  415 => 263,  410 => 261,  402 => 256,  397 => 254,  389 => 249,  384 => 247,  376 => 242,  370 => 239,  365 => 237,  357 => 232,  352 => 230,  344 => 225,  339 => 223,  330 => 217,  325 => 215,  320 => 213,  313 => 209,  307 => 205,  305 => 201,  300 => 199,  287 => 188,  280 => 185,  278 => 184,  273 => 183,  269 => 181,  257 => 172,  240 => 157,  237 => 156,  203 => 125,  199 => 124,  194 => 123,  191 => 122,  185 => 118,  177 => 113,  155 => 93,  150 => 90,  136 => 78,  134 => 77,  128 => 73,  126 => 72,  107 => 56,  69 => 21,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
