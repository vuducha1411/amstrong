<?php

/* ami/discount_model/edit_range.html.twig */
class __TwigTemplate_7267e4d1a642033625d429df26b241dec5da83f76eabf2ceb311325073bcdc50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/discount_model/edit_range.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "    ";
        echo form_open(site_url("ami/discount_model/save_range"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Range</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 44
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 46
        echo html_btn(site_url("ami/discount_model/range"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 47
        if ($this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array())) {
            // line 48
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/discount_model/delete_range/" . $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 51
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 61
        echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 63
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 67
        echo form_label("From", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 69
        echo form_input(array("name" => "from_date", "value" => $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "from_date", array()), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 73
        echo form_label("To", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 75
        echo form_input(array("name" => "to_date", "value" => $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "to_date", array()), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 76
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 77
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 81
        echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 84
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 86
        echo form_radio("active", 0, ($this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "active", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 87
        echo form_radio("active", 1, ($this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "active", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>

            ";
        // line 91
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["discount_model_range"]) ? $context["discount_model_range"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 96
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/discount_model/edit_range.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 96,  202 => 91,  195 => 87,  191 => 86,  186 => 84,  181 => 81,  175 => 78,  172 => 77,  170 => 76,  166 => 75,  161 => 73,  154 => 69,  149 => 67,  142 => 63,  137 => 61,  125 => 51,  118 => 48,  116 => 47,  112 => 46,  107 => 44,  94 => 35,  91 => 34,  85 => 31,  80 => 30,  77 => 29,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
