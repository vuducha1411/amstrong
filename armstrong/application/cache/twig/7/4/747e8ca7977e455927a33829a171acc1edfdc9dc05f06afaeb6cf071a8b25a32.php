<?php

/* ami/shifted_reason/edit.html.twig */
class __TwigTemplate_747e8ca7977e455927a33829a171acc1edfdc9dc05f06afaeb6cf071a8b25a32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/shifted_reason/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 10
        echo "    ";
        // line 11
        echo "    <script>
        \$(function () {
            \$('#expiry_date').datetimepicker({
              //  format: 'YYYY-MM-DD HH:II:ss'
            });
        });
    </script>
";
    }

    // line 20
    public function block_css($context, array $blocks = array())
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    ";
    }

    // line 25
    public function block_content($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        echo form_open(site_url("ami/shifted_reason/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Shifted Reason</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 35
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 37
        echo "                        ";
        echo html_btn(site_url("ami/shifted_reason"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 38
        if ($this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "id", array())) {
            // line 39
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/shifted_reason/delete/" . $this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 42
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 53
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 55
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 60
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 62
        echo form_radio("active", 0, ($this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "active", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 63
        echo form_radio("active", 1, ($this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "active", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>
            ";
        // line 66
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["shifted_reason"]) ? $context["shifted_reason"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 71
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/shifted_reason/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 71,  149 => 66,  143 => 63,  139 => 62,  134 => 60,  126 => 55,  121 => 53,  108 => 42,  101 => 39,  99 => 38,  94 => 37,  90 => 35,  77 => 26,  74 => 25,  67 => 21,  64 => 20,  53 => 11,  51 => 10,  47 => 8,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
