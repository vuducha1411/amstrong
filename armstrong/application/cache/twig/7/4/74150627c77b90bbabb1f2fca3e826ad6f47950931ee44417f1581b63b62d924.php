<?php

/* ami/components/form_btn_save.html.twig */
class __TwigTemplate_74150627c77b90bbabb1f2fca3e826ad6f47950931ee44417f1581b63b62d924 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<div class=\"btn-header-toolbar\">
    ";
        // line 4
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null))) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", (isset($context["permission"]) ? $context["permission"] : null))))) {
            // line 5
            echo "        ";
            echo form_button(array("type" => "button", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success", "onClick" => "saveRecord()"));
            echo "
    ";
        }
        // line 7
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/form_btn_save.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 7,  26 => 5,  24 => 4,  19 => 1,);
    }
}
