<?php

/* ami/contacts/edit_.html.twig */
class __TwigTemplate_7fb6fdcd8f7940be9739fef84249ac2a9dddb1db3b07f95742c148951353019f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/contacts/edit_.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery.signaturepad.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script>

        \$(function () {
            createUploader(\$('#imageUpload'));

            \$('#InputCustomers').autocomplete({
                source: \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputCustomersId').val(ui.item.value);
                    \$('#InputDistributors').val('');
                    \$('#InputDistributorsId').val('');
                    \$('#InputWholesalers').val('');
                    \$('#InputWholesalersId').val('');
                    check_primary_contact();
                }
            }).on('focus', function () {
                this.select();
            });

            \$('#InputWholesalers').autocomplete({
                source: \"";
        // line 33
        echo twig_escape_filter($this->env, site_url("ami/wholesalers/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputWholesalersId').val(ui.item.value);

                    \$('#InputCustomers').val('');
                    \$('#InputCustomersId').val('');
                    \$('#InputDistributors').val('');
                    \$('#InputDistributorsId').val('');
                }
            }).on('focus', function () {
                this.select();
            });

            \$('#InputDistributors').autocomplete({
                source: \"";
        // line 50
        echo twig_escape_filter($this->env, site_url("ami/distributors/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    \$('#InputDistributorsId').val(ui.item.value);

                    \$('#InputCustomers').val('');
                    \$('#InputCustomersId').val('');
                    \$('#InputWholesalers').val('');
                    \$('#InputWholesalersId').val('');
                }
            }).on('focus', function () {
                this.select();
            });

            \$.listen('parsley:form:validate', function (formInstance) {

                if (!\$('#InputWholesalers').val()
                        && !\$('#InputDistributors').val()
                        && !\$('#InputCustomers').val()
                ) {
                    // window.ParsleyUI.addError(\$('.parsley-need-one').parsley(), 'name', 'message');
                    formInstance.submitEvent.preventDefault();
                }
            });
            check_primary_contact();
        });

        function check_primary_contact() {
            var val = \$('#InputCustomers').val();
            if (val) {
                \$.ajax({
                    url: \"";
        // line 83
        echo twig_escape_filter($this->env, site_url("ami/contacts/check_primary_contact"), "html", null, true);
        echo "\",
                    data: {customers_id: val},
                    type: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        if (res == true) {
                            \$('#cusAlert').html('');
                        } else {
                            \$('#cusAlert').html('This Customer has\\'nt got contact primary !');
                        }
                    }
                });
            }
        }

        \$(document).ready(function () {
            var canvas = document.getElementById(\"theCanvas\"),
                    sigInput = \$('input[name=\"signature_base64\"]'),
                    signature;

            ";
        // line 103
        if (($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()) == "")) {
            // line 104
            echo "            \$('.show-sig').hide();
            \$('#smoothed').show();
            canvas.width = \$(\"#smoothed\").width() - 4;
            signature = \$('#smoothed').signaturePad({
                drawOnly: false,
                lineTop: 220,
                lineColour: '#111',
                lineWidth: 1,
                autoscale: true,
                drawBezierCurves: true
            });
            ";
        } else {
            // line 116
            echo "            \$('.show-sig').show();
            \$('#smoothed').hide();
            ";
        }
        // line 119
        echo "            \$('.changeButton').click(function () {
                \$('.show-sig').hide();
                \$('#smoothed').show();
                canvas.width = \$(\"#smoothed\").width() - 4;
                signature = \$('#smoothed').signaturePad({
                    lineTop: 220,
                    lineColour: '#111',
                    lineWidth: 1,
                    autoscale: true,
                    drawBezierCurves: true
                });
            });

            \$('button[type=\"submit\"]').click(function (e) {
                e.preventDefault();
                if(\$('.output').val()){
                    var signatureimg = signature.getSignatureImage();
                    signatureimg = signatureimg.replace('data:image/png;base64,', '');
                    sigInput.val(signatureimg);
                }else{
                    sigInput.val('";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()), "html", null, true);
        echo "');
                }
                \$('.contact-form').submit();
            });
        });
    </script>
";
    }

    // line 147
    public function block_css($context, array $blocks = array())
    {
        // line 148
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 149
        echo twig_escape_filter($this->env, site_url("res/css/jquery.signaturepad.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
    <style>
        #theCanvas, .show-sig img {
            border-radius: 10px;
            border: 2px solid orange;
        }

        .sigPad {
            width: 100%;
        }

        .sigWrapper {
            border: none;
        }

        .show-sig img{
            width: 100%;
        }

        .changeButton {
            display: list-item;
            bottom: 0.2em;
            position: absolute;
            right: 0;
            font-size: 0.75em;
            line-height: 1.375;

        }
    </style>
";
    }

    // line 181
    public function block_content($context, array $blocks = array())
    {
        // line 182
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 197
        echo form_open(site_url("ami/contacts/save"), array("class" => "form-horizontal contact-form", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Contacts</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 206
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 208
        echo "                        ";
        echo html_btn(site_url("ami/contacts"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 209
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())) {
            // line 210
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/contacts/delete/" . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 213
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 224
        echo form_label("Customers", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 226
        echo form_input(array("name" => "armstrong_2_customers_id", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), "class" => "form-control", "id" => "InputCustomers", "data-parsley-required" => "true"));
        // line 230
        echo "
                    <span id=\"cusAlert\"></span>
                </div>
                <input type=\"hidden\" name=\"tag_id[]\" id=\"InputCustomersId\"
                       value=\"";
        // line 234
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 0)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
            </div>

            <div class=\"form-group\">
                ";
        // line 238
        echo form_label("Wholesaler", "armstrong_2_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 240
        echo form_input(array("name" => "armstrong_2_wholesalers_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputWholesalers"));
        // line 243
        echo "
                    <input type=\"hidden\" name=\"tag_id[]\" id=\"InputWholesalersId\"
                           value=\"";
        // line 245
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 1)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 250
        echo form_label("Distributor", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 252
        echo form_input(array("name" => "armstrong_2_distributors_id", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputDistributors"));
        // line 255
        echo "
                    <input type=\"hidden\" name=\"tag_id[]\" id=\"InputDistributorsId\"
                           value=\"";
        // line 257
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 2)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 262
        echo form_label("Primary Contact", "primary_contact", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 264
        echo form_radio("primary_contact", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 0));
        echo "
                        No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 266
        echo form_radio("primary_contact", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 1));
        echo "
                        Yes</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 272
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 274
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 279
        echo form_label("Position", "position", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 281
        echo form_input(array("name" => "position", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "position", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 286
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 288
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 293
        echo form_label("Phone 2", "phone_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 295
        echo form_input(array("name" => "phone_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 300
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 302
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 307
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 309
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 314
        echo form_label("Email 2", "email_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 316
        echo form_input(array("name" => "email_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email_2", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 321
        echo form_label("Birthday", "birthday", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 323
        echo form_input(array("name" => "birthday", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "birthday", array()), "class" => "form-control", "data-provide" => "datepicker", "data-date-format" => "yyyy-mm-dd", "data-date-start-date" => "-50y"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 328
        echo form_label("Interests", "interests", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 330
        echo form_input(array("name" => "interests", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "interests", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 335
        echo form_label("", "status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 337
        echo form_radio("status", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 338
        echo form_radio("status", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 343
        echo form_label("Remarks", "remarks", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 345
        echo form_input(array("name" => "remarks", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "remarks", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 350
        echo form_label("Facebook", "facebook", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 352
        echo form_input(array("name" => "facebook", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "facebook", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 357
        echo form_label("Instagram", "instagram", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 359
        echo form_input(array("name" => "instagram", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "instagram", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 364
        echo form_label("Linkedln", "linkedln", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 366
        echo form_input(array("name" => "linkedln", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "linkedln", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 370
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/contacts/edit_.html.twig", 370)->display(array_merge($context, array("entry_type" => "contacts")));
        // line 371
        echo "
            <div class=\"form-group\">
                ";
        // line 373
        echo form_label("Signature", "signature", array("class" => "control-label col-sm-3"));
        echo "

                <div class=\"col-sm-6 show-sig\">
                    <ul class=\"sigNav\" style=\"display: block;\">
                        <li class=\"changeButton\"><a href=\"javascript:;\">Change</a></li>
                    </ul>
                    <img src=\"data:image/png;base64,";
        // line 379
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()), "html", null, true);
        echo "\" alt=\"\">
                </div>

                <div class=\"col-sm-6\" id=\"smoothed\">
                    <ul class=\"sigNav\">
                        <li class=\"drawIt\"><a href=\"#draw-it\" class=\"current\">Draw Signature</a></li>
                        <li class=\"clearButton\"><a href=\"#clear\">Clear</a></li>
                    </ul>
                    <div class=\"sig sigWrapper \" style=\"height:auto;\">
                        <div class=\"typed\"></div>
                        <canvas id=\"theCanvas\" class=\"pad\" height=\"250\"></canvas>
                        <input type=\"hidden\" name=\"output\" class=\"output\">
                    </div>
                </div>

            </div>

            ";
        // line 396
        echo form_input(array("name" => "signature_base64", "type" => "hidden", "class" => "hide"));
        echo "
            ";
        // line 397
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 401
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/contacts/edit_.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  634 => 401,  627 => 397,  623 => 396,  603 => 379,  594 => 373,  590 => 371,  588 => 370,  581 => 366,  576 => 364,  568 => 359,  563 => 357,  555 => 352,  550 => 350,  542 => 345,  537 => 343,  529 => 338,  525 => 337,  520 => 335,  512 => 330,  507 => 328,  499 => 323,  494 => 321,  486 => 316,  481 => 314,  473 => 309,  468 => 307,  460 => 302,  455 => 300,  447 => 295,  442 => 293,  434 => 288,  429 => 286,  421 => 281,  416 => 279,  408 => 274,  403 => 272,  394 => 266,  389 => 264,  384 => 262,  376 => 257,  372 => 255,  370 => 252,  365 => 250,  357 => 245,  353 => 243,  351 => 240,  346 => 238,  339 => 234,  333 => 230,  331 => 226,  326 => 224,  313 => 213,  306 => 210,  304 => 209,  299 => 208,  295 => 206,  283 => 197,  266 => 182,  263 => 181,  229 => 150,  225 => 149,  220 => 148,  217 => 147,  206 => 139,  184 => 119,  179 => 116,  165 => 104,  163 => 103,  140 => 83,  104 => 50,  84 => 33,  64 => 16,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
