<?php

/* spinwin/header.html.twig */
class __TwigTemplate_78de8200cc8d1f7be33d439856130d86920c0e17536f5313c04af2e60111443a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
\t<div class=\"col-sm-3\">
\t\t<header class=\"header\">
\t\t\t<nav class=\"menu-nav\">
\t\t\t\t<ul class=\"mn_table\">
\t\t\t\t\t<li class=\"mn_menu win\">
\t\t\t\t\t\t<a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("spin_and_win"), "html", null, true);
        echo "\">Winners</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"mn_menu country\">
\t\t\t\t\t\t<a href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("spin_and_win/country_stats"), "html", null, true);
        echo "\">Country Stats</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"mn_menu coin\">
\t\t\t\t\t\t<a href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("spin_and_win/perfect_calls"), "html", null, true);
        echo "\">Perfect Calls</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"mn_menu view\"><a href=\"\">View by</a><img class=\"btn_next\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("res/img/spinwin/button_next.png"), "html", null, true);
        echo "\" />
\t\t\t\t\t\t<ul class=\"mn_table1\">
\t\t\t\t\t\t\t<li class=\"mn_menu\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("spin_and_win/filter/day"), "html", null, true);
        echo "\">Today</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"mn_menu\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("spin_and_win/filter/week"), "html", null, true);
        echo "\">This Week</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"mn_menu\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("spin_and_win/filter/season"), "html", null, true);
        echo "\">This Season</a>
\t\t\t\t\t\t\t</li>
                            ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($this->getAttribute((isset($context["season"]) ? $context["season"] : null), "season", array()) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 27
            echo "                                <li class=\"mn_menu\">
                                    <a href='";
            // line 28
            echo twig_escape_filter($this->env, site_url("spin_and_win/filter/season"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "'>Season ";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "
\t\t\t\t\t\t</ul>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</nav>
\t\t</header>
\t</div>
\t<div class=\"col-sm-6\"></div>
\t<div class=\"col-sm-3\">
\t\t<div class=\"logo\">        
\t\t\t<div class=\"text_logo small-font ";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "filter", array()), "html", null, true);
        echo "\">
\t\t\t\t<div class=\"text-logo-container\">
\t\t\t\t\t";
        // line 43
        $context["now"] = $this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method");
        // line 44
        echo "\t\t\t\t\t<p>
\t\t\t\t\t\t";
        // line 45
        if (($this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "filter", array()) == "day")) {
            // line 46
            echo "\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["now"]) ? $context["now"] : null), "format", array(0 => "jS F"), "method"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif (($this->getAttribute(        // line 47
(isset($context["filter"]) ? $context["filter"] : null), "filter", array()) == "week")) {
            // line 48
            echo "\t\t\t\t\t\t\tWeek ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["week"]) ? $context["week"] : null)), "html", null, true);
            echo "  of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["now"]) ? $context["now"] : null), "format", array(0 => "F"), "method"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif (($this->getAttribute(        // line 49
(isset($context["filter"]) ? $context["filter"] : null), "filter", array()) == "month")) {
            // line 50
            echo "\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["now"]) ? $context["now"] : null), "format", array(0 => "F"), "method"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        }
        // line 52
        echo "\t\t\t\t\t</p>
\t\t\t\t\t<p>GAME RESULTS</p>
\t\t\t\t\t<p>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["now"]) ? $context["now"] : null), "format", array(0 => "Y"), "method"), "html", null, true);
        echo "</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<img class=\"img-responsive align-center\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("res/img/spinwin/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"/>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "spinwin/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 57,  137 => 54,  133 => 52,  127 => 50,  125 => 49,  118 => 48,  116 => 47,  111 => 46,  109 => 45,  106 => 44,  104 => 43,  99 => 41,  87 => 31,  74 => 28,  71 => 27,  67 => 26,  62 => 24,  56 => 21,  50 => 18,  44 => 15,  39 => 13,  33 => 10,  27 => 7,  19 => 1,);
    }
}
