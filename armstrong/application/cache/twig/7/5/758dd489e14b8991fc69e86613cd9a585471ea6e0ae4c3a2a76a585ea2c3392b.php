<?php

/* ami/media_archive/index.html.twig */
class __TwigTemplate_758dd489e14b8991fc69e86613cd9a585471ea6e0ae4c3a2a76a585ea2c3392b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/media_archive/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "

<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/bootbox.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/numeral.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/moment.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/jquery.ajax-progress.js"), "html", null, true);
        echo "\"></script>

<script>
\$(document).ready(function() {
    \$('.dataTables').dataTable({
        'order': [[0, 'asc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        \"iDisplayLength\": 20
    });
});
</script>
";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<style>
.error {
    color: #ff0000;
    font-weight: bold;
    text-align: left;
}
</style>
";
    }

    // line 38
    public function block_content($context, array $blocks = array())
    {
        // line 39
        echo "
    ";
        // line 40
        echo form_open(site_url("ami/promotions/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["module_title"]) ? $context["module_title"] : null), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, (isset($context["app_type_title"]) ? $context["app_type_title"] : null), "html", null, true);
        echo "</h1>
                <div class=\"text-right\">
                    <div class=\"btn-group btn-header-toolbar\">
                        <a href=\"";
        // line 48
        echo twig_escape_filter($this->env, site_url("ami/media_archive/pull"), "html", null, true);
        echo "\" class=\"btn btn-sm btn-success app_type pull ";
        echo twig_escape_filter($this->env, (isset($context["app_type_pull_button_state"]) ? $context["app_type_pull_button_state"] : null), "html", null, true);
        echo "\" title=\"Pull\" data-app-type=\"0\">PULL</a>
                        <a href=\"";
        // line 49
        echo twig_escape_filter($this->env, site_url("ami/media_archive/push"), "html", null, true);
        echo "\" class=\"btn btn-sm btn-warning app_type push ";
        echo twig_escape_filter($this->env, (isset($context["app_type_push_button_state"]) ? $context["app_type_push_button_state"] : null), "html", null, true);
        echo "\" title=\"Push\" data-app-type=\"1\">PUSH</a>
                        <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, site_url("ami/media_archive/sl"), "html", null, true);
        echo "\" class=\"btn btn-sm btn-primary app_type sl ";
        echo twig_escape_filter($this->env, (isset($context["app_type_sl_button_state"]) ? $context["app_type_sl_button_state"] : null), "html", null, true);
        echo "\" title=\"SL\" data-app-type=\"2\">SL</a>
                    </div>
                </div>

                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"panel panel-default m-t\"> 
                                
                <div class=\"panel-body tab-content\">                
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    <th title=\"Country ID\">#</th>
                                    <th class=\"text-center\">Country</th>
                                    <th class=\"text-center\">Size</th>
                                    <th class=\"text-center\">Last Archive</th>
                                    <!-- <th class=\"text-center\">On Queue</th> -->
                                    <th class=\"nosort text-center\"><!-- buttons --></th>
                                    <th class=\"text-center\">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 79
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["media_archive"]) ? $context["media_archive"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["archive"]) {
            echo "    
                                    <tr class=\"archive-row ";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "id", array()), "html", null, true);
            echo "\">
                                        <td class=\"text-center\">";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "id", array()), "html", null, true);
            echo "</td>
                                        <td><img src=\"";
            // line 82
            echo twig_escape_filter($this->env, site_url("res/img/flags"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "code", array()), "html", null, true);
            echo ".png\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"text-center size\">";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "size", array()), "html", null, true);
            echo "</td>
                                        <td class=\"text-center\">
                                                <span class=\"date-last-archive\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "updated_at", array()), "html", null, true);
            echo "</span>
                                                <br><small>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "time_zone", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "gmt", array()), "html", null, true);
            echo "</small>
                                        </td>
                                        <!-- <td class=\"text-center\">-</td> -->
                                        <td class=\"text-center\">
                                            <div class=\"btn-group\">
                                                <a href=\"#\" class=\"btn btn-default archive\" title=\"Archive\" data-country-id=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "id", array()), "html", null, true);
            echo "\"><i class=\"fa fa-archive\"></i></a>
                                                <a href=\"#\" class=\"btn btn-default download\" title=\"Download\" data-country-id=\"";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["archive"], "id", array()), "html", null, true);
            echo "\"><i class=\"fa fa-download\"></i></a>
                                            </div>
                                        </td>
                                        <td class=\"text-center\">
                                            <span class=\"status error\" style=\"display: none;\"></span>
                                            <img class=\"loader\" src=\"";
            // line 97
            echo twig_escape_filter($this->env, site_url("res/img/loader.gif"), "html", null, true);
            echo "\" style=\"display: none;\">
                                        </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archive'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "                            </tbody>
                        </table>                        
                    </div>
                </div>            
            </div>
        </div>
    </div>

    ";
        // line 109
        echo form_close();
        echo "
    
<script>
    \$(document).ready(function () {
        (function(){
            var format = {
                data: function(elem) {
                    \$(elem, this).each(function(i){
                        var size = \$(this).find('.size').text();

                        if (size != 'NA') {
                            \$(this).find('.size').html(numeral(size).format('0 b'))
                        }
                        
                        var date = \$(this).find('.date-last-archive').text();

                        if (date != 'NA') {
                            \$(this).find('.date-last-archive').html(moment(date).format('DD-MMM-YYYY, hh:mm a'));
                        }
                    });
                }
            };

            format.data(\$('.archive-row'));

            var app_type = \$('.app_type.active').data('appType');

            \$('.archive').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                var country_id = \$(this).data('countryId'),
                    row = '.archive-row.' + country_id + ' ';

                \$(row + '.loader').show();
                \$(row + '.archive').addClass('active');

                // \$.ajax({
                //     url : \"";
        // line 147
        echo twig_escape_filter($this->env, site_url("ami/media_archive/archive"), "html", null, true);
        echo "\",
                //     data : { country_id: country_id, app_type: app_type},
                //     dataType: \"json\",
                //     type: \"POST\",
                //     success : function(data) {
                //         \$(row + '.archive').removeClass('active');

                //         if (data.isSuccess) {
                //             setTimeout(function(){
                //                 // window.location = window.location.href; // refresh
                //             }, 2000);
                //         } else {
                //             \$('.loader').hide();
                //             \$(row + '.status.error').html(data.message).fadeIn();

                //             setTimeout(function(){
                //                 \$(row + '.status.error').hide();
                //             }, 2000);
                //         }
                //     }
                // });

                \$.ajax({
                    method: 'POST',
                    url: \"";
        // line 171
        echo twig_escape_filter($this->env, site_url("ami/media_archive/archive"), "html", null, true);
        echo "\",
                    data : { country_id: country_id, app_type: app_type},
                    dataType: 'json',
                    success : function(data) {
                        \$(row + '.archive').removeClass('active');

                        if (data.isSuccess) {
                            setTimeout(function(){
                                // window.location = window.location.href; // refresh
                            }, 2000);
                        } else {
                            \$('.loader').hide();
                            \$(row + '.status.error').html(data.message).fadeIn();

                            setTimeout(function(){
                                \$(row + '.status.error').hide();
                            }, 2000);
                        }
                    },
                    error: function() { },
                    progress: function(e) {
                        //make sure we can compute the length
                        if(e.lengthComputable) {
                            //calculate the percentage loaded
                            var pct = (e.loaded / e.total) * 100;

                            //log percentage loaded
                            console.log(pct);
                        }
                        //this usually happens when Content-Length isn't set
                        else {
                            console.warn('Content Length not reported!');
                        }
                    }
                });
            });

            \$('.download').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                var country_id = \$(this).data('countryId'),
                    row = '.archive-row.' + country_id + ' ';

                \$(row + '.loader').show();

                \$.ajax({
                    url : \"";
        // line 218
        echo twig_escape_filter($this->env, site_url("ami/media_archive/isArchiveExist"), "html", null, true);
        echo "\",
                    data : { country_id: country_id, app_type: app_type},
                    dataType: \"json\",
                    type: \"POST\",
                    success : function(data) {
                        if (data.isSuccess) {
                            setTimeout(function(){
                                \$('.loader').hide();

                                var url = \"";
        // line 227
        echo twig_escape_filter($this->env, site_url("ami/media_archive/download"), "html", null, true);
        echo "\",
                                    path = url + '?country_id=' + country_id + '&app_type=' + app_type;
                                window.location = path;
                            }, 2000);
                        } else {
                            \$('.loader').hide();
                            \$(row + '.status.error').html(data.message).fadeIn();

                            setTimeout(function(){
                                \$(row + '.status.error').hide();
                            }, 2000);
                        }
                    }
                });
            });
        })();
        console.clear();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "ami/media_archive/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  367 => 227,  355 => 218,  305 => 171,  278 => 147,  237 => 109,  227 => 101,  217 => 97,  209 => 92,  205 => 91,  195 => 86,  191 => 85,  186 => 83,  178 => 82,  174 => 81,  170 => 80,  164 => 79,  130 => 50,  124 => 49,  118 => 48,  110 => 45,  102 => 40,  99 => 39,  96 => 38,  83 => 28,  79 => 27,  76 => 26,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
