<?php

/* ami/news_feed/edit.html.twig */
class __TwigTemplate_7e61bd55df096bf22f1ec2b116220baf5f42fa3b6b77190440f9f07284556747 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/news_feed/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script>

        \$(function()
        {
            createUploader(\$('#imageUpload'));
            createUploader(\$('#videoUpload'));
        });

    </script>
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    <script>
        CKEDITOR.replace('inputContent', {
            customConfig: '";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.on('instanceReady', function(){
            \$.each( CKEDITOR.instances, function(instance) {
                CKEDITOR.instances[instance].on(\"change\", function(e) {
                    for ( instance in CKEDITOR.instances ) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                });
            });
        });

    </script>
";
    }

    // line 36
    public function block_content($context, array $blocks = array())
    {
        // line 37
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 52
        echo form_open(site_url("ami/news_feed/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "id", array()), "filter" => (isset($context["filter"]) ? $context["filter"] : null)));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">News Feed</h1>
                <div class=\"text-right\">
                    ";
        // line 59
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/news_feed/edit.html.twig", 59)->display(array_merge($context, array("url" => "ami/news_feed", "id" => $this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "id", array()), "permission" => "news_feed")));
        // line 60
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 69
        echo form_label("Armstrong 2 Salespersons Id", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 71
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 76
        echo form_label("Content", "content", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 78
        echo form_textarea(array("name" => "content", "value" => $this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "content", array()), "class" => "form-control input-xxlarge", "id" => "inputContent", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 83
        echo form_label("Type", "Type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 85
        echo form_radio("newsfeed_type", 0, ($this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "newsfeed_type", array()) == 0));
        echo " Local</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 86
        echo form_radio("newsfeed_type", 1, ($this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "newsfeed_type", array()) == 1));
        echo " Glocal</label>&nbsp;
                </div>
            </div>
            ";
        // line 89
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
            // line 90
            echo "                <div class=\"form-group\">
                    ";
            // line 91
            echo form_label("", "", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <label class=\"radio-inline\">";
            // line 93
            echo form_radio("status", 0, ($this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "status", array()) == 0));
            echo " Pending</label>&nbsp;
                        <label class=\"radio-inline\">";
            // line 94
            echo form_radio("status", 1, ($this->getAttribute((isset($context["news_feed"]) ? $context["news_feed"] : null), "status", array()) == 1));
            echo "
                            Approved</label>&nbsp;
                    </div>
                </div>
            ";
        }
        // line 99
        echo "
            ";
        // line 100
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/news_feed/edit.html.twig", 100)->display(array_merge($context, array("entry_type" => "news_feed")));
        // line 101
        echo "
            ";
        // line 102
        $this->loadTemplate("ami/components/uploader_video.html.twig", "ami/news_feed/edit.html.twig", 102)->display(array_merge($context, array("entry_type" => "news_feed")));
        // line 103
        echo "
            ";
        // line 104
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 108
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/news_feed/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 108,  202 => 104,  199 => 103,  197 => 102,  194 => 101,  192 => 100,  189 => 99,  181 => 94,  177 => 93,  172 => 91,  169 => 90,  167 => 89,  161 => 86,  157 => 85,  152 => 83,  144 => 78,  139 => 76,  131 => 71,  126 => 69,  115 => 60,  113 => 59,  103 => 52,  86 => 37,  83 => 36,  64 => 20,  58 => 17,  45 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
