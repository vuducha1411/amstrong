<?php

/* ami/customers/edit.html.twig */
class __TwigTemplate_709d07ab367cfc766f29ef3c6161b4da34088edf6c66977ab91262073ce47586 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/customers/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/customer-chain.js"), "html", null, true);
        echo "\"></script>
    <script>
        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        //mother
        var armstrong_2_related_mother_id = '";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_related_mother_id", array()), "html", null, true);
        echo "';
        \$('select[name=\"armstrong_2_related_mother_id\"]').change(function(){
            armstrong_2_related_mother_id = \$('select[name=\"armstrong_2_related_mother_id\"]').val();
        });
        function getRelatedMother()
        {
            var armstrong_2_salespersons = '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_salespersons_id\"]').val();
            armstrong_2_salespersons += '&armstrong_2_salespersons_id[]='+\$('select[name=\"armstrong_2_secondary_salespersons_id\"]').val();
            armstrong_2_salespersons += '&is_mother=1';
            armstrong_2_salespersons += '&type_cus=customers';
            armstrong_2_salespersons += '&action=related_mother';
            armstrong_2_salespersons += '&id_current='+\$('input[name=\"armstrong_2_customers_id\"]').val();
            \$.ajax({
                url: \"";
        // line 28
        echo twig_escape_filter($this->env, site_url("ami/customers/getCustomerBySalespersons"), "html", null, true);
        echo "\",
                data: armstrong_2_salespersons,
                type: 'POST',
                dataType: 'html',
                success: function (html) {
                    \$('select[name=\"armstrong_2_related_mother_id\"]').html(html);
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[selected=\"selected\"]').removeAttr('selected');
                    \$('select[name=\"armstrong_2_related_mother_id\"] option[value=\"'+armstrong_2_related_mother_id+'\"]').attr('selected', 'selected');
                }
            });
        }
        \$('select[name=\"armstrong_2_salespersons_id\"], select[name=\"armstrong_2_secondary_salespersons_id').change(function(){
            getRelatedMother();
        });
        getRelatedMother();
        \$('input[name=\"is_mother\"]').change(function(){
            console.log(\$('input[name=\"is_mother\"]').val());
           if(\$('input[name=\"is_mother\"]:checked').val() == 1)
           {
               \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').hide();
           }
           else
           {
               \$('select[name=\"armstrong_2_related_mother_id\"]').closest('.form-group').show();
           }
        });
        \$('input[name=\"is_mother\"]').trigger('change');
        //end mother
        \$(function () {

            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 66
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + res['value'][key] + '\">' + res['value'][key] + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            ";
        // line 93
        if ( !$this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
            // line 94
            echo "            \$('.select-change').trigger('change');
            ";
        }
        // line 96
        echo "
            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        weightage = \$this.find('option:selected').data('weightage') || 0,
                        convenience_factor = \$this.find('option:selected').data('convenience_lvl') || 0,
                        ufs_share = \$this.find('option:selected').data('ufs_food_share') || 0;
                \$('#ChannelWeightage, #FoodCostNew').val(weightage);
                ";
        // line 103
        $context["au_nz"] = array(0 => 2, 1 => 9);
        // line 104
        echo "                ";
        $context["id_sa"] = array(0 => 10, 1 => 4);
        // line 105
        echo "                ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
            // line 106
            echo "                \$('#convenienceFactor').val(convenience_factor);
                ";
        }
        // line 108
        echo "                ";
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 109
            echo "                \$('.id-ufs-share').val(ufs_share);
                ";
        }
        // line 111
        echo "                \$('input[name=\"ufs_share\"]').val(ufs_share);
            });

            \$('.fetch-otm').on('change', function () {
                var foodOverElement = \$('input[name=\"food_turnover_new\"]'),
                        foodPurchaseElement = \$('input[name=\"food_purchase_value_new\"]'),
                        otmFoodCostElement = \$('input[name=\"ufs_share_food_cost_new\"]'),
                        otmPotentialElement = \$('input[name=\"otm_potential_value_new\"]'),
                        otmNewElement = \$('input[name=\"otm_new\"]'),
                        aFoodCost = \$('#FoodCostNew').val(),
                        aUfsShare = \$('input[name=\"assumption_ufs_share_new\"]').val(),
                        convenience = \$('#convenienceFactor').val();
                if (\$('.convenience_other_country').length > 0) {
                    convenience = \$('.convenience_other_country').val() == 0 ? 0.5 : 1;
                }
                var foodOver,
                        foodPurchase,
                        otmFoodCost,
                        otmNew
                \$.each(\$('.fetch-food'), function () {
                    var \$val = \$(this).val();

                    if (!foodOver) {
                        foodOver = \$val;
                    } else {
                        foodOver = foodOver * \$val;
                    }
                });

//                foodOver = Math.round(foodOver).toFixed(3);
                foodPurchase = foodOver * aFoodCost;
                otmFoodCost = foodPurchase * aUfsShare;
                otmNew = otmFoodCost * convenience;

                foodOverElement.val(foodOver);
                foodPurchaseElement.val(foodPurchase);
                otmFoodCostElement.val(otmFoodCost);
                otmPotentialElement.val(otmNew);

                \$.ajax({
                    url: \"";
        // line 151
        echo twig_escape_filter($this->env, site_url("ami/customers/getOtmBandwidth"), "html", null, true);
        echo "\",
                    data: {otm: otmNew},
                    type: 'POST',
                    success: function (res) {
                        otmNewElement.val(res);
                    }
                });

            });

//            \$('.fetch-otm').trigger('change');

            \$('#SellingInput').on('change input blur', function (e) {
                \$('#BudgetInput').val('');
            });

            \$('#BudgetInput').on('change input blur', function (e) {
                \$('#SellingInput').val('');
            });

            \$('#InputBestTimeCall, .timepicker').datetimepicker({
                format: 'h:mm A'
            });

            ";
        // line 175
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 176
            echo "            \$('#subChannelSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        country_channels_id = optionSelected.data('country_channel'),
                        global_channels_id = \$('#ChannelSelect').find('option[value=' + country_channels_id + ']').data('global_channels_id');
                \$('#ChannelSelect, #channelHidden').val(country_channels_id);
                \$('#globalChannels, #globalChannelHidden').val(global_channels_id);
                \$('#ChannelSelect').trigger('change');
            });
            ";
        } else {
            // line 186
            echo "            \$('#globalChannels').on('change', function () {
                var \$this = \$(this),
                        global_channel_id = \$this.val(),
                        channel_option = \$('#ChannelSelect option');

                \$.each(channel_option, function () {
                    var data_global_channel_id = \$(this).data('global_channels_id');
                    if (data_global_channel_id == global_channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#ChannelSelect option:first-child').show().prop('selected', true);
                \$('#subChannelSelect option').hide();
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });

            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        channel_id = \$this.val(),
                        sub_channel_option = \$('#subChannelSelect option');
                \$.each(sub_channel_option, function () {
                    var country_channel_id = \$(this).data('country_channel');
                    if (country_channel_id == channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });
            ";
        }
        // line 219
        echo "        });

        \$(document).ready(function () {
            var approved = '";
        // line 222
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()), "html", null, true);
        echo "',
                    active = '";
        // line 223
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()), "html", null, true);
        echo "',
                    optionCloseDown = \$('#approved option[value=\"-2\"]'),
                    optionReject = \$('#approved option[value=\"-1\"]'),
                    optionApproved = \$('#approved option[value=\"1\"]');

            if (active == 1) {
                if (approved == -2 || approved == -1) {
                    \$('#approved').val('');
                }
                optionApproved.show();
                optionCloseDown.hide();
                optionReject.hide();
            } else {
                if (approved == 1) {
                    \$('#approved').val('');
                }
                optionApproved.hide();
                optionCloseDown.show();
                optionReject.show();
            }

            \$('input[name=\"active\"]').on('change', function () {
                var \$val = \$(this).val();
                \$('#approved').val('');
                if (\$val == 1) {
                    optionApproved.show();
                    optionCloseDown.hide();
                    optionReject.hide();
                } else {
                    optionApproved.hide();
                    optionCloseDown.show();
                    optionReject.show();
                }
            });

            // init sub channel
            ";
        // line 259
        if (!twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 260
            echo "            \$.each(\$('#subChannelSelect option'), function () {
                var country_channel_id = \$(this).data('country_channel');
                if (country_channel_id == \$('#ChannelSelect').val()) {
                    \$(this).show();
                } else {
                    \$(this).hide();
                }
            });
            ";
        }
        // line 269
        echo "
            ";
        // line 270
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 271
            echo "            \$('input[name=\"assumption_ufs_share_new\"]').val(\$('#ChannelSelect').find('option:selected').data('ufs_food_share'));
            ";
        }
        // line 273
        echo "        });

        \$(function () {
            \$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD hh:mm:ss'
            });
        });
    </script>
";
    }

    // line 283
    public function block_css($context, array $blocks = array())
    {
        // line 284
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\"
          href=\"";
        // line 286
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">

";
    }

    // line 290
    public function block_content($context, array $blocks = array())
    {
        // line 291
        echo "
    ";
        // line 292
        echo form_open(site_url("ami/customers/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_customers_id" => $this->getAttribute(        // line 293
(isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array()), "filter" =>         // line 294
(isset($context["filter"]) ? $context["filter"] : null)));
        // line 295
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Customers</h1>

                <div class=\"text-right\">
                    ";
        // line 303
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/customers/edit.html.twig", 303)->display(array_merge($context, array("url" => "ami/customers", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array()), "permission" => "customer")));
        // line 304
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 313
        echo form_label("Name", "armstrong_2_customers_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 315
        echo form_input(array("name" => "armstrong_2_customers_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_name", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-maxlength" => 100));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 319
        echo form_label("Armstrong 1 Customer Id", "armstrong_1_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 321
        echo form_input(array("name" => "armstrong_1_customers_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_1_customers_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 326
        echo form_label("Armstrong 2 Customer Id", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 328
        echo form_input(array("name" => "armstrong_2_customers_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array()), "class" => "form-control", "disabled" => "disabled"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 333
        echo form_label("Armstrong 2 Salespersons Id", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 335
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 340
        echo form_label("Armstrong 2 Secondary Salespersons Id", "armstrong_2_secondary_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 342
        echo form_dropdown("armstrong_2_secondary_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_secondary_salespersons_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 347
        echo form_label("Is Mother", "is_mother", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 349
        echo form_radio("is_mother", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "is_mother", array()) == 1));
        echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 350
        echo form_radio("is_mother", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "is_mother", array()) == 0));
        echo " No</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 354
        echo form_label("Related Mother", "armstrong_2_related_mother_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 356
        echo form_dropdown("armstrong_2_related_mother_id", (isset($context["related_mother"]) ? $context["related_mother"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_related_mother_id", array()), "class=\"form-control\" data-type=\"customers\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 360
        echo form_label("Armstrong 2 Chains Id", "armstrong_2_chains_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 362
        echo form_dropdown("armstrong_2_chains_id", (isset($context["chains"]) ? $context["chains"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_chains_id", array()), "class=\"form-control\" data-type=\"customers\"");
        echo "
                </div>
            </div>



            <div class=\"form-group\">
                ";
        // line 369
        echo form_label("Current Approval Status", "current_status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 371
        echo form_input(array("name" => "current_status", "value" => ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -2)) ? ("Close down") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -1)) ? ("Rejected") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 0)) ? ("Pending") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 1)) ? ("Approved") : (((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 2)) ? ("Edit") : ("")))))))))), "class" => "form-control", "readonly" => "readonly"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 376
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 378
        echo form_radio("active", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 379
        echo form_radio("active", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                </div>
            </div>

            ";
        // line 383
        if (((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw")) && ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) || ($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin"))) {
            // line 384
            echo "                <div class=\"form-group\">
                    ";
            // line 385
            echo form_label("Approved", "approved", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <select name=\"approved\" id=\"approved\" class=\"form-control\" data-parsley-required=\"true\">
                            <option value=\"\">--Select--</option>
                            <option value=\"-2\" ";
            // line 389
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -2)) ? ("selected") : (""));
            echo ">Close down</option>
                            <option value=\"-1\" ";
            // line 390
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) ==  -1)) ? ("selected") : (""));
            echo ">Rejected</option>
                            ";
            // line 392
            echo "                            <option value=\"1\" ";
            echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()) == 1)) ? ("selected") : (""));
            echo ">Approved</option>
                            ";
            // line 394
            echo "                        </select>
                    </div>
                </div>
            ";
        }
        // line 398
        echo "
            ";
        // line 399
        if ((twig_length_filter($this->env, (isset($context["groups"]) ? $context["groups"] : null)) > 1)) {
            // line 400
            echo "                <div class=\"form-group\">
                    ";
            // line 401
            echo form_label("Group", "groups_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 403
            echo form_dropdown("groups_id", (isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "groups_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 407
        echo "
            <div class=\"form-group\">
                ";
        // line 409
        echo form_label("SSD Name", "sap_cust_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 411
        echo form_input(array("name" => "sap_cust_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 416
        echo form_label("SSD Name 2", "sap_cust_name_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 418
        echo form_input(array("name" => "sap_cust_name_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 423
        echo form_label("SSD ID", "ssd_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 425
        echo form_input(array("name" => "ssd_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 430
        echo form_label("SSD ID 2", "ssd_id_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 432
        echo form_input(array("name" => "ssd_id_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 437
        echo form_label("Sap ID", "sap_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 439
        echo form_input(array("name" => "sap_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 444
        echo form_label("Street Address", "street_address", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 446
        echo form_input(array("name" => "street_address", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "street_address", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 477
        echo "
            ";
        // line 478
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countryHierachy"]) ? $context["countryHierachy"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
            // line 479
            echo "                <div class=\"form-group select-change-container\">
                    ";
            // line 480
            echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 482
            if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
                // line 483
                echo "                            <select name=\"";
                echo twig_escape_filter($this->env, $context["countryField"], "html", null, true);
                echo "\" class=\"form-control select-change\">
                                ";
                // line 484
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["countryValues"]) ? $context["countryValues"] : null), $context["countryField"], array(), "array"));
                foreach ($context['_seq'] as $context["k"] => $context["value"]) {
                    // line 485
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "\" ";
                    if ((twig_lower_filter($this->env, $context["value"]) == twig_lower_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array")))) {
                        echo " selected ";
                    }
                    echo " >";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 487
                echo "                            </select>

                            ";
                // line 490
                echo "                        ";
            } else {
                // line 491
                echo "                            ";
                $context["_values"] = ((($context["key"] == "root")) ? ((isset($context["rootValues"]) ? $context["rootValues"] : null)) : (array()));
                // line 492
                echo "                            ";
                echo form_dropdown($context["countryField"], (isset($context["_values"]) ? $context["_values"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            }
            // line 494
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 497
        echo "
            <div class=\"form-group\">
                ";
        // line 499
        echo form_label("Postal Code", "postal_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 501
        echo form_input(array("name" => "postal_code", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "postal_code", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 506
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 508
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 513
        echo form_label("Phone 2", "phone_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 515
        echo form_input(array("name" => "phone_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone_two", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 520
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 522
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 527
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 529
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 534
        echo form_label("Email 2", "email_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 536
        echo form_input(array("name" => "email_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email_two", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            ";
        // line 540
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
            // line 541
            echo "                <div class=\"form-group\">
                    ";
            // line 542
            echo form_label("Primary Contact", "primary_contact", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 544
            echo form_dropdown("primary_contact", (isset($context["contactListOptions"]) ? $context["contactListOptions"] : null), (isset($context["primaryContact"]) ? $context["primaryContact"] : null), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 548
        echo "            <div class=\"form-group\">
                ";
        // line 549
        echo form_label("Acct Opened", "acct_opened", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 551
        echo form_input(array("name" => "acct_opened", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "acct_opened", array()), "class" => "form-control datepicker"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 555
        echo form_label("Web Url", "web_url", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 557
        echo form_input(array("name" => "web_url", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "web_url", array()), "class" => "form-control", "data-parsley-type" => "url"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 562
        echo form_label("Global Channel", "global_channels", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 564
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 565
            echo "                        ";
            echo form_dropdown("hidden_global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\" disabled=\"disabled\"");
            echo "
                    ";
        } else {
            // line 567
            echo "                        ";
            echo form_dropdown("global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    ";
        }
        // line 569
        echo "                </div>
            </div>
            ";
        // line 571
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 572
            echo "                <input id=\"globalChannelHidden\" type=\"hidden\" name=\"global_channels_id\"
                       value=\"";
            // line 573
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "html", null, true);
            echo "\">
            ";
        }
        // line 575
        echo "
            ";
        // line 576
        if ((isset($context["country_channels"]) ? $context["country_channels"] : null)) {
            // line 577
            echo "                <div class=\"form-group\">
                    ";
            // line 578
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 581
            echo "                        <select name=\"";
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disable_channel") : ("channel"));
            echo "\"
                                class=\"form-control fetch-otm ";
            // line 582
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 2, 1 => 9))) ? ("channellv") : (""));
            echo "\"
                                id=\"ChannelSelect\"
                                data-parsley-required=\"true\" ";
            // line 584
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disabled=\"disabled\"") : (""));
            echo ">
                            <option value=\"\" data-weightage=\"\">- Select -</option>
                            ";
            // line 586
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["country_channels"]) ? $context["country_channels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 587
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute($context["country_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()))) ? ("selected=\"selected\"") : (""));
                echo "
                                        data-global_channels_id=\"";
                // line 588
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "global_channels_id", array()), "html", null, true);
                echo "\"
                                        data-weightage=\"";
                // line 589
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "channel_weightage", array()), "html", null, true);
                echo "\"
                                        data-convenience_lvl=\"";
                // line 590
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "convenience_lvl", array()), "html", null, true);
                echo "\"
                                        data-ufs_food_share=\"";
                // line 591
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "ufs_food_share", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 593
            echo "                        </select>
                    </div>
                </div>
                ";
            // line 596
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 597
                echo "                    <input id=\"channelHidden\" type=\"hidden\" name=\"channel\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()), "html", null, true);
                echo "\">
                ";
            }
            // line 599
            echo "            ";
        }
        // line 600
        echo "
            ";
        // line 601
        if ((twig_length_filter($this->env, (isset($context["sub_channels"]) ? $context["sub_channels"] : null)) > 1)) {
            // line 602
            echo "                <div class=\"form-group\">
                    ";
            // line 603
            echo form_label("Sub Channel", "sub_channel", array("class" => "control-label col-sm-3"));
            echo "
                    ";
            // line 604
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 605
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\" data-parsley-required=\"true\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 610
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 611
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 612
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 614
                echo "                            </select>
                        </div>
                    ";
            } else {
                // line 617
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 622
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 623
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 624
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 626
                echo "                            </select>
                        </div>
                    ";
            }
            // line 629
            echo "                </div>
            ";
        }
        // line 631
        echo "
            <div class=\"form-group\">
                ";
        // line 633
        echo form_label("Channel Weightage", "channel_weightage", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 635
        echo form_input(array("name" => "channel_weightage", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_weightage", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "ChannelWeightage"));
        echo "
                </div>
            </div>

            ";
        // line 639
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 10))) {
            // line 640
            echo "                <div class=\"form-group\">
                    ";
            // line 641
            echo form_label("Real Otm", "real_otm", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 643
            echo form_input(array("name" => "real_otm", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "real_otm", array()), "class" => "form-control", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 647
        echo "            ";
        // line 659
        echo "
        </div>

        <div class=\"form-group\">
            ";
        // line 663
        echo form_label("Key Manager", "key_acct", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 665
        echo form_input(array("name" => "key_acct", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "key_acct", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        ";
        // line 669
        if ((twig_length_filter($this->env, (isset($context["business_types"]) ? $context["business_types"] : null)) > 1)) {
            // line 670
            echo "            <div class=\"form-group\">
                ";
            // line 671
            echo form_label("Business Type", "business_type", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 673
            echo form_dropdown("business_type", (isset($context["business_types"]) ? $context["business_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "business_type", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 677
        echo "
        ";
        // line 684
        echo "
        ";
        // line 686
        echo "        ";
        if ((twig_length_filter($this->env, (isset($context["suppliers"]) ? $context["suppliers"] : null)) > 1)) {
            // line 687
            echo "            <div class=\"form-group\">
                ";
            // line 688
            echo form_label("Primary Supplier", "primary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 690
            echo form_dropdown("primary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "primary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
            // line 695
            echo form_label("Secondary Supplier", "secondary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 697
            echo form_dropdown("secondary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "secondary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 701
        echo "
        ";
        // line 737
        echo "        ";
        // line 738
        echo "        ";
        // line 739
        echo "        ";
        // line 740
        echo "        ";
        // line 741
        echo "        ";
        // line 742
        echo "        ";
        // line 743
        echo "        ";
        // line 744
        echo "        ";
        // line 745
        echo "        ";
        // line 746
        echo "        ";
        // line 747
        echo "        ";
        // line 748
        echo "        ";
        // line 783
        echo "        <div class=\"form-group\">
            ";
        // line 784
        echo form_label("Best Time To Call", "best_time_to_call", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 786
        echo form_input(array("name" => "best_time_to_call", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "best_time_to_call", array()), "class" => "form-control", "id" => "InputBestTimeCall"));
        // line 789
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 794
        echo form_label("Notes", "notes", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 796
        echo form_input(array("name" => "notes", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "notes", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 801
        echo form_label("Latitude", "latitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 803
        echo form_input(array("name" => "latitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "latitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 808
        echo form_label("Longitude", "longitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 810
        echo form_input(array("name" => "longitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "longitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 815
        echo form_label("Display Name", "display_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 817
        echo form_input(array("name" => "display_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "display_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 822
        echo form_label("Registered Name", "registered_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 824
        echo form_input(array("name" => "registered_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "registered_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 829
        echo form_label("Opening Hours", "opening_hours", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-3\">
                ";
        // line 831
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_from", array()), "class" => "form-control timepicker"));
        // line 833
        echo "
            </div>
            <div class=\"col-sm-3\">
                ";
        // line 836
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_to", array()), "class" => "form-control timepicker"));
        // line 838
        echo "
            </div>
        </div>

        ";
        // line 857
        echo "
        <div class=\"form-group\">
            ";
        // line 859
        echo form_label("Ufs Share", "ufs_share", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 861
        echo form_input(array("name" => "ufs_share", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly"));
        echo "
            </div>
        </div>

        ";
        // line 865
        if ((twig_length_filter($this->env, (isset($context["customers_types"]) ? $context["customers_types"] : null)) > 1)) {
            // line 866
            echo "            <div class=\"form-group\">
                ";
            // line 867
            echo form_label("Customers Types", "customers_types_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 869
            echo form_dropdown("customers_types_id", (isset($context["customers_types"]) ? $context["customers_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "customers_types_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 873
        echo "        <div class=\"form-group\">
            ";
        // line 874
        echo form_label("Buy Frequency", "buy_frequency", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                <select name=\"buy_frequency\" class=\"form-control\" id=\"ChannelSelect\">
                    <option value=\"0\" data-weightage=\"\">- Select -</option>
                    <option value=\"1\" ";
        // line 878
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 1)) ? ("selected=\"selected\"") : (""));
        echo " >Once a month
                    </option>
                    <option value=\"2\" ";
        // line 880
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 2)) ? ("selected=\"selected\"") : (""));
        echo " >Twice a month
                    </option>
                    <option value=\"3\" ";
        // line 882
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 3)) ? ("selected=\"selected\"") : (""));
        echo " >Three times a
                        month
                    </option>
                    <option value=\"4\" ";
        // line 885
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 4)) ? ("selected=\"selected\"") : (""));
        echo " >Once a week
                    </option>
                </select>
            </div>
        </div>

        ";
        // line 891
        if ((twig_length_filter($this->env, (isset($context["channel_groups"]) ? $context["channel_groups"] : null)) > 1)) {
            // line 892
            echo "            <div class=\"form-group\">
                ";
            // line 893
            echo form_label("Channel Groups", "channel_groups_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 895
            echo form_dropdown("channel_groups_id", (isset($context["channel_groups"]) ? $context["channel_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 899
        echo "
        ";
        // line 900
        if ((twig_length_filter($this->env, (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null)) > 1)) {
            // line 901
            echo "            <div class=\"form-group\">
                ";
            // line 902
            if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) == 6)) {
                // line 903
                echo "                    ";
                echo form_label("OG", "og", array("class" => "control-label col-sm-3"));
                echo "
                ";
            } else {
                // line 905
                echo "                    ";
                echo form_label("Cuisine Groups", "cuisine_groups_id", array("class" => "control-label col-sm-3"));
                echo "
                ";
            }
            // line 907
            echo "                <div class=\"col-sm-6\">
                    ";
            // line 908
            echo form_dropdown("cuisine_groups_id", (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "cuisine_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 912
        echo "        ";
        // line 913
        echo "        ";
        // line 914
        echo "            <div class=\"form-group\">
                ";
        // line 915
        echo form_label("Meals / Days", "meals_per_day_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 917
        echo form_input(array("name" => "meals_per_day_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "meals_per_day_new", array()), "class" => "form-control fetch-food fetch-otm", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 921
        echo form_label("Avarage Price / Meal", "selling_price_per_meal_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 923
        echo form_input(array("name" => "selling_price_per_meal_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "selling_price_per_meal_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 927
        echo form_label("Weeks / Year", "weeks_per_year_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 929
        echo form_input(array("name" => "weeks_per_year_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "weeks_per_year_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,52]"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 933
        echo form_label("Days / Week", "days_per_week_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 935
        echo form_input(array("name" => "days_per_week_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "days_per_week_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,7]"));
        echo "
                </div>
            </div>
            ";
        // line 938
        echo form_input(array("name" => "food_turnover_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_turnover_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
        echo "
            <div class=\"form-group\">
                ";
        // line 940
        echo form_label("Assumption: Food Cost %", "assumption_food_cost_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 942
        echo form_input(array("name" => "assumption_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "assumption_food_cost_new", array()), "class" => "form-control fetch-otm fetch-food-purchase", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "FoodCostNew"));
        echo "
                </div>
            </div>
            ";
        // line 945
        echo form_input(array("name" => "food_purchase_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_purchase_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
        echo "
            <div class=\"form-group\">
                ";
        // line 947
        echo form_label("Assumption: potential UFS Share of Food Cost %", "assumption_ufs_share_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 949
        echo form_input(array("name" => "assumption_ufs_share_new", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control id-ufs-share", "data-parsley-type" => "number", "readonly" => "readonly"));
        echo "
                </div>
            </div>
            ";
        // line 952
        echo form_input(array("name" => "ufs_share_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ufs_share_food_cost_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
        echo "
            ";
        // line 953
        echo form_input(array("name" => "otm_potential_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_potential_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
        echo "
            <div class=\"form-group\">
                ";
        // line 955
        echo form_label("Convenience Factor", "convenience_factor_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 957
        $context["au_nz"] = array(0 => 2, 1 => 9);
        // line 958
        echo "                    ";
        $context["mer"] = array(0 => 12, 1 => 13, 2 => 14, 3 => 15, 4 => 16, 5 => 17);
        // line 959
        echo "                    ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
            // line 960
            echo "                        ";
            echo form_input(array("name" => "convenience_factor_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class" => "form-control fetch-otm convenience_sp_country", "id" => "convenienceFactor", "data-parsley-type" => "number", "readonly" => "readonly"));
            echo "
                    ";
        } elseif (twig_in_filter($this->getAttribute(        // line 961
(isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["mer"]) ? $context["mer"] : null))) {
            // line 962
            echo "                        ";
            echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1", "1.5" => "1.5"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm\" id=\"convenienceFactor\"");
            echo "
                    ";
        } else {
            // line 964
            echo "                        ";
            echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm convenience_other_country\" id=\"convenienceFactor\"");
            echo "
                    ";
        }
        // line 966
        echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 969
        echo form_label("OTM New", "otm_new", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 971
        echo form_input(array("name" => "otm_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_new", array()), "class" => "form-control", "readonly" => "readonly", "id" => "OTMNew"));
        echo "
                </div>
            </div>
        ";
        // line 975
        echo "
        ";
        // line 976
        if (twig_in_filter((isset($context["country_id"]) ? $context["country_id"] : null), array(0 => 6, 1 => 88))) {
            // line 977
            echo "            <div class=\"form-group\">
                ";
            // line 978
            echo form_label("No Of Outlets", "no_of_outlets", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 980
            echo form_input(array("name" => "no_of_outlets", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "no_of_outlets", array()), "class" => "form-control", "data-parsley-type" => "integer"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 984
            echo form_label("Central Kitchen Operation", "central_kitchen_operation", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
            // line 986
            echo form_radio("central_kitchen_operation", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "central_kitchen_operation", array()) == 1));
            echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
            // line 987
            echo form_radio("central_kitchen_operation", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "central_kitchen_operation", array()) == 0));
            echo " No</label>&nbsp;
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 991
            echo form_label("Chain", "chain", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
            // line 993
            echo form_radio("chain", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "chain", array()) == 1));
            echo " Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
            // line 994
            echo form_radio("chain", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "chain", array()) == 0));
            echo " No</label>&nbsp;
                </div>
            </div>
        ";
        }
        // line 998
        echo "
        ";
        // line 1007
        echo "

        ";
        // line 1022
        echo "    </div>
    </div>

    ";
        // line 1025
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/customers/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1640 => 1025,  1635 => 1022,  1631 => 1007,  1628 => 998,  1621 => 994,  1617 => 993,  1612 => 991,  1605 => 987,  1601 => 986,  1596 => 984,  1589 => 980,  1584 => 978,  1581 => 977,  1579 => 976,  1576 => 975,  1570 => 971,  1565 => 969,  1560 => 966,  1554 => 964,  1548 => 962,  1546 => 961,  1541 => 960,  1538 => 959,  1535 => 958,  1533 => 957,  1528 => 955,  1523 => 953,  1519 => 952,  1513 => 949,  1508 => 947,  1503 => 945,  1497 => 942,  1492 => 940,  1487 => 938,  1481 => 935,  1476 => 933,  1469 => 929,  1464 => 927,  1457 => 923,  1452 => 921,  1445 => 917,  1440 => 915,  1437 => 914,  1435 => 913,  1433 => 912,  1426 => 908,  1423 => 907,  1417 => 905,  1411 => 903,  1409 => 902,  1406 => 901,  1404 => 900,  1401 => 899,  1394 => 895,  1389 => 893,  1386 => 892,  1384 => 891,  1375 => 885,  1369 => 882,  1364 => 880,  1359 => 878,  1352 => 874,  1349 => 873,  1342 => 869,  1337 => 867,  1334 => 866,  1332 => 865,  1325 => 861,  1320 => 859,  1316 => 857,  1310 => 838,  1308 => 836,  1303 => 833,  1301 => 831,  1296 => 829,  1288 => 824,  1283 => 822,  1275 => 817,  1270 => 815,  1262 => 810,  1257 => 808,  1249 => 803,  1244 => 801,  1236 => 796,  1231 => 794,  1224 => 789,  1222 => 786,  1217 => 784,  1214 => 783,  1212 => 748,  1210 => 747,  1208 => 746,  1206 => 745,  1204 => 744,  1202 => 743,  1200 => 742,  1198 => 741,  1196 => 740,  1194 => 739,  1192 => 738,  1190 => 737,  1187 => 701,  1180 => 697,  1175 => 695,  1167 => 690,  1162 => 688,  1159 => 687,  1156 => 686,  1153 => 684,  1150 => 677,  1143 => 673,  1138 => 671,  1135 => 670,  1133 => 669,  1126 => 665,  1121 => 663,  1115 => 659,  1113 => 647,  1106 => 643,  1101 => 641,  1098 => 640,  1096 => 639,  1089 => 635,  1084 => 633,  1080 => 631,  1076 => 629,  1071 => 626,  1061 => 624,  1054 => 623,  1050 => 622,  1043 => 617,  1038 => 614,  1028 => 612,  1021 => 611,  1017 => 610,  1010 => 605,  1008 => 604,  1004 => 603,  1001 => 602,  999 => 601,  996 => 600,  993 => 599,  987 => 597,  985 => 596,  980 => 593,  970 => 591,  966 => 590,  962 => 589,  958 => 588,  951 => 587,  947 => 586,  942 => 584,  937 => 582,  932 => 581,  927 => 578,  924 => 577,  922 => 576,  919 => 575,  914 => 573,  911 => 572,  909 => 571,  905 => 569,  899 => 567,  893 => 565,  891 => 564,  886 => 562,  878 => 557,  873 => 555,  866 => 551,  861 => 549,  858 => 548,  851 => 544,  846 => 542,  843 => 541,  841 => 540,  834 => 536,  829 => 534,  821 => 529,  816 => 527,  808 => 522,  803 => 520,  795 => 515,  790 => 513,  782 => 508,  777 => 506,  769 => 501,  764 => 499,  760 => 497,  752 => 494,  746 => 492,  743 => 491,  740 => 490,  736 => 487,  721 => 485,  717 => 484,  712 => 483,  710 => 482,  705 => 480,  702 => 479,  698 => 478,  695 => 477,  688 => 446,  683 => 444,  675 => 439,  670 => 437,  662 => 432,  657 => 430,  649 => 425,  644 => 423,  636 => 418,  631 => 416,  623 => 411,  618 => 409,  614 => 407,  607 => 403,  602 => 401,  599 => 400,  597 => 399,  594 => 398,  588 => 394,  583 => 392,  579 => 390,  575 => 389,  568 => 385,  565 => 384,  563 => 383,  556 => 379,  552 => 378,  547 => 376,  539 => 371,  534 => 369,  524 => 362,  519 => 360,  512 => 356,  507 => 354,  500 => 350,  496 => 349,  491 => 347,  483 => 342,  478 => 340,  470 => 335,  465 => 333,  457 => 328,  452 => 326,  444 => 321,  439 => 319,  432 => 315,  427 => 313,  416 => 304,  414 => 303,  404 => 295,  402 => 294,  401 => 293,  400 => 292,  397 => 291,  394 => 290,  387 => 286,  381 => 284,  378 => 283,  366 => 273,  362 => 271,  360 => 270,  357 => 269,  346 => 260,  344 => 259,  305 => 223,  301 => 222,  296 => 219,  261 => 186,  249 => 176,  247 => 175,  220 => 151,  178 => 111,  174 => 109,  171 => 108,  167 => 106,  164 => 105,  161 => 104,  159 => 103,  150 => 96,  146 => 94,  144 => 93,  114 => 66,  73 => 28,  57 => 15,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
