<?php

/* ami/customers/list_customer.html.twig */
class __TwigTemplate_70b1f26838ae5494885a2260d98392806f0baad308d05cca7ff1fb0d50fffc9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
    ";
        // line 2
        if ((isset($context["customers"]) ? $context["customers"] : null)) {
            // line 3
            echo "        <li class=\"selectall\">
            <div>
                <input class=\"checkAll\" type=\"checkbox\"> Select All
            </div>
        </li>
    ";
        }
        // line 9
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["customers"]) ? $context["customers"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["customer"]) {
            // line 10
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["customer"]);
            foreach ($context['_seq'] as $context["customer_id"] => $context["_customer"]) {
                // line 11
                echo "            <li class=\"fillable\">
                <div data-customer=\"";
                // line 12
                echo twig_escape_filter($this->env, $context["customer_id"], "html", null, true);
                echo "\"
                     data-salesperson=\"";
                // line 13
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\"><input class=\"check-box\"
                                                         type=\"checkbox\"> ";
                // line 14
                echo twig_escape_filter($this->env, $context["_customer"], "html", null, true);
                echo "</div>
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['customer_id'], $context['_customer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['customer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "ami/customers/list_customer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 18,  62 => 17,  53 => 14,  49 => 13,  45 => 12,  42 => 11,  37 => 10,  32 => 9,  24 => 3,  22 => 2,  19 => 1,);
    }
}
