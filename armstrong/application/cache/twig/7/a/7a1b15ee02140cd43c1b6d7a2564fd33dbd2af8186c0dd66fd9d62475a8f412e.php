<?php

/* ami/webshop/index.html.twig */
class __TwigTemplate_7a1b15ee02140cd43c1b6d7a2564fd33dbd2af8186c0dd66fd9d62475a8f412e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/webshop/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/js/webshop.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 17
    public function block_css($context, array $blocks = array())
    {
        // line 18
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/css/loading.css"), "html", null, true);
        echo "\">
";
    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        // line 25
        echo "    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Webshop</h1>
                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar\" id=\"CheckAllBtn\">
                        <button type=\"button\" class=\"btn btn-sm btn-primary\" data-backdrop=\"static\" data-toggle=\"modal\"
                                data-target=\"#myModal\">
                            <i class=\"fa fw fa-upload\"></i>&nbsp;Webshop Data Sync
                        </button>
                    </span>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class=\"row m-t-md\">
    <div class=\"col-lg-12\">
        <ul class=\"nav nav-tabs\" role=\"tablist\">
            <li role=\"presentation\" class=\"active\">
                <a href=\"#tagged\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Tagged Orders</a>
            </li>
            <li role=\"presentation\">
                <a href=\"#raw\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Raw Orders</a>
            </li>
            <li role=\"presentation\">
                <a href=\"#untagged\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Untagged Customers</a>
            </li>
        </ul>
        <div class=\"panel panel-default m-t\">
            <div class=\"panel-body tab-content\">
                <div role=\"tabpanel\" class=\"tab-pane active\" id=\"tagged\">
                    <table class=\"table table-striped table-bordered table-hover\" id=\"wbDataTables\">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Webshop Order ID</th>
                            <th>Salesperson ID</th>
                            <th>Customer Name</th>
                            ";
        // line 65
        if (((isset($context["country_id"]) ? $context["country_id"] : null) == "2")) {
            // line 66
            echo "                            <th>Call Records ID</th>
                            ";
        }
        // line 68
        echo "                            <th>Total Order</th>
                            <th>Date Created</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div role=\"tabpanel\" class=\"tab-pane\" id=\"raw\">
                    <table class=\"table table-striped table-bordered table-hover\" id=\"wbRawDataTables\">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Contact Person</th>
                            <th>Total Order Price</th>
                            <th>Order Date</th>
                            <th>Date Synced</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div role=\"tabpanel\" class=\"tab-pane\" id=\"untagged\">
                    <table class=\"table table-striped table-bordered table-hover\" id=\"wbUntaggedDt\">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Contact Person</th>
                            <th>Total Order Price</th>
                            <th>Order Date</th>
                            <th>Date Synced</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 112
        echo "    ";
        $this->loadTemplate("ami/webshop/modal.html.twig", "ami/webshop/index.html.twig", 112)->display($context);
        // line 113
        echo "    ";
    }

    public function getTemplateName()
    {
        return "ami/webshop/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 113,  188 => 112,  143 => 68,  139 => 66,  137 => 65,  95 => 25,  92 => 24,  86 => 21,  82 => 20,  78 => 19,  73 => 18,  70 => 17,  64 => 13,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
