<?php

/* ami/module_field_countries/preview.html.twig */
class __TwigTemplate_760af62d3239600ea2c2ba626ed5eabf4d5368bd2eac30ea8068d374a3c905c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo form_open(site_url("ami/module_field_countries/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "filter" => (isset($context["filter"]) ? $context["filter"] : null)));
        echo "
<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">Field value: ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["value_field"]) ? $context["value_field"] : null), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"form-group\">
                ";
        // line 10
        echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 12
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 16
        echo form_label("Section", "section", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 18
        echo form_input(array("name" => "section", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "section", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 22
        echo form_label("Row", "row", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 24
        echo form_input(array("name" => "row", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "row", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group select_cat\">
                ";
        // line 28
        echo form_label("Type", "Type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 30
        echo form_dropdown("type", (isset($context["list_type"]) ? $context["list_type"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()), "class=\"form-control form_select_cat\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div class=\"form-group select_app_type\">
                ";
        // line 34
        echo form_label("Required", "required", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 36
        echo form_radio("required", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "required", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 37
        echo form_radio("required", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "required", array()) == 1));
        echo " Yes</label>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            ";
        // line 42
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null))) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", (isset($context["permission"]) ? $context["permission"] : null))))) {
            // line 43
            echo "                ";
            echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
            echo "
            ";
        }
        // line 45
        echo "            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    </div>
</div>
";
        // line 49
        echo form_close();
        echo "

<script>
    \$(document).ready(function () {
        \$('.modal-body').slimScroll({
            height: '400px'
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "ami/module_field_countries/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 49,  107 => 45,  101 => 43,  99 => 42,  91 => 37,  87 => 36,  82 => 34,  75 => 30,  70 => 28,  63 => 24,  58 => 22,  51 => 18,  46 => 16,  39 => 12,  34 => 10,  27 => 6,  19 => 1,);
    }
}
