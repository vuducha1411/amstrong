<?php

/* ami/setting_game/index.html.twig */
class __TwigTemplate_798a2feb506b8cf7c54930999a75345cd95cf1c0d3a5fab3fa990c4eeeebc560 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_game/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

";
    }

    // line 11
    public function block_css($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "
    ";
        // line 18
        echo form_open(site_url("ami/kpi/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 23
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Game Setting Draft") : ("Game Setting"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 27
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 29
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/setting_game/index.html.twig", 29)->display(array_merge($context, array("url" => "ami/kpi", "icon" => "fa-line-chart", "title" => "Game Setting", "permission" => "kpi_settings")));
        // line 30
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">

                <div class=\"panel-body\">
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-primary\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-cogs fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 51
        echo "                                        <div>Game</div>
                                        <div>Play Settings</div>
                                    </div>
                                </div>
                            </div>
                            <a href=\"setting_game/setting_index\">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-green\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-check-square-o fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 75
        echo "                                        <div>Integrate</div>
                                        <div>With Applications</div>
                                    </div>
                                </div>
                            </div>
                            <a href=\"setting_game/integrate_with_applications\">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-red\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-empire fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 99
        echo "                                        <div>Spin</div>
                                        <div>And Win</div>
                                    </div>
                                </div>
                            </div>
                            <a target=\"_blank\" href=";
        // line 104
        echo twig_escape_filter($this->env, site_url("spin_and_win"), "html", null, true);
        echo ">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 124
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 124,  171 => 104,  164 => 99,  139 => 75,  114 => 51,  92 => 30,  90 => 29,  85 => 27,  78 => 23,  70 => 18,  67 => 17,  64 => 16,  58 => 13,  53 => 12,  50 => 11,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
