<?php

/* ami/contacts/edit.html.twig */
class __TwigTemplate_a3f60cd90fa121b4eff49e391b2ed11c7dd7fd7eecbd588c87e72f12d429c6c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/contacts/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery.signaturepad.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        function removeRequireInput(input){
            \$('#InputCustomers').removeAttr('data-parsley-required');
            \$('#InputWholesalers').removeAttr('data-parsley-required');
            \$('#InputDistributors').removeAttr('data-parsley-required');
            input.attr('data-parsley-required', \"true\");
        }
        \$(function () {
            createUploader(\$('#imageUpload'));
            \$('#InputCustomers').autocomplete({
                source: \"";
        // line 20
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    removeRequireInput(\$(this));
                    \$('#InputCustomersId').val(ui.item.value);
                    \$('#InputDistributors').val('');
                    \$('#InputDistributorsId').val('');
                    \$('#InputWholesalers').val('');
                    \$('#InputWholesalersId').val('');
                    check_primary_contact();
                }
            }).on('focus', function () {
                this.select();
            });

            \$('#InputWholesalers').autocomplete({
                source: \"";
        // line 38
        echo twig_escape_filter($this->env, site_url("ami/wholesalers/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    removeRequireInput(\$(this));
                    \$('#InputWholesalersId').val(ui.item.value);

                    \$('#InputCustomers').val('');
                    \$('#InputCustomersId').val('');
                    \$('#InputDistributors').val('');
                    \$('#InputDistributorsId').val('');
                }
            }).on('focus', function () {
                this.select();
            });

            \$('#InputDistributors').autocomplete({
                source: \"";
        // line 56
        echo twig_escape_filter($this->env, site_url("ami/distributors/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).val(ui.item.label);
                    removeRequireInput(\$(this));
                    \$('#InputDistributorsId').val(ui.item.value);
                    \$('#InputCustomers').val('');
                    \$('#InputCustomersId').val('');
                    \$('#InputWholesalers').val('');
                    \$('#InputWholesalersId').val('');
                }
            }).on('focus', function () {
                this.select();
            });

            \$.listen('parsley:form:validate', function (formInstance) {

                if (!\$('#InputWholesalers').val()
                        && !\$('#InputDistributors').val()
                        && !\$('#InputCustomers').val()
                ) {
                    // window.ParsleyUI.addError(\$('.parsley-need-one').parsley(), 'name', 'message');
                    formInstance.submitEvent.preventDefault();
                }
            });
            check_primary_contact();
        });

        function check_primary_contact() {
            var val = \$('#InputCustomers').val();
            if (val) {
                \$.ajax({
                    url: \"";
        // line 89
        echo twig_escape_filter($this->env, site_url("ami/contacts/check_primary_contact"), "html", null, true);
        echo "\",
                    data: {customers_id: val},
                    type: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        if (res == true) {
                            \$('#cusAlert').html('');
                        } else {
                            \$('#cusAlert').html('This Customer has\\'nt got contact primary !');
                        }
                    }
                });
            }
        }

        function toggle(className, obj) {
            var \$input = \$(obj);

            if (\$input.prop('checked')){
                \$(className).show();
                document.getElementById(\"opt_in_val\").value = \"1\";
            } 
            else {
                \$(className).hide();
                document.getElementById(\"opt_in_val\").value = \"0\";
                document.getElementById(\"check_mobile\").value = \"0\";
                document.getElementById(\"check_email\").value = \"0\";
                document.getElementById(\"paper_material\").value = \"0\";
                document.getElementById(\"online_campaign\").value = \"0\";
                document.getElementById(\"any_other_format\").value = \"0\";
            };
        }

        function toggle2(className, obj) {
            var \$input = \$(obj);
            console.log(className);

            if (\$input.prop('checked')){
                \$(className).value = \"1\";
            } 
            else {
                \$(className).value = \"0\";
            };
            console.log(\$(className).value);
        }

        \$( document ).ready(function() {
            var flag = ";
        // line 136
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "opt_in", array()), "html", null, true);
        echo ";
            if (flag == 1) \$('#checkboxes').show();
            else \$('#checkboxes').hide();
        });
    </script>
";
    }

    // line 143
    public function block_css($context, array $blocks = array())
    {
        // line 144
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 145
        echo twig_escape_filter($this->env, site_url("res/css/jquery.signaturepad.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
    <style>
        #theCanvas, .show-sig img {
            border-radius: 10px;
            border: 2px solid orange;
        }

        .sigPad {
            width: 100%;
        }

        .sigWrapper {
            border: none;
        }

        .show-sig img {
            width: 100%;
        }

        .changeButton {
            display: list-item;
            bottom: 0.2em;
            position: absolute;
            right: 0;
            font-size: 0.75em;
            line-height: 1.375;

        }
    </style>
";
    }

    // line 177
    public function block_content($context, array $blocks = array())
    {
        // line 178
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 193
        echo form_open(site_url("ami/contacts/save"), array("class" => "form-horizontal contact-form", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Contacts</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 202
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 204
        echo "                        ";
        echo html_btn(site_url("ami/contacts"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 205
        if ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array())) {
            // line 206
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/contacts/delete/" . $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 209
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 220
        echo form_label("Customers", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 222
        echo form_input(array("name" => "armstrong_2_customers_id", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputCustomers"));
        // line 225
        echo "
                    <span id=\"cusAlert\"></span>
                </div>
                <input type=\"hidden\" name=\"tag_id[]\" id=\"InputCustomersId\"
                       value=\"";
        // line 229
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 0)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
            </div>

            <div class=\"form-group\">
                ";
        // line 233
        echo form_label("Wholesaler", "armstrong_2_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 235
        echo form_input(array("name" => "armstrong_2_wholesalers_id", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputWholesalers"));
        // line 238
        echo "
                    <input type=\"hidden\" name=\"tag_id[]\" id=\"InputWholesalersId\"
                           value=\"";
        // line 240
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 1)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 245
        echo form_label("Distributor", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 247
        echo form_input(array("name" => "armstrong_2_distributors_id", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputDistributors"));
        // line 250
        echo "
                    <input type=\"hidden\" name=\"tag_id[]\" id=\"InputDistributorsId\"
                           value=\"";
        // line 252
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "type", array()) == 2)) ? ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tag_id", array())) : ("")), "html", null, true);
        echo "\"/>
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 257
        echo form_label("Primary Contact", "primary_contact", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 259
        echo form_radio("primary_contact", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 0));
        echo "
                        No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 261
        echo form_radio("primary_contact", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "primary_contact", array()) == 1));
        echo "
                        Yes</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 267
        echo form_label("First Name", "first_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 269
        echo form_input(array("name" => "first_name", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "first_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 274
        echo form_label("Last Name", "last_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 276
        echo form_input(array("name" => "last_name", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "last_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 281
        echo form_label("Position", "position", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 283
        echo form_input(array("name" => "position", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "position", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 288
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-4\">
                    ";
        // line 290
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
                <div class=\"col-sm-2\">
                    ";
        // line 293
        echo form_input(array("name" => "ext", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "ext", array()), "class" => "form-control", "placeholder=\"Ext\" maxlength=\"5\" data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 298
        echo form_label("Phone 2", "phone_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 300
        echo form_input(array("name" => "phone_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_2", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 305
        echo form_label("Phone 3", "phone_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 307
        echo form_input(array("name" => "phone_3", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "phone_3", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 312
        echo form_label("Mobile", "mobile", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 314
        echo form_input(array("name" => "mobile", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 319
        echo form_label("Mobile 2", "mobile_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 321
        echo form_input(array("name" => "mobile_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile_2", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 326
        echo form_label("Mobile 3", "mobile_3", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 328
        echo form_input(array("name" => "mobile_3", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "mobile_3", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 333
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 335
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 340
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 342
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 347
        echo form_label("Email 2", "email_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 349
        echo form_input(array("name" => "email_2", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "email_2", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 354
        echo form_label("Birthday", "birthday", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 356
        echo form_input(array("name" => "birthday", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "birthday", array()), "class" => "form-control", "data-provide" => "datepicker", "data-date-format" => "yyyy-mm-dd", "data-date-start-date" => "-50y"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 361
        echo form_label("Interests", "interests", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 363
        echo form_input(array("name" => "interests", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "interests", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 368
        echo form_label("", "status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 370
        echo form_radio("status", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 0));
        echo " Inactive</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 371
        echo form_radio("status", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "status", array()) == 1));
        echo " Active</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 376
        echo form_label("Remarks", "remarks", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 378
        echo form_input(array("name" => "remarks", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "remarks", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 383
        echo form_label("Facebook", "facebook", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 385
        echo form_input(array("name" => "facebook", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "facebook", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 390
        echo form_label("Instagram", "instagram", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 392
        echo form_input(array("name" => "instagram", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "instagram", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 397
        echo form_label("Linkedln", "linkedln", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 399
        echo form_input(array("name" => "linkedln", "value" => $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "linkedln", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 403
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/contacts/edit.html.twig", 403)->display(array_merge($context, array("entry_type" => "contacts")));
        // line 404
        echo "            ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 1))) {
            // line 405
            echo "                <div class=\"form-group\">
                    ";
            // line 406
            echo form_label("Signature", "signature", array("class" => "control-label col-sm-3"));
            echo "

                    <div class=\"col-sm-6 show-sig\">
                        <ul class=\"sigNav\" style=\"display: block;\">
                            <li class=\"changeButton\"><a href=\"javascript:;\">Change</a></li>
                        </ul>
                        <img src=\"data:image/png;base64,";
            // line 412
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "signature_base64", array()), "html", null, true);
            echo "\" alt=\"\">
                    </div>

                    <div class=\"col-sm-6\" id=\"smoothed\">
                        <ul class=\"sigNav\">
                            <li class=\"drawIt\"><a href=\"#draw-it\" class=\"current\">Draw Signature</a></li>
                            <li class=\"clearButton\"><a href=\"#clear\">Clear</a></li>
                        </ul>
                        <div class=\"sig sigWrapper \" style=\"height:auto;\">
                            <div class=\"typed\"></div>
                            <canvas id=\"theCanvas\" class=\"pad\" height=\"250\"></canvas>
                            <input type=\"hidden\" name=\"output\" class=\"output\">
                        </div>
                    </div>

                </div>
            ";
        }
        // line 429
        echo "            ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 12, 1 => 13, 2 => 14, 3 => 15, 4 => 16, 5 => 17, 6 => 18))) {
            // line 430
            echo "                <div class=\"form-group\">
                    ";
            // line 431
            echo form_label("Tea Relevant", "tea_relevant", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6 no-parsley\">
                        <label class=\"radio-inline\">";
            // line 433
            echo form_radio("tea_relevant", 0, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tea_relevant", array()) == 0));
            echo "
                            No</label>&nbsp;
                        <label class=\"radio-inline\">";
            // line 435
            echo form_radio("tea_relevant", 1, ($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "tea_relevant", array()) == 1));
            echo "
                            Yes</label>&nbsp;
                    </div>
                </div>
            ";
        }
        // line 440
        echo "            ";
        echo form_input(array("name" => "signature_base64", "type" => "hidden", "class" => "hide"));
        echo "
            ";
        // line 441
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "

            <div class=\"form-group\">
                ";
        // line 444
        echo form_label("Opt-in", "opt_in", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    <input type=\"checkbox\" value=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "opt_in", array()), "html", null, true);
        echo "\" id=\"opt_in_val\" name=\"opt_in\" onclick=\"toggle('#checkboxes', this)\" ";
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "opt_in", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Yes</input>
                </div>
            </div>

            <div class=\"form-group\" id=\"checkboxes\">
                ";
        // line 451
        echo form_label("", "", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    <input type=\"checkbox\" value=\"1\" name=\"check_mobile\" onclick=\"toggle2('#check_mobile', this)\" id=\"check_mobile\" ";
        // line 453
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "check_mobile", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Check Mobile</input><br />
                    <input type=\"checkbox\" value=\"1\" name=\"check_email\" onclick=\"toggle2('#check_email', this)\" id=\"check_email\" ";
        // line 454
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "check_email", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Check Email</input><br />
                    <input type=\"checkbox\" value=\"1\" name=\"online_campaign\" onclick=\"toggle2('#online_campaign', this)\" id=\"online_campaign\" ";
        // line 455
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "online_campaign", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Online Campaign</input><br />
                    <input type=\"checkbox\" value=\"1\" name=\"paper_material\" onclick=\"toggle2('#paper_material', this)\" id=\"paper_material\" ";
        // line 456
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "paper_material", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Paper Material</input><br />
                    <input type=\"checkbox\" value=\"1\" name=\"any_other_format\" onclick=\"toggle2('#any_other_format', this)\" id=\"any_other_format\" ";
        // line 457
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "any_other_format", array()) == "1")) ? ("checked") : (""));
        echo ">&nbsp;Any Other Format</input><br />
                </div>
            </div>
            
            <div class=\"form-group\">
                ";
        // line 462
        echo form_label("Opt-in Type", "opt_in_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <select name=\"opt_in_type\" id=\"approved\" class=\"form-control\">
                        <option value=\"\">--Select--</option>
                        <option value=\"verbal\" ";
        // line 466
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "opt_in_type", array()) == "VERBAL")) ? ("selected") : (""));
        echo ">Verbal</option>
                        <option value=\"face to face\" ";
        // line 467
        echo ((($this->getAttribute((isset($context["contacts"]) ? $context["contacts"] : null), "opt_in_type", array()) == "FACE TO FACE")) ? ("selected") : (""));
        echo ">Face to Face</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 474
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/contacts/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  785 => 474,  775 => 467,  771 => 466,  764 => 462,  756 => 457,  752 => 456,  748 => 455,  744 => 454,  740 => 453,  735 => 451,  725 => 446,  720 => 444,  714 => 441,  709 => 440,  701 => 435,  696 => 433,  691 => 431,  688 => 430,  685 => 429,  665 => 412,  656 => 406,  653 => 405,  650 => 404,  648 => 403,  641 => 399,  636 => 397,  628 => 392,  623 => 390,  615 => 385,  610 => 383,  602 => 378,  597 => 376,  589 => 371,  585 => 370,  580 => 368,  572 => 363,  567 => 361,  559 => 356,  554 => 354,  546 => 349,  541 => 347,  533 => 342,  528 => 340,  520 => 335,  515 => 333,  507 => 328,  502 => 326,  494 => 321,  489 => 319,  481 => 314,  476 => 312,  468 => 307,  463 => 305,  455 => 300,  450 => 298,  442 => 293,  436 => 290,  431 => 288,  423 => 283,  418 => 281,  410 => 276,  405 => 274,  397 => 269,  392 => 267,  383 => 261,  378 => 259,  373 => 257,  365 => 252,  361 => 250,  359 => 247,  354 => 245,  346 => 240,  342 => 238,  340 => 235,  335 => 233,  328 => 229,  322 => 225,  320 => 222,  315 => 220,  302 => 209,  295 => 206,  293 => 205,  288 => 204,  284 => 202,  272 => 193,  255 => 178,  252 => 177,  218 => 146,  214 => 145,  209 => 144,  206 => 143,  196 => 136,  146 => 89,  110 => 56,  89 => 38,  68 => 20,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
