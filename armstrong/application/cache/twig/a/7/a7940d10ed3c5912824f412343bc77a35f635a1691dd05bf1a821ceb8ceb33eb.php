<?php

/* ami/setting_country/items/application_setting.html.twig */
class __TwigTemplate_a7940d10ed3c5912824f412343bc77a35f635a1691dd05bf1a821ceb8ceb33eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div role=\"tabpanel\" class=\"tab-pane\" id=\"app_config\">
    <div class=\"form-group\">
        ";
        // line 3
        echo form_label("Price Configuration Pull", "price_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 5
        echo form_radio("application_config[pull][price_config][enable]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "enable", array()) == 1), "class=\"price-yes\" data-type=\"pull\"");
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 7
        echo form_radio("application_config[pull][price_config][enable]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "enable", array()) == 0), "class=\"price-no\" data-type=\"pull\"");
        echo "
                No</label>
            <label class=\"radio-inline\">";
        // line 9
        echo form_radio("application_config[pull][price_config][enable]", 2, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "user_setting", array()) == 1), "class=\"price-no\" data-type=\"pull\"");
        echo "
                User Setting</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 14
        echo form_label("Price Configuration Push", "price_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 16
        echo form_radio("application_config[push][price_config][enable]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "enable", array()) == 1), "class=\"price-yes\" data-type=\"push\"");
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 18
        echo form_radio("application_config[push][price_config][enable]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "enable", array()) == 0), "class=\"price-no\" data-type=\"push\"");
        echo "
                No</label>
            <label class=\"radio-inline\">";
        // line 20
        echo form_radio("application_config[push][price_config][enable]", 2, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "user_setting", array()) == 1), "class=\"price-no\" data-type=\"push\"");
        echo "
                User Setting</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 25
        echo form_label("Price Configuration Leader", "price_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 27
        echo form_radio("application_config[leader][price_config][enable]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "enable", array()) == 1), "class=\"price-yes\" data-type=\"leader\"");
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 29
        echo form_radio("application_config[leader][price_config][enable]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "enable", array()) == 0), "class=\"price-no\" data-type=\"leader\"");
        echo "
                No</label>
            <label class=\"radio-inline\">";
        // line 31
        echo form_radio("application_config[leader][price_config][enable]", 2, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "user_setting", array()) == 1), "class=\"price-no\" data-type=\"leader\"");
        echo "
                User Setting</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 36
        echo form_label("OTM UFS Share", "otm_ufs_share_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 38
        echo form_radio("application_config[pull][otm_ufs_share_config]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "otm_ufs_share_config", array()) == 0));
        echo "
                Country</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 40
        echo form_radio("application_config[pull][otm_ufs_share_config]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "otm_ufs_share_config", array()) == 1));
        echo "
                Country Channel</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 45
        echo form_label("Enable SKU Number", "application_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 47
        echo form_radio("application_config[pull][sku_number]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "sku_number", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 49
        echo form_radio("application_config[pull][sku_number]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "sku_number", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 54
        echo form_label("Toggle armstrong_1_customers_id Pull", "toggle_armstrong_1_customers_id", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 56
        echo form_radio("application_config[pull][toggle_armstrong_1_customers_id]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "toggle_armstrong_1_customers_id", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 58
        echo form_radio("application_config[pull][toggle_armstrong_1_customers_id]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "toggle_armstrong_1_customers_id", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 63
        echo form_label("Toggle armstrong_1_customers_id Push", "toggle_armstrong_1_customers_id", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 65
        echo form_radio("application_config[push][toggle_armstrong_1_customers_id]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "toggle_armstrong_1_customers_id", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 67
        echo form_radio("application_config[push][toggle_armstrong_1_customers_id]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "toggle_armstrong_1_customers_id", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 72
        echo form_label("Toggle armstrong_1_customers_id Leader", "toggle_armstrong_1_customers_id", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 74
        echo form_radio("application_config[leader][toggle_armstrong_1_customers_id]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "toggle_armstrong_1_customers_id", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 76
        echo form_radio("application_config[leader][toggle_armstrong_1_customers_id]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "toggle_armstrong_1_customers_id", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 81
        echo form_label("Customer Signature Pull", "customer_signature", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 83
        echo form_radio("application_config[pull][customer_signature]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "customer_signature", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 85
        echo form_radio("application_config[pull][customer_signature]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "customer_signature", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 90
        echo form_label("Customer Signature Push", "customer_signature", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 92
        echo form_radio("application_config[push][customer_signature]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "customer_signature", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 94
        echo form_radio("application_config[push][customer_signature]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "customer_signature", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 99
        echo form_label("Customer Signature Leader", "customer_signature", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 101
        echo form_radio("application_config[leader][customer_signature]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "customer_signature", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 103
        echo form_radio("application_config[leader][customer_signature]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "customer_signature", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 108
        echo form_label("Analysis Datasource (pull):", "analysis_datasource_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 datasourcepull no-parsley\">
            <label class=\"radio-inline p-l-0\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][analysis_datasource_config][tfo]\" ";
        // line 112
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "analysis_datasource_config", array()), "tfo", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                TFO
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][analysis_datasource_config][ssd]\" ";
        // line 118
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "analysis_datasource_config", array()), "ssd", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                SSD
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][analysis_datasource_config][pc]\" ";
        // line 124
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "analysis_datasource_config", array()), "pc", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                Pantry Check
            </label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 131
        echo form_label("Analysis Datasource (push):", "analysis_datasource_config", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 datasourcepush no-parsley\">
            <label class=\"radio-inline p-l-0\">
                <input type=\"checkbox\"
                       name=\"application_config[push][analysis_datasource_config][tfo]\" ";
        // line 135
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "analysis_datasource_config", array()), "tfo", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                PSD
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[push][analysis_datasource_config][ssd]\" ";
        // line 141
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "analysis_datasource_config", array()), "ssd", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                SSD Push
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[push][analysis_datasource_config][pc]\" ";
        // line 147
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "analysis_datasource_config", array()), "pc", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                Pantry Check
            </label>
        </div>
    </div>
    <!--div class=\"form-group\">
        ";
        // line 154
        echo form_label("Listing and Availability Compute", "list_and_availabiliy_compute", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 datasource no-parsley\">
            <label class=\"radio-inline p-l-0\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][list_and_availabiliy_compute][tfo]\" ";
        // line 158
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "list_and_availabiliy_compute", array()), "tfo", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                TFO
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][list_and_availabiliy_compute][ssd]\" ";
        // line 164
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "list_and_availabiliy_compute", array()), "ssd", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                SSD
            </label>
            <label class=\"radio-inline\">
                <input type=\"checkbox\"
                       name=\"application_config[pull][list_and_availabiliy_compute][pc]\" ";
        // line 170
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "list_and_availabiliy_compute", array()), "pc", array()) == 1)) ? ("checked") : (""));
        echo "
                       value=\"1\">
                Pantry Check
            </label>
        </div>
    </div-->
    <div class=\"form-group\">
        ";
        // line 177
        echo form_label("Phone number length", "phone_length", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6\">
            ";
        // line 179
        echo form_input(array("name" => "application_config[pull][phone_length]", "type" => "number", "value" => $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "phone_length", array()), "class" => "form-control", "placeholder" => 10));
        echo "
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 183
        echo form_label("Enable AMS", "enable_ams", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 185
        echo form_radio("application_config[pull][enable_ams]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_ams", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 187
        echo form_radio("application_config[pull][enable_ams]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_ams", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 192
        echo form_label("Enable Contact Strategy", "contact_strategy", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 194
        echo form_radio("application_config[pull][contact_strategy]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "contact_strategy", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 196
        echo form_radio("application_config[pull][contact_strategy]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "contact_strategy", array()) == 0));
        echo "
                No</label>
        </div>
    </div>

    <div class=\"form-group\">
        ";
        // line 202
        echo form_label("Enable Sales Cycle", "sales_cycle", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 204
        echo form_radio("application_config[pull][sales_cycle]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "sales_cycle", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 206
        echo form_radio("application_config[pull][sales_cycle]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "sales_cycle", array()) == 0));
        echo "
                No</label>
        </div>
    </div>

    <div class=\"form-group\">
        ";
        // line 212
        echo form_label("Enable Potential Customer", "potential_customer", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 214
        echo form_radio("application_config[pull][potential_customer]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "potential_customer", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 216
        echo form_radio("application_config[pull][potential_customer]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "potential_customer", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group enable_shifted_call\">
        ";
        // line 221
        echo form_label("Enable Shifted Call", "enable_shifted_call", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 223
        echo form_radio("application_config[pull][enable_shifted_call]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_shifted_call", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 225
        echo form_radio("application_config[pull][enable_shifted_call]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_shifted_call", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group require_reason\">
        ";
        // line 230
        echo form_label("Require Reasons fields", "require_reason_v4", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline ";
        // line 232
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason", array()), "html", null, true);
        echo "\">";
        echo form_radio("application_config[pull][require_reason_v4]", 1, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array(), "any", false, true), "require_reason_v4", array(), "any", true, true) && ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason_v4", array()) == 1)) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason", array()) == 1)));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 234
        echo form_radio("application_config[pull][require_reason_v4]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array(), "any", false, true), "require_reason_v4", array(), "any", true, true) && ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason_v4", array()) == 0)) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason", array()) == 0)));
        echo "
                No</label>
            <label class=\"radio-inline\">";
        // line 236
        echo form_radio("application_config[pull][require_reason_v4]", 2, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "require_reason_v4", array()) == 2));
        echo "
                No without prompt</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 241
        echo form_label("Enable discount approval", "enable_discount", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 243
        echo form_radio("application_config[leader][approve][enable_discount]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "approve", array()), "enable_discount", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 245
        echo form_radio("application_config[leader][approve][enable_discount]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "approve", array()), "enable_discount", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 250
        echo form_label("Enable out of trade approval", "enable_out_of_trade", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 252
        echo form_radio("application_config[leader][approve][enable_out_of_trade]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "approve", array()), "enable_out_of_trade", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 254
        echo form_radio("application_config[leader][approve][enable_out_of_trade]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "approve", array()), "enable_out_of_trade", array()) == 0));
        echo "
                No</label>
        </div>
    </div>
    <div class=\"form-group\">
        ";
        // line 259
        echo form_label("Enable pantry check warning (push)", "enable_pantry_check_warning", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 261
        echo form_radio("application_config[pull][enable_pantry_check_warning]", 1, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_pantry_check_warning", array()) == 1) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_pantry_check_warning", array()) == "")));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 263
        echo form_radio("application_config[pull][enable_pantry_check_warning]", 0, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_pantry_check_warning", array()) == "0"));
        echo "
                No</label>
        </div>
    </div>
    ";
        // line 267
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 4, 1 => 88))) {
            // line 268
            echo "        ";
            if (((isset($context["activeAVP"]) ? $context["activeAVP"] : null) > 0)) {
                // line 269
                echo "            <p style=\"padding-left: 16.5%; color: red\">There is an active SKU Point. AVP toggle is disabled.</p>
        ";
            }
            // line 271
            echo "        <div class=\"form-group\">
            ";
            // line 272
            echo form_label("Enable Enhanced AVP", "enable_enhanced_avp", array("class" => "control-label col-sm-4"));
            echo "

            ";
            // line 274
            if (((isset($context["activeAVP"]) ? $context["activeAVP"] : null) > 0)) {
                // line 275
                echo "                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
                // line 276
                echo form_radio("application_config[pull][enable_enhanced_avp]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == 1), "disabled=disabled");
                echo "
                        Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
                // line 278
                echo form_radio("application_config[pull][enable_enhanced_avp]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == "0") || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == "")), "disabled=disabled");
                echo "
                        No </label>
                </div>
            ";
            } else {
                // line 282
                echo "                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
                // line 283
                echo form_radio("application_config[pull][enable_enhanced_avp]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == 1));
                echo "
                        Yes</label>&nbsp;
                    <label class=\"radio-inline\">";
                // line 285
                echo form_radio("application_config[pull][enable_enhanced_avp]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == 0) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_enhanced_avp", array()) == "")));
                echo "
                        No </label>
                </div>
            ";
            }
            // line 289
            echo "
        </div>
    ";
        }
        // line 292
        echo "    <div class=\"form-group\">
        ";
        // line 293
        echo form_label("Enable Call Type (PULL)", "enable_call_type", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 295
        echo form_radio("application_config[pull][enable_call_type]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_call_type", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 297
        echo form_radio("application_config[pull][enable_call_type]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_call_type", array()) == 0) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_call_type", array()) == "")));
        echo "
                No</label>
        </div>
    </div>

    <div class=\"form-group\">
        ";
        // line 303
        echo form_label("Enable Customers and Contacts Edit for Secondary SR (PULL)", "enable_contact_cust_edit_secondary", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 305
        echo form_radio("application_config[pull][enable_contact_cust_edit_secondary]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_contact_cust_edit_secondary", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 307
        echo form_radio("application_config[pull][enable_contact_cust_edit_secondary]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_contact_cust_edit_secondary", array()) == 0) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_contact_cust_edit_secondary", array()) == "")));
        echo "
                No</label>
        </div>
    </div>

    <div class=\"form-group\">
        ";
        // line 313
        echo form_label("Enable Pantry Check, Sampling and Competitor for Secondary SR (PULL)", "enable_full_call_access_secondary", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 315
        echo form_radio("application_config[pull][enable_full_call_access_secondary]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_full_call_access_secondary", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 317
        echo form_radio("application_config[pull][enable_full_call_access_secondary]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_full_call_access_secondary", array()) == 0) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_full_call_access_secondary", array()) == "")));
        echo "
                No</label>
        </div>
    </div>

    <div class=\"form-group\">
        ";
        // line 323
        echo form_label("Enable Grab Display (PULL)", "enable_grab_display", array("class" => "control-label col-sm-4"));
        echo "
        <div class=\"col-sm-6 no-parsley\">
            <label class=\"radio-inline\">";
        // line 325
        echo form_radio("application_config[pull][enable_grab_display]", 1, ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_grab_display", array()) == 1));
        echo "
                Yes</label>&nbsp;
            <label class=\"radio-inline\">";
        // line 327
        echo form_radio("application_config[pull][enable_grab_display]", 0, (($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_grab_display", array()) == 0) || ($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "enable_grab_display", array()) == "")));
        echo "
                No</label>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/setting_country/items/application_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  647 => 327,  642 => 325,  637 => 323,  628 => 317,  623 => 315,  618 => 313,  609 => 307,  604 => 305,  599 => 303,  590 => 297,  585 => 295,  580 => 293,  577 => 292,  572 => 289,  565 => 285,  560 => 283,  557 => 282,  550 => 278,  545 => 276,  542 => 275,  540 => 274,  535 => 272,  532 => 271,  528 => 269,  525 => 268,  523 => 267,  516 => 263,  511 => 261,  506 => 259,  498 => 254,  493 => 252,  488 => 250,  480 => 245,  475 => 243,  470 => 241,  462 => 236,  457 => 234,  450 => 232,  445 => 230,  437 => 225,  432 => 223,  427 => 221,  419 => 216,  414 => 214,  409 => 212,  400 => 206,  395 => 204,  390 => 202,  381 => 196,  376 => 194,  371 => 192,  363 => 187,  358 => 185,  353 => 183,  346 => 179,  341 => 177,  331 => 170,  322 => 164,  313 => 158,  306 => 154,  296 => 147,  287 => 141,  278 => 135,  271 => 131,  261 => 124,  252 => 118,  243 => 112,  236 => 108,  228 => 103,  223 => 101,  218 => 99,  210 => 94,  205 => 92,  200 => 90,  192 => 85,  187 => 83,  182 => 81,  174 => 76,  169 => 74,  164 => 72,  156 => 67,  151 => 65,  146 => 63,  138 => 58,  133 => 56,  128 => 54,  120 => 49,  115 => 47,  110 => 45,  102 => 40,  97 => 38,  92 => 36,  84 => 31,  79 => 29,  74 => 27,  69 => 25,  61 => 20,  56 => 18,  51 => 16,  46 => 14,  38 => 9,  33 => 7,  28 => 5,  23 => 3,  19 => 1,);
    }
}
