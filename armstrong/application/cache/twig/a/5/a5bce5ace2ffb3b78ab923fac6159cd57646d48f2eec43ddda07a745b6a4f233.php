<?php

/* ami/setting_cycle/index.html.twig */
class __TwigTemplate_a5bce5ace2ffb3b78ab923fac6159cd57646d48f2eec43ddda07a745b6a4f233 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_cycle/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function () {

\t\$('.cycle-date').daterangepicker({
\t\tformat: 'YYYY-MM-DD',
\t})
\t.on('show.daterangepicker', function(ev, picker) {
\t\tvar \$this = \$(this),
\t\t\tpicker = \$this.data('daterangepicker'),
\t\t\tminDate = \$this.data('mindate'),
\t\t\tmaxDate = \$this.data('maxdate'),
\t\t\toptions = {format: 'YYYY-MM-DD'};

\t\tif (minDate)
\t\t{
\t\t\toptions.minDate = minDate;\t
\t\t}

\t\tif (maxDate)
\t\t{
\t\t\toptions.maxDate = maxDate
\t\t}

\t\tpicker.setOptions(options);
\t})
\t.on('apply.daterangepicker', function(ev, picker) {
\t\tvar \$this = \$(this)
\t\t\t\$parent = \$this.closest('.form-group')
\t\t\t\$startDate = \$parent.find('.cycle-date-start'),
\t\t\t\$endDate = \$parent.find('.cycle-date-end'),
\t\t\tstartDate = picker.startDate.format('YYYY-MM-DD'),
\t\t\tendDate = picker.endDate.format('YYYY-MM-DD');

\t\t\$startDate.val(startDate);
\t\t\$endDate.val(endDate);
\t});

});
</script>
";
    }

    // line 48
    public function block_css($context, array $blocks = array())
    {
        // line 49
        echo "\t";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 53
    public function block_content($context, array $blocks = array())
    {
        // line 54
        echo "\t";
        echo form_open(site_url("ami/setting_cycle/update"), array("class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "

\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1 class=\"pull-left\">OTM Cycle Settings</h1>

\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t<div class=\"btn-header-toolbar\">
\t\t\t\t\t\t";
        // line 63
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", "setting_cycle")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "setting_cycle")))) {
            // line 64
            echo "\t\t\t\t\t\t\t";
            echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
            echo "
\t\t\t\t\t\t";
        }
        // line 66
        echo "
\t\t\t\t\t\t";
        // line 67
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", "setting_cycle"))) {
            // line 68
            echo "\t\t\t\t\t\t\t";
            echo html_btn(site_url("ami/setting_cycle/add"), "<i class=\"fa fa-fw fa-plus\"></i> Add</a>", array("class" => "btn-default"));
            echo "
\t\t\t\t\t\t";
        }
        // line 70
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t";
        // line 79
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cycles"]) ? $context["cycles"] : null));
        foreach ($context['_seq'] as $context["i"] => $context["cycle"]) {
            // line 80
            echo "\t\t\t\t";
            $context["minDate"] = (($this->getAttribute($this->getAttribute((isset($context["cycles"]) ? $context["cycles"] : null), ($context["i"] - 1), array(), "array"), "date_end", array(), "array")) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["cycles"]) ? $context["cycles"] : null), ($context["i"] - 1), array(), "array"), "date_end", array(), "array"), "addDay", array(), "method")) : (""));
            // line 81
            echo "\t\t\t\t";
            $context["maxDate"] = (($this->getAttribute($this->getAttribute((isset($context["cycles"]) ? $context["cycles"] : null), ($context["i"] + 1), array(), "array"), "date_start", array(), "array")) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["cycles"]) ? $context["cycles"] : null), ($context["i"] + 1), array(), "array"), "date_start", array(), "array"), "subDay", array(), "method")) : (""));
            // line 82
            echo "                <div class=\"form-group\">
                    ";
            // line 83
            echo form_label("Cycle Name", ("cycle_" . $this->getAttribute($context["cycle"], "id", array())), array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 85
            echo form_input(array("name" => (("cycle[" . $this->getAttribute(            // line 86
$context["cycle"], "id", array())) . "][cycle_name]"), "value" => $this->getAttribute(            // line 87
$context["cycle"], "cycle_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
            // line 90
            echo "
                    </div>
                </div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
            // line 94
            echo form_label("Cycle", ("cycle_" . $this->getAttribute($context["cycle"], "id", array())), array("class" => "control-label col-sm-3"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 96
            echo form_input(array("name" => ("cycle_" . $this->getAttribute(            // line 97
$context["cycle"], "id", array())), "value" => $this->getAttribute(            // line 98
$context["cycle"], "date_range", array()), "class" => "form-control cycle-date", "data-mindate" => $this->getAttribute(            // line 100
(isset($context["minDate"]) ? $context["minDate"] : null), "toDateString", array(), "method"), "data-maxdate" => $this->getAttribute(            // line 101
(isset($context["maxDate"]) ? $context["maxDate"] : null), "toDateString", array(), "method")));
            // line 102
            echo "

\t\t\t\t\t\t";
            // line 104
            echo form_input(array("name" => (("cycle[" . $this->getAttribute(            // line 105
$context["cycle"], "id", array())) . "][date_start]"), "value" => $this->getAttribute(            // line 106
$context["cycle"], "date_start", array()), "type" => "hidden", "class" => "hide cycle-date-start"));
            // line 109
            echo "

\t\t\t\t\t\t";
            // line 111
            echo form_input(array("name" => (("cycle[" . $this->getAttribute(            // line 112
$context["cycle"], "id", array())) . "][date_end]"), "value" => $this->getAttribute(            // line 113
$context["cycle"], "date_end", array()), "type" => "hidden", "class" => "hide cycle-date-end"));
            // line 116
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t<a href=\"";
            // line 119
            echo twig_escape_filter($this->env, site_url(("ami/setting_cycle/delete/" . $this->getAttribute($context["cycle"], "id", array()))), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\" class=\"btn btn-default btn-clone delete-cycle-btn\"><i class=\"fa fa-times\"></i></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
                <hr/>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['cycle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "\t\t</div>
\t</div>

\t";
        // line 127
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_cycle/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 127,  217 => 124,  206 => 119,  201 => 116,  199 => 113,  198 => 112,  197 => 111,  193 => 109,  191 => 106,  190 => 105,  189 => 104,  185 => 102,  183 => 101,  182 => 100,  181 => 98,  180 => 97,  179 => 96,  174 => 94,  168 => 90,  166 => 87,  165 => 86,  164 => 85,  159 => 83,  156 => 82,  153 => 81,  150 => 80,  146 => 79,  135 => 70,  129 => 68,  127 => 67,  124 => 66,  118 => 64,  116 => 63,  103 => 54,  100 => 53,  94 => 50,  89 => 49,  86 => 48,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
