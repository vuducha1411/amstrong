<?php

/* ami/recipes/preview.html.twig */
class __TwigTemplate_ac2d7f06b349312fb46758bfdb224175c5fe3800429346adb079f5c12d602635 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <h3>Preparation</h3>               
            <p>";
        // line 9
        echo $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "preparation", array());
        echo "</p>
            <br>
            <h3>Ingredients</h3>               
            <p>";
        // line 12
        echo $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "ingredients", array());
        echo "</p>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>            
            ";
        // line 16
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "recipes"))) {
            // line 17
            echo "                ";
            echo html_btn(site_url(("ami/recipes/edit/" . $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 19
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/recipes/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 19,  47 => 17,  45 => 16,  38 => 12,  32 => 9,  25 => 5,  19 => 1,);
    }
}
