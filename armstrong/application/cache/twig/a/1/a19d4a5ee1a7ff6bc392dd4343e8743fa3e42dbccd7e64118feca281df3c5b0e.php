<?php

/* ami/pantry_check/index.html.twig */
class __TwigTemplate_a19d4a5ee1a7ff6bc392dd4343e8743fa3e42dbccd7e64118feca281df3c5b0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/pantry_check/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/call_modules.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 17
        echo twig_escape_filter($this->env, site_url("ami/pantry_check/ajaxData"), "html", null, true);
        echo "\",*/
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    // \$('#Generate').click(function(e) {
    //     xhr = null;
    //     var \$startDate = \$('#InputStartDate'),
    //         \$endDate = \$('#InputEndDate'),
    //         \$container = \$('#PantryChecksContainer');

    //     if (xhr) {
    //         xhr.abort();
    //     }

    //     if (!xhr && \$startDate.val() && \$endDate.val()) {
            
    //         \$container.html('').empty().html('<div class=\"col-sm-6 col-sm-offset-3\"><i class=\"fa fa-spinner fa-spin\"></i> Loading...</div>');

    //         \$.ajax({
    //             url: \"";
        // line 42
        echo twig_escape_filter($this->env, site_url("ami/pantry_check/pantry_check_data"), "html", null, true);
        echo "\",
    //             type: 'post',
    //             data: { start_date: \$startDate.val(), end_date: \$endDate.val() , draft: ";
        // line 44
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo " },
    //             success: function(res) {
    //                 try {
    //                     var resJson = JSON.parse(res);
    //                     if (resJson._session_expried) {
    //                         window.location.reload();
    //                         return;
    //                     }
                        
    //                 } catch(e) {}
                    
    //                 \$container.html('').empty();
    //                 if (res) {
    //                     \$container.append(res);
    //                     CheckAll(\$('input.CheckAll'));
    //                 }

    //                 xhr = null;
    //             }
    //         });
    //     }
    // });

    \$('#InputDateRange').daterangepicker({
        format: 'YYYY-MM-DD'
    }).on('apply.daterangepicker', function(ev, picker) {
        \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD'));
        \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD'));
    });
});
</script>
";
    }

    // line 77
    public function block_css($context, array $blocks = array())
    {
        // line 78
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 80
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 83
    public function block_content($context, array $blocks = array())
    {
        // line 84
        echo "
    ";
        // line 85
        echo form_open(site_url("ami/pantry_check/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 90
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Pantry Check Draft") : ("Pantry Check"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 93
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 95
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/pantry_check/index.html.twig", 95)->display(array_merge($context, array("url" => "ami/pantry_check", "icon" => "fa-binoculars", "title" => "Pantry Check", "permission" => "pantry_check", "showImport" => 1)));
        // line 96
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            ";
        // line 150
        echo "            
            ";
        // line 151
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 152
            echo "                <div class=\"form-group\">
                    ";
            // line 153
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 155
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"SalespersonsSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 159
        echo "            
            <div class=\"form-group\">
                ";
        // line 161
        echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 163
        echo form_input(array("name" => "date_time", "value" => null, "class" => "form-control", "id" => "InputDateRange"));
        // line 166
        echo "
                </div>

                <input type=\"hidden\" id=\"InputStartDate\" value=\"\" />
                <input type=\"hidden\" id=\"InputEndDate\" value=\"\" />
            </div>

            ";
        // line 178
        echo "
            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\" 
                        data-url=\"";
        // line 182
        echo twig_escape_filter($this->env, site_url("ami/pantry_check/pantry_check_data"), "html", null, true);
        echo "\"
                        data-container=\"#PantryChecksContainer\"
                        data-draft=\"";
        // line 184
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo "\">Generate</a>
                </div>
            </div>

            <div id=\"PantryChecksContainer\"></div>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 194
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/pantry_check/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 194,  245 => 184,  240 => 182,  234 => 178,  225 => 166,  223 => 163,  218 => 161,  214 => 159,  207 => 155,  202 => 153,  199 => 152,  197 => 151,  194 => 150,  183 => 96,  181 => 95,  176 => 93,  170 => 90,  162 => 85,  159 => 84,  156 => 83,  150 => 80,  146 => 79,  142 => 78,  139 => 77,  103 => 44,  98 => 42,  70 => 17,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
