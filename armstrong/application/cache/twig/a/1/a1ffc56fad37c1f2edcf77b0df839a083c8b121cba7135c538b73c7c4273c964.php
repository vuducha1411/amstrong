<?php

/* ami/perfect_store/index.html.twig */
class __TwigTemplate_a1ffc56fad37c1f2edcf77b0df839a083c8b121cba7135c538b73c7c4273c964 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/perfect_store/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        \"processing\": true,
        \"serverSide\": true,
        \"ajax\": {
            \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/perfect_store/ajaxData"), "html", null, true);
        echo "\",
            \"type\": \"POST\"
        },
        'aoColumns': [
            {mData : \"id\", \"sWidth\": \"10%\" },
            {mData : \"armstrong_2_salespersons_id\"},
            {mData : \"armstrong_2_call_records_id\"},
            {mData : \"type\"},
            {mData : \"date_created\", \"bSearchable\": false},
            {mData : \"last_updated\", \"bSearchable\": false}
        ],
        \"fnRowCallback\": function( nRow, aData, iDisplayIndex ) {
            \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"'+ aData['DT_RowId'] +'\"></td>');
            \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">'+ aData['buttons'] +'</td>');
        },
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        // 'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
});
</script>
";
    }

    // line 43
    public function block_css($context, array $blocks = array())
    {
        // line 44
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 48
    public function block_content($context, array $blocks = array())
    {
        // line 49
        echo "
    ";
        // line 50
        echo form_open(site_url("ami/perfect_store/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 55
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Perfect Store Draft") : ("Perfect Store"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 58
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    <span class=\"btn-header-toolbar m-r\">
                        ";
        // line 61
        echo html_btn(site_url("ami/perfect_store/cc_template"), "C&C Perfect Store", array("class" => "btn-info"));
        // line 63
        echo "
                    </span>
                    <span class=\"btn-header-toolbar m-r\">                   
                        ";
        // line 66
        echo html_btn(site_url("ami/perfect_store/template"), "Perfect Store Template", array("class" => "btn-info"));
        // line 68
        echo "
                    </span>
                    ";
        // line 70
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/perfect_store/index.html.twig", 70)->display(array_merge($context, array("url" => "ami/perfect_store", "icon" => "fa-star", "title" => "Perfect Store", "permission" => "perfect_store", "export" => 0, "showImport" => 0)));
        // line 71
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            <div class=\"panel panel-default\">                
                
                <div class=\"panel-body\">                
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 87
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "perfect_store"))) {
            // line 88
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 90
        echo "                                    <th>ID</th>
                                    <th>Salesperson ID</th>
                                    <th>Call Record ID</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 101
        echo "                                    ";
        // line 102
        echo "                                        ";
        // line 103
        echo "                                            ";
        // line 104
        echo "                                        ";
        // line 105
        echo "                                        ";
        // line 106
        echo "                                        ";
        // line 107
        echo "                                        ";
        // line 108
        echo "                                        ";
        // line 109
        echo "                                        ";
        // line 110
        echo "                                            ";
        // line 111
        echo "                                                ";
        // line 112
        echo "                                            ";
        // line 113
        echo "                                        ";
        // line 114
        echo "                                    ";
        // line 115
        echo "                                ";
        // line 116
        echo "                            </tbody>
                        </table>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 129
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/perfect_store/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 129,  210 => 116,  208 => 115,  206 => 114,  204 => 113,  202 => 112,  200 => 111,  198 => 110,  196 => 109,  194 => 108,  192 => 107,  190 => 106,  188 => 105,  186 => 104,  184 => 103,  182 => 102,  180 => 101,  168 => 90,  164 => 88,  162 => 87,  144 => 71,  142 => 70,  138 => 68,  136 => 66,  131 => 63,  129 => 61,  123 => 58,  117 => 55,  109 => 50,  106 => 49,  103 => 48,  97 => 45,  93 => 44,  90 => 43,  60 => 16,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
