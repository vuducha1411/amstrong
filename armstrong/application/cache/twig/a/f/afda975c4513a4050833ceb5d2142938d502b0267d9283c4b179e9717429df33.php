<?php

/* ami/customers/index.html.twig */
class __TwigTemplate_afda975c4513a4050833ceb5d2142938d502b0267d9283c4b179e9717429df33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/customers/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url(("ami/customers/ajaxData?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"c.armstrong_2_customers_id\"},
                    {mData: \"c.armstrong_2_customers_name\"},
                    {mData: \"c.armstrong_2_salespersons_id\"},
                    {mData: \"\$.salesperson_name\"},
                    {mData: \"c.approved\"},
                    {mData: \"c.date_created\", \"bSearchable\": false},
                    {mData: \"c.last_updated\", \"bSearchable\": false},
                    {mData: \"c.updated_name\"},
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    ";
        // line 30
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "customer"))) {
            // line 31
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    ";
        }
        // line 33
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
//                    ,{
//                        \"targets\": [3],
//                        \"bSearchable\": false
//                    }
//                    ,{
//                        \"targets\": [4],
//                        \"bSearchable\": false
//                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 66
    public function block_css($context, array $blocks = array())
    {
        // line 67
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 71
    public function block_content($context, array $blocks = array())
    {
        // line 72
        echo "
    ";
        // line 73
        echo form_open(site_url("ami/customers/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 78
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Customers Draft") : ("Customers"));
        echo "</h1>

                <div class=\"text-right\">
\t\t\t\t\t<span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
\t\t\t\t\t\t";
        // line 82
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
\t\t\t\t\t</span>
                    ";
        // line 84
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/customers/index.html.twig", 84)->display(array_merge($context, array("url" => "ami/customers", "icon" => "fa-user", "title" => "Customers", "permission" => "customer", "showImport" => 1)));
        // line 85
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 94
        if ((isset($context["message"]) ? $context["message"] : null)) {
            // line 95
            echo "                <div class=\"alert alert-success alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <strong>";
            // line 98
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "</strong>
                </div>
            ";
        }
        // line 101
        echo "            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 102
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "active")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                            href=\"";
        // line 103
        echo twig_escape_filter($this->env, site_url("ami/customers?filter=active"), "html", null, true);
        echo "\">Active</a>
                </li>
                <li role=\"presentation\" ";
        // line 105
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                              href=\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("ami/customers?filter=inactive"), "html", null, true);
        echo "\">Inactive</a>
                </li>
                ";
        // line 108
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")))) {
            // line 109
            echo "                    <li role=\"presentation\" ";
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo "><a role=\"menuitem\"
                                                                                                 href=\"";
            // line 110
            echo twig_escape_filter($this->env, site_url("ami/customers?filter=pending"), "html", null, true);
            echo "\">Pending</a>
                    </li>
                ";
        }
        // line 113
        echo "                ";
        // line 114
        echo "                ";
        // line 115
        echo "                ";
        // line 116
        echo "            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 125
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "customer"))) {
            // line 126
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 130
        echo "                                <th>ID</th>
                                <th>Name </th>
                                <th>Salesperson ID</th>
                                <th>Salesperson Name</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Updated By</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 143
        echo "                            ";
        // line 144
        echo "                            ";
        // line 145
        echo "                            ";
        // line 146
        echo "                            ";
        // line 147
        echo "                            ";
        // line 148
        echo "                            ";
        // line 149
        echo "                            ";
        // line 150
        echo "                            ";
        // line 151
        echo "                            ";
        // line 152
        echo "                            ";
        // line 153
        echo "                            ";
        // line 154
        echo "                            ";
        // line 155
        echo "                            ";
        // line 156
        echo "                            ";
        // line 157
        echo "                            ";
        // line 158
        echo "                            ";
        // line 159
        echo "                            ";
        // line 160
        echo "                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 172
        echo form_close();
        echo "
    ";
        // line 173
        $this->loadTemplate("ami/components/modal_import.html.twig", "ami/customers/index.html.twig", 173)->display($context);
        // line 174
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/customers/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 174,  310 => 173,  306 => 172,  292 => 160,  290 => 159,  288 => 158,  286 => 157,  284 => 156,  282 => 155,  280 => 154,  278 => 153,  276 => 152,  274 => 151,  272 => 150,  270 => 149,  268 => 148,  266 => 147,  264 => 146,  262 => 145,  260 => 144,  258 => 143,  244 => 130,  238 => 126,  236 => 125,  225 => 116,  223 => 115,  221 => 114,  219 => 113,  213 => 110,  208 => 109,  206 => 108,  201 => 106,  197 => 105,  192 => 103,  188 => 102,  185 => 101,  179 => 98,  174 => 95,  172 => 94,  161 => 85,  159 => 84,  154 => 82,  147 => 78,  139 => 73,  136 => 72,  133 => 71,  127 => 68,  122 => 67,  119 => 66,  84 => 33,  80 => 31,  78 => 30,  61 => 16,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
