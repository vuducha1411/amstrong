<?php

/* ami/minitools/swapsku.html.twig */
class __TwigTemplate_a4621fd25fdc8e560db2d48c44d9ef7b40f8497536fb73baa40b41e6bc445538 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/minitools/swapsku.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 12
    public function block_css($context, array $blocks = array())
    {
        // line 13
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"row row sticky sticky-h1 bg-white\">
                    <div class=\"col-lg-12\">
                        <div class=\"page-header nm\">
                            <h1 class=\"pull-left\"></h1>
                            <div class=\"text-right\">
                                <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                                    <a class=\"btn btn-sm btn-warning Tooltip\" id=\"ImportPickfiles\" href=\"javascript:;\"
                                       data-url=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("/ami/minitools/import"), "html", null, true);
        echo "?exelname=";
        echo twig_escape_filter($this->env, (isset($context["exelname"]) ? $context["exelname"] : null), "html", null, true);
        echo "\" title=\"Max records 10000 (only csv)\">
                                        <i class=\"fa fw fa-upload\"></i> Import</a>
                                    <a class=\"btn btn-sm btn-primary\" id=\"getTemplateimport\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "res/up/minitools/template/Tfo_Swap.csv\"
                                       target=\"_blank\" title=\"Get template import\"><i
                                                class=\"fa fw fa-download\"></i>Get Template</a>
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class=\"panel-body\">
                    ";
        // line 41
        if ((isset($context["tfoswap"]) ? $context["tfoswap"] : null)) {
            // line 42
            echo "                        <table class=\"table\">
                            <tr>
                                <th>STT</th>
                                <th>TFO</th>
                                <th>CALL</th>
                                <th>SKU OLD</th>
                                <th>SKU NEW</th>
                            </tr>
                            ";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tfoswap"]) ? $context["tfoswap"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["table"]) {
                // line 51
                echo "                                <tr>
                                    <td>";
                // line 52
                echo twig_escape_filter($this->env, ($context["key"] + 1), "html", null, true);
                echo "</td>
                                    <td>";
                // line 53
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 0, array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 1, array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 2, array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["table"], 3, array(), "array"), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['table'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                        </table>
                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, site_url("/ami/minitools/runswapsku"), "html", null, true);
            echo "\">Run Swap For TFO</a>
                        </br>
                        <a href=\"";
            // line 62
            echo twig_escape_filter($this->env, site_url("/ami/minitools/runswapskudetails"), "html", null, true);
            echo "\">Run Swap For TFO Detail</a>
                    ";
        } else {
            // line 64
            echo "                        <p>Please upload file exel</p>
                    ";
        }
        // line 66
        echo "                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/minitools/swapsku.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 66,  164 => 64,  159 => 62,  154 => 60,  151 => 59,  142 => 56,  138 => 55,  134 => 54,  130 => 53,  126 => 52,  123 => 51,  119 => 50,  109 => 42,  107 => 41,  93 => 30,  86 => 28,  74 => 18,  71 => 17,  65 => 14,  60 => 13,  57 => 12,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
