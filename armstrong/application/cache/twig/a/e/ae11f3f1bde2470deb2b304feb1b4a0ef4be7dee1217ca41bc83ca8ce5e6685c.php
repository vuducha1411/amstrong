<?php

/* ami/base.html.twig */
class __TwigTemplate_ae11f3f1bde2470deb2b304feb1b4a0ef4be7dee1217ca41bc83ca8ce5e6685c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ami/base.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'css' => array($this, 'block_css'),
            'js' => array($this, 'block_js'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["page_title"]) ? $context["page_title"] : null), "html", null, true);
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "\"></script>
    <script>
        var BASE_URL = '";
        // line 10
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "',
                CURRENT_URL = '";
        // line 11
        echo twig_escape_filter($this->env, current_url(), "html", null, true);
        echo "';
        function base_url() {
            return BASE_URL;
        }
        function current_url() {
            return CURRENT_URL;
        }
    </script>
";
    }

    // line 21
    public function block_css($context, array $blocks = array())
    {
        // line 22
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("res/css/jqueryui/jquery-ui-1.10.4.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("res/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, site_url("res/css/ami.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/css/plugins/metisMenu/metisMenu.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/css/plugins/parsley/parsley.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 30
    public function block_js($context, array $blocks = array())
    {
        // line 31
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, site_url("res/js/plugins/slimscroll/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, site_url("res/js/plugins/metisMenu/metisMenu.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootbox/bootbox.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, site_url("res/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, site_url("res/js/plugins/parsley/parsley.remote.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\"
            src=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url("res/js/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"), "html", null, true);
        echo "\"></script>

";
    }

    // line 44
    public function block_body($context, array $blocks = array())
    {
        // line 45
        echo "
    <div id=\"wrapper\">
        <nav class=\"navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
            ";
        // line 48
        $this->loadTemplate("ami/components/nav.html.twig", "ami/base.html.twig", 48)->display($context);
        // line 49
        echo "            ";
        // line 50
        echo "                ";
        $this->loadTemplate("ami/components/sidebar.html.twig", "ami/base.html.twig", 50)->display($context);
        // line 51
        echo "            ";
        // line 52
        echo "        </nav>

        <div id=\"page-wrapper\">
            ";
        // line 55
        $this->displayBlock('content', $context, $blocks);
        // line 58
        echo "        </div>
    </div>

    ";
        // line 61
        $this->loadTemplate("ami/components/modal_import.html.twig", "ami/base.html.twig", 61)->display($context);
        // line 62
        echo "
";
    }

    // line 55
    public function block_content($context, array $blocks = array())
    {
        // line 56
        echo "                ";
        $this->loadTemplate(((isset($context["subview"]) ? $context["subview"] : null) . ".html.twig"), "ami/base.html.twig", 56)->display($context);
        // line 57
        echo "            ";
    }

    public function getTemplateName()
    {
        return "ami/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 57,  183 => 56,  180 => 55,  175 => 62,  173 => 61,  168 => 58,  166 => 55,  161 => 52,  159 => 51,  156 => 50,  154 => 49,  152 => 48,  147 => 45,  144 => 44,  137 => 40,  132 => 38,  128 => 37,  124 => 36,  120 => 35,  116 => 34,  111 => 32,  106 => 31,  103 => 30,  97 => 27,  93 => 26,  89 => 25,  85 => 24,  81 => 23,  76 => 22,  73 => 21,  60 => 11,  56 => 10,  51 => 8,  47 => 7,  42 => 6,  39 => 5,  33 => 3,  11 => 1,);
    }
}
