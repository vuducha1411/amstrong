<?php

/* ami/accessibility/salespersons/modal_edit.html.twig */
class __TwigTemplate_a9da9f1b45bf55fa3dc268bf998d5f7fab0a08015fd0ae16832d7956c0af1af1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    ";
        // line 2
        echo form_open(("ami/accessibility/salespersons/edit/" . $this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "armstrong_2_salespersons_id", array())), array("class" => "modal-content form-horizontal"), array("id" => $this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "armstrong_2_salespersons_id", array())));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "first_name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "last_name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
    
                ";
        // line 10
        if ((twig_length_filter($this->env, (isset($context["roles"]) ? $context["roles"] : null)) > 1)) {
            // line 11
            echo "                    <div class=\"form-group\">
                        ";
            // line 12
            echo form_label("Role", "roles_id", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 14
            echo form_dropdown("roles_id", (isset($context["roles"]) ? $context["roles"] : null), $this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "roles_id", array()), "class=\"form-control\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 18
        echo "
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 23
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Proceed", "class" => "delete btn btn-danger"));
        echo "
        </div>
    ";
        // line 25
        echo form_close();
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "ami/accessibility/salespersons/modal_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 25,  62 => 23,  55 => 18,  48 => 14,  43 => 12,  40 => 11,  38 => 10,  28 => 5,  22 => 2,  19 => 1,);
    }
}
