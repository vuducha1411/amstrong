<?php

/* ami/setting_module_fields/module_fields_edit.html.twig */
class __TwigTemplate_a9538efa7af83f9f52e6292aa3c4e4eea61e2659a91d297584f0437212cc0550 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_module_fields/module_fields_edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 18
        echo form_open(site_url("ami/setting_module_fields/module_fields_save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "filter" => (isset($context["filter"]) ? $context["filter"] : null)));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Questions</h1>

                <div class=\"text-right\">
                    ";
        // line 26
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/setting_module_fields/module_fields_edit.html.twig", 26)->display(array_merge($context, array("url" => "ami/setting_module_fields/module_fields", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "setting_module_fields")));
        // line 27
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 36
        echo form_label("Field", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 38
        echo form_input(array("name" => "field", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "field", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group select_cat\" >
                ";
        // line 42
        echo form_label("Module", "modules", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 44
        echo form_dropdown("module", (isset($context["modules"]) ? $context["modules"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "module", array()), "class=\"form-control form_select_cat\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            ";
        // line 48
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 52
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_module_fields/module_fields_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 52,  96 => 48,  89 => 44,  84 => 42,  77 => 38,  72 => 36,  61 => 27,  59 => 26,  48 => 18,  31 => 3,  28 => 2,  11 => 1,);
    }
}
