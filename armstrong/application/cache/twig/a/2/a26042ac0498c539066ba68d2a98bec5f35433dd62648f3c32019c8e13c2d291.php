<?php

/* ami/demo/index_.html.twig */
class __TwigTemplate_a26042ac0498c539066ba68d2a98bec5f35433dd62648f3c32019c8e13c2d291 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ami/demo/index_.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Reports Penetration";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/jquery.storageapi.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/plugins/slimscroll/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/js/plugins/metisMenu/metisMenu.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootbox/bootbox.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("res/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("res/js/plugins/parsley/parsley.remote.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\"
            src=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/js/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("res/js/custom_daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, site_url("res/js/date.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/js/generateDataReport_demo.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/js/generateChartReport.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/js/generateHtmlReport.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("res/js/jquery.multiple.select.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("res/js/jquery.sumoselect.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        var BASE_URL = '";
        // line 32
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "',
                CURRENT_URL = '";
        // line 33
        echo twig_escape_filter($this->env, current_url(), "html", null, true);
        echo "';
        function base_url() {
            return BASE_URL;
        }
        function current_url() {
            return CURRENT_URL;
        }
    </script>

";
    }

    // line 44
    public function block_js($context, array $blocks = array())
    {
        // line 45
        echo "    <script>

        function getInternetExplorerVersion()
        // Returns the version of Internet Explorer or a -1
        // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp(\"MSIE ([0-9]{1,}[\\.0-9]{0,})\");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.\$1);
            }
            // 7 - ie7
            // 8 - ie 8
            // 9 -ie 9
            // 10 -ie 10
            //>-1 >10
            return rv;
        }
        function checkVersion() {
            var msg = \"You're not using Internet Explorer.\";
            var ver = getInternetExplorerVersion();

            if (ver > -1) {
                if (ver >= 8.0)
                    msg = \"You're using a recent copy of Internet Explorer.\";
                else
                    msg = \"You should upgrade your copy of Internet Explorer.\";
            }
            alert(msg + ' ' + ver);
        }

        function zoom_report() {
            \$('#ReportsGeneration').toggle();
            \$(\"#reporting-view #iframe_report\").toggleClass('height_zoom');
        }
        function get_sale(value) {
            \$('#armstrong_2_salespersons_id').html('');
            \$.ajax({
                type: 'POST',
                url: base_url() + 'ami/demo/penetration_customers_report/' + value,
                data: {},
                success: function (val) {
                    \$('#armstrong_2_salespersons_id').html(val);
                }
            })

        }

        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        function validateMultiSelect(selector) {
            var options_selected = selector.find('option:selected');
            if (options_selected.length > 0) {
                selector.closest('div').find('.ms-parent').removeClass('has-error');
            } else {
                selector.closest('div').find('.ms-parent').addClass('has-error');
            }
        }

        function resetText(selector, val) {
            var label = selector.closest('.SumoSelect').find('.optWrapper li[data-val=\"' + val + '\"]').text(),
                    captionCont = selector.closest('.SumoSelect').find('.CaptionCont');
            captionCont.find('span').text(label);
        }

        \$(document).ready(function () {
            \$('#InputDateRange').daterangepicker({
                \"showDropdowns\": true,
                \"maxDate\": moment(),
                \"startDate\": \"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "html", null, true);
        echo "\",
                format: 'YYYY-MM-DD'
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD')).trigger('change');
                \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD')).trigger('change');
            });

//            \$(document).on('change', 'select.monthselect', function(){
//                var selected = \$(this).find('option:selected').text();
//                \$(this).find('option:selected').text('<span>' +selected+'</span>');
//            });
            \$('select.multi_select').multipleSelect({placeholder: \"Select here\"});
            \$('.select').SumoSelect();
            \$('div.ms-drop ul').slimScroll();
            var \$val = \$('select[name=\"view_by\"]').val(),
                    channel = \$('.channelWrap'),
                    customers = \$('.gripCustomerWrap'),
                    otm = \$('.otmWrap'),
                    salesLeaders = \$('.slWrap'),
                    salespersons = \$('.spWrap'),
                    rolling = \$('.rolling'),
                    multi_rolling = \$('.multi_rolling');
            switch (\$val) {
                case '0':
                    channel.hide();
                    otm.hide();
                    rolling.hide();
                    salesLeaders.hide();
                    salespersons.hide();
                    customers.show();
                    multi_rolling.show();
                    break;
                case '1':
                    salesLeaders.hide();
                    salespersons.hide();
                    rolling.hide();
                    channel.show();
                    customers.show();
                    otm.show();
                    multi_rolling.show();
                    break;
                case '2':
                    rolling.hide();
                    channel.hide();
                    customers.hide();
                    otm.hide();
                    multi_rolling.hide();
                    salesLeaders.show();
                    salespersons.show();
                    break;
                case '3':
                case '4':
                    salesLeaders.hide();
                    salespersons.hide();
                    multi_rolling.hide();
                    customers.show();
                    otm.show();
                    channel.show();
                    rolling.show();
                    break;
                case '5':
                    rolling.hide();
                    channel.hide();
                    otm.hide();
                    multi_rolling.hide();
                    customers.show();
                    salesLeaders.show();
                    salespersons.show();
                    rolling.show();
                    break;
                case '6':
                    multi_rolling.hide();
                    rolling.show();
                    channel.show();
                    customers.show();
                    otm.show();
                    salesLeaders.show();
                    salespersons.show();
                    rolling.show();
                default:
                    break;
            }

        });

        \$(function () {
            var d = new Date(),
                    cr_month = d.getMonth() + 1,
                    cr_year = d.getFullYear();
            var action = '";
        // line 209
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
            var urlGetReport = '";
        // line 210
        echo twig_escape_filter($this->env, (isset($context["reportEndpoint"]) ? $context["reportEndpoint"] : null), "html", null, true);
        echo "';
            \$('select[name=\"product_type\"]').on('change', function () {
                var \$val = \$(this).val(),
                        products = \$('.topProduct'),
                        otherProducts = \$('.otherProduct');
                if (\$val == 0) {
                    products.show();
                    otherProducts.hide();
                } else {
                    products.hide();
                    otherProducts.show();
                }
            });

            \$('.reset').on('click', function () {
                var appTypeElement = \$('#input-app-type'),
                        monthElement = \$('#input-month'),
                        yearElement = \$('#input-year'),
                        salesLeaderElement = \$('#SalesLeaderSelect'),
                        channelElement = \$('#ChannelSelect'),
                        customerElement = \$('#gripCustomers'),
                        otmElement = \$('#InputOTM'),
                        dateRangeElement = \$('#rolling'),
                        multiDateRangeElement = \$('#multi_rolling');

                appTypeElement.val('PULL');
                monthElement.val(cr_month);
                yearElement.val(cr_year);
                customerElement.val(1);
                dateRangeElement.val(2);
                multiDateRangeElement.multipleSelect(\"setSelects\", [2]);
                salesLeaderElement.multipleSelect(\"checkAll\");
                channelElement.multipleSelect(\"checkAll\");
                otmElement.multipleSelect(\"checkAll\");

                resetText(appTypeElement, 'PULL');
                resetText(monthElement, cr_month);
                resetText(yearElement, cr_year);
                resetText(customerElement, 1);
                resetText(dateRangeElement, 2);
                \$('#input-month, .fetch-sl').trigger('change');
            });

            \$('select[name=\"view_by\"]').on('change', function (e) {
                var \$val = \$(this).val(),
                        channel = \$('.channelWrap'),
                        customers = \$('.gripCustomerWrap'),
                        otm = \$('.otmWrap'),
                        salesLeaders = \$('.slWrap'),
                        salespersons = \$('.spWrap'),
                        rolling = \$('.rolling'),
                        multi_rolling = \$('.multi_rolling');

                switch (\$val) {
                    case '0':
                        channel.hide();
                        otm.hide();
                        rolling.hide();
                        salesLeaders.hide();
                        salespersons.hide();
                        customers.show();
                        multi_rolling.show();
                        break;
                    case '1':
                        salesLeaders.hide();
                        salespersons.hide();
                        rolling.hide();
                        channel.show();
                        customers.show();
                        otm.show();
                        multi_rolling.show();
                        break;
                    case '2':
                        rolling.hide();
                        channel.hide();
                        customers.hide();
                        otm.hide();
                        multi_rolling.hide();
                        salesLeaders.show();
                        salespersons.show();
                        break;
                    case '3':
                    case '4':
                        salesLeaders.hide();
                        salespersons.hide();
                        multi_rolling.hide();
                        customers.show();
                        otm.show();
                        channel.show();
                        rolling.show();
                        break;
                    case '5':
                        rolling.hide();
                        channel.hide();
                        otm.hide();
                        multi_rolling.hide();
                        customers.show();
                        salesLeaders.show();
                        salespersons.show();
                        rolling.show();
                        break;
                    case '6':
                        multi_rolling.hide();
                        rolling.show();
                        channel.show();
                        customers.show();
                        otm.show();
                        salesLeaders.show();
                        salespersons.show();
                        rolling.show();
                    default:
                        break;
                }

            });
            ";
        // line 325
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 326
            echo "            \$('#input-month, #input-year, #input-app-type').on('change', function (e) {
                var \$this = \$(this),
                        month = \$('#input-month').val(),
                        year = \$('#input-year').val(),
                        app_type = \$('#input-app-type').val(),
                        countryID = ";
            // line 331
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
            echo ",
                        productWrap = \$('#top-products-sku'),
                        otherProductWrap = \$('#other-products-sku');
                \$.ajax({
                    url: \"";
            // line 335
            echo twig_escape_filter($this->env, site_url("ami/products/getTopProducts"), "html", null, true);
            echo "\",
                    data: {month: month, year: year, country_id: countryID, app_type: app_type},
                    type: 'POST',
                    dataType: 'json',
                    success: function (res) {
                        var productWrapper = productWrap.closest('div').find('.ms-drop.bottom ul'),
                                otherProductWrapper = otherProductWrap.closest('div').find('.ms-drop.bottom ul');
                        productWrap.html(res.topProducts);
                        otherProductWrap.html(res.otherProducts);

                        productWrapper.remove();
                        otherProductWrapper.remove();
                        productWrap.multipleSelect({placeholder: \"Select here\"});
                        otherProductWrap.multipleSelect({placeholder: \"Select here\"});
                        \$('div.ms-drop ul').slimScroll();
                    }
                });

            });
            //\$('#input-month').trigger('change');
            ";
        }
        // line 356
        echo "
            \$('#multiCountriesList').on('change', function (e) {
                var \$this = \$(this),
                        \$val = \$this.val(),
                        option = \$this.find('option');
                if (\$val == 0) {
                    \$.each(option, function () {
                        var op_val = \$(this).val();
                        \$val.push(op_val);
                    });
                    \$val.splice(0, 2);
                }
                \$this.val(\$val);
            });

            \$('#gripCustomers').on('change', function () {
                var \$val = \$(this).val();
                if (\$val == 1) {
                    var unassigned = '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                    \$('#armstrong_2_salespersons_id').find('option').end().append(unassigned);
                } else {
                    \$('#armstrong_2_salespersons_id').find('option[value=\"unassigned\"]').remove();
                }
            });
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 388
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + toTitleCase(res['value'][key]) + '\">' + toTitleCase(res['value'][key]) + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            \$('.select-change').trigger('change');

            var salesPersonSelect = \$('.SalespersonsSelect').html();
            var salesLeaderSelect = \$('#SalesLeaderSelect').html();

            \$('#SalesLeaderSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        sub_type = optionSelected.data('sub_type'),
                        \$target = \$('select.SalespersonsSelect'),
                        val = \$this.val();

//            var list = '<option value=\"\" selected=\"selected\">ALL</option>';
                var list = '';

                if (\$.inArray('ALL', val) !== -1) {
                    \$target.html(salesPersonSelect).multipleSelect(\"refresh\");
                    return;
                }

                \$target.html(salesPersonSelect).find('option').each(function () {
                    var \$option = \$(this);
                    slId = \$option.data('manager') || 0;

                    if (\$.inArray(slId, val) !== -1) {
                        list += \$option.prop('outerHTML');
                    }
                });
                list += '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                \$target.html(list).multipleSelect(\"refresh\");
                \$('div.SalespersonsSelect ul').remove();
                \$target.multipleSelect(\"refresh\");
//            \$('select.SalespersonsSelect').multipleSelect({placeholder: \"Select here\"});
                var arr_sub_type = [];
                \$.each(optionSelected, function () {
                    var sub_type = \$(this).data('sub_type');
                    arr_sub_type.push(sub_type);
                    if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                        \$target.html(salesPersonSelect).multipleSelect(\"refresh\");

                        return;
                    }
                });
                \$('input[name=\"sub_type\"]').val(arr_sub_type);
            });

            \$('.fetch-sl').on('change', function () {
                var \$appType = \$('#input-app-type'),
                        \$slStatus = \$('#input-sl-status'),
                        salesleader = \$('#SalesLeaderSelect').val() || '',
                        appType = \$appType.find(':selected').val() || '',
                        slStatus = \$slStatus.find(':selected').val() || '',
                        season = \$('select[name=\"season\"]').val(),
                        date_from = \$('input[name=\"from\"]').val(),
                        date_to = \$('input[name=\"to\"]').val(),
                        view_by = \$('select[name=\"view_by\"]').val(),
                        salespersonAll = \$('input[name=\"selectAllsales_leader[]\"]:checked').val();
                var range = (view_by == 0 || view_by == 1) ? 0 : \$('select[name=\"rolling\"]').val();
                if (season) {
                    var split_season = season.split('.');
                    date_from = split_season[0];
                    date_to = split_season[1];
                }

                \$('#armstrong_2_salespersons_id').html('');
                month = \$('#input-month').find(':selected').val();
                year = \$('#input-year').find(':selected').val();
                if (month === undefined) month = 0;
                if (year === undefined) year = 0;
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/salespersons/fetch_salesperson_demo',
                    dataType: 'html',
                    data: {
                        app_type: appType,
                        active: slStatus,
                        salespersons_manager_id: salesleader,
                        sales_all: salespersonAll,
                        date_from: date_from,
                        date_to: date_to,
                        month: month,
                        year: year,
                        range: range
                    },
                    success: function (val) {
                        salesLeaderSelect = val.salesleader;
                        salesPersonSelect = val;

                        \$('#SalesLeaderSelect').html(salesLeaderSelect);
                        \$('div.SalesLeaderSelect ul').remove();
                        \$('#SalesLeaderSelect').multipleSelect('refresh');

                        \$('#armstrong_2_salespersons_id').html(val);
                        \$('div.SalespersonsSelect ul').remove();
                        \$('#armstrong_2_salespersons_id').multipleSelect('refresh');
                    }
                });
            });

            \$('.fetch-sl').trigger('change');

            \$('.btn-clear').click(function (e) {
                e.preventDefault();
                \$(this).closest('.form-group').find(':text').val('');
            });

            var \$iFrame = \$('#iframe_report');
            var \$iFrame_zoom = \$('#iframe_report_zoom');
            // \$('#iframe_report').hide();
            // \$('#zoom_rp').hide();

            \$iFrame.load(function () {
                \$('#reporting-view .loading').html('');
                \$(\"#iframe_report\").show();
                \$(\"#zoom_rp\").show();
            });

            \$('form').on('submit', function (e) {
                if (\$(this).find('input[name=\"from\"]').val() === '' || \$(this).find('input[name=\"to\"]').val() === '')
                    e.preventDefault();
            });

            /*\$('button[type=\"reset\"]').click(function(){
             \$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/report_tfo');
             });*/

            /*\$('select#inputTable').on('change', function(){
             window.location = base_url() + 'management/reports?type=' + \$(this).val().replace('report_', '');;
             //\$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/' + \$(this).val());
             });

             \$('select#inputTable ul li a').on('click', function(){
             var mode =\$(this).attr('href').replace('<?=base_url()?>management/reports?type=', '');
             alert(mode);
             });*/

//            \$('button[type=\"reset\"]').on('click', function(){
//
//                var view_by = \$('select[name=\"view_by\"]');
//                var first_option = view_by.find('option[value=0]').text();
//                var view_text = view_by.closest('div').find('.CaptionCont>span');
//                view_by.val(0);
//                view_text.text(first_option);
//                view_by.trigger('change');
//            });
            \$('button.btn-export-report:not(.disabled)').click(function (e) {
                var otmElement = \$('#InputOTM'),
                        channelElement = \$('#ChannelSelect'),
                        spElement = \$('#armstrong_2_salespersons_id'),
                        slElement = \$('#SalesLeaderSelect'),
                        topProduct = \$('#top-products-sku'),
                        otherProduct = \$('#other-products-sku'),
                        view_by = \$('select[name=\"view_by\"]').val(),
                        month = \$('select[name=\"month\"]').val(),
                        year = \$('select[name=\"year\"]').val(),
                        multi_rolling = \$('#multi_rolling'),
                        type = \$('select[name=\"type\"]').val(),
                        app_type = \$('select[name=\"app_type\"]').val(),
                        from = \$('select[name=\"from\"]').val(),
                        to = \$('select[name=\"to\"]').val(),
                        otm = \$('select[name=\"otm[]\"]').val(),

                        denominator = \$('select[name=\"customers\"]').val(),
                        country_list = \$('select[name=\"country_list\"]').val();

                if (cr_year == year && cr_month < month) {
                    \$('select[name=\"month\"]').closest('div').find('p').addClass('has-error');
                } else {
                    \$('select[name=\"month\"]').closest('div').find('p').removeClass('has-error');
                }
                if (otmElement.length > 0) {
                    validateMultiSelect(otmElement);
                }
                if (channelElement.length > 0) {
                    validateMultiSelect(channelElement);
                }
                if (spElement.length > 0) {
                    validateMultiSelect(spElement);
                }
                if (slElement.length > 0) {
                    validateMultiSelect(slElement);
                }
                if (topProduct.length > 0) {
                    validateMultiSelect(topProduct);
                }
                if (otherProduct.length > 0) {
                    validateMultiSelect(otherProduct);
                }
                if (multi_rolling.length > 0) {
                    validateMultiSelect(multi_rolling);
                }
                // TODO validate multi select
                if (topProduct.closest('div').find('.has-error').length > 0 && otherProduct.closest('div').find('.has-error').length > 0) {
                    e.preventDefault();
                    return false;
                }

                if (topProduct.closest('div').find('.has-error').length > 0 || otherProduct.closest('div').find('.has-error').length > 0) {
                    topProduct.closest('div').find('.ms-parent').removeClass('has-error');
                    otherProduct.closest('div').find('.ms-parent').removeClass('has-error');
                }

                if (cr_year == year && cr_month < month) {
                    e.preventDefault();
                    return false;
                }

                if (view_by == 0 || view_by == 1) {
                    if (multi_rolling.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                if (view_by == 1 || view_by == 3) {
                    if (otmElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 2 || view_by == 5) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 6) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0 || otmElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

            });

            \$('button.btn-view-report:not(.disabled)').click(function (e) {
                e.preventDefault();
                var mode = '";
        // line 650
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
                var spElement = \$('#armstrong_2_salespersons_id');
                var getall = 0;
                var sp = 0;
                var view_by = \$('select[name=\"view_by\"]').val();
                var view_type = \$('select[name=\"view_type\"]').val();
                var slElement = \$('#SalesLeaderSelect');
                var sl = null;
                var sub_type = null;
                var rolling = \$('select[name=\"rolling\"]').val();
                var multi_rolling = \$('#multi_rolling');
                var from = \$('input[name=\"from\"]').val(),
                        to = \$('input[name=\"to\"]').val();
                var promotion_activity = \$('#promotion_name').val();
                if (slElement.length > 0) {

                    sl = slElement.val();
                    if (\$('input[name=\"selectAllsales_leader[]\"]').prop('checked')) {
                        sl = 'ALL';
                    } else {
                        sl = '';
                    }
                    var sl_op_selected = slElement.find('option:selected');
                    if (sl_op_selected.length > 0) {
                        slElement.closest('div').find('.ms-parent').removeClass('has-error');
                        \$.each(sl_op_selected, function () {
                            sub_type = \$(this).data('sub_type');
                            if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                                sl = null;
                                return false;
                            }
                        });
                    } else {
                        slElement.closest('div').find('.ms-parent').addClass('has-error');
                    }
                }
                if (spElement.length > 0) {
                    var sp_val = spElement.val();
                    if (\$('input[name=\"selectAllarmstrong_2_salespersons_id[]\"]').prop('checked')) {
                        sp_val = 'ALL';
                    } else {
                        sp_val = '';
                    }

                    if (sp_val == 'ALL') {
                        if (sl == 'ALL') {
                            getall = 1;
                            sp = 0;
                        } else {
                            getall = 0;
                            sp = spElement.val();
                        }
                    } else {
                        getall = 0;
                        sp = spElement.val();
                    }
                    validateMultiSelect(spElement);

                }
                var otmElement = \$('#InputOTM');
                var otm = '';
                if (otmElement.length > 0) {
                    otm = otmElement.val();
                    validateMultiSelect(otmElement);
                }

                var channelElement = \$('#ChannelSelect');
                var channel = '';
                if (channelElement.length > 0) {
                    channel = channelElement.val();
                    validateMultiSelect(channelElement);
                }

                if (\$('input[name=\"from\"]').length > 0) {
                    var fromdate = \$('input[name=\"from\"]').val() + ' 00:00:00';
                }
                if (\$('input[name=\"to\"]').length > 0) {
                    var todate = \$('input[name=\"to\"]').val() + ' 23:59:59';
                }
                if (\$('select[name=\"number_sku\"]').length > 0) {
                    var number_sku = \$('select[name=\"number_sku\"]').val();
                }

                var topProductElement = \$('#top-products-sku');
                var otherProductElement = \$('#other-products-sku');
                if (topProductElement.length > 0 && otherProductElement.length > 0) {
                    var top_products = topProductElement.val(),
                            other_products = otherProductElement.val();
                    if (!top_products && other_products) {
                        top_products = other_products;
                    }
                    else if (top_products && other_products) {
                        top_products = \$.merge(top_products, other_products);
                    }
                    validateMultiSelect(topProductElement);
                    validateMultiSelect(otherProductElement);
                }

                if (\$('select[name=\"country_list\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list\"]').val();
                    var countryId = country_list;
                } else if (\$('select[name=\"country_list[]\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list[]\"]').val();
                    var countryId = country_list;
//                }
//                if(\$.isArray(countryId)){
//                    countryId = countryId.join('&country=');
                } else {
                    var countryId = '";
        // line 758
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
        echo "';
                }

                if (\$('select[name=\"app_type\"]').length > 0) {
                    var app_type = \$('select[name=\"app_type\"]').val();
                }

                if (\$('select[name=\"active\"]') != null) {
                    var active = \$('select[name=\"active\"]').val();
                }

                if (view_by == 0 || view_by == 1) {
                    var range = multi_rolling.val();
                    validateMultiSelect(multi_rolling);
                } else {
                    var range = \$('select[name=\"rolling\"]').val();
                }

                if (\$('select[name=\"season\"]').length > 0) {
                    var season = \$('select[name=\"season\"]').val();
                    var season_arr = season.split('.');
                    fromdate = season_arr['0'] + ' 00:00:00';
                    todate = season_arr['1'] + ' 23:59:59';
                }

                if (\$('#gripCustomers').length > 0) {
                    var isGripCustomer = \$('#gripCustomers').val();
                }
                // var fromdate = '2014-05-01 00:00:01';
                // var todate = '2014-05-31 23:59:59';

                var elementMoth = \$('#input-month');
                var elementYear = \$('#input-year');
                if (elementMoth.length > 0 && elementYear.length > 0) {
                    var year = elementYear.val();
                    var month = elementMoth.val();
                    var dateStartMonth = year + '-' + month + '-01 00:00:00';
                    var lastday = (new Date(year, month, 0)).getDate();
                    var dateEndMonth = year + '-' + month + '-' + lastday + ' 23:59:59';
                    if (cr_year == year && cr_month < month) {
                        elementMoth.closest('div').find('p').addClass('has-error');
                    } else {
                        elementMoth.closest('div').find('p').removeClass('has-error');
                    }
                }
                var sp_too = (typeof sp !== 'undefined') ? \$('select[name=\"armstrong_2_salespersons_id\"] option[value=\"' + sp + '\"]').html() : \"";
        // line 803
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
        echo " [";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
        echo "]\";


                var isMultiCountry = 0;
                var regional = 0;
                // grip grab filter
                var filters = [];
                if (\$('select[name=\"filter[]\"]').length > 0) {
                    filters = \$('select[name=\"filter[]\"]').val();
                }

                ";
        // line 814
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 815
            echo "
                if (view_by == 0) {
                    channel = -1;
                } else if (view_by == 1) {
                    sp = 0;
                }
                ";
        }
        // line 822
        echo "
                ";
        // line 823
        if (((isset($context["filter"]) ? $context["filter"] : null) == "regional")) {
            // line 824
            echo "                regional = 1;
                getall = 1;
                sp = 0;
                channel = -1;
                otm = 'ALL';
                if (\$('select[name=\"country_list[]\"]').length > 0) {
                    isMultiCountry = 1;
                }
                ";
        }
        // line 833
        echo "
                // TODO validate multi select
                if (topProductElement.closest('div').find('.has-error').length > 0 && otherProductElement.closest('div').find('.has-error').length > 0) {
                    e.preventDefault();
                    return false;
                }

                if (topProductElement.closest('div').find('.has-error').length > 0 || otherProductElement.closest('div').find('.has-error').length > 0) {
                    topProductElement.closest('div').find('.ms-parent').removeClass('has-error');
                    otherProductElement.closest('div').find('.ms-parent').removeClass('has-error');
                }

                if (cr_year == year && cr_month < month) {
                    e.preventDefault();
                    return false;
                }

                if (view_by == 0 || view_by == 1) {
                    if (multi_rolling.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                if (view_by == 1 || view_by == 3) {
                    if (otmElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 2 || view_by == 5) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 6) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0 || otmElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                // set local storage
                var tb_ns = \$.initNamespaceStorage('tb_ns');
                var storage = tb_ns.localStorage;
                storage.set({
                    'urlGetReport': urlGetReport,
                    'sp': sp,
                    'fromdate': fromdate,
                    'todate': todate,
                    'countryId': countryId,
                    'getall': getall,
                    'dateStartMonth': dateStartMonth,
                    'dateEndMonth': dateEndMonth,
                    'month': month,
                    'year': year,
                    'number_sku': number_sku,
                    'app_type': app_type,
                    'active': active,
                    'channel': channel,
                    'otm': otm,
                    'top_products': top_products,
                    'isGripCustomer': isGripCustomer,
                    'view_by': view_by,
                    'range': range,
                    'isMultiCountry': isMultiCountry,
                    'regional': regional,
                    'mode': action,
                    'filters': filters,
                    'promotion_name': promotion_activity,
                    'view_type': view_type
                });

                // set local storage for export
                var export_ns = \$.initNamespaceStorage('export_ns');
                var exportStorage = export_ns.localStorage;
                exportStorage.set({
                    'urlGetReport': urlGetReport,
                    'salespersons': spElement.val(),
                    'salesLeader': slElement.val(),
                    'fromDate': from,
                    'toDate': to,
                    'countryId': country_list,
                    'month': month,
                    'year': year,
                    'app_type': app_type,
                    'channel': channelElement.val(),
                    'otm': otm,
                    'topProducts': topProductElement.val(),
                    'otherProducts': otherProductElement.val(),
                    'denominator': isGripCustomer,
                    'view_by': view_by,
                    'range': rolling,
                    'multiRange': multi_rolling.val(),
                    'mode': action,
                    'filters': filters,
                    'promotion_name': promotion_activity,
                    'view_type': view_type
                });

                var url = base_url() + 'ami/reports/view';
                var win = window.open(url, \"_blank\");
                win.focus();
//                    getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, top_products, isGripCustomer, view_by, range, isMultiCountry, regional);

            });

        });
        \$(window).resize(function () {
            var window_width = \$(this).width();
            if (window_width < 1000) {
                \$('#reporting-view').width('100%');
            } else {
                \$('#reporting-view').width('95%');
            }
        });
    </script>
";
    }

    // line 951
    public function block_css($context, array $blocks = array())
    {
        // line 952
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 953
        echo twig_escape_filter($this->env, site_url("res/css/jqueryui/jquery-ui-1.10.4.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 954
        echo twig_escape_filter($this->env, site_url("res/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 955
        echo twig_escape_filter($this->env, site_url("res/css/ami.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 956
        echo twig_escape_filter($this->env, site_url("res/css/plugins/metisMenu/metisMenu.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 957
        echo twig_escape_filter($this->env, site_url("res/css/plugins/parsley/parsley.css"), "html", null, true);
        echo "\"/>

    <link rel=\"stylesheet\" href=\"";
        // line 959
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 960
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 961
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 962
        echo twig_escape_filter($this->env, site_url("res/css/sumoselect.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 963
        echo twig_escape_filter($this->env, site_url("res/css/multiple-select.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 964
        echo twig_escape_filter($this->env, site_url("res/css/demo.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 965
        echo twig_escape_filter($this->env, site_url("res/css/chart.css"), "html", null, true);
        echo "\">

    <style type=\"text/css\">
        .inputWrap {
            margin-bottom: 7px;
        }

        .SumoSelect > .CaptionCont {
            height: 34px;
        }

        .content-wrap {
            margin: 0 auto;
        }
    </style>
";
    }

    // line 982
    public function block_body($context, array $blocks = array())
    {
        // line 983
        echo "
    ";
        // line 984
        echo form_open(site_url("ami/demo/generate"), array("class" => "form-horizontal"));
        echo "
    <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                        data-target=\"#bs-example-navbar-collapse\" aria-expanded=\"false\">
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <h1 id=\"logo_demo\"><a href=\"";
        // line 994
        echo twig_escape_filter($this->env, site_url("ami/demo"), "html", null, true);
        echo "\"><img
                                src=\"";
        // line 995
        echo twig_escape_filter($this->env, site_url("res/img/logo-u-food.jpg"), "html", null, true);
        echo "\" title=\"home\"></a></h1>
            </div>
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse\">
                <ul class=\"nav navbar-nav\">
                    <li><a href=\"";
        // line 999
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Home</a></li>
                    <li><a href=\"";
        // line 1000
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Account</a></li>
                    <li><a href=\"";
        // line 1001
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Country</a></li>
                    <li><a href=\"";
        // line 1002
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Pending Customers</a></li>
                    <li class=\"active\">
                        <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"
                           aria-haspopup=\"true\">Reports</a>
                        <ul class=\"dropdown-menu\">
                            <li>
                                <a href=\"";
        // line 1008
        echo twig_escape_filter($this->env, site_url("ami/new_reports/penetration_data_report"), "html", null, true);
        echo "\">Penetration Data
                                    Report</a>
                            </li>
                            ";
        // line 1012
        echo "                            ";
        // line 1013
        echo "                            ";
        // line 1014
        echo "                        </ul>
                    </li>
                    <li><a href=\"";
        // line 1016
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">AMI</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"container m-t-md content-wrap\">
        <div class=\"col-lg-12\">
            ";
        // line 1024
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 1025
            echo "                <div class=\"form-group\">
                    <div class=\"viewWrap inputWrap col-sm-10\">
                        ";
            // line 1027
            echo form_label("Report Type", "view_by", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1029
            echo form_dropdown("view_by", array(0 => "SKU", 1 => "Channel", 3 => "Channel/SKU", 5 => "SR/SKU", 6 => "SR/Channel"), null, "class=\"select\"");
            echo "
                        </div>
                    </div>

                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1034
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1036
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>

                    <div class=\"monthWrap inputWrap col-sm-10\">
                        ";
            // line 1041
            echo form_label("Month", "month", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1043
            echo form_dropdown("month", select_month(), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "m"), "method"), "class=\"fetch-sl select\" id=\"input-month\"");
            echo "
                        </div>
                    </div>

                    <div class=\"yearWrap inputWrap col-sm-10\">
                        ";
            // line 1048
            echo form_label("Year", "year", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1050
            echo form_dropdown("year", select_year(2014), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "Y"), "method"), "class=\"select fetch-sl\" id=\"input-year\"");
            echo "
                        </div>
                    </div>

                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1055
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1059
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1060
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1061
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1063
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1068
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1072
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1073
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1074
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1075
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1077
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"topProduct inputWrap col-sm-10\">
                        ";
            // line 1082
            echo form_label("Top SKU", "top_products", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1084
            echo form_dropdown("top_products[]", array(), 0, "class=\"\" id=\"top-products-sku\"  multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                    <div class=\"otherProduct inputWrap col-sm-10\">
                        ";
            // line 1089
            echo form_label("Other SKU", "other_products", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"other_products[]\" class=\"\" id=\"other-products-sku\" multiple=\"multiple\">
                                ";
            // line 1092
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["otherProducts"]) ? $context["otherProducts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["sku"]) {
                // line 1093
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sku"], "sku_number", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sku"], "sku_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sku'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1095
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"channelWrap inputWrap col-sm-10\">
                        ";
            // line 1100
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"channel[]\" class=\"multi_select\" multiple=\"multiple\" id=\"ChannelSelect\">
                                ";
            // line 1103
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryChannels", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 1104
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1105
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1107
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"otmWrap inputWrap col-sm-10\">
                        ";
            // line 1112
            echo form_label("OTM", "otm", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"otm[]\" class=\"multi_select\" id=\"InputOTM\" multiple=\"multiple\">
                                ";
            // line 1115
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "O" => "Other"));
            foreach ($context['_seq'] as $context["otm"] => $context["title"]) {
                // line 1116
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $context["otm"], "html", null, true);
                echo "\" selected>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sl"]) ? $context["sl"] : null), "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['otm'], $context['title'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1118
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"rolling inputWrap col-sm-10\">
                        ";
            // line 1123
            echo form_label("Date Range", "rolling", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1125
            $context["roll_arr"] = array(1 => "Rolling 3 months", 2 => "Rolling 6 months", 3 => "Rolling 12 months", 4 => "Jan - Jun Last Year", 5 => "Jul - Dec Last Year", 6 => "1 year Last Year");
            // line 1126
            echo "                            ";
            echo form_dropdown("rolling", (isset($context["roll_arr"]) ? $context["roll_arr"] : null), 2, "class=\"fetch-sl select\" id=\"rolling\"");
            echo "
                        </div>
                    </div>

                    <div class=\"multi_rolling inputWrap col-sm-10\">
                        ";
            // line 1131
            echo form_label("Date Range", "rolling", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1133
            $context["roll_arr"] = array(1 => "Rolling 3 months", 2 => "Rolling 6 months", 3 => "Rolling 12 months", 4 => "Jan - Jun Last Year", 5 => "Jul - Dec Last Year", 6 => "1 year Last Year");
            // line 1134
            echo "                            ";
            echo form_dropdown("multi_rolling[]", (isset($context["roll_arr"]) ? $context["roll_arr"] : null), 2, "class=\"multi_select\" id=\"multi_rolling\" multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                    <div class=\"gripCustomerWrap inputWrap col-sm-10\">
                        ";
            // line 1139
            echo form_label("Denominator", "grip_customers", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1141
            echo form_dropdown("customers", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "grip_customers", array()), "list", array()), 1, "class=\"select\" id=\"gripCustomers\"");
            echo "
                        </div>
                    </div>
                </div>
            ";
        } elseif ((        // line 1145
(isset($context["action"]) ? $context["action"] : null) == "grip_grab_report")) {
            // line 1146
            echo "                <div class=\"form-group\">
                    <div class=\"fromToWrap inputWrap col-sm-10\">
                        ";
            // line 1148
            echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1150
            $context["default_day"] = (($this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method") . " - ") . $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"));
            // line 1151
            echo "                            ";
            echo form_input(array("name" => "date_time", "value" => (isset($context["default_day"]) ? $context["default_day"] : null), "class" => "form-control", "id" => "InputDateRange", "placeholder" => "Date"));
            // line 1155
            echo "
                        </div>

                        <input type=\"hidden\" id=\"InputStartDate\" class=\"fetch-sl\" name=\"from\"
                               value=\"";
            // line 1159
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" id=\"InputEndDate\" class=\"fetch-sl\" name=\"to\"
                               value=\"";
            // line 1161
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                    </div>

                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1165
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1167
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>

                    <div class=\"channelWrap inputWrap col-sm-10\">
                        ";
            // line 1172
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"channel[]\" class=\"multi_select\" multiple=\"multiple\" id=\"ChannelSelect\">
                                ";
            // line 1175
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryChannels", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 1176
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1177
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1179
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"otmWrap inputWrap col-sm-10\">
                        ";
            // line 1184
            echo form_label("OTM", "otm", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"otm[]\" class=\"multi_select\" id=\"InputOTM\" multiple=\"multiple\">
                                ";
            // line 1187
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "O" => "Other"));
            foreach ($context['_seq'] as $context["otm"] => $context["title"]) {
                // line 1188
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $context["otm"], "html", null, true);
                echo "\" selected>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sl"]) ? $context["sl"] : null), "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['otm'], $context['title'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1190
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1195
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1199
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1200
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1201
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1203
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1208
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1212
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1213
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1214
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1215
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1217
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"filterWrap inputWrap col-sm-10\">
                        ";
            // line 1222
            echo form_label("Report Filter", "filter", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1224
            $context["data_arr"] = array("armstrong2SalespersonsId" => "Salesperson ID", "salespersonsName" => "Salesperson Name", "newGrip" => "Total Grip", "totalLostGrip" => "New Grip", "totalGrip" => "Lost Grip", "newGrab" => "Total Grab", "totalLostGrap" => "New Grab", "totalGrab" => "Lost Grab");
            // line 1225
            echo "                            ";
            echo form_dropdown("filter[]", (isset($context["data_arr"]) ? $context["data_arr"] : null), array(0 => "armstrong2SalespersonsId", 1 => "salespersonsName", 2 => "newGrip"), "class=\"multi_select\" id=\"report_filter\" multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                </div>
            ";
        } elseif ((        // line 1230
(isset($context["action"]) ? $context["action"] : null) == "promotions_activity_report")) {
            // line 1231
            echo "                <div class=\"promotion_name inputWrap col-sm-10\">
                    ";
            // line 1232
            echo form_label("Promotion Name", "promotion_name", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        ";
            // line 1234
            echo form_dropdown("promotion_name", (((isset($context["promotion_activities"]) ? $context["promotion_activities"] : null)) ? ((isset($context["promotion_activities"]) ? $context["promotion_activities"] : null)) : (array())), null, "class=\"select\" id=\"promotion_name\"");
            echo "
                    </div>
                </div>

                <div class=\"slWrap inputWrap col-sm-10\">
                    ";
            // line 1239
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                multiple=\"multiple\">
                            ";
            // line 1243
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1244
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                        selected>";
                // line 1245
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1247
            echo "                        </select>
                    </div>
                </div>

                <div class=\"spWrap inputWrap col-sm-10\">
                    ";
            // line 1252
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                            ";
            // line 1256
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1257
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                        data-manager=\"";
                // line 1258
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                        selected>";
                // line 1259
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1261
            echo "                        </select>
                    </div>
                </div>
            /*";
        } elseif ((        // line 1264
(isset($context["action"]) ? $context["action"] : null) == "last_pantry_check_report")) {
            // line 1265
            echo "                <div class=\"form-group\">
                    <div class=\"fromToWrap inputWrap col-sm-10\">
                        ";
            // line 1267
            echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1269
            $context["default_day"] = (($this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method") . " - ") . $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"));
            // line 1270
            echo "                            ";
            echo form_input(array("name" => "date_time", "value" => (isset($context["default_day"]) ? $context["default_day"] : null), "class" => "form-control", "id" => "InputDateRange", "placeholder" => "Date"));
            // line 1274
            echo "
                        </div>

                        <input type=\"hidden\" id=\"InputStartDate\" class=\"fetch-sl\" name=\"from\"
                               value=\"";
            // line 1278
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" id=\"InputEndDate\" class=\"fetch-sl\" name=\"to\"
                               value=\"";
            // line 1280
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1283
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1287
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1288
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1289
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1291
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1296
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1300
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1301
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1302
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1303
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1305
            echo "                            </select>
                        </div>
                    </div>
                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1309
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1311
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>
                    <div class=\"viewTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1315
            echo form_label("View by", "view_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1317
            $context["view_type"] = array("sku" => "SKU", "region" => "Region", "sl/sr" => "SL/SR", "otm" => "OTM");
            // line 1318
            echo "                            ";
            echo form_dropdown("view_type", (isset($context["view_type"]) ? $context["view_type"] : null), null, "class=\" select\" id=\"input-view-type\"");
            echo "
                        </div>
                    </div>
                </div>*/
            ";
        }
        // line 1323
        echo "            <input type=\"hidden\" name=\"type\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" name=\"sub_type\" value=\"\"/>
            <input type=\"hidden\" name=\"filter\" value=\"";
        // line 1325
        echo twig_escape_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null), "html", null, true);
        echo "\"/>

            <div class=\"row\">
                <div class=\"form-group form-group-demo\">
                    <div class=\"col-sm-12 text-center\">
                        <a class=\"btn btn-default reset\"><i class=\"fa fw fa-times\"></i> Reset</a>
                        <button class=\"btn btn-primary btn-view-report\" ";
        // line 1331
        echo (((!twig_in_filter((isset($context["action"]) ? $context["action"] : null), array(0 => "data_customers")) && $this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_view", array()))) ? ("") : ("disabled"));
        echo ">
                            <i class=\"fa fw fa-eye\"></i> View
                        </button>
                        <button type=\"submit\"
                                class=\"btn btn-primary btn-export-report\" ";
        // line 1335
        echo (($this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_export", array())) ? ("") : ("disabled"));
        echo ">
                            <i class=\"fa fw fa-download\"></i> Export
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    ";
        // line 1345
        echo form_close();
        echo "

    <div class=\"modal fade\" id=\"myModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-body text-center\">
                    <img style=\"width: 200px\" src=\"";
        // line 1351
        echo twig_escape_filter($this->env, site_url("res/img/_AMILOAD.gif"), "html", null, true);
        echo "\"/>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <button id=\"zoom_rp\" class=\"btn btn-mini btn-zoom-report\" onclick=\"zoom_report()\"><i class=\"icon-zoom-in\"></i>&nbsp;Zoom
    </button>
    <div id=\"reporting-view\">
        <!-- reports get generated here -->
        <div class=\"loading\"></div>
        <iframe id=\"iframe_report\"></iframe>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/demo/index_.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1985 => 1351,  1976 => 1345,  1963 => 1335,  1956 => 1331,  1947 => 1325,  1941 => 1323,  1932 => 1318,  1930 => 1317,  1925 => 1315,  1918 => 1311,  1913 => 1309,  1907 => 1305,  1897 => 1303,  1893 => 1302,  1888 => 1301,  1884 => 1300,  1877 => 1296,  1870 => 1291,  1860 => 1289,  1855 => 1288,  1851 => 1287,  1844 => 1283,  1838 => 1280,  1833 => 1278,  1827 => 1274,  1824 => 1270,  1822 => 1269,  1817 => 1267,  1813 => 1265,  1811 => 1264,  1806 => 1261,  1796 => 1259,  1792 => 1258,  1787 => 1257,  1783 => 1256,  1776 => 1252,  1769 => 1247,  1759 => 1245,  1754 => 1244,  1750 => 1243,  1743 => 1239,  1735 => 1234,  1730 => 1232,  1727 => 1231,  1725 => 1230,  1716 => 1225,  1714 => 1224,  1709 => 1222,  1702 => 1217,  1692 => 1215,  1688 => 1214,  1683 => 1213,  1679 => 1212,  1672 => 1208,  1665 => 1203,  1655 => 1201,  1650 => 1200,  1646 => 1199,  1639 => 1195,  1632 => 1190,  1619 => 1188,  1615 => 1187,  1609 => 1184,  1602 => 1179,  1594 => 1177,  1589 => 1176,  1585 => 1175,  1579 => 1172,  1571 => 1167,  1566 => 1165,  1559 => 1161,  1554 => 1159,  1548 => 1155,  1545 => 1151,  1543 => 1150,  1538 => 1148,  1534 => 1146,  1532 => 1145,  1525 => 1141,  1520 => 1139,  1511 => 1134,  1509 => 1133,  1504 => 1131,  1495 => 1126,  1493 => 1125,  1488 => 1123,  1481 => 1118,  1468 => 1116,  1464 => 1115,  1458 => 1112,  1451 => 1107,  1443 => 1105,  1438 => 1104,  1434 => 1103,  1428 => 1100,  1421 => 1095,  1410 => 1093,  1406 => 1092,  1400 => 1089,  1392 => 1084,  1387 => 1082,  1380 => 1077,  1370 => 1075,  1366 => 1074,  1361 => 1073,  1357 => 1072,  1350 => 1068,  1343 => 1063,  1333 => 1061,  1328 => 1060,  1324 => 1059,  1317 => 1055,  1309 => 1050,  1304 => 1048,  1296 => 1043,  1291 => 1041,  1283 => 1036,  1278 => 1034,  1270 => 1029,  1265 => 1027,  1261 => 1025,  1259 => 1024,  1248 => 1016,  1244 => 1014,  1242 => 1013,  1240 => 1012,  1234 => 1008,  1225 => 1002,  1221 => 1001,  1217 => 1000,  1213 => 999,  1206 => 995,  1202 => 994,  1189 => 984,  1186 => 983,  1183 => 982,  1163 => 965,  1159 => 964,  1155 => 963,  1151 => 962,  1147 => 961,  1143 => 960,  1139 => 959,  1134 => 957,  1130 => 956,  1126 => 955,  1122 => 954,  1118 => 953,  1113 => 952,  1110 => 951,  990 => 833,  979 => 824,  977 => 823,  974 => 822,  965 => 815,  963 => 814,  947 => 803,  899 => 758,  788 => 650,  523 => 388,  489 => 356,  465 => 335,  458 => 331,  451 => 326,  449 => 325,  331 => 210,  327 => 209,  235 => 120,  158 => 45,  155 => 44,  141 => 33,  137 => 32,  132 => 30,  128 => 29,  124 => 28,  120 => 27,  116 => 26,  112 => 25,  108 => 24,  104 => 23,  100 => 22,  96 => 21,  92 => 20,  87 => 18,  82 => 16,  78 => 15,  74 => 14,  70 => 13,  66 => 12,  62 => 11,  58 => 10,  54 => 9,  50 => 8,  46 => 7,  41 => 6,  38 => 5,  32 => 3,  11 => 1,);
    }
}
