<?php

/* ami/wholesalers/index.html.twig */
class __TwigTemplate_8d0d630cbe8a2c8bed0099ddb7e6731b227e5d80a73a2a3cb2782287df0620b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/wholesalers/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url(("ami/wholesalers/ajaxData?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData : \"c.armstrong_2_wholesalers_id\", \"sWidth\": \"10%\" },
                    {mData : \"c.name\"},
                    {mData: \"c.armstrong_2_salespersons_id\"},
                    {mData: \"\$.salesperson_name\"},
                    {mData: \"c.approved\"},
//                    {mData: \"c.is_perfect_store\"},
                    {mData : \"c.date_created\", \"bSearchable\": false},
                    {mData : \"c.last_updated\", \"bSearchable\": false},
                    {mData: \"c.updated_name\"},
                ],
                \"fnRowCallback\": function( nRow, aData, iDisplayIndex ) {
                    ";
        // line 31
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "wholesalers"))) {
            // line 32
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"'+ aData['DT_RowId'] +'\"></td>');
                    ";
        }
        // line 34
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">'+ aData['buttons'] +'</td>');
                },
                'order': [[2, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': true,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 59
    public function block_css($context, array $blocks = array())
    {
        // line 60
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 64
    public function block_content($context, array $blocks = array())
    {
        // line 65
        echo "
    ";
        // line 66
        echo form_open(site_url("ami/wholesalers/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 71
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Wholesalers Draft") : ("Wholesalers"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 74
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 76
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/wholesalers/index.html.twig", 76)->display(array_merge($context, array("url" => "ami/wholesalers", "icon" => "fa-shopping-cart", "title" => "Wholesalers", "permission" => "wholesalers", "showImport" => 1)));
        // line 77
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 88
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "active")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"  href=\"";
        echo twig_escape_filter($this->env, site_url("ami/wholesalers?filter=active"), "html", null, true);
        echo "\">Active</a></li>
                <li role=\"presentation\" ";
        // line 89
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"  href=\"";
        echo twig_escape_filter($this->env, site_url("ami/wholesalers?filter=inactive"), "html", null, true);
        echo "\">Inactive</a></li>
                ";
        // line 90
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")))) {
            // line 91
            echo "                    <li role=\"presentation\" ";
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo "><a role=\"menuitem\"
                                                                                                 href=\"";
            // line 92
            echo twig_escape_filter($this->env, site_url("ami/wholesalers?filter=pending"), "html", null, true);
            echo "\">Pending</a>
                    </li>
                ";
        }
        // line 95
        echo "            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 104
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "wholesalers"))) {
            // line 105
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 107
        echo "                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Salesperson Id</th>
                                    <th>Salesperson Name</th>
                                    <th>Status</th>
                                    ";
        // line 113
        echo "                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Updated By</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                            ";
        // line 121
        echo "                                ";
        // line 122
        echo "                                    ";
        // line 123
        echo "                                        ";
        // line 124
        echo "                                    ";
        // line 125
        echo "                                    ";
        // line 126
        echo "                                    ";
        // line 127
        echo "                                    ";
        // line 128
        echo "                                        ";
        // line 129
        echo "                                    ";
        // line 130
        echo "                                    ";
        // line 131
        echo "                                    ";
        // line 132
        echo "                                    ";
        // line 133
        echo "                                    ";
        // line 134
        echo "                                        ";
        // line 135
        echo "                                    ";
        // line 136
        echo "                                ";
        // line 137
        echo "                            ";
        // line 138
        echo "                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 150
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 150,  256 => 138,  254 => 137,  252 => 136,  250 => 135,  248 => 134,  246 => 133,  244 => 132,  242 => 131,  240 => 130,  238 => 129,  236 => 128,  234 => 127,  232 => 126,  230 => 125,  228 => 124,  226 => 123,  224 => 122,  222 => 121,  213 => 113,  206 => 107,  202 => 105,  200 => 104,  189 => 95,  183 => 92,  178 => 91,  176 => 90,  170 => 89,  164 => 88,  151 => 77,  149 => 76,  144 => 74,  138 => 71,  130 => 66,  127 => 65,  124 => 64,  118 => 61,  114 => 60,  111 => 59,  84 => 34,  80 => 32,  78 => 31,  60 => 16,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
