<?php

/* ami/competitor_products/edit.html.twig */
class __TwigTemplate_86ff768d3aca27da75b7f6c7ca972761ec3d7590f5a7d5d80ade8458218acbd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/competitor_products/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/jquery-number/jquery.number.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function initProductAutocomplete(destroy) {
            \$('.product-tokenfield').autocomplete({
                source: \"";
        // line 9
        echo twig_escape_filter($this->env, site_url("ami/products/tokenfield"), "html", null, true);
        echo "\",
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    var \$this = \$(this),
                            \$base = \$this.closest('.sku-base'),
                            \$input = \$base.find('.InputProductId');
                    \$this.val(ui.item.label);
                    \$input.val(ui.item.value);
                }
            }).on('focus', function () {
                this.select();
            });
        }
        \$(function () {
            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        \$clone = \$(this).closest('.sku-base').clone(),
                        nextCounter = \$('.product-tokenfield').length;
                \$clone.find('.hide').removeClass('hide').show();
                if (\$this.hasClass('clone-add')) {
                    \$new = \$clone.clone();

                    \$new.find('input[name], select[name]').each(function () {
                        var \$this = \$(this);
                        \$this.attr('name', \$this.attr('name').replace(/\\[(\\d+)\\]/, '[' + nextCounter + ']'));

                        if (\$this.is('input:text')) {
                            \$this.val('');
                        }
                        else {
                            \$this.val(0);
                        }
                    });
                    \$new.appendTo(\$('#sku-base-block'));
                }
                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.sku-base').remove();
                }
                initProductAutocomplete();
            });
            initProductAutocomplete();
        });
    </script>
";
    }

    // line 57
    public function block_content($context, array $blocks = array())
    {
        // line 58
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 73
        echo form_open(site_url("ami/competitor_products/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Competitor Products</h1>

                <div class=\"text-right\">
                    ";
        // line 81
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/competitor_products/edit.html.twig", 81)->display(array_merge($context, array("url" => "ami/competitor_products", "id" => $this->getAttribute((isset($context["ppostt"]) ? $context["ppostt"] : null), "id", array()), "permission" => "competitor_products")));
        // line 82
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 91
        echo form_label("Competitor Products", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 93
        echo form_input(array("name" => "product_name", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "product_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 97
        echo form_label("quantity", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 99
        echo form_input(array("name" => "quantity", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "quantity", array()), "class" => "form-control", "data-parsley-required" => "false"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 103
        echo form_label("price", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 105
        echo form_input(array("name" => "price", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "price", array()), "class" => "form-control", "data-parsley-required" => "false"));
        echo "
                </div>
            </div>
            ";
        // line 108
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 109
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "products_array", array()));
        foreach ($context['_seq'] as $context["key"] => $context["product"]) {
            // line 110
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 111
            echo form_label("Ufs Skus", "ufs_skus", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 113
            echo form_input(array("name" => "sku_number_", "value" => get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null)), "class" => "form-control product-tokenfield"));
            // line 115
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 116
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"";
            // line 117
            echo twig_escape_filter($this->env, $context["product"], "html", null, true);
            echo "\"
                               data-price=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "price", array()), "html", null, true);
            echo "\"
                               data-pcsprice=\"";
            // line 119
            echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "price", array()) / $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["product"], array(), "array"), "quantity_case", array())), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add hide\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "            ";
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 128
            echo "                ";
            $context["counter"] = twig_length_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "products_array", array()));
            // line 129
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 130
            echo form_label("Ufs Skus", "ufs_skus", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 132
            echo form_input(array("name" => "sku_number_", "value" => null, "class" => "form-control product-tokenfield"));
            // line 134
            echo "
                        <input type=\"hidden\" name=\"products[";
            // line 135
            echo twig_escape_filter($this->env, (isset($context["counter"]) ? $context["counter"] : null), "html", null, true);
            echo "][sku_number]\" class=\"InputProductId\"
                               value=\"\"
                               data-price=\"\"
                               data-pcsprice=\"\" />
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
                <div id=\"sku-base-block\"></div>
            ";
        }
        // line 147
        echo "
            ";
        // line 148
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 152
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/competitor_products/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  263 => 152,  256 => 148,  253 => 147,  238 => 135,  235 => 134,  233 => 132,  228 => 130,  225 => 129,  222 => 128,  219 => 127,  205 => 119,  201 => 118,  197 => 117,  193 => 116,  190 => 115,  188 => 113,  183 => 111,  180 => 110,  175 => 109,  173 => 108,  167 => 105,  162 => 103,  155 => 99,  150 => 97,  143 => 93,  138 => 91,  127 => 82,  125 => 81,  114 => 73,  97 => 58,  94 => 57,  44 => 9,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
