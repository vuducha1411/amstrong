<?php

/* ami/shipment/sap_invoice.html.twig */
class __TwigTemplate_867adb81b97814c0819a81018af8d709f8532fb9d81f7a94f08ce35251e0ae2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/shipment/sap_invoice.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/ami/sap_invoice.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 15
    public function block_css($context, array $blocks = array())
    {
        // line 16
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("res/css/loading.css"), "html", null, true);
        echo "\">
";
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">SAP Invoice</h1>
                <div class=\"text-right\">
                    <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-warning Tooltip\" id=\"ImportPickfiles\" href=\"javascript:;\"
                           data-url=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("ami/shipment/import_sap"), "html", null, true);
        echo "\" title=\"Excel / CSV files only\" data-table = 'sapTable'>
                            <i class=\"fa fw fa-upload\"></i> Import</a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
        <div>
            <ul class=\"breadcrumb\">
                <li><a href=";
        // line 40
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo ">Dashboard</a></li>
                <li><a href=";
        // line 41
        echo twig_escape_filter($this->env, (base_url() . "ami/shipment"), "html", null, true);
        echo ">Shipment</a></li>
                <li><a href=\"#\">SAP Invoice</a></li>
            </ul>
        </div>
    </div>
    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 49
        echo form_label("Year", "year", array("class" => "control-label col-lg-3"));
        echo "
                <div class=\"col-sm-3\">
                    ";
        // line 51
        echo form_dropdown("year", (isset($context["yearList"]) ? $context["yearList"] : null), (isset($context["year"]) ? $context["year"] : null), "id=\"year\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <br/>
            <div class=\"form-group\">
                ";
        // line 56
        echo form_label("Month", "month", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-3\">
                    ";
        // line 58
        echo form_dropdown("month", (isset($context["monthList"]) ? $context["monthList"] : null), (isset($context["month"]) ? $context["month"] : null), "id=\"month\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <br />
            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"generate_tbl\">Generate</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"tagged\">
                    <table class=\"table table-striped table-bordered table-hover\" id=\"sapTable\">
                        <thead>
                        <tr>
                            <th>Distributor ID</th>
                            <th>Distributor Name</th>
                            <th>SKU Number</th>
                            <th>SKU Name</th>
                            <th>Invoice Qty</th>
                            <th>Invoice Value</th>
                            <th>Invoice Date</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/shipment/sap_invoice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 58,  144 => 56,  136 => 51,  131 => 49,  120 => 41,  116 => 40,  103 => 30,  94 => 23,  91 => 22,  85 => 19,  81 => 18,  77 => 17,  72 => 16,  69 => 15,  63 => 12,  59 => 11,  55 => 10,  51 => 9,  47 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
