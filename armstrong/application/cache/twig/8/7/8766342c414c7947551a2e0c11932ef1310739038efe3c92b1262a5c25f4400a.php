<?php

/* ami/cron_monitoring/index.html.twig */
class __TwigTemplate_8766342c414c7947551a2e0c11932ef1310739038efe3c92b1262a5c25f4400a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/cron_monitoring/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/js/cron_monitoring.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 17
    public function block_css($context, array $blocks = array())
    {
        // line 18
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/css/loading.css"), "html", null, true);
        echo "\">
";
    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        // line 25
        echo "    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Cronjob Monitoring</h1>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class=\"row m-t-md\">
    <div class=\"col-lg-12\">

        <div class=\"panel panel-default m-t\">
            <div class=\"panel-body tab-content\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"cronDataTable\">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cronjob Name</th>
                            <th>Description</th>
                            <th>Schedule</th>
                            <th>Last Run</th>
                        </tr>
                    </thead>
                </table>
               
               
              
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/cron_monitoring/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  92 => 24,  86 => 21,  82 => 20,  78 => 19,  73 => 18,  70 => 17,  64 => 13,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
