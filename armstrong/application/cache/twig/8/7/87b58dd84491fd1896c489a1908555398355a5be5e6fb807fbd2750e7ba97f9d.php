<?php

/* ami/potential_customers/index.html.twig */
class __TwigTemplate_87b58dd84491fd1896c489a1908555398355a5be5e6fb807fbd2750e7ba97f9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/potential_customers/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            i = 0;
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 17
        echo twig_escape_filter($this->env, site_url("ami/potential_customers/ajaxData"), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"c.id\", render: function (data, type, row) {
                        if (type === 'display' || type === 'filter') {
                            return i++;
                        }
                        return data;
                    }},
                    {mData: \"c.armstrong_1_customers_id\"},
                    {mData: \"c.armstrong_2_customers_name\"},
                    {mData: \"\$.salesperson_name\"},
                    {mData: \"c.active\"},
//                    {mData: \"c.otm_new\"},
//                    {mData: \"c.channel\"},
                    {mData: \"c.date_created\", \"bSearchable\": false},
                    {mData: \"c.last_updated\", \"bSearchable\": false}
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    ";
        // line 37
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "customer"))) {
            // line 38
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    ";
        }
        // line 40
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
//                    ,{
//                        \"targets\": [3],
//                        \"bSearchable\": false
//                    }
//                    ,{
//                        \"targets\": [4],
//                        \"bSearchable\": false
//                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 73
    public function block_css($context, array $blocks = array())
    {
        // line 74
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 75
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 78
    public function block_content($context, array $blocks = array())
    {
        // line 79
        echo "
    ";
        // line 80
        echo form_open(site_url("ami/potential_customers/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 85
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Potential Customers Draft") : ("Potential Customers"));
        echo "</h1>

                <div class=\"text-right\">
\t\t\t\t\t<span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
\t\t\t\t\t\t";
        // line 89
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
\t\t\t\t\t</span>
                    ";
        // line 91
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/potential_customers/index.html.twig", 91)->display(array_merge($context, array("url" => "ami/potential_customers", "icon" => "fa-user", "title" => "Customers", "permission" => "potential_customers", "showImport" => 1, "getTemplateimport" => "res/up/potential_customers.xlsx")));
        // line 92
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 101
        if ((isset($context["message"]) ? $context["message"] : null)) {
            // line 102
            echo "                <div class=\"alert alert-success alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <strong>";
            // line 105
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "</strong>
                </div>
            ";
        }
        // line 108
        echo "
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 116
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "potential_customers"))) {
            // line 117
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 121
        echo "                                <th>ID</th>
                                <th>Armstrong 1 Customer Id</th>
                                <th>Name</th>
                                <th>Salesperson Name</th>
                                <th>Active</th>
                                ";
        // line 127
        echo "                                ";
        // line 128
        echo "                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 145
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/potential_customers/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 145,  219 => 128,  217 => 127,  210 => 121,  204 => 117,  202 => 116,  192 => 108,  186 => 105,  181 => 102,  179 => 101,  168 => 92,  166 => 91,  161 => 89,  154 => 85,  146 => 80,  143 => 79,  140 => 78,  134 => 75,  129 => 74,  126 => 73,  91 => 40,  87 => 38,  85 => 37,  62 => 17,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
