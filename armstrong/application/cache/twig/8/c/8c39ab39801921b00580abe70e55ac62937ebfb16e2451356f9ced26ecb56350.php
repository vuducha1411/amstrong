<?php

/* ami/organization/index.html.twig */
class __TwigTemplate_8c39ab39801921b00580abe70e55ac62937ebfb16e2451356f9ced26ecb56350 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/organization/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/getorgchart/getorgchart.js"), "html", null, true);
        echo "\"></script>


<script>
\$(document).ready(function() {
    // var readUrl = \"http://getorgchart.com/JsonInitialization/Read?callback=?\";
    var readUrl = \"";
        // line 11
        echo twig_escape_filter($this->env, site_url("ami/organization/build"), "html", null, true);
        echo "\"
    // var readUrl = \"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/json/build.json"), "html", null, true);
        echo "\"
     
    \$.getJSON(readUrl, function(people) {     
        // console.log(people) ;
        \$('#people').getOrgChart({
            theme: \"vivian\",
            color: \"lightblue\",
            primaryColumns: [\"Name\", \"Title\"],
            imageColumn: \"Image\",
            linkType: \"M\",
            editable: false,  
            scale: 0.5,             
            dataSource: people,
            clickEvent: function(sender, args) {
                return false;
            }
        });

        \$.each(people, function(i, item) {
            \$(\"#people\").getOrgChart(\"setBoxColor\", item.id, item.Color);
        });
    });
});
</script>
";
    }

    // line 38
    public function block_css($context, array $blocks = array())
    {
        // line 39
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url("res/css/plugins/getorgchart/getorgchart.css"), "html", null, true);
        echo "\">
";
    }

    // line 43
    public function block_content($context, array $blocks = array())
    {
        // line 44
        echo "    ";
        // line 45
        echo "    <div id=\"people\"></div>
";
    }

    public function getTemplateName()
    {
        return "ami/organization/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 45,  95 => 44,  92 => 43,  86 => 40,  82 => 39,  79 => 38,  50 => 12,  46 => 11,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
