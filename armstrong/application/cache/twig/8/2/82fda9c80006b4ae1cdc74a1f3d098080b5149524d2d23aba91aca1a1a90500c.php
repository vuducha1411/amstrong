<?php

/* ami/components/table_btn_head.html.twig */
class __TwigTemplate_82fda9c80006b4ae1cdc74a1f3d098080b5149524d2d23aba91aca1a1a90500c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("view", (isset($context["permission"]) ? $context["permission"] : null)))) {
            // line 2
            echo "    <div role=\"group\" class=\"btn-group btn-header-toolbar\">
        ";
            // line 4
            echo "            ";
            // line 5
            echo "                        ";
            // line 6
            echo "        ";
            // line 7
            echo "        ";
            if (((isset($context["transfer_btn"]) ? $context["transfer_btn"] : null) == 1)) {
                // line 8
                echo "            <a class=\"btn btn-sm btn-primary\" href=\"";
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/transfer_view")), "html", null, true);
                echo "\"><i
                        class=\"fa fa-exchange\"></i> Transfer</a>
        ";
            } elseif ((            // line 10
(isset($context["transfer_btn"]) ? $context["transfer_btn"] : null) == 2)) {
                // line 11
                echo "            ";
                if (((isset($context["permission"]) ? $context["permission"] : null) == "customer")) {
                    // line 12
                    echo "            <a class=\"btn btn-sm btn-primary\" href=\"";
                    echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/transfer")), "html", null, true);
                    echo "\"><i
                        class=\"fa fa-exchange\"></i> Transfer</a>
            ";
                } else {
                    // line 15
                    echo "            <a class=\"btn btn-sm btn-primary\" href=\"";
                    echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/transfer_customers")), "html", null, true);
                    echo "\"><i
                        class=\"fa fa-exchange\"></i> Transfer</a>
            ";
                }
                // line 18
                echo "        ";
            }
            // line 19
            echo "        ";
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null)))) {
                // line 20
                echo "            <a class=\"btn btn-sm btn-success\" href=\"";
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/add")), "html", null, true);
                echo "\"><i class=\"fa fw fa-plus\"></i> Add</a>
        ";
            }
            // line 22
            echo "        ";
            if ((isset($context["draft"]) ? $context["draft"] : null)) {
                // line 23
                echo "            <a class=\"btn btn-sm btn-default\" href=\"";
                echo twig_escape_filter($this->env, site_url((isset($context["url"]) ? $context["url"] : null)), "html", null, true);
                echo "\"><i class=\"fa fw ";
                echo twig_escape_filter($this->env, (isset($context["icon"]) ? $context["icon"] : null), "html", null, true);
                echo "\"></i> ";
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "
            </a>
        ";
            } else {
                // line 26
                echo "            ";
                // line 27
                echo "        ";
            }
            // line 28
            echo "
        ";
            // line 29
            if ((isset($context["getTemplateimport"]) ? $context["getTemplateimport"] : null)) {
                // line 30
                echo "        <a class=\"btn btn-sm btn-primary\" id=\"getTemplateimport\" href=\"";
                echo twig_escape_filter($this->env, (site_url() . (isset($context["getTemplateimport"]) ? $context["getTemplateimport"] : null)), "html", null, true);
                echo "\"
           target=\"_blank\" title=\"Get template import\"><i
                    class=\"fa fw fa-download\"></i>Get Template</a>
        ";
            }
            // line 34
            echo "
        ";
            // line 35
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", (isset($context["permission"]) ? $context["permission"] : null))) && (isset($context["showImport"]) ? $context["showImport"] : null))) {
                // line 36
                echo "            <a class=\"btn btn-sm btn-warning Tooltip\" id=\"ImportPickfiles\" href=\"javascript:;\"
               data-url=\"";
                // line 37
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/import")), "html", null, true);
                echo "\" title=\"Max records 10000 (csv, xls, xlsx)\">
                <i class=\"fa fw fa-upload\"></i> Import</a>
        ";
            }
            // line 40
            echo "
        ";
            // line 41
            if ((isset($context["export"]) ? $context["export"] : null)) {
                // line 42
                echo "            <div class=\"btn-group\" role=\"group\">
                <button class=\"btn btn-sm btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><i
                            class=\"fa fw fa-file-excel-o\"></i> Export <span class=\"caret\"></span></button>
                <ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\">
                    <li><a href=\"";
                // line 46
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/export/csv")), "html", null, true);
                echo "\">CSV</a></li>
                    <li><a href=\"";
                // line 47
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/export/xlsx")), "html", null, true);
                echo "\">XLSX</a></li>
                    <li><a href=\"";
                // line 48
                echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/export/xls")), "html", null, true);
                echo "\">XLS</a></li>
                </ul>
            </div>
        ";
            }
            // line 52
            echo "    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/components/table_btn_head.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 52,  135 => 48,  131 => 47,  127 => 46,  121 => 42,  119 => 41,  116 => 40,  110 => 37,  107 => 36,  105 => 35,  102 => 34,  94 => 30,  92 => 29,  89 => 28,  86 => 27,  84 => 26,  73 => 23,  70 => 22,  64 => 20,  61 => 19,  58 => 18,  51 => 15,  44 => 12,  41 => 11,  39 => 10,  33 => 8,  30 => 7,  28 => 6,  26 => 5,  24 => 4,  21 => 2,  19 => 1,);
    }
}
