<?php

/* ami/shipment/index.html.twig */
class __TwigTemplate_80fab6f3429c12843b6788fbeead6d581a6827e60eebf0002544f9f9f91ea18d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/shipment/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 12
    public function block_css($context, array $blocks = array())
    {
        // line 13
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("res/css/loading.css"), "html", null, true);
        echo "\">
";
    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        // line 20
        echo "    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Shipment Plan</h1>
                ";
        // line 32
        echo "                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-primary\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-truck fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 50
        echo "                                        <div>Shipment</div>
                                        <div>Plan</div>
                                    </div>
                                </div>
                            </div>
                            <a href=";
        // line 55
        echo twig_escape_filter($this->env, (base_url() . "ami/shipment/shipment_plan"), "html", null, true);
        echo ">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-green\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-files-o fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 74
        echo "                                        <div>SAP</div>
                                        <div>Invoice</div>
                                    </div>
                                </div>
                            </div>
                            <a href=";
        // line 79
        echo twig_escape_filter($this->env, (base_url() . "ami/shipment/sap_invoice"), "html", null, true);
        echo ">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-lg-3 col-md-6\">
                        <div class=\"panel panel-primary\">
                            <div class=\"panel-heading\">
                                <div class=\"row\">
                                    <div class=\"col-xs-3\">
                                        <i class=\"fa fa-file fa-5x\"></i>
                                    </div>
                                    <div class=\"col-xs-9 text-right\">
                                        ";
        // line 98
        echo "                                        <div>GIT</div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                            <a href=";
        // line 103
        echo twig_escape_filter($this->env, (base_url() . "ami/shipment/git"), "html", null, true);
        echo ">
                                <div class=\"panel-footer\">
                                    <span class=\"pull-left\">View Details</span>
                                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>

                                    <div class=\"clearfix\"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
    }

    public function getTemplateName()
    {
        return "ami/shipment/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 103,  163 => 98,  142 => 79,  135 => 74,  114 => 55,  107 => 50,  88 => 32,  82 => 20,  79 => 19,  73 => 16,  69 => 15,  65 => 14,  60 => 13,  57 => 12,  51 => 9,  47 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
