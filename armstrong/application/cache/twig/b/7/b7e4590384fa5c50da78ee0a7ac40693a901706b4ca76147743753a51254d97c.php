<?php

/* ami/webshop/view.html.twig */
class __TwigTemplate_b7e4590384fa5c50da78ee0a7ac40693a901706b4ca76147743753a51254d97c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/webshop/view.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/webshop.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 16
    public function block_css($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        echo form_open_multipart(site_url("ami/webshop/update_untag"), array("class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Tag Webshop Order</h1>
                <div class=\"text-right\">
                    ";
        // line 28
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/webshop/view.html.twig", 28)->display(array_merge($context, array("url" => "ami/webshop", "id" => (isset($context["order_id"]) ? $context["order_id"] : null), "permission" => "webshop")));
        // line 29
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
            <div>
                <ul class=\"breadcrumb\">
                    <li><a href=";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo ">Dashboard</a></li>
                    <li><a href=";
        // line 35
        echo twig_escape_filter($this->env, site_url("ami/webshop"), "html", null, true);
        echo ">Webshop</a></li>
                    <li><a href=\"#\">Tag Order</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 46
        echo form_label("Salesperson ID", "salesperson", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 48
        echo form_dropdown("", (isset($context["salespersons"]) ? $context["salespersons"] : null), (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), (("
                            class=\"form-control\"
                            data-parsley-required=\"true\"
                            id=\"armstrong_2_salesperson\"
                            data-url=\"" . site_url("ami/salespersons/fetch_customer")) . "\""));
        // line 52
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_salespersons_id\" id=\"armstrong_2_salespersons_id\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), "html", null, true);
        echo "\">
                        <a onclick=\"showFields('armstrong')\" id=\"armstrong_fields\">Show Armstrong Customer Fields</a>
                        <a onclick=\"showFields('potential')\" id=\"potential_fields\">Show Potential Customer Fields</a>
                    </div>
                </div>
            </div>
            <div id=\"armstrong\">
                <div class=\"col-md-12 form-group\" id=\"customer\">
                    <div class=\"form-group\">
                        ";
        // line 62
        echo form_label("Customer ID", "customer", array("class" => "control-label col-md-3"));
        echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
        // line 64
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_customer"));
        echo "
                            <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"armstrong_2_customers_id\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), "html", null, true);
        echo "\">
                            <div id=\"addPotential\"></div>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
        // line 72
        echo form_label("Distributor ID", "distributor", array("class" => "control-label col-md-3"));
        echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
        // line 74
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_distributor"));
        // line 76
        echo "
                            <input type=\"hidden\" name=\"armstrong_2_distributors_id\" id=\"armstrong_2_distributors_id\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), "html", null, true);
        echo "\">
                        </div>
                    </div>
                </div>
                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
        // line 83
        echo form_label("Wholesaler ID", "wholesaler", array("class" => "control-label col-md-3"));
        echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
        // line 85
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), (isset($context["armstrong_2_wholesalers_id"]) ? $context["armstrong_2_wholesalers_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_wholesaler"));
        // line 87
        echo "
                            <input type=\"hidden\" name=\"armstrong_2_wholesalers_id\" id=\"armstrong_2_wholesalers_id\" value=\"";
        // line 88
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_wholesalers_id"]) ? $context["armstrong_2_wholesalers_id"] : null), "html", null, true);
        echo "\">
                        </div>
                    </div>
                </div>
            </div>
            <div id=\"potential\">
                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
        // line 96
        echo form_label("Potential Customer ID", "customer", array("class" => "control-label col-md-3"));
        echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
        // line 98
        echo form_input(array("name" => "potential[armstrong_1_customers_id]", "value" => (isset($context["armstrong_1_customers_id"]) ? $context["armstrong_1_customers_id"] : null), "class" => "form-control", "id" => "potential_id"));
        // line 99
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
        // line 105
        echo form_label("Potential Customer Name", "customer", array("class" => "control-label col-md-3"));
        echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
        // line 107
        echo form_input(array("name" => "potential[armstrong_2_customers_name]", "value" => (isset($context["armstrong_2_customers_name"]) ? $context["armstrong_2_customers_name"] : null), "class" => "form-control", "id" => "potential_name"));
        // line 108
        echo "
                        </div>
                    </div>
                </div>
            </div>
            ";
        // line 113
        if (((isset($context["country_id"]) ? $context["country_id"] : null) == 2)) {
            // line 114
            echo "                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
            // line 116
            echo form_label("Call Record", "call_record", array("class" => "control-label col-md-3"));
            echo "
                        <div class=\"col-md-6 no-parsley\">";
            // line 117
            echo form_input(array("name" => "armstrong_2_call_record", "value" => "", "class" => "form-control", "id" => "armstrong_2_call_record"));
            // line 119
            echo "</div>
                        <input type=\"hidden\" name=\"armstrong_2_call_records_id\" id=\"armstrong_2_call_records_id\" value=\"";
            // line 120
            echo twig_escape_filter($this->env, (isset($context["armstrong_2_call_records_id"]) ? $context["armstrong_2_call_records_id"] : null), "html", null, true);
            echo "\">
                    </div>
                </div>
            ";
        }
        // line 124
        echo "            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 126
        echo form_label("Webshop Order ID", "order_id", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 127
        echo form_input(array("name" => "webshop_order_id", "value" => (isset($context["order_id"]) ? $context["order_id"] : null), "class" => "form-control", "id" => "webshop_order_id"));
        echo "</div>
                </div>
            </div>
            ";
        // line 130
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["product"]) {
            // line 131
            echo "                <div class=\"col-md-12 form-group sku-base\">
                    <div class=\"form-group\">
                        ";
            // line 133
            echo form_label("Product", "product", array("class" => "control-label col-md-3"));
            echo "
                        <div class=\"col-md-6\">
                            ";
            // line 135
            echo form_input(array("name" => "", "value" => (($this->getAttribute($context["product"], "product_number", array()) . " - ") . $this->getAttribute($context["product"], "product_name", array())), "class" => "form-control", "id" => "product"));
            echo "
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            <b>Webshop : &nbsp;</b>
                            ";
            // line 139
            echo form_hidden((("webshop[" . $context["key"]) . "][sku_number]"), $this->getAttribute($context["product"], "product_number", array()));
            echo "
                            Quantity
                            ";
            // line 141
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_quantity]"), "value" => $this->getAttribute($context["product"], "product_quantity", array()), "class" => "form-control sub-input qty-case", "id" => "product_quantity", "readonly" => "readonly"));
            echo " &nbsp;
                            ";
            // line 142
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_package_type]"), "value" => $this->getAttribute($context["product"], "product_package_type", array()), "class" => "form-control sub-input subtotal", "id" => "product_package_type", "readonly" => "readonly"));
            echo " &nbsp;
                            Price ";
            // line 143
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][product_price]"), "value" => $this->getAttribute($context["product"], "product_price", array()), "class" => "form-control sub-input subtotal", "id" => "product_price", "readonly" => "readonly"));
            echo " &nbsp;
                            Sub Total ";
            // line 144
            echo form_input(array("name" => (("webshop[" . $context["key"]) . "][total_price]"), "value" => $this->getAttribute($context["product"], "product_total_price", array()), "class" => "form-control sub-input subtotal", "id" => "total_price", "readonly" => "readonly"));
            echo "
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            <b>Armstrong : &nbsp;</b>
                            ";
            // line 148
            echo form_hidden((("armstrong[" . $context["key"]) . "][id]"), $this->getAttribute($context["product"], "id", array()));
            echo "
                            ";
            // line 149
            echo form_hidden((("armstrong[" . $context["key"]) . "][sku_number]"), $this->getAttribute($context["product"], "product_number", array()));
            echo "
                            ";
            // line 151
            echo "                            Quantity Per Case ";
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][quantity_case]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_per_case", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_qty_per_case", "readonly" => "readonly"));
            echo "
                            Price ";
            // line 152
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][price]"), "value" => $this->getAttribute($context["product"], "armstrong_product_price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_product_price", "readonly" => "readonly"));
            echo " &nbsp;
                        </div>
                        <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                            Quantity
                            ";
            // line 156
            if (($this->getAttribute($context["product"], "product_package_type", array()) == "Case")) {
                // line 157
                echo "                                ";
                echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][qty_case]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_case", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_qty_case", "readonly" => "readonly"));
                echo "
                            ";
            } else {
                // line 159
                echo "                                ";
                echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][qty_piece]"), "value" => $this->getAttribute($context["product"], "armstrong_qty_pcs", array()), "class" => "form-control sub-input qty-case", "id" => "armstrong_qty_pcs", "readonly" => "readonly"));
                echo "
                            ";
            }
            // line 161
            echo "                            ";
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][package_type]"), "value" => $this->getAttribute($context["product"], "product_package_type", array()), "class" => "form-control sub-input subtotal", "id" => "product_package_type", "readonly" => "readonly"));
            echo " &nbsp;
                            Sub Total ";
            // line 162
            echo form_input(array("name" => (("armstrong[" . $context["key"]) . "][total_price]"), "value" => $this->getAttribute($context["product"], "armstrong_total_price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_total_price", "readonly" => "readonly"));
            echo "
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 169
        echo form_label("Total Order", "total_order", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 170
        echo form_input(array("name" => "total", "value" => (isset($context["total_order"]) ? $context["total_order"] : null), "class" => "form-control", "id" => "total"));
        echo "</div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 175
        echo form_label("Order Date", "order_date", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">";
        // line 176
        echo form_input(array("name" => "order_date", "value" => (isset($context["order_date"]) ? $context["order_date"] : null), "class" => "form-control", "id" => "order_date"));
        echo "</div>
                </div>
            </div>
            ";
        // line 179
        echo form_close();
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/webshop/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  390 => 179,  384 => 176,  380 => 175,  372 => 170,  368 => 169,  364 => 167,  353 => 162,  348 => 161,  342 => 159,  336 => 157,  334 => 156,  327 => 152,  322 => 151,  318 => 149,  314 => 148,  307 => 144,  303 => 143,  299 => 142,  295 => 141,  290 => 139,  283 => 135,  278 => 133,  274 => 131,  270 => 130,  264 => 127,  260 => 126,  256 => 124,  249 => 120,  246 => 119,  244 => 117,  240 => 116,  236 => 114,  234 => 113,  227 => 108,  225 => 107,  220 => 105,  212 => 99,  210 => 98,  205 => 96,  194 => 88,  191 => 87,  189 => 85,  184 => 83,  175 => 77,  172 => 76,  170 => 74,  165 => 72,  155 => 65,  151 => 64,  146 => 62,  134 => 53,  131 => 52,  125 => 48,  120 => 46,  106 => 35,  102 => 34,  95 => 29,  93 => 28,  83 => 22,  80 => 21,  74 => 18,  69 => 17,  66 => 16,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
