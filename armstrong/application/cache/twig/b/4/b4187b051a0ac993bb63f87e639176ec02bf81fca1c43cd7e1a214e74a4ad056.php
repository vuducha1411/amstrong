<?php

/* ami/admin_management/preview.html.twig */
class __TwigTemplate_b4187b051a0ac993bb63f87e639176ec02bf81fca1c43cd7e1a214e74a4ad056 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "first_name", array()) . " ") . $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "last_name", array())), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                        <div class=\"m-l-xl\">
                            <h4 class=\"list-group-item-heading text-muted\">Email</h4>
                            <p class=\"list-group-item-text\"><a href=\"mailto:";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "email", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "email", array()), "html", null, true);
        echo "</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 23
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "admin_management"))) {
            // line 24
            echo "                ";
            echo html_btn(site_url(("ami/admin_management/edit/" . $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 26
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/admin_management/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 26,  55 => 24,  53 => 23,  40 => 15,  25 => 5,  19 => 1,);
    }
}
