<?php

/* ami/global_ssd/masterViewSSD.html.twig */
class __TwigTemplate_b188b963fb08252b49f3ee1b190560002d1ddc82d36fcc17f1808faf8ec3521e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/masterViewSSD.html.twig", 6);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_js($context, array $blocks = array())
    {
        // line 9
        echo "\t";
        $this->displayParentBlock("js", $context, $blocks);
        echo " 
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js\"></script>
\t<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/functions.js"), "html", null, true);
        echo "\"></script>

    <script>
    \t\$(document).ready(function(){
    \t\tvar distributorsDropDownHeight = \$(\"#input-distributors\").height();
    \t\tvar dropDownMinHeight = 200;
    \t// \t\$.ajax({
    \t// \t\turl : base_url() + 'ami/global_ssd/distributorsList',
    \t// \t\ttype : 'GET',
    \t// \t\tdataType : 'json',
    \t// \t\tsuccess : function(data) {
\t\t\t\t\t// data.forEach(function(val){
\t\t\t\t\t// \tdistributorsDropDownHeight += 20;
\t\t\t\t\t// \t\$(\"#input-distributors\").append('<option value=\"'+val.armstrong_2_distributors_id+'\">'+val.name+'</option>')
\t\t\t\t\t// });
\t\t\t\t\t// \$(\"#input-distributors\").height(distributorsDropDownHeight > dropDownMinHeight ? dropDownMinHeight : distributorsDropDownHeight);
    \t// \t\t}
    \t// \t})
    \t});
    </script>
";
    }

    // line 34
    public function block_css($context, array $blocks = array())
    {
        // line 35
        echo "\t";
        $this->displayParentBlock("css", $context, $blocks);
        echo "

    <link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">

\t<style>
        #mainContent {
            margin-top: 20px;
        }
\t</style>
";
    }

    // line 46
    public function block_content($context, array $blocks = array())
    {
        // line 47
        echo "
\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1>";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["page_title"]) ? $context["page_title"] : null), "html", null, true);
        echo "</h1>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div id=\"mainContent\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-6\"></div>
\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t<label for=\"\">Actions</label>
\t\t\t\t\t\t\t\t<ul class=\"list-group en-hover\">
\t\t\t\t\t\t\t\t\t";
        // line 68
        echo "\t\t\t\t\t\t\t\t\t<li 
\t\t\t\t\t\t\t\t\t\tclass=\"list-group-item export-table\" 
\t\t\t\t\t\t\t\t\t\tdata-target=\"#tbl_global_ssd\" 
\t\t\t\t\t\t\t\t\t\tdata-sheetname=\"SSD Export\" 
\t\t\t\t\t\t\t\t\t\tdata-filename=\"SSD_EXPORT\"
\t\t\t\t\t\t\t\t\t> 
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-download\"></i> 
\t\t\t\t\t\t\t\t\t\tExport 
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"inputSearch\">Search Table</label>
\t\t\t\t\t\t\t\t\t\t\t<input 
\t\t\t\t\t\t\t\t\t\t\t\tdata-target=\"#tbl_global_ssd\"\t
\t\t\t\t\t\t\t\t\t\t\t\tid=\"inputSearch\" 
\t\t\t\t\t\t\t\t\t\t\t\ttype=\"text\" 
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"form-control search-table\" 
\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Enter keyword here\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"scroll-x\">
\t\t\t\t\t<table id=\"tbl_global_ssd\" class=\"table-ssd table table-bordered table-condensed table-striped\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>ID</th>
\t\t\t\t\t\t\t\t<th>DISTRIBUTOR</th>
\t\t\t\t\t\t\t\t<th>DT_CUSTOMER_CODE</th>
\t\t\t\t\t\t\t\t<th>DT_CUSTOMER_NAME</th>
\t\t\t\t\t\t\t\t<th>CUSTOMERS</th>
\t\t\t\t\t\t\t\t<th>GROUPING_CUSTOMER</th>
\t\t\t\t\t\t\t\t<th>TEAM</th>
\t\t\t\t\t\t\t\t<th>SALESMAN</th>
\t\t\t\t\t\t\t\t<th>CHANNELS</th>
\t\t\t\t\t\t\t\t<th>SALESMAN_CODE</th>
\t\t\t\t\t\t\t\t<th>WHOLESALE_SUB_CHANNEL</th>
\t\t\t\t\t\t\t\t<th>GOLD_PLUS</th>
\t\t\t\t\t\t\t\t<th>CPU</th>
\t\t\t\t\t\t\t\t<th>OEM</th>
\t\t\t\t\t\t\t\t<th>DT_PRODUCT_CODE</th>
\t\t\t\t\t\t\t\t<th>DT_PRODUCT_NAME</th>
\t\t\t\t\t\t\t\t<th>PRODUCT</th>
\t\t\t\t\t\t\t\t<th>PRODUCT_CODE</th>
\t\t\t\t\t\t\t\t<th>PRODUCT_GROUPING</th>
\t\t\t\t\t\t\t\t<th>CATEGORY</th>
\t\t\t\t\t\t\t\t<th>MARKET</th>
\t\t\t\t\t\t\t\t<th>HURRICANE_10</th>
\t\t\t\t\t\t\t\t<th>TOP_SKUS</th>
\t\t\t\t\t\t\t\t<th>BRAND</th>
\t\t\t\t\t\t\t\t<th>YEAR</th>
\t\t\t\t\t\t\t\t<th>MONTH</th>
\t\t\t\t\t\t\t\t<th>DATE_OF_TRANSACTION</th>
\t\t\t\t\t\t\t\t<th>SUM_OF_TOTAL_PURCHASE_PRODUCT</th>
\t\t\t\t\t\t\t\t<th>SUM_OF_TOTAL_QTY_PURCHASE</th>
\t\t\t\t\t\t\t\t<th>UOM</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
        // line 130
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ssds"]) ? $context["ssds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ssd"]) {
            // line 131
            echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "id", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DISTRIBUTOR", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DT_CUSTOMER_CODE", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DT_CUSTOMER_NAME", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "CUSTOMERS", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "GROUPING_CUSTOMER", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "TEAM", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "SALESMAN", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "CHANNEL", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "SALESMAN_CODE", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 142
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "WHOLESALE_SUB_CHANNEL", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 143
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "GOLD_PLUS", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 144
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "CPU", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "OEM", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 146
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DT_PRODUCT_CODE", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 147
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DT_PRODUCT_NAME", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "PRODUCT", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "PRODUCT_CODE", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 150
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "PRODUCT_GROUPING", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "CATEGORY", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 152
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "MARKET", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "HURRICANE_10", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 154
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "TOP_SKUS", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 155
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "BRAND", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 156
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "YEAR", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "MONTH", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "DATE_OF_TRANSACTION", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 159
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "SUM_OF_TOTAL_PURCHASE_PRODUCT", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "SUM_OF_TOTAL_QTY_PURCHASE", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t\t<td> ";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute($context["ssd"], "UOM", array(), "array"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ssd'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/masterViewSSD.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 164,  304 => 161,  300 => 160,  296 => 159,  292 => 158,  288 => 157,  284 => 156,  280 => 155,  276 => 154,  272 => 153,  268 => 152,  264 => 151,  260 => 150,  256 => 149,  252 => 148,  248 => 147,  244 => 146,  240 => 145,  236 => 144,  232 => 143,  228 => 142,  224 => 141,  220 => 140,  216 => 139,  212 => 138,  208 => 137,  204 => 136,  200 => 135,  196 => 134,  192 => 133,  188 => 132,  185 => 131,  181 => 130,  117 => 68,  98 => 51,  92 => 47,  89 => 46,  77 => 37,  71 => 35,  68 => 34,  43 => 12,  38 => 10,  33 => 9,  30 => 8,  11 => 6,);
    }
}
