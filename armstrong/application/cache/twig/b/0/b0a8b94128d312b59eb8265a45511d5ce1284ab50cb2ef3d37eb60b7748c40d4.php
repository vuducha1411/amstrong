<?php

/* ami/global_ssd/viewAcknowledged.html.twig */
class __TwigTemplate_b0a8b94128d312b59eb8265a45511d5ce1284ab50cb2ef3d37eb60b7748c40d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/viewAcknowledged.html.twig", 6);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_css($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        // line 11
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/jquery.dataTables-1.10.16.min.css"), "html", null, true);
        echo "\">
    ";
        // line 12
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <style>
        #mainContent {
            margin-top: 20px;
        }
    </style>
";
    }

    // line 21
    public function block_js($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables-1.10.16.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 25
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.buttons.1.5.1.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.buttons.1.5.1.html5.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/jszip.3.1.3.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 29
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/dataTables.checkboxes.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 31
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, site_url("res/js/jquery.table2excel-1.1.10.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/functions.js"), "html", null, true);
        echo "\"></script>

    <script>
        ";
        // line 37
        echo "        \$(document).ready(function(){ GLOBAL_SSD_ACK(\"";
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/entrySSDData?a=true"), "html", null, true);
        echo "\"); });
    </script>
";
    }

    // line 41
    public function block_content($context, array $blocks = array())
    {
        // line 42
        echo "    <div id=\"loadingOverlay\" class=\"overlay load-overlay\">
        ";
        // line 44
        echo "        <div class=\"overlay-content\">
            <p id=\"overlay-message\"></p>
        </div>
    </div>

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1>";
        // line 52
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</h1>
                ";
        // line 54
        echo "                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div id=\"mainContent\">
                <div class=\"row\">
                    <div class=\"col-md-4 col-md-offset-2\">
                        <div class=\"form-group\">
                            <label for=\"dot_datepicker\">(Date of transaction) From *</label>
                            <div class='input-group date datepicker'>
                                <input
                                        type='text'
                                        class=\"form-control datepicker\"
                                        id=\"dotFrom_datepicker\"
                                        name=\"dotFrom_datepicker\"
                                        placeholder=\"Select date\"
                                        data-target=\"#dotTo_datepicker\" />
                                <span class=\"input-group-addon\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-calendar\"></span>
\t\t\t\t\t\t\t\t</span>
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"dot_datepicker\">(Date of transaction) To *</label>
                            <div class='input-group date datepicker'>
                                <input
                                        type='text'
                                        class=\"form-control datepicker datepicker-today\"
                                        id=\"dotTo_datepicker\"
                                        name=\"dotTo_datepicker\"
                                        placeholder=\"Select date\" />
                                <span class=\"input-group-addon\">
\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-calendar\"></span>
\t\t\t\t\t\t\t\t</span>
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <button class=\"btn btn-xs btn-default pull-right\" id=\"filterDateOfTrans\"><i class=\"fa fa-eye\"></i> View</button>
                        </div>
                    </div>  
                    <div class=\"col-md-4\">
                        <label for=\"\">Actions</label>
                        <ul class=\"list-group en-hover\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-eye\"></i>
                                <a class=\"ml5\" href=\"";
        // line 104
        echo twig_escape_filter($this->env, site_url("ami/global_ssd"), "html", null, true);
        echo "\">View All SSD</a>
                            </li>
                            <li id=\"ssdExportBtn\" class=\"list-group-item export\">
                                <i class=\"fa fa-download\"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-12 mb15 scroll-f\">
            <div class=\"col-lg-12 mb15 scroll-f\">
                <hr/>
                ";
        // line 118
        echo "                <table cellspacing=\"0\" width=\"100%\" id=\"tbl_global_ssd\" class=\"mb15 table-ssd table table-bordered table-condensed table-striped\">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>DISTRIBUTOR</th>
                        <th>DT_CUSTOMER_CODE</th>
                        <th>DT_CUSTOMER_NAME</th>
                        <th>CUSTOMERS</th>
                        <th>GROUPING_CUSTOMER</th>
                        <th>TEAM</th>
                        <th>SALESMAN</th>
                        <th>CHANNELS</th>
                        <th>SALESMAN_CODE</th>
                        <th>WHOLESALE_SUB_CHANNEL</th>
                        <th>GOLD_PLUS</th>
                        <th>CPU</th>
                        <th>OEM</th>
                        <th>DT_PRODUCT_CODE</th>
                        <th>DT_PRODUCT_NAME</th>
                        <th>PRODUCT</th>
                        <th>PRODUCT_CODE</th>
                        <th>PRODUCT_GROUPING</th>
                        <th>CATEGORY</th>
                        <th>MARKET</th>
                        <th>HURRICANE_10</th>
                        <th>TOP_SKUS</th>
                        <th>BRAND</th>
                        <th>YEAR</th>
                        <th>MONTH</th>
                        <th>DATE_OF_TRANSACTION</th>
                        <th>SUM_OF_TOTAL_PURCHASE_PRODUCT</th>
                        <th>SUM_OF_TOTAL_QTY_PURCHASE</th>
                        <th>UOM</th>
                    </tr>
                    </thead>
                    ";
        // line 157
        echo "                </table>
                ";
        // line 159
        echo "            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/viewAcknowledged.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 159,  233 => 157,  196 => 118,  180 => 104,  128 => 54,  124 => 52,  114 => 44,  111 => 42,  108 => 41,  100 => 37,  94 => 33,  90 => 32,  85 => 31,  80 => 29,  76 => 27,  72 => 26,  67 => 25,  63 => 23,  58 => 22,  55 => 21,  44 => 13,  40 => 12,  35 => 11,  33 => 10,  30 => 9,  11 => 6,);
    }
}
