<?php

/* ami/sampling/index.html.twig */
class __TwigTemplate_b049e919ad0f731e48607ddc8425253f06e887d2d722d9b895f4df190381f2a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/sampling/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/call_modules.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    \$('#InputDateRange').daterangepicker({
        format: 'YYYY-MM-DD'
    }).on('apply.daterangepicker', function(ev, picker) {
        \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD'));
        \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD'));
    });
});
</script>
";
    }

    // line 34
    public function block_css($context, array $blocks = array())
    {
        // line 35
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 40
    public function block_content($context, array $blocks = array())
    {
        // line 41
        echo "
    ";
        // line 42
        echo form_open(site_url("ami/sampling/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 47
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Sampling Draft") : ("Sampling"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 50
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 52
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/sampling/index.html.twig", 52)->display(array_merge($context, array("url" => "ami/sampling", "icon" => "fa-binoculars", "title" => "Sampling", "permission" => "sampling", "showImport" => 1)));
        // line 53
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            
            ";
        // line 63
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 64
            echo "                <div class=\"form-group\">
                    ";
            // line 65
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 67
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"SalespersonsSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 71
        echo "            
            <div class=\"form-group\">
                ";
        // line 73
        echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 75
        echo form_input(array("name" => "date_time", "value" => null, "class" => "form-control", "id" => "InputDateRange"));
        // line 78
        echo "
                </div>

                <input type=\"hidden\" id=\"InputStartDate\" value=\"\" />
                <input type=\"hidden\" id=\"InputEndDate\" value=\"\" />
            </div>

            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\" 
                        data-url=\"";
        // line 88
        echo twig_escape_filter($this->env, site_url("ami/sampling/sampling_data"), "html", null, true);
        echo "\"
                        data-container=\"#SamplingContainer\"
                        data-draft=\"";
        // line 90
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo "\">Generate</a>
                </div>
            </div>

            <div id=\"SamplingContainer\"></div>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 100
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/sampling/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 100,  188 => 90,  183 => 88,  171 => 78,  169 => 75,  164 => 73,  160 => 71,  153 => 67,  148 => 65,  145 => 64,  143 => 63,  131 => 53,  129 => 52,  124 => 50,  118 => 47,  110 => 42,  107 => 41,  104 => 40,  98 => 37,  94 => 36,  90 => 35,  87 => 34,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
