<?php

/* ami/recipes/edit.html.twig */
class __TwigTemplate_b8c593f8ba5c1bc1286411a1590dff306ab8ecdb9064c330d11ab5628f5e092d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/recipes/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            createUploader(\$('#imageUpload'));
            createUploader(\$('#brochureUpload'));

            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$clone = \$(this).closest('.sku-base').clone();

                \$clone.find('.hide').removeClass('hide').show();

                if (\$this.hasClass('clone-add')) {
                    \$clone.clone().appendTo(\$('#sku-base-block'));
                }

                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.sku-base').remove();
                }
            });

            \$('.product-remove').on('click', function (e) {
                e.preventDefault();

                var \$this = \$(this);

                \$this.closest('.form-group').empty().remove();
            });
        });
    </script>
    <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    <script>
        CKEDITOR.replace('inputIngredients', {
            customConfig: '";
        // line 42
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.replace('inputPreparation', {
            customConfig: '";
        // line 46
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.on('instanceReady', function () {
            \$.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on(\"change\", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                });
            });
        });

    </script>
";
    }

    // line 62
    public function block_content($context, array $blocks = array())
    {
        // line 63
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 78
        echo form_open(site_url("ami/recipes/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Recipes</h1>
                <div class=\"text-right\">
                    ";
        // line 85
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/recipes/edit.html.twig", 85)->display(array_merge($context, array("url" => "ami/recipes", "id" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "id", array()), "permission" => "recipes")));
        // line 86
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 95
        echo form_label("Recipe Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 97
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 102
        echo form_label("Recipe Name (Other Language)", "name_alt", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 104
        echo form_input(array("name" => "name_alt", "value" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "name_alt", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 109
        echo form_label("Preparation", "preparation", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 111
        echo form_textarea(array("name" => "preparation", "value" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "preparation", array()), "class" => "form-control input-xxlarge", "id" => "inputPreparation", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 116
        echo form_label("Ingredients", "ingredients", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 118
        echo form_textarea(array("name" => "ingredients", "value" => $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "ingredients", array()), "class" => "form-control input-xxlarge", "id" => "inputIngredients", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            ";
        // line 122
        if ((isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null)) {
            // line 123
            echo "                <div class=\"form-group\">
                    ";
            // line 124
            echo form_label("Cuisine Channels", "cuisine_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 126
            echo form_dropdown("cuisine_channels_id", (isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null), $this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "cuisine_channels_id", array()), "class=\"form-control\" ");
            echo "
                    </div>
                </div>
            ";
        }
        // line 130
        echo "
            ";
        // line 131
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "product_ids", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["product_id"]) {
            // line 132
            echo "                <div class=\"form-group\">
                    ";
            // line 133
            echo form_label("Product", "sku_number", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 135
            echo form_dropdown("sku_number[]", (isset($context["products"]) ? $context["products"] : null), $context["product_id"], "class=\"form-control\"");
            echo "
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default product-remove\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_id'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 142
        echo "
            ";
        // line 143
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 144
            echo "                <div class=\"form-group sku-base\">
                    ";
            // line 145
            echo form_label("Product", "sku_number", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 147
            echo form_dropdown("sku_number[]", (isset($context["products"]) ? $context["products"] : null), null, "class=\"form-control\"");
            echo "
                    </div>
                    <div class=\"col-sm-3\">
                        <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                        <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                    </div>
                </div>
                <div id=\"sku-base-block\"></div>
            ";
        }
        // line 156
        echo "
            ";
        // line 163
        echo "
            <div class=\"form-group\">
                ";
        // line 165
        echo form_label("", "listed", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 167
        echo form_radio("listed", 0, ($this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "listed", array()) == 0));
        echo " Delisted</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 168
        echo form_radio("listed", 1, ($this->getAttribute((isset($context["recipe"]) ? $context["recipe"] : null), "listed", array()) == 1));
        echo " Listed</label>&nbsp;
                </div>
            </div>

            ";
        // line 172
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/recipes/edit.html.twig", 172)->display(array_merge($context, array("entry_type" => "recipes")));
        // line 173
        echo "
            ";
        // line 174
        $this->loadTemplate("ami/components/uploader_brochure.html.twig", "ami/recipes/edit.html.twig", 174)->display(array_merge($context, array("entry_type" => "recipes")));
        // line 175
        echo "
            ";
        // line 176
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 180
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/recipes/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  316 => 180,  309 => 176,  306 => 175,  304 => 174,  301 => 173,  299 => 172,  292 => 168,  288 => 167,  283 => 165,  279 => 163,  276 => 156,  264 => 147,  259 => 145,  256 => 144,  254 => 143,  251 => 142,  238 => 135,  233 => 133,  230 => 132,  226 => 131,  223 => 130,  216 => 126,  211 => 124,  208 => 123,  206 => 122,  199 => 118,  194 => 116,  186 => 111,  181 => 109,  173 => 104,  168 => 102,  160 => 97,  155 => 95,  144 => 86,  142 => 85,  132 => 78,  115 => 63,  112 => 62,  93 => 46,  86 => 42,  80 => 39,  45 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
