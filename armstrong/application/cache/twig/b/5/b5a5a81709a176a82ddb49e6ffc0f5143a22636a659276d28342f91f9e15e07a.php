<?php

/* ami/setting_cycle/edit.html.twig */
class __TwigTemplate_b5a5a81709a176a82ddb49e6ffc0f5143a22636a659276d28342f91f9e15e07a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_cycle/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {

            ";
        // line 10
        $context["startDate"] = (($this->getAttribute((isset($context["lastCycle"]) ? $context["lastCycle"] : null), "date_end", array())) ? ($this->getAttribute($this->getAttribute(        // line 11
(isset($context["lastCycle"]) ? $context["lastCycle"] : null), "date_end", array()), "addDay", array(), "method")) : ($this->getAttribute(        // line 12
(isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method")));
        // line 13
        echo "
            var options = {
                format: 'YYYY-MM-DD',
                startDate: '";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["startDate"]) ? $context["startDate"] : null), "toDateString", array(), "method"), "html", null, true);
        echo "',
                endDate: '";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["startDate"]) ? $context["startDate"] : null), "addDay", array()), "toDateString", array(), "method"), "html", null, true);
        echo "'
            }

            ";
        // line 20
        if ($this->getAttribute((isset($context["lastCycle"]) ? $context["lastCycle"] : null), "date_end", array())) {
            // line 21
            echo "            options.minDate = '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lastCycle"]) ? $context["lastCycle"] : null), "date_end", array()), "addDay", array(), "method"), "toDateString", array(), "method"), "html", null, true);
            echo "'
            ";
        }
        // line 23
        echo "
            console.log(options);

            \$('.cycle-date').daterangepicker(options)
                    .on('apply.daterangepicker', function (ev, picker) {
                        var \$this = \$(this)
                        \$parent = \$this.closest('.form-group')
                        \$startDate = \$parent.find('.cycle-date-start'),
                                \$endDate = \$parent.find('.cycle-date-end'),
                                startDate = picker.startDate.format('YYYY-MM-DD'),
                                endDate = picker.endDate.format('YYYY-MM-DD');

                        \$startDate.val(startDate);
                        \$endDate.val(endDate);
                    });

        });
    </script>
";
    }

    // line 43
    public function block_css($context, array $blocks = array())
    {
        // line 44
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 48
    public function block_content($context, array $blocks = array())
    {
        // line 49
        echo "    ";
        echo form_open(site_url("ami/setting_cycle/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Add Cycle</h1>

                <div class=\"text-right\">
                    ";
        // line 57
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/setting_cycle/edit.html.twig", 57)->display(array_merge($context, array("url" => "ami/setting_cycle", "id" => null, "permission" => "setting_cycle")));
        // line 58
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 67
        echo form_label("Cycle Name", "cycle_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 69
        echo form_input(array("name" => "cycle_name", "value" => "", "class" => "form-control", "data-parsley-required" => "true"));
        // line 74
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 79
        echo form_label("Cycle", "cycle", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 81
        echo form_input(array("name" => "cycle", "value" => "", "class" => "form-control cycle-date", "data-parsley-required" => "true"));
        // line 86
        echo "

                    ";
        // line 88
        echo form_input(array("name" => "date_start", "value" => "", "type" => "hidden", "class" => "hide cycle-date-start"));
        // line 93
        echo "

                    ";
        // line 95
        echo form_input(array("name" => "date_end", "value" => "", "type" => "hidden", "class" => "hide cycle-date-end"));
        // line 100
        echo "
                </div>
            </div>
        </div>
    </div>

    ";
        // line 106
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/setting_cycle/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 106,  173 => 100,  171 => 95,  167 => 93,  165 => 88,  161 => 86,  159 => 81,  154 => 79,  147 => 74,  145 => 69,  140 => 67,  129 => 58,  127 => 57,  115 => 49,  112 => 48,  106 => 45,  101 => 44,  98 => 43,  76 => 23,  70 => 21,  68 => 20,  62 => 17,  58 => 16,  53 => 13,  51 => 12,  50 => 11,  49 => 10,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
