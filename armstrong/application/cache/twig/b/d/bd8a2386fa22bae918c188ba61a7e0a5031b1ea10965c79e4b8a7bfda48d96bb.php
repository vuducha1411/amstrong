<?php

/* ami/route_plan/plan.html.twig */
class __TwigTemplate_bd8a2386fa22bae918c188ba61a7e0a5031b1ea10965c79e4b8a7bfda48d96bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["plans"]) ? $context["plans"] : null)) {
            echo " 
    <div class=\"panel panel-default\">                
                    
        <div class=\"panel-body\">      

            <div class=\"\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                    <thead>
                        <tr>
                            ";
            // line 10
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan"))) {
                // line 11
                echo "                                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                            ";
            }
            // line 13
            echo "                            <th>ID</th>
                            <th>Salesperson Name</th>
                            <th>Operator</th>
                            <th>Planned Date</th>
                            <th>Shifted Date</th>
                            <th>Created</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 23
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["plans"]) ? $context["plans"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["plan"]) {
                // line 24
                echo "                            <tr>
                                ";
                // line 25
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan"))) {
                    // line 26
                    echo "                                    <td class=\"text-center\">";
                    echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["plan"], "armstrong_2_route_plan_id", array())));
                    echo "</td>
                                ";
                }
                // line 28
                echo "                                <td><a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/route_plan/edit/" . $this->getAttribute($context["plan"], "armstrong_2_route_plan_id", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "armstrong_2_route_plan_id", array()), "html", null, true);
                echo "\" data-toggle=\"ajaxModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "armstrong_2_route_plan_id", array()), "html", null, true);
                echo "</a></td>
                                <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "last_name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "armstrong_2_customers_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "armstrong_2_customers_name", array()), "html", null, true);
                echo "</td>
                                <td class=\"text-center\">";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "planned_date", array()), "html", null, true);
                echo "</td>
                                <td class=\"text-center\">";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "shifted_date", array()), "html", null, true);
                echo "</td>
                                <td class=\"text-center\">";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["plan"], "date_created", array()), "html", null, true);
                echo "</td>
                                <td class=\"text-center\" style=\"min-width: 80px;\">
                                    ";
                // line 35
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "route_plan")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "route_plan")))) {
                    // line 36
                    echo "                                        ";
                    $this->loadTemplate("ami/components/table_btn.html.twig", "ami/route_plan/plan.html.twig", 36)->display(array_merge($context, array("url" => "ami/route_plan", "id" => $this->getAttribute($context["plan"], "armstrong_2_route_plan_id", array()), "permission" => "route_plan")));
                    // line 37
                    echo "                                    ";
                }
                // line 38
                echo "                                </td>
                            </tr>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['plan'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('#dataTables').dataTable({
            \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
            'order': [[1, 'asc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });        
    });  
    </script>
";
        } else {
            // line 64
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no route plan</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/route_plan/plan.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 64,  137 => 41,  121 => 38,  118 => 37,  115 => 36,  113 => 35,  108 => 33,  104 => 32,  100 => 31,  94 => 30,  86 => 29,  77 => 28,  71 => 26,  69 => 25,  66 => 24,  49 => 23,  37 => 13,  33 => 11,  31 => 10,  19 => 1,);
    }
}
