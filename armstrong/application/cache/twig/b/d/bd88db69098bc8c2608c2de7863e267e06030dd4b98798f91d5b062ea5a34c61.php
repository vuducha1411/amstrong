<?php

/* ami/accessibility/preview.html.twig */
class __TwigTemplate_bd88db69098bc8c2608c2de7863e267e06030dd4b98798f91d5b062ea5a34c61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "name", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menuItems"]) ? $context["menuItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menuItem"]) {
            // line 11
            echo "                    ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["menuItem"], "child_nodes", array())) > 0)) {
                // line 12
                echo "                        <table class=\"table no-parsley permissions m-b-lg\" id=\"md_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "name", array()), "html", null, true);
                echo "\">
                            <tr>
                                <th class=\"group-title text-ellipsis\"><i class=\"fa ";
                // line 14
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "icon", array()), "html", null, true);
                echo " fa-fw\"></i> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "display_name", array()), "html", null, true);
                echo " </th>
                                <th class=\"text-center\">View</th>
                                <th class=\"text-center\">Add</th>
                                <th class=\"text-center\">Edit</th>
                                <th class=\"text-center\">Delete</th>
                            </tr>

                            ";
                // line 21
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menuItem"], "child_nodes", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["childNode"]) {
                    // line 22
                    echo "                            <tr>
                                <th class=\"permission-title text-ellipsis\">";
                    // line 23
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childNode"], "display_name", array()), "html", null, true);
                    echo "</th>
                                <td class=\"text-center\"><i class=\"fa ";
                    // line 24
                    echo ((twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "view", array()))) ? ("fa-check") : ("fa-circle-o"));
                    echo " fa-fw\"></i></td>
                                <td class=\"text-center\"><i class=\"fa ";
                    // line 25
                    echo ((twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "add", array()))) ? ("fa-check") : ("fa-circle-o"));
                    echo " fa-fw\"></i></td>
                                <td class=\"text-center\"><i class=\"fa ";
                    // line 26
                    echo ((twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "edit", array()))) ? ("fa-check") : ("fa-circle-o"));
                    echo " fa-fw\"></i></td>
                                <td class=\"text-center\"><i class=\"fa ";
                    // line 27
                    echo ((twig_in_filter($this->getAttribute($context["childNode"], "id", array()), $this->getAttribute($this->getAttribute((isset($context["group"]) ? $context["group"] : null), "restrictions", array()), "delete", array()))) ? ("fa-check") : ("fa-circle-o"));
                    echo " fa-fw\"></i></td>
                            </tr>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['childNode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "                   
                        </table>
                        <div class=\"m-b-md\"></div>
                    ";
            }
            // line 33
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menuItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 39
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "accessibility"))) {
            // line 40
            echo "                ";
            echo html_btn(site_url(("ami/accessibility/edit/" . $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 42
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/accessibility/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 42,  111 => 40,  109 => 39,  102 => 34,  96 => 33,  90 => 29,  81 => 27,  77 => 26,  73 => 25,  69 => 24,  65 => 23,  62 => 22,  58 => 21,  46 => 14,  40 => 12,  37 => 11,  33 => 10,  25 => 5,  19 => 1,);
    }
}
