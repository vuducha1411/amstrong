<?php

/* ami/news/edit.html.twig */
class __TwigTemplate_b9ebaae6efbf44212d9559e1594170c5eb6167d12c2efb6ad067f535039e15f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/news/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script>

        \$(function () {
            createUploader(\$('#imageUpload'));
        });

    </script>
    <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    <script>
        CKEDITOR.replace('inputContent', {
            customConfig: '";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "',
            filebrowserUploadUrl: \"";
        // line 19
        echo twig_escape_filter($this->env, site_url("ami/media/ckupload"), "html", null, true);
        echo "\"
        });

        CKEDITOR.on('instanceReady', function () {
            \$.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on(\"change\", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                });
            });
        });

    </script>
";
    }

    // line 35
    public function block_content($context, array $blocks = array())
    {
        // line 36
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 51
        echo form_open(site_url("ami/news/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">News</h1>

                <div class=\"text-right\">
                    ";
        // line 59
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/news/edit.html.twig", 59)->display(array_merge($context, array("url" => "ami/news", "id" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "id", array()), "permission" => "news")));
        // line 60
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 69
        echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 71
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 76
        echo form_label("Sub Title", "sub_title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 78
        echo form_input(array("name" => "sub_title", "value" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "sub_title", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 83
        echo form_label("Content", "content", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 85
        echo form_textarea(array("name" => "content", "value" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "content", array()), "class" => "form-control input-xxlarge", "id" => "inputContent", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 90
        echo form_label("Summary", "summary", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 92
        echo form_textarea(array("name" => "summary", "value" => $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "summary", array()), "class" => "form-control", "rows" => 3));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 97
        echo form_label("", "listed", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 99
        echo form_radio("listed", 0, ($this->getAttribute((isset($context["new"]) ? $context["new"] : null), "listed", array()) == 0));
        echo " Delisted</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 100
        echo form_radio("listed", 1, ($this->getAttribute((isset($context["new"]) ? $context["new"] : null), "listed", array()) == 1));
        echo " Listed</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 105
        echo form_label("", "global", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 107
        echo form_radio("global", 0, ($this->getAttribute((isset($context["new"]) ? $context["new"] : null), "global", array()) == 0));
        echo " Private</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 108
        echo form_radio("global", 1, ($this->getAttribute((isset($context["new"]) ? $context["new"] : null), "global", array()) == 1));
        echo " Global</label>&nbsp;
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 113
        echo form_label("App", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 115
        echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), $this->getAttribute((isset($context["new"]) ? $context["new"] : null), "app_type", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            ";
        // line 119
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/news/edit.html.twig", 119)->display(array_merge($context, array("entry_type" => "news")));
        // line 120
        echo "
            ";
        // line 121
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 125
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/news/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 125,  232 => 121,  229 => 120,  227 => 119,  220 => 115,  215 => 113,  207 => 108,  203 => 107,  198 => 105,  190 => 100,  186 => 99,  181 => 97,  173 => 92,  168 => 90,  160 => 85,  155 => 83,  147 => 78,  142 => 76,  134 => 71,  129 => 69,  118 => 60,  116 => 59,  105 => 51,  88 => 36,  85 => 35,  66 => 19,  62 => 18,  56 => 15,  45 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
