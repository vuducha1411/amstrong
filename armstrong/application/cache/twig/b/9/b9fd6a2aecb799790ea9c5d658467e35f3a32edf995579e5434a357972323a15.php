<?php

/* ami/global_ssd/mappingTable.html.twig */
class __TwigTemplate_b9fd6a2aecb799790ea9c5d658467e35f3a32edf995579e5434a357972323a15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/mappingTable.html.twig", 6);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_js($context, array $blocks = array())
    {
        // line 9
        echo "\t";
        $this->displayParentBlock("js", $context, $blocks);
        echo " 
\t<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/sortable.min.js"), "html", null, true);
        echo "\"></script>
\t <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/js/global_ssd/functions.js"), "html", null, true);
        echo "\"></script>
\t";
        // line 15
        echo "\t<script>\$(document).ready(MAPPING_TABLE_INTERFACE());</script>
";
    }

    // line 18
    public function block_css($context, array $blocks = array())
    {
        // line 19
        echo "\t";
        $this->displayParentBlock("css", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">

\t<style>
\t\t#mainContent {
\t\t\tmargin-top: 20px;
\t\t}
\t</style>
";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1>Mapping Table</h1>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div id=\"mainContent\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-6 mapview-selection\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t<label for=\"\">View PRODUCT MAPPING</label>
\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<select name=\"\" id=\"dropdown_cpdtg\" class=\"form-control\" disabled=\"disabled\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">Loading maps..</option>
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group dropup\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-eye\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"dynaEditMap\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   href=\"#\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-map=\"1\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-href=\"";
        // line 64
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/modDraftMapping/"), "html", null, true);
        echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-target=\"#dropdown_cpdtg\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t>Edit selected</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 69
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/viewAllMap/1/WHLE"), "html", null, true);
        echo "\">View all maps (Whole)</a></li>
                                                        ";
        // line 71
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a data-entity=\"1\" class=\"dynaViewMap\" id=\"view_selected_dropdown_cpdtg\" data-target=\"#dropdown_cpdtg\" href=\"#\">View selected</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t<label for=\"\">View CUSTOMER MAPPING</label>
\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<select name=\"\" id=\"dropdown_ccustg\" class=\"form-control\" disabled=\"disabled\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">Loading maps..</option>
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group dropup\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-eye\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"dynaEditMap\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   href=\"#\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-map=\"2\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-href=\"";
        // line 93
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/modDraftMapping/"), "html", null, true);
        echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-target=\"#dropdown_ccustg\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t>Edit selected</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>
                                                        ";
        // line 99
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/viewAllMap/2/WHLE"), "html", null, true);
        echo "\">View all maps (Whole)</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a data-entity=\"2\" class=\"dynaViewMap\" id=\"view_selected_dropdown_ccustg\" data-target=\"#dropdown_ccustg\" href=\"#\">View selected</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t<label for=\"\">Actions</label>
\t\t\t\t\t\t\t\t\t<ul class=\"list-group en-hover\">
\t\t\t\t\t\t\t\t\t\t";
        // line 111
        echo "\t\t\t\t\t\t\t\t\t\t<li 
\t\t\t\t\t\t\t\t\t\t\tclass=\"list-group-item export-table\" 
\t\t\t\t\t\t\t\t\t\t\tid=\"doExport\" 
\t\t\t\t\t\t\t\t\t\t\tdata-target=\"#map_table\" 
\t\t\t\t\t\t\t\t\t\t\tdata-sheetname=\"Mapping Table\" 
\t\t\t\t\t\t\t\t\t\t\tdata-filename=\"MAPPING_TABLE_EXPORT\"
\t\t\t\t\t\t\t\t\t\t> 
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-download\"></i> 
\t\t\t\t\t\t\t\t\t\t\tExport 
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
        // line 121
        if ( !(isset($context["is_admin"]) ? $context["is_admin"] : null)) {
            // line 122
            echo "\t\t\t\t\t\t\t\t\t\t\t<li 
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"list-group-item\" 
\t\t\t\t\t\t\t\t\t\t\t\tdata-toggle=\"modal\" 
\t\t\t\t\t\t\t\t\t\t\t\tdata-target=\"#initNewMapping\"
\t\t\t\t\t\t\t\t\t\t\t\tdata-backdrop=\"static\" 
\t\t\t\t\t\t\t\t\t\t\t\tdata-keyboard=\"false\"
\t\t\t\t\t\t\t\t\t\t\t> <i class=\"fa fa-plus\"></i> New Mapping </li>
\t\t\t\t\t\t\t\t\t\t";
        }
        // line 130
        echo "\t\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"inputSearch\">Quick Search</label>
\t\t\t\t\t\t\t\t\t\t\t\t<input data-target=\"#map_table\" disabled=\"disabled\" id=\"inputSearch\" type=\"text\" class=\"form-control search-table\" placeholder=\"Enter keyword here\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 143
        echo "
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h4 id=\"map_title\"> <small id=\"map_stat\"></small></h4>
\t\t\t\t\t\t<div class=\"scroll-x scrollXDivFull mapping-tbl-div mb15\">
\t\t\t\t\t\t\t<table tabindex=\"1\" id=\"map_table\" class=\"table-ssd sortable table table-bordered table-condensed table-striped\">
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div> </div>
\t\t</div>
\t</div>

\t";
        // line 156
        if ( !(isset($context["is_admin"]) ? $context["is_admin"] : null)) {
            // line 157
            echo "\t\t<div class=\"modal bs-modal-sm\" id=\"initNewMapping\" tabindex=\"-1\" role=\"dialog\">
\t\t\t<div class=\"modal-dialog modal-md\" role=\"document\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t\t\t<h4 class=\"modal-title\"><i class=\"fa fa-plus\"></i> Add New Mapping</h4>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<form method=\"POST\" action=\"";
            // line 165
            echo twig_escape_filter($this->env, site_url("ami/global_ssd/addNewMappingDraft"), "html", null, true);
            echo "\" id=\"addNewMappingDraft\"></form>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t<form action=\"";
            // line 168
            echo twig_escape_filter($this->env, site_url("ami/global_ssd/doAddMappingTableDraft"), "html", null, true);
            echo "\"></form>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label for=\"entity\">Select map entity</label>
\t\t\t\t\t\t\t\t\t<select name=\"entity\" id=\"entity\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected>Please select a map..</option>
\t\t\t\t\t\t\t\t\t\t";
            // line 173
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 174
                echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array(), "array"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 175
                if (($this->getAttribute($context["entity"], "name", array(), "array") == "COMBINED_CUST_GROUPING")) {
                    // line 176
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\tCustomer Mapping
\t\t\t\t\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 177
$context["entity"], "name", array(), "array") == "COMBINED_PDT_GROUPING")) {
                    // line 178
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\tProduct Mapping
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 180
                    echo "                                                    ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "name", array(), "array"), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 182
                echo "\t\t\t\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 184
            echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"no-show form-group add-newselect\" id=\"selectDiv_PDT\">
\t\t\t\t\t\t\t\t\t<label for=\"select_PDT\">DT / WHS / CUSTOMER</label>
\t\t\t\t\t\t\t\t\t<select name=\"select_PDT\" id=\"select_PDT\" class=\"obj-entity form-control\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t<option value=\"0\">Loading list..</option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"no-show form-group add-newselect\" id=\"selectDiv_CUST\">
\t\t\t\t\t\t\t\t\t<label for=\"select_CUST\">DT / WHS / CUSTOMER</label>
\t\t\t\t\t\t\t\t\t<select name=\"select_CUST\" id=\"select_CUST\" class=\"obj-entity form-control\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t<option value=\"0\">Loading list..</option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<button id=\"btnInitMappingCreate\" class=\"btn btn-success btn-block\">Create New Mapping</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p><ul id=\"errorMsg\"></ul></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t";
            // line 207
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["cols"]) {
                // line 208
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"no-show entity-columns\" id=\"columns_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["cols"], "id", array(), "array"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"\">Columns - total of ";
                // line 209
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_split_filter($this->env, $this->getAttribute($context["cols"], "columns", array(), "array"), ":")), "html", null, true);
                echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"ami-list list-group\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 211
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(twig_split_filter($this->env, $this->getAttribute($context["cols"], "columns", array(), "array"), ":"));
                foreach ($context['_seq'] as $context["_key"] => $context["cols"]) {
                    // line 212
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\">";
                    echo twig_escape_filter($this->env, $context["cols"], "html", null, true);
                    echo "</li>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cols'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 214
                echo "\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cols'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 217
            echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div><!-- /.modal-content -->
\t\t\t</div><!-- /.modal-dialog -->
\t\t</div><!-- /.modal -->
\t";
        }
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/mappingTable.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 217,  338 => 214,  329 => 212,  325 => 211,  320 => 209,  315 => 208,  311 => 207,  286 => 184,  279 => 182,  273 => 180,  269 => 178,  267 => 177,  264 => 176,  262 => 175,  257 => 174,  253 => 173,  245 => 168,  239 => 165,  229 => 157,  227 => 156,  212 => 143,  198 => 130,  188 => 122,  186 => 121,  174 => 111,  159 => 99,  151 => 93,  127 => 71,  123 => 69,  115 => 64,  80 => 31,  77 => 30,  65 => 21,  59 => 19,  56 => 18,  51 => 15,  47 => 13,  43 => 12,  38 => 10,  33 => 9,  30 => 8,  11 => 6,);
    }
}
