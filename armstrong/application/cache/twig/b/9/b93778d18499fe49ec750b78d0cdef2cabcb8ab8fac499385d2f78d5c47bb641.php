<?php

/* ami/dashboard/infographic_new.html.twig */
class __TwigTemplate_b93778d18499fe49ec750b78d0cdef2cabcb8ab8fac499385d2f78d5c47bb641 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
\t";
        // line 2
        echo form_open(site_url("ami/dashboard/infographic_new"), array("class" => "form-horizontal modal-content"));
        echo "
\t\t<div class=\"modal-header\">
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t<h3 id=\"myModalLabel\">Select Month/Year</h3>
\t\t</div>
\t\t<div class=\"modal-body\">

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 10
        echo form_label("Type", "type", array("class" => "control-label col-sm-4"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 12
        echo form_dropdown("type", (isset($context["type"]) ? $context["type"] : null), null, "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 17
        echo form_label("Month", "month", array("class" => "control-label col-sm-4"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 19
        echo form_dropdown("month", select_month(), (isset($context["month"]) ? $context["month"] : null), "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 24
        echo form_label("Year", "year", array("class" => "control-label col-sm-4"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 26
        echo form_dropdown("year", select_year(2010), (isset($context["year"]) ? $context["year"] : null), "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t";
        // line 30
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) {
            // line 31
            echo "\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
            // line 32
            echo form_label("Country", "country", array("class" => "control-label col-sm-4"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 34
            echo form_dropdown("country", (isset($context["countries"]) ? $context["countries"] : null), $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), "class=\"form-control\"");
            echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
        }
        // line 38
        echo "
\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 40
        echo form_label("View By", "viewType", array("class" => "control-label col-sm-4"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 42
        echo form_dropdown("viewType", (isset($context["viewType"]) ? $context["viewType"] : null), null, "class=\"form-control\" id=\"viewType\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div id=\"slsrdrop\">
\t\t\t\t";
        // line 46
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()), array(0 => "Admin", 1 => "Sales Manager"))) {
            // line 47
            echo "\t\t\t\t<div class='form-group' id='salesperson_status'>
\t\t\t\t\t";
            // line 48
            echo form_label("Salesperson Status", "salespersonStatus", array("class" => "control-label col-sm-4"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 50
            echo form_dropdown("salespersonStatus", (isset($context["salesperson_status"]) ? $context["salesperson_status"] : null), 2, "class=\"form-control\"  id=\"salespersonStatus\"");
            echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class='form-group' id='salesleader'>
\t\t\t\t\t";
            // line 54
            echo form_label("Sales Leader", "salesleader", array("class" => "control-label col-sm-4"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 56
            echo form_dropdown("salesleader", (isset($context["salesleader"]) ? $context["salesleader"] : null), null, "class=\"form-control\"  id=\"sldropdown\"");
            echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\" id='salesrep'>
\t\t\t\t\t";
            // line 60
            echo form_label("Salesperson", "salespersons", array("class" => "control-label col-sm-4"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 62
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "class=\"form-control\" id=\"srdropdown\"");
            echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
                ";
        } else {
            // line 66
            echo "\t\t\t\t<div class=\"form-group\" id='salesrep'>
\t\t\t\t\t";
            // line 67
            echo form_label("Salesperson", "salespersons", array("class" => "control-label col-sm-4"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<select name=\"salespersons\" class=\"form-control\" id=\"srdropdown\">
\t\t\t\t\t\t\t<option value=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 75
        echo "\t\t\t</div>
\t\t</div>

\t\t";
        // line 86
        echo "\t\t<div class=\"modal-footer\">
\t\t\t<button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">
\t\t\t\t<i class=\"fa fa-ban\"></i> Cancel
\t\t\t</button>
\t\t\t<button name=\"btn_submit\" value=\"generate\" class=\"conf_prime delete btn btn-primary\">
\t\t\t\t<i class=\"fa fa-exclamation\"></i> Generate
\t\t\t</button>
\t\t\t<button name=\"btn_submit\" value=\"exportPDF\" class=\"conf_prime btn btn-primary\">
\t\t\t\t<i class=\"fa fa-file\"></i> Print
\t\t\t</button>
\t\t\t
\t\t</div>
\t";
        // line 98
        echo form_close();
        echo "
</div>

<script type=\"text/javascript\">
\tvar baseUrl = \"";
        // line 102
        echo twig_escape_filter($this->env, site_url("ami/dashboard/getSalespersonByLeader"), "html", null, true);
        echo "\"; //\"<?php echo base_url(); ?>\";
\tvar baseUrlCoutryChange = \"";
        // line 103
        echo twig_escape_filter($this->env, site_url("ami/dashboard/getSaleByCountry"), "html", null, true);
        echo "\"; //\"<?php echo base_url(); ?>\";

//\t\$(\"#salesleader\").hide();
//\t\$(\"#salesrep\").hide();
//\t\$(\"#salesperson_status\").hide();
\tfunction changetype(){
\t\tif(\$(\"#viewType\").val() == \"team\"){
\t\t\t\$(\"#salesleader\").show();
\t\t\t\$(\"#salesrep\").show();
\t\t\t\$(\"#salesperson_status\").show();
\t\t}else{
\t\t\t\$(\"#salesleader\").hide();
\t\t\t\$(\"#salesrep\").hide();
\t\t\t\$(\"#salesperson_status\").hide();
\t\t}
\t}
\t\$(\"#viewType\").change(function(){
\t\tchangetype();
\t});
\tchangetype();
\tfunction loadSale()
\t{
\t\tvar datachoose = {
\t\t\t'year' : \$('select[name=\"year\"]').val(),
\t\t\t'month' : \$('select[name=\"month\"]').val(),
\t\t\t'country_id' : \$('select[name=\"country\"]').val(),
\t\t\t'salespersonStatus' : \$('select[name=\"salespersonStatus\"]').val(),
\t\t};
\t\t\$.ajax({
\t\t\turl: baseUrlCoutryChange,
\t\t\ttype: 'POST',
\t\t\tdata: datachoose,
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function(){
//\t\t\t\t\$(\"#sldropdown\").attr(\"disabled\", \"disabled\");
\t\t\t\t\$('#sldropdown').html('<option value=\"0\">Please Wait...</option>');
//\t\t\t\t\$(\"#srdropdown\").attr(\"disabled\", \"disabled\");
\t\t\t\t\$('#srdropdown').html('<option value=\"0\">Please Wait...</option>');
\t\t\t},
\t\t\tsuccess: function(json){
\t\t\t\tconsole.log(json);
\t\t\t\t\$('#srdropdown').html(json.salespersons);
//\t\t\t\t\$('#srdropdown').removeAttr(\"disabled\");
\t\t\t\t\$('#sldropdown').html(json.salesleader);
//\t\t\t\t\$('#sldropdown').removeAttr(\"disabled\");
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\tconsole.log(xhr.status);
\t\t\t\tconsole.log(thrownError);
\t\t\t}
\t\t});
\t}
\t\$('select[name=\"year\"], select[name=\"month\"], select[name=\"country\"], select[name=\"salespersonStatus\"]').change(function () {
\t\tloadSale();
\t});
\t\$(\"#salesleader\").change(function(){
\t\t\$(\"#salesrep\").show();
\t\tvar datachoose = {
\t\t\t'year' : \$('select[name=\"year\"]').val(),
\t\t\t'month' : \$('select[name=\"month\"]').val(),
\t\t\t'country_id' : \$('select[name=\"country\"]').val(),
\t\t\t'salespersonStatus' : \$('select[name=\"salespersonStatus\"]').val(),
\t\t\t'sl_id' : \$(\"#sldropdown\").val(),
\t\t};
\t\t\$.ajax({
\t\t\turl: baseUrl,
\t\t\t/*dataType: 'JSON',*/
\t\t\ttype: 'POST',
\t\t\tdata: datachoose, //{ 'sl_id' : \$(\"#sldropdown\").val(), 'country_id' : \$('select[name=\"country\"]').val() },
\t\t\tbeforeSend: function(){
\t\t\t\t//\$(\"#srdropdown\").attr(\"disabled\", \"disabled\");
\t\t\t\t\$('#srdropdown').html('<option value=\"0\">Please Wait...</option>');
\t\t\t},
\t\t\tsuccess: function(response){
\t\t\t\t\$('#srdropdown').html(response);
\t\t\t//\t\$('#srdropdown').removeAttr(\"disabled\");
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
             \tconsole.log(xhr.status);
             \tconsole.log(thrownError);
            }
\t\t});
\t});
//\t\$('select[name=\"country\"]').trigger('change');
\t</script>";
    }

    public function getTemplateName()
    {
        return "ami/dashboard/infographic_new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 103,  192 => 102,  185 => 98,  171 => 86,  166 => 75,  154 => 70,  148 => 67,  145 => 66,  138 => 62,  133 => 60,  126 => 56,  121 => 54,  114 => 50,  109 => 48,  106 => 47,  104 => 46,  97 => 42,  92 => 40,  88 => 38,  81 => 34,  76 => 32,  73 => 31,  71 => 30,  64 => 26,  59 => 24,  51 => 19,  46 => 17,  38 => 12,  33 => 10,  22 => 2,  19 => 1,);
    }
}
