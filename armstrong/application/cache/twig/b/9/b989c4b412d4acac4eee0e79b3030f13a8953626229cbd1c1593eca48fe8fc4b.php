<?php

/* ami/wholesalers/transfer_customers.html.twig */
class __TwigTemplate_b989c4b412d4acac4eee0e79b3030f13a8953626229cbd1c1593eca48fe8fc4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/wholesalers/transfer_customers.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script>
        function checkTargetSalesperson() {
            var currentVal = \$('select[name=\"armstrong_2_salespersons_id\"]').val();
            var targetVal = \$('select[name=\"armstrong_2_salespersons_id_new\"]').val();
            if (!targetVal && currentVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Target Salesperson]'
                });
                return false;
            } else if (!currentVal && targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Current Salesperson]'
                });
                return false;
            } else if (!currentVal && !targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Target Salesperson] and [Current Salesperson]'
                });
                return false;
            } else if (currentVal == targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: \"[Current Salesperson] is same [Target Salesperson] <br />Can't transfer\"
                });
                return false;
            }
            return true;
        }

        function getCustomers() {
            var targetElement = \$('.customers_assigned'),
                    inputCustomers = \$('input[name=\"customers\"]'),
                    items = targetElement.find('li div'),
                    array_val = [];
            \$.each(items, function () {
                var \$val = \$(this).data('customer');
                array_val.push(\$val);
            });
            inputCustomers.val(array_val);
        }

        \$('.customers_block').perfectScrollbar();
        \$(function () {
            var item_left_selected = \$('.customers_unassigned ul').finderSelect({children: \".current_item\"});
            var item_right_selected = \$('.customers_assigned ul').finderSelect({children: \".current_item\"});
            \$('#armstrong_2_salespersons_id').on('change', function () {
                var \$val = \$(this).val(),
                        customers_block_wrapper = \$('.customers_unassigned');
                item_left_selected.finderSelect(\"selected\").removeClass('selected');
                \$.each(customers_block_wrapper.find('li div'), function () {
                    var armstrong_2_salesperson = \$(this).data('salesperson');
                    if (armstrong_2_salesperson !== \$val) {
                        \$(this).closest('li').removeClass('current_item').hide();
                    } else {
                        \$(this).closest('li').removeClass('hidden').addClass('current_item').show();
                    }
                });
            });

            \$('.transfer-right').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = item_left_selected.finderSelect('selected').clone(),
                            targetBlock = \$('.customers_assigned ul');
                    item_left_selected.finderSelect('selected').remove();
                    selectedElement.appendTo(targetBlock);
                    getCustomers();
                }
            });

            \$('.transfer-left').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = item_right_selected.finderSelect('selected').clone(),
                            targetBlock = \$('.customers_unassigned ul');
                    item_right_selected.finderSelect('selected').remove();
                    selectedElement.appendTo(targetBlock);
                    getCustomers();
                }
            });

            \$('button[type=\"submit\"]').on('click', function (e) {
                e.preventDefault();
                var input_customers = \$('input[name=\"customers\"]').val();
                if (!input_customers) {
                    \$.alert({
                        title: 'Alert!',
                        content: \"Nothing to transfer\"
                    });
                    return false;
                }
                \$.confirm({
                    title: 'Confirm!',
                    content: 'All current Customers of [Current Salesperson] will be shifted to [Target Salesperson]. <br />Are you sure?',
                    confirm: function () {
                        \$('#myform').submit();
                    },
                    cancel: function () {

                    }
                });
            });
        });
    </script>
";
    }

    // line 112
    public function block_css($context, array $blocks = array())
    {
        // line 113
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .customers_block {
            height: 300px;
            overflow: hidden;
            position: relative;
            border: 1px solid #ddd;
            border-radius: 10px;
            padding: 10px;
        }

        .customers_block ul {
            list-style: none;
            padding: 0;
        }

        .customers_block ul li {
            cursor: pointer;
        }

        .customers_block ul li.selected {
            background-color: #337ab7;
            color: #fff;
        }

        .btn_transfer {
            margin-top: 100px;
        }
    </style>
";
    }

    // line 144
    public function block_content($context, array $blocks = array())
    {
        // line 145
        echo "    ";
        echo form_open(site_url("ami/wholesalers/transfer_customers_do"), array("class" => "", "id" => "myform", "data-parsley-validate" => "true"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">

            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Transfer of Customers</h1>

                <div class=\"text-right\">
                    ";
        // line 154
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/wholesalers/transfer_customers.html.twig", 154)->display(array_merge($context, array("url" => "ami/customers", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "permission" => "customer")));
        // line 155
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"left-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 165
        echo form_label("Current Salesperson", "armstrong_2_salespersons_id", array("class" => ""));
        echo "
                    ";
        // line 166
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "id=\"armstrong_2_salespersons_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    ";
        // line 169
        echo form_label("Unassigned", "customers_unassigned", array("class" => ""));
        echo "
                    <div class=\"customers_unassigned customers_block\">
                        <ul>
                            ";
        // line 172
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["customers"]) ? $context["customers"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["customer"]) {
            // line 173
            echo "                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["customer"]);
            foreach ($context['_seq'] as $context["customer_id"] => $context["_customer"]) {
                // line 174
                echo "                                    <li class=\"hidden\">
                                        <div data-customer=\"";
                // line 175
                echo twig_escape_filter($this->env, $context["customer_id"], "html", null, true);
                echo "\"
                                             data-salesperson=\"";
                // line 176
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["_customer"], "html", null, true);
                echo "</div>
                                    </li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['customer_id'], $context['_customer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 179
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['customer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 180
        echo "                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"btn_transfer col-sm-2\">
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-right\">
                    <i class=\"fa fa-long-arrow-right\"></i>
                    Left to Right
                </a>
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-left\">
                    <i class=\"fa fa-long-arrow-left\"></i>
                    Right to Left
                </a>
            </div>
            <div class=\"right-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 196
        echo form_label("Target Salesperson", "armstrong_2_salespersons_id_new", array("class" => ""));
        echo "
                    ";
        // line 197
        echo form_dropdown("armstrong_2_salespersons_id_new", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id_new", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    ";
        // line 200
        echo form_label("Assigned", "customers_assigned", array("class" => ""));
        echo "
                    <div class=\"customers_assigned customers_block\">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type=\"hidden\" name=\"customers\">
    ";
        // line 209
        echo form_close();
        echo "
    <link href=\"";
        // line 210
        echo twig_escape_filter($this->env, site_url("res/css/plugins/perfect-scrollbar/perfect-scrollbar.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <link href=\"";
        // line 212
        echo twig_escape_filter($this->env, site_url("res/css/jquery-confirm.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, site_url("res/js/jquery-confirm.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, site_url("res/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"//oss.maxcdn.com/libs/jquery.finderselect/0.6.0/jquery.finderselect.min.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/transfer_customers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 215,  316 => 214,  311 => 212,  306 => 210,  302 => 209,  290 => 200,  284 => 197,  280 => 196,  262 => 180,  256 => 179,  245 => 176,  241 => 175,  238 => 174,  233 => 173,  229 => 172,  223 => 169,  217 => 166,  213 => 165,  201 => 155,  199 => 154,  186 => 145,  183 => 144,  148 => 113,  145 => 112,  33 => 4,  30 => 3,  11 => 1,);
    }
}
