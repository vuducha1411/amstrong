<?php

/* ami/discount_model/clone_item.html.twig */
class __TwigTemplate_b61a4d46ae2f0b7e7faac735d2ed2d5545b763f1cebda803065605addf5e2494 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"clone-product-step hide\">
    <div class=\"form-group\">
        <hr/>
        ";
        // line 4
        echo form_label("Products", "sku_number", array("class" => "control-label col-sm-3"));
        echo "
        <div class=\"col-sm-6\">
            <select name=\"\" class=\"form-control products\" multiple>
                ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 8
            echo "                    <option data-cat-web=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "products_cat_web_id", array()), "html", null, true);
            echo "\"
                            value=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["product"], "sku_number", array()) . " - ") . $this->getAttribute($context["product"], "sku_name", array())), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "            </select>
            <div><p><strong>Select multiple by holding CTRL or SHIFT while clicking.</strong></p></div>
        </div>
        <div class=\"col-sm-3\">
            <button class=\"btn btn-default btn-clone-section clone-add\"><i class=\"fa fa-plus\"></i>
            </button>
            <button class=\"btn btn-default btn-clone-section clone-remove\"><i class=\"fa fa-minus\"></i>
            </button>
        </div>
    </div>
    <div class=\"base\" data-section=\"\">
        <div class=\"form-group step\">
            ";
        // line 23
        echo form_label("Tier 1 Case", "tier_1_case", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-3\">
                <input type=\"text\" name=\"\" class=\"form-control input-case\" min=\"1\" data-parsley-type=\"number\"/>
            </div>
            ";
        // line 27
        echo form_label("Discount(%)", "tier_1_discount", array("class" => "control-label col-sm-1"));
        echo "
            <div class=\"col-sm-2\">
                <input type=\"text\" name=\"\" class=\"form-control input-discount\"
                       min=\"1\" data-parsley-type=\"number\"/>
            </div>
            <div class=\"col-sm-3\">
                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i>
                </button>
            </div>
        </div>
        <div class=\"form-group clone-step hide\">
            ";
        // line 39
        echo form_label("Tier 1 Case", "", array("class" => "control-label col-sm-3 case-label"));
        echo "
            <div class=\"col-sm-3 input-case\">
                ";
        // line 42
        echo "            </div>
            ";
        // line 43
        echo form_label("Discount(%)", "tier_1_discount", array("class" => "control-label col-sm-1"));
        echo "
            <div class=\"col-sm-2 input-discount\">
                ";
        // line 46
        echo "            </div>
            <div class=\"col-sm-3\">
                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                </button>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/discount_model/clone_item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 46,  93 => 43,  90 => 42,  85 => 39,  70 => 27,  63 => 23,  49 => 11,  39 => 9,  34 => 8,  30 => 7,  24 => 4,  19 => 1,);
    }
}
