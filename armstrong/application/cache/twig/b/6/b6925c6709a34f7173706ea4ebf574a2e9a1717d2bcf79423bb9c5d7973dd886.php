<?php

/* ami/dashboard/wellcome.html.twig */
class __TwigTemplate_b6925c6709a34f7173706ea4ebf574a2e9a1717d2bcf79423bb9c5d7973dd886 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/dashboard/wellcome.html.twig", 1);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_css($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<style type=\"text/css\">
\t#wellcome{
\t\ttext-align:center;
\t\tfont-size:30px;
\t\tmargin:auto;
\t\tpadding-top:40px;
\t\ttext-shadow: -5px 5px 5px rgba(0,0,0,0.3);
\t}
</style>
";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        // line 18
        echo "    <div id=\"wellcome\">Welcome to Armstrong Management Interface</div>
";
    }

    public function getTemplateName()
    {
        return "ami/dashboard/wellcome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 18,  50 => 17,  47 => 16,  32 => 4,  29 => 3,  11 => 1,);
    }
}
