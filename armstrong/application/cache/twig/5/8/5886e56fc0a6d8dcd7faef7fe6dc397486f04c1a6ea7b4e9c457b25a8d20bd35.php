<?php

/* ami/dashboard/infographic.html.twig */
class __TwigTemplate_5886e56fc0a6d8dcd7faef7fe6dc397486f04c1a6ea7b4e9c457b25a8d20bd35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
\t";
        // line 2
        echo form_open(site_url("ami/dashboard/infographic"), array("class" => "form-horizontal modal-content"));
        echo "
\t\t<div class=\"modal-header\">
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t<h3 id=\"myModalLabel\">Select Month/Year</h3>
\t\t</div>
\t\t<div class=\"modal-body\">

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 10
        echo form_label("Type", "type", array("class" => "control-label col-sm-3"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 12
        echo form_dropdown("type", array("tfo" => "TFO", "ssd" => "SSD"), (isset($context["month"]) ? $context["month"] : null), "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 17
        echo form_label("Month", "month", array("class" => "control-label col-sm-3"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 19
        echo form_dropdown("month", select_month(), (isset($context["month"]) ? $context["month"] : null), "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t";
        // line 24
        echo form_label("Year", "year", array("class" => "control-label col-sm-3"));
        echo "
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t";
        // line 26
        echo form_dropdown("year", select_year(2010), (isset($context["year"]) ? $context["year"] : null), "class=\"form-control\"");
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t";
        // line 30
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "roleName", array()) == "Admin")) {
            // line 31
            echo "\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
            // line 32
            echo form_label("Country", "country", array("class" => "control-label col-sm-3"));
            echo "
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t";
            // line 34
            echo form_dropdown("country", (isset($context["countries"]) ? $context["countries"] : null), $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), "class=\"form-control\"");
            echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
        }
        // line 38
        echo "\t\t</div>
\t\t<div class=\"modal-footer\">
\t\t\t<button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">
\t\t\t\t<i class=\"fa fa-ban\"></i> Cancel
\t\t\t</button>
\t\t\t<button name=\"btn_submit\" value=\"generate\" class=\"conf_prime delete btn btn-primary\">
\t\t\t\t<i class=\"fa fa-exclamation\"></i> Generate
\t\t\t</button>
\t\t\t<button name=\"btn_submit\" value=\"exportPDF\" class=\"conf_prime btn btn-primary\">
\t\t\t\t<i class=\"fa fa-file\"></i> Print
\t\t\t</button>
\t\t</div>
\t";
        // line 50
        echo form_close();
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "ami/dashboard/infographic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 50,  88 => 38,  81 => 34,  76 => 32,  73 => 31,  71 => 30,  64 => 26,  59 => 24,  51 => 19,  46 => 17,  38 => 12,  33 => 10,  22 => 2,  19 => 1,);
    }
}
