<?php

/* ami/module_field_qc/index.html.twig */
class __TwigTemplate_5bfa26320beb596a62d36bae04fc939c73cfcd6fe172c5dbb685a72b3fce1e30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/module_field_qc/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 12
        echo twig_escape_filter($this->env, site_url("ami/module_field_qc/ajaxData"), "html", null, true);
        echo "\",*/
              //  \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
               // 'order': [[1, 'asc']],
                'bPaginate': false,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            var num_field = '";
        // line 21
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["list_fields"]) ? $context["list_fields"] : null)), "html", null, true);
        echo "';
            console.log(num_field);
            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
        var flagsord = true;
        \$(document).on('keyup', '#dataTables_filter input[type=\"search\"]', function(){
            if(\$(this).val() != '')
            {
                \$('.saveaction').hide();
                flagsord = false;
                \$('.sortquestion').sortable('disable');
            }
            else
            {
                \$('.saveaction').show();
                flagsord = true;
                \$( \".sortquestion\" ).sortable();
                \$( \".sortquestion\" ).sortable( \"option\", \"disabled\", false );
                // ^^^ this is required otherwise re-enabling sortable will not work!
                \$( \".sortquestion\" ).disableSelection();
                return false;
            }
        });
        \$(document).on('click','button',function(){
            if(\$(this).data('dismiss')==\"modal\"){
                \$('#myModal #list_field > option').each(function(k,v){
                    if(\$(v).data('temp') == 1){
                        \$(this).remove();
                    }
                });
            }
        });
        \$('.editquestion').click(function(){
            var idquestion = \$(this).data('id');
            \$.ajax({
                url: \"";
        // line 63
        echo twig_escape_filter($this->env, site_url("ami/module_field_qc/edit/"), "html", null, true);
        echo "/\"+idquestion,
                type: 'get',
                dataType: 'json',
                success: function (json)
                {
                    if(json.id)
                    {
                        \$('#myModal input[name=\"title\"]').val(json.title);
                        \$('#myModal input[name=\"id\"]').val(json.id);
                        \$('#myModal #list_field').prepend('<option value='+json.field_name+' data-temp=\"1\" selected=\"selected\">'+json.field_name+'</option>');
                        \$('#myModal #list_type').val(json.type);
                        \$('#myModal input[name=\"app_type\"][value=\"'+json.app_type+'\"]').click();
                        \$('#myModal input[name=\"type\"][value=\"'+json.type+'\"]').click();
                        \$('#myModal input[name=\"parent_id\"]').val(json.parent_id);
                        \$('#myModal input[name=\"type_question\"]').val(json.type_question);
                        \$('#myModal input[name=\"require\"][value=\"'+json.require+'\"]').click();
                        \$('#myModal').modal('show');
                    }
                    else
                    {
                        alert(json)
                    }
                }
            });
            return false;
        });
        \$('.addaction').click(function(){
            \$('#myModal input[name=\"title\"]').val('');
            \$('#myModal input[name=\"id\"]').val('');
            \$('#myModal input[name=\"app_type\"][value=\"0\"]').click();
            \$('#myModal input[name=\"type\"][value=\"0\"]').click();
            \$('#myModal input[name=\"parent_id\"]').val('";
        // line 94
        echo twig_escape_filter($this->env, (isset($context["parent_id"]) ? $context["parent_id"] : null), "html", null, true);
        echo "');
            \$('#myModal input[name=\"type_question\"]').val('";
        // line 95
        echo (((isset($context["parent_id"]) ? $context["parent_id"] : null)) ? (1) : (0));
        echo "');
            \$('#myModal input[name=\"require\"][value=\"0\"]').click();
            \$('#myModal #list_type').val('');
            \$('#myModal').modal('show');
        });
        function updateOrder()
        {
            \$.ajax({
                url: \"";
        // line 103
        echo twig_escape_filter($this->env, site_url("ami/module_field_qc/update/"), "html", null, true);
        echo "/\",
                type: 'POST',
                beforeSend: function () {
                    \$('.loaddingicon').show();
                },
                complete: function () {
                    \$('.loaddingicon').hide();
                },
                dataType: 'json',
                data: \$('.sortquestion input[type=\"checkbox\"]'),
                success: function (json)
                {

                }
            });
        }
        \$(\".sortquestion\").sortable({
            update: function(event, ui) {
                updateOrder();
            }
        }).disableSelection();
    </script>
";
    }

    // line 127
    public function block_css($context, array $blocks = array())
    {
        // line 128
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 129
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <style>
        .sortquestion .center {
            cursor: move;
        }
        .ui-sortable-disabled .center {
            cursor: default !important;
        }
        .nosort::after{
            display: none;
        }
    </style>
";
    }

    // line 143
    public function block_content($context, array $blocks = array())
    {
        // line 144
        echo "
    ";
        // line 145
        echo form_open(site_url("ami/module_field_qc/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 150
        echo twig_escape_filter($this->env, (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Questions Draft") : ((isset($context["tittle_page"]) ? $context["tittle_page"] : null))), "html", null, true);
        echo "</h1>

                <div class=\"text-right\">
                    ";
        // line 153
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("view", "questions"))) {
            // line 154
            echo "                        <div role=\"group\" class=\"btn-group btn-header-toolbar\">
                            ";
            // line 155
            echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger hide\" nam=\"delete\"  id=\"CheckAllBtn\""));
            echo "
                            ";
            // line 156
            if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
                // line 157
                echo "                                <a class=\"btn btn-sm btn-default\" href=\"";
                echo twig_escape_filter($this->env, site_url("ami/module_field_qc/index"), "html", null, true);
                echo "\"><i class=\"fa fa-fw fa-ban\"></i> Cancel</a>
                            ";
            }
            // line 159
            echo "                            ";
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", "questions"))) {
                // line 160
                echo "                                <button type=\"button\" class=\"btn btn-sm btn-success addaction\">
                                    <i class=\"fa fw fa-plus\"></i> Add
                                </button>
                                ";
                // line 164
                echo "                                ";
                // line 165
                echo "                            ";
            }
            // line 166
            echo "                        </div>
                    ";
        }
        // line 168
        echo "
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    ";
        // line 175
        if ( !(isset($context["draft"]) ? $context["draft"] : null)) {
            // line 176
            echo "        <!--div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"dropdown\">
                    <button class=\"btn btn-default dropdown-toggle\" type=\"button\"
                            id=\"dropdownFilter\" data-toggle=\"dropdown\">
                        Filter: ";
            // line 181
            echo twig_escape_filter($this->env, (((isset($context["filter"]) ? $context["filter"] : null)) ? (twig_capitalize_string_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null))) : ("All")), "html", null, true);
            echo " <span
                                class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownFilter\">
                        <li role=\"presentation\" ";
            // line 185
            echo (((isset($context["filter"]) ? $context["filter"] : null)) ? ("") : ("class=\"active\""));
            echo "><a
                                    role=\"menuitem\" tabindex=\"-1\"
                                    href=\"";
            // line 187
            echo twig_escape_filter($this->env, site_url("ami/questions"), "html", null, true);
            echo "\">All</a></li>
                        <li role=\"presentation\" ";
            // line 188
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "approved")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 190
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=pull"), "html", null, true);
            echo "\">Pull</a>
                        </li>
                        <li role=\"presentation\" ";
            // line 192
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 194
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=push"), "html", null, true);
            echo "\">Push</a>
                        </li>
                        <li role=\"presentation\" ";
            // line 196
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "reject")) ? ("class=\"active\"") : (""));
            echo ">
                            <a role=\"menuitem\" tabindex=\"-1\"
                               href=\"";
            // line 198
            echo twig_escape_filter($this->env, site_url("ami/questions?filter=leader"), "html", null, true);
            echo "\">Leader</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div-->
    ";
        }
        // line 205
        echo "    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">

                            <thead>
                            <tr>
                                ";
        // line 215
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions"))) {
            // line 216
            echo "                                    <th style=\"width:30px;\" class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 220
        echo "                                <th style=\"width:30px;\" class=\"nosort\">Order</th>
                                <th class=\"nosort\">Title</th>
                                ";
        // line 222
        if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
            // line 223
            echo "                                    <th style=\"width:80px;\" class=\"nosort\">Require</th>
                                    <th style=\"width:80px;\" class=\"nosort\">Field Name</th>
                                    <th style=\"width:80px;\" class=\"nosort\">Type</th>
                                ";
        } else {
            // line 227
            echo "                                    <th style=\"width:80px;\" class=\"nosort\">App Type</th>
                                    <th style=\"width:80px;\" class=\"nosort\">Is SKU</th>
                                    <th style=\"width:80px;\" class=\"nosort\">Total Elements</th>
                                ";
        }
        // line 231
        echo "
                                ";
        // line 233
        echo "                                <th style=\"width:120px;\" class=\"nosort\">Updated</th>
                                <th style=\"width:80px;\" class=\"nosort text-center\">
                                    <img width=\"30px\" style=\"display: none; \" class=\"loaddingicon\" src=\"";
        // line 235
        echo twig_escape_filter($this->env, site_url("res/img"), "html", null, true);
        echo "/loading.gif\">
                                </th>
                            </tr>
                            </thead>
                            <tbody class=\"sortquestion\">
                            ";
        // line 240
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 241
            echo "                                <tr>
                                    ";
            // line 242
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions"))) {
                // line 243
                echo "                                        <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                    ";
            }
            // line 245
            echo "                                    ";
            // line 246
            echo "                                    <td class=\"center\">
                                        <span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
                                    </td>
                                    <td class=\"center\">
                                        ";
            // line 250
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "questions"))) {
                // line 251
                echo "                                            ";
                if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
                    // line 252
                    echo "                                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                    echo "
                                            ";
                } else {
                    // line 254
                    echo "                                                <a href=\"";
                    echo twig_escape_filter($this->env, site_url(((("ami/module_field_qc/index/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                    echo "</a>
                                            ";
                }
                // line 256
                echo "                                        ";
            } else {
                // line 257
                echo "                                            ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "
                                        ";
            }
            // line 258
            echo "</td>

                                    ";
            // line 260
            if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
                // line 261
                echo "                                        <td>";
                echo ((($this->getAttribute($context["post"], "require", array()) == 1)) ? ("Yes") : ("No"));
                echo "</td>
                                        <td>";
                // line 262
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "field_name", array()), "html", null, true);
                echo "</td>
                                        <td>";
                // line 263
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["list_types"]) ? $context["list_types"] : null), $this->getAttribute($context["post"], "type", array()), array(), "array"), "html", null, true);
                echo "</td>
                                    ";
            } else {
                // line 265
                echo "                                        <td class=\"center\">";
                if (($this->getAttribute($context["post"], "app_type", array()) == 0)) {
                    echo "  Pull
                                            ";
                } elseif (($this->getAttribute(                // line 266
$context["post"], "app_type", array()) == 1)) {
                    echo " Push
                                            ";
                } else {
                    // line 267
                    echo "  All
                                            ";
                }
                // line 269
                echo "                                        </td>
                                        <td>";
                // line 270
                echo ((($this->getAttribute($context["post"], "type", array()) == 10)) ? ("Yes") : ("No"));
                echo "</td>
                                        <td>";
                // line 271
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "total_element", array()), "html", null, true);
                echo "</td>
                                    ";
            }
            // line 273
            echo "                                    ";
            // line 274
            echo "                                    <td class=\"center\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                    <td class=\"center text-center\" style=\"min-width: 80px;\">
                                        <div class=\"btn-group\">

                                            ";
            // line 278
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "questions"))) {
                // line 279
                echo "                                                ";
                echo html_btn(site_url(((("ami/module_field_qc/index/" . $this->getAttribute($context["post"], "id", array())) . "?filter=") . (isset($context["filter"]) ? $context["filter"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit editquestion", "data-id" => $this->getAttribute($context["post"], "id", array()), "title" => "Edit"));
                echo "

                                            ";
            }
            // line 282
            echo "
                                            ";
            // line 283
            if (((isset($context["filter"]) ? $context["filter"] : null) == "pending")) {
                // line 284
                echo "                                                ";
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")))) {
                    // line 285
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_qc/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-up\"></i>", array("class" => "btn-default approve", "title" => "Approve", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                    // line 286
                    echo html_btn(site_url(("ami/module_field_qc/pending/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-thumbs-down\"></i>", array("class" => "btn-default reject", "title" => "Reject", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 288
                echo "                                            ";
            }
            // line 289
            echo "
                                            ";
            // line 290
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "questions")) && ((isset($context["filter"]) ? $context["filter"] : null) != "pending"))) {
                // line 291
                echo "                                                ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 292
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_qc/restore/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                } else {
                    // line 294
                    echo "                                                    ";
                    echo html_btn(site_url(("ami/module_field_qc/delete/" . $this->getAttribute($context["post"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                ";
                }
                // line 296
                echo "                                            ";
            }
            // line 297
            echo "                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 301
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 305
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 306
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/module_field_qc/draft?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/module_field_qc/draft")));
            // line 307
            echo "                                ";
        } else {
            // line 308
            echo "                                    ";
            $context["pagination_url"] = (((isset($context["filter"]) ? $context["filter"] : null)) ? (site_url(("ami/module_field_qc/?filter=" . (isset($context["filter"]) ? $context["filter"] : null)))) : (site_url("ami/questions")));
            // line 309
            echo "                                ";
        }
        // line 310
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => (isset($context["pagination_url"]) ? $context["pagination_url"] : null), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    ";
        // line 323
        echo form_close();
        echo "
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Add ";
        // line 329
        echo twig_escape_filter($this->env, (isset($context["tittle_page"]) ? $context["tittle_page"] : null), "html", null, true);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    ";
        // line 332
        echo form_open(site_url("ami/module_field_qc/save"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "
                    <div class=\"form-group\">
                        ";
        // line 334
        echo form_label("Title", "title", array("class" => "control-label"));
        echo "
                        ";
        // line 335
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                    </div>
                    ";
        // line 337
        if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
            // line 338
            echo "                        <div class=\"form-group\">
                            ";
            // line 339
            echo form_label("Field", "field_name", array("class" => "control-label"));
            echo "
                            ";
            // line 340
            echo form_dropdown("field_name", (isset($context["list_fields"]) ? $context["list_fields"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "field_name", array()), "id=\"list_field\" class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                        </div>
                        <div class=\"form-group\">
                            ";
            // line 343
            echo form_label("Type", "type", array("class" => "control-label"));
            echo "
                            ";
            // line 344
            echo form_dropdown("type", (isset($context["list_types"]) ? $context["list_types"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()), "id=\"list_type\" class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                        </div>
                    ";
        } else {
            // line 347
            echo "

                    <div class=\"form-group select_app_type \">
                        ";
            // line 350
            echo form_label("App Type", "Type", array("class" => "control-label"));
            echo "
                        <div class=\"no-parsley\">
                            <label class=\"radio-inline\">";
            // line 352
            echo form_radio("app_type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 0));
            echo " Pull</label>&nbsp;
                            <label class=\"radio-inline\">";
            // line 353
            echo form_radio("app_type", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 1));
            echo " Push</label>&nbsp;
                            <label class=\"radio-inline\">";
            // line 354
            echo form_radio("app_type", 2, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array()) == 2));
            echo " All</label>
                        </div>

                    </div>
                    <div class=\"form-group select_app_type\">
                        ";
            // line 359
            echo form_label("Is SKU", "Type", array("class" => "control-label"));
            echo "
                        <div class=\"no-parsley\">
                            <label class=\"radio-inline\">";
            // line 361
            echo form_radio("type", 10, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()) == 10));
            echo " Yes</label>&nbsp;
                            <label class=\"radio-inline\">";
            // line 362
            echo form_radio("type", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array()) == 0));
            echo " No</label>&nbsp;
                        </div>
                    </div>
                    ";
        }
        // line 366
        echo "
                    <div class=\"form-group ";
        // line 367
        if ((isset($context["istitle"]) ? $context["istitle"] : null)) {
        } else {
            echo "hide";
        }
        echo "\">
                        ";
        // line 368
        echo form_label("Require", "Require", array("class" => "control-label"));
        echo "
                        <div class=\"no-parsley\">
                            <label class=\"radio-inline\">";
        // line 370
        echo form_radio("require", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "require", array()) == 1));
        echo " Yes</label>&nbsp;
                            <label class=\"radio-inline\">";
        // line 371
        echo form_radio("require", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "require", array()) == 0));
        echo " No</label>&nbsp;
                        </div>
                    </div>
                    <input type=\"hidden\" name=\"type_question\" value=\"";
        // line 374
        echo (((isset($context["parent_id"]) ? $context["parent_id"] : null)) ? (1) : (0));
        echo "\">
                    <input type=\"hidden\" name=\"parent_id\" value=\"";
        // line 375
        echo twig_escape_filter($this->env, (isset($context["parent_id"]) ? $context["parent_id"] : null), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"id\" value=\"\">
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>
                </div>
                ";
        // line 382
        echo form_close();
        echo "
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/module_field_qc/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  726 => 382,  716 => 375,  712 => 374,  706 => 371,  702 => 370,  697 => 368,  690 => 367,  687 => 366,  680 => 362,  676 => 361,  671 => 359,  663 => 354,  659 => 353,  655 => 352,  650 => 350,  645 => 347,  639 => 344,  635 => 343,  629 => 340,  625 => 339,  622 => 338,  620 => 337,  615 => 335,  611 => 334,  606 => 332,  600 => 329,  591 => 323,  574 => 310,  571 => 309,  568 => 308,  565 => 307,  562 => 306,  560 => 305,  554 => 301,  545 => 297,  542 => 296,  536 => 294,  530 => 292,  527 => 291,  525 => 290,  522 => 289,  519 => 288,  514 => 286,  509 => 285,  506 => 284,  504 => 283,  501 => 282,  494 => 279,  492 => 278,  484 => 274,  482 => 273,  477 => 271,  473 => 270,  470 => 269,  466 => 267,  461 => 266,  456 => 265,  451 => 263,  447 => 262,  442 => 261,  440 => 260,  436 => 258,  430 => 257,  427 => 256,  419 => 254,  413 => 252,  410 => 251,  408 => 250,  402 => 246,  400 => 245,  394 => 243,  392 => 242,  389 => 241,  385 => 240,  377 => 235,  373 => 233,  370 => 231,  364 => 227,  358 => 223,  356 => 222,  352 => 220,  346 => 216,  344 => 215,  332 => 205,  322 => 198,  317 => 196,  312 => 194,  307 => 192,  302 => 190,  297 => 188,  293 => 187,  288 => 185,  281 => 181,  274 => 176,  272 => 175,  263 => 168,  259 => 166,  256 => 165,  254 => 164,  249 => 160,  246 => 159,  240 => 157,  238 => 156,  234 => 155,  231 => 154,  229 => 153,  223 => 150,  215 => 145,  212 => 144,  209 => 143,  192 => 129,  187 => 128,  184 => 127,  157 => 103,  146 => 95,  142 => 94,  108 => 63,  63 => 21,  51 => 12,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
