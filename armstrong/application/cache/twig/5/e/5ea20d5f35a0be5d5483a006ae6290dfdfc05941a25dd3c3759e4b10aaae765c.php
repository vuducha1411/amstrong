<?php

/* ami/webshop/edit.html.twig */
class __TwigTemplate_5ea20d5f35a0be5d5483a006ae6290dfdfc05941a25dd3c3759e4b10aaae765c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/webshop/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/webshop.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 16
    public function block_css($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        echo form_open_multipart(site_url(("ami/webshop/add_new/" . (isset($context["armstrong_2_webshop_id"]) ? $context["armstrong_2_webshop_id"] : null))), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_webshop_id" => (isset($context["armstrong_2_webshop_id"]) ? $context["armstrong_2_webshop_id"] : null)));
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Webshop Order</h1>
                <div class=\"text-right\">
                    ";
        // line 28
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/webshop/edit.html.twig", 28)->display(array_merge($context, array("url" => "ami/webshop", "id" => (isset($context["armstrong_2_webshop_id"]) ? $context["armstrong_2_webshop_id"] : null), "permission" => "webshop")));
        // line 29
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
            <div>
                <ul class=\"breadcrumb\">
                    <li><a href=";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo ">Dashboard</a></li>
                    <li><a href=";
        // line 35
        echo twig_escape_filter($this->env, (base_url() . "ami/webshop"), "html", null, true);
        echo ">Webshop</a></li>
                    <li><a href=\"#\">Edit Order</a></li>
                </ul>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 47
        echo form_label("Salesperson ID", "salesperson", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 49
        echo form_dropdown("", (isset($context["salespersons"]) ? $context["salespersons"] : null), (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), "class = 'form-control',
                        id='armstrong_2_salesperson'");
        // line 51
        echo "
                    </div>
                    <input type=\"hidden\" name=\"armstrong_2_salespersons_id\" id=\"armstrong_2_salespersons_id\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_salespersons_id"]) ? $context["armstrong_2_salespersons_id"] : null), "html", null, true);
        echo "\">
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 58
        echo form_label("Customer ID", "customer", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 60
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_customer"));
        echo "
                    </div>
                    <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"armstrong_2_customers_id\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_customers_id"]) ? $context["armstrong_2_customers_id"] : null), "html", null, true);
        echo "\">
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 67
        echo form_label("Distributor ID", "distributor", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 69
        echo form_input(array("name" => "", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), array(), "array"), "class" => "form-control", "id" => "armstrong_2_distributor"));
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_distributors_id\" id=\"armstrong_2_distributors_id\" value=\"";
        // line 70
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_distributors_id"]) ? $context["armstrong_2_distributors_id"] : null), "html", null, true);
        echo "\">
                    </div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 76
        echo form_label("Wholesaler ID", "wholesaler", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 78
        echo form_input(array("name" => "", "value" => (isset($context["armstrong_2_wholesaler_id"]) ? $context["armstrong_2_wholesaler_id"] : null), "class" => "form-control", "id" => "armstrong_2_wholesaler"));
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_wholesalers_id\" id=\"armstrong_2_wholesalers_id\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["armstrong_2_wholesalers_id"]) ? $context["armstrong_2_wholesalers_id"] : null), "html", null, true);
        echo "\">
                    </div>
                </div>
            </div>
            ";
        // line 83
        if (((isset($context["country_id"]) ? $context["country_id"] : null) == 2)) {
            // line 84
            echo "                <div class=\"col-md-12 form-group\">
                    <div class=\"form-group\">
                        ";
            // line 86
            echo form_label("Call Record", "call_record", array("class" => "control-label col-md-3"));
            echo "
                        <div class=\"col-md-6 no-parsley\">
                            ";
            // line 88
            echo form_input(array("name" => "armstrong_2_call_record", "value" => "", "class" => "form-control", "id" => "armstrong_2_call_records_id"));
            echo "
                            <input type=\"hidden\" name=\"armstrong_2_call_records_id\" id=\"armstrong_2_call_records_id\" value=\"";
            // line 89
            echo twig_escape_filter($this->env, (isset($context["armstrong_2_call_records_id"]) ? $context["armstrong_2_call_records_id"] : null), "html", null, true);
            echo "\">
                        </div>
                    </div>
                </div>
            ";
        }
        // line 94
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["product"]) {
            // line 95
            echo "            <div class=\"col-md-12 form-group sku-base\">
                <div class=\"form-group\">
                    ";
            // line 97
            echo form_label("Product", "product", array("class" => "control-label col-md-3"));
            echo "
                    <div class=\"col-md-6\">
                        ";
            // line 99
            echo form_input(array("name" => "", "value" => (($this->getAttribute($context["product"], "sku_number", array()) . " - ") . $this->getAttribute($context["product"], "sku_name", array())), "class" => "form-control", "id" => "product"));
            echo "
                        <input type=\"hidden\" value=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
            echo "\" name=\"product[";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_num]\">
                    </div>
                    <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                        Quantity Per Case ";
            // line 103
            echo form_input(array("name" => (("product[" . $context["key"]) . "][quantity_case]"), "value" => $this->getAttribute($context["product"], "quantity_case", array()), "class" => "form-control sub-input qty-case", "id" => "quantity_case", "readonly" => "readonly"));
            echo "
                        Price ";
            // line 104
            echo form_input(array("name" => (("product[" . $context["key"]) . "][price]"), "value" => $this->getAttribute($context["product"], "price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_product_price", "readonly" => "readonly"));
            echo " &nbsp;
                    </div>
                    <div class=\"col-md-offset-3 col-md-9 m-t no-parsley\">
                        Quantity
                        ";
            // line 108
            if (($this->getAttribute($context["product"], "package_type", array()) == "Case")) {
                // line 109
                echo "                            ";
                echo form_input(array("name" => (("product[" . $context["key"]) . "][qty_case]"), "value" => $this->getAttribute($context["product"], "qty_case", array()), "class" => "form-control sub-input qty-case", "id" => "qty_case", "readonly" => "readonly"));
                echo "
                        ";
            } else {
                // line 111
                echo "                            ";
                echo form_input(array("name" => (("product[" . $context["key"]) . "][qty_piece]"), "value" => $this->getAttribute($context["product"], "qty_piece", array()), "class" => "form-control sub-input qty-case", "id" => "qty_pcs", "readonly" => "readonly"));
                echo "
                        ";
            }
            // line 113
            echo "                        ";
            echo form_input(array("name" => (("product[" . $context["key"]) . "][package_type]"), "value" => $this->getAttribute($context["product"], "package_type", array()), "class" => "form-control sub-input subtotal", "id" => "package_type", "readonly" => "readonly"));
            echo "
                        Sub Total ";
            // line 114
            echo form_input(array("name" => (("product[" . $context["key"]) . "][total_price]"), "value" => $this->getAttribute($context["product"], "total_price", array()), "class" => "form-control sub-input subtotal", "id" => "armstrong_total_price", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 121
        echo form_label("Total Order", "total_order", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 123
        echo form_input(array("name" => "total", "value" => (isset($context["total"]) ? $context["total"] : null), "class" => "form-control", "id" => "total", "readonly" => "true"));
        echo "
                    </div>
                </div>
            </div>
            <div class=\"col-md-12 form-group\">
                <div class=\"form-group\">
                    ";
        // line 129
        echo form_label("Order Date", "order_date", array("class" => "control-label col-md-3"));
        echo "
                    <div class=\"col-md-6 no-parsley\">
                        ";
        // line 131
        echo form_input(array("name" => "order_date", "value" => (isset($context["order_date"]) ? $context["order_date"] : null), "class" => "form-control", "id" => "order_date", "readonly" => "true"));
        echo "
                    </div>
                </div>
            </div>
            ";
        // line 135
        echo form_close();
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/webshop/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 135,  306 => 131,  301 => 129,  292 => 123,  287 => 121,  283 => 119,  272 => 114,  267 => 113,  261 => 111,  255 => 109,  253 => 108,  246 => 104,  242 => 103,  234 => 100,  230 => 99,  225 => 97,  221 => 95,  216 => 94,  208 => 89,  204 => 88,  199 => 86,  195 => 84,  193 => 83,  186 => 79,  182 => 78,  177 => 76,  168 => 70,  164 => 69,  159 => 67,  151 => 62,  146 => 60,  141 => 58,  133 => 53,  129 => 51,  126 => 49,  121 => 47,  106 => 35,  102 => 34,  95 => 29,  93 => 28,  83 => 22,  80 => 21,  74 => 18,  69 => 17,  66 => 16,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  43 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
