<?php

/* ami/free_gifts/edit.html.twig */
class __TwigTemplate_525c019ec3224a12e25afd337aa7db3d5f73f089252af3ba8653ebc8188767f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/free_gifts/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 10
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#expiry_date').datetimepicker({
              //  format: 'YYYY-MM-DD HH:II:ss'
            });
        });
    </script>
";
    }

    // line 20
    public function block_css($context, array $blocks = array())
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 25
    public function block_content($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        echo form_open(site_url("ami/free_gifts/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Free Gift</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 35
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 37
        echo "                        ";
        echo html_btn(site_url("ami/free_gifts"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 38
        if ($this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "id", array())) {
            // line 39
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/free_gifts/delete/" . $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 42
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 52
        echo form_label("Expiry Date", "expiry_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5 ";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "expiry_date", array()), "html", null, true);
        echo "\">
                    ";
        // line 54
        echo form_input(array("name" => "expiry_date", "value" => $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "expiry_date", array()), "id" => "expiry_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 58
        echo form_label("App type", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 60
        echo form_dropdown("app_type", (isset($context["apps"]) ? $context["apps"] : null), $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "app_type", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 64
        echo form_label("Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 66
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 71
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 73
        echo form_radio("active", 0, ($this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "active", array()) == 0));
        echo " No</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 74
        echo form_radio("active", 1, ($this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "active", array()) == 1));
        echo " Yes</label>&nbsp;
                </div>
            </div>
            ";
        // line 77
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["free_gifts"]) ? $context["free_gifts"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 82
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/free_gifts/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 82,  183 => 77,  177 => 74,  173 => 73,  168 => 71,  160 => 66,  155 => 64,  148 => 60,  143 => 58,  136 => 54,  132 => 53,  128 => 52,  116 => 42,  109 => 39,  107 => 38,  102 => 37,  98 => 35,  85 => 26,  82 => 25,  76 => 22,  71 => 21,  68 => 20,  54 => 10,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
