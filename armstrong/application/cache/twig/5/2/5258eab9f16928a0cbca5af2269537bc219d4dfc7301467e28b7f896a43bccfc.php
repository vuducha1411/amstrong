<?php

/* ami/promotions/index.html.twig */
class __TwigTemplate_5258eab9f16928a0cbca5af2269537bc219d4dfc7301467e28b7f896a43bccfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotions/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('.dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/promotions/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        \"iDisplayLength\": 20
    });
});
</script>
";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
        // line 32
        echo "
    ";
        // line 33
        echo form_open(site_url("ami/promotions/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "
    
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 38
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Promotions Draft") : ("Promotions"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 41
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 43
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/promotions/index.html.twig", 43)->display(array_merge($context, array("url" => "ami/promotions", "icon" => "fa-barcode", "title" => "Promotions", "permission" => "promotions")));
        // line 44
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
           
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\"><a href=\"#listed\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Listed</a></li>
                <li role=\"presentation\"><a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Delisted</a></li>
            </ul>

            <div class=\"panel panel-default m-t\"> 
                                
                <div class=\"panel-body tab-content\">                
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 66
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions"))) {
            // line 67
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>                                    
                                    ";
        }
        // line 69
        echo "                                    <th>SKU name</th>
                                    <th>SKU number</th>
                                    <th>App type</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Updated By</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 79
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 80
            echo "                                    <tr>
                                        ";
            // line 81
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions"))) {
                // line 82
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 84
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/promotions/edit/" . $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "sku_name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim(((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ($this->getAttribute($context["post"], "name_alt", array())) : ($this->getAttribute($context["post"], "sku_name", array()))), 50);
            echo "</a></td>
                                        <td>";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "sku_number", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 86
            echo ((($this->getAttribute($context["post"], "app_type", array()) == 0)) ? ("PULL") : (((($this->getAttribute($context["post"], "app_type", array()) == 1)) ? ("PUSH") : ("ALL"))));
            echo "</td>
                                        <td class=\"center\">";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 89
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["post"], "updated_name", array()) != "")) ? ($this->getAttribute($context["post"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 91
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotions")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions")))) {
                // line 92
                echo "                                                ";
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/promotions/index.html.twig", 92)->display(array_merge($context, array("url" => "ami/promotions", "id" => $this->getAttribute($context["post"], "id", array()), "permission" => "promotions")));
                // line 93
                echo "                                            ";
            }
            // line 94
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "                            </tbody>
                        </table>                        
                    </div>

                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <table class=\"table table-striped table-bordered table-hover dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 105
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions"))) {
            // line 106
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 108
        echo "                                    <th>SKU name</th>
                                    <th>SKU number</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Updated By</th>
                                    ";
        // line 113
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotions")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions")))) {
            // line 114
            echo "                                        <th class=\"nosort text-center\"></th>
                                    ";
        }
        // line 116
        echo "                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 119
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "inactive", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 120
            echo "                                    <tr>
                                        ";
            // line 121
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions"))) {
                // line 122
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["post"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 124
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/promotions/edit/" . $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "sku_name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo wordTrim(((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ($this->getAttribute($context["post"], "name_alt", array())) : ($this->getAttribute($context["post"], "sku_name", array()))), 50);
            echo "</a></td>
                                        <td>";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "sku_number", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 128
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["post"], "updated_name", array()) != "")) ? ($this->getAttribute($context["post"], "updated_name", array())) : ("OPS")), "html", null, true);
            echo "</td>
                                        ";
            // line 129
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "promotions")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "promotions")))) {
                // line 130
                echo "                                            <td class=\"center text-center\" style=\"min-width: 80px;\">
                                                ";
                // line 131
                $this->loadTemplate("ami/components/table_btn.html.twig", "ami/promotions/index.html.twig", 131)->display(array_merge($context, array("url" => "ami/promotions", "id" => $this->getAttribute($context["post"], "id", array()), "permission" => "promotions")));
                // line 132
                echo "                                            </td>
                                        ";
            }
            // line 134
            echo "                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "                            </tbody>
                        </table>                        
                    </div>
                </div>            
            </div>
            
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 146
        echo form_close();
        echo "
    
";
    }

    public function getTemplateName()
    {
        return "ami/promotions/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 146,  344 => 136,  329 => 134,  325 => 132,  323 => 131,  320 => 130,  318 => 129,  314 => 128,  310 => 127,  306 => 126,  302 => 125,  293 => 124,  287 => 122,  285 => 121,  282 => 120,  265 => 119,  260 => 116,  256 => 114,  254 => 113,  247 => 108,  243 => 106,  241 => 105,  231 => 97,  215 => 94,  212 => 93,  209 => 92,  207 => 91,  202 => 89,  198 => 88,  194 => 87,  190 => 86,  186 => 85,  177 => 84,  171 => 82,  169 => 81,  166 => 80,  149 => 79,  137 => 69,  133 => 67,  131 => 66,  107 => 44,  105 => 43,  100 => 41,  94 => 38,  86 => 33,  83 => 32,  80 => 31,  74 => 28,  70 => 27,  67 => 26,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
