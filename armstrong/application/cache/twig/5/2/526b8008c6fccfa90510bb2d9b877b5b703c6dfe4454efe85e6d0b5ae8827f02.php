<?php

/* ami/demo/index.html.twig */
class __TwigTemplate_526b8008c6fccfa90510bb2d9b877b5b703c6dfe4454efe85e6d0b5ae8827f02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ami/demo/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Reports Penetration";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/jquery.storageapi.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/plugins/slimscroll/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("res/js/plugins/metisMenu/metisMenu.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootbox/bootbox.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("res/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("res/js/plugins/parsley/parsley.remote.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\"
            src=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url("res/js/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("res/js/custom_daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, site_url("res/js/date.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("res/js/generateDataReport_demo.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("res/js/generateChartReport.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, site_url("res/js/generateHtmlReport.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("res/js/jquery.multiple.select.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("res/js/jquery.sumoselect.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        var BASE_URL = '";
        // line 32
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "',
                CURRENT_URL = '";
        // line 33
        echo twig_escape_filter($this->env, current_url(), "html", null, true);
        echo "';
        function base_url() {
            return BASE_URL;
        }
        function current_url() {
            return CURRENT_URL;
        }
    </script>

";
    }

    // line 44
    public function block_js($context, array $blocks = array())
    {
        // line 45
        echo "    <script>

        function getInternetExplorerVersion()
        // Returns the version of Internet Explorer or a -1
        // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp(\"MSIE ([0-9]{1,}[\\.0-9]{0,})\");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.\$1);
            }
            // 7 - ie7
            // 8 - ie 8
            // 9 -ie 9
            // 10 -ie 10
            //>-1 >10
            return rv;
        }
        function checkVersion() {
            var msg = \"You're not using Internet Explorer.\";
            var ver = getInternetExplorerVersion();

            if (ver > -1) {
                if (ver >= 8.0)
                    msg = \"You're using a recent copy of Internet Explorer.\";
                else
                    msg = \"You should upgrade your copy of Internet Explorer.\";
            }
            alert(msg + ' ' + ver);
        }

        function zoom_report() {
            \$('#ReportsGeneration').toggle();
            \$(\"#reporting-view #iframe_report\").toggleClass('height_zoom');
        }
        function get_sale(value) {
            \$('#armstrong_2_salespersons_id').html('');
            \$.ajax({
                type: 'POST',
                url: base_url() + 'ami/demo/penetration_customers_report/' + value,
                data: {},
                success: function (val) {
                    \$('#armstrong_2_salespersons_id').html(val);
                }
            })

        }

        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        function validateMultiSelect(selector) {
            var options_selected = selector.find('option:selected');
            if (options_selected.length > 0) {
                selector.closest('div').find('.ms-parent').removeClass('has-error');
            } else {
                selector.closest('div').find('.ms-parent').addClass('has-error');
            }
        }

        function resetText(selector, val) {
            var label = selector.closest('.SumoSelect').find('.optWrapper li[data-val=\"' + val + '\"]').text(),
                    captionCont = selector.closest('.SumoSelect').find('.CaptionCont');
            captionCont.find('span').text(label);
        }

        \$(document).ready(function () {
            \$('#InputDateRange').daterangepicker({
                \"showDropdowns\": true,
                \"maxDate\": moment(),
                \"startDate\": \"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "html", null, true);
        echo "\",
                format: 'YYYY-MM-DD'
            }).on('apply.daterangepicker', function (ev, picker) {
                \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD')).trigger('change');
                \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD')).trigger('change');
            });

//            \$(document).on('change', 'select.monthselect', function(){
//                var selected = \$(this).find('option:selected').text();
//                \$(this).find('option:selected').text('<span>' +selected+'</span>');
//            });
            \$('select.multi_select').multipleSelect({placeholder: \"Select here\"});
            \$('.select').SumoSelect();
            \$('div.ms-drop ul').slimScroll();
            var \$val = \$('select[name=\"view_by\"]').val(),
                    channel = \$('.channelWrap'),
                    customers = \$('.gripCustomerWrap'),
                    otm = \$('.otmWrap'),
                    salesLeaders = \$('.slWrap'),
                    salespersons = \$('.spWrap'),
                    rolling = \$('.rolling'),
                    multi_rolling = \$('.multi_rolling');
            switch (\$val) {
                case '0':
                    channel.hide();
                    otm.hide();
                    rolling.hide();
                    salesLeaders.hide();
                    salespersons.hide();
                    customers.show();
                    multi_rolling.show();
                    break;
                case '1':
                    salesLeaders.hide();
                    salespersons.hide();
                    rolling.hide();
                    channel.show();
                    customers.show();
                    otm.show();
                    multi_rolling.show();
                    break;
                case '2':
                    rolling.hide();
                    channel.hide();
                    customers.hide();
                    otm.hide();
                    multi_rolling.hide();
                    salesLeaders.show();
                    salespersons.show();
                    break;
                case '3':
                case '4':
                    salesLeaders.hide();
                    salespersons.hide();
                    multi_rolling.hide();
                    customers.show();
                    otm.show();
                    channel.show();
                    rolling.show();
                    break;
                case '5':
                    rolling.hide();
                    channel.hide();
                    otm.hide();
                    multi_rolling.hide();
                    customers.show();
                    salesLeaders.show();
                    salespersons.show();
                    rolling.show();
                    break;
                case '6':
                    multi_rolling.hide();
                    rolling.show();
                    channel.show();
                    customers.show();
                    otm.show();
                    salesLeaders.show();
                    salespersons.show();
                    rolling.show();
                default:
                    break;
            }

        });

        \$(function () {
            var d = new Date(),
                    cr_month = d.getMonth() + 1,
                    cr_year = d.getFullYear();
            var action = '";
        // line 209
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "';
            var urlGetReport = '";
        // line 210
        echo twig_escape_filter($this->env, (isset($context["reportEndpoint"]) ? $context["reportEndpoint"] : null), "html", null, true);
        echo "';
            \$('select[name=\"product_type\"]').on('change', function () {
                var \$val = \$(this).val(),
                        products = \$('.topProduct'),
                        otherProducts = \$('.otherProduct');
                if (\$val == 0) {
                    products.show();
                    otherProducts.hide();
                } else {
                    products.hide();
                    otherProducts.show();
                }
            });

            \$('.reset').on('click', function () {
                var appTypeElement = \$('#input-app-type'),
                        monthElement = \$('#input-month'),
                        yearElement = \$('#input-year'),
                        salesLeaderElement = \$('#SalesLeaderSelect'),
                        channelElement = \$('#ChannelSelect'),
                        customerElement = \$('#gripCustomers'),
                        otmElement = \$('#InputOTM'),
                        dateRangeElement = \$('#rolling'),
                        multiDateRangeElement = \$('#multi_rolling');

                appTypeElement.val('PULL');
                monthElement.val(cr_month);
                yearElement.val(cr_year);
                customerElement.val(1);
                dateRangeElement.val(2);
                multiDateRangeElement.multipleSelect(\"setSelects\", [2]);
                salesLeaderElement.multipleSelect(\"checkAll\");
                channelElement.multipleSelect(\"checkAll\");
                otmElement.multipleSelect(\"checkAll\");

                resetText(appTypeElement, 'PULL');
                resetText(monthElement, cr_month);
                resetText(yearElement, cr_year);
                resetText(customerElement, 1);
                resetText(dateRangeElement, 2);
                \$('#input-month, .fetch-sl').trigger('change');
            });

            \$('select[name=\"view_by\"]').on('change', function (e) {
                var \$val = \$(this).val(),
                        channel = \$('.channelWrap'),
                        customers = \$('.gripCustomerWrap'),
                        otm = \$('.otmWrap'),
                        salesLeaders = \$('.slWrap'),
                        salespersons = \$('.spWrap'),
                        rolling = \$('.rolling'),
                        multi_rolling = \$('.multi_rolling');

                switch (\$val) {
                    case '0':
                        channel.hide();
                        otm.hide();
                        rolling.hide();
                        salesLeaders.hide();
                        salespersons.hide();
                        customers.show();
                        multi_rolling.show();
                        break;
                    case '1':
                        salesLeaders.hide();
                        salespersons.hide();
                        rolling.hide();
                        channel.show();
                        customers.show();
                        otm.show();
                        multi_rolling.show();
                        break;
                    case '2':
                        rolling.hide();
                        channel.hide();
                        customers.hide();
                        otm.hide();
                        multi_rolling.hide();
                        salesLeaders.show();
                        salespersons.show();
                        break;
                    case '3':
                    case '4':
                        salesLeaders.hide();
                        salespersons.hide();
                        multi_rolling.hide();
                        customers.show();
                        otm.show();
                        channel.show();
                        rolling.show();
                        break;
                    case '5':
                        rolling.hide();
                        channel.hide();
                        otm.hide();
                        multi_rolling.hide();
                        customers.show();
                        salesLeaders.show();
                        salespersons.show();
                        rolling.show();
                        break;
                    case '6':
                        multi_rolling.hide();
                        rolling.show();
                        channel.show();
                        customers.show();
                        otm.show();
                        salesLeaders.show();
                        salespersons.show();
                        rolling.show();
                    default:
                        break;
                }

            });
            ";
        // line 325
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 326
            echo "            \$('#input-month, #input-year, #input-app-type').on('change', function (e) {
                var \$this = \$(this),
                        month = \$('#input-month').val(),
                        year = \$('#input-year').val(),
                        app_type = \$('#input-app-type').val(),
                        countryID = ";
            // line 331
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
            echo ",
                        productWrap = \$('#top-products-sku'),
                        otherProductWrap = \$('#other-products-sku');
                \$.ajax({
                    url: \"";
            // line 335
            echo twig_escape_filter($this->env, site_url("ami/products/getTopProducts"), "html", null, true);
            echo "\",
                    data: {month: month, year: year, country_id: countryID, app_type: app_type},
                    type: 'POST',
                    dataType: 'json',
                    success: function (res) {
                        var productWrapper = productWrap.closest('div').find('.ms-drop.bottom ul'),
                                otherProductWrapper = otherProductWrap.closest('div').find('.ms-drop.bottom ul');
                        productWrap.html(res.topProducts);
                        otherProductWrap.html(res.otherProducts);

                        productWrapper.remove();
                        otherProductWrapper.remove();
                        productWrap.multipleSelect({placeholder: \"Select here\"});
                        otherProductWrap.multipleSelect({placeholder: \"Select here\"});
                        \$('div.ms-drop ul').slimScroll();
                    }
                });

            });
            //\$('#input-month').trigger('change');
            ";
        }
        // line 356
        echo "
            \$('#multiCountriesList').on('change', function (e) {
                var \$this = \$(this),
                        \$val = \$this.val(),
                        option = \$this.find('option');
                if (\$val == 0) {
                    \$.each(option, function () {
                        var op_val = \$(this).val();
                        \$val.push(op_val);
                    });
                    \$val.splice(0, 2);
                }
                \$this.val(\$val);
            });

            \$('#gripCustomers').on('change', function () {
                var \$val = \$(this).val();
                if (\$val == 1) {
                    var unassigned = '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                    \$('#armstrong_2_salespersons_id').find('option').end().append(unassigned);
                } else {
                    \$('#armstrong_2_salespersons_id').find('option[value=\"unassigned\"]').remove();
                }
            });
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 388
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + toTitleCase(res['value'][key]) + '\">' + toTitleCase(res['value'][key]) + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            \$('.select-change').trigger('change');

            var salesPersonSelect = \$('.SalespersonsSelect').html();
            var salesLeaderSelect = \$('#SalesLeaderSelect').html();

            \$('#SalesLeaderSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        sub_type = optionSelected.data('sub_type'),
                        \$target = \$('select.SalespersonsSelect'),
                        val = \$this.val();

//            var list = '<option value=\"\" selected=\"selected\">ALL</option>';
                var list = '';

                if (\$.inArray('ALL', val) !== -1) {
                    \$target.html(salesPersonSelect).multipleSelect(\"refresh\");
                    return;
                }

                \$target.html(salesPersonSelect).find('option').each(function () {
                    var \$option = \$(this);
                    slId = \$option.data('manager') || 0;

                    if (\$.inArray(slId, val) !== -1) {
                        list += \$option.prop('outerHTML');
                    }
                });
                list += '<option value=\"unassigned\" data-manager=\"\">Unassigned</option>';
                \$target.html(list).multipleSelect(\"refresh\");
                \$('div.SalespersonsSelect ul').remove();
                \$target.multipleSelect(\"refresh\");
//            \$('select.SalespersonsSelect').multipleSelect({placeholder: \"Select here\"});
                var arr_sub_type = [];
                \$.each(optionSelected, function () {
                    var sub_type = \$(this).data('sub_type');
                    arr_sub_type.push(sub_type);
                    if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                        \$target.html(salesPersonSelect).multipleSelect(\"refresh\");

                        return;
                    }
                });
                \$('input[name=\"sub_type\"]').val(arr_sub_type);
            });

            \$('.fetch-sl').on('change', function () {
                var \$appType = \$('#input-app-type'),
                        \$slStatus = \$('#input-sl-status'),
                        salesleader = \$('#SalesLeaderSelect').val() || '',
                        appType = \$appType.find(':selected').val() || '',
                        slStatus = \$slStatus.find(':selected').val() || '',
                        season = \$('select[name=\"season\"]').val(),
                        date_from = \$('input[name=\"from\"]').val(),
                        date_to = \$('input[name=\"to\"]').val(),
                        view_by = \$('select[name=\"view_by\"]').val(),
                        salespersonAll = \$('input[name=\"selectAllsales_leader[]\"]:checked').val();
                var range = (view_by == 0 || view_by == 1) ? 0 : \$('select[name=\"rolling\"]').val();
                if (season) {
                    var split_season = season.split('.');
                    date_from = split_season[0];
                    date_to = split_season[1];
                }

                \$('#armstrong_2_salespersons_id').html('');
                month = \$('#input-month').find(':selected').val();
                year = \$('#input-year').find(':selected').val();
                if (month === undefined) month = 0;
                if (year === undefined) year = 0;
                \$.ajax({
                    type: 'POST',
                    url: base_url() + 'ami/salespersons/fetch_salesperson_demo',
                    dataType: 'html',
                    data: {
                        app_type: appType,
                        active: slStatus,
                        salespersons_manager_id: salesleader,
                        sales_all: salespersonAll,
                        date_from: date_from,
                        date_to: date_to,
                        month: month,
                        year: year,
                        range: range
                    },
                    success: function (val) {
                        salesLeaderSelect = val.salesleader;
                        salesPersonSelect = val;

                        \$('#SalesLeaderSelect').html(salesLeaderSelect);
                        \$('div.SalesLeaderSelect ul').remove();
                        \$('#SalesLeaderSelect').multipleSelect('refresh');

                        \$('#armstrong_2_salespersons_id').html(val);
                        \$('div.SalespersonsSelect ul').remove();
                        \$('#armstrong_2_salespersons_id').multipleSelect('refresh');
                    }
                });
            });

            \$('.fetch-sl').trigger('change');

            \$('.btn-clear').click(function (e) {
                e.preventDefault();
                \$(this).closest('.form-group').find(':text').val('');
            });

            var \$iFrame = \$('#iframe_report');
            var \$iFrame_zoom = \$('#iframe_report_zoom');
            // \$('#iframe_report').hide();
            // \$('#zoom_rp').hide();

            \$iFrame.load(function () {
                \$('#reporting-view .loading').html('');
                \$(\"#iframe_report\").show();
                \$(\"#zoom_rp\").show();
            });

            \$('form').on('submit', function (e) {
                if (\$(this).find('input[name=\"from\"]').val() === '' || \$(this).find('input[name=\"to\"]').val() === '')
                    e.preventDefault();
            });

            /*\$('button[type=\"reset\"]').click(function(){
             \$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/report_tfo');
             });*/

            /*\$('select#inputTable').on('change', function(){
             window.location = base_url() + 'management/reports?type=' + \$(this).val().replace('report_', '');;
             //\$(\"form#ReportsGeneration\").attr(\"action\", base_url() + 'manual_reports/' + \$(this).val());
             });

             \$('select#inputTable ul li a').on('click', function(){
             var mode =\$(this).attr('href').replace('<?=base_url()?>management/reports?type=', '');
             alert(mode);
             });*/

//            \$('button[type=\"reset\"]').on('click', function(){
//
//                var view_by = \$('select[name=\"view_by\"]');
//                var first_option = view_by.find('option[value=0]').text();
//                var view_text = view_by.closest('div').find('.CaptionCont>span');
//                view_by.val(0);
//                view_text.text(first_option);
//                view_by.trigger('change');
//            });
            \$('button.btn-export-report:not(.disabled)').click(function (e) {
                var otmElement = \$('#InputOTM'),
                        channelElement = \$('#ChannelSelect'),
                        spElement = \$('#armstrong_2_salespersons_id'),
                        slElement = \$('#SalesLeaderSelect'),
                        topProduct = \$('#top-products-sku'),
                        otherProduct = \$('#other-products-sku'),
                        view_by = \$('select[name=\"view_by\"]').val(),
                        month = \$('select[name=\"month\"]').val(),
                        year = \$('select[name=\"year\"]').val(),
                        multi_rolling = \$('#multi_rolling'),
                        type = \$('select[name=\"type\"]').val(),
                        app_type = \$('select[name=\"app_type\"]').val(),
                        from = \$('select[name=\"from\"]').val(),
                        to = \$('select[name=\"to\"]').val(),
                        otm = \$('select[name=\"otm[]\"]').val(),

                        denominator = \$('select[name=\"customers\"]').val(),
                        country_list = \$('select[name=\"country_list\"]').val();

                if (cr_year == year && cr_month < month) {
                    \$('select[name=\"month\"]').closest('div').find('p').addClass('has-error');
                } else {
                    \$('select[name=\"month\"]').closest('div').find('p').removeClass('has-error');
                }
                if (otmElement.length > 0) {
                    validateMultiSelect(otmElement);
                }
                if (channelElement.length > 0) {
                    validateMultiSelect(channelElement);
                }
                if (spElement.length > 0) {
                    validateMultiSelect(spElement);
                }
                if (slElement.length > 0) {
                    validateMultiSelect(slElement);
                }
                if (topProduct.length > 0) {
                    validateMultiSelect(topProduct);
                }
                if (otherProduct.length > 0) {
                    validateMultiSelect(otherProduct);
                }
                if (multi_rolling.length > 0) {
                    validateMultiSelect(multi_rolling);
                }
                // TODO validate multi select
                if (topProduct.closest('div').find('.has-error').length > 0 && otherProduct.closest('div').find('.has-error').length > 0) {
                    e.preventDefault();
                    return false;
                }

                if (topProduct.closest('div').find('.has-error').length > 0 || otherProduct.closest('div').find('.has-error').length > 0) {
                    topProduct.closest('div').find('.ms-parent').removeClass('has-error');
                    otherProduct.closest('div').find('.ms-parent').removeClass('has-error');
                }

                if (cr_year == year && cr_month < month) {
                    e.preventDefault();
                    return false;
                }

                if (view_by == 0 || view_by == 1) {
                    if (multi_rolling.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                if (view_by == 1 || view_by == 3) {
                    if (otmElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 2 || view_by == 5) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 6) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0 || otmElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

            });

            \$('button.btn-view-report:not(.disabled)').click(function (e) {
                e.preventDefault();
                var mode = '";
        // line 650
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "',
                        spElement = \$('#armstrong_2_salespersons_id'),
                        getall = 0,
                        sp = 0,
                        view_by = \$('select[name=\"view_by\"]').val(),
                        view_type = \$('select[name=\"view_type\"]').val(),
                        slElement = \$('#SalesLeaderSelect'),
                        sl = null,
                        sub_type = null,
                        rolling = \$('select[name=\"rolling\"]').val(),
                        multi_rolling = \$('#multi_rolling'),
                        from = \$('input[name=\"from\"]').val(),
                        to = \$('input[name=\"to\"]').val(),
                        salespersonsActive = \$('#input-sl-status').val(),
                        promotion_activity = \$('#promotion_name').val(),
                        global_channels = \$('select[name=\"global_channels\"]').val();
                if (slElement.length > 0) {

                    sl = slElement.val();
                    if (\$('input[name=\"selectAllsales_leader[]\"]').prop('checked')) {
                        sl = 'ALL';
                    } else {
                        sl = '';
                    }
                    var sl_op_selected = slElement.find('option:selected');
                    if (sl_op_selected.length > 0) {
                        slElement.closest('div').find('.ms-parent').removeClass('has-error');
                        \$.each(sl_op_selected, function () {
                            sub_type = \$(this).data('sub_type');
                            if (\$.inArray(sub_type, ['nsm', 'md', 'cdops']) !== -1) {
                                sl = null;
                                return false;
                            }
                        });
                    } else {
                        slElement.closest('div').find('.ms-parent').addClass('has-error');
                    }
                }
                if (spElement.length > 0) {
                    var sp_val = spElement.val();
                    if (\$('input[name=\"selectAllarmstrong_2_salespersons_id[]\"]').prop('checked')) {
                        sp_val = 'ALL';
                    } else {
                        sp_val = '';
                    }

                    if (sp_val == 'ALL') {
                        if (sl == 'ALL') {
                            getall = 1;
                            sp = 0;
                        } else {
                            getall = 0;
                            sp = spElement.val();
                        }
                    } else {
                        getall = 0;
                        sp = spElement.val();
                    }
                    validateMultiSelect(spElement);

                }
                var otmElement = \$('#InputOTM');
                var otm = '';
                if (otmElement.length > 0) {
                    otm = otmElement.val();
                    validateMultiSelect(otmElement);
                }

                var channelElement = \$('#ChannelSelect');
                var channel = '';
                if (channelElement.length > 0) {
                    channel = channelElement.val();
                    validateMultiSelect(channelElement);
                }

                if (\$('input[name=\"from\"]').length > 0) {
                    var fromdate = \$('input[name=\"from\"]').val() + ' 00:00:00';
                }
                if (\$('input[name=\"to\"]').length > 0) {
                    var todate = \$('input[name=\"to\"]').val() + ' 23:59:59';
                }
                if (\$('select[name=\"number_sku\"]').length > 0) {
                    var number_sku = \$('select[name=\"number_sku\"]').val();
                }

                var topProductElement = \$('#top-products-sku');
                var otherProductElement = \$('#other-products-sku');
                if (topProductElement.length > 0 && otherProductElement.length > 0) {
                    var top_products = topProductElement.val(),
                            other_products = otherProductElement.val();
                    if (!top_products && other_products) {
                        top_products = other_products;
                    }
                    else if (top_products && other_products) {
                        top_products = \$.merge(top_products, other_products);
                    }
                    validateMultiSelect(topProductElement);
                    validateMultiSelect(otherProductElement);
                }

                if (\$('select[name=\"country_list\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list\"]').val();
                    var countryId = country_list;
                } else if (\$('select[name=\"country_list[]\"]').length > 0) {
                    var country_list = \$('select[name=\"country_list[]\"]').val();
                    var countryId = country_list;
//                }
//                if(\$.isArray(countryId)){
//                    countryId = countryId.join('&country=');
                } else {
                    var countryId = '";
        // line 760
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), "html", null, true);
        echo "';
                }

                if (\$('select[name=\"app_type\"]').length > 0) {
                    var app_type = \$('select[name=\"app_type\"]').val();
                }

                if (\$('select[name=\"active\"]') != null) {
                    var active = \$('select[name=\"active\"]').val();
                }

                if (view_by == 0 || view_by == 1) {
                    var range = multi_rolling.val();
                    validateMultiSelect(multi_rolling);
                } else {
                    var range = \$('select[name=\"rolling\"]').val();
                }

                if (\$('select[name=\"season\"]').length > 0) {
                    var season = \$('select[name=\"season\"]').val();
                    var season_arr = season.split('.');
                    fromdate = season_arr['0'] + ' 00:00:00';
                    todate = season_arr['1'] + ' 23:59:59';
                }

                if (\$('#gripCustomers').length > 0) {
                    var isGripCustomer = \$('#gripCustomers').val();
                }
                // var fromdate = '2014-05-01 00:00:01';
                // var todate = '2014-05-31 23:59:59';

                var elementMoth = \$('#input-month');
                var elementYear = \$('#input-year');
                if (elementMoth.length > 0 && elementYear.length > 0) {
                    var year = elementYear.val();
                    var month = elementMoth.val();
                    var dateStartMonth = year + '-' + month + '-01 00:00:00';
                    var lastday = (new Date(year, month, 0)).getDate();
                    var dateEndMonth = year + '-' + month + '-' + lastday + ' 23:59:59';
                    if (cr_year == year && cr_month < month) {
                        elementMoth.closest('div').find('p').addClass('has-error');
                    } else {
                        elementMoth.closest('div').find('p').removeClass('has-error');
                    }
                }
                var sp_too = (typeof sp !== 'undefined') ? \$('select[name=\"armstrong_2_salespersons_id\"] option[value=\"' + sp + '\"]').html() : \"";
        // line 805
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "name", array()), "html", null, true);
        echo " [";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["session"]) ? $context["session"] : null), "id", array()), "html", null, true);
        echo "]\";


                var isMultiCountry = 0;
                var regional = 0;
                // grip grab filter
                var filters = [];
                if (\$('select[name=\"filter[]\"]').length > 0) {
                    filters = \$('select[name=\"filter[]\"]').val();
                }

                ";
        // line 816
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 817
            echo "
                if (view_by == 0) {
                    channel = -1;
                } else if (view_by == 1) {
                    sp = 0;
                }
                ";
        }
        // line 824
        echo "
                ";
        // line 825
        if (((isset($context["filter"]) ? $context["filter"] : null) == "regional")) {
            // line 826
            echo "                regional = 1;
                getall = 1;
                sp = 0;
                channel = -1;
                otm = 'ALL';
                if (\$('select[name=\"country_list[]\"]').length > 0) {
                    isMultiCountry = 1;
                }
                ";
        }
        // line 835
        echo "
                // TODO validate multi select
                if (topProductElement.closest('div').find('.has-error').length > 0 && otherProductElement.closest('div').find('.has-error').length > 0) {
                    e.preventDefault();
                    return false;
                }

                if (topProductElement.closest('div').find('.has-error').length > 0 || otherProductElement.closest('div').find('.has-error').length > 0) {
                    topProductElement.closest('div').find('.ms-parent').removeClass('has-error');
                    otherProductElement.closest('div').find('.ms-parent').removeClass('has-error');
                }

                if (cr_year == year && cr_month < month) {
                    e.preventDefault();
                    return false;
                }

                if (view_by == 0 || view_by == 1) {
                    if (multi_rolling.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                if (view_by == 1 || view_by == 3) {
                    if (otmElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 2 || view_by == 5) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                } else if (view_by == 6) {
                    if (slElement.closest('div').find('.has-error').length > 0 || spElement.closest('div').find('.has-error').length > 0 || channelElement.closest('div').find('.has-error').length > 0 || otmElement.closest('div').find('.has-error').length > 0) {
                        e.preventDefault();
                        return false;
                    }
                }

                // set local storage
                var tb_ns = \$.initNamespaceStorage('tb_ns');
                var storage = tb_ns.localStorage;
                storage.set({
                    'urlGetReport': urlGetReport,
                    'sp': sp,
                    'fromdate': fromdate,
                    'todate': todate,
                    'countryId': countryId,
                    'getall': getall,
                    'dateStartMonth': dateStartMonth,
                    'dateEndMonth': dateEndMonth,
                    'month': month,
                    'year': year,
                    'number_sku': number_sku,
                    'app_type': app_type,
                    'active': active,
                    'channel': channel,
                    'otm': otm,
                    'top_products': top_products,
                    'isGripCustomer': isGripCustomer,
                    'view_by': view_by,
                    'range': range,
                    'isMultiCountry': isMultiCountry,
                    'regional': regional,
                    'mode': action,
                    'filters': filters,
                    'promotion_name': promotion_activity,
                    'view_type': view_type,
                    'salespersonsActive': salespersonsActive,
                    'globalChannels': global_channels
                });

                // set local storage for export
                var export_ns = \$.initNamespaceStorage('export_ns');
                var exportStorage = export_ns.localStorage;
                exportStorage.set({
                    'urlGetReport': urlGetReport,
                    'salespersons': spElement.val(),
                    'salesLeader': slElement.val(),
                    'fromDate': from,
                    'toDate': to,
                    'countryId': country_list,
                    'month': month,
                    'year': year,
                    'app_type': app_type,
                    'channel': channelElement.val(),
                    'otm': otm,
                    'topProducts': topProductElement.val(),
                    'otherProducts': otherProductElement.val(),
                    'denominator': isGripCustomer,
                    'view_by': view_by,
                    'range': rolling,
                    'multiRange': multi_rolling.val(),
                    'mode': action,
                    'filters': filters,
                    'promotion_name': promotion_activity,
                    'view_type': view_type,
                    'salespersonsActive': salespersonsActive,
                    'globalChannels': global_channels
                });

                var url = base_url() + 'ami/reports/view';
                var win = window.open(url, \"_blank\");
                win.focus();
//                    getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, top_products, isGripCustomer, view_by, range, isMultiCountry, regional);

            });

        });
        \$(window).resize(function () {
            var window_width = \$(this).width();
            if (window_width < 1000) {
                \$('#reporting-view').width('100%');
            } else {
                \$('#reporting-view').width('95%');
            }
        });
    </script>
";
    }

    // line 957
    public function block_css($context, array $blocks = array())
    {
        // line 958
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 959
        echo twig_escape_filter($this->env, site_url("res/css/jqueryui/jquery-ui-1.10.4.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 960
        echo twig_escape_filter($this->env, site_url("res/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 961
        echo twig_escape_filter($this->env, site_url("res/css/ami.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 962
        echo twig_escape_filter($this->env, site_url("res/css/plugins/metisMenu/metisMenu.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 963
        echo twig_escape_filter($this->env, site_url("res/css/plugins/parsley/parsley.css"), "html", null, true);
        echo "\"/>

    <link rel=\"stylesheet\" href=\"";
        // line 965
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 966
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 967
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 968
        echo twig_escape_filter($this->env, site_url("res/css/sumoselect.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 969
        echo twig_escape_filter($this->env, site_url("res/css/multiple-select.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 970
        echo twig_escape_filter($this->env, site_url("res/css/demo.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 971
        echo twig_escape_filter($this->env, site_url("res/css/chart.css"), "html", null, true);
        echo "\">

    <style type=\"text/css\">
        .inputWrap {
            margin-bottom: 7px;
        }

        .SumoSelect > .CaptionCont {
            height: 34px;
        }

        .content-wrap {
            margin: 0 auto;
        }
    </style>
";
    }

    // line 988
    public function block_body($context, array $blocks = array())
    {
        // line 989
        echo "
    ";
        // line 990
        echo form_open(site_url("ami/demo/generate"), array("class" => "form-horizontal"));
        echo "
    <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                        data-target=\"#bs-example-navbar-collapse\" aria-expanded=\"false\">
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <h1 id=\"logo_demo\"><a href=\"";
        // line 1000
        echo twig_escape_filter($this->env, site_url("ami/demo"), "html", null, true);
        echo "\"><img
                                src=\"";
        // line 1001
        echo twig_escape_filter($this->env, site_url("res/img/logo-u-food.jpg"), "html", null, true);
        echo "\" title=\"home\"></a></h1>
            </div>
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse\">
                <ul class=\"nav navbar-nav\">
                    <li><a href=\"";
        // line 1005
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Home</a></li>
                    <li><a href=\"";
        // line 1006
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Account</a></li>
                    <li><a href=\"";
        // line 1007
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Country</a></li>
                    <li><a href=\"";
        // line 1008
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Pending Customers</a></li>
                    <li class=\"active\">
                        <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"
                           aria-haspopup=\"true\">Reports</a>
                        <ul class=\"dropdown-menu\">
                            <li>
                                <a href=\"";
        // line 1014
        echo twig_escape_filter($this->env, site_url("ami/new_reports/penetration_data_report"), "html", null, true);
        echo "\">Penetration Data
                                    Report</a>
                            </li>
                            ";
        // line 1018
        echo "                            ";
        // line 1019
        echo "                            ";
        // line 1020
        echo "                        </ul>
                    </li>
                    <li><a href=\"";
        // line 1022
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">AMI</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"container m-t-md content-wrap\">
        <div class=\"col-lg-12\">
            ";
        // line 1030
        if (((isset($context["action"]) ? $context["action"] : null) == "penetration_data_report")) {
            // line 1031
            echo "                <div class=\"form-group\">
                    <div class=\"viewWrap inputWrap col-sm-10\">
                        ";
            // line 1033
            echo form_label("Report Type", "view_by", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1035
            echo form_dropdown("view_by", array(0 => "SKU", 1 => "Channel", 3 => "Channel/SKU", 5 => "SR/SKU", 6 => "SR/Channel"), null, "class=\"select\"");
            echo "
                        </div>
                    </div>

                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1040
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1042
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>

                    <div class=\"monthWrap inputWrap col-sm-10\">
                        ";
            // line 1047
            echo form_label("Month", "month", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1049
            echo form_dropdown("month", select_month(), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "m"), "method"), "class=\"fetch-sl select\" id=\"input-month\"");
            echo "
                        </div>
                    </div>

                    <div class=\"yearWrap inputWrap col-sm-10\">
                        ";
            // line 1054
            echo form_label("Year", "year", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1056
            echo form_dropdown("year", select_year(2014), $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "now", array(), "method"), "format", array(0 => "Y"), "method"), "class=\"select fetch-sl\" id=\"input-year\"");
            echo "
                        </div>
                    </div>

                    <div class=\"salesperosnsActive inputWrap col-sm-10\">
                        ";
            // line 1061
            echo form_label("Salespersons Status", "active", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1063
            echo form_dropdown("active", array("-1" => "All", "1" => "Active", "0" => "Inactive"),  -1, "class=\"select fetch-sl\" id=\"input-sl-status\"");
            echo "
                        </div>
                    </div>

                    <div class=\"salesperosnsActive inputWrap col-sm-10\">
                        ";
            // line 1068
            echo form_label("Global Channel", "global_channels", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1070
            echo form_dropdown("global_channels", (isset($context["global_channels"]) ? $context["global_channels"] : null), null, "class=\"select\"");
            echo "
                        </div>
                    </div>

                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1075
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1079
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1080
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1081
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1083
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1088
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1092
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1093
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1094
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1095
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1097
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"topProduct inputWrap col-sm-10\">
                        ";
            // line 1102
            echo form_label("Top SKU", "top_products", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1104
            echo form_dropdown("top_products[]", array(), 0, "class=\"\" id=\"top-products-sku\"  multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                    <div class=\"otherProduct inputWrap col-sm-10\">
                        ";
            // line 1109
            echo form_label("Other SKU", "other_products", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"other_products[]\" class=\"\" id=\"other-products-sku\" multiple=\"multiple\">
                                ";
            // line 1112
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["otherProducts"]) ? $context["otherProducts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["sku"]) {
                // line 1113
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sku"], "sku_number", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sku"], "sku_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sku'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1115
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"channelWrap inputWrap col-sm-10\">
                        ";
            // line 1120
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"channel[]\" class=\"multi_select\" multiple=\"multiple\" id=\"ChannelSelect\">
                                ";
            // line 1123
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryChannels", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 1124
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1125
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1127
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"otmWrap inputWrap col-sm-10\">
                        ";
            // line 1132
            echo form_label("OTM", "otm", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"otm[]\" class=\"multi_select\" id=\"InputOTM\" multiple=\"multiple\">
                                ";
            // line 1135
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "O" => "Other"));
            foreach ($context['_seq'] as $context["otm"] => $context["title"]) {
                // line 1136
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $context["otm"], "html", null, true);
                echo "\" selected>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sl"]) ? $context["sl"] : null), "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['otm'], $context['title'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1138
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"rolling inputWrap col-sm-10\">
                        ";
            // line 1143
            echo form_label("Date Range", "rolling", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1145
            $context["roll_arr"] = array(1 => "Rolling 3 months", 2 => "Rolling 6 months", 3 => "Rolling 12 months", 4 => "Jan - Jun Last Year", 5 => "Jul - Dec Last Year", 6 => "1 year Last Year");
            // line 1146
            echo "                            ";
            echo form_dropdown("rolling", (isset($context["roll_arr"]) ? $context["roll_arr"] : null), 2, "class=\"fetch-sl select\" id=\"rolling\"");
            echo "
                        </div>
                    </div>

                    <div class=\"multi_rolling inputWrap col-sm-10\">
                        ";
            // line 1151
            echo form_label("Date Range", "rolling", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1153
            $context["roll_arr"] = array(1 => "Rolling 3 months", 2 => "Rolling 6 months", 3 => "Rolling 12 months", 4 => "Jan - Jun Last Year", 5 => "Jul - Dec Last Year", 6 => "1 year Last Year");
            // line 1154
            echo "                            ";
            echo form_dropdown("multi_rolling[]", (isset($context["roll_arr"]) ? $context["roll_arr"] : null), 2, "class=\"multi_select\" id=\"multi_rolling\" multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                    <div class=\"gripCustomerWrap inputWrap col-sm-10\">
                        ";
            // line 1159
            echo form_label("Denominator", "grip_customers", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1161
            echo form_dropdown("customers", $this->getAttribute($this->getAttribute((isset($context["report"]) ? $context["report"] : null), "grip_customers", array()), "list", array()), 1, "class=\"select\" id=\"gripCustomers\"");
            echo "
                        </div>
                    </div>
                </div>
            ";
        } elseif ((        // line 1165
(isset($context["action"]) ? $context["action"] : null) == "grip_grab_report")) {
            // line 1166
            echo "                <div class=\"form-group\">
                    <div class=\"fromToWrap inputWrap col-sm-10\">
                        ";
            // line 1168
            echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1170
            $context["default_day"] = (($this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method") . " - ") . $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"));
            // line 1171
            echo "                            ";
            echo form_input(array("name" => "date_time", "value" => (isset($context["default_day"]) ? $context["default_day"] : null), "class" => "form-control", "id" => "InputDateRange", "placeholder" => "Date"));
            // line 1175
            echo "
                        </div>

                        <input type=\"hidden\" id=\"InputStartDate\" class=\"fetch-sl\" name=\"from\"
                               value=\"";
            // line 1179
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" id=\"InputEndDate\" class=\"fetch-sl\" name=\"to\"
                               value=\"";
            // line 1181
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                    </div>

                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1185
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1187
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>

                    <div class=\"channelWrap inputWrap col-sm-10\">
                        ";
            // line 1192
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"channel[]\" class=\"multi_select\" multiple=\"multiple\" id=\"ChannelSelect\">
                                ";
            // line 1195
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "countryChannels", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 1196
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1197
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1199
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"otmWrap inputWrap col-sm-10\">
                        ";
            // line 1204
            echo form_label("OTM", "otm", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"otm[]\" class=\"multi_select\" id=\"InputOTM\" multiple=\"multiple\">
                                ";
            // line 1207
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "O" => "Other"));
            foreach ($context['_seq'] as $context["otm"] => $context["title"]) {
                // line 1208
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $context["otm"], "html", null, true);
                echo "\" selected>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sl"]) ? $context["sl"] : null), "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['otm'], $context['title'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1210
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1215
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1219
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1220
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1221
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1223
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1228
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1232
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1233
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1234
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1235
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1237
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"filterWrap inputWrap col-sm-10\">
                        ";
            // line 1242
            echo form_label("Report Filter", "filter", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1244
            $context["data_arr"] = array("armstrong2SalespersonsId" => "Salesperson ID", "salespersonsName" => "Salesperson Name", "newGrip" => "Total Grip", "totalLostGrip" => "New Grip", "totalGrip" => "Lost Grip", "newGrab" => "Total Grab", "totalLostGrap" => "New Grab", "totalGrab" => "Lost Grab");
            // line 1245
            echo "                            ";
            echo form_dropdown("filter[]", (isset($context["data_arr"]) ? $context["data_arr"] : null), array(0 => "armstrong2SalespersonsId", 1 => "salespersonsName", 2 => "newGrip"), "class=\"multi_select\" id=\"report_filter\" multiple=\"multiple\"");
            echo "
                        </div>
                    </div>

                </div>
            ";
        } elseif ((        // line 1250
(isset($context["action"]) ? $context["action"] : null) == "promotions_activity_report")) {
            // line 1251
            echo "                <div class=\"promotion_name inputWrap col-sm-10\">
                    ";
            // line 1252
            echo form_label("Promotion Name", "promotion_name", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        ";
            // line 1254
            echo form_dropdown("promotion_name", (((isset($context["promotion_activities"]) ? $context["promotion_activities"] : null)) ? ((isset($context["promotion_activities"]) ? $context["promotion_activities"] : null)) : (array())), null, "class=\"select\" id=\"promotion_name\"");
            echo "
                    </div>
                </div>

                <div class=\"slWrap inputWrap col-sm-10\">
                    ";
            // line 1259
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                multiple=\"multiple\">
                            ";
            // line 1263
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1264
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                        selected>";
                // line 1265
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1267
            echo "                        </select>
                    </div>
                </div>

                <div class=\"spWrap inputWrap col-sm-10\">
                    ";
            // line 1272
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                    <div class=\"col-sm-7\">
                        <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                            ";
            // line 1276
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1277
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                        data-manager=\"";
                // line 1278
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                        selected>";
                // line 1279
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1281
            echo "                        </select>
                    </div>
                </div>
                /*";
        } elseif ((        // line 1284
(isset($context["action"]) ? $context["action"] : null) == "last_pantry_check_report")) {
            // line 1285
            echo "                <div class=\"form-group\">
                    <div class=\"fromToWrap inputWrap col-sm-10\">
                        ";
            // line 1287
            echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1289
            $context["default_day"] = (($this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method") . " - ") . $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"));
            // line 1290
            echo "                            ";
            echo form_input(array("name" => "date_time", "value" => (isset($context["default_day"]) ? $context["default_day"] : null), "class" => "form-control", "id" => "InputDateRange", "placeholder" => "Date"));
            // line 1294
            echo "
                        </div>

                        <input type=\"hidden\" id=\"InputStartDate\" class=\"fetch-sl\" name=\"from\"
                               value=\"";
            // line 1298
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "first day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" id=\"InputEndDate\" class=\"fetch-sl\" name=\"to\"
                               value=\"";
            // line 1300
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => "last day of this month"), "method"), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"slWrap inputWrap col-sm-10\">
                        ";
            // line 1303
            echo form_label("Sales Leader", "sales_leader", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"sales_leader[]\" class=\"SalesLeaderSelect multi_select\" id=\"SalesLeaderSelect\"
                                    multiple=\"multiple\">
                                ";
            // line 1307
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salesleaders", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sl"]) {
                // line 1308
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1309
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sl"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sl'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1311
            echo "                            </select>
                        </div>
                    </div>

                    <div class=\"spWrap inputWrap col-sm-10\">
                        ";
            // line 1316
            echo form_label("Sales Persons", "salesperson", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            <select name=\"armstrong_2_salespersons_id[]\" class=\"SalespersonsSelect multi_select\"
                                    id=\"armstrong_2_salespersons_id\" multiple=\"multiple\">
                                ";
            // line 1320
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
                // line 1321
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\"
                                            data-manager=\"";
                // line 1322
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
                echo "\"
                                            selected>";
                // line 1323
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1325
            echo "                            </select>
                        </div>
                    </div>
                    <div class=\"appTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1329
            echo form_label("App Type", "app_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1331
            echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), null, "class=\" fetch-sl select\" id=\"input-app-type\"");
            echo "
                        </div>
                    </div>
                    <div class=\"viewTypeWrap inputWrap col-sm-10\">
                        ";
            // line 1335
            echo form_label("View by", "view_type", array("class" => "control-label col-sm-4"));
            echo "
                        <div class=\"col-sm-7\">
                            ";
            // line 1337
            $context["view_type"] = array("sku" => "SKU", "region" => "Region", "sl/sr" => "SL/SR", "otm" => "OTM");
            // line 1338
            echo "                            ";
            echo form_dropdown("view_type", (isset($context["view_type"]) ? $context["view_type"] : null), null, "class=\" select\" id=\"input-view-type\"");
            echo "
                        </div>
                    </div>
                </div>*/
            ";
        }
        // line 1343
        echo "            <input type=\"hidden\" name=\"type\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" name=\"sub_type\" value=\"\"/>
            <input type=\"hidden\" name=\"filter\" value=\"";
        // line 1345
        echo twig_escape_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null), "html", null, true);
        echo "\"/>

            <div class=\"row\">
                <div class=\"form-group form-group-demo\">
                    <div class=\"col-sm-12 text-center\">
                        <a class=\"btn btn-default reset\"><i class=\"fa fw fa-times\"></i> Reset</a>
                        <button class=\"btn btn-primary btn-view-report\" ";
        // line 1351
        echo (((!twig_in_filter((isset($context["action"]) ? $context["action"] : null), array(0 => "data_customers")) && $this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_view", array()))) ? ("") : ("disabled"));
        echo ">
                            <i class=\"fa fw fa-eye\"></i> View
                        </button>
                        <button type=\"submit\"
                                class=\"btn btn-primary btn-export-report\" ";
        // line 1355
        echo (($this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_export", array())) ? ("") : ("disabled"));
        echo ">
                            <i class=\"fa fw fa-download\"></i> Export
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    ";
        // line 1365
        echo form_close();
        echo "

    <div class=\"modal fade\" id=\"myModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-body text-center\">
                    <img style=\"width: 200px\" src=\"";
        // line 1371
        echo twig_escape_filter($this->env, site_url("res/img/_AMILOAD.gif"), "html", null, true);
        echo "\"/>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <button id=\"zoom_rp\" class=\"btn btn-mini btn-zoom-report\" onclick=\"zoom_report()\"><i class=\"icon-zoom-in\"></i>&nbsp;Zoom
    </button>
    <div id=\"reporting-view\">
        <!-- reports get generated here -->
        <div class=\"loading\"></div>
        <iframe id=\"iframe_report\"></iframe>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/demo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2017 => 1371,  2008 => 1365,  1995 => 1355,  1988 => 1351,  1979 => 1345,  1973 => 1343,  1964 => 1338,  1962 => 1337,  1957 => 1335,  1950 => 1331,  1945 => 1329,  1939 => 1325,  1929 => 1323,  1925 => 1322,  1920 => 1321,  1916 => 1320,  1909 => 1316,  1902 => 1311,  1892 => 1309,  1887 => 1308,  1883 => 1307,  1876 => 1303,  1870 => 1300,  1865 => 1298,  1859 => 1294,  1856 => 1290,  1854 => 1289,  1849 => 1287,  1845 => 1285,  1843 => 1284,  1838 => 1281,  1828 => 1279,  1824 => 1278,  1819 => 1277,  1815 => 1276,  1808 => 1272,  1801 => 1267,  1791 => 1265,  1786 => 1264,  1782 => 1263,  1775 => 1259,  1767 => 1254,  1762 => 1252,  1759 => 1251,  1757 => 1250,  1748 => 1245,  1746 => 1244,  1741 => 1242,  1734 => 1237,  1724 => 1235,  1720 => 1234,  1715 => 1233,  1711 => 1232,  1704 => 1228,  1697 => 1223,  1687 => 1221,  1682 => 1220,  1678 => 1219,  1671 => 1215,  1664 => 1210,  1651 => 1208,  1647 => 1207,  1641 => 1204,  1634 => 1199,  1626 => 1197,  1621 => 1196,  1617 => 1195,  1611 => 1192,  1603 => 1187,  1598 => 1185,  1591 => 1181,  1586 => 1179,  1580 => 1175,  1577 => 1171,  1575 => 1170,  1570 => 1168,  1566 => 1166,  1564 => 1165,  1557 => 1161,  1552 => 1159,  1543 => 1154,  1541 => 1153,  1536 => 1151,  1527 => 1146,  1525 => 1145,  1520 => 1143,  1513 => 1138,  1500 => 1136,  1496 => 1135,  1490 => 1132,  1483 => 1127,  1475 => 1125,  1470 => 1124,  1466 => 1123,  1460 => 1120,  1453 => 1115,  1442 => 1113,  1438 => 1112,  1432 => 1109,  1424 => 1104,  1419 => 1102,  1412 => 1097,  1402 => 1095,  1398 => 1094,  1393 => 1093,  1389 => 1092,  1382 => 1088,  1375 => 1083,  1365 => 1081,  1360 => 1080,  1356 => 1079,  1349 => 1075,  1341 => 1070,  1336 => 1068,  1328 => 1063,  1323 => 1061,  1315 => 1056,  1310 => 1054,  1302 => 1049,  1297 => 1047,  1289 => 1042,  1284 => 1040,  1276 => 1035,  1271 => 1033,  1267 => 1031,  1265 => 1030,  1254 => 1022,  1250 => 1020,  1248 => 1019,  1246 => 1018,  1240 => 1014,  1231 => 1008,  1227 => 1007,  1223 => 1006,  1219 => 1005,  1212 => 1001,  1208 => 1000,  1195 => 990,  1192 => 989,  1189 => 988,  1169 => 971,  1165 => 970,  1161 => 969,  1157 => 968,  1153 => 967,  1149 => 966,  1145 => 965,  1140 => 963,  1136 => 962,  1132 => 961,  1128 => 960,  1124 => 959,  1119 => 958,  1116 => 957,  992 => 835,  981 => 826,  979 => 825,  976 => 824,  967 => 817,  965 => 816,  949 => 805,  901 => 760,  788 => 650,  523 => 388,  489 => 356,  465 => 335,  458 => 331,  451 => 326,  449 => 325,  331 => 210,  327 => 209,  235 => 120,  158 => 45,  155 => 44,  141 => 33,  137 => 32,  132 => 30,  128 => 29,  124 => 28,  120 => 27,  116 => 26,  112 => 25,  108 => 24,  104 => 23,  100 => 22,  96 => 21,  92 => 20,  87 => 18,  82 => 16,  78 => 15,  74 => 14,  70 => 13,  66 => 12,  62 => 11,  58 => 10,  54 => 9,  50 => 8,  46 => 7,  41 => 6,  38 => 5,  32 => 3,  11 => 1,);
    }
}
