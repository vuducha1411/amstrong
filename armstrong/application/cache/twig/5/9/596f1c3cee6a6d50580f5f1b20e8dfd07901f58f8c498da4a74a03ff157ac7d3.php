<?php

/* ami/media/view.html.twig */
class __TwigTemplate_596f1c3cee6a6d50580f5f1b20e8dfd07901f58f8c498da4a74a03ff157ac7d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"ModalMediaGallery\" class=\"modal-dialog\" data-class=\"";
        echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true);
        echo "\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">Media Gallery</h3>
            <i style=\"font-size: 11px;\">Warning: Deleting a media will remove all its instances.</i>
            <ul class=\"nav nav-tabs\">
                <li role=\"presentation\" class=\"active\"><a href=\"#thumbView\">Thumbnail View</a></li>
                <li role=\"presentation\"><a href=\"#listView\">List View</a></li>
            </ul>
        </div>
        <div id=\"message\"></div>
        <div class=\"modal-body\">
            <div id=\"thumbView\" style=\"position: relative; height: 400px; overflow: hidden;\">
                ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["media"]) ? $context["media"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 16
            echo "                    <div class=\"col-md-4\">
                        <div class=\"thumbnail\">
                            <a class=\"mediathumb\" data-id=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\" style=\"cursor: pointer;\">
                                <img src=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "thumb", array()), "html", null, true);
            echo "\" class=\"img-responsive\" style=\"width: 100%; height: 120px;\">
                            </a>

                            <div class=\"caption\">
                                <span class=\"label label-primary\">";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "filename_old", array()), "html", null, true);
            echo "</span>
                                <ul class=\"list-inline\" style=\"margin: 10px 0 0 0\">
                                    ";
            // line 25
            if (($this->getAttribute($context["item"], "add_path", array()) == 1)) {
                // line 26
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, (((((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "entry_type", array())) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                           class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                    class=\"fa fa-eye\"></i> View</a>
                                    ";
            } else {
                // line 30
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, (((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                           class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                    class=\"fa fa-eye\"></i> View</a>
                                    ";
            }
            // line 34
            echo "
                                    <a href=\"#message\" data-id=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "class", array()), "html", null, true);
            echo "\"
                                       class=\"btn btn-xs btn-default pull-right delete delete-media-gallery\"><i
                                                class=\"fa fa-close\"></i> Delete</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "            </div>
            <div id=\"listView\" style=\"position: relative; height: 400px; overflow: hidden;\">
                <table class=\"table table-hover table-responsive\">
                    <tbody>
                    ";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["media"]) ? $context["media"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 48
            echo "                        <tr>
                            <td class=\"media-name\">
                                <a class=\"mediathumb\" title=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "filename_old", array()), "html", null, true);
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\" style=\"cursor: pointer;\">
                                    <span class=\"filename\">";
            // line 51
            echo wordTrim($this->getAttribute($context["item"], "filename_old", array()), 50);
            echo "</span>
                                    <img src=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "thumb", array()), "html", null, true);
            echo "\" class=\"img-responsive\" style=\"width: 100%; height: 120px;\">
                                </a>
                                <div class=\"caption\">
                                    <span class=\"label label-primary\">";
            // line 55
            echo wordTrim($this->getAttribute($context["item"], "filename_old", array()), 16);
            echo "</span>
                                    <ul class=\"list-inline\" style=\"margin: 10px 0 0 0\">
                                        ";
            // line 57
            if (($this->getAttribute($context["item"], "add_path", array()) == 1)) {
                // line 58
                echo "                                            <a href=\"";
                echo twig_escape_filter($this->env, (((((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "entry_type", array())) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                               class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                        class=\"fa fa-eye\"></i> View</a>
                                        ";
            } else {
                // line 62
                echo "                                            <a href=\"";
                echo twig_escape_filter($this->env, (((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                               class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                        class=\"fa fa-eye\"></i> View</a>
                                        ";
            }
            // line 66
            echo "
                                        <a href=\"#message\" data-id=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "class", array()), "html", null, true);
            echo "\"
                                           class=\"btn btn-xs btn-default pull-right delete delete-media-gallery\"><i
                                                    class=\"fa fa-close\"></i> Delete</a>
                                    </ul>
                                </div>
                            </td>
                            <td>";
            // line 73
            if (($this->getAttribute($context["item"], "add_path", array()) == 1)) {
                // line 74
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, (((((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "entry_type", array())) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                       class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                class=\"fa fa-eye\"></i> View</a>
                                ";
            } else {
                // line 78
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, (((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/") . (isset($context["class"]) ? $context["class"] : null)) . "/") . $this->getAttribute($context["item"], "path", array())), "html", null, true);
                echo "\"
                                       class=\"btn btn-xs btn-default modal-view\" target=\"_blank\"><i
                                                class=\"fa fa-eye\"></i> View</a>
                                ";
            }
            // line 82
            echo "
                                <a href=\"#message\" data-id=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "class", array()), "html", null, true);
            echo "\"
                                   class=\"btn btn-xs btn-default pull-right delete delete-media-gallery\"><i
                                            class=\"fa fa-close\"></i> Delete</a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                    </tbody>
                </table>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button id=\"modalClose\" class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i
                        class=\"fa fa-fw fa-close\"></i> Cancel
            </button>
        </div>
    </div>
</div>
<link href=\"";
        // line 100
        echo twig_escape_filter($this->env, site_url("res/css/plugins/perfect-scrollbar/perfect-scrollbar.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css'>
<script src=\"";
        // line 101
        echo twig_escape_filter($this->env, site_url("res/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"), "html", null, true);
        echo "\"></script>
<script>
    \$('#thumbView, #listView').perfectScrollbar();
    \$(document).ready(function () {
        \$('#listView').hide();
        \$('ul.nav-tabs li').on('click', function () {
            var prev_active = \$('ul.nav-tabs li.active'),
                    prev_block = prev_active.find('a').attr('href'),
                    cur_block = \$(this).find('a').attr('href');
            prev_active.removeClass('active');
            \$(this).addClass('active');
            \$(prev_block).hide();
            \$(cur_block).show();
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "ami/media/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 101,  213 => 100,  200 => 89,  186 => 83,  183 => 82,  175 => 78,  167 => 74,  165 => 73,  154 => 67,  151 => 66,  143 => 62,  135 => 58,  133 => 57,  128 => 55,  122 => 52,  118 => 51,  112 => 50,  108 => 48,  104 => 47,  98 => 43,  82 => 35,  79 => 34,  71 => 30,  63 => 26,  61 => 25,  56 => 23,  49 => 19,  45 => 18,  41 => 16,  37 => 15,  19 => 1,);
    }
}
