<?php

/* ami/promotions_activity/edit.html.twig */
class __TwigTemplate_5d70848a34b94518b9bcc8daa7a127433ed822d1fe5f2f97bde358fd03f67169 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotions_activity/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    \$('#image-preview').attr('src', e.target.result).css('width', '160px');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        \$(function() {
            \$('.datetimepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            })
        });

        \$(document).ready(function () {
            \$('#image').on(\"change\", function () {
                readURL(this);
            });
        });
    </script>
";
    }

    // line 38
    public function block_css($context, array $blocks = array())
    {
        // line 39
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 43
    public function block_content($context, array $blocks = array())
    {
        // line 44
        echo "    ";
        echo form_open(site_url("ami/promotions_activity/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true", "enctype" => "multipart/form-data"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Promotional Activity</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 53
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 55
        echo "                        ";
        echo html_btn(site_url("ami/promotions_activity"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 56
        if ($this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "armstrong_2_promotions_activity_id", array())) {
            // line 57
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/promotions_activity/delete/" . $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "armstrong_2_promotions_activity_id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 60
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 71
        echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 73
        echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 77
        echo form_label("Subject", "subject", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 79
        echo form_input(array("name" => "subject", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "subject", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 83
        echo form_label("Url", "url", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 85
        echo form_input(array("name" => "url", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "url", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 89
        echo form_label("Date Start", "date_start", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 91
        echo form_input(array("name" => "date_start", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "date_start", array()), "class" => "form-control datetimepicker", "id" => "InputDateStart"));
        // line 93
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 97
        echo form_label("Date End", "date_end", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 99
        echo form_input(array("name" => "date_end", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "date_end", array()), "class" => "form-control datetimepicker", "id" => "InputDateEnd"));
        // line 101
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 105
        echo form_label("Image", "image", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <img class=\"img-responsive profile-img margin-bottom-20\" alt=\"\" id=\"image-preview\"
                         src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "image", array()), "html", null, true);
        echo "\">
                    <input type=\"file\" name=\"image\" id=\"image\">
                    ";
        // line 111
        echo "                </div>
            </div>
            ";
        // line 113
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["promotions_activity"]) ? $context["promotions_activity"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 118
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/promotions_activity/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 118,  224 => 113,  220 => 111,  215 => 108,  209 => 105,  203 => 101,  201 => 99,  196 => 97,  190 => 93,  188 => 91,  183 => 89,  176 => 85,  171 => 83,  164 => 79,  159 => 77,  152 => 73,  147 => 71,  134 => 60,  127 => 57,  125 => 56,  120 => 55,  116 => 53,  103 => 44,  100 => 43,  94 => 40,  89 => 39,  86 => 38,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
