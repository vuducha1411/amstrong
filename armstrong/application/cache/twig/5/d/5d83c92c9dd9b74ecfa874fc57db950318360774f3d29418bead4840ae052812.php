<?php

/* ami/sampling/sampling.html.twig */
class __TwigTemplate_5d83c92c9dd9b74ecfa874fc57db950318360774f3d29418bead4840ae052812 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["samplings"]) ? $context["samplings"] : null)) {
            echo " 

    <div class=\"panel panel-default\">                            
        <div class=\"panel-body\">                
            <div class=\"\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                    <thead>
                        <tr>
                            ";
            // line 9
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sampling"))) {
                // line 10
                echo "                                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                            ";
            }
            // line 12
            echo "                            <th>ID</th>
                            <th>Salesperson Name</th>
                            <th>Operator</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 21
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["samplings"]) ? $context["samplings"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["sampling"]) {
                // line 22
                echo "                            <tr>
                                ";
                // line 23
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sampling"))) {
                    // line 24
                    echo "                                    <td class=\"text-center\">";
                    echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["sampling"], "armstrong_2_sampling_id", array())));
                    echo "</td>
                                ";
                }
                // line 26
                echo "                                <td><a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/sampling/edit/" . $this->getAttribute($context["sampling"], "armstrong_2_sampling_id", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "armstrong_2_sampling_id", array()), "html", null, true);
                echo "\" data-toggle=\"ajaxModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "armstrong_2_sampling_id", array()), "html", null, true);
                echo "</a></td>
                                <td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "last_name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "armstrong_2_customers_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "armstrong_2_customers_name", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "date_created", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["sampling"], "last_updated", array()), "html", null, true);
                echo "</td>
                                <td class=\"center text-center\" style=\"min-width: 80px;\">
                                    ";
                // line 32
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "sampling")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sampling")))) {
                    // line 33
                    echo "                                        ";
                    $this->loadTemplate("ami/components/table_btn.html.twig", "ami/sampling/sampling.html.twig", 33)->display(array_merge($context, array("url" => "ami/sampling", "id" => $this->getAttribute($context["sampling"], "armstrong_2_sampling_id", array()), "permission" => "sampling")));
                    // line 34
                    echo "                                    ";
                }
                // line 35
                echo "                                </td>
                            </tr>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sampling'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                    </tbody>
                </table>
            </div>
            <!-- /. -->
        </div>
        <!-- /.panel-body -->

    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('#dataTables').dataTable({
            'order': [[1, 'asc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });        
    });  
    </script>
";
        } else {
            // line 62
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no Sampling</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/sampling/sampling.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 62,  131 => 38,  115 => 35,  112 => 34,  109 => 33,  107 => 32,  102 => 30,  98 => 29,  92 => 28,  84 => 27,  75 => 26,  69 => 24,  67 => 23,  64 => 22,  47 => 21,  36 => 12,  32 => 10,  30 => 9,  19 => 1,);
    }
}
