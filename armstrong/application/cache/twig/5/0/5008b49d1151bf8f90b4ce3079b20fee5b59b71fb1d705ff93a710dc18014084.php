<?php

/* ami/global_ssd/viewAllMapDTandWHS.html.twig */
class __TwigTemplate_5008b49d1151bf8f90b4ce3079b20fee5b59b71fb1d705ff93a710dc18014084 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/global_ssd/viewAllMapDTandWHS.html.twig", 6);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_js($context, array $blocks = array())
    {
        // line 9
        echo "\t";
        $this->displayParentBlock("js", $context, $blocks);
        echo " 
";
    }

    // line 12
    public function block_css($context, array $blocks = array())
    {
        // line 13
        echo "\t";
        $this->displayParentBlock("css", $context, $blocks);
        echo "

\t<style>
        #mainContent {
            margin-top: 20px;
        }
\t</style>
";
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "\t<div class=\"row sticky sticky-h1 bg-white\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"page-header nm\">
\t\t\t\t<h1>
\t\t\t\t\tMapping Table 
\t\t\t\t\t<small>
\t\t\t\t\t\tfor all DT and WHS
\t\t\t\t\t</small>
\t\t\t\t</h1>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row m-t-md\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div id=\"mainContent\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h3>Distributors <small>(";
        // line 42
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["allMaps"]) ? $context["allMaps"] : null), "distributors", array(), "array")), "html", null, true);
        echo ")</small></h3><hr>
\t\t\t\t\t\t\t";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["allMaps"]) ? $context["allMaps"] : null), "distributors", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["dist"]) {
            // line 44
            echo "\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 45
            echo twig_escape_filter($this->env, site_url("ami/global_ssd/modDraftMapping"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dist"], "map_id", array(), "array"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dist"], "name", array(), "array"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
            // line 46
            echo (($this->getAttribute($context["dist"], "is_draft", array(), "array")) ? ("<small>(Draft)</small>") : (""));
            echo "
\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t<div class=\"scroll-x\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-condensed table-bordered table-hover table-striped\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 52
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(twig_split_filter($this->env, $this->getAttribute($context["dist"], "columns", array(), "array"), ":"));
            foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                // line 53
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<th>";
                echo twig_escape_filter($this->env, $context["col"], "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 58
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["dist"], "value", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
                // line 59
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 60
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($context["val"]);
                foreach ($context['_seq'] as $context["_key"] => $context["indVal"]) {
                    // line 61
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $context["indVal"], "html", null, true);
                    echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indVal'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dist'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h3>Wholesalers <small>(";
        // line 73
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["allMaps"]) ? $context["allMaps"] : null), "wholesalers", array(), "array")), "html", null, true);
        echo ")</small></h3><hr>
\t\t\t\t\t\t\t";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["allMaps"]) ? $context["allMaps"] : null), "wholesalers", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["dist"]) {
            // line 75
            echo "\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 76
            echo twig_escape_filter($this->env, site_url("ami/global_ssd/modDraftMapping"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dist"], "map_id", array(), "array"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["dist"], "name", array(), "array"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
            // line 77
            echo (($this->getAttribute($context["dist"], "is_draft", array(), "array")) ? ("<small>(Draft)</small>") : (""));
            echo "
\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t<div class=\"scroll-x\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-condensed table-bordered table-hover table-striped\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 83
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(twig_split_filter($this->env, $this->getAttribute($context["dist"], "columns", array(), "array"), ":"));
            foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                // line 84
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<th>";
                echo twig_escape_filter($this->env, $context["col"], "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 89
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["dist"], "value", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
                // line 90
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 91
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($context["val"]);
                foreach ($context['_seq'] as $context["_key"] => $context["indVal"]) {
                    // line 92
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $context["indVal"], "html", null, true);
                    echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indVal'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dist'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "ami/global_ssd/viewAllMapDTandWHS.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 100,  241 => 96,  234 => 94,  225 => 92,  221 => 91,  218 => 90,  214 => 89,  209 => 86,  200 => 84,  196 => 83,  187 => 77,  179 => 76,  176 => 75,  172 => 74,  168 => 73,  162 => 69,  153 => 65,  146 => 63,  137 => 61,  133 => 60,  130 => 59,  126 => 58,  121 => 55,  112 => 53,  108 => 52,  99 => 46,  91 => 45,  88 => 44,  84 => 43,  80 => 42,  59 => 23,  56 => 22,  43 => 13,  40 => 12,  33 => 9,  30 => 8,  11 => 6,);
    }
}
