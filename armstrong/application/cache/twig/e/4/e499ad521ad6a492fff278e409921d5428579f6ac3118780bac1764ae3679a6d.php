<?php

/* ami/ssd/edit.html.twig */
class __TwigTemplate_e499ad521ad6a492fff278e409921d5428579f6ac3118780bac1764ae3679a6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/ssd/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/jquery-number/jquery.number.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/salespersons.js"), "html", null, true);
        echo "\"></script>
<script>

function initProductAutocomplete(destroy) {

    if (destroy) {
        \$('.product-tokenfield').autocomplete( \"destroy\" );
    }

    \$('.product-tokenfield').autocomplete({
        source: \"";
        // line 18
        echo twig_escape_filter($this->env, site_url("ami/products/tokenfield"), "html", null, true);
        echo "\",
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            var \$this = \$(this),
                \$base = \$this.closest('.sku-base'),
                \$input = \$base.find('.InputProductId'),
                price = ui.item.price || 0,
                pcsPrice = ui.item.pcsprice || 0,
                \$qtyCase = \$base.find('.qty-case'),
                \$qtyPcs = \$base.find('.qty-pcs'),
                qtyCase = parseInt(\$qtyCase.val()) || 0,
                qtyPcs = parseInt(\$qtyPcs.val()) || 0,        
                // \$total = ";
        // line 31
        echo (($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array())) ? ("\$('#total')") : ("\$base.find('.total-price')"));
        echo ";
                \$total = \$base.find('.total-price');


            if (price && pcsPrice)
            {
                var total = (price * qtyCase + pcsPrice * qtyPcs);
                \$total.val(\$.number(total, 2));
            }

            \$this.val(ui.item.label);
            \$input.val(ui.item.value)
                .attr('data-price', price)
                .attr('data-pcsprice', pcsPrice);
        }
    }).on('focus', function() {
        this.select();
    });
}



function toggle(obj) {
    var \$input = \$(obj);
    if (\$input.prop('checked')){
        document.getElementById(\"isConsolidatedOrder\").value = \"1\";
    } 
    else {
        document.getElementById(\"isConsolidatedOrder\").value = \"0\";
    };
}

\$(function()
{
    \$(document).on('change', '.product-selectz', function(e) {
        var \$this = \$(this),
            \$selected = \$this.find(\"option:selected\"),
            price = \$selected.data('price') || 0,
            pcsPrice = \$selected.data('pcsprice') || 0,
            \$base = \$(this).closest('.sku-base'),
            \$qtyCase = \$base.find('.qty-case'),
            \$qtyPcs = \$base.find('.qty-pcs'),
            qtyCase = parseInt(\$qtyCase.val()) || 0,
            qtyPcs = parseInt(\$qtyPcs.val()) || 0,        
            // \$total = ";
        // line 75
        echo (($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array())) ? ("\$('#total')") : ("\$base.find('.total-price')"));
        echo ";
            \$total = \$base.find('.total-price');

        if (price && pcsPrice)
        {
            var total = (price * qtyCase + pcsPrice * qtyPcs);
            \$total.val(\$.number(total, 2));
        }
    });

    \$(document).on('change input', '.qty-case, .qty-pcs', function(e) {
        var \$this = \$(this),
            \$base = \$this.closest('.sku-base'),
            \$input = \$base.find('.InputProductId'),
            price = \$input.data('price') || 0,
            pcsPrice = \$input.data('pcsprice') || 0,
            \$qtyCase = \$base.find('.qty-case'),
            \$qtyPcs = \$base.find('.qty-pcs'),
            qtyCase = parseInt(\$qtyCase.val()) || 0,
            qtyPcs = parseInt(\$qtyPcs.val()) || 0,        
            // \$total = ";
        // line 95
        echo (($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array())) ? ("\$('#total')") : ("\$base.find('.total-price')"));
        echo ";
            \$total = \$base.find('.total-price');

        if (price && pcsPrice)
        {
            var total = (price * qtyCase + pcsPrice * qtyPcs);
            \$total.val(\$.number(total, 2));
        }
    });

    \$(document).on('click', '.btn-clone', function(e) {
        initProductAutocomplete();
    });

    \$('.datetimepicker').datetimepicker({
        //format: 'YYYY-MM-DD HH:ii:ss'
    });

    \$('#InputWholesalers').autocomplete({
       // source: \"";
        // line 114
        echo twig_escape_filter($this->env, site_url("ami/wholesalers/tokenfield"), "html", null, true);
        echo "\",
        source: function(request, response) {
            \$.ajax({
                url: \"";
        // line 117
        echo twig_escape_filter($this->env, site_url("ami/wholesalers/tokenfield"), "html", null, true);
        echo "\",
                dataType: \"json\",
                data: {
                    term : request.term,
                    action: 'ssd',
                    salespersons : \$('#SelectSalesperson').find(\":selected\").val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            \$(this).val(ui.item.label);
            \$('#InputWholesalersId').val(ui.item.value);

            \$('#InputDistributors').val('');
            \$('#InputDistributorsId').val('');
        }
    }).on('focus', function() {
        this.select();
    });

    \$('#InputDistributors').autocomplete({
       // source: \"";
        // line 143
        echo twig_escape_filter($this->env, site_url("ami/distributors/tokenfield"), "html", null, true);
        echo "\",
        source: function(request, response) {
            \$.ajax({
                url: \"";
        // line 146
        echo twig_escape_filter($this->env, site_url("ami/distributors/tokenfield"), "html", null, true);
        echo "\",
                dataType: \"json\",
                data: {
                    term : request.term,
                    action: 'ssd',
                    salespersons : \$('#SelectSalesperson').find(\":selected\").val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            \$(this).val(ui.item.label);
            \$('#InputDistributorsId').val(ui.item.value);

            \$('#InputWholesalers').val('');
            \$('#InputWholesalersId').val('');
        }
    }).on('focus', function() {
        this.select();
    });

    \$('#InputCallRecords').autocomplete({
        source: \"";
        // line 172
        echo twig_escape_filter($this->env, site_url("ami/call_records/tokenfield"), "html", null, true);
        echo "\",
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            \$(this).val(ui.item.label);
            \$('#InputCallRecordsId').val(ui.item.value);
        }
    }).on('focus', function() {
        this.select();
    });

    \$('#InputCustomers').autocomplete({
        // source: \"";
        // line 184
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield?action=ssd&salespersons="), "html", null, true);
        echo "\" + \$('#SelectSalesperson').find(\":selected\").val(),
        source: function(request, response) {
            \$.ajax({
                url: \"";
        // line 187
        echo twig_escape_filter($this->env, site_url("ami/customers/tokenfield"), "html", null, true);
        echo "\",
                dataType: \"json\",
                data: {
                    term : request.term,
                    action: 'ssd',
                    salespersons : \$('#SelectSalesperson').find(\":selected\").val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            \$(this).val(ui.item.label);
            \$('#InputCustomersId').val(ui.item.value);
        }
    }).on('focus', function() {
        this.select();
    });
    
    \$('#SelectSalesperson').on('change', function() {
        \$(\"#InputCustomers\").autocomplete( \"search\", \"\" );
    });


    // window.ParsleyUI.addError(\$('#InputWholesalers').parsley(), 'required', ' ');
    // window.ParsleyUI.addError(\$('#InputDistributors').parsley(), 'required', ' '); 

    \$.listen('parsley:form:validate', function(formInstance) {
        
        window.ParsleyUI.removeError(\$('#InputWholesalers').parsley(), 'required', 'Either this value or Distributor value is required');
        window.ParsleyUI.removeError(\$('#InputDistributors').parsley(), 'required', 'Either this value or Wholesaler value is required');

        if (!\$('#InputWholesalers').val() && !\$('#InputDistributors').val()) {

            window.ParsleyUI.addError(\$('#InputWholesalers').parsley(), 'required', 'Either this value or Distributor value is required');
            window.ParsleyUI.addError(\$('#InputDistributors').parsley(), 'required', 'Either this value or Wholesaler value is required');

            formInstance.submitEvent.preventDefault();
        }
    });
    
    // \$('.parlsey-form').on('submit', function(e) {
    //     console.log(\$('#InputWholesalers').val());
    //     console.log(\$('#InputDistributors').val());
    //     if (\$('#InputWholesalers').val() == '' && \$('#InputDistributors').val() == '') {
    //         console.log('123');
    //         window.ParsleyUI.addError(\$('#InputWholesalers').parsley(), 'required', 'Either this value or Distributor value is required');
    //         window.ParsleyUI.addError(\$('#InputDistributors').parsley(), 'required', 'Either this value or Wholesaler value is required');

    //         e.preventDefault();
    //     }
    // });

    initProductAutocomplete();

    var ngayon = new Date();
    ngayon.setMonth(ngayon.getMonth() - 4);

    \$('.ssd-datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        minDate: ngayon
    }).on(\"dp.change\", function (e) {
        \$('#InputMonth').val(moment(e.date).format('M'));
        \$('#InputYear').val(moment(e.date).format('YYYY'));
    });

    // \$('.parsley-need-one').change(function() {
    //     var \$this = \$(this),
    //         \$selected = \$this.find(\"option:selected\"),
    //         target = \$this.data('target') || false;

    //     if (target)
    //     {
    //         if (\$selected.val())
    //         {
    //             \$(target).attr('data-parsley-required', 'false');
    //         }
    //         else
    //         {
    //             \$(target).attr('data-parsley-required', 'true');
    //         }
    //     }
    // });
});
</script>
";
    }

    // line 277
    public function block_css($context, array $blocks = array())
    {
        // line 278
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 279
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
";
    }

    // line 282
    public function block_content($context, array $blocks = array())
    {
        // line 283
        echo "
    ";
        // line 284
        echo form_open(site_url("ami/ssd/save"), array("class" => "form-horizontal parlsey-form", "data-parsley-validate" => "true"), array("id" => $this->getAttribute(        // line 285
(isset($context["ssd"]) ? $context["ssd"] : null), "id", array()), "pending" =>         // line 286
(isset($context["pending"]) ? $context["pending"] : null)));
        // line 287
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Ssd</h1>
                <div class=\"text-right\">
                    ";
        // line 295
        echo "                    <div class=\"btn-header-toolbar\">
                        ";
        // line 296
        $context["url"] = (((isset($context["pending"]) ? $context["pending"] : null)) ? ("ami/ssd/pending") : ("ami/ssd"));
        echo "  
                        ";
        // line 297
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("add", "ssd")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "ssd")))) {
            echo "  
                            ";
            // line 298
            echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
            echo "
                        ";
        }
        // line 300
        echo "
                        ";
        // line 301
        echo html_btn(site_url((isset($context["url"]) ? $context["url"] : null)), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        
                        ";
        // line 303
        if (($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array()) && call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "ssd")))) {
            // line 304
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/ssd/delete/" . $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\" class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 305
        echo "    
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">             
                ";
        // line 315
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_code", array()), array(0 => "tw"))) {
            // line 316
            echo "                    <div class=\"form-group\">
                        ";
            // line 317
            echo form_label("Is Consolidated Order?", "is_consolidated_order", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-5\">
                            <input type=\"checkbox\" value=\"1\" name=\"is_consolidated_order\" id=\"isConsolidatedOrder\" onclick=\"toggle(this)\" ";
            // line 319
            echo ((($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "is_consolidated_order", array()) == "1")) ? ("checked") : (""));
            echo ">&nbsp; Yes</input>
                        </div>
                    </div>
                ";
        }
        // line 322
        echo "                    
                <div class=\"form-group\">
                    ";
        // line 324
        echo form_label("Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 326
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_salespersons_id", array()), (("
                            class=\"form-control\" 
                            data-parsley-required=\"true\" 
                            id=\"SelectSalesperson\" 
                            data-url=\"" . site_url("ami/salespersons/fetch_customer")) . "\"
                        "));
        // line 331
        echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 336
        echo form_label("Customer", "armstrong_2_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 343
        echo "                        ";
        echo form_input(array("name" => "armstrong_2_customers_id_", "value" => $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_customers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputCustomers", "data-parsley-required" => "true"));
        // line 347
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_customers_id\" id=\"InputCustomersId\" value=\"";
        // line 348
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_customers_id", array()), "html", null, true);
        echo "\" />
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 353
        echo form_label("Wholesaler", "armstrong_2_wholesalers_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 360
        echo "                        ";
        echo form_input(array("name" => "armstrong_2_wholesalers_id_", "value" => $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_wholesalers_id", array()), array(), "array"), "class" => "form-control", "id" => "InputWholesalers"));
        // line 363
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_wholesalers_id\" id=\"InputWholesalersId\" value=\"";
        // line 364
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_wholesalers_id", array()), "html", null, true);
        echo "\" />
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 369
        echo form_label("Distributor", "armstrong_2_distributors_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 376
        echo "                        ";
        echo form_input(array("name" => "armstrong_2_distributors_id_", "value" => $this->getAttribute((isset($context["distributors"]) ? $context["distributors"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_distributors_id", array()), array(), "array"), "class" => "form-control", "id" => "InputDistributors"));
        // line 379
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_distributors_id\" id=\"InputDistributorsId\" value=\"";
        // line 380
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_distributors_id", array()), "html", null, true);
        echo "\" />
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
        // line 385
        echo form_label("Call Record", "armstrong_2_call_records_id", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 388
        echo "                        ";
        echo form_input(array("name" => "armstrong_2_call_records_id_", "value" => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_call_records_id", array()), "class" => "form-control", "id" => "InputCallRecords"));
        // line 391
        echo "
                        <input type=\"hidden\" name=\"armstrong_2_call_records_id\" id=\"InputCallRecordsId\" value=\"";
        // line 392
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "armstrong_2_call_records_id", array()), "html", null, true);
        echo "\" />
                    </div>
                </div>


                ";
        // line 397
        $context["productTitle"] = ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 6))) ? ("name_alt") : ("sku_name"));
        // line 398
        echo "                ";
        if ($this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "id", array())) {
            echo "          
\t\t\t\t\t";
            // line 399
            if ((twig_length_filter($this->env, (isset($context["products_array"]) ? $context["products_array"] : null)) > 0)) {
                // line 400
                echo "\t\t\t\t\t\t";
                // line 421
                echo "
\t\t\t\t\t\t";
                // line 422
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["products_array"]) ? $context["products_array"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["ssd_product"]) {
                    // line 423
                    echo "\t\t\t\t\t\t\t<div class=\"form-group sku-base\">
\t\t\t\t\t\t\t\t";
                    // line 424
                    echo form_label("Product", "sku_number_", array("class" => "control-label col-sm-3"));
                    echo "
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">                        
\t\t\t\t\t\t\t\t\t";
                    // line 426
                    echo form_input(array("name" => "sku_number_", "value" => (($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array")) ? (get_product_label($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), (isset($context["productTitle"]) ? $context["productTitle"] : null))) : ("")), "class" => "form-control product-tokenfield"));
                    // line 428
                    echo "
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"ssd_update[";
                    // line 429
                    echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "id", array()), "html", null, true);
                    echo "][sku_number]\" class=\"InputProductId\" value=\"";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" data-price=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), "price", array()), "html", null, true);
                    echo "\" data-pcsprice=\"";
                    echo twig_escape_filter($this->env, (((($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), "price", array()) > 0) && ($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), "quantity_case", array()) > 0))) ? (($this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), "price", array()) / $this->getAttribute($this->getAttribute((isset($context["products"]) ? $context["products"] : null), $context["key"], array(), "array"), "quantity_case", array()))) : (0)), "html", null, true);
                    echo "\" />
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"ssd_update[";
                    // line 430
                    echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "id", array()), "html", null, true);
                    echo "][id]\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "id", array()), "html", null, true);
                    echo "\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".sku-base\"><i class=\"fa fa-plus\"></i></button>
\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default btn-clone remove-clone-btn\" data-clone=\".sku-base\"><i class=\"fa fa-minus\"></i></button>
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" data-name=\"ssd_delete[]\" data-target=\"#SsdDelete\" class=\"clone-move\" value=\"";
                    // line 435
                    echo twig_escape_filter($this->env, $this->getAttribute($context["ssd_product"], "id", array()), "html", null, true);
                    echo "\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
\t\t\t\t\t\t\t\t\tQty case ";
                    // line 438
                    echo form_input(array("name" => (("ssd_update[" . $this->getAttribute($context["ssd_product"], "id", array())) . "][qty_cases]"), "value" => $this->getAttribute($context["ssd_product"], "qty_cases", array()), "class" => "form-control sub-input qty-case", "maxlength" => 3, "data-parsley-required" => "true"));
                    echo "&nbsp; 
\t\t\t\t\t\t\t\t\tQty pcs ";
                    // line 439
                    echo form_input(array("name" => (("ssd_update[" . $this->getAttribute($context["ssd_product"], "id", array())) . "][qty_pcs]"), "value" => $this->getAttribute($context["ssd_product"], "qty_pcs", array()), "class" => "form-control sub-input qty-pcs", "maxlength" => 3, "data-parsley-required" => "true"));
                    echo "&nbsp;
\t\t\t\t\t\t\t\t\tFree case ";
                    // line 440
                    echo form_input(array("name" => (("ssd_update[" . $this->getAttribute($context["ssd_product"], "id", array())) . "][free_qty_cases]"), "value" => $this->getAttribute($context["ssd_product"], "free_qty_cases", array()), "class" => "form-control sub-input", "maxlength" => 3));
                    echo "&nbsp;
\t\t\t\t\t\t\t\t\tFree pcs ";
                    // line 441
                    echo form_input(array("name" => (("ssd_update[" . $this->getAttribute($context["ssd_product"], "id", array())) . "][free_qty_pcs]"), "value" => $this->getAttribute($context["ssd_product"], "free_qty_pcs", array()), "class" => "form-control sub-input", "maxlength" => 3));
                    echo "
\t\t\t\t\t\t\t\t\tTotal ";
                    // line 442
                    echo form_input(array("name" => (("ssd_update[" . $this->getAttribute($context["ssd_product"], "id", array())) . "][total_price]"), "value" => twig_number_format_filter($this->env, $this->getAttribute($context["ssd_product"], "total_price", array()), 2), "class" => "form-control sub-input total-price", "readonly" => "readonly", "style" => "width: 15%"));
                    // line 446
                    echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['ssd_product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 450
                echo "\t\t\t\t\t";
            } else {
                // line 451
                echo "\t\t\t\t\t\t<div class=\"form-group sku-base\">
\t\t\t\t\t\t\t";
                // line 452
                echo form_label("Product", "sku_number_", array("class" => "control-label col-sm-3"));
                echo "
\t\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t";
                // line 460
                echo "
\t\t\t\t\t\t\t\t";
                // line 461
                echo form_input(array("name" => "sku_number_", "value" => null, "class" => "form-control product-tokenfield", "data-parsley-required" => "true"));
                // line 464
                echo "
\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"products[0][sku_number]\" class=\"InputProductId\" value=\"\" data-price=\"0\" data-pcsprice=\"0\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".sku-base\"><i class=\"fa fa-plus\"></i></button>
\t\t\t\t\t\t\t\t<button class=\"btn btn-default btn-clone remove-clone-btn\" data-clone=\".sku-base\" style=\"display: none;\"><i class=\"fa fa-minus\"></i></button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
\t\t\t\t\t\t\t\tQty case ";
                // line 472
                echo form_input(array("name" => "products[0][qty_cases]", "value" => null, "class" => "form-control sub-input qty-case", "maxlength" => 3, "data-parsley-required" => "true"));
                echo "&nbsp; 
\t\t\t\t\t\t\t\tQty pcs ";
                // line 473
                echo form_input(array("name" => "products[0][qty_pcs]", "value" => null, "class" => "form-control sub-input qty-pcs", "maxlength" => 3, "data-parsley-required" => "true"));
                echo "&nbsp;
\t\t\t\t\t\t\t\tFree case ";
                // line 474
                echo form_input(array("name" => "products[0][free_qty_cases]", "value" => null, "class" => "form-control sub-input", "maxlength" => 3));
                echo "&nbsp;
\t\t\t\t\t\t\t\tFree pcs ";
                // line 475
                echo form_input(array("name" => "products[0][free_qty_pcs]", "value" => null, "class" => "form-control sub-input", "maxlength" => 3));
                echo "
\t\t\t\t\t\t\t\tTotal ";
                // line 476
                echo form_input(array("name" => "products[0][total_price]", "value" => 0, "class" => "form-control sub-input total-price", "readonly" => "readonly", "style" => "width: 15%"));
                // line 480
                echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
            }
            // line 484
            echo "                ";
        } else {
            // line 485
            echo "                    <div class=\"form-group sku-base\">
                        ";
            // line 486
            echo form_label("Product", "sku_number_", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 494
            echo "
                            ";
            // line 495
            echo form_input(array("name" => "sku_number_", "value" => null, "class" => "form-control product-tokenfield", "data-parsley-required" => "true"));
            // line 498
            echo "
                            <input type=\"hidden\" name=\"products[0][sku_number]\" class=\"InputProductId\" value=\"\" data-price=\"0\" data-pcsprice=\"0\" />
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-btn\" data-clone=\".sku-base\"><i class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone remove-clone-btn\" data-clone=\".sku-base\" style=\"display: none;\"><i class=\"fa fa-minus\"></i></button>
                        </div>
                        <div class=\"col-sm-offset-3 col-sm-9 m-t no-parsley\">
                            Qty case ";
            // line 506
            echo form_input(array("name" => "products[0][qty_cases]", "value" => null, "class" => "form-control sub-input qty-case", "maxlength" => 3, "data-parsley-required" => "true"));
            echo "&nbsp; 
                            Qty pcs ";
            // line 507
            echo form_input(array("name" => "products[0][qty_pcs]", "value" => null, "class" => "form-control sub-input qty-pcs", "maxlength" => 3, "data-parsley-required" => "true"));
            echo "&nbsp;
                            Free case ";
            // line 508
            echo form_input(array("name" => "products[0][free_qty_cases]", "value" => null, "class" => "form-control sub-input", "maxlength" => 3));
            echo "&nbsp;
                            Free pcs ";
            // line 509
            echo form_input(array("name" => "products[0][free_qty_pcs]", "value" => null, "class" => "form-control sub-input", "maxlength" => 3));
            echo "
                            Total ";
            // line 510
            echo form_input(array("name" => "products[0][total_price]", "value" => 0, "class" => "form-control sub-input total-price", "readonly" => "readonly", "style" => "width: 15%"));
            // line 514
            echo "
                        </div>
                    </div>
                ";
        }
        // line 517
        echo "  

                <div id=\"SsdDelete\"></div>             

                ";
        // line 529
        echo "
                <div class=\"form-group\">
                    ";
        // line 531
        echo form_label("Method", "method", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 533
        echo form_dropdown("method", (isset($context["ssd_method"]) ? $context["ssd_method"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "method", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                    </div>
                </div>
            <div class=\"form-group\">
                ";
        // line 537
        echo form_label("Status", "status", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 539
        echo form_dropdown("status", (isset($context["ssd_status"]) ? $context["ssd_status"] : null), $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "status", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
                <div class=\"form-group\">
                    ";
        // line 543
        echo form_label("SSD Date", "ssd_date", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 545
        echo form_input(array("name" => "ssd_date", "value" => $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "ssd_date", array())), "method"), "toDateTimeString", array(), "method"), "class" => "form-control ssd-datetimepicker"));
        // line 547
        echo "
                    </div>
                </div>                

                <div class=\"form-group\">
                    ";
        // line 552
        echo form_label("Month", "ssd_month", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 555
        echo "                        ";
        echo form_input(array("name" => "ssd_month", "value" => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "ssd_month", array()), "class" => "form-control", "data-parsley-type" => "digits", "readonly" => "readonly", "id" => "InputMonth"));
        // line 560
        echo "
                    </div>
                </div>  

                <div class=\"form-group\">
                    ";
        // line 565
        echo form_label("Year", "ssd_year", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 568
        echo "                        ";
        echo form_input(array("name" => "ssd_year", "value" => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "ssd_year", array()), "class" => "form-control", "data-parsley-type" => "digits", "readonly" => "readonly", "id" => "InputYear"));
        // line 573
        echo "
                    </div>
                </div>               

                <!--div class=\"form-group\">
                    ";
        // line 578
        echo form_label("Date Sync", "date_sync", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 580
        echo form_input(array("name" => "date_sync", "value" => $this->getAttribute($this->getAttribute((isset($context["Carbon"]) ? $context["Carbon"] : null), "parse", array(0 => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "date_sync", array())), "method"), "toDateTimeString", array(), "method"), "class" => "form-control datetimepicker"));
        // line 582
        echo "
                    </div>
                </div-->

                <div class=\"form-group\">
                    ";
        // line 587
        echo form_label("Remarks", "remarks", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 589
        echo form_textarea(array("name" => "remarks", "value" => $this->getAttribute((isset($context["ssd"]) ? $context["ssd"] : null), "remarks", array()), "class" => "form-control", "rows" => 3));
        echo "
                    </div>
                </div>

                ";
        // line 598
        echo "                  
        </div>
    </div> 

    ";
        // line 602
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/ssd/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  840 => 602,  834 => 598,  827 => 589,  822 => 587,  815 => 582,  813 => 580,  808 => 578,  801 => 573,  798 => 568,  793 => 565,  786 => 560,  783 => 555,  778 => 552,  771 => 547,  769 => 545,  764 => 543,  757 => 539,  752 => 537,  745 => 533,  740 => 531,  736 => 529,  730 => 517,  724 => 514,  722 => 510,  718 => 509,  714 => 508,  710 => 507,  706 => 506,  696 => 498,  694 => 495,  691 => 494,  686 => 486,  683 => 485,  680 => 484,  674 => 480,  672 => 476,  668 => 475,  664 => 474,  660 => 473,  656 => 472,  646 => 464,  644 => 461,  641 => 460,  636 => 452,  633 => 451,  630 => 450,  621 => 446,  619 => 442,  615 => 441,  611 => 440,  607 => 439,  603 => 438,  597 => 435,  587 => 430,  577 => 429,  574 => 428,  572 => 426,  567 => 424,  564 => 423,  560 => 422,  557 => 421,  555 => 400,  553 => 399,  548 => 398,  546 => 397,  538 => 392,  535 => 391,  532 => 388,  527 => 385,  519 => 380,  516 => 379,  513 => 376,  508 => 369,  500 => 364,  497 => 363,  494 => 360,  489 => 353,  481 => 348,  478 => 347,  475 => 343,  470 => 336,  463 => 331,  456 => 326,  451 => 324,  447 => 322,  440 => 319,  435 => 317,  432 => 316,  430 => 315,  418 => 305,  412 => 304,  410 => 303,  405 => 301,  402 => 300,  397 => 298,  393 => 297,  389 => 296,  386 => 295,  377 => 287,  375 => 286,  374 => 285,  373 => 284,  370 => 283,  367 => 282,  361 => 279,  357 => 278,  354 => 277,  261 => 187,  255 => 184,  240 => 172,  211 => 146,  205 => 143,  176 => 117,  170 => 114,  148 => 95,  125 => 75,  78 => 31,  62 => 18,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
