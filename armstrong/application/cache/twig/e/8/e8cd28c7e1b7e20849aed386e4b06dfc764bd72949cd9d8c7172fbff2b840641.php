<?php

/* ami/setting_country/edit.html.twig */
class __TwigTemplate_e8cd28c7e1b7e20849aed386e4b06dfc764bd72949cd9d8c7172fbff2b840641 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/setting_country/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_js($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/ami/setting_country.js"), "html", null, true);
        echo "\"></script>
    <script>
        CKEDITOR.replace('inputPersonalPrivacyNotes', {
            customConfig: '";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });
        CKEDITOR.replace('visitSummaryPull', {
            customConfig: '";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });
        CKEDITOR.replace('visitSummaryPush', {
            customConfig: '";
        // line 14
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });
        function validateProductInput() {
            var validate = false;
            \$.each(\$('select[name=\"sku_number_\"]'), function () {
                if (\$(this).val() !== '') {
                    validate = true;
                }
            });
            return validate;
        }
        \$(document).ready(function () {
            \$('.price-yes').click(function () {
                var modalElement = \$(this).data('type');
                \$('#price_' + modalElement).modal();
            });

            \$('.price-no').click(function () {
                var modalElement = \$(this).data('type');
                \$('#price_' + modalElement + ' input[type=\"checkbox\"]').prop('checked', false);
            });

            \$('.modal-price-item').on('shown.bs.modal', function (e) {
                var numberChecker = \$(this).find('input[type=\"checkbox\"]:checked').length;
                if (numberChecker == 0) {
                    \$(this).find('input[type=\"checkbox\"]').prop('checked', true);
                }
            });

            \$('.modal-price-item').on('hidden.bs.modal', function (e) {
                var numberChecker = \$(this).find('input[type=\"checkbox\"]:checked').length,
                        type = \$(this).data('type');
                if (numberChecker == 0) {
                    \$('input[name=\"price_config[' + type + '][enable]\"].price-yes').prop('checked', false);
                    \$('input[name=\"price_config[' + type + '][enable]\"].price-no').prop('checked', true);
                }
            });

            var original_checked_number = \$('.datasourcepull input[type=\"checkbox\"]:checked').length;
            if (original_checked_number == 0) {
                \$('.datasourcepull input[type=\"checkbox\"]').prop('checked', true);
            }
            \$('.datasourcepull input[type=\"checkbox\"]').click(function () {
                var checked_number = \$('.datasourcepull input[type=\"checkbox\"]:checked').length;
                if (checked_number == 0) {
                    alert('Please check at least one item!');
                    \$(this).prop('checked', true);
                }
            });

            var original_checked_number = \$('.datasourcepush input[type=\"checkbox\"]:checked').length;
            if (original_checked_number == 0) {
                \$('.datasourcepush input[type=\"checkbox\"]').prop('checked', true);
            }
            \$('.datasourcepush input[type=\"checkbox\"]').click(function () {
                var checked_number = \$('.datasourcepush input[type=\"checkbox\"]:checked').length;
                if (checked_number == 0) {
                    alert('Please check at least one item!');
                    \$(this).prop('checked', true);
                }
            });

            function loadshifted(){
                var enable_shifted_call = \$('.enable_shifted_call input[type=\"radio\"]:checked').val();
                if (enable_shifted_call == \"0\") {
                    \$('.require_reason input').attr('disabled', 'disabled');
                }
                else {
                    \$('.require_reason input').removeAttr('disabled');
                }
            }
            \$('.enable_shifted_call input[type=\"radio\"]').click(function () {
                loadshifted();
            });
            loadshifted();

            var counter = \$('.InputProductId').length - 1;
            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$block = \$this.closest('.areas-block'),
                        \$clone = \$this.closest('.sku-base').clone();
                \$clone.find('.hide').removeClass('hide').show();

                if (\$this.hasClass('clone-add')) {
                    \$new = \$clone.clone();
                    counter++;
                    \$new.find('input[name], select[name]').each(function () {
                        var \$this = \$(this);
                        \$this.attr('name', \$this.attr('name').replace(/\\[(\\d+)\\]/, '[' + counter + ']'));

                        if (\$this.hasClass('product-tokenfield') || \$this.is('input:text')) {
                            \$this.val('');
                        }
                        else {
                            \$this.val(0);
                        }
                    });
                    \$new.find('ul.parsley-errors-list').remove();
                    \$new.appendTo(\$block.find('.base-block'));
                }

                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.sku-base').remove();
                    var validate = validateProductInput();
                    if (!validate) {
                        \$('select[name=\"sku_number_\"]').first().attr('data-parsley-required', 'true');
                    }
                }
            });
        });
    </script>
";
    }

    // line 128
    public function block_css($context, array $blocks = array())
    {
        // line 129
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style>
        .p-l-0 {
            padding-left: 0;
        }

        .title-left {
            text-align: left !important;
            margin-left: 20px;
        }
    </style>
";
    }

    // line 141
    public function block_content($context, array $blocks = array())
    {
        // line 142
        echo "    ";
        echo form_open(site_url("ami/setting_country/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true", "id" => "country_settings"), array("id" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Setting Country</h1>

                <div class=\"text-right\">
                    ";
        // line 150
        $this->loadTemplate("ami/components/form_btn_save.html.twig", "ami/setting_country/edit.html.twig", 150)->display(array_merge($context, array("url" => "ami/setting_country", "id" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "id", array()), "permission" => "setting_country")));
        // line 151
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" class=\"active\">
                    <a href=\"#listed\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">News Feed</a>
                </li>
                <li role=\"presentation\">
                    <a href=\"#unlisted\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Business Objective</a>
                </li>
                <li role=\"presentation\">
                    <a href=\"#privacy\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Privacy</a>
                </li>
                <li role=\"presentation\">
                    <a href=\"#customer_approval\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Customer
                        Approval</a>
                </li>
                <li role=\"presentation\">
                    <a href=\"#working_day\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Working Day</a>
                </li>
                <!-- <li role=\"presentation\">
                    <a href=\"#coaching_forms\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Coaching Config</a>
                </li> -->
                <li role=\"presentation\">
                    <a href=\"#app_config\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Application Config</a>
                </li>
                ";
        // line 183
        echo "                ";
        // line 184
        echo "                <li role=\"presentation\">
                    <a href=\"#customer_history\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Customer
                        History Config</a>
                </li>
                ";
        // line 189
        echo "                <li role=\"presentation\">
                    <a href=\"#app_todo\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Todo</a>
                </li>
                <li role=\"presentation\">
                    <a href=\"#other\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Other</a>
                </li>
            </ul>
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <div class=\"form-group\">
                            ";
        // line 200
        echo form_label("To approval", "approval_news_feed", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6 no-parsley\">
                                <label class=\"radio-inline\">";
        // line 202
        echo form_radio("approval_news_feed", 0, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "approval_news_feed", array()) == 0));
        echo "
                                    Only Local</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 204
        echo form_radio("approval_news_feed", 1, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "approval_news_feed", array()) == 1));
        echo "
                                    Only Global</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 206
        echo form_radio("approval_news_feed", 2, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "approval_news_feed", array()) == 2));
        echo "
                                    Both</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 208
        echo form_radio("approval_news_feed", 3, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "approval_news_feed", array()) == 3));
        echo "
                                    None</label>
                            </div>
                        </div>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"unlisted\">
                        <div class=\"form-group\">
                            ";
        // line 215
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "mandatory_incall_tabs", array(), "array"));
        foreach ($context['_seq'] as $context["key"] => $context["post"]) {
            // line 216
            echo "                                <div class=\"form-group\">
                                    ";
            // line 217
            echo form_label($this->getAttribute($context["post"], "title", array()), $this->getAttribute($context["post"], "title", array()), array("class" => "control-label col-sm-3"));
            echo "
                                    <div class=\"col-sm-6 no-parsley\">
                                        <label class=\"radio-inline\">";
            // line 219
            echo form_radio((("mandatory_incall_tabs[" . twig_lower_filter($this->env, $context["key"])) . "]"), 1, ($this->getAttribute($context["post"], "mandatory", array()) == 1));
            echo "
                                            Active</label>&nbsp;
                                        <label class=\"radio-inline\">";
            // line 221
            echo form_radio((("mandatory_incall_tabs[" . twig_lower_filter($this->env, $context["key"])) . "]"), 0, ($this->getAttribute($context["post"], "mandatory", array()) == 0));
            echo "
                                            Inactive</label>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 226
        echo "
                        </div>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"privacy\">
                        <div class=\"form-group\">
                            ";
        // line 231
        echo form_label("Personal Privacy Notes", "personal_privacy_notes", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6\">
                                ";
        // line 233
        echo form_textarea(array("name" => "application_config[pull][privacy][personal_privacy_notes]", "value" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "privacy", array()), "personal_privacy_notes", array()), "class" => "form-control input-xxlarge", "id" => "inputPersonalPrivacyNotes"));
        // line 235
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            ";
        // line 239
        echo form_label("Show Accept", "personal_privacy_notes_show_accept", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6 no-parsley\">
                                <label class=\"radio-inline\">";
        // line 241
        echo form_radio("application_config[pull][privacy][personal_privacy_notes_show_accept]", 1, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "privacy", array()), "personal_privacy_notes_show_accept", array()) == 1));
        echo "
                                    Yes</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 243
        echo form_radio("application_config[pull][privacy][personal_privacy_notes_show_accept]", 0, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "privacy", array()), "personal_privacy_notes_show_accept", array()) == 0));
        echo "
                                    No</label>
                            </div>
                        </div>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"customer_approval\">
                        <div class=\"form-group\">
                            ";
        // line 250
        echo form_label("New Customer Auto-approval", "approve_customers", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6 no-parsley\">
                                <label class=\"radio-inline\">";
        // line 252
        echo form_radio("approve_customers[add]", 0, ($this->getAttribute((isset($context["approve_customers"]) ? $context["approve_customers"] : null), "add", array()) == 0));
        echo "
                                    Yes</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 254
        echo form_radio("approve_customers[add]", 1, ($this->getAttribute((isset($context["approve_customers"]) ? $context["approve_customers"] : null), "add", array()) == 1));
        echo "
                                    No</label>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            ";
        // line 259
        echo form_label("Edit Customer Auto-approval", "approve_customers", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6 no-parsley\">
                                <label class=\"radio-inline\">";
        // line 261
        echo form_radio("approve_customers[edit]", 0, ($this->getAttribute((isset($context["approve_customers"]) ? $context["approve_customers"] : null), "edit", array()) == 0));
        echo "
                                    Yes</label>&nbsp;
                                <label class=\"radio-inline\">";
        // line 263
        echo form_radio("approve_customers[edit]", 1, ($this->getAttribute((isset($context["approve_customers"]) ? $context["approve_customers"] : null), "edit", array()) == 1));
        echo "
                                    No</label>
                            </div>
                        </div>
                    </div>
                    ";
        // line 269
        echo "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"customer_history\">
                        ";
        // line 271
        echo "                        <ul class=\"nav nav-tabs\" role=\"tablist\">
                            <li role=\"presentation\" class=\"active\">
                                <a href=\"#customer_history_pull\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">PULL</a>
                            </li>
                            <li role=\"presentation\">
                                <a href=\"#customer_history_push\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">PUSH</a>
                            </li>
                        </ul>
                        <div class=\"panel panel-default m-t\">
                            <div class=\"panel-body tab-content\">
                                <div role=\"tabpanel\" class=\"tab-pane active\" id=\"customer_history_pull\">
                                    <div class=\"form-group\">
                                        <div class=\"form-group\">
                                            ";
        // line 284
        echo form_label("Default Call History", "isCallHistoryDefault", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-6 no-parsley\">
                                                <label class=\"radio-inline\">";
        // line 286
        echo form_radio("customer_history_config[pull][isCallHistoryDefault]", 1, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "pull", array()), "isCallHistoryDefault", array()) == 1), "checked = TRUE");
        echo "
                                                    Yes</label>&nbsp;
                                                <label class=\"radio-inline\">";
        // line 288
        echo form_radio("customer_history_config[pull][isCallHistoryDefault]", 0, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "pull", array()), "isCallHistoryDefault", array()) == 0));
        echo "
                                                    No</label>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            ";
        // line 293
        echo form_label("View by Delivery Date", "viewByDeliveryDate", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-6 no-parsley\">
                                                <label class=\"radio-inline\">";
        // line 295
        echo form_radio("customer_history_config[pull][viewByDeliveryDate]", 1, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "pull", array()), "viewByDeliveryDate", array()) == 1));
        echo "
                                                    Yes</label>&nbsp;
                                                <label class=\"radio-inline\">";
        // line 297
        echo form_radio("customer_history_config[pull][viewByDeliveryDate]", 0, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "pull", array()), "viewByDeliveryDate", array()) == 0));
        echo "
                                                    No</label>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            ";
        // line 302
        echo form_label("Sort Order Record By", "sortOrderBy", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-3 no-parsley\">
                                                ";
        // line 304
        echo form_dropdown("customer_history_config[pull][sortOrderBy]", (isset($context["customer_history_sortBy"]) ? $context["customer_history_sortBy"] : null), $this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "pull", array()), "sortOrderBy", array()), "class=\"form-control\"");
        echo "
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role=\"tabpanel\" class=\"tab-pane\" id=\"customer_history_push\">
                                    <div class=\"form-group\">
                                        <div class=\"form-group\">
                                            ";
        // line 312
        echo form_label("Default Call History", "isCallHistoryDefault", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-6 no-parsley\">
                                                <label class=\"radio-inline\">";
        // line 314
        echo form_radio("customer_history_config[push][isCallHistoryDefault]", 1, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "push", array()), "isCallHistoryDefault", array()) == 1), "checked = TRUE");
        echo "
                                                    Yes</label>&nbsp;
                                                <label class=\"radio-inline\">";
        // line 316
        echo form_radio("customer_history_config[push][isCallHistoryDefault]", 0, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "push", array()), "isCallHistoryDefault", array()) == 0));
        echo "
                                                    No</label>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            ";
        // line 321
        echo form_label("View by Delivery Date", "viewByDeliveryDate", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-6 no-parsley\">
                                                <label class=\"radio-inline\">";
        // line 323
        echo form_radio("customer_history_config[push][viewByDeliveryDate]", 1, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "push", array()), "viewByDeliveryDate", array()) == 1));
        echo "
                                                    Yes</label>&nbsp;
                                                <label class=\"radio-inline\">";
        // line 325
        echo form_radio("customer_history_config[push][viewByDeliveryDate]", 0, ($this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "push", array()), "viewByDeliveryDate", array()) == 0));
        echo "
                                                    No</label>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            ";
        // line 330
        echo form_label("Sort Order Record By", "sortOrderBy", array("class" => "control-label col-sm-3"));
        echo "
                                            <div class=\"col-sm-3 no-parsley\">
                                                ";
        // line 332
        echo form_dropdown("customer_history_config[push][sortOrderBy]", (isset($context["customer_history_sortBy"]) ? $context["customer_history_sortBy"] : null), $this->getAttribute($this->getAttribute((isset($context["customer_history_config"]) ? $context["customer_history_config"] : null), "push", array()), "sortOrderBy", array()), "class=\"form-control\"");
        echo "
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ";
        // line 365
        echo "                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"app_todo\">
                        <div class=\"form-group\">
                            ";
        // line 368
        echo form_label("Interval Reminder", "interval_reminder", array("class" => "control-label col-sm-3"));
        echo "
                            <div class=\"col-sm-6 \">
                                ";
        // line 370
        echo form_input(array("name" => "interval_reminder", "value" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "interval_reminder", array()), "class" => "form-control", "data-parsley-required" => "true", "maxlength" => 5));
        echo "
                            </div>
                        </div>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"other\">
                        <div class=\"form-group\">
                            <div class=\"form-group\">
                                ";
        // line 377
        echo form_label("Client Time Out", "client_time_out", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6\">
                                    ";
        // line 379
        echo form_input(array("name" => "client_time_out", "value" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "client_time_out", array()), "class" => "form-control", "data-parsley-required" => "true", "maxlength" => 5));
        echo "
                                </div>
                            </div>
                            <div class=\"form-group\">
                                ";
        // line 383
        echo form_label("Criteria of Store Check", "client_time_out", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6\">
                                    ";
        // line 385
        echo form_input(array("name" => "criteria_of_sc", "value" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "criteria_of_sc", array()), "class" => "form-control", "data-parsley-required" => "true", "maxlength" => 2));
        echo "
                                </div>
                            </div>
                            <div class=\"form-group\">
                                ";
        // line 389
        echo form_label("Survey Hash Key", "survey_key", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6\">
                                    ";
        // line 391
        echo form_input(array("name" => "survey_key", "value" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "survey_key", array()), "class" => "form-control"));
        echo "
                                </div>
                            </div>
                            <div class=\"form-group\">
                                ";
        // line 395
        echo form_label("Enable Telesales", "enable_telesales", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6 no-parsley\">
                                    <label class=\"radio-inline\">";
        // line 397
        echo form_radio("enable_telesales", 1, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "enable_telesales", array()) == 1));
        echo "
                                        Yes</label>&nbsp;
                                    <label class=\"radio-inline\">";
        // line 399
        echo form_radio("enable_telesales", 0, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "enable_telesales", array()) == 0));
        echo "
                                        No</label>
                                </div>
                            </div>
                            <div class=\"form-group\">
                                ";
        // line 404
        echo form_label("Enable SMS", "enable_sms_report", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6 no-parsley\">
                                    <label class=\"radio-inline\">";
        // line 406
        echo form_radio("enable_sms_report", 1, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "enable_sms_report", array()) == 1));
        echo "
                                        Yes</label>&nbsp;
                                    <label class=\"radio-inline\">";
        // line 408
        echo form_radio("enable_sms_report", 0, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "enable_sms_report", array()) == 0));
        echo "
                                        No</label>
                                </div>
                            </div>

                            <div class=\"form-group\">
                                ";
        // line 414
        echo form_label("Enable Promotional Activity", "promotion_activity", array("class" => "control-label col-sm-3"));
        echo "
                                <div class=\"col-sm-6 no-parsley\">
                                    <label class=\"radio-inline\">";
        // line 416
        echo form_radio("promotion_activity", 1, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "promotion_activity", array()) == 1));
        echo "
                                        Yes</label>&nbsp;
                                    <label class=\"radio-inline\">";
        // line 418
        echo form_radio("promotion_activity", 0, ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "promotion_activity", array()) == 0));
        echo "
                                        No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"working_day\">
                        <table class=\"table\">
                            <thead>
                            <tr>
                                <th class=\"text-center\">Monday</th>
                                <th class=\"text-center\">Tuesday</th>
                                <th class=\"text-center\">Wednesday</th>
                                <th class=\"text-center\">Thursday</th>
                                <th class=\"text-center\">Friday</th>
                                <th class=\"text-center\">Saturday</th>
                                <th class=\"text-center\">Sunday</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"2\" ";
        // line 439
        echo ((twig_in_filter(2, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"3\" ";
        // line 440
        echo ((twig_in_filter(3, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"4\" ";
        // line 441
        echo ((twig_in_filter(4, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"5\" ";
        // line 442
        echo ((twig_in_filter(5, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"6\" ";
        // line 443
        echo ((twig_in_filter(6, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"7\" ";
        // line 444
        echo ((twig_in_filter(7, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                                <td class=\"text-center\"><div><input type=\"checkbox\" name=\"application_config[pull][working_day][]\" value=\"1\" ";
        // line 445
        echo ((twig_in_filter(1, $this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "working_day", array()))) ? ("checked") : (""));
        echo "></div></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    ";
        // line 450
        $this->loadTemplate("ami/setting_country/items/application_setting.html.twig", "ami/setting_country/edit.html.twig", 450)->display(array_merge($context, array("application_config" => (isset($context["application_config"]) ? $context["application_config"] : null))));
        // line 451
        echo "                    ";
        $this->loadTemplate("ami/setting_country/items/coaching_form_setting.html.twig", "ami/setting_country/edit.html.twig", 451)->display(array_merge($context, array("application_config" => (isset($context["application_config"]) ? $context["application_config"] : null))));
        // line 452
        echo "                </div>
            </div>
        </div>
    </div>
    <div id=\"price_pull\" class=\"modal fade modal-price-item\" data-type=\"pull\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog modal-sm\">
            <div class=\"modal-content\" style=\"padding: 10px\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Show/Hide Items</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group checkbox\">
                        ";
        // line 466
        echo form_label("Case", "case", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[pull][price_config][case]\" ";
        // line 470
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "case", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 476
        echo form_label("Piece", "piece", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[pull][price_config][piece]\" ";
        // line 480
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "piece", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 486
        echo form_label("Total", "total", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[pull][price_config][total]\" ";
        // line 490
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "pull", array()), "price_config", array()), "total", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id=\"price_push\" class=\"modal fade modal-price-item\" data-type=\"push\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog modal-sm\">
            <div class=\"modal-content\" style=\"padding: 10px\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Show/Hide Items</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group checkbox\">
                        ";
        // line 510
        echo form_label("Case", "case", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[push][price_config][case]\" ";
        // line 514
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "case", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 520
        echo form_label("Piece", "piece", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[push][price_config][piece]\" ";
        // line 524
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "piece", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 530
        echo form_label("Total", "total", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[push][price_config][total]\" ";
        // line 534
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "push", array()), "price_config", array()), "total", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id=\"price_leader\" class=\"modal fade modal-price-item\" data-type=\"leader\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog modal-sm\">
            <div class=\"modal-content\" style=\"padding: 10px\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\">Show/Hide Items</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group checkbox\">
                        ";
        // line 554
        echo form_label("Case", "case", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[leader][price_config][case]\" ";
        // line 558
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "case", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 564
        echo form_label("Piece", "piece", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[leader][price_config][piece]\" ";
        // line 568
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "piece", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                    <div class=\"form-group checkbox\">
                        ";
        // line 574
        echo form_label("Total", "total", array("class" => "col-sm-5 col-sm-offset-2"));
        echo "
                        <div class=\"col-sm-5 no-parsley\">
                            <label>
                                <input type=\"checkbox\"
                                       name=\"application_config[leader][price_config][total]\" ";
        // line 578
        echo ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["application_config"]) ? $context["application_config"] : null), "leader", array()), "price_config", array()), "total", array()) == 1)) ? ("checked") : (""));
        echo "
                                       value=\"1\">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 587
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/setting_country/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  879 => 587,  867 => 578,  860 => 574,  851 => 568,  844 => 564,  835 => 558,  828 => 554,  805 => 534,  798 => 530,  789 => 524,  782 => 520,  773 => 514,  766 => 510,  743 => 490,  736 => 486,  727 => 480,  720 => 476,  711 => 470,  704 => 466,  688 => 452,  685 => 451,  683 => 450,  675 => 445,  671 => 444,  667 => 443,  663 => 442,  659 => 441,  655 => 440,  651 => 439,  627 => 418,  622 => 416,  617 => 414,  608 => 408,  603 => 406,  598 => 404,  590 => 399,  585 => 397,  580 => 395,  573 => 391,  568 => 389,  561 => 385,  556 => 383,  549 => 379,  544 => 377,  534 => 370,  529 => 368,  524 => 365,  514 => 332,  509 => 330,  501 => 325,  496 => 323,  491 => 321,  483 => 316,  478 => 314,  473 => 312,  462 => 304,  457 => 302,  449 => 297,  444 => 295,  439 => 293,  431 => 288,  426 => 286,  421 => 284,  406 => 271,  403 => 269,  395 => 263,  390 => 261,  385 => 259,  377 => 254,  372 => 252,  367 => 250,  357 => 243,  352 => 241,  347 => 239,  341 => 235,  339 => 233,  334 => 231,  327 => 226,  316 => 221,  311 => 219,  306 => 217,  303 => 216,  299 => 215,  289 => 208,  284 => 206,  279 => 204,  274 => 202,  269 => 200,  256 => 189,  250 => 184,  248 => 183,  215 => 151,  213 => 150,  201 => 142,  198 => 141,  181 => 129,  178 => 128,  60 => 14,  54 => 11,  48 => 8,  42 => 5,  38 => 4,  33 => 3,  30 => 2,  11 => 1,);
    }
}
