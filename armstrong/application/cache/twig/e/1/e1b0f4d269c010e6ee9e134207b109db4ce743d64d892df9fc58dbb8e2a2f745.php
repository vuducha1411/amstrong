<?php

/* ami/classification/index.html.twig */
class __TwigTemplate_e1b0f4d269c010e6ee9e134207b109db4ce743d64d892df9fc58dbb8e2a2f745 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/classification/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/classification/ajaxData"), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"id\", \"sWidth\": \"10%\"},
                    {mData: \"name\", \"sWidth\": \"20%\"},
                    {mData: \"from_date\"},
                    {mData: \"to_date\"}
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    ";
        // line 26
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "sales_cycle"))) {
            // line 27
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    ";
        }
        // line 29
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 120px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });
        });
    </script>
";
    }

    // line 45
    public function block_css($context, array $blocks = array())
    {
        // line 46
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 50
    public function block_content($context, array $blocks = array())
    {
        // line 51
        echo "
    ";
        // line 52
        echo form_open(site_url("ami/classification/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

       <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Classification</h1>

                <div class=\"text-right\">
\t\t\t\t\t<span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
\t\t\t\t\t\t";
        // line 61
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
\t\t\t\t\t</span>
                    ";
        // line 63
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/classification/index.html.twig", 63)->display(array_merge($context, array("url" => "ami/classification", "icon" => "fa-user", "title" => "Classification", "permission" => "classification", "showImport" => 0)));
        // line 64
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 81
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "classification"))) {
            // line 82
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 86
        echo "                                <th>ID</th>
                                <th>Name</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 105
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/classification/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 105,  164 => 86,  158 => 82,  156 => 81,  137 => 64,  135 => 63,  130 => 61,  118 => 52,  115 => 51,  112 => 50,  106 => 47,  101 => 46,  98 => 45,  80 => 29,  76 => 27,  74 => 26,  61 => 16,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
