<?php

/* ami/promotions/edit.html.twig */
class __TwigTemplate_e94a399f2ef2d799f43fe906f9fb247dc4e519ccaedfba973759d31e1b2eaeed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/promotions/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>

    <script>
        otm_val = '';
        \$(document).ready(function () {
            var type_val = \$('select[name=\"type\"]').val();
            if (type_val != 1) {
                \$('select[name=\"otm\"]').closest('div.form-group').hide();
                \$('select[name=\"otm\"]').val('');
            } else {
                otm_val = \$('select[name=\"otm\"]').val();
            }
        });

        \$(function () {
            createUploader(\$('#imageUpload'));
            createUploader(\$('#videoUpload'));
            createUploader(\$('#brochureUpload'));
            createUploader(\$('#specUpload'));
            createUploader(\$('#certUpload'));


            \$('select[name=\"type\"]').on('change', function (e) {
                var \$val = \$(this).val(),
                        otm = \$('select[name=\"otm\"]');
                if (\$val == 1) {
                    otm.val(otm_val);
                    otm.closest('div.form-group').show();
                } else {
                    otm.closest('div.form-group').hide();
                    otm.val('');
                }
            });

            \$('select[name=\"otm\"]').on('change', function (e) {
                otm_val = \$(this).val();
            });
        });

        CKEDITOR.replace('inputContent', {
            customConfig: '";
        // line 48
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.replace('inputDescription', {
            customConfig: '";
        // line 52
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.replace('inputNutrition', {
            customConfig: '";
        // line 56
        echo twig_escape_filter($this->env, site_url("res/js/ckeditor_config.js"), "html", null, true);
        echo "'
        });

        CKEDITOR.on('instanceReady', function () {
            \$.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on(\"change\", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                });
            });
        });
    </script>
";
    }

    // line 71
    public function block_content($context, array $blocks = array())
    {
        // line 72
        echo "
    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 87
        echo form_open(site_url("ami/promotions/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Promotions</h1>

                <div class=\"text-right\">
                    ";
        // line 95
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/promotions/edit.html.twig", 95)->display(array_merge($context, array("url" => "ami/promotions", "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()), "permission" => "promotions")));
        // line 96
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 105
        echo form_label("SKU Name", "sku_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 107
        echo form_input(array("name" => "sku_name", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sku_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 111
        echo form_label("SKU Code", "sku_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 113
        echo form_input(array("name" => "sku_code", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "sku_code", array()), "class" => "form-control"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 117
        echo form_label("Products", "products_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 119
        echo form_dropdown("products_id", (isset($context["products"]) ? $context["products"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "products_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 123
        echo form_label("SKU Name (Other Language)", "name_alt", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 125
        echo form_input(array("name" => "name_alt", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "name_alt", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 130
        echo form_label("Description", "description", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 132
        echo form_textarea(array("name" => "description", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "description", array()), "class" => "form-control input-xxlarge", "id" => "inputDescription"));
        // line 134
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 138
        echo form_label("Ingredients", "ingredients", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 140
        echo form_textarea(array("name" => "ingredients", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "ingredients", array()), "class" => "form-control input-xxlarge", "id" => "inputContent", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 145
        echo form_label("Brand", "brands_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 147
        echo form_dropdown("brands_id", (isset($context["brands"]) ? $context["brands"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "brands_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 152
        echo form_label("Barcode", "barcode", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 154
        echo form_input(array("name" => "barcode", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "barcode", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 159
        echo form_label("Price", "price", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 161
        echo form_input(array("name" => "price", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "price", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 166
        echo form_label("Quantity in Case", "quantity_case", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 168
        echo form_input(array("name" => "quantity_case", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "quantity_case", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-type" => "integer"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 173
        echo form_label("Weight per Pc", "weight_pc", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 175
        echo form_input(array("name" => "weight_pc", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "weight_pc", array()), "class" => "form-control", "data-parsley-required" => "true"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 180
        echo form_label("Segment", "segments_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 182
        echo form_dropdown("glob_pro_hierarchy[]", (isset($context["segments"]) ? $context["segments"] : null), $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "glob_pro_hierarchy_array", array()), 4, array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 187
        echo form_label("Shelf Life", "shelf_life", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 189
        echo form_input(array("name" => "shelf_life", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "shelf_life", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 193
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 4))) {
            // line 194
            echo "                <div class=\"form-group\">
                    ";
            // line 195
            echo form_label("Pallet Configuration", "pallet_configuration", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 197
            echo form_input(array("name" => "pallet_configuration", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "pallet_configuration", array()), "class" => "form-control"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 201
        echo "
            <div class=\"form-group\">
                ";
        // line 203
        echo form_label("Is Top Ten", "is_top_ten", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    <div class=\"checkbox\">
                        <label class=\"\">";
        // line 206
        echo form_checkbox("is_top_ten", 1, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "is_top_ten", array()));
        echo "</label>
                    </div>
                </div>
            </div>

            ";
        // line 211
        if ((twig_length_filter($this->env, (isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null)) > 1)) {
            // line 212
            echo "                <div class=\"form-group\">
                    ";
            // line 213
            echo form_label("Cuisine Channels", "cuisine_channels_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 215
            echo form_dropdown("cuisine_channels_id", (isset($context["cuisine_channels"]) ? $context["cuisine_channels"] : null), $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "cuisine_channels_id", array()), "class=\"form-control\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 219
        echo "
            <div class=\"form-group\">
                ";
        // line 221
        echo form_label("Nutrition Name", "nutrition_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 223
        echo form_textarea(array("name" => "nutrition_name", "value" => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "nutrition_name", array()), "class" => "form-control input-xxlarge", "id" => "inputNutrition"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 227
        echo form_label("Type", "type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 229
        echo form_dropdown("type", (isset($context["type"]) ? $context["type"] : null), (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "type", array())) : (1)), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 233
        echo form_label("OTM", "otm", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 235
        echo form_dropdown("otm", (isset($context["otm"]) ? $context["otm"] : null), (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "otm", array())) : ("D")), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 239
        echo form_label("App Type", "app_type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 241
        echo form_dropdown("app_type", (isset($context["app_type"]) ? $context["app_type"] : null), (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array())) ? ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "app_type", array())) : (0)), "class=\"form-control\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 245
        echo form_label("", "listed", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 247
        echo form_radio("listed", 0, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "listed", array()) == 0));
        echo " Delisted</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 248
        echo form_radio("listed", 1, ($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "listed", array()) == 1));
        echo " Listed</label>&nbsp;
                </div>
            </div>

            ";
        // line 252
        $this->loadTemplate("ami/components/uploader_image.html.twig", "ami/promotions/edit.html.twig", 252)->display(array_merge($context, array("entry_type" => "promotions")));
        // line 253
        echo "
            ";
        // line 254
        $this->loadTemplate("ami/components/uploader_video.html.twig", "ami/promotions/edit.html.twig", 254)->display(array_merge($context, array("entry_type" => "promotions")));
        // line 255
        echo "
            ";
        // line 256
        $this->loadTemplate("ami/components/uploader_brochure.html.twig", "ami/promotions/edit.html.twig", 256)->display(array_merge($context, array("entry_type" => "promotions")));
        // line 257
        echo "
            ";
        // line 258
        $this->loadTemplate("ami/components/uploader_spec.html.twig", "ami/promotions/edit.html.twig", 258)->display(array_merge($context, array("entry_type" => "promotions")));
        // line 259
        echo "
            ";
        // line 260
        $this->loadTemplate("ami/components/uploader_cert.html.twig", "ami/promotions/edit.html.twig", 260)->display(array_merge($context, array("entry_type" => "promotions")));
        // line 261
        echo "
            ";
        // line 262
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>
    </div>

    ";
        // line 266
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/promotions/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  485 => 266,  478 => 262,  475 => 261,  473 => 260,  470 => 259,  468 => 258,  465 => 257,  463 => 256,  460 => 255,  458 => 254,  455 => 253,  453 => 252,  446 => 248,  442 => 247,  437 => 245,  430 => 241,  425 => 239,  418 => 235,  413 => 233,  406 => 229,  401 => 227,  394 => 223,  389 => 221,  385 => 219,  378 => 215,  373 => 213,  370 => 212,  368 => 211,  360 => 206,  354 => 203,  350 => 201,  343 => 197,  338 => 195,  335 => 194,  333 => 193,  326 => 189,  321 => 187,  313 => 182,  308 => 180,  300 => 175,  295 => 173,  287 => 168,  282 => 166,  274 => 161,  269 => 159,  261 => 154,  256 => 152,  248 => 147,  243 => 145,  235 => 140,  230 => 138,  224 => 134,  222 => 132,  217 => 130,  209 => 125,  204 => 123,  197 => 119,  192 => 117,  185 => 113,  180 => 111,  173 => 107,  168 => 105,  157 => 96,  155 => 95,  144 => 87,  127 => 72,  124 => 71,  106 => 56,  99 => 52,  92 => 48,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
