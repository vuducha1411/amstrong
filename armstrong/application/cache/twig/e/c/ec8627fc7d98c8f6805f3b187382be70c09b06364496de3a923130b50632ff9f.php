<?php

/* ami/distributors/transfer.html.twig */
class __TwigTemplate_ec8627fc7d98c8f6805f3b187382be70c09b06364496de3a923130b50632ff9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/distributors/transfer.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(document).ready(function() {
            \$('.dataTables').dataTable({
                /*\"processing\": true,
                 \"serverSide\": true,
                 \"ajax\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url("ami/distributors/ajaxData"), "html", null, true);
        echo "\",*/
                'order': [[1, 'asc']],
                'aoColumnDefs': [{
                    'aTargets': ['nosort']
                }],
                \"iDisplayLength\": 50
            });
        });
    </script>
    <script>
        function getSalesperson(){
//            var param = document.getElementsByName(\"armstrong_2_salespersons_id\").value;
            var param = \$('select[name=\"armstrong_2_salespersons_id\"]').val();
            if(param == null || param == ''){
                window.location.href = \"";
        // line 30
        echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/ami/distributors/show_distributors")), "html", null, true);
        echo "/\"+param;
                alert('Must be select current Salesperson to show !');
            }else{
                window.location.href = \"";
        // line 33
        echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/ami/distributors/show_distributors")), "html", null, true);
        echo "/\"+param;
            }
        }
    </script>
    <script>
        var Example = (function() {
            \"use strict\";
            var elem,
                    hideHandler,
                    that = {};

            that.init = function(options) {
                elem = \$(options.selector);
            };

            that.show = function(text) {
                clearTimeout(hideHandler);

                elem.find(\"span\").html(text);
                elem.delay(200).fadeIn().delay(4000).fadeOut();
            };

            return that;
        }());
    </script>
    <script>
        function myConfirm() {
            var param = \$('select[name=\"armstrong_2_salespersons_id\"]').val();
            var param_new = \$('select[name=\"armstrong_2_salespersons_id_new\"]').val();

            if(param !='' && param_new !='' && param!==param_new) {
                bootbox.dialog({
                    message: \"All current Customers of [Current Salesperson] will be shifted to [Target Salesperson] . <br />Are you sure?\",
                    title: \"Transfer! Please Confirm.\",
                    buttons: {
                        success: {
                            label: \"Yes\",
                            className: \"btn-success\",
                            callback: function() {
                                window.location.href = \"";
        // line 72
        echo twig_escape_filter($this->env, site_url(((isset($context["url"]) ? $context["url"] : null) . "/ami/distributors/transfer_do")), "html", null, true);
        echo "/\"+param+\"/\"+param_new;
                                Example.show('great success');
                            }
                        },
                        main: {
                            label: \"No\",
                            className: \"btn-primary\"
                        }
                    }
                });
            }
            if(param===param_new && param !=''){
                bootbox.dialog({
                    message: \"[Current Salesperson] is same [Target Salesperson] <br />Can't transfer\",
                    title: \"Transfer! Please Confirm\",
                    buttons: {
                        main: {
                            label: \"Yes\",
                            className: \"btn-success\"
                        }
                    }
                });
            }
            if( (param =='' || param_new=='')){
                \$('#myform').submit();
            }
        }
    </script>
";
    }

    // line 101
    public function block_css($context, array $blocks = array())
    {
        // line 102
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 103
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 104
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
";
    }

    // line 106
    public function block_content($context, array $blocks = array())
    {
        // line 107
        echo "
    ";
        // line 108
        echo form_open(site_url("ami/distributors/transfer_do"), array("class" => "form-horizontal", "id" => "myform", "data-parsley-validate" => "true"), array("armstrong_2_distributors_id" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Transfer of Distributors</h1>
                <div class=\"text-right\">
                    ";
        // line 115
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/distributors/transfer.html.twig", 115)->display(array_merge($context, array("url" => "ami/distributors", "id" => $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_distributors_id", array()), "permission" => "distributor")));
        // line 116
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

                <div class=\"form-group\">
                   ";
        // line 126
        echo form_label("Current Salesperson", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                     <div class=\"col-sm-6\">
                      ";
        // line 128
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                     </div>
                </div>

            ";
        // line 132
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 133
            echo "                <div class=\"form-group\">
                    ";
            // line 134
            echo form_label("Target Salesperson", "armstrong_2_salespersons_id_new", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 136
            echo form_dropdown("armstrong_2_salespersons_id_new", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["distributor"]) ? $context["distributor"] : null), "armstrong_2_salespersons_id_new", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 140
        echo "            <hr>
            ";
        // line 141
        if (((isset($context["show_table"]) ? $context["show_table"] : null) == 1)) {
            // line 142
            echo "            <div class=\"panel panel-default m-t\">
                 <div class=\"panel-body tab-content\">

                <table class=\"table table-striped table-bordered table-hover dataTables\">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
            // line 155
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["distributors"]) ? $context["distributors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["dis"]) {
                // line 156
                echo "                        <tr>
                            <td>";
                // line 157
                echo twig_escape_filter($this->env, $this->getAttribute($context["dis"], "armstrong_2_distributors_id", array()), "html", null, true);
                echo "</td>
                            <td>";
                // line 158
                echo twig_escape_filter($this->env, $this->getAttribute($context["dis"], "name", array()), "html", null, true);
                echo "</td>
                            <td class=\"center\">";
                // line 159
                echo twig_escape_filter($this->env, $this->getAttribute($context["dis"], "date_created", array()), "html", null, true);
                echo "</td>
                            <td class=\"center\">";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute($context["dis"], "last_updated", array()), "html", null, true);
                echo "</td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dis'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 163
            echo "                    </tbody>
                </table>
            </div>
        </div>
            ";
        }
        // line 168
        echo "
    </div>
        </div>

    ";
        // line 172
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/distributors/transfer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 172,  291 => 168,  284 => 163,  275 => 160,  271 => 159,  267 => 158,  263 => 157,  260 => 156,  256 => 155,  241 => 142,  239 => 141,  236 => 140,  229 => 136,  224 => 134,  221 => 133,  219 => 132,  212 => 128,  207 => 126,  195 => 116,  193 => 115,  183 => 108,  180 => 107,  177 => 106,  171 => 104,  167 => 103,  162 => 102,  159 => 101,  126 => 72,  84 => 33,  78 => 30,  61 => 16,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
