<?php

/* ami/distributors/index.html.twig */
class __TwigTemplate_ebca7c47241d2a3ca2b3f722ff6397c4f54cf40f1e542e90bde8227387ab4c97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/distributors/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 16
        echo twig_escape_filter($this->env, site_url(("ami/distributors/ajaxData?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"c.armstrong_2_distributors_id\"},
                    {mData: \"c.name\"},
                    {mData: \"c.armstrong_2_salespersons_id\"},
                    {mData: \"\$.salesperson_name\"},
                    {mData: \"c.approved\"},
//                    {mData: \"c.is_perfect_store\"},
                    {mData: \"c.date_created\", \"bSeardchable\": false},
                    {mData: \"c.last_updated\", \"bSearchable\": false},
                    {mData: \"c.updated_name\"},
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    ";
        // line 31
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "distributor"))) {
            // line 32
            echo "                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    ";
        }
        // line 34
        echo "                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                // 'bPaginate': false,
                'aoColumnDefs': [
                    {
                        'bSortable': true,
                        'aTargets': ['nosort']
                    }
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 59
    public function block_css($context, array $blocks = array())
    {
        // line 60
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 64
    public function block_content($context, array $blocks = array())
    {
        // line 65
        echo "
    ";
        // line 66
        echo form_open(site_url("ami/distributors/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 71
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Distributors Draft") : ("Distributors"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 74
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 76
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/distributors/index.html.twig", 76)->display(array_merge($context, array("url" => "ami/distributors", "icon" => "fa-truck", "title" => "Distributors", "permission" => "distributor", "showImport" => 1)));
        // line 77
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 87
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "active")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                            href=\"";
        // line 88
        echo twig_escape_filter($this->env, site_url("ami/distributors?filter=active"), "html", null, true);
        echo "\">Active</a>
                </li>
                <li role=\"presentation\" ";
        // line 90
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                              href=\"";
        // line 91
        echo twig_escape_filter($this->env, site_url("ami/distributors?filter=inactive"), "html", null, true);
        echo "\">Inactive</a>
                </li>
                ";
        // line 93
        if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_all", "salespersons")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("manage_staff", "salespersons")))) {
            // line 94
            echo "                    <li role=\"presentation\" ";
            echo ((((isset($context["filter"]) ? $context["filter"] : null) == "pending")) ? ("class=\"active\"") : (""));
            echo "><a role=\"menuitem\"
                                                                                                 href=\"";
            // line 95
            echo twig_escape_filter($this->env, site_url("ami/distributors?filter=pending"), "html", null, true);
            echo "\">Pending</a>
                    </li>
                ";
        }
        // line 98
        echo "            </ul>

            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 107
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "distributor"))) {
            // line 108
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                    ";
            // line 112
            echo "                                        ";
            // line 113
            echo "                                ";
        }
        // line 114
        echo "                                <th>ID</th>
                                <th>Name</th>
                                <th>Salesperson Id</th>
                                <th>Salesperson Name</th>
                                <th>Status</th>
                                ";
        // line 120
        echo "                                <th>Created</th>
                                <th>Updated</th>
                                <th>Updated By</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 128
        echo "                                ";
        // line 129
        echo "                                    ";
        // line 130
        echo "                                        ";
        // line 131
        echo "                                    ";
        // line 132
        echo "                                    ";
        // line 133
        echo "                                    ";
        // line 134
        echo "                                    ";
        // line 135
        echo "                                        ";
        // line 136
        echo "                                    ";
        // line 137
        echo "                                    ";
        // line 138
        echo "                                    ";
        // line 139
        echo "                                    ";
        // line 140
        echo "                                    ";
        // line 141
        echo "                                        ";
        // line 142
        echo "                                    ";
        // line 143
        echo "                                ";
        // line 144
        echo "                            ";
        // line 145
        echo "                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 157
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/distributors/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 157,  269 => 145,  267 => 144,  265 => 143,  263 => 142,  261 => 141,  259 => 140,  257 => 139,  255 => 138,  253 => 137,  251 => 136,  249 => 135,  247 => 134,  245 => 133,  243 => 132,  241 => 131,  239 => 130,  237 => 129,  235 => 128,  226 => 120,  219 => 114,  216 => 113,  214 => 112,  209 => 108,  207 => 107,  196 => 98,  190 => 95,  185 => 94,  183 => 93,  178 => 91,  174 => 90,  169 => 88,  165 => 87,  153 => 77,  151 => 76,  146 => 74,  140 => 71,  132 => 66,  129 => 65,  126 => 64,  120 => 61,  115 => 60,  112 => 59,  85 => 34,  81 => 32,  79 => 31,  61 => 16,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
