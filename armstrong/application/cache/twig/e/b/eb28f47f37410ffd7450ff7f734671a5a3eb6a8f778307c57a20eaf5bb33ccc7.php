<?php

/* ami/customers/transfer_customers.html.twig */
class __TwigTemplate_eb28f47f37410ffd7450ff7f734671a5a3eb6a8f778307c57a20eaf5bb33ccc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/customers/transfer_customers.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script>
        function checkTargetSalesperson() {
            var currentVal = \$('select[name=\"armstrong_2_salespersons_id\"]').val();
            var targetVal = \$('select[name=\"armstrong_2_salespersons_id_new\"]').val();
            if (!targetVal && currentVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Target Salesperson]'
                });
                return false;
            } else if (!currentVal && targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Current Salesperson]'
                });
                return false;
            } else if (!currentVal && !targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: 'Please chose [Target Salesperson] and [Current Salesperson]'
                });
                return false;
            } else if (currentVal == targetVal) {
                \$.alert({
                    title: 'Alert!',
                    content: \"[Current Salesperson] is same [Target Salesperson] <br />Can't transfer\"
                });
                return false;
            }
            return true;
        }

        function getCustomers() {
            var targetElement = \$('.customers_assigned'),
                    inputCustomers = \$('input[name=\"customers\"]'),
                    items = targetElement.find('li div'),
                    array_val = [];
            \$.each(items, function () {
                var \$val = \$(this).data('customer');
                array_val.push(\$val);
            });
            inputCustomers.val(array_val);
        }

        function checkSelectAllCheckBox(selector) {
            var itemVisible = selector.find('ul li.fillable:visible'),
                    itemSelected = selector.find('ul .ui-selected:visible'),
                    selectAllItem = selector.find('ul li.selectall');

            if (itemVisible.length > 0) {
                selectAllItem.removeClass('hidden');
            } else {
                selectAllItem.addClass('hidden');
            }
            if (itemSelected.length == itemVisible.length) {
                selectAllItem.find('.checkAll').prop('checked', true);
            } else {
                selectAllItem.find('.checkAll').prop('checked', false);
            }
        }

        \$(document).ready(function () {
            \$('.customers_block').perfectScrollbar();
//            \$('#armstrong_2_salespersons_id').on('change', function () {
//                var \$val = \$(this).val(),
//                        customers_block_wrapper = \$('.customers_unassigned');
//                \$.each(customers_block_wrapper.find('li:not(.selectall) div'), function () {
//                    var armstrong_2_salesperson = \$(this).data('salesperson');
//                    if (armstrong_2_salesperson !== \$val) {
//                        \$(this).closest('li').hide();
//                    } else {
//                        \$(this).closest('li').removeClass('hidden').show();
//                    }
//                });
//                checkSelectAllCheckBox(customers_block_wrapper);
//            });
            \$('.transfer-right').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = \$('.customers_unassigned ul li.ui-selected'),
                            selectedElementClone = selectedElement.clone(),
                            targetBlock = \$('.customers_assigned ul');
                    selectedElement.remove();
                    selectedElementClone.appendTo(targetBlock);
                    checkSelectAllCheckBox(\$('.customers_unassigned'));
                    checkSelectAllCheckBox(\$('.customers_assigned'));
                    getCustomers();
                }
            });
            \$('.transfer-left').on('click', function () {
                if (checkTargetSalesperson()) {
                    var selectedElement = \$('.customers_assigned ul li.ui-selected'),
                            selectedElementClone = selectedElement.clone(),
                            targetBlock = \$('.customers_unassigned ul');
                    selectedElement.remove();
                    selectedElementClone.appendTo(targetBlock);
                    checkSelectAllCheckBox(\$('.customers_unassigned'));
                    checkSelectAllCheckBox(\$('.customers_assigned'));
                    getCustomers();
                }
            });

            \$(document).on('click', 'input.checkAll', function () {
                var checkbox = \$(this).closest('.customers_block').find('input.check-box:visible');
                if (\$(this).is(':checked')) {
                    checkbox.prop('checked', true);
                    checkbox.closest('li').addClass('ui-selected');
                } else {
                    checkbox.prop('checked', false);
                    checkbox.closest('li').removeClass('ui-selected');
                }
            });
            \$(document).on('click', 'input.check-box', function () {
                var currentBlock = \$(this).closest('.customers_block'),
                        checkAll = currentBlock.find('input.checkAll'),
                        flagCheck = true;
                if (\$(this).is(':checked')) {
                    \$(this).closest('li').addClass('ui-selected');
                    currentBlock.find('input.check-box:visible').each(function (k, e) {
                        var checked = \$(this).prop('checked');
                        if (checked == false) {
                            flagCheck = false;
                            return false;
                        }
                    });
                    if (flagCheck) {
                        checkAll.prop('checked', true);
                    } else {
                        checkAll.prop('checked', false);
                    }
                } else {
                    \$(this).closest('li').removeClass('ui-selected');
                    checkAll.prop('checked', false);
                }
            });

            // Filter
            \$('.filter').on('input', function () {
                var \$val = \$(this).val(),
                        idAttr = \$(this).attr('id'),
                        wrapClass = (idAttr == 'unAssignedFilter') ? '.customers_unassigned' : '.customers_assigned',
                        options = \$(wrapClass + ' ul li.fillable');
                if (\$val == '') {
                    \$(wrapClass + ' ul li.fillable').removeClass('hidden');
                } else {
                    options.each(function () {
                        var str = \$(this).find('div').text(),
                                regex = new RegExp(\$val, 'gi'),
                                res = str.match(regex);
                        if (res) {
                            \$(this).removeClass('hidden');
                        } else {
                            \$(this).addClass('hidden');
                        }
                    });
                }
            });
            \$('button[type=\"submit\"]').on('click', function (e) {
                e.preventDefault();
                var input_customers = \$('input[name=\"customers\"]').val();
                if (!input_customers) {
                    \$.alert({
                        title: 'Alert!',
                        content: \"Nothing to transfer\"
                    });
                    return false;
                }
                \$.confirm({
                    title: 'Confirm!',
                    content: 'All current Customers of [Current Salesperson] will be shifted to [Target Salesperson]. <br />Are you sure?',
                    confirm: function () {
                        \$('button[type=\"submit\"]').attr('disabled', 'disabled');
                        \$('#myform').submit();
                    },
                    cancel: function () {

                    }
                });
            });

            //nam add
            \$('#armstrong_2_salespersons_id').change(function(){
                var sale_id = \$(this).val();
                var list_cus = '';
                var selected = \$('.customers_assigned li');
                \$.each(selected,function(k,v){
                    var cus = \$(v).children().data('customer');
                    if(cus != undefined){
                        list_cus = list_cus == '' ? list_cus + cus : list_cus + ',' + cus;
                    }
                });
                console.log(list_cus)
                \$.ajax({
                    url: \"";
        // line 197
        echo twig_escape_filter($this->env, site_url("ami/customers/getCustomerOfSale"), "html", null, true);
        echo "\"+ '/' + sale_id +'?list_cus=' + list_cus ,
                    type: 'GET',
                    dataType: 'html',
                    beforeSend: function() {
                        \$('.customer_append').text('Loading...');
                    },
                    success: function(res) {
                       \$('.customer_append').html(res);
                    }
                });
            })
        });
    </script>
";
    }

    // line 212
    public function block_css($context, array $blocks = array())
    {
        // line 213
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .customers_block {
            height: 300px !important;
            overflow: hidden;
            position: relative;
            border: 1px solid #ddd;
            border-radius: 10px;
            padding: 10px;
        }

        .customers_block ul {
            list-style: none;
            padding: 0;
        }

        .customers_block ul li {
            cursor: pointer;
        }

        .customers_block ul li.selected {
            background-color: #337ab7;
            color: #fff;
        }

        .btn_transfer {
            margin-top: 100px;
        }
    </style>
";
    }

    // line 244
    public function block_content($context, array $blocks = array())
    {
        // line 245
        echo "    ";
        echo form_open(site_url("ami/customers/transfer_customers_do"), array("class" => "", "id" => "myform", "data-parsley-validate" => "false"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">

            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Transfer of Customers</h1>

                <div class=\"text-right\">
                    ";
        // line 254
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/customers/transfer_customers.html.twig", 254)->display(array_merge($context, array("url" => "ami/customers", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "permission" => "customer")));
        // line 255
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"left-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 265
        echo form_label("Current Salesperson", "armstrong_2_salespersons_id", array("class" => ""));
        echo "
                    ";
        // line 266
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "id=\"armstrong_2_salespersons_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    <input type=\"text\" id=\"unAssignedFilter\" class=\"form-control filter\" placeholder=\"Input text to search\">
                </div>
                <div class=\"form-group\">
                    ";
        // line 272
        echo form_label("Unassigned", "customers_unassigned", array("class" => ""));
        echo "
                    <div class=\"customers_unassigned customers_block\">
                        <div class=\"customer_append\"></div>
                    </div>
                </div>
            </div>
            <div class=\"btn_transfer col-sm-2\">
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-right\">
                    <i class=\"fa fa-long-arrow-right\"></i>
                    Left to Right
                </a>
                <a href=\"javascript:;\" class=\"btn btn-default m-b-md transfer-left\">
                    <i class=\"fa fa-long-arrow-left\"></i>
                    Right to Left
                </a>
            </div>
            <div class=\"right-block col-sm-5\">
                <div class=\"form-group\">
                    ";
        // line 290
        echo form_label("Target Salesperson", "armstrong_2_salespersons_id_new", array("class" => ""));
        echo "
                    ";
        // line 291
        echo form_dropdown("armstrong_2_salespersons_id_new", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id_new", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
                <div class=\"form-group\">
                    <input type=\"text\" name=\"\" id=\"assignedFilter\" class=\"form-control filter\" placeholder=\"Input text to search\">
                </div>
                <div class=\"form-group\">
                    ";
        // line 297
        echo form_label("Assigned", "customers_assigned", array("class" => ""));
        echo "
                    <div class=\"customers_assigned customers_block\">
                        <ul>
                            <li class=\"selectall hidden\">
                                <div>
                                    <input class=\"checkAll\" type=\"checkbox\"> Select All
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type=\"hidden\" name=\"customers\">
    ";
        // line 312
        echo form_close();
        echo "
    <link href=\"";
        // line 313
        echo twig_escape_filter($this->env, site_url("res/css/plugins/perfect-scrollbar/perfect-scrollbar.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <link href=\"";
        // line 315
        echo twig_escape_filter($this->env, site_url("res/css/jquery-confirm.min.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <link href=\"";
        // line 317
        echo twig_escape_filter($this->env, site_url("res/css/multiple-select.css"), "html", null, true);
        echo "\" rel='stylesheet'
          type='text/css'>
    <script src=\"";
        // line 319
        echo twig_escape_filter($this->env, site_url("res/js/jquery-confirm.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 320
        echo twig_escape_filter($this->env, site_url("res/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 322
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, site_url("res/js/jquery.multiple.select.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "ami/customers/transfer_customers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  408 => 322,  404 => 320,  400 => 319,  395 => 317,  390 => 315,  385 => 313,  381 => 312,  363 => 297,  354 => 291,  350 => 290,  329 => 272,  320 => 266,  316 => 265,  304 => 255,  302 => 254,  289 => 245,  286 => 244,  251 => 213,  248 => 212,  230 => 197,  33 => 4,  30 => 3,  11 => 1,);
    }
}
