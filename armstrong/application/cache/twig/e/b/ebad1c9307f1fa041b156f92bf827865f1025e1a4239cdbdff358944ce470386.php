<?php

/* ami/pantry_check/edit.html.twig */
class __TwigTemplate_ebad1c9307f1fa041b156f92bf827865f1025e1a4239cdbdff358944ce470386 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/pantry_check/edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        echo form_open(site_url("ami/pantry_check/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("armstrong_2_pantry_check_id" => $this->getAttribute((isset($context["pantry_check"]) ? $context["pantry_check"] : null), "armstrong_2_pantry_check_id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Pantry Check</h1>
                <div class=\"text-right\">
                    ";
        // line 12
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/pantry_check/edit.html.twig", 12)->display(array_merge($context, array("url" => "ami/pantry_check", "id" => $this->getAttribute((isset($context["pantry_check"]) ? $context["pantry_check"] : null), "armstrong_2_pantry_check_id", array()), "permission" => "pantry_check")));
        // line 13
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>              
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            

                ";
        // line 22
        if ((isset($context["call_records"]) ? $context["call_records"] : null)) {
            // line 23
            echo "                    <div class=\"form-group\">
                        ";
            // line 24
            echo form_label("Call Records", "armstrong_2_call_records_id", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 26
            echo form_dropdown("armstrong_2_call_records_id", (isset($context["call_records"]) ? $context["call_records"] : null), $this->getAttribute((isset($context["pantry_check"]) ? $context["pantry_check"] : null), "armstrong_2_call_records_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                        </div>
                    </div>
                ";
        }
        // line 30
        echo "
                <div class=\"form-group\">
                    ";
        // line 32
        echo form_label("Notes", "notes", array("class" => "control-label col-sm-3"));
        echo "
                    <div class=\"col-sm-6\">
                        ";
        // line 34
        echo form_textarea(array("name" => "notes", "value" => $this->getAttribute((isset($context["pantry_check"]) ? $context["pantry_check"] : null), "notes", array()), "class" => "form-control", "rows" => 3));
        echo "
                    </div>
                </div>
        </div>
    </div> 

    ";
        // line 40
        echo form_close();
        echo "   
";
    }

    public function getTemplateName()
    {
        return "ami/pantry_check/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 40,  83 => 34,  78 => 32,  74 => 30,  67 => 26,  62 => 24,  59 => 23,  57 => 22,  46 => 13,  44 => 12,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
