<?php

/* ami/components/uploader_video.html.twig */
class __TwigTemplate_e66f6b52cef99000fc8598320e82239d9b6a399b7ed2a1219bb67f0b9e6d2715 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\" id=\"videoUpload\" data-button=\"uploader_video_pickfiles\" data-container=\"uploader_video\"
    data-url=\"";
        // line 2
        echo twig_escape_filter($this->env, site_url("ami/media/upload"), "html", null, true);
        echo "\" 
    data-maxfilesize=\"200mb\"
    data-params=\"";
        // line 4
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array("entry_type" => (isset($context["entry_type"]) ? $context["entry_type"] : null), "class" => "video")), "html", null, true);
        echo "\" 
    data-mimetypes=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array(0 => array("title" => "Video Clips", "extensions" => "mp4,avi"))), "html", null, true);
        echo "\">
    ";
        // line 6
        echo form_label("Video", "inputVideos", array("class" => "control-label col-sm-3"));
        echo "
    <div id=\"uploader_video\" class=\"controls uploader col-sm-6\">                        
        <div class=\"filelist\"></div>
        <div class=\"upload-actions\">
            <a class=\"btn btn-default pick-gallery Tooltip\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url(("ami/media/view/video/" . (isset($context["entry_type"]) ? $context["entry_type"] : null))), "html", null, true);
        echo "\" data-toggle=\"ajaxModal\" title=\"Select from Media\"><i class=\"fa fa-photo\"></i> Media</a>&nbsp;
            <a class=\"btn btn-default btn-upload\" id=\"uploader_video_pickfiles\" href=\"javascript:;\"><i class=\"fa fa-upload\"></i> Upload file</a>
            <p style=\"margin-top: 10px\"><mark>Videos must be under 200 megabytes and in .mp4, .avi format</mark></p>
        </div>                        
        <div class=\"uploaded row\" style=\"margin-top: 10px\">
            ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["videos"]) ? $context["videos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 16
            echo "                <div class=\"col-md-4\">
                    <div class=\"thumbnail\">
                        <img src=\"";
            // line 18
            echo twig_escape_filter($this->env, site_url("res/img/video.png"), "html", null, true);
            echo "\" width=\"100%\" class=\"img-responsive\">
                        <div class=\"caption\">
                            <span class=\"label label-primary\" title=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "filename_old", array()), "html", null, true);
            echo "\">";
            echo wordTrim($this->getAttribute($context["video"], "filename_old", array()), 15);
            echo "</span>
                            <ul class=\"list-inline\" style=\"margin: 10px 0 0 0;\">
                                <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, ((((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/video/") . (isset($context["MEDIA_FOLDER"]) ? $context["MEDIA_FOLDER"] : null)) . $this->getAttribute($context["video"], "path", array())), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn btn-xs btn-default\"><i class=\"fa fa-eye\"></i> View</a>
                                <a href=\"#\" data-id=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "class", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-default pull-right delete-media\"><i class=\"fa fa-close\"></i> Delete</a>
                            </ul>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/uploader_video.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 29,  74 => 23,  70 => 22,  63 => 20,  58 => 18,  54 => 16,  50 => 15,  42 => 10,  35 => 6,  31 => 5,  27 => 4,  22 => 2,  19 => 1,);
    }
}
