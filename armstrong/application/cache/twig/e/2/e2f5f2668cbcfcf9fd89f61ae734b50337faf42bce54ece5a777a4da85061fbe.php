<?php

/* ami/accessibility/index.html.twig */
class __TwigTemplate_e2f5f2668cbcfcf9fd89f61ae734b50337faf42bce54ece5a777a4da85061fbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/accessibility/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/nestedSortable/jquery.mjs.nestedSortable.js"), "html", null, true);
        echo "\"></script>

<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/accessibility/ajaxData"), "html", null, true);
        echo "\",*/
        \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    reset = \$(\".sortable\").html();

    \$(\".sortable\").nestedSortable({
        forcePlaceholderSize: true,
        handle: \".handle\",
        helper: \"clone\",
        listType: \"ul\",
        items: \"li\",
        maxLevels: 0,
        opacity: .6,
        placeholder: \"placeholder\",
        revert: 250,
        tabSize: 25,
        tolerance: \"pointer\",
        toleranceElement: \"> div\",
        expandOnHover: 700,
        scroll: false,
        startCollapsed: true,
        isTree: true,
        // create: function(e, t) {},
        create: function(e, t) {
            list_original = \$(this).nestedSortable(\"serialize\");            
        },
        stop: function(event, ui) {
            var list = \"\";
            list = \$(this).nestedSortable(\"serialize\");

            if (list_original != list) {

                var \$alert = \$('<div class=\"alert alert-block fade in\" style=\"display: none;\"></div>'),
                    \$alertBlock = \$('#alert-block');

                var interval = setInterval(function() {
                    \$alertBlock.html('');
                    
                    \$alert.removeClass(\"alert-success\").removeClass(\"alert-danger\").addClass(\"alert-info\")
                        .appendTo(\$alertBlock)
                        .html('<button type=\"button\" class=\"close close-sm\" data-dismiss=\"alert\"><i class=\"fa fa-times\"></i></button><strong><i class=\"fa fa-spinner fa-spin\"></i></strong> This action could take a while.')
                        .show();
                }, 2000);
                
                var array_list = \$(this).nestedSortable(\"toArray\");
                // var l = array_list.length;
                // for (var k = 0; k < l; k++) {
                //     if (array_list[k].item_id == \$(ui.item).find(\"div\").attr(\"data-id\")) {
                //         if (array_list[k].parent_id == \"root\") {
                //             \$(ui.item).closest(\".toggle\").show()
                //         }
                //         break
                //     }
                // }
                // if (!\$(ui.item).parent().hasClass(\"sortable\")) {
                //     \$(ui.item).parent().addClass(\"subcategory\")
                // }
            
                // var plist = array_list.reduce(function(e, t, n) {
                //     e[n] = {
                //         c: t.item_id,
                //         p: t.parent_id
                //     };
                //     return e
                // }, {});

                console.log(JSON.stringify(array_list));

                \$.ajax({
                    type: \"POST\",
                    url: \"";
        // line 92
        echo twig_escape_filter($this->env, site_url("ami/accessibility/chart"), "html", null, true);
        echo "\",
                    data: {list: JSON.stringify(array_list)},
                    dataType: 'json',
                    success: function(res) {
                        clearInterval(interval);
                        \$alertBlock.html('');

                        if (res.error) {                            
                            \$alert.removeClass(\"alert-success\").removeClass(\"alert-info\").addClass(\"alert-danger\")
                                .appendTo(\$alertBlock)
                                .html('<button type=\"button\" class=\"close close-sm\" data-dismiss=\"alert\"><i class=\"fa fa-times\"></i></button><strong>Oh snap!</strong> ' + res.error)
                                .show();
                            \$('.sortable').html(reset);
                        } else {                            
                            \$alert.removeClass(\"alert-danger\").removeClass(\"alert-info\").addClass(\"alert-success\")
                                .appendTo(\$alertBlock)
                                .html('<button type=\"button\" class=\"close close-sm\" data-dismiss=\"alert\"><i class=\"fa fa-times\"></i></button><strong>Well done!</strong> ' + res.ok)
                                .show();
                            reset = \$('.sortable').html();
                        }

                        \$alert.fadeTo(2000, 500).slideUp(500);
                    },
                    error: function() {
                        clearInterval(interval);
                        \$alertBlock.html('');
                        \$alert.removeClass(\"alert-success\").removeClass(\"alert-info\").addClass(\"alert-danger\")
                            .appendTo(\$alertBlock)
                            .html('<button type=\"button\" class=\"close close-sm\" data-dismiss=\"alert\"><i class=\"fa fa-times\"></i></button><strong>Oh snap!</strong> Ajax error, try again.')
                            .show();
                        \$('.sortable').html(reset);

                        \$alert.fadeTo(2000, 500).slideUp(500);
                    }
                });
                list_original = list
            }
        }
    });
});
</script>
";
    }

    // line 135
    public function block_css($context, array $blocks = array())
    {
        // line 136
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 137
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 140
    public function block_content($context, array $blocks = array())
    {
        // line 141
        echo "
    ";
        // line 142
        echo form_open(site_url("ami/accessibility/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 147
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Accessibility Draft") : ("Accessibility"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 150
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 152
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/accessibility/index.html.twig", 152)->display(array_merge($context, array("url" => "ami/accessibility", "icon" => "fa-gears", "title" => "Accessibility", "permission" => "accessibility")));
        // line 153
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-8\">            
            <div class=\"panel panel-default\">                
                
                <div class=\"panel-body\">                
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    ";
        // line 169
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "accessibility"))) {
            // line 170
            echo "                                        <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                                    ";
        }
        // line 172
        echo "                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 180
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["p_groups"]) ? $context["p_groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 181
            echo "                                    <tr>
                                        ";
            // line 182
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "accessibility"))) {
                // line 183
                echo "                                            <td class=\"text-center\">";
                echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["group"], "id", array())));
                echo "</td>
                                        ";
            }
            // line 185
            echo "                                        <td><a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/accessibility/edit/" . $this->getAttribute($context["group"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "id", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 186
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "name", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 187
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "date_created", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 188
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "last_updated", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 190
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "accessibility")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "accessibility")))) {
                // line 191
                echo "                                                <div class=\"btn-group\">                                                
                                                    ";
                // line 192
                echo html_btn(site_url(("ami/accessibility/edit/" . $this->getAttribute($context["group"], "id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                echo "
                                                    ";
                // line 193
                $context["btnClass"] = ((twig_in_filter($this->getAttribute($context["group"], "name", array()), array(0 => "Admin", 1 => "Sales Manager", 2 => "Salesperson"))) ? ("disabled") : (""));
                // line 194
                echo "                                                    ";
                if ((isset($context["draft"]) ? $context["draft"] : null)) {
                    // line 195
                    echo "                                                        ";
                    echo html_btn(site_url(("ami/accessibility/restore/" . $this->getAttribute($context["group"], "id", array()))), "<i class=\"fa fa-history\"></i>", array("class" => ("btn-default restore " . (isset($context["btnClass"]) ? $context["btnClass"] : null)), "title" => "Restore", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                } else {
                    // line 197
                    echo "                                                        ";
                    echo html_btn(site_url(("ami/accessibility/delete/" . $this->getAttribute($context["group"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => ("btn-default delete " . (isset($context["btnClass"]) ? $context["btnClass"] : null)), "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                }
                // line 199
                echo "                                                </div>
                                                ";
                // line 201
                echo "                                            ";
            }
            // line 202
            echo "                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 205
        echo "                            </tbody>
                        </table>
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                ";
        // line 209
        $context["paginationUrl"] = (((isset($context["draft"]) ? $context["draft"] : null)) ? ("ami/accessibility/draft") : ("ami/accessibility"));
        // line 210
        echo "                                ";
        echo call_user_func_array($this->env->getFunction('pagination')->getCallable(), array(array("base_url" => site_url((isset($context["paginationUrl"]) ? $context["paginationUrl"] : null)), "per_page" => (isset($context["per_page"]) ? $context["per_page"] : null), "total_rows" => (isset($context["total"]) ? $context["total"] : null))));
        echo "
                            </div>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>

        <div class=\"col-lg-4\">
            <div class=\"panel panel-default\">
                <div class=\"category-container panel-body\">
                    <div id=\"alert-block\"></div>
                    <div class=\"list-categories\">";
        // line 226
        echo (isset($context["rolesTemplate"]) ? $context["rolesTemplate"] : null);
        echo "</div>
                </div>
            </div>
        </div>

    </div>
    
    ";
        // line 233
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/accessibility/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  371 => 233,  361 => 226,  341 => 210,  339 => 209,  333 => 205,  325 => 202,  322 => 201,  319 => 199,  313 => 197,  307 => 195,  304 => 194,  302 => 193,  298 => 192,  295 => 191,  293 => 190,  288 => 188,  284 => 187,  280 => 186,  271 => 185,  265 => 183,  263 => 182,  260 => 181,  256 => 180,  246 => 172,  242 => 170,  240 => 169,  222 => 153,  220 => 152,  215 => 150,  209 => 147,  201 => 142,  198 => 141,  195 => 140,  189 => 137,  185 => 136,  182 => 135,  136 => 92,  56 => 15,  46 => 8,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
