<?php

/* ami/chains/index.html.twig */
class __TwigTemplate_fa6fc5f37884b91c642185b8c2038d535d92d335a34585fe6a3324d5901c2e0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/chains/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <!-- DataTables JavaScript -->
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        \$(function () {
            \$('#dataTables').dataTable({
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\": \"";
        // line 18
        echo twig_escape_filter($this->env, site_url(("ami/chains/ajaxData?filter=" . (isset($context["filter"]) ? $context["filter"] : null))), "html", null, true);
        echo "\",
                    \"type\": \"POST\"
                },
                'aoColumns': [
                    {mData: \"armstrong_2_chains_id\"},
                    {mData: \"armstrong_2_chains_name\"},
                    {mData: \"date_created\", \"bSearchable\": false},
                    {mData: \"last_updated\", \"bSearchable\": false}
                ],
                \"fnRowCallback\": function (nRow, aData, iDisplayIndex) {
                    \$(nRow).prepend('<td class=\\\"text-center\\\"><input type=\\\"checkbox\\\" name=\"ids[]\" value=\"' + aData['DT_RowId'] + '\"></td>');
                    \$(nRow).append('<td class=\\\"text-center\\\" style=\\\"min-width: 130px;\\\">' + aData['buttons'] + '</td>');
                },
                'order': [[1, 'asc']],
                'aoColumnDefs': [
                    {
                        'bSortable': false,
                        'aTargets': ['nosort']
                    },
                    {\"width\": \"100px\", \"targets\": [0,1]},
                    {\"width\": \"250px\", \"targets\": [2]}
                ],
                \"iDisplayLength\": 50
            });

            \$(document).on('click', '.btn-pending-submit', function (e) {
                e.preventDefault();

                var \$this = \$(this),
                        \$form = \$this.closest('form');

                \$form.attr('action', \$this.attr('href')).submit();
            });
        });
    </script>
";
    }

    // line 55
    public function block_css($context, array $blocks = array())
    {
        // line 56
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 60
    public function block_content($context, array $blocks = array())
    {
        // line 61
        echo "
    ";
        // line 62
        echo form_open(site_url("ami/chains/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("") : ("submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 67
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Chains Draft") : ("Chains"));
        echo "</h1>

                <div class=\"text-right\">
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">
                        ";
        // line 71
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>

                    <div class=\"btn-group btn-header-toolbar\">
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 75
        echo twig_escape_filter($this->env, site_url("ami/chains/add"), "html", null, true);
        echo "\"><i
                                    class=\"fa fw fa-plus\"></i> Add</a>
                        ";
        // line 77
        if ((isset($context["draft"]) ? $context["draft"] : null)) {
            // line 78
            echo "                            <a class=\"btn btn-sm btn-default\" href=\"";
            echo twig_escape_filter($this->env, site_url("ami/chains"), "html", null, true);
            echo "\"><i
                                        class=\"fa fw fa-tags\"></i> Chains</a>
                        ";
        } else {
            // line 81
            echo "                            ";
            // line 82
            echo "                        ";
        }
        // line 83
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                <li role=\"presentation\" ";
        // line 94
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "active")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                            href=\"";
        // line 95
        echo twig_escape_filter($this->env, site_url("ami/chains?filter=active"), "html", null, true);
        echo "\">Active</a>
                </li>
                <li role=\"presentation\" ";
        // line 97
        echo ((((isset($context["filter"]) ? $context["filter"] : null) == "inactive")) ? ("class=\"active\"") : (""));
        echo "><a role=\"menuitem\"
                                                                                              href=\"";
        // line 98
        echo twig_escape_filter($this->env, site_url("ami/chains?filter=inactive"), "html", null, true);
        echo "\">Inactive</a>
                </li>
                ";
        // line 101
        echo "                    ";
        // line 102
        echo "                                                                                                 ";
        // line 103
        echo "                    ";
        // line 104
        echo "                ";
        // line 105
        echo "            </ul>
            <div class=\"panel panel-default m-t\">
                <div class=\"panel-body tab-content\">

                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"listed\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                            <tr>
                                ";
        // line 113
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "chains"))) {
            // line 114
            echo "                                    <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\"
                                                                          data-target=\"tbody\"
                                                                          data-description=\"#CheckAllBtn\"></th>
                                ";
        }
        // line 118
        echo "                                <th>Chain ID</th>
                                <th>Chain Name</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class=\"nosort text-center\"></th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 127
        echo "                                ";
        // line 128
        echo "                                    ";
        // line 129
        echo "                                        ";
        // line 130
        echo "                                    ";
        // line 131
        echo "                                    ";
        // line 132
        echo "                                    ";
        // line 133
        echo "                                    ";
        // line 134
        echo "                                    ";
        // line 135
        echo "                                    ";
        // line 136
        echo "                                        ";
        // line 137
        echo "                                            ";
        // line 138
        echo "                                                ";
        // line 139
        echo "                                                ";
        // line 140
        echo "                                            ";
        // line 141
        echo "                                            ";
        // line 142
        echo "                                        ";
        // line 143
        echo "                                    ";
        // line 144
        echo "                                ";
        // line 145
        echo "                            ";
        // line 146
        echo "                            </tbody>
                        </table>
                    </div>

                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    ";
        // line 159
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/chains/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 159,  269 => 146,  267 => 145,  265 => 144,  263 => 143,  261 => 142,  259 => 141,  257 => 140,  255 => 139,  253 => 138,  251 => 137,  249 => 136,  247 => 135,  245 => 134,  243 => 133,  241 => 132,  239 => 131,  237 => 130,  235 => 129,  233 => 128,  231 => 127,  221 => 118,  215 => 114,  213 => 113,  203 => 105,  201 => 104,  199 => 103,  197 => 102,  195 => 101,  190 => 98,  186 => 97,  181 => 95,  177 => 94,  164 => 83,  161 => 82,  159 => 81,  152 => 78,  150 => 77,  145 => 75,  138 => 71,  131 => 67,  123 => 62,  120 => 61,  117 => 60,  111 => 57,  106 => 56,  103 => 55,  63 => 18,  51 => 9,  47 => 8,  43 => 7,  39 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
