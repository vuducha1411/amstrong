<?php

/* ami/customers/contacts.html.twig */
class __TwigTemplate_f9765ddf45cebec60c49ebe433d97f6e23613a75017d4567cea5f4cf3752b3ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/customers/contacts.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<!-- DataTables JavaScript -->
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 15
        echo twig_escape_filter($this->env, site_url("ami/contacts/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
});
</script>
";
    }

    // line 27
    public function block_css($context, array $blocks = array())
    {
        // line 28
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
";
    }

    // line 32
    public function block_content($context, array $blocks = array())
    {
        // line 33
        echo "
    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Contacts - ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), "armstrong_2_customers_name", array()), "html", null, true);
        echo "</h1>
                <div class=\"text-right\">                    
                    <div class=\"btn-group btn-header-toolbar\">                        
                        <a class=\"btn btn-sm btn-success\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url(("ami/contacts/add?armstrong_2_customers_id=" . $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), "armstrong_2_customers_id", array()))), "html", null, true);
        echo "\"><i class=\"fa fw fa-plus\"></i> Add</a>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            <div class=\"panel panel-default\">                
                
                <div class=\"panel-body\">                
                    <div class=\"\">
                        <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Position</th>
                                    <th>Phone</th>
                                    <th class=\"nosort text-center\"></th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contacts"]) ? $context["contacts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 67
            echo "                                    <tr class=\"text-center\">
                                        <td><a href=\"";
            // line 68
            echo twig_escape_filter($this->env, site_url(("ami/contacts/edit/" . $this->getAttribute($context["contact"], "id", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["contact"], "name", array()), "html", null, true);
            echo "\" data-toggle=\"ajaxModal\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["contact"], "name", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["contact"], "email", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["contact"], "position", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center\">";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["contact"], "phone", array()), "html", null, true);
            echo "</td>
                                        <td class=\"center text-center\" style=\"min-width: 80px;\">
                                            ";
            // line 73
            if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "contacts")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "contacts")))) {
                // line 74
                echo "                                                <div class=\"btn-group\">    
                                                    ";
                // line 75
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "contacts"))) {
                    echo "    

                                                        ";
                    // line 77
                    $context["class"] = (($this->getAttribute($context["contact"], "primary_contact", array())) ? ("btn-default primary disabled") : ("btn-default primary"));
                    // line 78
                    echo "
                                                        ";
                    // line 79
                    echo html_btn(site_url(((("ami/contacts/primary/" . $this->getAttribute($context["contact"], "id", array())) . "?armstrong_2_customers_id=") . $this->getAttribute((isset($context["customers"]) ? $context["customers"] : null), "armstrong_2_customers_id", array()))), "<i class=\"fa fa-tag\"></i>", array("class" =>                     // line 80
(isset($context["class"]) ? $context["class"] : null), "title" => "Set Primary Contact"));
                    // line 82
                    echo "

                                                        ";
                    // line 84
                    echo html_btn(site_url(("ami/contacts/edit/" . $this->getAttribute($context["contact"], "id", array()))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
                    echo "
                                                    ";
                }
                // line 86
                echo "
                                                    ";
                // line 87
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "contacts"))) {
                    // line 88
                    echo "                                                        ";
                    echo html_btn(site_url(("ami/contacts/delete/" . $this->getAttribute($context["contact"], "id", array()))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                    echo "
                                                    ";
                }
                // line 90
                echo "                                                </div>
                                            ";
            }
            // line 91
            echo "                                        
                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "                            </tbody>
                        </table>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /.panel-body -->
            
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "ami/customers/contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 95,  195 => 91,  191 => 90,  185 => 88,  183 => 87,  180 => 86,  175 => 84,  171 => 82,  169 => 80,  168 => 79,  165 => 78,  163 => 77,  158 => 75,  155 => 74,  153 => 73,  148 => 71,  144 => 70,  140 => 69,  132 => 68,  129 => 67,  125 => 66,  96 => 40,  90 => 37,  84 => 33,  81 => 32,  75 => 29,  71 => 28,  68 => 27,  53 => 15,  42 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
