<?php

/* ami/customers/preview.html.twig */
class __TwigTemplate_f911c841dfbd651efb0e505d8f3cd4d5972e8f137a0d56722468f5829731e548 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">

                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x fa-map-marker pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <div class=\"list-group-item-text\">
                                <h4 class=\"list-group-item-heading text-muted\">Location</h4>
                                <p><span class=\"text-muted\">Street Address:</span> ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "street_address", array()), "html", null, true);
        echo "</p>
                                ";
        // line 17
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "city", array())) {
            // line 18
            echo "                                    <p><span class=\"text-muted\">City:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "city", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 20
        echo "                                ";
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "postal_code", array())) {
            // line 21
            echo "                                    <p><span class=\"text-muted\">Postal Code:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "postal_code", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 23
        echo "                                ";
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "region", array())) {
            // line 24
            echo "                                    <p><span class=\"text-muted\">Region:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "region", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 26
        echo "                                ";
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "state", array())) {
            // line 27
            echo "                                    <p><span class=\"text-muted\">State:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "state", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 29
        echo "                                ";
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "province", array())) {
            // line 30
            echo "                                    <p><span class=\"text-muted\">Province:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "province", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 32
        echo "                                ";
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "district", array())) {
            // line 33
            echo "                                    <p><span class=\"text-muted\">District:</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "district", array()), "html", null, true);
            echo "</p>
                                ";
        }
        // line 35
        echo "                            </div>
                        </div>
                    </div>

                    ";
        // line 39
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array())) {
            // line 40
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-envelope-o pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Email</h4>
                                <p class=\"list-group-item-text\"><a href=\"mailto:";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array()), "html", null, true);
            echo "</a>  ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email_two", array())) ? (((" - <a href=\"mailto:{{ customer.email_two }}\">" . $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email_two", array())) . "</a>")) : ("")), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 48
        echo "
                    ";
        // line 49
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "salesperson_name", array())) {
            // line 50
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-usd pull-left\"></i>
                            <div class=\"m-l-xl\">
                                <h4 class=\"list-group-item-heading text-muted\">Salesperson Name</h4>
                                <p class=\"list-group-item-text\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "salesperson_name", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 58
        echo "
                    ";
        // line 59
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone", array())) {
            // line 60
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-phone pull-left\"></i>
                            <div class=\"m-l-xl\">                          
                                <h4 class=\"list-group-item-heading text-muted\">Phone</h4>
                                <p class=\"list-group-item-text\">";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone_two", array())) ? ((" - " . $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone_two", array()))) : ("")), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 68
        echo "
                    ";
        // line 69
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "fax", array())) {
            // line 70
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-fax pull-left\"></i>
                            <div class=\"m-l-lg\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Fax</h4>
                                <p class=\"list-group-item-text\">";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "fax", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 78
        echo "
                    ";
        // line 79
        if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "contact_details", array())) {
            // line 80
            echo "                        <div class=\"list-group-item\">
                            <i class=\"fa fa-2x fa-info-circle pull-left\"></i>
                            <div class=\"m-l-xl\">                        
                                <h4 class=\"list-group-item-heading text-muted\">Contact Details</h4>
                                <p class=\"list-group-item-text\">";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "contact_details", array()), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                    ";
        }
        // line 88
        echo "                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 93
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "customer"))) {
            // line 94
            echo "                ";
            echo html_btn(site_url(("ami/customers/edit/" . $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array()))), "<i class=\"fa fa-fw fa-edit\"></i> Edit</a>", array("class" => "btn-info"));
            echo "
            ";
        }
        // line 96
        echo "        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/customers/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 96,  206 => 94,  204 => 93,  197 => 88,  190 => 84,  184 => 80,  182 => 79,  179 => 78,  172 => 74,  166 => 70,  164 => 69,  161 => 68,  152 => 64,  146 => 60,  144 => 59,  141 => 58,  134 => 54,  128 => 50,  126 => 49,  123 => 48,  112 => 44,  106 => 40,  104 => 39,  98 => 35,  92 => 33,  89 => 32,  83 => 30,  80 => 29,  74 => 27,  71 => 26,  65 => 24,  62 => 23,  56 => 21,  53 => 20,  47 => 18,  45 => 17,  41 => 16,  25 => 5,  19 => 1,);
    }
}
