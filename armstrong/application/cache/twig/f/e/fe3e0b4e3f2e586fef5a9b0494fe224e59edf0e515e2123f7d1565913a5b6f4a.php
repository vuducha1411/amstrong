<?php

/* ami/components/sidebar.html.twig */
class __TwigTemplate_fe3e0b4e3f2e586fef5a9b0494fe224e59edf0e515e2123f7d1565913a5b6f4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"navbar-default sidebar\" role=\"navigation\">
   <div class=\"sidebar-nav navbar-collapse\" id=\"sidebar\">
        <ul class=\"nav\" id=\"sidebar-search\">
            <li class=\"sidebar-search\">
                <div class=\"input-group custom-search-form\">
                    <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">
                    <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\">
                            <i class=\"fa fa-search\"></i>
                        </button>
                    </span>
                </div>
            </li>
        </ul>
       <ul class=\"nav\" id=\"side-menu\">
            ";
        // line 17
        echo "            ";
        // line 18
        echo "            ";
        // line 19
        echo "                ";
        // line 20
        echo "                ";
        // line 21
        echo "                    ";
        // line 22
        echo "                    ";
        // line 23
        echo "                    ";
        // line 24
        echo "                ";
        // line 25
        echo "            ";
        // line 26
        echo "            ";
        // line 27
        echo "            ";
        // line 28
        echo "            ";
        // line 29
        echo "            ";
        // line 30
        echo "            ";
        // line 31
        echo "            ";
        // line 32
        echo "            ";
        // line 33
        echo "            ";
        // line 34
        echo "            ";
        // line 35
        echo "           ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menuItems"]) ? $context["menuItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menuItem"]) {
            // line 36
            echo "                ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["menuItem"], "child_nodes", array())) > 0)) {
                // line 37
                echo "                    ";
                if ($this->getAttribute($context["menuItem"], "canView", array())) {
                    // line 38
                    echo "                        <li class=\"sidebar-item ";
                    echo ((($this->getAttribute((isset($context["menuHierarchy"]) ? $context["menuHierarchy"] : null), ("ami/" . $this->getAttribute((isset($context["router"]) ? $context["router"] : null), "class", array())), array(), "array") == $this->getAttribute($context["menuItem"], "name", array()))) ? ("active") : (""));
                    echo "\">
                            <a href=\"#\"><i class=\"fa ";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "icon", array()), "html", null, true);
                    echo " fa-fw\"></i> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["menuItem"], "display_name", array()), "html", null, true);
                    echo " <span class=\"fa arrow\"></span></a>
                            <ul class=\"nav nav-second-level\">
                                ";
                    // line 41
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menuItem"], "child_nodes", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["childNode"]) {
                        // line 42
                        echo "                                    ";
                        if ($this->getAttribute($context["childNode"], "canView", array())) {
                            // line 43
                            echo "                                        <li>";
                            echo anchor($this->getAttribute($context["childNode"], "slug", array()), ((("<i class=\"fa " . $this->getAttribute($context["childNode"], "icon", array())) . " fa-fw\"></i> ") . $this->getAttribute($context["childNode"], "display_name", array())), array("class" => (((("ami/" . $this->getAttribute(                            // line 44
(isset($context["router"]) ? $context["router"] : null), "class", array())) == $this->getAttribute($context["childNode"], "slug", array()))) ? ("active_z") : (""))));
                            // line 45
                            echo "</li>
                                    ";
                        }
                        // line 47
                        echo "                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['childNode'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 48
                    echo "                            </ul>
                        </li>
                    ";
                }
                // line 51
                echo "                ";
            } else {
                // line 52
                echo "                    <li>";
                echo anchor($this->getAttribute($context["menuItem"], "slug", array()), ((("<i class=\"fa " . $this->getAttribute($context["menuItem"], "icon", array())) . " fa-fw\"></i> ") . $this->getAttribute($context["menuItem"], "display_name", array())));
                echo "</li>
                ";
            }
            // line 54
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menuItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "
            ";
        // line 57
        echo "            <li class=\"sidebar-item \">
                <a href=\"#\"><i class=\"fa fa-cubes fa-fw\"></i> Global SSD <span class=\"fa arrow\"></span></a>
                <ul class=\"nav nav-second-level\">
                    <li><a href=\"";
        // line 60
        echo twig_escape_filter($this->env, site_url("ami/global_ssd"), "html", null, true);
        echo "\" class=\"\"><i class=\"fa fa-eye fa-fw\"></i> View SSD</a></li>
                    <li><a href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("ami/global_ssd/mappingTable"), "html", null, true);
        echo "\" class=\"\"><i class=\"fa fa-sitemap fa-fw\"></i> Mapping Table</a></li>
                </ul>
            </li>
        </ul>

    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 61,  144 => 60,  139 => 57,  136 => 55,  130 => 54,  124 => 52,  121 => 51,  116 => 48,  110 => 47,  106 => 45,  104 => 44,  102 => 43,  99 => 42,  95 => 41,  88 => 39,  83 => 38,  80 => 37,  77 => 36,  72 => 35,  70 => 34,  68 => 33,  66 => 32,  64 => 31,  62 => 30,  60 => 29,  58 => 28,  56 => 27,  54 => 26,  52 => 25,  50 => 24,  48 => 23,  46 => 22,  44 => 21,  42 => 20,  40 => 19,  38 => 18,  36 => 17,  19 => 1,);
    }
}
