<?php

/* ami/tfo/tfo.html.twig */
class __TwigTemplate_f4d3995ea907a703ca152a078c50b9c17821d521d750f14e0fb1a5d783ad7265 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["tfo"]) ? $context["tfo"] : null)) {
            echo " 

    <div class=\"panel panel-default\">                            
        <div class=\"panel-body\">                
            <div class=\"\">
                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables\">
                    <thead>
                        <tr>
                            ";
            // line 9
            if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "tfo"))) {
                // line 10
                echo "                                <th class=\"nosort text-center\"><input type=\"checkbox\" class=\"CheckAll\" data-target=\"tbody\" data-description=\"#CheckAllBtn\"></th>
                            ";
            }
            // line 12
            echo "                            <th>ID</th>
                            <th>Salesperson Name</th>
                            <th>Operator</th>
                            <th>Call ID</th>
                            <th>Total</th>
                            <th>Method</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th class=\"nosort text-center\"></th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 24
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tfo"]) ? $context["tfo"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["_tfo"]) {
                // line 25
                echo "                            <tr>
                                ";
                // line 26
                if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "tfo"))) {
                    // line 27
                    echo "                                    <td class=\"text-center\">";
                    echo form_checkbox(array("name" => "ids[]", "value" => $this->getAttribute($context["_tfo"], "armstrong_2_tfo_id", array())));
                    echo "</td>
                                ";
                }
                // line 29
                echo "                                <td><a href=\"";
                echo twig_escape_filter($this->env, site_url(("ami/tfo/edit/" . $this->getAttribute($context["_tfo"], "armstrong_2_tfo_id", array()))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_tfo_id", array()), "html", null, true);
                echo "\" data-toggle=\"ajaxModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_tfo_id", array()), "html", null, true);
                echo "</a></td>
                                <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "last_name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_customers_id", array()), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_customers_name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "armstrong_2_call_records_id", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "total", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["methods"]) ? $context["methods"] : null), $this->getAttribute($context["_tfo"], "method", array()), array(), "array"), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "date_created", array()), "html", null, true);
                echo "</td>
                                <td class=\"center\">";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["_tfo"], "last_updated", array()), "html", null, true);
                echo "</td>
                                <td class=\"center text-center\" style=\"min-width: 80px;\">
                                    ";
                // line 38
                if ((call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", "tfo")) || call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", "tfo")))) {
                    // line 39
                    echo "                                        ";
                    $this->loadTemplate("ami/components/table_btn.html.twig", "ami/tfo/tfo.html.twig", 39)->display(array_merge($context, array("url" => "ami/tfo", "id" => $this->getAttribute($context["_tfo"], "armstrong_2_tfo_id", array()), "permission" => "tfo")));
                    // line 40
                    echo "                                    ";
                }
                // line 41
                echo "                                </td>
                            </tr>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_tfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                    </tbody>
                </table>
            </div>
            <!-- /. -->
        </div>
        <!-- /.panel-body -->

    </div>

    <script>
    \$(function() {

        // \$('#dataTables').dataTable.destroy();

        \$('#dataTables').dataTable({
            \"lengthMenu\": [[50, 100, 200, 1000], [50, 100, 200, 1000]],
            'order': [[1, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });        
    });  
    </script>
";
        } else {
            // line 69
            echo "    <div class=\"col-sm-6 col-sm-offset-3\">There are no TFO</div>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/tfo/tfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 69,  146 => 44,  130 => 41,  127 => 40,  124 => 39,  122 => 38,  117 => 36,  113 => 35,  109 => 34,  105 => 33,  101 => 32,  95 => 31,  87 => 30,  78 => 29,  72 => 27,  70 => 26,  67 => 25,  50 => 24,  36 => 12,  32 => 10,  30 => 9,  19 => 1,);
    }
}
