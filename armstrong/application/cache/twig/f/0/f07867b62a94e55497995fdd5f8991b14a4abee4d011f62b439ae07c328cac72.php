<?php

/* ami/demo/view.html.twig */
class __TwigTemplate_f07867b62a94e55497995fdd5f8991b14a4abee4d011f62b439ae07c328cac72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ami/demo/view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Reports";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/jquery.storageapi.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/generateDataReport_demo.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/generateChartReport.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("res/js/generateHtmlReport.js"), "html", null, true);
        echo "\"></script>
    <script>
        var BASE_URL = '";
        // line 14
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "',
                CURRENT_URL = '";
        // line 15
        echo twig_escape_filter($this->env, current_url(), "html", null, true);
        echo "';
        function base_url() {
            return BASE_URL;
        }
        function current_url() {
            return CURRENT_URL;
        }
    </script>
";
    }

    // line 25
    public function block_js($context, array $blocks = array())
    {
        // line 26
        echo "    <script>
        function getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, top_products, isGripCustomer, view_by, range, isMultiCountry, regional, mode, filterColumn, promotionName, viewType, active, globalChannels) {
            \$('#reporting-view').html('');
            \$('#myModal').on('show.bs.modal', function (e) {
                \$('#myModal').show();
                var modalHeight = \$('div.modal-content').outerHeight();
                \$('#myModal').hide();
                var modalTop = \$(window).height() / 2 - modalHeight;
                \$('#myModal .modal-dialog').css('top', modalTop);
            });
            \$('#myModal').modal({backdrop: 'static', keyboard: false});
            urlGetReport += 'json/';
            var filter = \$('#filter_listed').val();
            var sr_manage = '";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["sr_manage"]) ? $context["sr_manage"] : null), "html", null, true);
        echo "';
            switch (mode) {
                case 'get_perfect_store_report':
                    getPerfectStoreReport(urlGetReport, countryId, getall, sp, month, year, app_type, active, isMultiCountry, regional);
                    break;
                case 'get_perfect_call_report_tracking':
                    getPerfectCallReportTracking(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'get_perfect_call_report_tracking_detail':
                    getPerfectCallReportTrackingDetail(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    break;
                case 'penetration_customers_report':
                    penetrationCustomersReport(urlGetReport, month, year, countryId, number_sku, sp, getall, app_type, channel, otm, isMultiCountry, regional);
                    break;
                case 'grip_grab_report':
                    gripGrabReport(urlGetReport, fromdate, todate, countryId, app_type, channel, otm, getall, sp, filterColumn, sr_manage, isMultiCountry, regional);
                    break;
                case 'penetration_data_report':
                    if (view_by == 0 || view_by == 1) {
                        penetrationDataReport(urlGetReport, month, year, countryId, app_type, active, channel, globalChannels, otm, getall, sp, top_products, isGripCustomer, view_by, range, sr_manage, isMultiCountry, regional);
                    } else if (view_by == 3) {
                        penetrationDataChannelSkuReport(urlGetReport, month, year, countryId, app_type, active, channel, globalChannels, otm, top_products, isGripCustomer, view_by, range, sr_manage, isMultiCountry, regional);
                    } else if (view_by == 5) {
                        penetrationDataSrSkuReport(urlGetReport, month, year, countryId, app_type, active, globalChannels, getall, sp, top_products, isGripCustomer, view_by, range, sr_manage, isMultiCountry, regional);
                    } else if (view_by == 6) {
                        penetrationDataSrChannelReport(urlGetReport, month, year, countryId, app_type, active, channel, globalChannels, otm, getall, sp, top_products, isGripCustomer, view_by, range, sr_manage, isMultiCountry, regional);
                    }
                    break;
                case 'rich_media':
                    richMediaReport(urlGetReport, countryId, sr_manage, isMultiCountry, regional);
                    break;
                case 'promotions_activity_report':
                    promotionsActivityReport(urlGetReport, countryId, getall, sp, promotionName, sr_manage, isMultiCountry, regional);
                    break;
                case 'report_last_pantry_check':
                    reportLastPantryCheck(urlGetReport, fromdate, todate, countryId, getall, sp, app_type, viewType, isMultiCountry, regional);
                    break;
                case 'get_perfect_call_report':
                    getPerfectCallReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'get_detail_app_versions_report':
                    getDetailAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'get_app_versions_report':
                    getAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'report_all_in_one':
                    getReportAllInOne(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional);
                    break;
                case 'report_pantry_check':
                    loadReportPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'data_tfo':
                    switch (countryId) {
                        case '3':
                            dataTfoHk(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                            break;
                        case '6':
                            dataTfoTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                            break;
                        default:
                            dataTfoAll(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                            break;
                    }
                    break;
                case 'data_ssd':
                    if (countryId != 6) {
                        dataSsdReprotAll(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    } else {
                        dataSsdReprotTw(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    }
                    // loadDataSsd(urlGetReport, sp, month, year, countryId, getall);
                    break;
                case 'data_call':
                    loadDataCall(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    break;
                case 'coaching_note':
                    coachingNoteDataReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'route_master_temp':
                    routeMasterTempDataReport(urlGetReport, sp, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'customer_contact_data':
                    customerContactReport(urlGetReport, sp, countryId, getall, app_type, active, isMultiCountry, regional);
                    break;
                case 'data_sampling':
                    if (countryId != 6) {
                        dataSamplingAll(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    } else {
                        dataSamplingTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    }
                    // loadDataSampling(urlGetReport, sp, fromdate, todate, countryId,getall,active);
                    break;
                case 'data_pantry_check':
                    loadDataPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    break;
                case 'report_otm':
                    loadReportOtm(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'report_call':
                    loadReportCall(urlGetReport, sp, fromdate, todate, countryId, channel, otm, getall, app_type, isMultiCountry, regional);
                    break;
                case 'report_customers':
                    loadReportCustomer(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'report_summary':
                    allInOneTransactionsReprot(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'report_tfo':
                    tfoReprotSr(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'report_sampling':
                    samplingReport(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'report_approval':
                    approvalReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'top_ten_penetration_sr':
                    topTenPenetrationSrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'report_route_plan':
                    routePlanReprot(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional);
                    break;
                case 'report_grip_and_grab_sr':
                    gripAndGrabReportSR(urlGetReport, sp, dateStartMonth, dateEndMonth, countryId, getall, isMultiCountry, regional);
                    break;
                case 'data_otm':
                    dataOtm(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'new_customer_report':
                    newCustomerReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'top_ten_penetration_calculation_tr':
                    topTenPenetrationCalculationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'new_grip_report':
                    newGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'new_grab_report':
                    newGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'lost_grip_report':
                    lostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'lost_grab_report':
                    lostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'potential_lost_grip_report':
                    potentialLostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'potential_lost_grab_report':
                    potentialLostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'top_ten_penetration_tr':
                    topTenPenetrationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'competitor_reprot':
                    competitorReprot(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
                    break;
                case 'objective_record_data':
                    objectiveRecordData(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'activities_log_data':
                    activitiesLogData(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional);
                    break;
                case 'data_product':
                    productData(urlGetReport, countryId, filter, isMultiCountry, regional);
                    break;
                case 'data_recipes':
                    getRecipesData(urlGetReport, countryId, isMultiCountry, regional);
                    break;
                case 'data_route_plan':
                    dataRoutePlan(urlGetReport, sp, countryId, getall, fromdate, todate, app_type, isMultiCountry, regional);
                    break;
                case 'data_organization':
                    salespersonsData(urlGetReport, sp, countryId, isMultiCountry, regional);
                    break;
                case 'data_region_master':
                    regionReport(urlGetReport, sp, countryId, isMultiCountry, regional);
                    break;
                case 'data_ssd_mt':
                    ssData(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'data_distributors':
                    app_type = 'PUSH';
                    dataDistributors(urlGetReport, sp, countryId, getall, app_type, isMultiCountry, regional);
                    break;
                case 'data_wholesalers':
                    dataWholesalers(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    break;
                case 'data_customers':
                    if (countryId != 6) {
                        dataCustomersReprotAll(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    } else {
                        dataCustomersReprotTw(urlGetReport, sp, countryId, getall, isMultiCountry, regional);
                    }

                    break;
                default:
//                    loadDataOld();
                    break;

            }
        }

        \$(document).ready(function () {
            // Get local storage data for table view
            var tb_ns = \$.initNamespaceStorage('tb_ns');
            var storage = tb_ns.localStorage;
            var urlGetReport = storage.get('urlGetReport'),
                    sp = storage.get('sp'),
                    fromdate = storage.get('fromdate'),
                    todate = storage.get('todate'),
                    countryId = storage.get('countryId'),
                    getall = storage.get('getall'),
                    dateStartMonth = storage.get('dateStartMonth'),
                    dateEndMonth = storage.get('dateEndMonth'),
                    month = storage.get('month'),
                    year = storage.get('year'),
                    number_sku = storage.get('number_sku'),
                    app_type = storage.get('app_type'),
                    active = storage.get('active'),
                    channel = storage.get('channel'),
                    otm = storage.get('otm'),
                    top_products = storage.get('top_products'),
                    isGripCustomer = storage.get('isGripCustomer'),
                    view_by = storage.get('view_by'),
                    range = storage.get('range'),
                    isMultiCountry = storage.get('isMultiCountry'),
                    regional = storage.get('regional'),
                    mode = storage.get('mode'),
                    filterColumn = storage.get('filters'),
                    promotionName = storage.get('promotion_name'),
                    viewType = storage.get('view_type'),
                    salespersonsActive = storage.get('salespersonsActive'),
                    globalChannels = storage.get('globalChannels');

            // Get local storage data for export button
            var export_ns = \$.initNamespaceStorage('export_ns');
            var exportStorage = export_ns.localStorage;
            var exUrlGetReport = exportStorage.get('urlGetReport'),
                    exSalespersons = exportStorage.get('salespersons'),
                    exSalesLeader = exportStorage.get('salesLeader'),
                    exFrom = exportStorage.get('fromDate'),
                    exTo = exportStorage.get('toDate'),
                    exCountryId = exportStorage.get('countryId'),
                    exMonth = exportStorage.get('month'),
                    exYear = exportStorage.get('year'),
                    exAppType = exportStorage.get('app_type'),
                    exChannel = exportStorage.get('channel'),
                    exOtm = exportStorage.get('otm'),
                    exTopProducts = exportStorage.get('topProducts'),
                    exOtherProducts = exportStorage.get('otherProducts'),
                    denominator = exportStorage.get('denominator'),
                    exViewBy = exportStorage.get('view_by'),
                    exRange = exportStorage.get('range'),
                    exMultiRange = exportStorage.get('multiRange'),
                    exMode = exportStorage.get('mode'),
                    exFilterColumn = storage.get('filters'),
                    exPromotionName = storage.get('promotion_name'),
                    exViewType = storage.get('view_type'),
                    exSalespersonsActive = exportStorage.get('salespersonsActive'),
                    exGlobalChannels = exportStorage.get('globalChannels');

            \$('input[name=\"type\"]').val(exMode);
            \$('input[name=\"view_by\"]').val(exViewBy);
            \$('input[name=\"app_type\"]').val(exAppType);
            \$('input[name=\"country_list\"]').val(exCountryId);
            \$('input[name=\"month\"]').val(exMonth);
            \$('input[name=\"year\"]').val(exYear);
            \$('input[name=\"from\"]').val(exFrom);
            \$('input[name=\"to\"]').val(exTo);
            \$('input[name=\"sales_leader[]\"]').val(exSalesLeader);
            \$('input[name=\"armstrong_2_salespersons_id[]\"]').val(exSalespersons);
            \$('input[name=\"top_products[]\"]').val(exTopProducts);
            \$('input[name=\"other_products[]\"]').val(exOtherProducts);
            \$('input[name=\"channel[]\"]').val(exChannel);
            \$('input[name=\"otm[]\"]').val(exOtm);
            \$('input[name=\"rolling\"]').val(exRange);
            \$('input[name=\"multi_rolling[]\"]').val(exMultiRange);
            \$('input[name=\"customers\"]').val(denominator);
            \$('input[name=\"promotion_name\"]').val(exPromotionName);
            \$('input[name=\"view_type\"]').val(exViewType);
            \$('input[name=\"active\"]').val(exSalespersonsActive);
            \$('input[name=\"global_channels\"]').val(exGlobalChannels);

            getdataForTableOnOther(urlGetReport, sp, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, top_products, isGripCustomer, view_by, range, isMultiCountry, regional, mode, filterColumn, promotionName, viewType, salespersonsActive, globalChannels);
        });

        \$(function () {
            \$(document).on('click', '.table-promotion-activity .pdf-generate', function () {
                var \$this = \$(this),
                        activity_id = \$this.closest('td').siblings('.activity-id').data('id');
                \$('input[name=\"promotion_activity_id\"]').val(activity_id);
                \$('form#form_pdf_generate').submit();
            });
        });


    </script>
";
    }

    // line 342
    public function block_css($context, array $blocks = array())
    {
        // line 343
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 344
        echo twig_escape_filter($this->env, site_url("res/css/jqueryui/jquery-ui-1.10.4.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 345
        echo twig_escape_filter($this->env, site_url("res/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 346
        echo twig_escape_filter($this->env, site_url("res/css/ami.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 347
        echo twig_escape_filter($this->env, site_url("res/css/plugins/metisMenu/metisMenu.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 348
        echo twig_escape_filter($this->env, site_url("res/css/plugins/parsley/parsley.css"), "html", null, true);
        echo "\"/>

    <link rel=\"stylesheet\" href=\"";
        // line 350
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 351
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 352
        echo twig_escape_filter($this->env, site_url("res/css/sumoselect.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 353
        echo twig_escape_filter($this->env, site_url("res/css/multiple-select.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 354
        echo twig_escape_filter($this->env, site_url("res/css/demo.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 355
        echo twig_escape_filter($this->env, site_url("res/css/chart.css"), "html", null, true);
        echo "\">

    <style type=\"text/css\">
        .inputWrap {
            margin-bottom: 7px;
        }

        .SumoSelect > .CaptionCont {
            height: 34px;
        }
    </style>
";
    }

    // line 368
    public function block_body($context, array $blocks = array())
    {
        // line 369
        echo "    <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                        data-target=\"#bs-example-navbar-collapse\" aria-expanded=\"false\">
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <h1 id=\"logo_demo\"><a href=\"";
        // line 378
        echo twig_escape_filter($this->env, site_url("ami/demo"), "html", null, true);
        echo "\"><img
                                src=\"";
        // line 379
        echo twig_escape_filter($this->env, site_url("res/img/logo-u-food.jpg"), "html", null, true);
        echo "\" title=\"home\"></a></h1>
            </div>
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse\">
                <ul class=\"nav navbar-nav\">
                    <li><a href=\"";
        // line 383
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Home</a></li>
                    <li><a href=\"";
        // line 384
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Account</a></li>
                    <li><a href=\"";
        // line 385
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Country</a></li>
                    <li><a href=\"";
        // line 386
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">Pending Customers</a></li>
                    <li class=\"active\"><a href=\"";
        // line 387
        echo twig_escape_filter($this->env, site_url("ami/new_reports/penetration_data_report"), "html", null, true);
        echo "\">Reports</a>
                    </li>
                    <li><a href=\"";
        // line 389
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">AMI</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"modal fade\" id=\"myModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-body text-center\">
                    <img style=\"width: 200px\" src=\"";
        // line 399
        echo twig_escape_filter($this->env, site_url("res/img/_AMILOAD.gif"), "html", null, true);
        echo "\"/>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div><!-- /.modal -->

    ";
        // line 407
        echo form_open(site_url("ami/demo/generate"), array("id" => "form_excel_generate", "class" => "form-horizontal"));
        echo "
    <input type=\"hidden\" name=\"type\" value=\"\"/>
    <input type=\"hidden\" name=\"view_by\" value=\"\"/>
    <input type=\"hidden\" name=\"app_type\" value=\"\"/>
    <input type=\"hidden\" name=\"country_list\" value=\"\"/>
    <input type=\"hidden\" name=\"month\" value=\"\"/>
    <input type=\"hidden\" name=\"year\" value=\"\"/>
    <input type=\"hidden\" name=\"from\" value=\"\"/>
    <input type=\"hidden\" name=\"to\" value=\"\"/>
    <input type=\"hidden\" name=\"sales_leader[]\" value=\"\"/>
    <input type=\"hidden\" name=\"armstrong_2_salespersons_id[]\" value=\"\"/>
    <input type=\"hidden\" name=\"top_products[]\" value=\"\"/>
    <input type=\"hidden\" name=\"other_products[]\" value=\"\"/>
    <input type=\"hidden\" name=\"channel[]\" value=\"\"/>
    <input type=\"hidden\" name=\"otm[]\" value=\"\"/>
    <input type=\"hidden\" name=\"rolling\" value=\"\"/>
    <input type=\"hidden\" name=\"multi_rolling[]\" value=\"\"/>
    <input type=\"hidden\" name=\"customers\" value=\"\"/>
    <input type=\"hidden\" name=\"promotion_name\" value=\"\"/>
    <input type=\"hidden\" name=\"active\" value=\"\"/>
    <input type=\"hidden\" name=\"global_channels\" value=\"\"/>
    <div class=\"row\" style=\"padding-left: 50px\">
        <button type=\"submit\"
                class=\"btn btn-primary btn-export-report\" ";
        // line 430
        echo (($this->getAttribute((isset($context["permission"]) ? $context["permission"] : null), "report_export", array())) ? ("") : ("disabled"));
        echo ">
            <i class=\"fa fw fa-download\"></i> Export
        </button>
    </div>
    ";
        // line 434
        echo form_close();
        echo "

    ";
        // line 436
        echo form_open(site_url("ami/demo/generate_pdf"), array("id" => "form_pdf_generate", "class" => "form-horizontal"));
        echo "
    <input type=\"hidden\" name=\"promotion_activity_id\" value=\"\"/>
    ";
        // line 438
        echo form_close();
        echo "
    <div id=\"reporting-view\">
        <!-- reports get generated here -->
        <div class=\"loading\"></div>
        <iframe id=\"iframe_report\"></iframe>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ami/demo/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  589 => 438,  584 => 436,  579 => 434,  572 => 430,  546 => 407,  535 => 399,  522 => 389,  517 => 387,  513 => 386,  509 => 385,  505 => 384,  501 => 383,  494 => 379,  490 => 378,  479 => 369,  476 => 368,  460 => 355,  456 => 354,  452 => 353,  448 => 352,  444 => 351,  440 => 350,  435 => 348,  431 => 347,  427 => 346,  423 => 345,  419 => 344,  414 => 343,  411 => 342,  106 => 39,  91 => 26,  88 => 25,  75 => 15,  71 => 14,  66 => 12,  62 => 11,  58 => 10,  54 => 9,  50 => 8,  46 => 7,  41 => 6,  38 => 5,  32 => 3,  11 => 1,);
    }
}
