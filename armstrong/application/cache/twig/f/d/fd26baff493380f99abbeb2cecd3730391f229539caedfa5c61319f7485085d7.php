<?php

/* ami/components/uploader_brochure.html.twig */
class __TwigTemplate_fd26baff493380f99abbeb2cecd3730391f229539caedfa5c61319f7485085d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\" id=\"brochureUpload\" data-button=\"uploader_brochure_pickfiles\" data-container=\"uploader_brochure\"
     data-url=\"";
        // line 2
        echo twig_escape_filter($this->env, site_url("ami/media/upload"), "html", null, true);
        echo "\"
     data-multi=\"true\"
     data-maxfilesize=\"100mb\"
     data-params=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array("entry_type" => (isset($context["entry_type"]) ? $context["entry_type"] : null), "class" => "brochure")), "html", null, true);
        echo "\"
     data-mimetypes=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_jsonencode_filter(array(0 => array("title" => "PDF Files", "extensions" => "pdf"))), "html", null, true);
        echo "\">
    ";
        // line 7
        $context["label"] = ((((isset($context["entry_type"]) ? $context["entry_type"] : null) == "recipes")) ? ("Recipe Sheet") : ("Brochure"));
        // line 8
        echo "    ";
        echo form_label((isset($context["label"]) ? $context["label"] : null), "inputBrochures", array("class" => "control-label col-sm-3"));
        echo "
    <div id=\"uploader_brochure\" class=\"controls uploader col-sm-6\">
        <div class=\"upload-actions\">
            <a class=\"btn btn-default pick-gallery Tooltip\"
               href=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url(("ami/media/view/brochure/" . (isset($context["entry_type"]) ? $context["entry_type"] : null))), "html", null, true);
        echo "\" data-toggle=\"ajaxModal\"
               title=\"Select from Media\"><i class=\"fa fa-photo\"></i> Media</a>&nbsp;
            <a class=\"btn btn-default btn-upload\" id=\"uploader_brochure_pickfiles\" href=\"javascript:;\"><i
                        class=\"fa fa-upload\"></i> Upload file</a>
        </div>
        <div class=\"filelist\"></div>
        <div class=\"uploaded row\" style=\"margin-top: 10px\">
            ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["brochures"]) ? $context["brochures"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["brochure"]) {
            // line 20
            echo "                <div class=\"col-md-4\">
                    <div class=\"thumbnail\">
                        <img src=\"";
            // line 22
            echo twig_escape_filter($this->env, site_url("res/img/pdf.png"), "html", null, true);
            echo "\" width=\"100%\" class=\"img-responsive\">
                        <div class=\"caption\">
                            <span class=\"label label-primary\"
                                  title=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["brochure"], "filename_old", array()), "html", null, true);
            echo "\">";
            echo wordTrim($this->getAttribute($context["brochure"], "filename_old", array()), 15);
            echo "</span>
                            <ul class=\"list-inline\" style=\"margin: 10px 0 0 0;\">
                                <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, (((isset($context["MEDIA_URL"]) ? $context["MEDIA_URL"] : null) . "/brochure/") . $this->getAttribute($context["brochure"], "path", array())), "html", null, true);
            echo "\" target=\"_blank\"
                                   class=\"btn btn-xs btn-default\"><i class=\"fa fa-eye\"></i> View</a>
                                <a href=\"#\" data-id=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["brochure"], "id", array()), "html", null, true);
            echo "\" data-class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brochure"], "class", array()), "html", null, true);
            echo "\"
                                   class=\"btn btn-xs btn-default pull-right delete-media\"><i class=\"fa fa-close\"></i>
                                    Delete</a>
                            </ul>
                        </div>
                        <div class=\"app_type\" data-id=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["brochure"], "id", array()), "html", null, true);
            echo "\">
                            <ul class=\"list-inline\" style=\"margin: 0 10px;\">
                                <input type=\"checkbox\" name=\"app_pull\"
                                       value=\"0\" ";
            // line 37
            echo (((($this->getAttribute($context["brochure"], "app_type", array()) == 0) || ($this->getAttribute($context["brochure"], "app_type", array()) == 2))) ? ("checked") : (""));
            echo ">
                                <span style=\"margin-right: 15px\">Pull </span>
                                <input type=\"checkbox\" name=\"app_push\"
                                       value=\"1\" ";
            // line 40
            echo (((($this->getAttribute($context["brochure"], "app_type", array()) == 1) || ($this->getAttribute($context["brochure"], "app_type", array()) == 2))) ? ("checked") : (""));
            echo ">
                                Push
                            </ul>
                        </div>
                    </div>
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brochure'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/uploader_brochure.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 48,  104 => 40,  98 => 37,  92 => 34,  82 => 29,  77 => 27,  70 => 25,  64 => 22,  60 => 20,  56 => 19,  46 => 12,  38 => 8,  36 => 7,  32 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }
}
