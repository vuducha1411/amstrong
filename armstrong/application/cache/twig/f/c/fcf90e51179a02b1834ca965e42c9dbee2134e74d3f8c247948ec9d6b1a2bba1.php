<?php

/* ami/sku_point/edit.html.twig */
class __TwigTemplate_fcf90e51179a02b1834ca965e42c9dbee2134e74d3f8c247948ec9d6b1a2bba1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/sku_point/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });
        });
        function validateForm(){
            var flag = 0;
            \$('.skunumber').each(function(){
                if(!\$(this).val()){
                    alert(\"Invalid product.\");
                    flag = 1;
                }
            });

            if (flag == 1)
                return false;
            else
                return true;
        }
        function eraseProduct(x){
            \$('.productvalue'+x).val(\"\");
            \$('.productvalue'+x).prop('readonly', false);
        }
        function addAutoSku(){
            var sku_except = '?sku_except=';
            \$('.skunumber').each(function(){
                sku_except += ','+\$(this).val();
            });
            \$('.skuproduct').autocomplete({
                source: \"";
        // line 50
        echo twig_escape_filter($this->env, site_url("ami/products/tokenfield"), "html", null, true);
        echo "\"+sku_except,
                delay: 25,
                select: function (event, ui) {
                    event.preventDefault();
                    \$(this).prop('readonly', true);
                    \$('.erase'+\$(this).data('nbconfig')).show();
                    \$(this).val(ui.item.label);
                    \$('.skuvalue'+\$(this).data('nbconfig')).val(ui.item.value);
                }
            }).on('focus', function () {
                this.select();
            });
        }
        addAutoSku();
        ";
        // line 64
        if ($this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "point_settings", array())) {
            // line 65
            echo "        var nb_config = ";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "point_settings", array())), "html", null, true);
            echo ";
        ";
        } else {
            // line 67
            echo "        var nb_config = 0;
        ";
        }
        // line 69
        echo "        \$('#addpointsetting').click(function(){
            var flag = true;
            \$('.skuproduct').each(function(){
                if(\$(this).val() == '')
                {
                    \$(this).focus();
                    flag = false;
                }
            });
            if(flag)
            {
                nb_config++;
                var html = ''+
                '<tr class=\"pointst'+nb_config+'\"> ' +
                    '<td width=\"3\">'+
                        '<p style=\"font-size: 17pt; display: none\" class=\"erase'+nb_config+'\" onClick=\"eraseProduct('+nb_config+')\">&#215; </p>'+
                    '</td>'+
                    '<td width=\"70%\"> '+
                        '<input type=\"text\" data-nbconfig=\"'+nb_config+'\" class=\"form-control skuproduct productvalue'+nb_config+'\" name=\"point_settings['+nb_config+'][sku_name]\" required=\"required\" value=\"\">'+
                        '<input type=\"hidden\" class=\"skuvalue'+nb_config+' skunumber\" name=\"point_settings['+nb_config+'][sku]\" value=\"\" >'+
                    '</td>'+
                    '<td>'+
                        '<input type=\"number\" class=\"form-control\" name=\"point_settings['+nb_config+'][point]\" required=\"required\" value=\"0\" min=\"0\" step=\".01\">'+
                    '</td>'+
                    '<td>'+
                        '<span class=\"btn btn-danger removepointst\" data-nbconfig=\"'+nb_config+'\">Remove</span>'+
                    '</td>'+
                '</tr>';
                \$('#pointsetting').append(html);
                addAutoSku();
            }
        });
        \$(document).on('click', '.removepointst', function () {
           \$('.pointst'+\$(this).data('nbconfig')).remove();
        });
    </script>
";
    }

    // line 107
    public function block_css($context, array $blocks = array())
    {
        // line 108
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 109
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 112
    public function block_content($context, array $blocks = array())
    {
        // line 113
        echo "    ";
        echo form_open(site_url("ami/sku_point/save"), array("class" => "form-horizontal"), array("id" => $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "id", array())), array("onSubmit" => "return validateForm()"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Sku Point</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 122
        if (($this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "dateFlag", array()) != 1)) {
            // line 123
            echo "                            ";
            echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success", "onclick" => "return validateForm()"));
            echo "
                        ";
        }
        // line 125
        echo "
                        ";
        // line 126
        echo html_btn(site_url("ami/sku_point"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 127
        if ($this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "id", array())) {
            // line 128
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/sku_point/delete/" . $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 131
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 141
        echo form_label("From", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 143
        echo form_input(array("name" => "from_date", "value" => $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "from_date", array()), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 147
        echo form_label("To", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 149
        echo form_input(array("name" => "to_date", "value" => $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "to_date", array()), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 153
        echo form_label("AVP Name", "name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 155
        echo form_input(array("name" => "name", "value" => $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "name", array()), "id" => "name", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control parsley-error") : ("form-control")), "data-parsley-required" => "true"));
        echo "

                    ";
        // line 157
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 158
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\" style=\"color:red;\">";
            // line 159
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 162
        echo "                </div>
            </div>
            ";
        // line 164
        if (((isset($context["enable_enhanced_avp"]) ? $context["enable_enhanced_avp"] : null) == 1)) {
            // line 165
            echo "            <div class=\"form-group\">
                ";
            // line 166
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-3 errors"));
            echo "
                <div class=\"col-sm-5\">
                    <!-- <select name=\"channel[]\" class=\"form-control fetch-otm\" id=\"ChannelSelect\" multiple> -->
                        ";
            // line 169
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["country_channels"]) ? $context["country_channels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 170
                echo "                            ";
                if (twig_in_filter($this->getAttribute($context["country_channel"], "id", array()), $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "channel", array()))) {
                    // line 171
                    echo "                                <input type=\"checkbox\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                    echo "\" name=\"channel[]\" checked=\"checked\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                    echo "</input>
                            ";
                } else {
                    // line 173
                    echo "                                <input type=\"checkbox\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                    echo "\" name=\"channel[]\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                    echo "</input>
                            ";
                }
                // line 175
                echo "                            <br />
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 177
            echo "                    <!-- </select> -->
                </div>
            </div>
            ";
        }
        // line 181
        echo "            <table class=\"table table-striped\">
                <thead>
                    <tr> 
                        <th width=\"3\"></th>
                        <th width=\"60%\">Product </th>
                        <th>Point</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id=\"pointsetting\">
                ";
        // line 191
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "point_settings", array()));
        foreach ($context['_seq'] as $context["key"] => $context["skupoint"]) {
            // line 192
            echo "                    <tr class=\"pointst";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
                        <td>
                            <p style=\"font-size: 17pt\" class=\"erase";
            // line 194
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" onClick=\"eraseProduct(";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo ")\">&#215; </p>
                        </td>
                        <td width=\"70%\">
                            <input type=\"text\" data-nbconfig=\"";
            // line 197
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" class=\"form-control skuproduct productvalue";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" name=\"point_settings[";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku_name]\" required=\"required\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["skupoint"], "sku_name", array()), "html", null, true);
            echo "\" readonly=\"readonly\">
                            <input type=\"hidden\" class=\"skuvalue";
            // line 198
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo " skunumber\" name=\"point_settings[";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][sku]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["skupoint"], "sku", array()), "html", null, true);
            echo "\" >
                        </td>
                        <td>
                            <input type=\"number\" class=\"form-control\" name=\"point_settings[";
            // line 201
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "][point]\" required=\"required\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["skupoint"], "point", array()), "html", null, true);
            echo "\" min=\"0\" step=\".01\">
                        </td>
                        <td>
                            <span class=\"btn btn-danger removepointst\" data-nbconfig=\"";
            // line 204
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">Remove</span>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['skupoint'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 208
        echo "                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=\"100%\">
                            <span class=\"btn btn-primary pull-right\" id=\"addpointsetting\">Add config point</span>
                        </td>
                    </tr>
                </tfoot>
            </table>
            ";
        // line 217
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["sku_point"]) ? $context["sku_point"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 222
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/sku_point/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 222,  408 => 217,  397 => 208,  387 => 204,  379 => 201,  369 => 198,  359 => 197,  351 => 194,  345 => 192,  341 => 191,  329 => 181,  323 => 177,  316 => 175,  308 => 173,  300 => 171,  297 => 170,  293 => 169,  287 => 166,  284 => 165,  282 => 164,  278 => 162,  272 => 159,  269 => 158,  267 => 157,  262 => 155,  257 => 153,  250 => 149,  245 => 147,  238 => 143,  233 => 141,  221 => 131,  214 => 128,  212 => 127,  208 => 126,  205 => 125,  199 => 123,  197 => 122,  184 => 113,  181 => 112,  175 => 109,  170 => 108,  167 => 107,  127 => 69,  123 => 67,  117 => 65,  115 => 64,  98 => 50,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
