<?php

/* ami/holidays/edit.html.twig */
class __TwigTemplate_fc686379409fd123ffc963c7850e8967417a00c6992c0395320dde6d7fd18567 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/holidays/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(function () {
            function check_date_duplicate(arr){
//                var arr = [9, 9, 111, 2, 3, 4, 4, 5, 7];
                var sorted_arr = arr.sort(); // You can define the comparing function here.
                                             // JS by default uses a crappy string compare.
                var results = [];
                for (var i = 0; i < arr.length - 1; i++) {
                    if (sorted_arr[i + 1] == sorted_arr[i]) {
                        results.push(sorted_arr[i]);
                    }
                }

                return results;
            }

            \$(document).on('click', '.btn-success', function(e){
                e.preventDefault();
                var \$this = \$(this),
                        inputDate = \$('.datepicker'),
                        holidayID = \$('input[name=\"armstrong_2_holiday_id\"]').val(),
                        arr_date = [];
                \$this.addClass('disabled');
                inputDate.each(function (){
                    var date = \$(this).val();
                    arr_date.push(date);
                });
                var dup = check_date_duplicate(arr_date);
                if(dup.length > 0){
                    alert('Date must be unique!');
                    \$this.removeClass('disabled');
                }
                else{
                    \$.ajax({
                        dataType: 'JSON',
                        type: 'POST',
                        url: base_url() + 'ami/holidays/checkDuplicate',
                        data: {id: holidayID, dates: arr_date},
                        success: function (result) {
                            if(result.status == 'error'){
                                alert(result.ms);
                                \$this.removeClass('disabled');
                            }else{
                                \$('#submit-form').submit();
                            }
                        }
                    });
                }
            });

            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        \$clone = \$(this).closest('.holiday_base').clone(),
                        \$counter = \$('.tokenfield').length;
                \$clone.find('.hide').removeClass('hide').show();
                if (\$this.hasClass('clone-add')) {
                    \$new = \$clone.clone();

                    \$new.find('input[name], select[name]').each(function () {
                        var \$this = \$(this);
                        if(\$this.hasClass('tokenfield')){
                            \$this.attr('name', 'holiday[' + \$counter + '][title]');
                        }else{
                            \$this.attr('name', 'holiday[' + \$counter + '][date]');
                        }

                        if (\$this.is('input:text')) {
                            \$this.val('');
                        }
                        else {
                            \$this.val(0);
                        }
                    });

                    \$new.find('.datepicker').datetimepicker({
                        format: 'YYYY-MM-DD'
                    });
                    \$new.appendTo(\$('#holiday_base_block'));
                }
                if (\$this.hasClass('clone-remove')) {
                    \$(this).closest('.holiday_base').remove();
                }
            });

            \$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });


    </script>
";
    }

    // line 104
    public function block_css($context, array $blocks = array())
    {
        // line 105
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 109
    public function block_content($context, array $blocks = array())
    {
        // line 110
        echo "    ";
        echo form_open(site_url("ami/holidays/save"), array("id" => "submit-form", "class" => "form-horizontal", "data-parsley-validate" => "true"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Holidays</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 119
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "
                        ";
        // line 121
        echo "                        ";
        echo html_btn(site_url("ami/holidays"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 122
        if ($this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "armstrong_2_holiday_id", array())) {
            // line 123
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/holidays/delete/" . $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "armstrong_2_holiday_id", array()))), "html", null, true);
            echo "\"
                               title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 127
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            ";
        // line 136
        if ((isset($context["holidays"]) ? $context["holidays"] : null)) {
            // line 137
            echo "                <div class=\"form-group\">
                    ";
            // line 138
            echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 140
            echo form_input(array("name" => "title", "value" => $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "title", array()), "class" => "form-control", "data-parsley-required" => "true"));
            echo "
                    </div>
                </div>

                <div class=\"form-group\">
                    ";
            // line 145
            echo form_label("Date", "date", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 147
            echo form_input(array("name" => "date", "value" => $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "date", array()), "class" => "form-control datepicker", "data-parsley-required" => "true"));
            echo "
                    </div>
                </div>
            ";
        } else {
            // line 151
            echo "                <div class=\"holiday_base\">
                    <div class=\"form-group\">
                        ";
            // line 153
            echo form_label("Title", "title", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 155
            echo form_input(array("name" => "holiday[0][title]", "value" => $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "title", array()), "class" => "form-control tokenfield", "data-parsley-required" => "true"));
            echo "
                        </div>

                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone clone-remove hide\"><i class=\"fa fa-minus\"></i></button>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        ";
            // line 165
            echo form_label("Date", "date", array("class" => "control-label col-sm-3"));
            echo "
                        <div class=\"col-sm-6\">
                            ";
            // line 167
            echo form_input(array("name" => "holiday[0][date]", "value" => $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "date", array()), "class" => "form-control datepicker", "data-parsley-required" => "true"));
            echo "
                        </div>
                    </div>
                    <hr>
                </div>
                <div id=\"holiday_base_block\"></div>
            ";
        }
        // line 174
        echo "            ";
        echo form_input(array("name" => "armstrong_2_holiday_id", "value" => $this->getAttribute((isset($context["holidays"]) ? $context["holidays"] : null), "armstrong_2_holiday_id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 179
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/holidays/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 179,  279 => 174,  269 => 167,  264 => 165,  251 => 155,  246 => 153,  242 => 151,  235 => 147,  230 => 145,  222 => 140,  217 => 138,  214 => 137,  212 => 136,  201 => 127,  193 => 123,  191 => 122,  186 => 121,  182 => 119,  169 => 110,  166 => 109,  160 => 106,  155 => 105,  152 => 104,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
