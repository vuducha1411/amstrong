<?php

/* ami/sales_cycle/edit.html.twig */
class __TwigTemplate_f67d65c003e2418311f2cc8a0ecb67932fa2da5a3936882e528a68c6595d4011 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/sales_cycle/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#from_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            \$('#to_date').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
            \$(\"#from_date\").on(\"dp.change\", function (e) {
                \$('#to_date').data(\"DateTimePicker\").minDate(e.date);
            });
            \$(\"#to_date\").on(\"dp.change\", function (e) {
                \$('#from_date').data(\"DateTimePicker\").maxDate(e.date);
            });
        });
    </script>
";
    }

    // line 29
    public function block_css($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, site_url("res/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\"/>
";
    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        // line 35
        echo "    ";
        echo form_open(site_url("ami/sales_cycle/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Sales Cycle</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        ";
        // line 44
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Save", "class" => "btn btn-success"));
        echo "

                        ";
        // line 46
        echo html_btn(site_url("ami/sales_cycle"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 47
        if ($this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array())) {
            // line 48
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/sales_cycle/delete/" . $this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 51
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 61
        echo form_label("From", "from_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 63
        echo form_input(array("name" => "from_date", "value" => $this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "from_date", array()), "id" => "from_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 67
        echo form_label("To", "to_date", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-5\">
                    ";
        // line 69
        echo form_input(array("name" => "to_date", "value" => $this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "to_date", array()), "id" => "to_date", "class" => (((isset($context["errors"]) ? $context["errors"] : null)) ? ("form-control datepicker parsley-error") : ("form-control datepicker")), "data-parsley-required" => "true"));
        echo "
                    ";
        // line 70
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 71
            echo "                        <ul class=\"parsley-errors-list filled\">
                            <li class=\"parsley-overlaps\">";
            // line 72
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo "</li>
                        </ul>
                    ";
        }
        // line 75
        echo "                </div>
            </div>

            ";
        // line 78
        echo form_input(array("name" => "id", "value" => $this->getAttribute((isset($context["sales_cycle"]) ? $context["sales_cycle"] : null), "id", array()), "type" => "hidden", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 83
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/sales_cycle/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 83,  174 => 78,  169 => 75,  163 => 72,  160 => 71,  158 => 70,  154 => 69,  149 => 67,  142 => 63,  137 => 61,  125 => 51,  118 => 48,  116 => 47,  112 => 46,  107 => 44,  94 => 35,  91 => 34,  85 => 31,  80 => 30,  77 => 29,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
