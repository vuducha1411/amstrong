<?php

/* ami/salespersons/manager.html.twig */
class __TwigTemplate_ff54b0ba116ec3e46a119f9feb18918976361438cb236f6866785e54e9769e39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (twig_length_filter($this->env, (isset($context["managerss"]) ? $context["managerss"] : null))) {
            // line 2
            echo "    <div class=\"form-group\">
        ";
            // line 3
            echo form_label("Manager", "salespersons_manager_id", array("class" => "control-label col-sm-3"));
            echo "
        <div class=\"col-sm-6\">
            <select name=\"salespersons_manager_id\" class=\"form-control\">
                ";
            // line 6
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["managers"]) ? $context["managers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["manager"]) {
                // line 7
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "armstrong_2_salespersons_id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute((isset($context["salesperson"]) ? $context["salesperson"] : null), "manager_id", array()) == $this->getAttribute($context["manager"], "id", array()))) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "first_name", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["manager"], "last_name", array()), "html", null, true);
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manager'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "     
            </select>
        </div>
    </div>
";
        }
        // line 13
        echo "
";
        // line 14
        if ((isset($context["persons"]) ? $context["persons"] : null)) {
            // line 15
            echo "    <div class=\"form-group\">
        ";
            // line 16
            echo form_label("Persons", "persons", array("class" => "control-label col-sm-3"));
            echo "
        <div class=\"col-sm-6\">
            ";
            // line 18
            echo form_input(array("name" => "persons", "class" => "form-control", "id" => "tokenfield"));
            echo "
        </div>
    </div>

    <script type=\"text/javascript\">
    \$(function() {
        var source = ";
            // line 24
            echo twig_jsonencode_filter((isset($context["persons"]) ? $context["persons"] : null));
            echo ";

        \$('#tokenfield').tokenfield('destroy');
        \$('#tokenfield')
        .on('tokenfield:createtoken', function (e) {
            var existingTokens = \$(this).tokenfield('getTokens');
            \$.each(existingTokens, function(index, token) {
                if (token.value === e.attrs.value) {
                    e.preventDefault();
                }
            });
        })
        .on('tokenfield:createdtoken', function (e) {
            var found = false;
            \$.each(source, function(index, token) {
                if (token.value === e.attrs.value) {
                    // e.preventDefault();
                    found = true;
                }
            });

            if (!found) {
                \$(e.relatedTarget).addClass('invalid');
            }
        })
        .tokenfield({
            autocomplete: {
                source: source,
                delay: 100
            }
        });
    });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "ami/salespersons/manager.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 24,  69 => 18,  64 => 16,  61 => 15,  59 => 14,  56 => 13,  49 => 8,  34 => 7,  30 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
