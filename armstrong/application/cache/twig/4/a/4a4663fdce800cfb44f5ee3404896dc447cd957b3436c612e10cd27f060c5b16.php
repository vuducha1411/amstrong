<?php

/* ami/setting_game/preview.html.twig */
class __TwigTemplate_4a4663fdce800cfb44f5ee3404896dc447cd957b3436c612e10cd27f060c5b16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\" style=\"max-height: 400px\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\"> Game setting season ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "season", array()), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"modal-body\">
            <div class=\"panel\">
                <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Game plan configuration</h4>
                            <p class=\"list-group-item-text\">From ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_start", array()), "html", null, true);
        echo " - To ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "date_expired", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Prizes configuration</h4>
                            <p class=\"list-group-item-text\">Special Prize: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_special", array()), "html", null, true);
        echo " - Steps number: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_special", array()), "html", null, true);
        echo "</p>
                            <p class=\"list-group-item-text\">1nd Prize: ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_first", array()), "html", null, true);
        echo " - Steps number: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_first", array()), "html", null, true);
        echo "</p>
                            <p class=\"list-group-item-text\">2nd Prize: ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_prize", array()), "prize_second", array()), "html", null, true);
        echo " - Steps number: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "limit_prize", array()), "prize_second", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Pull SR</h4>
                            <p class=\"list-group-item-text\">Min number of criterias: ";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new", array()), "min_number_of_criterias", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_pull_new_total", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Push SR</h4>
                            <p class=\"list-group-item-text\">Min number of criterias: ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new", array()), "min_number_of_criterias", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_push_new_total", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"list-group-item\">
                        <i class=\"fa fa-2x pull-left\"></i>
                        <div class=\"m-l-xl\">                        
                            <h4 class=\"list-group-item-heading text-muted\">Sale Leader</h4>
                            Min number of criterias 1/";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "config_leader_total", array()), "html", null, true);
        echo "
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
        </div>
    </div>
</div>

<script>
\$(document).ready(function() {
    \$('.modal-body').slimScroll({
        height: '400px'
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "ami/setting_game/preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 48,  88 => 40,  75 => 32,  62 => 24,  56 => 23,  50 => 22,  37 => 14,  25 => 5,  19 => 1,);
    }
}
