<?php

/* base.html.twig */
class __TwigTemplate_4032d8356976dfaa2094e9b94d9bed8621f88f9eefae38bed65e41166d8e3ea2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'htmlClass' => array($this, 'block_htmlClass'),
            'head' => array($this, 'block_head'),
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'css' => array($this, 'block_css'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'body' => array($this, 'block_body'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\" class=\"";
        // line 2
        $this->displayBlock('htmlClass', $context, $blocks);
        echo "\">

<head>
\t";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 25
        echo "</head>

<body class=\"";
        // line 27
        $this->displayBlock('bodyClass', $context, $blocks);
        echo "\">
";
        // line 28
        $this->displayBlock('body', $context, $blocks);
        // line 29
        echo "
";
        // line 30
        $this->displayBlock('js', $context, $blocks);
        // line 31
        echo "</body>
</html>";
    }

    // line 2
    public function block_htmlClass($context, array $blocks = array())
    {
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "    <meta charset=\"utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />

    ";
        // line 10
        $this->displayBlock('meta', $context, $blocks);
        // line 14
        echo "
    <title>";
        // line 15
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn|Bevan' rel='stylesheet' type='text/css'>
    ";
        // line 17
        $this->displayBlock('css', $context, $blocks);
        // line 18
        echo "
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

    ";
    }

    // line 10
    public function block_meta($context, array $blocks = array())
    {
        // line 11
        echo "    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    ";
    }

    // line 15
    public function block_title($context, array $blocks = array())
    {
    }

    // line 17
    public function block_css($context, array $blocks = array())
    {
    }

    // line 27
    public function block_bodyClass($context, array $blocks = array())
    {
    }

    // line 28
    public function block_body($context, array $blocks = array())
    {
    }

    // line 30
    public function block_js($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  123 => 30,  118 => 28,  113 => 27,  108 => 17,  103 => 15,  97 => 11,  94 => 10,  84 => 18,  82 => 17,  77 => 15,  74 => 14,  72 => 10,  66 => 6,  63 => 5,  58 => 2,  53 => 31,  51 => 30,  48 => 29,  46 => 28,  42 => 27,  38 => 25,  36 => 5,  30 => 2,  27 => 1,);
    }
}
