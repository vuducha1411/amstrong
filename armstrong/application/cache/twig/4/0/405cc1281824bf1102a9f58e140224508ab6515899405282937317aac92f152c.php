<?php

/* ami/admin_management/edit.html.twig */
class __TwigTemplate_405cc1281824bf1102a9f58e140224508ab6515899405282937317aac92f152c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/admin_management/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    ";
        // line 5
        $context["validationUrl"] = (site_url("ami/admin_management/email_validation") . (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "email", array())) ? (("/" . $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "id", array()))) : ("")));
        // line 6
        echo "    <script>
        jQuery('#email').parsley().addAsyncValidator(
                'email', function(xhr) {
                    var registerEmail = \$('#email').parsley();
                    window.ParsleyUI.removeError(registerEmail, 'errorEmail');
                    if (xhr.status == '200') {
                        return 200;
                    }

                    if (xhr.status == '404') {
                        response = \$.parseJSON(xhr.responseText);
                        window.ParsleyUI.addError(registerEmail, 'errorEmail', response.error);
                    }
                }, '";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["validationUrl"]) ? $context["validationUrl"] : null), "html", null, true);
        echo "'
        );
    </script>
";
    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        // line 25
        echo "    <div id=\"ModalView\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\" data-keyboard=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&#735;</button>
                    <h3 class=\"modal-title\">Preview</h3>
                </div>
                <div class=\"modal-body\"></div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    ";
        // line 39
        echo form_open(site_url("ami/admin_management/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "id", array())));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Admin Management</h1>
                <div class=\"text-right\">
                    ";
        // line 46
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/admin_management/edit.html.twig", 46)->display(array_merge($context, array("url" => "ami/admin_management", "id" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "id", array()), "permission" => "admin_management")));
        // line 47
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 57
        echo form_label("Firstname", "first_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 59
        echo form_input(array("name" => "first_name", "id" => "first_name", "value" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "first_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        // line 60
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 64
        echo form_label("Lastname", "last_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 66
        echo form_input(array("name" => "last_name", "id" => "last_name", "value" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "last_name", array()), "class" => "form-control", "data-parsley-required" => "true"));
        // line 67
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 71
        echo form_label("Global Roles", "roles_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 73
        echo form_dropdown("role_id", (isset($context["globalRoles"]) ? $context["globalRoles"] : null), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "role_id", array()), "class=\"form-control\" ");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 77
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 79
        echo form_input(array("name" => "email", "id" => "email", "value" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email", "data-ajax-name" => "_unique_email", "data-parsley-remote-options" => "{'type':'POST','data':{'email':'_unique_email'}}", "data-parsley-remote-validator" => "email", "data-parsley-remote" => "1", "data-parsley-errors-messages-disabled" => "1"));
        // line 84
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 88
        echo form_label("Password", "password", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 90
        echo form_input(array("type" => "password", "name" => "password", "id" => "password", "value" => null, "class" => "form-control", "data-parsley-required" => (($this->getAttribute(        // line 91
(isset($context["admin"]) ? $context["admin"] : null), "password", array())) ? ("false") : ("true")), "data-parsley-minlength" => "6"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 96
        echo form_label("Confirm Password", "confirm_password", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 98
        echo form_input(array("type" => "password", "name" => "confirm_password", "id" => "confirm_password", "value" => null, "class" => "form-control", "data-parsley-required" => (($this->getAttribute(        // line 99
(isset($context["admin"]) ? $context["admin"] : null), "password", array())) ? ("false") : ("true")), "data-parsley-equalto" => "#password", "data-parsley-minlength" => "6", "data-parsley-error-message" => "Password and Confirm Password must be the same."));
        // line 101
        echo "
                </div>
            </div>

            ";
        // line 105
        echo form_input(array("name" => "uniqueId", "value" => twig_random($this->env), "type" => "hidden", "class" => "hide"));
        echo "
        </div>

    </div>

    ";
        // line 110
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/admin_management/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 110,  184 => 105,  178 => 101,  176 => 99,  175 => 98,  170 => 96,  162 => 91,  161 => 90,  156 => 88,  150 => 84,  148 => 79,  143 => 77,  136 => 73,  131 => 71,  125 => 67,  123 => 66,  118 => 64,  112 => 60,  110 => 59,  105 => 57,  93 => 47,  91 => 46,  81 => 39,  65 => 25,  62 => 24,  54 => 19,  39 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
