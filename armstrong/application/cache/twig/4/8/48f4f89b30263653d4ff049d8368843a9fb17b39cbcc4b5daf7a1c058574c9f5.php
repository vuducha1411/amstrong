<?php

/* ami/wholesalers/pending_action_data.html.twig */
class __TwigTemplate_48f4f89b30263653d4ff049d8368843a9fb17b39cbcc4b5daf7a1c058574c9f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"panel\">
    <table class=\"table table-striped table-bordered table-hover\">
        <thead>
        <tr>
            <th>Field</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 11
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["wholesaler"]) ? $context["wholesaler"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 12
            echo "            ";
            if ((($context["key"] == "cuisine_name") || ($context["key"] == "serving_name"))) {
                // line 13
                echo "            ";
            } else {
                // line 14
                echo "                <tr class=\"";
                echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                echo "\">
                    <td>";
                // line 15
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</td>
                    <td>";
                // line 16
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</td>
                </tr>
                ";
                // line 18
                if (($context["key"] == "cuisine_channels_id")) {
                    // line 19
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 20
                    echo "cuisine_channels_name";
                    echo "</td>
                        <td>";
                    // line 21
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "cuisine_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 24
                echo "                ";
                if (($context["key"] == "serving_types_id")) {
                    // line 25
                    echo "                    <tr class=\"";
                    echo ((twig_in_filter($context["key"], $this->getAttribute((isset($context["amendedData"]) ? $context["amendedData"] : null), "fields", array()))) ? ("warning") : (""));
                    echo "\">
                        <td>";
                    // line 26
                    echo "serving_types_name";
                    echo "</td>
                        <td>";
                    // line 27
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wholesalers"]) ? $context["wholesalers"] : null), "serving_name", array()), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                // line 30
                echo "
            ";
            }
            // line 32
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/wholesalers/pending_action_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 33,  94 => 32,  90 => 30,  84 => 27,  80 => 26,  75 => 25,  72 => 24,  66 => 21,  62 => 20,  57 => 19,  55 => 18,  50 => 16,  46 => 15,  41 => 14,  38 => 13,  35 => 12,  30 => 11,  19 => 1,);
    }
}
