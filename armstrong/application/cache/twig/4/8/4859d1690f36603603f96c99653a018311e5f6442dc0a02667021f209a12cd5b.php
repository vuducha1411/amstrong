<?php

/* ami/tfo/index.html.twig */
class __TwigTemplate_4859d1690f36603603f96c99653a018311e5f6442dc0a02667021f209a12cd5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/tfo/index.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("js", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/dataTables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/import.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("res/js/call_modules.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
    \$('#dataTables').dataTable({
        /*\"processing\": true,
        \"serverSide\": true,
        \"ajax\": \"";
        // line 17
        echo twig_escape_filter($this->env, site_url("ami/tfo/ajaxData"), "html", null, true);
        echo "\",*/
        'order': [[1, 'asc']],
        'bPaginate': false,
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });

    // \$('#Generate').click(function(e) {
    //     xhr = null;
    //     var \$startDate = \$('#InputStartDate'),
    //         \$endDate = \$('#InputEndDate'),
    //         \$container = \$('#TfoContainer');

    //     if (xhr) {
    //         xhr.abort();
    //     }

    //     if (!xhr && \$startDate.val() && \$endDate.val()) {
            
    //         \$container.html('').empty().html('<div class=\"col-sm-6 col-sm-offset-3\"><i class=\"fa fa-spinner fa-spin\"></i> Loading...</div>');

    //         \$.ajax({
    //             url: \"";
        // line 41
        echo twig_escape_filter($this->env, site_url("ami/tfo/tfo_data"), "html", null, true);
        echo "\",
    //             type: 'post',
    //             data: { start_date: \$startDate.val(), end_date: \$endDate.val() , draft: ";
        // line 43
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo " },
    //             success: function(res) {
    //                 try {
    //                     var resJson = JSON.parse(res);
    //                     if (resJson._session_expried) {
    //                         window.location.reload();
    //                         return;
    //                     }
                        
    //                 } catch(e) {}
                    
    //                 \$container.html('').empty();
    //                 if (res) {
    //                     \$container.append(res);
    //                     CheckAll(\$('input.CheckAll'));
    //                 }

    //                 xhr = null;
    //             }
    //         });
    //     }
    // });

    \$('#InputDateRange').daterangepicker({
        format: 'YYYY-MM-DD'
    }).on('apply.daterangepicker', function(ev, picker) {
        \$('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD'));
        \$('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD'));
    });
});
</script>
";
    }

    // line 76
    public function block_css($context, array $blocks = array())
    {
        // line 77
        $this->displayParentBlock("css", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, site_url("res/css/plugins/dataTables/dataTables.bootstrap.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
";
    }

    // line 82
    public function block_content($context, array $blocks = array())
    {
        // line 83
        echo "
    ";
        // line 84
        echo form_open(site_url("ami/tfo/update"), array("class" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("form-horizontal") : ("form-horizontal submit-confirm"))), array("draft" => (isset($context["draft"]) ? $context["draft"] : null)));
        echo "

    <div class=\"row row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">";
        // line 89
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? ("Tfo Draft") : ("Tfo"));
        echo "</h1>
                <div class=\"text-right\">                    
                    <span class=\"btn-header-toolbar hide m-r\" id=\"CheckAllBtn\">                   
                        ";
        // line 92
        echo form_button(array("type" => "submit", "content" => (((isset($context["draft"]) ? $context["draft"] : null)) ? ("<i class=\"fa fa-fw fa-history\"></i> Restore") : ("<i class=\"fa fa-fw fa-close\"></i> Delete")), "class" => "btn btn-sm btn-danger"));
        echo "
                    </span>
                    ";
        // line 94
        $this->loadTemplate("ami/components/table_btn_head.html.twig", "ami/tfo/index.html.twig", 94)->display(array_merge($context, array("url" => "ami/tfo", "icon" => "fa-money", "title" => "TFO", "permission" => "tfo")));
        // line 95
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">            
            ";
        // line 149
        echo "
            ";
        // line 150
        if ((twig_length_filter($this->env, (isset($context["salespersons"]) ? $context["salespersons"] : null)) > 1)) {
            // line 151
            echo "                <div class=\"form-group\">
                    ";
            // line 152
            echo form_label("Salespersons", "salespersons", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 154
            echo form_dropdown("salespersons", (isset($context["salespersons"]) ? $context["salespersons"] : null), null, "id=\"SalespersonsSelector\" class=\"form-control select-multiple\" data-parsley-required=\"true\" multiple");
            echo "
                    </div>
                </div>
            ";
        }
        // line 158
        echo "
            <div class=\"form-group\">
                ";
        // line 160
        echo form_label("Date Time", "date_time", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 162
        echo form_input(array("name" => "date_time", "value" => null, "class" => "form-control", "id" => "InputDateRange"));
        // line 165
        echo "
                </div>

                <input type=\"hidden\" id=\"InputStartDate\" value=\"\" />
                <input type=\"hidden\" id=\"InputEndDate\" value=\"\" />
            </div>

            ";
        // line 177
        echo "
            <div class=\"form-group\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <a href=\"#\" class=\"btn btn-primary\" id=\"Generate\" 
                        data-url=\"";
        // line 181
        echo twig_escape_filter($this->env, site_url("ami/tfo/tfo_data"), "html", null, true);
        echo "\"
                        data-container=\"#TfoContainer\"
                        data-draft=\"";
        // line 183
        echo (((isset($context["draft"]) ? $context["draft"] : null)) ? (1) : (0));
        echo "\">Generate</a>
                </div>
            </div>

            <div id=\"TfoContainer\"></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    ";
        // line 192
        echo form_close();
        echo "

";
    }

    public function getTemplateName()
    {
        return "ami/tfo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 192,  244 => 183,  239 => 181,  233 => 177,  224 => 165,  222 => 162,  217 => 160,  213 => 158,  206 => 154,  201 => 152,  198 => 151,  196 => 150,  193 => 149,  182 => 95,  180 => 94,  175 => 92,  169 => 89,  161 => 84,  158 => 83,  155 => 82,  149 => 79,  145 => 78,  141 => 77,  138 => 76,  102 => 43,  97 => 41,  70 => 17,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  45 => 7,  41 => 6,  37 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
