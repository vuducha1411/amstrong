<?php

/* ami/components/table_btn.html.twig */
class __TwigTemplate_4f464327f7cc0eed60a6b10cdcb624295866f0ba922e990e985048d413cdad6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group\">    
    ";
        // line 2
        if (call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("edit", (isset($context["permission"]) ? $context["permission"] : null)))) {
            echo "                                            
        ";
            // line 3
            echo html_btn(site_url((((isset($context["url"]) ? $context["url"] : null) . "/edit/") . (isset($context["id"]) ? $context["id"] : null))), "<i class=\"fa fa-edit\"></i>", array("class" => "btn-default edit", "title" => "Edit"));
            echo "
    ";
        }
        // line 5
        echo "
    ";
        // line 6
        if (((isset($context["id"]) ? $context["id"] : null) && call_user_func_array($this->env->getFunction('has_permission')->getCallable(), array("delete", (isset($context["permission"]) ? $context["permission"] : null))))) {
            // line 7
            echo "        ";
            if ((isset($context["draft"]) ? $context["draft"] : null)) {
                // line 8
                echo "            ";
                echo html_btn(site_url((((isset($context["url"]) ? $context["url"] : null) . "/restore/") . (isset($context["id"]) ? $context["id"] : null))), "<i class=\"fa fa-history\"></i>", array("class" => "btn-default restore", "title" => "Restore", "data-toggle" => "ajaxModal"));
                echo "        
        ";
            } else {
                // line 10
                echo "            ";
                echo html_btn(site_url((((isset($context["url"]) ? $context["url"] : null) . "/delete/") . (isset($context["id"]) ? $context["id"] : null))), "<i class=\"fa fa-remove\"></i>", array("class" => "btn-default delete", "title" => "Delete", "data-toggle" => "ajaxModal"));
                echo "
        ";
            }
            // line 12
            echo "    ";
        }
        // line 13
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/table_btn.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 13,  51 => 12,  45 => 10,  39 => 8,  36 => 7,  34 => 6,  31 => 5,  26 => 3,  22 => 2,  19 => 1,);
    }
}
