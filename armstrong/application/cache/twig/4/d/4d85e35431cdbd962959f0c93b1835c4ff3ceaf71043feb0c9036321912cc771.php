<?php

/* ami/components/modal_form.html.twig */
class __TwigTemplate_4d85e35431cdbd962959f0c93b1835c4ff3ceaf71043feb0c9036321912cc771 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal-dialog\">
    ";
        // line 2
        echo form_open((isset($context["action"]) ? $context["action"] : null), array("class" => "modal-content"), array("redirect" => (isset($context["redirect"]) ? $context["redirect"] : null)));
        echo "
        <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
            <h3 id=\"myModalLabel\">Confirm</h3>
        </div>
        <div class=\"modal-body\">
            <p>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>
        </div>
        <div class=\"modal-footer\">
            <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-fw fa-close\"></i> Close</button>
            ";
        // line 12
        echo form_button(array("type" => "submit", "content" => "<i class=\"fa fa-fw fa-check\"></i> Proceed", "class" => "delete btn btn-danger"));
        echo "
        </div>
    ";
        // line 14
        echo form_close();
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "ami/components/modal_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 14,  38 => 12,  31 => 8,  22 => 2,  19 => 1,);
    }
}
