<?php

/* ami/discount_model/edit_setting.html.twig */
class __TwigTemplate_4dfdc20cbc7119f7a9005ca1d3b1c1f9ad9dafb80f1a04e014f5a5fe1d3d6af5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/discount_model/edit_setting.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/plupload/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("res/js/upload.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("res/js/media.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function urlParam(url, name) {
            var results = new RegExp('[\\?&]' + name + \"=([^&#/]*)\").exec(url);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        function showHideCloneButton() {
            var element = \$('.product-step'),
                    counter;

            \$.each(element, function (k, v) {
                var step = \$(this).find('.step');
                counter = step.length;
                \$.each(step, function (i, j) {
                    if (i + 1 != counter) {
                        \$(this).find('.btn-clone').hide();
                    } else {
                        \$(this).find('.btn-clone').show();
                    }

                    if (counter == 1) {
                        \$(this).find('.clone-remove').hide();
                    }
//                    hieu fixed
                    if (counter == 2) {
                        \$(this).find('.clone-add').hide();
                    }
                    if (counter == 3) {
                        \$(this).find('.clone-add').hide();
                    }
                });
            });
        }

        function showHideProductElement(type) {
            var base = \$('.product-step .base[data-section != \"0\"]'),
                    product = \$('.product-step .products'),
                    productGroup = product.closest('.form-group');
            if (type == 1) {
                product.removeAttr('data-parsley-required');
                base.find('input[type=\"text\"]').removeAttr('data-parsley-required');
                productGroup.hide();
                base.hide();
            } else {
                product.attr('data-parsley-required', 'true');
                base.find('input[type=\"text\"]').attr('data-parsley-required', 'true');
                productGroup.show();
                base.show();
            }
        }

        function showHideProducts(cat_id) {
//            \$('#products option:selected').prop('selected', false);
            \$('.products option[data-cat-web=\"' + cat_id + '\"]').show();
            \$('.products option[data-cat-web!=\"' + cat_id + '\"]').hide();
        }

        function setHtmlError(rs){
            var elementError = rs.type == 1 ? \$('#cat_web') : \$('.product-step .products'),
                    errorList = elementError.closest('.form-group').find('.parsley-errors-list'),
                    errorMessage = '<li class=\"parsley-required\">' + rs.message + '</li>';

            if (rs.success == false) {
                elementError.addClass('parsley-error');
                errorList.addClass('filled');
                errorList.empty().append(errorMessage);
                \$('button[type=\"submit\"]').prop('disabled', true);
                return false;
            } else {
                elementError.removeClass('parsley-error');
                errorList.removeClass('filled');
                errorList.empty();
                \$('button[type=\"submit\"]').prop('disabled', false);
                return true;
            }
        }

        function ajaxValidate(url, data) {
            \$.ajax({
                'url': url,
                'type': 'GET',
                'data': data,
                'dataType': 'json',
                success: function (rs) {
                    setHtmlError(rs);
                }
            });
        }

        \$(function () {
            var section_counter = 0,
                    type_element = \$('#type'),
                    cat_web_element = \$('#cat_web');

            showHideProductElement(type_element.val());
            showHideProducts(cat_web_element.val());
            showHideCloneButton();

            type_element.change(function () {
                var \$val = \$(this).val();
                cat_web_element.val('');
                showHideProductElement(\$val);
                showHideProducts(cat_web_element.val());
            });

            \$(document).on('change', '#cat_web, .product-step .products', function () {
                var \$val = \$(this).val(),
                        \$this_id = \$(this).attr('id'),
                        ajaxUrl = '";
        // line 123
        echo twig_escape_filter($this->env, site_url("ami/discount_model/validate"), "html", null, true);
        echo "',
                        type = type_element.val(),
                        range_id = \$('#range_id').val(),
                        id = \$('#hidden_id').val(),
                        cat_web_id = cat_web_element.val(),
                        data;
                if (\$this_id == 'cat_web') {
                    showHideProducts(\$val);
                }

                if (type == 1) {
                    data = {
                        'type': type,
                        'range_id': range_id,
                        'id': id,
                        'cat_web_id': cat_web_id
                    };
                    ajaxValidate(ajaxUrl, data);
                } else if (type == 2 && \$val) {
                    var temp_arr = [];
                    \$.each(\$('.product-step .products'), function () {
                        var value = \$(this).val();
                        \$.each(value, function(k,v){
                            temp_arr.push(v);
                        });
                    });

                    temp_arr = temp_arr.sort();
                    var results = [];
                    for (var i = 0; i < temp_arr.length - 1; i++) {
                        if (temp_arr[i + 1] == temp_arr[i]) {
                            results.push(temp_arr[i]);
                        }
                    }

                    if(results.length > 0){
                        var rs = {
                            'type': type,
                            'success': false,
                            'message': 'This product has been choose'
                        };

                        setHtmlError(rs);
                    }else{
                        data = {
                            'type': type,
                            'range_id': range_id,
                            'id': id,
                            'cat_web_id': cat_web_id,
                            'sku_number': \$val
                        };
                        ajaxValidate(ajaxUrl, data);
                    }
                }
            });

            \$(document).on('click', '.btn-clone', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        base = \$this.closest('.base'),
                        section_number = base.data('section'),
                        \$clone = base.find('.clone-step').clone(),
                        counter = base.find('.step').length;

                if (\$this.hasClass('clone-add') && counter < 3) {
                    \$clone.removeClass('hide clone-step').addClass('step');
                    var closestStep = \$clone.closest('.step'),
                            input_case = closestStep.find('.input-case'),
                            input_discount = closestStep.find('.input-discount'),
                            tier_case = \$('<input class=\"form-control\" min=\"1\" name=\"section[' + section_number + '][tier][tier_' + (counter + 1) + '_case]\" data-parsley-required=\"true\" data-parsley-type=\"number\">'),
                            tier_discount = \$('<input class=\"form-control\" min=\"1\" name=\"section[' + section_number + '][tier][tier_' + (counter + 1) + '_discount]\" data-parsley-required=\"true\" data-parsley-type=\"number\">');

                    closestStep.find('.case-label').text('Tier ' + (counter + 1) + ' Case');
                    tier_case.appendTo(input_case);
                    tier_discount.appendTo(input_discount);
                    \$clone.appendTo(base);
                    showHideCloneButton();
                }

                if (\$this.hasClass('clone-remove') && counter > 1) {
                    base.find('.step:last').remove();
                    showHideCloneButton();
                }
            });

            \$(document).on('click', '.btn-clone-section', function (e) {
                e.preventDefault();
                var \$this = \$(this),
                        base = \$this.closest('#section-base'),
                        \$clone = base.find('.clone-product-step').clone();
                section_counter = section_counter > 0 ? section_counter + 1 : base.find('.product-step').length;

                if (\$this.hasClass('clone-add')) {
                    \$clone.removeClass('hide clone-product-step').addClass('product-step');
                    var closestStep = \$clone.closest('.product-step');

                    closestStep.find('.base').attr('data-section', section_counter);
                    closestStep.find('.products').attr('name', 'section[' + section_counter + '][sku_number][]').attr('data-parsley-required', true);
                    closestStep.find('.input-case').attr('name', 'section[' + section_counter + '][tier][tier_1_case]').attr('data-parsley-required', true).attr('data-parsley-type', 'number');
                    closestStep.find('.input-discount').attr('name', 'section[' + section_counter + '][tier][tier_1_discount]').attr('data-parsley-required', true).attr('data-parsley-type', 'number');
                    \$clone.appendTo(base);
                    showHideCloneButton();
                }

                if (\$this.hasClass('clone-remove') && \$('.product-step').length > 1) {
                    base.find('.product-step:last').remove();
                    showHideCloneButton();
                }
            });
        });

    </script>
";
    }

    // line 237
    public function block_css($context, array $blocks = array())
    {
        // line 238
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 239
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datepicker/datepicker3.css"), "html", null, true);
        echo "\"/>
    <style>
        select[multiple] {
            min-height: 200px;
        }
    </style>
";
    }

    // line 247
    public function block_content($context, array $blocks = array())
    {
        // line 248
        echo "    ";
        echo form_open(site_url("ami/discount_model/save_setting"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => "edit_setting"));
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Discount Setting</h1>

                <div class=\"text-right\">
                    <div class=\"btn-header-toolbar\">
                        <button name=\"btn_submit\" type=\"submit\" class=\"btn btn-success\"><i
                                    class=\"fa fa-fw fa-check\"></i> Save
                        </button>
                        ";
        // line 261
        echo "
                        ";
        // line 262
        echo html_btn(site_url("ami/discount_model/setting"), "<i class=\"fa fa-fw fa-ban\"></i> Cancel</a>", array("class" => "btn-default"));
        echo "
                        ";
        // line 263
        if ($this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "id", array())) {
            // line 264
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, site_url(("ami/discount_model/delete_setting/" . $this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "id", array()))), "html", null, true);
            echo "\"
                               title=\"Delete\"
                               class=\"btn btn-danger\" data-toggle=\"ajaxModal\"><i class=\"fa fa-close\"></i> Delete</a>
                        ";
        }
        // line 268
        echo "                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                ";
        // line 278
        echo form_label("Type", "type", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 280
        echo form_dropdown("type", (isset($context["select_type"]) ? $context["select_type"] : null), $this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "type", array()), "id=\"type\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 284
        echo form_label("Date Range", "range_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 286
        echo form_dropdown("range_id", (isset($context["range"]) ? $context["range"] : null), $this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "range_id", array()), "id=\"range_id\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 290
        echo form_label("Product Cat Web", "cat_web_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 292
        echo form_dropdown("cat_web_id", (isset($context["cat_web"]) ? $context["cat_web"] : null), $this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "cat_web_id", array()), "id=\"cat_web\" class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>
            <div id=\"section-base\">
                <div class=\"product-step\">
                    <div class=\"form-group\">
                        <hr/>
                        ";
        // line 299
        echo form_label("Products", "sku_number", array("class" => "control-label col-sm-3"));
        echo "
                        <div class=\"col-sm-6\">
                            <select name=\"section[0][sku_number][]\" class=\"form-control products\" multiple>
                                ";
        // line 302
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 303
            echo "                                    ";
            if (twig_in_filter($this->getAttribute($context["product"], "sku_number", array()), (isset($context["recent_product"]) ? $context["recent_product"] : null))) {
                // line 304
                echo "                                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
                echo "\"
                                                data-cat-web=\"";
                // line 305
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "products_cat_web_id", array()), "html", null, true);
                echo "\"
                                                selected=\"selected\">";
                // line 306
                echo twig_escape_filter($this->env, (($this->getAttribute($context["product"], "sku_number", array()) . " - ") . $this->getAttribute($context["product"], "sku_name", array())), "html", null, true);
                echo "</option>
                                    ";
            } else {
                // line 308
                echo "                                        <option data-cat-web=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "products_cat_web_id", array()), "html", null, true);
                echo "\"
                                                value=\"";
                // line 309
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "sku_number", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (($this->getAttribute($context["product"], "sku_number", array()) . " - ") . $this->getAttribute($context["product"], "sku_name", array())), "html", null, true);
                echo "</option>
                                    ";
            }
            // line 311
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 312
        echo "                            </select>
                            <div><p><strong>Select multiple by holding CTRL or SHIFT while clicking.</strong></p></div>
                        </div>
                        <div class=\"col-sm-3\">
                            <button class=\"btn btn-default btn-clone-section clone-add\"><i class=\"fa fa-plus\"></i></button>
                            <button class=\"btn btn-default btn-clone-section clone-remove\"><i class=\"fa fa-minus\"></i>
                            </button>
                        </div>
                    </div>
                    <div class=\"base\" data-section=\"0\">
                        ";
        // line 322
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["discount"]) ? $context["discount"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["index"] => $context["tier"]) {
            // line 323
            echo "                            ";
            $context["tier_case"] = (("tier_" . $context["index"]) . "_case");
            // line 324
            echo "                            ";
            $context["tier_discount"] = (("tier_" . $context["index"]) . "_discount");
            // line 325
            echo "                            <div class=\"form-group step\">
                                ";
            // line 326
            echo form_label((("Tier " . $context["index"]) . " Case"), (isset($context["tier_case"]) ? $context["tier_case"] : null), array("class" => "control-label col-sm-3"));
            echo "
                                <div class=\"col-sm-3\">
                                    <input type=\"text\" name=\"section[0][tier][";
            // line 328
            echo twig_escape_filter($this->env, (isset($context["tier_case"]) ? $context["tier_case"] : null), "html", null, true);
            echo "]\"
                                           value=\"";
            // line 329
            echo twig_escape_filter($this->env, $this->getAttribute($context["tier"], "tier_case", array()), "html", null, true);
            echo "\"
                                           class=\"form-control input-case\" min=\"1\"
                                           data-parsley-required=\"true\" data-parsley-type=\"number\"/>
                                </div>
                                ";
            // line 333
            echo form_label("Discount(%)", (isset($context["tier_discount"]) ? $context["tier_discount"] : null), array("class" => "control-label col-sm-1"));
            echo "
                                <div class=\"col-sm-2\">
                                    <input type=\"text\" name=\"section[0][tier][";
            // line 335
            echo twig_escape_filter($this->env, (isset($context["tier_discount"]) ? $context["tier_discount"] : null), "html", null, true);
            echo "]\"
                                           value=\"";
            // line 336
            echo twig_escape_filter($this->env, $this->getAttribute($context["tier"], "tier_discount", array()), "html", null, true);
            echo "\"
                                           class=\"form-control input-discount\" min=\"1\"
                                           data-parsley-required=\"true\" data-parsley-type=\"number\"/>
                                </div>
                                <div class=\"col-sm-3\">
                                    <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i>
                                    </button>
                                    <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                    </button>
                                </div>
                            </div>
                        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 348
            echo "                            <div class=\"form-group step\">
                                ";
            // line 349
            echo form_label("Tier 1 Case", "tier_1_case", array("class" => "control-label col-sm-3"));
            echo "
                                <div class=\"col-sm-3\">
                                    <input type=\"text\" name=\"section[0][tier][tier_1_case]\"
                                           class=\"form-control input-case\" min=\"1\"
                                           data-parsley-required=\"true\" data-parsley-type=\"number\"/>
                                </div>
                                ";
            // line 355
            echo form_label("Discount(%)", "tier_1_discount", array("class" => "control-label col-sm-1"));
            echo "
                                <div class=\"col-sm-2\">
                                    <input type=\"text\" name=\"section[0][tier][tier_1_discount]\"
                                           class=\"form-control input-discount\"
                                           min=\"1\"
                                           data-parsley-required=\"true\" data-parsley-type=\"number\"/>
                                </div>
                                <div class=\"col-sm-3\">
                                    <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i>
                                    </button>
                                    <button class=\"btn btn-default btn-clone clone-remove hide\"><i
                                                class=\"fa fa-minus\"></i>
                                    </button>
                                </div>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['tier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 371
        echo "                        <div class=\"form-group clone-step hide\">
                            ";
        // line 372
        echo form_label("Tier 1 Case", "tier_1_discount", array("class" => "control-label col-sm-3 case-label"));
        echo "
                            <div class=\"col-sm-3 input-case\">
                                ";
        // line 375
        echo "                            </div>
                            ";
        // line 376
        echo form_label("Discount(%)", "tier_1_discount", array("class" => "control-label col-sm-1"));
        echo "
                            <div class=\"col-sm-2 input-discount\">
                                ";
        // line 379
        echo "                            </div>
                            <div class=\"col-sm-3\">
                                <button class=\"btn btn-default btn-clone clone-add\"><i class=\"fa fa-plus\"></i></button>
                                <button class=\"btn btn-default btn-clone clone-remove\"><i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                ";
        // line 389
        $this->loadTemplate("ami/discount_model/clone_item.html.twig", "ami/discount_model/edit_setting.html.twig", 389)->display(array_merge($context, array("products" => (isset($context["products"]) ? $context["products"] : null))));
        // line 390
        echo "            </div>


            ";
        // line 393
        echo form_input(array("name" => "section[0][id]", "value" => $this->getAttribute((isset($context["discount_model"]) ? $context["discount_model"] : null), "id", array()), "type" => "hidden", "id" => "hidden_id", "class" => "hide"));
        echo "

        </div>
    </div>

    ";
        // line 398
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/discount_model/edit_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  584 => 398,  576 => 393,  571 => 390,  569 => 389,  557 => 379,  552 => 376,  549 => 375,  544 => 372,  541 => 371,  519 => 355,  510 => 349,  507 => 348,  490 => 336,  486 => 335,  481 => 333,  474 => 329,  470 => 328,  465 => 326,  462 => 325,  459 => 324,  456 => 323,  451 => 322,  439 => 312,  433 => 311,  426 => 309,  421 => 308,  416 => 306,  412 => 305,  407 => 304,  404 => 303,  400 => 302,  394 => 299,  384 => 292,  379 => 290,  372 => 286,  367 => 284,  360 => 280,  355 => 278,  343 => 268,  335 => 264,  333 => 263,  329 => 262,  326 => 261,  310 => 248,  307 => 247,  296 => 239,  291 => 238,  288 => 237,  171 => 123,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
