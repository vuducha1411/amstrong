<?php

/* ami/potential_customers/edit.html.twig */
class __TwigTemplate_4101b2fbfbf47ef1af95a2271b72d285a87a50df22cd2fc5f2d363c788983c34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("ami/base.html.twig", "ami/potential_customers/edit.html.twig", 1);
        $this->blocks = array(
            'js' => array($this, 'block_js'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ami/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_js($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("js", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, site_url("res/js/plugins/moment/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("res/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        function toTitleCase(str) {
            return str.replace(/\\w\\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        \$(function () {
            ";
        // line 16
        echo "            ";
        // line 17
        echo "            ";
        // line 18
        echo "            ";
        // line 19
        echo "            ";
        // line 20
        echo "            ";
        // line 21
        echo "            ";
        // line 22
        echo "            ";
        // line 23
        echo "            ";
        // line 24
        echo "            ";
        // line 25
        echo "            ";
        // line 26
        echo "            ";
        // line 27
        echo "            ";
        // line 28
        echo "            ";
        // line 29
        echo "            ";
        // line 30
        echo "            ";
        // line 31
        echo "            ";
        // line 32
        echo "            ";
        // line 33
        echo "            ";
        // line 34
        echo "            ";
        // line 35
        echo "            ";
        // line 36
        echo "            ";
        // line 37
        echo "            ";
        // line 38
        echo "            ";
        // line 39
        echo "            ";
        // line 40
        echo "            ";
        // line 41
        echo "            ";
        // line 42
        echo "            ";
        // line 43
        echo "            ";
        // line 44
        echo "            ";
        // line 45
        echo "
            \$('.select-change').on('change', function (e) {
                var \$this = \$(this),
                        name = \$this.attr('name'),
                        val = \$this.find(\":selected\").val(),
                        \$container = \$this.closest('.select-change-container');

                if (val) {
                    \$.ajax({
                        url: \"";
        // line 54
        echo twig_escape_filter($this->env, site_url("ami/customers/country"), "html", null, true);
        echo "\",
                        data: {key: name, value: val},
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {

                            if (!res.field) {
                                return;
                            }

                            var options = '<option value=\"\">-Select-</option>';
                            for (var key in res.value) {
                                options += '<option value=\"' + res['value'][key] + '\">' + res['value'][key] + '</option>';
                            }

                            \$('select[name=' + res.field + ']').find('option').remove().end().append(options);
                        }
                    });
                } else {
                    if (\$container.length) {
                        \$container.nextAll('.select-change-container').each(function () {
                            \$(this).find('.select-change option').remove();
                        });
                    }
                }
            });

            ";
        // line 81
        if ( !$this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
            // line 82
            echo "            \$('.select-change').trigger('change');
            ";
        }
        // line 84
        echo "
            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        weightage = \$this.find('option:selected').data('weightage') || 0,
                        convenience_factor = \$this.find('option:selected').data('convenience_lvl') || 0,
                        ufs_share = \$this.find('option:selected').data('ufs_food_share') || 0;
                \$('#ChannelWeightage, #FoodCostNew').val(weightage);
                ";
        // line 91
        $context["au_nz"] = array(0 => 2, 1 => 9);
        // line 92
        echo "                ";
        $context["id_sa"] = array(0 => 10, 1 => 4);
        // line 93
        echo "                ";
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
            // line 94
            echo "                \$('#convenienceFactor').val(convenience_factor);
                ";
        }
        // line 96
        echo "                ";
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 97
            echo "                \$('.id-ufs-share').val(ufs_share);
                ";
        }
        // line 99
        echo "                \$('input[name=\"ufs_share\"]').val(ufs_share);
            });

            \$('.fetch-otm').on('change', function () {
                var foodOverElement = \$('input[name=\"food_turnover_new\"]'),
                        foodPurchaseElement = \$('input[name=\"food_purchase_value_new\"]'),
                        otmFoodCostElement = \$('input[name=\"ufs_share_food_cost_new\"]'),
                        otmPotentialElement = \$('input[name=\"otm_potential_value_new\"]'),
                        otmNewElement = \$('input[name=\"otm_new\"]'),
                        aFoodCost = \$('#FoodCostNew').val(),
                        aUfsShare = \$('input[name=\"assumption_ufs_share_new\"]').val(),
                        convenience = \$('#convenienceFactor').val();

                if (\$('.convenience_other_country').length > 0) {
                    convenience = \$('.convenience_other_country').val() == 0 ? 0.5 : 1;
                }

                var foodOver,
                        foodPurchase,
                        otmFoodCost,
                        otmNew
                \$.each(\$('.fetch-food'), function () {
                    var \$val = \$(this).val();

                    if (!foodOver) {
                        foodOver = \$val;
                    } else {
                        foodOver = foodOver * \$val;
                    }
                });

//                foodOver = Math.round(foodOver).toFixed(3);
                foodPurchase = foodOver * aFoodCost;
                otmFoodCost = foodPurchase * aUfsShare;
                otmNew = otmFoodCost * convenience;

                foodOverElement.val(foodOver);
                foodPurchaseElement.val(foodPurchase);
                otmFoodCostElement.val(otmFoodCost);
                otmPotentialElement.val(otmNew);

                \$.ajax({
                    url: \"";
        // line 141
        echo twig_escape_filter($this->env, site_url("ami/customers/getOtmBandwidth"), "html", null, true);
        echo "\",
                    data: {otm: otmNew},
                    type: 'POST',
                    success: function (res) {
                        otmNewElement.val(res);
                    }
                });

            });

//            \$('.fetch-otm').trigger('change');

            \$('#SellingInput').on('change input blur', function (e) {
                \$('#BudgetInput').val('');
            });

            \$('#BudgetInput').on('change input blur', function (e) {
                \$('#SellingInput').val('');
            });

            \$('#InputBestTimeCall, .timepicker').datetimepicker({
                format: 'h:m A'
            });

            ";
        // line 165
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 166
            echo "            \$('#subChannelSelect').on('change', function () {
                var \$this = \$(this),
                        optionSelected = \$this.find('option:selected'),
                        country_channels_id = optionSelected.data('country_channel'),
                        global_channels_id = \$('#ChannelSelect').find('option[value=' + country_channels_id + ']').data('global_channels_id');
                \$('#ChannelSelect, #channelHidden').val(country_channels_id);
                \$('#globalChannels, #globalChannelHidden').val(global_channels_id);
                \$('#ChannelSelect').trigger('change');
            });
            ";
        } else {
            // line 176
            echo "            \$('#globalChannels').on('change', function () {
                var \$this = \$(this),
                        global_channel_id = \$this.val(),
                        channel_option = \$('#ChannelSelect option');

                \$.each(channel_option, function () {
                    var data_global_channel_id = \$(this).data('global_channels_id');
                    if (data_global_channel_id == global_channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#ChannelSelect option:first-child').show().prop('selected', true);
                \$('#subChannelSelect option').hide();
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });

            \$('#ChannelSelect').on('change', function () {
                var \$this = \$(this),
                        channel_id = \$this.val(),
                        sub_channel_option = \$('#subChannelSelect option');
                \$.each(sub_channel_option, function () {
                    var country_channel_id = \$(this).data('country_channel');
                    if (country_channel_id == channel_id) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
                \$('#subChannelSelect option:first-child').show().prop('selected', true);
            });
            ";
        }
        // line 209
        echo "        });

        \$(document).ready(function () {
            var approved = '";
        // line 212
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "approved", array()), "html", null, true);
        echo "',
                    active = '";
        // line 213
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()), "html", null, true);
        echo "',
                    optionCloseDown = \$('#approved option[value=\"-2\"]'),
                    optionReject = \$('#approved option[value=\"-1\"]'),
                    optionApproved = \$('#approved option[value=\"1\"]');

            if (active == 1) {
                if (approved == -2 || approved == -1) {
                    \$('#approved').val('');
                }
                optionApproved.show();
                optionCloseDown.hide();
                optionReject.hide();
            } else {
                if (approved == 1) {
                    \$('#approved').val('');
                }
                optionApproved.hide();
                optionCloseDown.show();
                optionReject.show();
            }

            \$('input[name=\"active\"]').on('change', function () {
                var \$val = \$(this).val();
                \$('#approved').val('');
                if (\$val == 1) {
                    optionApproved.show();
                    optionCloseDown.hide();
                    optionReject.hide();
                } else {
                    optionApproved.hide();
                    optionCloseDown.show();
                    optionReject.show();
                }
            });

            // init sub channel
            ";
        // line 249
        if (!twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 250
            echo "            \$.each(\$('#subChannelSelect option'), function () {
                var country_channel_id = \$(this).data('country_channel');
                if (country_channel_id == \$('#ChannelSelect').val()) {
                    \$(this).show();
                } else {
                    \$(this).hide();
                }
            });
            ";
        }
        // line 259
        echo "
            ";
        // line 260
        if (((isset($context["otm_ufs_share_config"]) ? $context["otm_ufs_share_config"] : null) == 1)) {
            // line 261
            echo "            \$('input[name=\"assumption_ufs_share_new\"]').val(\$('#ChannelSelect').find('option:selected').data('ufs_food_share'));
            ";
        }
        // line 263
        echo "        });
        function checkidCustomer(){
            \$('.hieuerror').remove();
            var id1 = \$('input[name=\"armstrong_1_customers_id\"]').val();
            if(id1 != ''){
                \$.ajax({
                    url: \"";
        // line 269
        echo twig_escape_filter($this->env, site_url(("ami/potential_customers/checkid/" . $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "id", array()))), "html", null, true);
        echo "\",
                    data: 'armstrong_1_customers_id='+id1,
                    type: 'POST',
                    success: function (html) {
                        if(html == 'no')
                        {
                            \$('.hieuerror').remove();
                            \$('input[name=\"armstrong_1_customers_id\"]').after('<span style=\"color: red;\" class=\"error hieuerror\" >This ID has been used</span>')
                            \$('.btn.btn-success').addClass('disabled');
                        }
                        else
                        {
                            \$('.btn.btn-success').removeClass('disabled');
                        }
                    }
                });
            }
        }
        checkidCustomer();
        \$('input[name=\"armstrong_1_customers_id\"]').keyup(function(){
            checkidCustomer();
        });
        \$(function () {
            \$('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD hh:mm:ss'
            });
        });
    </script>
";
    }

    // line 299
    public function block_css($context, array $blocks = array())
    {
        // line 300
        echo "    ";
        $this->displayParentBlock("css", $context, $blocks);
        echo "
    <link rel=\"stylesheet\"
          href=\"";
        // line 302
        echo twig_escape_filter($this->env, site_url("res/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">

";
    }

    // line 306
    public function block_content($context, array $blocks = array())
    {
        // line 307
        echo "
    ";
        // line 308
        echo form_open(site_url("ami/potential_customers/save"), array("class" => "form-horizontal", "data-parsley-validate" => "true"), array("id" => $this->getAttribute(        // line 309
(isset($context["customer"]) ? $context["customer"] : null), "id", array())));
        // line 310
        echo "

    <div class=\"row sticky sticky-h1 bg-white\">
        <div class=\"col-lg-12\">
            <div class=\"page-header nm\">
                <h1 class=\"pull-left\">Potential Customers</h1>

                <div class=\"text-right\">
                    ";
        // line 318
        $this->loadTemplate("ami/components/form_btn.html.twig", "ami/potential_customers/edit.html.twig", 318)->display(array_merge($context, array("url" => "ami/potential_customers", "id" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "id", array()), "permission" => "customer")));
        // line 319
        echo "                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>

    <div class=\"row m-t-md\">
        <div class=\"col-lg-12\">

            <div class=\"form-group\">
                ";
        // line 329
        echo form_label("Armstrong 1 Customer Id", "armstrong_1_customers_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 331
        echo form_input(array("name" => "armstrong_1_customers_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_1_customers_id", array()), "class" => "form-control\" data-parsley-required=\"true\""));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 336
        echo form_label("Armstrong 2 Salespersons Id", "armstrong_2_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 338
        echo form_dropdown("armstrong_2_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_salespersons_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 343
        echo form_label("Armstrong 2 Secondary Salespersons Id", "armstrong_2_secondary_salespersons_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 345
        echo form_dropdown("armstrong_2_secondary_salespersons_id", (isset($context["salespersons"]) ? $context["salespersons"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_secondary_salespersons_id", array()), "class=\"form-control\"");
        echo "
                </div>
            </div>

            ";
        // line 350
        echo "                ";
        // line 351
        echo "                ";
        // line 352
        echo "                    ";
        // line 353
        echo "                ";
        // line 354
        echo "            ";
        // line 355
        echo "
            <div class=\"form-group\">
                ";
        // line 357
        echo form_label("Name", "armstrong_2_customers_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 359
        echo form_input(array("name" => "armstrong_2_customers_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_name", array()), "class" => "form-control", "data-parsley-required" => "true", "data-parsley-maxlength" => 100));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 364
        echo form_label("Active", "active", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6 no-parsley\">
                    <label class=\"radio-inline\">";
        // line 366
        echo form_radio("active", 1, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 1));
        echo " Active</label>&nbsp;
                    <label class=\"radio-inline\">";
        // line 367
        echo form_radio("active", 0, ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "active", array()) == 0));
        echo " Inactive</label>&nbsp;
                </div>
            </div>

            ";
        // line 371
        if ((twig_length_filter($this->env, (isset($context["groups"]) ? $context["groups"] : null)) > 1)) {
            // line 372
            echo "                <div class=\"form-group\">
                    ";
            // line 373
            echo form_label("Group", "groups_id", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 375
            echo form_dropdown("groups_id", (isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "groups_id", array()), "class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    </div>
                </div>
            ";
        }
        // line 379
        echo "
            <div class=\"form-group\">
                ";
        // line 381
        echo form_label("SSD Name", "sap_cust_name", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 383
        echo form_input(array("name" => "sap_cust_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 388
        echo form_label("SSD Name 2", "sap_cust_name_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 390
        echo form_input(array("name" => "sap_cust_name_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_cust_name_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 395
        echo form_label("SSD ID", "ssd_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 397
        echo form_input(array("name" => "ssd_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 402
        echo form_label("SSD ID 2", "ssd_id_2", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 404
        echo form_input(array("name" => "ssd_id_2", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ssd_id_2", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 409
        echo form_label("Sap ID", "sap_id", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 411
        echo form_input(array("name" => "sap_id", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sap_id", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 416
        echo form_label("Street Address", "street_address", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 418
        echo form_input(array("name" => "street_address", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "street_address", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            ";
        // line 422
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countryHierachy"]) ? $context["countryHierachy"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["countryField"]) {
            // line 423
            echo "                <div class=\"form-group select-change-container\">
                    ";
            // line 424
            echo form_label(twig_capitalize_string_filter($this->env, $context["countryField"]), $context["countryField"], array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 426
            if ($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "armstrong_2_customers_id", array())) {
                // line 427
                echo "                            ";
                echo form_dropdown($context["countryField"], $this->getAttribute((isset($context["countryValues"]) ? $context["countryValues"] : null), $context["countryField"], array(), "array"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            } else {
                // line 429
                echo "                            ";
                $context["_values"] = ((($context["key"] == "root")) ? ((isset($context["rootValues"]) ? $context["rootValues"] : null)) : (array()));
                // line 430
                echo "                            ";
                echo form_dropdown($context["countryField"], (isset($context["_values"]) ? $context["_values"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), $context["countryField"], array(), "array"), "class=\"form-control select-change\"");
                echo "
                        ";
            }
            // line 432
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['countryField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 435
        echo "
            <div class=\"form-group\">
                ";
        // line 437
        echo form_label("Postal Code", "postal_code", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 439
        echo form_input(array("name" => "postal_code", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "postal_code", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 444
        echo form_label("Phone", "phone", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 446
        echo form_input(array("name" => "phone", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 451
        echo form_label("Phone 2", "phone_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 453
        echo form_input(array("name" => "phone_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "phone_two", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 458
        echo form_label("Fax", "fax", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 460
        echo form_input(array("name" => "fax", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "fax", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 465
        echo form_label("Email", "email", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 467
        echo form_input(array("name" => "email", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 472
        echo form_label("Email 2", "email_two", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 474
        echo form_input(array("name" => "email_two", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "email_two", array()), "class" => "form-control", "data-parsley-type" => "email"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 479
        echo form_label("Territory", "territory", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 481
        echo form_input(array("name" => "territory", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "territory", array()), "class" => "form-control"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 486
        echo form_label("Acct Opened", "acct_opened", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 488
        echo form_input(array("name" => "acct_opened", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "acct_opened", array()), "class" => "form-control datepicker"));
        echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 492
        echo form_label("Web Url", "web_url", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 494
        echo form_input(array("name" => "web_url", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "web_url", array()), "class" => "form-control", "data-parsley-type" => "url"));
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
        // line 499
        echo form_label("Global Channel", "global_channels", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 501
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 502
            echo "                        ";
            echo form_dropdown("hidden_global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\" disabled=\"disabled\"");
            echo "
                    ";
        } else {
            // line 504
            echo "                        ";
            echo form_dropdown("global_channels_id", (isset($context["global_channels"]) ? $context["global_channels"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "id=\"globalChannels\" class=\"form-control\" data-parsley-required=\"true\"");
            echo "
                    ";
        }
        // line 506
        echo "                </div>
            </div>
            ";
        // line 508
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
            // line 509
            echo "                <input id=\"globalChannelHidden\" type=\"hidden\" name=\"global_channels_id\"
                       value=\"";
            // line 510
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "global_channels_id", array()), "html", null, true);
            echo "\">
            ";
        }
        // line 512
        echo "
            ";
        // line 513
        if ((isset($context["country_channels"]) ? $context["country_channels"] : null)) {
            // line 514
            echo "                <div class=\"form-group\">
                    ";
            // line 515
            echo form_label("Channel", "channel", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 518
            echo "                        <select name=\"";
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disable_channel") : ("channel"));
            echo "\"
                                class=\"form-control fetch-otm ";
            // line 519
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 2, 1 => 9))) ? ("channellv") : (""));
            echo "\"
                                id=\"ChannelSelect\"
                                data-parsley-required=\"true\" ";
            // line 521
            echo ((twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) ? ("disabled=\"disabled\"") : (""));
            echo ">
                            <option value=\"\" data-weightage=\"\">- Select -</option>
                            ";
            // line 523
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["country_channels"]) ? $context["country_channels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["country_channel"]) {
                // line 524
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute($context["country_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()))) ? ("selected=\"selected\"") : (""));
                echo "
                                        data-global_channels_id=\"";
                // line 525
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "global_channels_id", array()), "html", null, true);
                echo "\"
                                        data-weightage=\"";
                // line 526
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "channel_weightage", array()), "html", null, true);
                echo "\"
                                        data-convenience_lvl=\"";
                // line 527
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "convenience_lvl", array()), "html", null, true);
                echo "\"
                                        data-ufs_food_share=\"";
                // line 528
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "ufs_food_share", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["country_channel"], "name", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country_channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 530
            echo "                        </select>
                    </div>
                </div>
                ";
            // line 533
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 534
                echo "                    <input id=\"channelHidden\" type=\"hidden\" name=\"channel\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel", array()), "html", null, true);
                echo "\">
                ";
            }
            // line 536
            echo "            ";
        }
        // line 537
        echo "
            ";
        // line 538
        if ((twig_length_filter($this->env, (isset($context["sub_channels"]) ? $context["sub_channels"] : null)) > 1)) {
            // line 539
            echo "                <div class=\"form-group\">
                    ";
            // line 540
            echo form_label("Sub Channel", "sub_channel", array("class" => "control-label col-sm-3"));
            echo "
                    ";
            // line 541
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 6))) {
                // line 542
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\" data-parsley-required=\"true\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 547
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 548
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 549
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 551
                echo "                            </select>
                        </div>
                    ";
            } else {
                // line 554
                echo "                        <div class=\"col-sm-6\">
                            <select name=\"sub_channel\"
                                    class=\"form-control\"
                                    id=\"subChannelSelect\">
                                <option value=\"\">- Select -</option>
                                ";
                // line 559
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["sub_channels"]) ? $context["sub_channels"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_channel"]) {
                    // line 560
                    echo "                                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "id", array()), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute($context["sub_channel"], "id", array()) == $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "sub_channel", array()))) ? ("selected=\"selected\"") : (""));
                    echo "
                                            data-country_channel=\"";
                    // line 561
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "country_channels_id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_channel"], "name", array()), "html", null, true);
                    echo "</option>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 563
                echo "                            </select>
                        </div>
                    ";
            }
            // line 566
            echo "                </div>
            ";
        }
        // line 568
        echo "
            <div class=\"form-group\">
                ";
        // line 570
        echo form_label("Channel Weightage", "channel_weightage", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 572
        echo form_input(array("name" => "channel_weightage", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_weightage", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "ChannelWeightage"));
        echo "
                </div>
            </div>

            ";
        // line 576
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 10))) {
            // line 577
            echo "                <div class=\"form-group\">
                    ";
            // line 578
            echo form_label("Real Otm", "real_otm", array("class" => "control-label col-sm-3"));
            echo "
                    <div class=\"col-sm-6\">
                        ";
            // line 580
            echo form_input(array("name" => "real_otm", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "real_otm", array()), "class" => "form-control", "readonly" => "readonly"));
            echo "
                    </div>
                </div>
            ";
        }
        // line 584
        echo "
            <div class=\"form-group\">
                ";
        // line 586
        echo form_label("Otm", "otm", array("class" => "control-label col-sm-3"));
        echo "
                <div class=\"col-sm-6\">
                    ";
        // line 588
        if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), array(0 => 3, 1 => 5))) {
            // line 589
            echo "                        ";
            echo form_dropdown("otm", array("A" => "A", "B" => "B", "C" => "C", "D" => "D", "N/A" => "N/A"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm", array()), "class=\"form-control\"");
            echo "
                    ";
        } else {
            // line 591
            echo "                        ";
            echo form_input(array("name" => "otm", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm", array()), "class" => "form-control", "readonly" => "readonly"));
            echo "
                    ";
        }
        // line 593
        echo "                </div>
            </div>


        </div>

        <div class=\"form-group\">
            ";
        // line 600
        echo form_label("Key Manager", "key_acct", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 602
        echo form_input(array("name" => "key_acct", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "key_acct", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        ";
        // line 606
        if ((twig_length_filter($this->env, (isset($context["business_types"]) ? $context["business_types"] : null)) > 1)) {
            // line 607
            echo "            <div class=\"form-group\">
                ";
            // line 608
            echo form_label("Business Type", "business_type", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 610
            echo form_dropdown("business_type", (isset($context["business_types"]) ? $context["business_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "business_type", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 614
        echo "
        ";
        // line 615
        if ((twig_length_filter($this->env, (isset($context["suppliers"]) ? $context["suppliers"] : null)) > 1)) {
            // line 616
            echo "            <div class=\"form-group\">
                ";
            // line 617
            echo form_label("Primary Supplier", "primary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 619
            echo form_dropdown("primary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "primary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>

            <div class=\"form-group\">
                ";
            // line 624
            echo form_label("Secondary Supplier", "secondary_supplier", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 626
            echo form_dropdown("secondary_supplier", (isset($context["suppliers"]) ? $context["suppliers"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "secondary_supplier", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 630
        echo "
        <div class=\"form-group\">
            ";
        // line 632
        echo form_label("Trading Days Per Year", "trading_days_per_yr", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 634
        echo form_input(array("name" => "trading_days_per_yr", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "trading_days_per_yr", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-range" => "[1,366]"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 639
        echo form_label("Trading Weeks Per Year", "trading_wks_per_yr", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 641
        echo form_input(array("name" => "trading_wks_per_yr", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "trading_wks_per_yr", array()), "class" => "form-control", "data-parsley-type" => "integer", "data-parsley-range" => "[1,53]"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 646
        echo form_label("Turnover", "turnover", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 648
        echo form_input(array("name" => "turnover", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "turnover", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 653
        echo form_label("Capacity", "capacity", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 655
        echo form_input(array("name" => "capacity", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "capacity", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 660
        echo form_label("No Of Outlets", "no_of_outlets", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 662
        echo form_input(array("name" => "no_of_outlets", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "no_of_outlets", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 667
        echo form_label("Meals Per Day", "meals_per_day", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 669
        echo form_input(array("name" => "meals_per_day", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "meals_per_day", array()), "class" => "form-control", "data-parsley-type" => "integer"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 674
        echo form_label("Selling Price", "selling_price", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 676
        echo form_input(array("name" => "selling_price", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "selling_price", array()), "class" => "form-control", "data-parsley-type" => "number", "id" => "SellingInput"));
        // line 679
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 684
        echo form_label("Budget Price", "budget_price", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 686
        echo form_input(array("name" => "budget_price", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "budget_price", array()), "class" => "form-control", "data-parsley-type" => "number", "id" => "BudgetInput"));
        // line 689
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 694
        echo form_label("Food Cost", "food_cost", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 696
        echo form_input(array("name" => "food_cost", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_cost", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 701
        echo form_label("Best Time To Call", "best_time_to_call", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 703
        echo form_input(array("name" => "best_time_to_call", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "best_time_to_call", array()), "class" => "form-control", "id" => "InputBestTimeCall"));
        // line 706
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 711
        echo form_label("Notes", "notes", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 713
        echo form_input(array("name" => "notes", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "notes", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 718
        echo form_label("Latitude", "latitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 720
        echo form_input(array("name" => "latitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "latitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 725
        echo form_label("Longitude", "longitude", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 727
        echo form_input(array("name" => "longitude", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "longitude", array()), "class" => "form-control", "data-parsley-type" => "number"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 732
        echo form_label("Display Name", "display_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 734
        echo form_input(array("name" => "display_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "display_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 739
        echo form_label("Registered Name", "registered_name", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 741
        echo form_input(array("name" => "registered_name", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "registered_name", array()), "class" => "form-control"));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 746
        echo form_label("Opening Hours", "opening_hours", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-3\">
                ";
        // line 748
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_from", array()), "class" => "form-control timepicker"));
        // line 750
        echo "
            </div>
            <div class=\"col-sm-3\">
                ";
        // line 753
        echo form_input(array("name" => "opening_hours[]", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "opening_hours_to", array()), "class" => "form-control timepicker"));
        // line 755
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 760
        echo form_label("Ufs Share", "ufs_share", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                ";
        // line 762
        echo form_input(array("name" => "ufs_share", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control", "data-parsley-type" => "number", "readonly" => "readonly"));
        echo "
            </div>
        </div>

        ";
        // line 766
        if ((twig_length_filter($this->env, (isset($context["customers_types"]) ? $context["customers_types"] : null)) > 1)) {
            // line 767
            echo "            <div class=\"form-group\">
                ";
            // line 768
            echo form_label("Customers Types", "customers_types_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 770
            echo form_dropdown("customers_types_id", (isset($context["customers_types"]) ? $context["customers_types"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "customers_types_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 774
        echo "        <div class=\"form-group\">
            ";
        // line 775
        echo form_label("Buy Frequency", "buy_frequency", array("class" => "control-label col-sm-3"));
        echo "
            <div class=\"col-sm-6\">
                <select name=\"buy_frequency\" class=\"form-control\" id=\"ChannelSelect\">
                    <option value=\"0\" data-weightage=\"\">- Select -</option>
                    <option value=\"1\" ";
        // line 779
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 1)) ? ("selected=\"selected\"") : (""));
        echo " >Once a month
                    </option>
                    <option value=\"2\" ";
        // line 781
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 2)) ? ("selected=\"selected\"") : (""));
        echo " >Twice a month
                    </option>
                    <option value=\"3\" ";
        // line 783
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 3)) ? ("selected=\"selected\"") : (""));
        echo " >Three times a
                        month
                    </option>
                    <option value=\"4\" ";
        // line 786
        echo ((($this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "buy_frequency", array()) == 4)) ? ("selected=\"selected\"") : (""));
        echo " >Once a week
                    </option>
                </select>
            </div>
        </div>

        ";
        // line 792
        if ((twig_length_filter($this->env, (isset($context["channel_groups"]) ? $context["channel_groups"] : null)) > 1)) {
            // line 793
            echo "            <div class=\"form-group\">
                ";
            // line 794
            echo form_label("Channel Groups", "channel_groups_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 796
            echo form_dropdown("channel_groups_id", (isset($context["channel_groups"]) ? $context["channel_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "channel_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 800
        echo "
        ";
        // line 801
        if ((twig_length_filter($this->env, (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null)) > 1)) {
            // line 802
            echo "            <div class=\"form-group\">
                ";
            // line 803
            echo form_label("Cuisine Groups", "cuisine_groups_id", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 805
            echo form_dropdown("cuisine_groups_id", (isset($context["cuisine_groups"]) ? $context["cuisine_groups"] : null), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "cuisine_groups_id", array()), "class=\"form-control\"");
            echo "
                </div>
            </div>
        ";
        }
        // line 809
        echo "        ";
        // line 810
        echo "        ";
        if (($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()) != 3)) {
            // line 811
            echo "            <div class=\"form-group\">
                ";
            // line 812
            echo form_label("Meals / Days", "meals_per_day_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 814
            echo form_input(array("name" => "meals_per_day_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "meals_per_day_new", array()), "class" => "form-control fetch-food fetch-otm", "data-parsley-type" => "number"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 818
            echo form_label("Avarage Price / Meal", "selling_price_per_meal_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 820
            echo form_input(array("name" => "selling_price_per_meal_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "selling_price_per_meal_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 824
            echo form_label("Weeks / Year", "weeks_per_year_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 826
            echo form_input(array("name" => "weeks_per_year_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "weeks_per_year_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,52]"));
            echo "
                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 830
            echo form_label("Days / Week", "days_per_week_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 832
            echo form_input(array("name" => "days_per_week_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "days_per_week_new", array()), "class" => "form-control fetch-otm fetch-food", "data-parsley-type" => "number", "data-parsley-range" => "[0,7]"));
            echo "
                </div>
            </div>
            ";
            // line 835
            echo form_input(array("name" => "food_turnover_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_turnover_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 837
            echo form_label("Assumption: Food Cost %", "assumption_food_cost_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 839
            echo form_input(array("name" => "assumption_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "assumption_food_cost_new", array()), "class" => "form-control fetch-otm fetch-food-purchase", "data-parsley-type" => "number", "readonly" => "readonly", "id" => "FoodCostNew"));
            echo "
                </div>
            </div>
            ";
            // line 842
            echo form_input(array("name" => "food_purchase_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "food_purchase_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 844
            echo form_label("Assumption: potential UFS Share of Food Cost %", "assumption_ufs_share_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 846
            echo form_input(array("name" => "assumption_ufs_share_new", "value" => $this->getAttribute((isset($context["country"]) ? $context["country"] : null), "ufs_share", array()), "class" => "form-control id-ufs-share", "data-parsley-type" => "number", "readonly" => "readonly"));
            echo "
                </div>
            </div>
            ";
            // line 849
            echo form_input(array("name" => "ufs_share_food_cost_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "ufs_share_food_cost_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            ";
            // line 850
            echo form_input(array("name" => "otm_potential_value_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_potential_value_new", array()), "class" => "form-control", "data-parsley-type" => "number", "type" => "hidden"));
            echo "
            <div class=\"form-group\">
                ";
            // line 852
            echo form_label("Convenience Factor", "convenience_factor_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 854
            $context["au_nz"] = array(0 => 2, 1 => 9);
            // line 855
            echo "                    ";
            $context["mer"] = array(0 => 12, 1 => 13, 2 => 14, 3 => 15, 4 => 16, 5 => 17);
            // line 856
            echo "                    ";
            if (twig_in_filter($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["au_nz"]) ? $context["au_nz"] : null))) {
                // line 857
                echo "                        ";
                echo form_input(array("name" => "convenience_factor_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class" => "form-control fetch-otm convenience_sp_country", "id" => "convenienceFactor", "data-parsley-type" => "number", "readonly" => "readonly"));
                echo "
                    ";
            } elseif (twig_in_filter($this->getAttribute(            // line 858
(isset($context["session"]) ? $context["session"] : null), "country_id", array()), (isset($context["mer"]) ? $context["mer"] : null))) {
                // line 859
                echo "                        ";
                echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1", "1.5" => "1.5"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm\" id=\"convenienceFactor\"");
                echo "
                    ";
            } else {
                // line 861
                echo "                        ";
                echo form_dropdown("convenience_factor_new", array("0.5" => "0.5", "1" => "1"), $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "convenience_factor_new", array()), "class=\"form-control fetch-otm convenience_other_country\" id=\"convenienceFactor\"");
                echo "
                    ";
            }
            // line 863
            echo "                </div>
            </div>
            <div class=\"form-group\">
                ";
            // line 866
            echo form_label("OTM New", "otm_new", array("class" => "control-label col-sm-3"));
            echo "
                <div class=\"col-sm-6\">
                    ";
            // line 868
            echo form_input(array("name" => "otm_new", "value" => $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "otm_new", array()), "class" => "form-control", "readonly" => "readonly", "id" => "OTMNew"));
            echo "
                </div>
            </div>
        ";
        }
        // line 872
        echo "        ";
        // line 880
        echo "

        ";
        // line 895
        echo "    </div>
    </div>

    ";
        // line 898
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ami/potential_customers/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1604 => 898,  1599 => 895,  1595 => 880,  1593 => 872,  1586 => 868,  1581 => 866,  1576 => 863,  1570 => 861,  1564 => 859,  1562 => 858,  1557 => 857,  1554 => 856,  1551 => 855,  1549 => 854,  1544 => 852,  1539 => 850,  1535 => 849,  1529 => 846,  1524 => 844,  1519 => 842,  1513 => 839,  1508 => 837,  1503 => 835,  1497 => 832,  1492 => 830,  1485 => 826,  1480 => 824,  1473 => 820,  1468 => 818,  1461 => 814,  1456 => 812,  1453 => 811,  1450 => 810,  1448 => 809,  1441 => 805,  1436 => 803,  1433 => 802,  1431 => 801,  1428 => 800,  1421 => 796,  1416 => 794,  1413 => 793,  1411 => 792,  1402 => 786,  1396 => 783,  1391 => 781,  1386 => 779,  1379 => 775,  1376 => 774,  1369 => 770,  1364 => 768,  1361 => 767,  1359 => 766,  1352 => 762,  1347 => 760,  1340 => 755,  1338 => 753,  1333 => 750,  1331 => 748,  1326 => 746,  1318 => 741,  1313 => 739,  1305 => 734,  1300 => 732,  1292 => 727,  1287 => 725,  1279 => 720,  1274 => 718,  1266 => 713,  1261 => 711,  1254 => 706,  1252 => 703,  1247 => 701,  1239 => 696,  1234 => 694,  1227 => 689,  1225 => 686,  1220 => 684,  1213 => 679,  1211 => 676,  1206 => 674,  1198 => 669,  1193 => 667,  1185 => 662,  1180 => 660,  1172 => 655,  1167 => 653,  1159 => 648,  1154 => 646,  1146 => 641,  1141 => 639,  1133 => 634,  1128 => 632,  1124 => 630,  1117 => 626,  1112 => 624,  1104 => 619,  1099 => 617,  1096 => 616,  1094 => 615,  1091 => 614,  1084 => 610,  1079 => 608,  1076 => 607,  1074 => 606,  1067 => 602,  1062 => 600,  1053 => 593,  1047 => 591,  1041 => 589,  1039 => 588,  1034 => 586,  1030 => 584,  1023 => 580,  1018 => 578,  1015 => 577,  1013 => 576,  1006 => 572,  1001 => 570,  997 => 568,  993 => 566,  988 => 563,  978 => 561,  971 => 560,  967 => 559,  960 => 554,  955 => 551,  945 => 549,  938 => 548,  934 => 547,  927 => 542,  925 => 541,  921 => 540,  918 => 539,  916 => 538,  913 => 537,  910 => 536,  904 => 534,  902 => 533,  897 => 530,  887 => 528,  883 => 527,  879 => 526,  875 => 525,  868 => 524,  864 => 523,  859 => 521,  854 => 519,  849 => 518,  844 => 515,  841 => 514,  839 => 513,  836 => 512,  831 => 510,  828 => 509,  826 => 508,  822 => 506,  816 => 504,  810 => 502,  808 => 501,  803 => 499,  795 => 494,  790 => 492,  783 => 488,  778 => 486,  770 => 481,  765 => 479,  757 => 474,  752 => 472,  744 => 467,  739 => 465,  731 => 460,  726 => 458,  718 => 453,  713 => 451,  705 => 446,  700 => 444,  692 => 439,  687 => 437,  683 => 435,  675 => 432,  669 => 430,  666 => 429,  660 => 427,  658 => 426,  653 => 424,  650 => 423,  646 => 422,  639 => 418,  634 => 416,  626 => 411,  621 => 409,  613 => 404,  608 => 402,  600 => 397,  595 => 395,  587 => 390,  582 => 388,  574 => 383,  569 => 381,  565 => 379,  558 => 375,  553 => 373,  550 => 372,  548 => 371,  541 => 367,  537 => 366,  532 => 364,  524 => 359,  519 => 357,  515 => 355,  513 => 354,  511 => 353,  509 => 352,  507 => 351,  505 => 350,  498 => 345,  493 => 343,  485 => 338,  480 => 336,  472 => 331,  467 => 329,  455 => 319,  453 => 318,  443 => 310,  441 => 309,  440 => 308,  437 => 307,  434 => 306,  427 => 302,  421 => 300,  418 => 299,  385 => 269,  377 => 263,  373 => 261,  371 => 260,  368 => 259,  357 => 250,  355 => 249,  316 => 213,  312 => 212,  307 => 209,  272 => 176,  260 => 166,  258 => 165,  231 => 141,  187 => 99,  183 => 97,  180 => 96,  176 => 94,  173 => 93,  170 => 92,  168 => 91,  159 => 84,  155 => 82,  153 => 81,  123 => 54,  112 => 45,  110 => 44,  108 => 43,  106 => 42,  104 => 41,  102 => 40,  100 => 39,  98 => 38,  96 => 37,  94 => 36,  92 => 35,  90 => 34,  88 => 33,  86 => 32,  84 => 31,  82 => 30,  80 => 29,  78 => 28,  76 => 27,  74 => 26,  72 => 25,  70 => 24,  68 => 23,  66 => 22,  64 => 21,  62 => 20,  60 => 19,  58 => 18,  56 => 17,  54 => 16,  42 => 6,  38 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }
}
