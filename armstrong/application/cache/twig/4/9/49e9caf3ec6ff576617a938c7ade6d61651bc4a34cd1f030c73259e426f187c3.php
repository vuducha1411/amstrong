<?php

/* ami/reports/custom/data_customers.twig.html */
class __TwigTemplate_49e9caf3ec6ff576617a938c7ade6d61651bc4a34cd1f030c73259e426f187c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("ami/reports/custom/filter.twig.html", "ami/reports/custom/data_customers.twig.html", 1)->display(array_merge($context, array("channel" => 0, "otm" => 0, "leaderAndPerson" => 0)));
        // line 6
        echo "
<div class=\"form-group\">
\t";
        // line 8
        echo form_label("Armstrong 2 salespersons id", "salesperson", array("class" => "control-label col-sm-3"));
        echo "
\t<div class=\"col-sm-6\">
\t\t<select name=\"armstrong_2_salespersons_id[]\" class=\"form-control SalespersonsSelect\" id=\"armstrong_2_salespersons_id\" multiple=\"multiple\" style=\"min-height: 300px;\">
\t\t\t<option value=\"\" selected=\"selected\" data-manager=\"\">ALL</option>
\t\t\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["custom"]) ? $context["custom"] : null), "salespersons", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sp"]) {
            // line 13
            echo "\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo "\" data-manager=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "salespersons_manager_id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "armstrong_2_salespersons_id", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sp"], "last_name", array()), "html", null, true);
            echo "</option>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t\t\t<option value=\"unassigned\" data-manager=\"\">Unassigned</option>
\t\t</select>
\t</div>
</div>

<div class=\"form-group\">
    ";
        // line 21
        echo form_label("Customer Status", "c_status", array("class" => "control-label col-sm-3"));
        echo "
    <div class=\"col-sm-6\">
        <select name=\"c_status\" class=\"form-control\" id=\"customer-status\">
        \t<option value=\"\" selected=\"selected\">ALL</option>
        \t<option value=\"1\">Active</option>
        \t<option value=\"0\">Inactive</option>
        \t<option value=\"2\">Potential Customers</option>
        </select>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ami/reports/custom/data_customers.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 21,  53 => 15,  36 => 13,  32 => 12,  25 => 8,  21 => 6,  19 => 1,);
    }
}
