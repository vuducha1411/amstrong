
jQuery(function($){

    //PLUPLOAD_IMAGE START

    PLUPLOADER_IMAGE = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_image_pickfiles',
        container: 'uploader_image',
        max_file_size : '1024mb',
        //drop_element : "uploader",
        url : base_url() + 'ami/media/plUpload' + '?class=image&entry_type=' + ENTRY_TYPE,
        resize : {width : 1000, height : 9000, quality : 100},
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: false,
        filters : [
            {title : "Image files", extensions : "jpg,jpeg,gif,png"}
        ]
    });

    PLUPLOADER_IMAGE.bind('Init', function(up, params){
        // $('#uploader_image .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    $('#uploadfiles').on('click', function(e){
        //PLUPLOADER_IMAGE.start(); //UPLOADING SHOULD START AFTER SUCCESSFULLY SUBMITTING THE FORM
        e.preventDefault();
    });

    PLUPLOADER_IMAGE.init();

    PLUPLOADER_IMAGE.bind('FilesAdded', function(up, files){
        $('#uploader_image .filelist .alert-info button.close').trigger('click'); //limit uploading to 1
        $('#uploader_image .uploaded').remove();
        $('#uploadfiles').show().removeClass('hide');
        $('#uploader_image .upload-actions').hide();
        $.each(files, function(i, file){
            $('#uploader_image .filelist').append(
                '<div id="' + file.id + '" class="alert alert-info">' +
                '<span class="filename">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b> <button type="button" class="close cancelUpload">&times;</button>' +
            '<div class="progress progress-striped"><div class="bar" style="width: 1%;"></div></div></div>');
            //queue_count++;
            $('#uploader_image .filelist').on('click', '#' + file.id + ' button.cancelUpload', function(){
                //queue_count--;
                PLUPLOADER_IMAGE.removeFile(file);
                $('#' + file.id).remove();
                $('#uploader_image .upload-actions').show();
            });
        });
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });

    PLUPLOADER_IMAGE.bind('UploadProgress', function(up, file){
        $('#' + file.id + ' b').html(file.percent + '%');
        $('#' + file.id + ' .progress .bar').animate({width: file.percent + '%'}, 100, 'linear');
        if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
            $('#' + file.id + ' .progress').addClass('active');
            $('#' + file.id + ' button.cancelUpload').hide();
        }
    });

    PLUPLOADER_IMAGE.bind('Error', function(up, err){
    $('#uploader_image .filelist').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        'Error: ' + err.code + ', Message: ' + err.message +
            (err.file ? ', File: ' + err.file.name : '') +
            "</div>"
        );
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_IMAGE.bind('FileUploaded', function(up, file, info){
        var response = JSON.parse(info.response);
        $('#' + file.id + ' b').html("100%");
        $('#' + file.id + ' .progress .bar').animate({width: '100%'}, 100,  'linear');
        $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').addClass('progress-success');
        $('#' + file.id + ' button.cancelUpload').remove();
        var post_id = (typeof $('#uploader_image .filelist').attr('data-attr-id') !== 'undefined') ? $('#uploader_image .filelist').attr('data-attr-id') : null;

        var response = JSON.parse(info.response);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: base_url() + 'media/ref',
            data: {id: last_post_id, media : response.id, entry_type: ENTRY_TYPE},
            success: function(result){
                //queue_count--;
            }
        });
    });

    //PLUPLOAD_IMAGE END

    //PLUPLOAD_BROCHURE START

    PLUPLOADER_BROCHURE = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_brochure_pickfiles',
        container: 'uploader_brochure',
        max_file_size : '1024mb',
        //drop_element : "uploader",
        url : base_url() + 'amiproc/plUpload' + '?class=brochure&entry_type=' + ENTRY_TYPE,
        resize : {},
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: true,
        filters : [
            {title : "PDF Files", extensions : "pdf"}
        ]
    });

    PLUPLOADER_BROCHURE.bind('Init', function(up, params){
        // $('#uploader_brochure .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    $('#uploadfiles').click(function(e){
        //uploader.start(); //UPLOADING SHOULD START AFTER SUCCESSFULLY SUBMITTING THE FORM
        e.preventDefault();
    });

    PLUPLOADER_BROCHURE.init();

    PLUPLOADER_BROCHURE.bind('FilesAdded', function(up, files){
        //$('#uploader_brochure .filelist .alert-info button.close').trigger('click'); //limit uploading to 1
        $('#uploader_brochure .uploaded').remove();
        //$('#uploader_brochure .upload-actions').hide();
        $.each(files, function(i, file){
            $('#uploader_brochure .filelist').append(
                '<div id="' + file.id + '" class="alert alert-info">' +
                '<span class="filename">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b> <button type="button" class="close cancelUpload">&times;</button>' +
            '<div class="progress progress-striped"><div class="bar" style="width: 1%;"></div></div></div>');
            //queue_count++;
            $('#uploader_brochure .filelist').on('click', '#' + file.id + ' button.cancelUpload', function(){
                //queue_count--;
                PLUPLOADER_BROCHURE.removeFile(file);
                $('#' + file.id).remove();
                //$('#uploader_brochure .upload-actions').show();
            });
        });
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_BROCHURE.bind('UploadProgress', function(up, file){
        $('#' + file.id + ' b').html(file.percent + '%');
        $('#' + file.id + ' .progress .bar').animate({width: file.percent + '%'}, 100, 'linear');
        if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
            $('#' + file.id + ' .progress').addClass('active');
            $('#' + file.id + ' button.cancelUpload').hide();
        }
    });

    PLUPLOADER_BROCHURE.bind('Error', function(up, err){
    $('#uploader_brochure .filelist').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        'Error: ' + err.code + ', Message: ' + err.message +
            (err.file ? ', File: ' + err.file.name : '') +
            "</div>"
        );

        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_BROCHURE.bind('FileUploaded', function(up, file, info){
        var response = JSON.parse(info.response);
        $('#' + file.id + ' b').html("100%");
        $('#' + file.id + ' .progress .bar').animate({width: '100%'}, 100,  'linear');
        $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').addClass('progress-success');
        $('#' + file.id + ' button.cancelUpload').remove();
        var post_id = (typeof $('#uploader_brochure .filelist').attr('data-attr-id') !== 'undefined') ? $('#uploader_brochure .filelist').attr('data-attr-id') : null;

        var response = JSON.parse(info.response);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: base_url() + 'amiproc/media_ref',
            data: {id: last_post_id, media : response.id, entry_type: ENTRY_TYPE},
            success: function(result){
                //queue_count--;
            }
        });
    });

    //PLUPLOAD_BROCHURE END

    //PLUPLOAD_VIDEO START

    PLUPLOADER_VIDEO = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_video_pickfiles',
        container: 'uploader_video',
        max_file_size : '1024mb',
        //drop_element : "uploader",
        url : base_url() + 'amiproc/plUpload' + '?class=video&entry_type=' + ENTRY_TYPE,
        resize : {},
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: true,
        filters : [
            {title : "Video Clips", extensions : "mp4"}
        ]
    });

    PLUPLOADER_VIDEO.bind('Init', function(up, params){
        // $('#uploader_video .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    $('#uploadfiles').click(function(e){
        //uploader.start(); //UPLOADING SHOULD START AFTER SUCCESSFULLY SUBMITTING THE FORM
        e.preventDefault();
    });

    PLUPLOADER_VIDEO.init();

    PLUPLOADER_VIDEO.bind('FilesAdded', function(up, files){
        //$('#uploader_video .filelist .alert-info button.close').trigger('click'); //limit uploading to 1
        $('#uploader_video .uploaded').remove();
        //$('#uploader_video .upload-actions').hide();
        $.each(files, function(i, file){
            $('#uploader_video .filelist').append(
                '<div id="' + file.id + '" class="alert alert-info">' +
                '<span class="filename">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b> <button type="button" class="close cancelUpload">&times;</button>' +
            '<div class="progress progress-striped"><div class="bar" style="width: 1%;"></div></div></div>');
            //queue_count++;
            $('#uploader_video .filelist').on('click', '#' + file.id + ' button.cancelUpload', function(){
                //queue_count--;
                PLUPLOADER_VIDEO.removeFile(file);
                $('#' + file.id).remove();
                //$('#uploader_video .upload-actions').show();
            });
        });
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_VIDEO.bind('UploadProgress', function(up, file){
        $('#' + file.id + ' b').html(file.percent + '%');
        $('#' + file.id + ' .progress .bar').animate({width: file.percent + '%'}, 100, 'linear');
        if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
            $('#' + file.id + ' .progress').addClass('active');
            $('#' + file.id + ' button.cancelUpload').hide();
        }
    });

    PLUPLOADER_VIDEO.bind('Error', function(up, err){
    $('#uploader_video .filelist').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        'Error: ' + err.code + ', Message: ' + err.message +
            (err.file ? ', File: ' + err.file.name : '') +
            "</div>"
        );

        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_VIDEO.bind('FileUploaded', function(up, file, info){
        var response = JSON.parse(info.response);
        $('#' + file.id + ' b').html("100%");
        $('#' + file.id + ' .progress .bar').animate({width: '100%'}, 100,  'linear');
        $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').addClass('progress-success');
        $('#' + file.id + ' button.cancelUpload').remove();
        var post_id = (typeof $('#uploader_video .filelist').attr('data-attr-id') !== 'undefined') ? $('#uploader_video .filelist').attr('data-attr-id') : null;

        var response = JSON.parse(info.response);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: base_url() + 'amiproc/media_ref',
            data: {id: last_post_id, media : response.id, entry_type: ENTRY_TYPE},
            success: function(result){
                //queue_count--;
            }
        });
    });

    //PLUPLOAD_VIDEO END

    //PLUPLOAD_SPEC START

    PLUPLOADER_SPEC = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_spec_pickfiles',
        container: 'uploader_spec',
        max_file_size : '1024mb',
        //drop_element : "uploader",
        url : base_url() + 'amiproc/plUpload' + '?class=spec&entry_type=' + ENTRY_TYPE,
        resize : {},
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: true,
        filters : [
            {title : "PDF Files", extensions : "pdf"}
        ]
    });

    PLUPLOADER_SPEC.bind('Init', function(up, params){
        // $('#uploader_spec .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    $('#uploadfiles').click(function(e){
        //uploader.start(); //UPLOADING SHOULD START AFTER SUCCESSFULLY SUBMITTING THE FORM
        e.preventDefault();
    });

    PLUPLOADER_SPEC.init();

    PLUPLOADER_SPEC.bind('FilesAdded', function(up, files){
        //$('#uploader_spec .filelist .alert-info button.close').trigger('click'); //limit uploading to 1
        $('#uploader_spec .uploaded').remove();
        //$('#uploader_spec .upload-actions').hide();
        $.each(files, function(i, file){
            $('#uploader_spec .filelist').append(
                '<div id="' + file.id + '" class="alert alert-info">' +
                '<span class="filename">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b> <button type="button" class="close cancelUpload">&times;</button>' +
            '<div class="progress progress-striped"><div class="bar" style="width: 1%;"></div></div></div>');
            //queue_count++;
            $('#uploader_spec .filelist').on('click', '#' + file.id + ' button.cancelUpload', function(){
                PLUPLOADER_SPEC.removeFile(file);
                $('#' + file.id).remove();
                //$('#uploader_spec .upload-actions').show();
                //queue_count--;
            });
        });
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_SPEC.bind('UploadProgress', function(up, file){
        $('#' + file.id + ' b').html(file.percent + '%');
        $('#' + file.id + ' .progress .bar').animate({width: file.percent + '%'}, 100, 'linear');
        if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
            $('#' + file.id + ' .progress').addClass('active');
            $('#' + file.id + ' button.cancelUpload').hide();
        }
    });

    PLUPLOADER_SPEC.bind('Error', function(up, err){
    $('#uploader_spec .filelist').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        'Error: ' + err.code + ', Message: ' + err.message +
            (err.file ? ', File: ' + err.file.name : '') +
            "</div>"
        );

        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_SPEC.bind('FileUploaded', function(up, file, info){
        var response = JSON.parse(info.response);
        $('#' + file.id + ' b').html("100%");
        $('#' + file.id + ' .progress .bar').animate({width: '100%'}, 100,  'linear');
        $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').addClass('progress-success');
        $('#' + file.id + ' button.cancelUpload').remove();
        var post_id = (typeof $('#uploader_spec .filelist').attr('data-attr-id') !== 'undefined') ? $('#uploader_spec .filelist').attr('data-attr-id') : null;

        var response = JSON.parse(info.response);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: base_url() + 'amiproc/media_ref',
            data: {id: last_post_id, media : response.id, entry_type: ENTRY_TYPE},
            success: function(result){
                //queue_count--;
            }
        });
    });

    //PLUPLOAD_SPEC END

    //PLUPLOAD_CERT START

    PLUPLOADER_CERT = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_cert_pickfiles',
        container: 'uploader_cert',
        max_file_size : '1024mb',
        //drop_element : "uploader",
        url : base_url() + 'amiproc/plUpload' + '?class=cert&entry_type=' + ENTRY_TYPE,
        resize : {},
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: true,
        filters : [
            {title : "PDF Files", extensions : "pdf"}
        ]
    });

    PLUPLOADER_CERT.bind('Init', function(up, params){
        // $('#uploader_cert .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    $('#uploadfiles').click(function(e){
        //uploader.start(); //UPLOADING SHOULD START AFTER SUCCESSFULLY SUBMITTING THE FORM
        e.preventDefault();
    });

    PLUPLOADER_CERT.init();

    PLUPLOADER_CERT.bind('FilesAdded', function(up, files){
        //$('#uploader_cert .filelist .alert-info button.close').trigger('click'); //limit uploading to 1
        $('#uploader_cert .uploaded').remove();
        //$('#uploader_cert .upload-actions').hide();
        $.each(files, function(i, file){
            $('#uploader_cert .filelist').append(
                '<div id="' + file.id + '" class="alert alert-info">' +
                '<span class="filename">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b> <button type="button" class="close cancelUpload">&times;</button>' +
            '<div class="progress progress-striped"><div class="bar" style="width: 1%;"></div></div></div>');
            //queue_count++;
            $('#uploader_cert .filelist').on('click', '#' + file.id + ' button.cancelUpload', function(){
                PLUPLOADER_CERT.removeFile(file);
                $('#' + file.id).remove();
                //$('#uploader_cert .upload-actions').show();
                //queue_count--;
            });
        });
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_CERT.bind('UploadProgress', function(up, file){
        $('#' + file.id + ' b').html(file.percent + '%');
        $('#' + file.id + ' .progress .bar').animate({width: file.percent + '%'}, 100, 'linear');
        if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
            $('#' + file.id + ' .progress').addClass('active');
            $('#' + file.id + ' button.cancelUpload').hide();
        }
    });

    PLUPLOADER_CERT.bind('Error', function(up, err){
    $('#uploader_cert .filelist').append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
        'Error: ' + err.code + ', Message: ' + err.message +
            (err.file ? ', File: ' + err.file.name : '') +
            "</div>"
        );

        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_CERT.bind('FileUploaded', function(up, file, info){
        var response = JSON.parse(info.response);
        $('#' + file.id + ' b').html("100%");
        $('#' + file.id + ' .progress .bar').animate({width: '100%'}, 100,  'linear');
        $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').addClass('progress-success');
        $('#' + file.id + ' button.cancelUpload').remove();
        var post_id = (typeof $('#uploader_cert .filelist').attr('data-attr-id') !== 'undefined') ? $('#uploader_cert .filelist').attr('data-attr-id') : null;

        var response = JSON.parse(info.response);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: base_url() + 'amiproc/media_ref',
            data: {id: last_post_id, media : response.id, entry_type: ENTRY_TYPE},
            success: function(result){
                //queue_count--;
            }
        });
    });

    //PLUPLOAD_CERT END
    
    //PLUPLOAD_IMPORTSSD START
    
    PLUPLOADER_IMPORTSSD = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'uploader_ssdimport_pickfiles',
        container: 'uploader_ssdimport',
        max_file_size : '1024mb',
        drop_element : "dropper",
        url : base_url() + 'amiproc/csvUpload',
        flash_swf_url : base_url() + 'res/js//plugins/plupload/Moxie.swf',
        silverlight_xap_url : base_url() + 'res/js//plugins/plupload/Moxie.xap',
        unique_names : false,
        multi_selection: false,
        filters : [
            {title : "Comma Separated Values", extensions : "CSV"}
        ],
        init : {
            FilesAdded: function(up, files){
                $('#dropper h2').hide();
                $('#dropper p.loader-dotted span.status').text('Uploading');
                $('#dropper p.loader-dotted').show();
                up.start();
            }
        }
    });
    
    PLUPLOADER_IMPORTSSD.bind('Init', function(up, params){
        // $('#uploader_cert .filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    

    PLUPLOADER_IMPORTSSD.init();

    

    PLUPLOADER_IMPORTSSD.bind('Error', function(up, err){
    
        up.refresh(); // Reposition Flash/Silverlight
    });

    PLUPLOADER_IMPORTSSD.bind('FileUploaded', function(up, file, info){
        
        $('#dropper p.loader-dotted span.status').text('Parsing file...');
        
        var response = JSON.parse(info.response);
        $.ajax({
            dataType: 'json',
            url: base_url() + 'amiproc/csvParse/' + response.cleanFileName,
            data: {},
            success: function(result){
                //$('#dropper h2').show();
                var htmlstr = '<table class="table list-generic">';
                for (i = 1; i <= result['numRows']; i++) {
                    if(i === 1){
                        htmlstr += '<thead><tr>';
                    }else if(i === 2){
                        htmlstr += '</thead><tbody>';
                    }
                    for (j = 1; j <= result['numCols']; j++) {
                        if(i === 1){
                            htmlstr += '<th>' + result['cells'][i][j] + '</th>';
                        }else{
                            htmlstr += '<td>' + result['cells'][i][j] + '</td>';
                        }
                    }
                    htmlstr += '</tr><tr>';
                }
                htmlstr += '</tr></tbody></table>';
                $('#dropper').replaceWith(htmlstr);
            }
        });
        
        
    });
    //PLUPLOAD_IMPORTSSD END
    
});