var sales_type,
    sub_type;

function propCountriesBlock() {
    var countriesSelector = $('#countriesSelector'),
        countriesSelectorGroup = countriesSelector.closest('.form-group');
    switch (sales_type) {
        case "PULL":
        case "PUSH":
        case "MIX":
        case "CHAIN":
            countriesSelector.prop('disabled', true);
            countriesSelectorGroup.hide();
            break;
        default:
            countriesSelector.prop('disabled', false);
            countriesSelectorGroup.show();
            break;
    }
}

function propCountriesBlockValidate() {
    var countriesSelector = $('#countriesSelector');

    if ($.inArray(sub_type, ['vp', 'svp']) !== -1) {
        countriesSelector.attr('data-parsley-required', 'false');
    } else {
        countriesSelector.attr('data-parsley-required', 'true');
    }
}

$(document).ready(function () {
    var type = $('select[name="type"]'),
        subType = $('select[name="sub_type"]'),
        roles = $('#RolesSelector').find('option:selected'),
        type_option = type.find('option'),
        subType_option = subType.find('option');
    sales_type = type.val();
    sub_type = subType.val();
    switch (roles.data('role')) {
        case 'Sales Manager':
            $.each(type_option, function () {
                if ($(this).val() != 'SL') {
                    $(this).prop('disabled', true);
                }
            });
            $.each(subType_option, function () {
                if ($(this).val() == 'cdops') {
                    $(this).prop('disabled', true);
                }
            });
            break;
        case 'Marketing':
            $.each(type_option, function () {
                if ($(this).val() != 'MARKETING') {
                    $(this).prop('disabled', true);
                }
            });
            subType.val('');
            subType.prop('disabled', true);
            break;
        case 'Salesperson':
            $.each(type_option, function () {
                if ($.inArray($(this).val(), ['PULL', 'PUSH', 'MIX', 'CHAIN']) == -1) {
                    $(this).prop('disabled', true);
                }
            });
            subType.val('');
            subType.prop('disabled', true);
            break;
        case 'Admin':
            $.each(type_option, function () {
                if ($(this).val() != 'ADMIN') {
                    $(this).prop('disabled', true);
                }
            });
            type.val('ADMIN');
            subType.val('');
            subType.prop('disabled', true);
            break;
        case 'Global':
            $.each(type_option, function () {
                if ($.inArray($(this).val(), ['ADMIN', 'MARKETING']) == -1) {
                    $(this).prop('disabled', true);
                }
            });
            type.val('ADMIN');
            subType.val('');
            subType.prop('disabled', true);
            break;
        case 'Trainer':
            $.each(type_option, function () {
                if ($(this).val() != 'TRAINER') {
                    $(this).prop('disabled', true);
                }
            });
            $.each(subType_option, function () {
                if ($(this).val() != 'nsm') {
                    $(this).prop('disabled', true);
                }
            });
            type.val('TRAINER');
            subType.val('nsm');
            break;
        case 'Managing Director':
            $.each(type_option, function () {
                if ($(this).val() != 'SL') {
                    $(this).prop('disabled', true);
                }
            });
            $.each(subType_option, function () {
                if ($(this).val() != 'md') {
                    $(this).prop('disabled', true);
                }
            });
            type.val('SL');
            subType.val('md');
            break;
        default:
            break;
    }

    propCountriesBlock();
    propCountriesBlockValidate();
});
function reset() {
    var type = $('select[name="type"]'),
        subType = $('select[name="sub_type"]');
    type.find('option').prop('disabled', false);
    subType.prop('disabled', false);
    subType.find('option').prop('disabled', false);
}
$(function () {
    // $('#RolesSelector').trigger('change');
    $('#RolesSelector').change(function (e) {

        xhr = null;
        var $this = $(this),
            url = $this.data('fetchurl'),
            $selected = $this.find(":selected"),
            depth = $selected.data('depth'),
            parentId = $selected.data('parent'),
            edit = $this.attr('data-edit') || null,
            $container = $('#FetchContainter');

        roles_val = $this.val();
        $('#RoleDepth').val(depth);

        if (!xhr && url) {

            xhr = $.ajax({
                url: url,
                type: "POST",
                // dataType: 'json',
                data: {depth: depth, parent: parentId, id: $selected.val(), edit: edit},
                success: function (res) {
                    $container.html('').empty();
                    if (res) {
                        $container.append(res);
                    }
                    xhr = null;
                },
                error: function (res) {
                    xhr = null;
                }
            });
        }
    });

    $('#RolesSelector').change(function () {
        var type = $('select[name="type"]'),
            subType = $('select[name="sub_type"]'),
            roles = $(this).find('option:selected'),
            type_option = type.find('option'),
            subType_option = subType.find('option');
        switch (roles.data('role')) {
            case 'Sales Manager':
                reset();
                $.each(type_option, function () {
                    if ($(this).val() != 'SL') {
                        $(this).prop('disabled', true);
                    }
                });
                $.each(subType_option, function () {
                    if ($(this).val() == 'cdops') {
                        $(this).prop('disabled', true);
                    }
                });
                type.val('SL');
                subType.val(sub_type);
                break;
            case 'Marketing':
                reset();
                $.each(type_option, function () {
                    if ($(this).val() != 'MARKETING') {
                        $(this).prop('disabled', true);
                    }
                });
                type.val('MARKETING');
                subType.val('');
                subType.prop('disabled', true);
                break;
            case 'Salesperson':
                reset();
                $.each(type_option, function () {
                    if ($.inArray($(this).val(), ['PULL', 'PUSH', 'MIX', 'CHAIN']) == -1) {
                        $(this).prop('disabled', true);
                    }
                });
                if($.inArray(type.val(), ['PULL', 'PUSH', 'MIX', 'CHAIN']) == -1)
                {
                    type.val('PULL');
                }
                subType.val('');
                subType.prop('disabled', true);
                break;
            case 'Admin':
                reset();
                $.each(type_option, function () {
                    if ($(this).val() != 'ADMIN') {
                        $(this).prop('disabled', true);
                    }
                });
                type.val('ADMIN');
                subType.val('');
                subType.prop('disabled', true);
                break;
            case 'Global':
                reset();
                $.each(type_option, function () {
                    if ($.inArray($(this).val(), ['ADMIN', 'MARKETING']) == -1) {
                        $(this).prop('disabled', true);
                    }
                });
                if($.inArray(type.val(), ['ADMIN', 'MARKETING']) == -1)
                {
                    type.val('ADMIN');
                }
                subType.val('');
                subType.prop('disabled', true);
                break;
            case 'Trainer':
                reset();
                $.each(type_option, function () {
                    if ($(this).val() != 'TRAINER') {
                        $(this).prop('disabled', true);
                    }
                });
                $.each(subType_option, function () {
                    if ($(this).val() != 'nsm') {
                        $(this).prop('disabled', true);
                    }
                });
                type.val('TRAINER');
                subType.val('nsm');
                break;
            case 'Managing Director':
                $.each(type_option, function () {
                    if ($(this).val() != 'SL') {
                        $(this).prop('disabled', true);
                    }
                });
                $.each(subType_option, function () {
                    if ($(this).val() != 'md') {
                        $(this).prop('disabled', true);
                    }
                });
                type.val('SL');
                subType.val('md');
                break;
            default:
                break;
        }

        type.trigger('change');
    });

    $('select[name="sub_type"]').change(function () {
        sub_type = $(this).val();
        propCountriesBlockValidate();
    });

    $(document).on('change', 'select[name="type"]', function () {
        sales_type = $(this).val();
        propCountriesBlock();
    });

});

