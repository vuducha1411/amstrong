var responJson,
    filterColumns,
    colorFlag = 1;
    colorTable = ['#f8d2de', '#b0e5dc'],
    colorChart = ['#3bbfa8', '#da1c5c', '#eda43b', '#2d89ef', '#1e7145', '#2b5797', '#da532c', '#1e7145','#F3B200','#77B900','#2572EB','#AD103C','#632F00','#B01E00','#C1004F','#7200AC','#4617B4','#006AC1','#008287','#199900','#00C13F','#FF981D','#FF2E12','#FF1D77','#AA40FF','#1FAEFF','#56C5FF','#00D8CC','#91D100','#E1B700','#FF76BC','#00A3A3','#FE7C22'];
function customerContactReport(urlGetReport, sp, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/customer_contact_data';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Armstrong_2_customers_id ',
                    'Armstrong_2_customers_name', 'Contact Name', 'Primary Contact', 'Position', 'Phone', 'Phone2',
                    'Fax', 'Email', 'Interests', 'Birthday', 'Remarks'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2CustomersId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "name"},
                        {data: "primaryContactDisplay"},
                        {data: "position"},
                        {data: "phone"},
                        {data: "phone2"},
                        {data: "fax"},
                        {data: "email"},
                        {data: "interests"},
                        {
                            data: "birthday",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "remarks"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function routeMasterTempDataReport(urlGetReport, sp, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/route_master_temp';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Armstrong_2_salespersons_ID',
                    'Armstrong_2_salespersons_Name', 'Armstrong_2_customers_id ', 'Armstrong_2_customers_name', 'Planned Day', 'Planned Week',
                    'Date Created', 'Last updated'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "plannedDay"},
                        {data: "plannedWeek"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "lastUpdated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        }
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function coachingNoteDataReport(urlGetReport, sp, fromdate, todate, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/coaching_note';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Armstrong_2_salespersons_ID',
                    'Salesperson Name', 'Armstrong _2_CCD_ID', 'Armstrong_2_Call_record_ID', 'Date Created', 'Summary of Day',
                    'Grow Summary', 'Follow Up'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "armstrong2SalespersonsName"},
                        {data: "armstrong2CcdId"},
                        {data: "armstrong2CallRecordsId"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "summaryOfDay"},
                        {data: "growSummary"},
                        {data: "followUps"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function getPerfectCallReportTracking(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional) {
    switch (app_type) {
        case 'PULL':
            getPerfectCallReportTrackingPull(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
            break;
        case 'PUSH':
            getPerfectCallReportTrackingPush(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
            break;
        case 'SL':
            getPerfectCallReportTrackingSl(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional);
            break;
        default:
            break;
    }
}

function getPerfectCallReportTrackingPush(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_perfect_call_report_tracking';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = ''
                if (regional == 1) {
                    var arr = ['Country',
                        'Total Calls Made', 'Perfect Call Made', 'Personal Objectives', 'Pantry Check',
                        'Rich Media Demo', 'Store Audit', 'TFO', 'Competitor Product Info'
                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'Total Calls Made', 'Perfect Call Made', 'Personal Objectives', 'Pantry Check',
                        'Rich Media Demo', 'Store Audit', 'TFO', 'Competitor Product Info'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "totalCallMade"},
                            {data: "perfectCallMade"},
                            {data: "numPersonalObjectives"},
                            {data: "numPantryCheck"},
                            {data: "numRichMediaDemo"},
                            {data: "numStoreAudit"},
                            {data: "numTFO"},
                            {data: "numCompetitorProductInfo"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "totalCallMade"},
                            {data: "perfectCallMade"},
                            {data: "numPersonalObjectives"},
                            {data: "numPantryCheck"},
                            {data: "numRichMediaDemo"},
                            {data: "numStoreAudit"},
                            {data: "numTFO"},
                            {data: "numCompetitorProductInfo"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReportTrackingPull(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_perfect_call_report_tracking';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                if (regional == 1) {
                    var arr = ['Country',
                        'Total Calls Made', 'Perfect Call Made', 'Personal Objectives', 'Pantry Check',
                        'Rich Media Demo', 'Samplings', 'TFO', 'Competitor Product Info'
                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'Total Calls Made', 'Perfect Call Made', 'Personal Objectives', 'Pantry Check',
                        'Rich Media Demo', 'Samplings', 'TFO', 'Competitor Product Info'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "totalCallMade"},
                            {data: "perfectCallMade"},
                            {data: "numPersonalObjectives"},
                            {data: "numPantryCheck"},
                            {data: "numRichMediaDemo"},
                            {data: "numSamplings"},
                            {data: "numTFO"},
                            {data: "numCompetitorProductInfo"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "totalCallMade"},
                            {data: "perfectCallMade"},
                            {data: "numPersonalObjectives"},
                            {data: "numPantryCheck"},
                            {data: "numRichMediaDemo"},
                            {data: "numSamplings"},
                            {data: "numTFO"},
                            {data: "numCompetitorProductInfo"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReportTrackingSl(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_perfect_call_report_tracking';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                if (regional == 1) {
                    var arr = ['Country',
                        'Total View UFS Organization', 'Total Customer Approval Made', 'Total Coaching Conducted', 'Total Set Of SR KPI Targets'

                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'Total View UFS Organization', 'Total Customer Approval Made', 'Total Coaching Conducted', 'Total Set Of SR KPI Targets'

                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "totalViewUFS"},
                            {data: "totalCusAppMade"},
                            {data: "totalCoachingConducted"},
                            {data: "totalSetOfSRKpiTargets"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "totalViewUFS"},
                            {data: "totalCusAppMade"},
                            {data: "totalCoachingConducted"},
                            {data: "totalSetOfSRKpiTargets"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReport(urlGetReport, countryId, getall, sp, month, year, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_call_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 CallRecords ID', 'Personal Objectives', 'Pantry Check', 'Rich Media Demo',
                    'Samplings', 'TFO', 'Competitor Product Info'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "isPersonalObjectives"},
                        {data: "isPantryCheck"},
                        {data: "isRichMediaDemo"},
                        {data: "isSamplings"},
                        {data: "isTFO"},
                        {data: "isCompetitorProductInfo"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function getPerfectCallReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_call_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 CallRecords ID', 'Personal Objectives', 'Pantry Check', 'Rich Media Demo',
                    'Samplings', 'TFO', 'Competitor Product Info'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "isPersonalObjectives"},
                        {data: "isPantryCheck"},
                        {data: "isRichMediaDemo"},
                        {data: "isSamplings"},
                        {data: "isTFO"},
                        {data: "isCompetitorProductInfo"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function penetrationDataReport(urlGetReport, month, year, countryId, appType, active, channel, globalChannels, otm, getall, sp, top_products, isGripCustomer, viewBy, range, sr_manage, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/penetration_data_report';
    var per_page = 20;
    var current_page = 1;
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            month: month,
            year: year,
            country: countryId,
            channel: channel,
            globalChannels: globalChannels,
            otm: otm,
            app_type: appType,
            productActive: active,
            getall: getall,
            topProducts: top_products,
            isGripCustomer: isGripCustomer,
            isViewByChannel: viewBy,
            range: range,
            spId: sp,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },

        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {

                // TODO sort result by skuName
                result.listData.sort(function (a, b) {
                    var a1 = a.skuName;
                    var b1 = b.skuName;
                    if (a1 == b1) return 0;
                    return a1 > b1 ? 1 : -1;
                });
                responJson = result.listData;
                var html = '';
                var total_data = result.listData.length;
                //var total_page = total_data / per_page;
                //if (total_page % 1 !== 0) {
                //    total_page = total_page + 1;
                //}

                if(result.listData.length > 0){
                    html += generateHtmlChart(result.listData,0);
                    html += generateHtmlReport(result.listData,viewBy);
                }else{
                    html = '<div class="notify">No data</div>';
                }

                //html +='<nav>' +
                //'<ul class="pagination">' +
                //'<li data-page="prev"><a href="#self" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                //for(var p = 1; p <= total_page; p++){
                //    if(p == current_page){
                //        html += '<li class="active" data-page="'+p+'"><a href="#self">' +p+ '</a></li>';
                //    }else{
                //        html += '<li data-page="'+p+'"><a href="#self">' +p+ '</a></li>';
                //    }
                //}
                //html += '<li data-page="next"><a href="#self" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                //html += '</ul>' +
                //'</nav>';
                $('#reporting-view').html(html);

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function penetrationDataChannelSkuReport(urlGetReport, month, year, countryId, appType, active, channel, globalChannels, otm, top_products, isGripCustomer, viewBy, range, sr_manage, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/penetration_data_report_channel_sku';
    var per_page = 20;
    var current_page = 1;
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            month: month,
            year: year,
            country: countryId,
            channel: channel,
            globalChannels: globalChannels,
            otm: otm,
            app_type: appType,
            productActive: active,
            topProducts: top_products,
            isGripCustomer: isGripCustomer,
            isViewByChannel: viewBy,
            range: range,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                result.listData.sort(function(a, b) {
                    var a1 = a.name;
                    var b1 = b.name;
                    if (a1 == b1) return 0;
                    return a1 > b1 ? 1 : -1;
                });
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0) {
                    html += generateHtmlChart(result.listData,3);
                    html += generateHtmlReport(result.listData,viewBy);
                }else{
                    html = '<div class="notify">No data</div>';
                }
                $('#reporting-view').html(html);

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function penetrationDataSrSkuReport(urlGetReport, month, year, countryId, appType, active, globalChannels, getall, sp, top_products, isGripCustomer, viewBy, range, sr_manage, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/penetration_data_report_sr_sku';
    var per_page = 20;
    var current_page = 1;
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            month: month,
            year: year,
            country: countryId,
            getall: getall,
            app_type: appType,
            productActive: active,
            globalChannels: globalChannels,
            topProducts: top_products,
            isGripCustomer: isGripCustomer,
            spId: sp,
            isViewByChannel: viewBy,
            range: range,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                result.listData.sort(function(a, b) {
                    var a1 = a.name;
                    var b1 = b.name;
                    if (a1 == b1) return 0;
                    return a1 > b1 ? 1 : -1;
                });
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0) {
                    html += generateHtmlChart(result.listData,5);
                    html += generateHtmlReport(result.listData,viewBy);
                }else{
                    html = '<div class="notify">No data</div>';
                }
                $('#reporting-view').html(html);

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function penetrationDataSrChannelReport(urlGetReport, month, year, countryId, appType, active, channel, globalChannels, otm, getall, sp, top_products, isGripCustomer, viewBy, range, sr_manage, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/penetration_data_report_sr_channel';
    var per_page = 20;
    var current_page = 1;
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            month: month,
            year: year,
            country: countryId,
            getall: getall,
            channel: channel,
            globalChannels: globalChannels,
            otm: otm,
            app_type: appType,
            productActive: active,
            topProducts: top_products,
            isGripCustomer: isGripCustomer,
            spId: sp,
            isViewByChannel: viewBy,
            range: range,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                result.listData.sort(function(a, b) {
                    var a1 = a.name;
                    var b1 = b.name;
                    if (a1 == b1) return 0;
                    return a1 > b1 ? 1 : -1;
                });
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0) {
                    html += generateHtmlChart(result.listData,5);
                    html += generateHtmlReport(result.listData,viewBy);
                }else{
                    html = '<div class="notify">No data</div>';
                }
                $('#reporting-view').html(html);

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function getSplitValue(key, pos) {
    var res = '';
    if(key !== null){
        res = key.split(" ");
        res = (res[pos] != null) ? res[pos] : '';
    }
    return res;
}

function getHeightColum(c_height) {
    var results = new Array();
    results['value'] = (300 / 100) * c_height;
    //if(results['value'] < 40) results['value'] = results['value'] + 30;
    if (results['value'] == 0) results['value'] = results['value'] + 1;
    results['y'] = 130 + (300 - results['value']);
    return results;
}

function getMaxValue(list_arr) {
    var results = new Array();
    var max = list_arr['0'];
    for (i = 0; i < list_arr.length; i++) {
        if (max < list_arr[i]) max = list_arr[i];
    }
    results['max'] = max;
    results['position_y'] = 10 + (345 - max);
    return results;
}

function gripGrabReport(urlGetReport, fromdate, todate, countryId, appType, channel, otm, getall, sp, filterColumn, sr_manage, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/grip_and_grab_report_sr';
    filterColumns = filterColumn;
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            channel: channel,
            otm: otm,
            getall: getall,
            app_type: appType,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                responJson = result.listData;
                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0) {
                    html += generateGripGrabHtmlReport(result.listData);
                }else{
                    html = '<div class="notify">No data</div>';
                }

                $('#reporting-view').html(html);
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function richMediaReport(urlGetReport, countryId, sr_manage, isMultiCountry, regional){
    var urlget = urlGetReport + 'summaryreports/rich_media';

    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            country: countryId,
            is_multi_country: isMultiCountry,
            sr_manage: sr_manage
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0){
                    html += generateRichMediaHtmlReport(result.listData);
                }else{
                    html = '<div class="notify">No data</div>';
                }

                $('#reporting-view').html(html);
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function promotionsActivityReport(urlGetReport, countryId, getall, sp, promotionName, sr_manage, isMultiCountry, regional){
    var urlget = urlGetReport + 'summaryreports/promotion_activities_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            country: countryId,
            getall: getall,
            spId: sp,
            promotionActivityID: promotionName,
            sr_manage: sr_manage,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0){
                    html += generatePromotionActivityHtmlReport(result.listData);
                }else{
                    html = '<div class="notify">No data</div>';
                }

                $('#reporting-view').html(html);
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

/*function lastPantryCheckReport(urlGetReport, fromdate, todate, countryId, getall, sp, app_type, viewType, isMultiCountry, regional){
    var urlget = urlGetReport + 'summaryreports/report_last_pantry_check';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            country: countryId,
            from_date: fromdate,
            to_date: todate,
            getall: getall,
            spId: sp,
            app_type: app_type,
            view_type: viewType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            $('#myModal').modal('hide');
            if (result.codeError === undefined || result.codeError === null) {
                responJson = result.listData;

                var html = '';
                var total_data = result.listData.length;
                if (total_data > 0){
                    html += generatePromotionActivityHtmlReport(result.listData);
                }else{
                    html = '<div class="notify">No data</div>';
                }

                $('#reporting-view').html(html);
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}*/

function getPerfectStoreReport(urlGetReport, countryId, getall, sp, month, year, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_store_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            month: month,
            year: year,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = result.data.cellsName;
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '<tbody>';
                //body
                var stt;
                for (var j = 0; j < result.data.listData.length; j++) {
                    var valueRow = result.data.listData[j];
                    stt = j + 1;
                    html += '<tr>';
                    html += '<td>' + stt + '</td>';
                    html += '<td>' + valueRow.armstrong_2_call_records_id + '</td>';
                    html += '<td>' + valueRow.armstrong2SalespersonsId + '</td>';
                    html += '<td>' + valueRow.armstrong2SalespersonsName + '</td>';
                    html += '<td>' + valueRow.armstrong2CustomersId + '</td>';
                    html += '<td>' + valueRow.armstrong2CustomersName + '</td>';
                    html += '<td>' + valueRow.typePlus + '</td>';
                    html += '<td>' + valueRow.plusOutlets + '</td>';
                    html += '<td>' + valueRow.channel + '</td>';
                    var list = valueRow.listCriteriaValueByCallRecordId;
                    for (var k = 0; k < list.length; k++) {
                        html += '<td>' + list[k] + '</td>';
                    }
                    html += '<td>' + valueRow.dateCreated + '</td>';
                    html += '<td>' + valueRow.datetimeCallStart + '</td>';
                    html += '<td>' + valueRow.datetimeCallEnd + '</td>';
                    html += '<td>' + valueRow.week + '</td>';
                    html += '</tr>';
                }

                //end body
                html += '</tbody>';

                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable();
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReportTrackingDetail(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional) {
    switch (app_type) {
        case 'PULL':
            getPerfectCallReportTrackingDetailPull(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
            break;
        case 'PUSH':
            getPerfectCallReportTrackingDetailPush(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
            break;
        case 'SL':
            getPerfectCallReportTrackingDetailSl(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional);
            break;
        default:
            break;
    }
}
function getPerfectCallReportTrackingDetailPull(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_call_report_tracking_detail';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['SR ID',
                    'SR Name', 'Call Record ID', 'Date', 'Customer ID', 'Customer Name',
                    'Customer OTM', 'Customer Channel', 'Personal Objectives', 'Pantry Check', 'Rich Media Demo',
                    'Samplings', 'TFO', 'Competitor Product Info', 'Is Perfect Call'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "date"},
                        {data: "customerId"},
                        {data: "customerName"},
                        {data: "customerOTM"},
                        {data: "customerChannel"},
                        {data: "isPersonalObjectives"},
                        {data: "isPantryCheck"},
                        {data: "isRichMediaDemo"},
                        {data: "isSamplings"},
                        {data: "isTFO"},
                        {data: "isCompetitorProductInfo"},
                        {data: "isPerfectCall"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReportTrackingDetailPush(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_call_report_tracking_detail';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['SR ID',
                    'SR Name', 'Call Record ID', 'Date', 'Customer ID', 'Customer Name',
                    'Customer OTM', 'Customer Channel', 'Personal Objectives', 'Inventory Check', 'Store Audit',
                    'Rich Media Demo', 'TFO', 'Competitor Product Info', 'Is Perfect Call'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "date"},
                        {data: "customerId"},
                        {data: "customerName"},
                        {data: "customerOTM"},
                        {data: "customerChannel"},
                        {data: "isPersonalObjectives"},
                        {data: "isPantryCheck"},
                        {data: "storeAudit"},
                        {data: "isRichMediaDemo"},
                        {data: "isTFO"},
                        {data: "isCompetitorProductInfo"},
                        {data: "isPerfectCall"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getPerfectCallReportTrackingDetailSl(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_perfect_call_report_tracking_detail';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: app_type,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['SR ID',
                    'SR Name', 'Date', 'View UFS Organization', 'Customer Approval Made', 'Coaching Conducted',
                    'Set Of SR KPI Targets'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "date"},
                        {data: "viewUFS"},
                        {data: "cusAppMade"},
                        {data: "coachingConducted"},
                        {data: "setOfSRKpiTargets"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function penetrationCustomersReport(urlGetReport, month, year, countryId, number_sku, sp, getall, app_type, channel, otm, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/penetration_customers_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            month: month,
            year: year,
            country: countryId,
            numberSku: number_sku,
            spId: sp,
            getall: getall,
            channel: channel,
            otm: otm,
            app_type: app_type,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Salespersons  Name',
                    'Penetration % (rolling 3 months)', 'Penetration % (rolling 6 months)', 'Penetration % (rolling 12 months)', 'Penetration % (Jan-Jun) last qualifying period', 'Penetration % (Jul-Dec) last qualifying period',
                    '1 year Penetration last qualifying'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "saleName",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "saleName"},
                        {data: "penetrationRolling3Month"},
                        {data: "penetrationRolling6Month"},
                        {data: "penetrationRolling12Month"},
                        {data: "penetrationJanToJun"},
                        {data: "penetrationJulToDec"},
                        {data: "penetration1Year"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function getDetailAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/get_detail_app_versions_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Country Id',
                    'Country Name', 'AS2 Salespersons ID', 'Salesperson Name', 'Customer ID', 'Customer Name',
                    'Call Closed', 'Sync Time', 'Call and Sync Timing'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "countryId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "countryId"},
                        {data: "coutryName"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "customerID"},
                        {data: "customerName"},
                        {
                            data: "callClosed",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "syncTime",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "callAndSyncTiming"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getAppVersionsReport(urlGetReport, sp, fromdate, todate, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_app_versions_report';


    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = null;

                if (appType == 'SL') {
                    arr = ['Country Id',
                        'Country Name', 'AS2 Salespersons ID', 'Salesperson Name', 'Last Sync', 'App Versions'
                    ];
                } else {
                    arr = ['Country Id',
                        'Country Name', 'AS2 Salespersons ID', 'Salesperson Name', 'Last Sync', 'App Versions',
                        'Calls synced within 24hrs/Total Calls (%)', 'Calls synced more than 24hrs/Total Calls (%)'
                    ];
                }


                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = '';

                if (appType == 'SL') {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "countryId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryId"},
                            {data: "coutryName"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {
                                data: "lastSync",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        var d = new Date(data);
                                        return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                    }
                                    return data;
                                }
                            },
                            {data: "version"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                } else {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "countryId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryId"},
                            {data: "coutryName"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {
                                data: "lastSync",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        var d = new Date(data);
                                        return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                    }
                                    return data;
                                }
                            },
                            {data: "version"},
                            {data: "callsSyncedWithin24hrs"},
                            {data: "callsSyncedMoreThan24hrs"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });

                }


            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getReportAllInOne(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_all_in_one';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            month: month,
            year: year,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['SP Manager',
                    'Salesperson Name', 'Total Call', 'Total Call length', 'Average time per call (minutes)', 'Call Period Morning',
                    'Call Period Afternoon', 'Call Period Evening', 'OTM A Call Frequency', 'OTM B Call Frequency', 'OTM C Call Frequency',
                    'OTM Call D Frequency', 'OTM N/a Call Frequency', 'Pantry check', '% Pantry check call (Mighty 10 SKU)', 'Total TFO',
                    'Total TFO Rs', 'Total TFO Ws', '% TFO buy from RS', '% TFO buy from WS', 'Total SKU',
                    'SKU/TFO', 'Total Amount', 'Average amount/TFO', 'Total # of Sampling'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "salespersonsManagerName",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "salespersonsManagerName"},
                        {data: "armstrong2SalespersonsName"},
                        {data: "totalCall"},
                        {
                            data: "totalCallLength",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "averageTimePerCall",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "percentCallPeriodMorning"},
                        {data: "percentCallPeriodAfternoon"},
                        {data: "percentCallPeriodEvening"},
                        {data: "percentTimesCallOtmA"},
                        {data: "percentTimesCallOtmB"},
                        {data: "percentTimesCallOtmC"},
                        {data: "percentTimesCallOtmD"},
                        {data: "percentTimesCallOtmNa"},
                        {data: "totalPantryCheck"},
                        {data: "percentPcItemTopTenPerCall"},
                        {data: "totalTfo"},
                        {data: "totalTfoRs"},
                        {data: "totalTfoWs"},
                        {data: "percentTotalTfoRs"},
                        {data: "percentTotalTfoWs"},
                        {data: "totalSku"},
                        {data: "averagetotalSku"},
                        {
                            data: "totalAmount",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "averagetotalAmount",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "totalSampling"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getRecipesData(urlGetReport, countryId, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/data_recipes';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            country: countryId
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['name', 'name_alt', 'cuisine_channels_id', 'cuisine_channels_name', 'preparation', 'ingredients',
                    'sku_number', 'sku_name', 'total_sku', 'country', 'listed', 'date_created', 'last_updated'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "name",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "name"},
                        {data: "nameAlt"},
                        {data: "cuisineChannelsId"},
                        {data: "cuisineChannelsName"},
                        {data: "preparation"},
                        {data: "ingredients"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "totalSkuUse"},
                        {data: "countryName"},
                        {data: "sListed"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "lastUpdated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        }
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function getReportAllInOne(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/get_all_in_one';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            month: month,
            year: year,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['SP Manager',
                    'Salesperson Name', 'Total Call', 'Total Call length', 'Average time per call (minutes)', 'Call Period Morning',
                    'Call Period Afternoon', 'Call Period Evening', 'OTM A Call Frequency', 'OTM B Call Frequency', 'OTM C Call Frequency',
                    'OTM Call D Frequency', 'OTM N/a Call Frequency', 'Pantry check', '% Pantry check call (Mighty 10 SKU)', 'Total TFO',
                    'Total TFO Rs', 'Total TFO Ws', '% TFO buy from RS', '% TFO buy from WS', 'Total SKU',
                    'SKU/TFO', 'Total Amount', 'Average amount/TFO', 'Total # of Sampling'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "salespersonsManagerName",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "salespersonsManagerName"},
                        {data: "armstrong2SalespersonsName"},
                        {data: "totalCall"},
                        {
                            data: "totalCallLength",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "averageTimePerCall",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "percentCallPeriodMorning"},
                        {data: "percentCallPeriodAfternoon"},
                        {data: "percentCallPeriodEvening"},
                        {data: "percentTimesCallOtmA"},
                        {data: "percentTimesCallOtmB"},
                        {data: "percentTimesCallOtmC"},
                        {data: "percentTimesCallOtmD"},
                        {data: "percentTimesCallOtmNa"},
                        {data: "totalPantryCheck"},
                        {data: "percentPcItemTopTenPerCall"},
                        {data: "totalTfo"},
                        {data: "totalTfoRs"},
                        {data: "totalTfoWs"},
                        {data: "percentTotalTfoRs"},
                        {data: "percentTotalTfoWs"},
                        {data: "totalSku"},
                        {data: "averagetotalSku"},
                        {
                            data: "totalAmount",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "averagetotalAmount",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "totalSampling"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function allInOneTransactionsReprot(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/all_in_one_transactions';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Total customer', 'Total customer OTM A', 'Total customer OTM B', 'Total customer OTM C',
                    'Total customer OTM D', 'Total Customer OTM unspecified', 'TFO Total Number', 'TFO’s sales value', 'Total Call made',
                    'Total Pantry Unique count', 'Total Sampling Unique Count', 'Total number of Grip', 'Total number of Grab', 'Total New Grip',
                    'Total New Grab', 'Total newly approved customers'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "totalCustomer"},
                        {data: "totalOtmA"},
                        {data: "totalOtmB"},
                        {data: "totalOtmC"},
                        {data: "totalOtmD"},
                        {data: "totalOtmUnspec"},
                        {data: "totalTfoNumber"},
                        {
                            data: "tfoSalesValue",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "totalCall"},
                        {data: "pantryUniqueCount"},
                        {data: "samlingUniqueCount"},
                        {data: "numberOfGrip"},
                        {data: "numberOfGrab"},
                        {data: "numberNewGrip"},
                        {data: "numberNewGrab"},
                        {data: "totalApproveCustomer"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function tfoReprotSr(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/tfo_reprot_sr';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Total Unique Count of TFO', 'Total number of unique TFO made by Email', 'Total number of unique TFO made by Fax', 'Total number of unique TFO made by Save Record',
                    'Total number of unique TFO made by Phone', 'Total Sales value', 'Average Sales Value per TFO', 'Total number of Unique SKU', 'Total number of SKU',
                    'Average SKU sold per TFO'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "totalTfo"},
                        {data: "totalTfoByEmail"},
                        {data: "totalTfoByFax"},
                        {data: "totalTfoBySaveRecord"},
                        {data: "totalTfoByPhone"},
                        {
                            data: "totalSaleValue",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "avgSalesValuePerTfo",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "totalSkuUnique"},
                        {data: "totalSku"},
                        {data: "averageSkuSoldPerTfo"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function samplingReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/sampling_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Total Calls', 'Total Count of all Sampling done', 'Average Sampling check per Call', 'Total Sampling Wet',
                    'Total Sampling DRY', 'Total Sampling Cooking Demo', 'Average number of sampling made per Unique Call', 'Total Unique sampling items', 'Average number of Unique sampling items made per Unique Call',
                    'Total TFOs generated by Sampling'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "totalCalls"},
                        {data: "countAllSampling"},
                        {data: "argSamplingCheckPerCall"},
                        {data: "countSamplingWet"},
                        {data: "countSamplingDry"},
                        {data: "countSamplingCookingDemo"},
                        {data: "argSamplingPerUniqueCall"},
                        {data: "uniqueSamplingItem"},
                        {data: "argUniqueSamplingPerUniquaCall"},
                        {data: "totalTfoGrenareBySampling"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function approvalReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/approval_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Approved', 'Date Created'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "approved"},
                        {data: "stDateCreated"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function topTenPenetrationSrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/top_ten_penetration_sr';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Total Customer penetrations top ten', 'Total Customer', 'Percent Customer penetrations top ten'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "numberCuostomerByTopTenProduct"},
                        {data: "numberCustomer"},
                        {data: "percentCustomer"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function routePlanReprot(urlGetReport, sp, month, year, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/route_plan_reprot';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            month: month,
            year: year,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Number of Adhoc Calls added', 'Number of Adhoc Calls Shifted', 'Number of Planned Calls Removed', 'Rate Adhoc calls added/Total calls'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "numAdhocAdd"},
                        {data: "numAddhocShif"},
                        {data: "numPlanCallRemove"},
                        {data: "perAddOnTotall"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function gripAndGrabReportSR(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/grip_and_grab_report_sr';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Total Grip', 'New grip', 'Total Grab', 'New Grab',
                    'Average New Grip /Total Grip', 'Average New Grab /Total Grab', 'New Grab value', 'New Grab value per New Grab', 'Value from New Grip',
                    'Value per New Grip', 'Total Amount Lost Grab', 'Total Amount Lost Grip'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "totalGrip"},
                        {data: "newGrip"},
                        {data: "totalGrab"},
                        {data: "newGrab"},
                        {data: "averageGripPerTotalGrip"},
                        {data: "averageGrabPerTotalGrab"},
                        {
                            data: "valueNewGrap",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "valueNewGrapPerNewGrap",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "valueNewGrip",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "valueNewGripPerGrip",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "totalLostGrap"},
                        {data: "totalLostGrip"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadReportCall(urlGetReport, sp, fromdate, todate, countryId, channel, otm, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/reportcall';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            channel: channel,
            otm: otm,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'Call Made', 'Working Day', 'Actual Working Day', 'Average Call per Working Day',
                    'Average Call per Actual Working Day', 'TFO Made Inside Call', 'TFO Made Inside Call Value', 'TFO Made Outside Call', 'TFO Made Outside Call Value',
                    'Strike Rate'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "callMade"},
                        {data: "workingDay"},
                        {data: "actualWorkingDay"},
                        {
                            data: "aveCallPerWorkingDay",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {
                            data: "aveCallPerActualWorkingDay",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "tfoMadeInsideCall"},
                        {
                            data: "tfoMadeInsideCallValue",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "tfoMadeOutsideCall"},
                        {
                            data: "tfoMadeOutsideCallValue",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "strikeRate"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadReportCustomer(urlGetReport, sp, fromdate, todate, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/customers';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            app_type: appType,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                if (regional == 1) {
                    var arr = ['Country',
                        'Total Customers', 'OTM A', 'OTM B', 'OTM C',
                        'OTM D', 'OTM Unclassified', 'Channel Unclassified', 'Total SKU sold', 'Total TFO Count',
                        'Total TFO Revenue'
                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'Total Customers', 'OTM A', 'OTM B', 'OTM C',
                        'OTM D', 'OTM Unclassified', 'Channel Unclassified', 'Total SKU sold', 'Total TFO Count',
                        'Total TFO Revenue'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "totalCustomer"},
                            {data: "totalOtmA"},
                            {data: "totalOtmB"},
                            {data: "totalOtmC"},
                            {data: "totalOtmD"},
                            {data: "totalOtmUnclassified"},
                            {data: "totalChannelUnclassified"},
                            {data: "totalSukuSold"},
                            {data: "totalTfoCount"},
                            {
                                data: "totalTfoRevenue",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        return data.toFixed(2);
                                    }
                                    return data;
                                }
                            }
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "totalCustomer"},
                            {data: "totalOtmA"},
                            {data: "totalOtmB"},
                            {data: "totalOtmC"},
                            {data: "totalOtmD"},
                            {data: "totalOtmUnclassified"},
                            {data: "totalChannelUnclassified"},
                            {data: "totalSukuSold"},
                            {data: "totalTfoCount"},
                            {
                                data: "totalTfoRevenue",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        return data.toFixed(2);
                                    }
                                    return data;
                                }
                            }
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadReportOtm(urlGetReport, sp, fromdate, todate, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/report_otm';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                if (regional == 1) {
                    var arr = ['Country',
                        'OTM A Customer Count', 'OTM B Customer Count', 'OTM C Customer Count', 'OTM D Customer Count',
                        'Total OTM A Customer Visited', 'Total OTM B Customer Visited', 'Total OTM C Customer Visited', 'Total OTM D Customer Visited', 'OTM A Fulfillment Rate',
                        'OTM B Fulfillment Rate', 'OTM C Fulfillment Rate', 'OTM D Fulfillment Rate'
                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'OTM A Customer Count', 'OTM B Customer Count', 'OTM C Customer Count', 'OTM D Customer Count',
                        'Total OTM A Customer Visited', 'Total OTM B Customer Visited', 'Total OTM C Customer Visited', 'Total OTM D Customer Visited', 'OTM A Fulfillment Rate',
                        'OTM B Fulfillment Rate', 'OTM C Fulfillment Rate', 'OTM D Fulfillment Rate'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "otm_A_customer_count"},
                            {data: "otm_B_customer_count"},
                            {data: "otm_C_customer_count"},
                            {data: "otm_D_customer_count"},
                            {data: "otm_A_customer_count_visited"},
                            {data: "otm_B_customer_count_visited"},
                            {data: "otm_C_customer_count_visited"},
                            {data: "otm_D_customer_count_visited"},
                            {data: "otm_A_fulfillment_rate"},
                            {data: "otm_B_fulfillment_rate"},
                            {data: "otm_C_fulfillment_rate"},
                            {data: "otm_D_fulfillment_rate"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "otm_A_customer_count"},
                            {data: "otm_B_customer_count"},
                            {data: "otm_C_customer_count"},
                            {data: "otm_D_customer_count"},
                            {data: "otm_A_customer_count_visited"},
                            {data: "otm_B_customer_count_visited"},
                            {data: "otm_C_customer_count_visited"},
                            {data: "otm_D_customer_count_visited"},
                            {data: "otm_A_fulfillment_rate"},
                            {data: "otm_B_fulfillment_rate"},
                            {data: "otm_C_fulfillment_rate"},
                            {data: "otm_D_fulfillment_rate"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadReportPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'summaryreports/report_pantry_check';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                if (regional == 1) {
                    var arr = ['Country',
                        'Total Calls', 'Total Pantry Checks', 'Average Pantry Check Per Call', 'Total Pantry Checks Items',
                        'Average Pantry Check Items Per Call', 'Total Unique Calls', 'Total Unique Pantry Checks', 'Average Unique Pantry Check Per Unique Call', 'Total Unique Pantry Checks Items',
                        'Average Unique Pantry Check Items Per Unique Call'
                    ];
                } else {
                    var arr = ['AS2 Salespersons ID',
                        'Salesperson Name', 'Total Calls', 'Total Pantry Checks', 'Average Pantry Check Per Call', 'Total Pantry Checks Items',
                        'Average Pantry Check Items Per Call', 'Total Unique Calls', 'Total Unique Pantry Checks', 'Average Unique Pantry Check Per Unique Call', 'Total Unique Pantry Checks Items',
                        'Average Unique Pantry Check Items Per Unique Call'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                if (regional == 1 && isMultiCountry == 1) {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "countryCode",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "countryCode"},
                            {data: "totalCalls"},
                            {data: "totalPantryChecks"},
                            {data: "averagePantryCheckPerCall"},
                            {data: "totalPantryChecksItems"},
                            {data: "averagePantryCheckItemsPerCall"},
                            {data: "totalUniqueCalls"},
                            {data: "totalUniquesPantryChecks"},
                            {data: "averageUniquePantryCheckPerUniqueCall"},
                            {data: "totalsUniquePantryChecksItem"},
                            {data: "averageUniquePantryCheckItemsPerUniqueCall"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                } else {
                    var t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SalespersonsId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "totalCalls"},
                            {data: "totalPantryChecks"},
                            {data: "averagePantryCheckPerCall"},
                            {data: "totalPantryChecksItems"},
                            {data: "averagePantryCheckItemsPerCall"},
                            {data: "totalUniqueCalls"},
                            {data: "totalUniquesPantryChecks"},
                            {data: "averageUniquePantryCheckPerUniqueCall"},
                            {data: "totalsUniquePantryChecksItem"},
                            {data: "averageUniquePantryCheckItemsPerUniqueCall"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataSamplingAll(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_sampling';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Sampling ID',
                    'AS2 Call Records ID', 'AS2 Salespersons ID', 'Salesperson Name', 'AS2 Customers ID', 'Customer Name',
                    'SKU number', 'SKU name', 'Packing Size', 'Country Channel', 'Type',
                    'Comments', 'Customer Type', 'Business Type', 'Date Call Start', 'Time Call Start'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SamplingId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SamplingId"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "sku_number"},
                        {data: "sku_name"},
                        {data: "pakingSize"},
                        {data: "countryChannel"},
                        {data: "type"},
                        {data: "comments"},
                        {data: "customerType"},
                        {data: "businessType"},
                        {data: "dateCallStart"},
                        {data: "timeCallStart"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataSamplingTw(urlGetReport, sp, fromdate, todate, countryId, getall, app_type, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_sampling';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Sampling ID',
                    'AS2 Call Records ID', 'AS2 Salespersons ID', 'Salesperson Name', 'Sales Hierarchy', 'AS2 Customers ID',
                    'Customer Name', 'SKU number', 'SKU name', 'Packing Size', 'Country Channel',
                    'Type', 'Comments', 'Customer Type', 'Business Type', 'Date Call Start',
                    'Time Call Start'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SamplingId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SamplingId"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "salesHierarchy"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "sku_number"},
                        {data: "sku_name"},
                        {data: "pakingSize"},
                        {data: "countryChannel"},
                        {data: "type"},
                        {data: "comments"},
                        {data: "customerType"},
                        {data: "businessType"},
                        {data: "dateCallStart"},
                        {data: "timeCallStart"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadDataPantryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_pantry_check';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Pantry Check ID',
                    'AS2 Call Records ID', 'AS2 Salespersons ID', 'Salesperson Name', 'AS2 Customers ID', 'Customer Name',
                    'SKU number', 'SKU name', 'Packing Size', 'Current Qty Case', 'Current Qty Pc',
                    'Consumption Case', 'Consumption Pc', 'Country Channel', 'Notes', 'Date Call Start',
                    'Time Call Start', 'Customer Type', 'Bussiness Type'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2PantryCheckId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2PantryCheckId"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "sku_number"},
                        {data: "sku_name"},
                        {data: "pakingSize"},
                        {data: "current_qty_case"},
                        {data: "current_qty_pc"},
                        {data: "consumption_case"},
                        {data: "consumption_pc"},
                        {data: "countryChannel"},
                        {data: "notes"},
                        {data: "dateCallStart"},
                        {data: "timeCallStart"},
                        {data: "customerType"},
                        {data: "businessType"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadDataInventoryCheck(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_inventory_check';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Pantry Check ID',
                    'AS2 Call Records ID', 'AS2 Salespersons ID', 'Salesperson Name', 'AS2 Customers ID', 'Customer Name',
                    'SKU number', 'SKU name', 'Packing Size', 'Current Qty Case', 'Current Qty Pc',
                    'Consumption Case', 'Consumption Pc', 'Country Channel', 'Notes', 'Date Call Start',
                    'Time Call Start', 'Customer Type', 'Bussiness Type'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2PantryCheckId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2PantryCheckId"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "sku_number"},
                        {data: "sku_name"},
                        {data: "pakingSize"},
                        {data: "current_qty_case"},
                        {data: "current_qty_pc"},
                        {data: "consumption_case"},
                        {data: "consumption_pc"},
                        {data: "countryChannel"},
                        {data: "notes"},
                        {data: "dateCallStart"},
                        {data: "timeCallStart"},
                        {data: "customerType"},
                        {data: "businessType"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataSsdReprotAll(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_ssd';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr;
                if (countryId == 4 || countryId == 5 || countryId == 6 || countryId == 7) {
                    arr = [
                        'armstrong_2_ssd_id', 'armstrong_2_salespersons_id', 'salespersons', 'armstrong_2_customers_id', 'customers', 'armstrong_1_distributors_id',
                        'armstrong_2_distributors_id', 'distributors_name', 'armstrong_1_wholesalers_id', 'armstrong_2_wholesalers_id', 'wholesalers_name',
                        'armstrong_2_call_records_id', 'sku_number', 'sku_name', 'packing_size', 'qty_cases', 'qty_pcs', 'free_qty_cases',
                        'free_qty_pcs', 'total_price', 'invoice_number', 'method', 'ssd_month',
                        'ssd_year', 'ssd_date', 'date_created', 'delivery_date', 'remarks',
                        'status', 'business_type', 'country_id'
                    ];
                } else {
                    arr = [
                        'armstrong_2_ssd_id', 'armstrong_2_salespersons_id', 'salespersons', 'armstrong_2_customers_id', 'customers', 'armstrong_1_distributors_id',
                        'armstrong_2_distributors_id', 'distributors_name', 'armstrong_1_wholesalers_id', 'armstrong_2_wholesalers_id', 'wholesalers_name',
                        'armstrong_2_call_records_id', 'sku_number', 'sku_name', 'packing_size', 'qty_cases', 'qty_pcs', 'free_qty_cases',
                        'free_qty_pcs', 'total_price', 'invoice_number', 'method', 'ssd_month',
                        'ssd_year', 'ssd_date', 'date_created', 'delivery_date', 'remarks',
                        'new_sku', 'status', 'business_type', 'country_id'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t;

                if (countryId == 4 || countryId == 5 || countryId == 6 || countryId == 7) {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SsdId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SsdId"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "armstrong2CustomersId"},
                            {data: "armstrong2CustomersName"},
                            {data: "armstrong1DistributorsId"},
                            {data: "armstrong2DistributorsId"},
                            {data: "armstrong2DistributorsName"},
                            {data: "armstrong1WholesalersId"},
                            {data: "armstrong2WholesalersId"},
                            {data: "armstrong2WholesalersName"},
                            {data: "armstrong2CallRecordsId"},
                            {data: "skuNumber"},
                            {data: "skuName"},
                            {data: "pakingSize"},
                            {data: "qtyCases"},
                            {data: "qtyPcs"},
                            {data: "freeQtyCases"},
                            {data: "freeQtyPcs"},
                            {
                                data: "totalPrice",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        return data.toFixed(2);
                                    }
                                    return data;
                                }
                            },
                            {data: "invoiceNumber"},
                            {data: "method"},
                            {data: "ssdMonth"},
                            {data: "ssdYear"},
                            {data: "stSsdDate"},
                            {data: "stDateCreated"},
                            {data: "stDeliveryDate"},
                            {data: "remarks"},
                            {data: "status"},
                            {data: "businessType"},
                            {data: "countryId"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }
                else {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2SsdId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2SsdId"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "armstrong2CustomersId"},
                            {data: "armstrong2CustomersName"},
                            {data: "armstrong1DistributorsId"},
                            {data: "armstrong2DistributorsId"},
                            {data: "armstrong2DistributorsName"},
                            {data: "armstrong1WholesalersId"},
                            {data: "armstrong2WholesalersId"},
                            {data: "armstrong2WholesalersName"},
                            {data: "armstrong2CallRecordsId"},
                            {data: "skuNumber"},
                            {data: "skuName"},
                            {data: "pakingSize"},
                            {data: "qtyCases"},
                            {data: "qtyPcs"},
                            {data: "freeQtyCases"},
                            {data: "freeQtyPcs"},
                            {
                                data: "totalPrice",
                                render: function (data, type, row) {
                                    if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                        return data.toFixed(2);
                                    }
                                    return data;
                                }
                            },
                            {data: "invoiceNumber"},
                            {data: "method"},
                            {data: "ssdMonth"},
                            {data: "ssdYear"},
                            {data: "stSsdDate"},
                            {data: "stDateCreated"},
                            {data: "stDeliveryDate"},
                            {data: "remarks"},
                            {data: "newSku"},
                            {data: "status"},
                            {data: "businessType"},
                            {data: "countryId"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataSsdReprotTw(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_ssd';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = [
                    'armstrong_2_ssd_id', 'armstrong_2_salespersons_id', 'salespersons', 'sales_hierarchy', 'armstrong_2_customers_id',
                    'customers', 'armstrong_1_distributors_id', 'armstrong_1_distributors_name',
                    'armstrong_2_distributors_id', 'armstrong_2_distributors_name', 'armstrong_1_wholesalers_id',
                    'armstrong_1_wholesalers_name', 'armstrong_2_wholesalers_id', 'armstrong_2_wholesalers_name',
                    'armstrong_2_call_records_id', 'customer_type', 'sku_number', 'sku_name', 'packing_size', 'qty_cases',
                    'qty_pcs', 'free_qty_cases', 'free_qty_pcs', 'total_price', 'price_per_case',
                    'invoice_number', 'method', 'ssd_month', 'ssd_year', 'ssd_date', 'date_created',
                    'delivery_date', 'remarks', 'status', 'business_type', 'country_id'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SsdId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SsdId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "salesHierarchy"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong1DistributorsId"},
                        {data: "armstrong1DistributorsName"},
                        {data: "armstrong2DistributorsId"},
                        {data: "armstrong2DistributorsName"},
                        {data: "armstrong1WholesalersId"},
                        {data: "armstrong1WholesalersName"},
                        {data: "armstrong2WholesalersId"},
                        {data: "armstrong2WholesalersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "customerType"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "pakingSize"},
                        {data: "qtyCases"},
                        {data: "qtyPcs"},
                        {data: "freeQtyCases"},
                        {data: "freeQtyPcs"},
                        {
                            data: "totalPrice",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "pricePerCase"},
                        {data: "invoiceNumber"},
                        {data: "method"},
                        {data: "ssdMonth"},
                        {data: "ssdYear"},
                        {data: "stSsdDate"},
                        {data: "stDateCreated"},
                        {data: "stDeliveryDate"},
                        {data: "remarks"},
                        {data: "status"},
                        {data: "businessType"},
                        {data: "countryId"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataTfoAll(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_tfo';

    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr;
                if (countryId == 4 || countryId == 5 || countryId == 6 || countryId == 7) {
                    arr = ['AS2 TFO ID',
                        'AS2 Customer ID', 'Customer Name', 'Global Channel', 'Country Channel', 'AS2 Salesperson ID',
                        'Salesperson', 'AS2 Distributors ID', 'Distributors Name', 'Distributor Group', 'AS2 Wholesalers ID',
                        'Wholesalers Name', 'AS2 Call Records ID', 'SKU number', 'SKU name', 'Packing Size',
                        'Top Ten', 'Qty case', 'Qty pcs', 'Free cases', 'Free pieces',
                        'Total', 'Method', 'Date created', 'Time Created', 'Delivery date', 'Delivery Time', 'Country Id',
                        'Business Type', 'Customer Type', 'Remarks', 'Signature', 'Email'
                    ];
                } else {
                    arr = ['AS2 TFO ID',
                        'AS2 Customer ID', 'Customer Name', 'Global Channel', 'Country Channel', 'AS2 Salesperson ID',
                        'Salesperson', 'AS2 Distributors ID', 'Distributors Name', 'Distributor Group', 'AS2 Wholesalers ID',
                        'Wholesalers Name', 'AS2 Call Records ID', 'SKU number', 'SKU name', 'Packing Size',
                        'Top Ten', 'Qty case', 'Qty pcs', 'Free cases', 'Free pieces',
                        'New SKU', 'Total', 'Method', 'Date created', 'Time Created', 'Delivery date', 'Delivery Time', 'Country Id',
                        'Business Type', 'Customer Type', 'Remarks', 'Signature', 'Email'
                    ];
                }

                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;

                var t;
                if (countryId == 4 || countryId == 5 || countryId == 6 || countryId == 7) {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2TfoId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2TfoId"},
                            {data: "armstrong2CustomersId"},
                            {data: "customersName"},
                            {data: "globalChannel"},
                            {data: "countryChannel"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "armstrong2Distributors_id"},
                            {data: "distributorsName"},
                            {data: "distributorGroup"},
                            {data: "armstrong2WholesalersId"},
                            {data: "wholesalersName"},
                            {data: "armstrong2CallRecordsId"},
                            {data: "skuNumber"},
                            {data: "skuName"},
                            {data: "packingSize"},
                            {data: "isTopTen"},
                            {data: "qtyCase"},
                            {data: "qtyPcs"},
                            {data: "freeCase"},
                            {data: "freePcs"},
                            {data: "total"},
                            {data: "method"},
                            {data: "dateCreated"},
                            {data: "timeCreated"},
                            {data: "deliveryDate"},
                            {data: "deliveryTime"},
                            {data: "countryId"},
                            {data: "businessTypel"},
                            {data: "customerType"},
                            {data: "remarks"},
                            {data: "signature"},
                            {data: "email"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }
                else {
                    t = $('#example').dataTable({
                        data: result.listData,
                        columns: [
                            {
                                data: "armstrong2TfoId",
                                render: function (data, type, row) {
                                    if (type === 'display' || type === 'filter') {
                                        return i++;
                                    }
                                    return data;
                                }
                            },
                            {data: "armstrong2TfoId"},
                            {data: "armstrong2CustomersId"},
                            {data: "customersName"},
                            {data: "globalChannel"},
                            {data: "countryChannel"},
                            {data: "armstrong2SalespersonsId"},
                            {data: "salespersonsName"},
                            {data: "armstrong2Distributors_id"},
                            {data: "distributorsName"},
                            {data: "distributorGroup"},
                            {data: "armstrong2WholesalersId"},
                            {data: "wholesalersName"},
                            {data: "armstrong2CallRecordsId"},
                            {data: "skuNumber"},
                            {data: "skuName"},
                            {data: "packingSize"},
                            {data: "isTopTen"},
                            {data: "qtyCase"},
                            {data: "qtyPcs"},
                            {data: "freeCase"},
                            {data: "freePcs"},
                            {data: "newSku"},
                            {data: "total"},
                            {data: "method"},
                            {data: "dateCreated"},
                            {data: "timeCreated"},
                            {data: "deliveryDate"},
                            {data: "deliveryTime"},
                            {data: "countryId"},
                            {data: "businessTypel"},
                            {data: "customerType"},
                            {data: "remarks"},
                            {data: "signature"},
                            {data: "email"}
                        ],
                        fnDrawCallback: function (oSettings) {
                            if (oSettings.bSorted || oSettings.bFiltered) {
                                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                                }
                            }
                        },
                        aoColumnDefs: [
                            {"bSortable": false, "aTargets": [0]}
                        ],
                        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                    });
                }

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function dataTfoHk(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_tfo';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 TFO ID',
                    'AS2 Customer ID', 'Customer Name', 'Global Channel', 'Country Channel', 'AS2 Salesperson ID',
                    'Salesperson', 'AS2 Distributors ID', 'Distributors Name', 'Distributor Group', 'AS2 Wholesalers ID',
                    'Wholesalers Name', 'AS2 Call Records ID', 'SKU number', 'SKU name', 'Packing Size', "Unit Price",
                    'Top Ten', 'Qty case', 'Qty pcs', 'Free cases', 'Free pieces',
                    'Total', 'Method', 'Date created', 'Time Created', 'Delivery date', 'Delivery Time', 'Country Id',
                    'Business Type', 'Customer Type', 'Remarks', 'Signature'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2TfoId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2TfoId"},
                        {data: "armstrong2CustomersId"},
                        {data: "customersName"},
                        {data: "globalChannel"},
                        {data: "countryChannel"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2Distributors_id"},
                        {data: "distributorsName"},
                        {data: "distributorGroup"},
                        {data: "armstrong2WholesalersId"},
                        {data: "wholesalersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "packingSize"},
                        {
                            data: "unitPrice",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "isTopTen"},
                        {data: "qtyCase"},
                        {data: "qtyPcs"},
                        {data: "freeCase"},
                        {data: "freePcs"},
                        {
                            data: "total",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "method"},
                        {data: "dateCreated"},
                        {data: "timeCreated"},
                        {data: "deliveryDate"},
                        {data: "deliveryTime"},
                        {data: "countryId"},
                        {data: "businessTypel"},
                        {data: "customerType"},
                        {data: "remarks"},
                        {data: "signature"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}


function dataTfoTw(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_tfo';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_2_tfo_id',
                    'armstrong_2_customers_id', 'customers_name', 'armstrong_2_salespersons_id',
                    'salespersons_name', 'armstrong_2_distributors_id', 'distributors_name',
                    'armstrong_2_wholesalers_id', 'wholesalers_name', 'armstrong_2_call_records_id',
                    'sku_number', 'sku_name', 'packing_size', 'is_top_ten', 'qty_case', 'qty_pcs', 'free_case',
                    'free_pcs', 'total', 'price_per_case', 'method', 'date_created',
                    'time_created', 'delivery_date', 'delivery_time', 'country_id', 'business_type',
                    'remarks', 'signature', 'customer_type', 'sales_hierarchy'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2TfoId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2TfoId"},
                        {data: "armstrong2CustomersId"},
                        {data: "customersName"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2Distributors_id"},
                        {data: "distributorsName"},
                        {data: "armstrong2WholesalersId"},
                        {data: "wholesalersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "packingSize"},
                        {data: "isTopTen"},
                        {data: "qtyCase"},
                        {data: "qtyPcs"},
                        {data: "freeCase"},
                        {data: "freePcs"},
                        {
                            data: "total",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "pricePerCase"},
                        {data: "method"},
                        {data: "dateCreated"},
                        {data: "timeCreated"},
                        {data: "deliveryDate"},
                        {data: "deliveryTime"},
                        {data: "countryId"},
                        {data: "businessTypel"},
                        {data: "remarks"},
                        {data: "signature"},
                        {data: "customerType"},
                        {data: "salesHierarchy"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function loadDataCall(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_call';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Call Records ID',
                    'AS2 Route Plan ID', 'AS2 Salespersons ID', 'Salesperson Name', 'AS2 Customers ID', 'Customers name',
                    'Country Channel', 'Otm', 'Date', 'Time Call Start', 'Time Call End',
                    'Call Length', 'Call assessment', 'Customer type', 'Month Week', 'Call type', "Notes"];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2CallRecordsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2RoutePlanId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "countryChannel"},
                        {data: "otm"},
                        {data: "date"},
                        {data: "timeCallStart"},
                        {data: "timeCallEnd"},
                        {data: "callLength"},
                        {data: "callAssessment"},
                        {data: "customerType"},
                        {data: "monthWeek"},
                        {data: "makeByMaster"},
                        {data: "notes"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataOtm(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/data_otm';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'OTM', 'Date Visited'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "otmValue"},
                        {data: "sDateVisited"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function newCustomerReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/new_customer_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Approved', 'Date Created'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "approved"},
                        {data: "stDateCreated"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function topTenPenetrationCalculationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/top_ten_penetration_calculation_tr';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Target Penetration Rate', 'No.of top10 Country',
                    'No. of top10 SKU bought by customer', 'Pentrated Value', 'Penetrated'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "targetRate"},
                        {data: "noOfTop10Country"},
                        {data: "noT10SkuBoughtByCustomer"},
                        {data: "pentratedValue"},
                        {data: "isPentrated"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function newGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/new_grip_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Country channel'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "countryChannel"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function newGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/new_grab_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'TFO Id', 'SKU Id',
                    'SKU Name', 'Packing Size', 'Quantity', 'Total value'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2TfoId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "pakingSize"},
                        {data: "quantity"},
                        {
                            data: "totalValue",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        }
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function lostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/lost_grip_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Date of the lost of grip'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "stLastDateOder"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function lostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/lost_grab_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Lost SKU ID', 'Lost sku name',
                    'Packing Size', 'Last date of purchase'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "pakingSize"},
                        {data: "stLastDateOder"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function potentialLostGripReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/potential_lost_grip_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'Date of the lost of grip'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "stLastDateOder"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function potentialLostGrabReport(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/potential_lost_grab_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'SKU ID', 'SKU name',
                    'Packing Size', 'Number of Days of the last bought', 'Last date of purchase'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "pakingSize"},
                        {data: "daysBetween"},
                        {data: "stLastDateOder"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function topTenPenetrationTrReport(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/top_ten_penetration_tr';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['AS2 Salespersons ID',
                    'Salesperson Name', 'AS2 Customers ID', 'Customers Name', 'AS2 Call Records ID', 'AS2 Tfo ID',
                    'SKU number', 'SKU name', 'Quantity', 'Value', 'Data source',
                    'Date ceate'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "armstrong2TfoId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "quantity"},
                        {
                            data: "value",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "dataSource"},
                        {data: "stDateCreated"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}


function competitorReprot(urlGetReport, sp, fromdate, todate, countryId, getall, appType, active, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/competitor_reprot';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            app_type: appType,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Armstrong 2 Salesperson ID',
                    'Armstrong 2 Salesperson Name', 'Armstrong 2 customers ID', 'Armstrong 2 customers name', 'Arstrong 2 Call Records ID', 'Brand',
                    'Product Name', 'Quantity', 'Last Attempt Date', 'Date Created', 'Responses'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "brand"},
                        {data: "productName"},
                        {data: "quantity"},
                        {data: "stLastAttempt"},
                        {data: "stDateCreated"},
                        {data: "response"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function objectiveRecordData(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/objective_record_data';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';

                html += '<div class="tabbable"><ul class="nav nav-tabs">';
                html += ' <li class="active"><a href="#personalObjective" data-toggle="tab">Personal Objective</a></li>';
                html += '<li><a href="#countryObjective" data-toggle="tab">Country Objective</a></li>';
                html += '<li><a href="#countryChannelObjective" data-toggle="tab">Country Channel Objective</a></li>';
                html += '</ul> <div class="tab-content">';
                html += '<div class="tab-pane active" id="personalObjective"></div>';
                html += ' <div class="tab-pane" id="countryObjective"></div>';
                html += '<div class="tab-pane" id="countryChannelObjective"></div>';
                html += '</div></div>';

                $('#reporting-view').html(html);
                personalObjective(result.data.listPersonalObjective);
                countryObjective(result.data.listCountryObjective);
                countryChannelObjective(result.data.listCountryChannelObjective);

            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function countryChannelObjective(dataObjective) {
    var html = '';
    var arr = ['Armstrong 2 Objective Records', 'Description',
        'Armstrong 2 Call ID', 'Salesperson ID', 'Salesperson Name', 'Armstrong 2 Customer ID', 'Armstrong 2 Customer Name',
        'Status', 'Reason'
    ];
    html += '<table class="table table-report display" id="countryChannelObjectiveTable" >';
    html += '<thead><tr><th>#</th>';
    for (var i = 0; i < arr.length; i++) {
        html += '<th>' + arr[i] + '</th>';
    }
    ;
    html += '</tr></thead>';
    html += '</table>';
    $('#countryChannelObjective').html(html);
    var i = 1;
    var t = $('#countryChannelObjectiveTable').dataTable({
        data: dataObjective,
        columns: [
            {
                data: "armstrong2ObjectiveRecordsId",
                render: function (data, type, row) {
                    if (type === 'display' || type === 'filter') {
                        return i++;
                    }
                    return data;
                }
            },
            {data: "armstrong2ObjectiveRecordsId"},
            {data: "description"},
            {data: "armstrong2CallRecordsId"},
            {data: "armstrong2SalespersonsId"},
            {data: "salespersonsName"},
            {data: "armstrong2CustomersId"},
            {data: "armstrong2CustomersName"},
            {data: "status"},
            {data: "reason"}
        ],
        fnDrawCallback: function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        aoColumnDefs: [
            {"bSortable": false, "aTargets": [0]}
        ],
        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
    });
}

function countryObjective(dataObjective) {
    var html = '';
    var arr = ['Armstrong 2 Objective Records', 'Description',
        'Armstrong 2 Call ID', 'Salesperson ID', 'Salesperson Name', 'Armstrong 2 Customer ID', 'Armstrong 2 Customer Name',
        'Status', 'Reason'
    ];
    html += '<table class="table table-report display" id="countryObjectiveTable" >';
    html += '<thead><tr><th>#</th>';
    for (var i = 0; i < arr.length; i++) {
        html += '<th>' + arr[i] + '</th>';
    }
    ;
    html += '</tr></thead>';
    html += '</table>';
    $('#countryObjective').html(html);
    var i = 1;
    var t = $('#countryObjectiveTable').dataTable({
        data: dataObjective,
        columns: [
            {
                data: "armstrong2ObjectiveRecordsId",
                render: function (data, type, row) {
                    if (type === 'display' || type === 'filter') {
                        return i++;
                    }
                    return data;
                }
            },
            {data: "armstrong2ObjectiveRecordsId"},
            {data: "description"},
            {data: "armstrong2CallRecordsId"},
            {data: "armstrong2SalespersonsId"},
            {data: "salespersonsName"},
            {data: "armstrong2CustomersId"},
            {data: "armstrong2CustomersName"},
            {data: "status"},
            {data: "reason"}
        ],
        fnDrawCallback: function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        aoColumnDefs: [
            {"bSortable": false, "aTargets": [0]}
        ],
        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
    });
}
function personalObjective(dataObjective) {

    var html = '';
    var arr = ['Armstrong 2 Objective ID', 'Description',
        'Armstrong 2 Call ID', 'Salesperson ID', 'Salesperson Name', 'Armstrong 2 Customer ID', 'Armstrong 2 Customer Name',
        'Date created'
    ];
    html += '<table class="table table-report display" id="personalObjectiveTable" >';
    html += '<thead><tr><th>#</th>';
    for (var i = 0; i < arr.length; i++) {
        html += '<th>' + arr[i] + '</th>';
    }
    ;
    html += '</tr></thead>';
    html += '</table>';
    $('#personalObjective').html(html);
    var i = 1;
    var t = $('#personalObjectiveTable').dataTable({
        data: dataObjective,
        columns: [
            {
                data: "armstrong2ObjectiveRecordsId",
                render: function (data, type, row) {
                    if (type === 'display' || type === 'filter') {
                        return i++;
                    }
                    return data;
                }
            },
            {data: "armstrong2ObjectiveRecordsId"},
            {data: "description"},
            {data: "armstrong2CallRecordsId"},
            {data: "armstrong2SalespersonsId"},
            {data: "salespersonsName"},
            {data: "armstrong2CustomersId"},
            {data: "armstrong2CustomersName"},
            {data: "stDateCreated"}
        ],
        fnDrawCallback: function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },
        aoColumnDefs: [
            {"bSortable": false, "aTargets": [0]}
        ],
        lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
    });
}

function activitiesLogData(urlGetReport, sp, fromdate, todate, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/activities_log_data';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Id',
                    'Salesperson ID', 'Salesperson Name', 'Event Name', 'Event Message', 'Object Name',
                    'New Data', 'Old Data', 'Date Start', 'Date End', 'Country Id',
                    'Ipad Str', 'Version'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "id",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "id"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "eventName"},
                        {data: "eventMessage"},
                        {data: "objectName"},
                        {data: "newData"},
                        {data: "oldData"},
                        {data: "stDateStart"},
                        {data: "stDateEnd"},
                        {data: "countryId"},
                        {data: "ipadStr"},
                        {data: "version"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function productData(urlGetReport, countryId, productActive, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/product_data';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            country: countryId,
            productActive: productActive
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_products_id',
                    'sku_number', 'sku_name', 'name_alt', 'description', 'groups_id',
                    'groups_name', 'ingredients', 'dish_application', 'price', 'barcode',
                    'quantity_case', 'weight_pc', 'packing_size', 'brands_id', 'brands_name',
                    'products_cat_web_id', 'products_cat_web_name', 'glob_pro_hierarchy', 'categories_name', 'markets_name',
                    'sectors_name', 'sub_sectors_name', 'segments_name', 'shelf_life', 'pro_claims',
                    'is_top_ten', 'total_recipes_use_product', 'cuisine_channels_id', 'cuisine_channels_name', 'country',
                    'nutrition_name', 'listed', 'date_created', 'last_updated'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1ProductsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1ProductsId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "nameAlt"},
                        {data: "description"},
                        {data: "groupsId"},
                        {data: "groupsName"},
                        {data: "ingredients"},
                        {data: "dishApplication"},
                        {
                            data: "price",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "barcode"},
                        {data: "quantityCase"},
                        {data: "weightPc"},
                        {data: "pakingSize"},
                        {data: "brandsId"},
                        {data: "brandsName"},
                        {data: "productsCatWebId"},
                        {data: "productsCatWebName"},
                        {data: "globProHierarchy"},
                        {data: "categoriesName"},
                        {data: "marketsName"},
                        {data: "sectorsName"},
                        {data: "subSectorsName"},
                        {data: "segmentsName"},
                        {data: "shelfLife"},
                        {data: "proClaims"},
                        {data: "topTen"},
                        {data: "totalRecipesUse"},
                        {data: "cuisineChannelsId"},
                        {data: "cuisineChannelsName"},
                        {data: "countryName"},
                        {data: "nutritionName"},
                        {data: "sListed"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "lastUpdated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        }
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[10, 50, 100, 1000, -1], [10, 50, 100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function dataRoutePlan(urlGetReport, sp, countryId, getall, fromdate, todate, appType, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/data_route_plan';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall,
            from_date: fromdate,
            to_date: todate,
            app_type: appType,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_2_route_plan_id',
                    'armstrong_2_salespersons_id', 'salespersons Name', 'armstrong_2_customers_id', 'planned_day', 'planned_date', 'date_created',
                    'order', 'route_status_id', 'shifted_date', 'last_updated',
                    'version'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2RoutePlanId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2RoutePlanId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "armstrong2CustomersId"},
                        {data: "plannedDay"},
                        {data: "stPlannedDate"},
                        {data: "stDateCreated"},
                        {data: "order"},
                        {data: "routeStatusId"},
                        {data: "stShiftedDate"},
                        {data: "stLastUpdated"},
                        {data: "version"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function salespersonsData(urlGetReport, sp, countryId, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/salespersons';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_salespersons_id',
                    'armstrong_2_salespersons_id', 'first_name', 'last_name', 'email', 'alias',
                    'roles_id', 'roles_name', 'type', 'country_id', 'country_name',
                    'admins_id', 'territory', 'handphone', 'did', 'date_created',
                    'last_updated', 'salespersons_manager_id', 'salespersons_manager_name'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1SalespersonsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1SalespersonsId"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "firstName"},
                        {data: "lastName"},
                        {data: "email"},
                        {data: "alias"},
                        {data: "rolesId"},
                        {data: "roleName"},
                        {data: "type"},
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "adminsId"},
                        {data: "territory"},
                        {data: "handphone"},
                        {data: "did"},
                        {data: "stDateCreated"},
                        {data: "stLastUpdated"},
                        {data: "salespersonsManagerId"},
                        {data: "salespersonsManagerName"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function regionReport(urlGetReport, sp, countryId, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/region';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Country Id',
                    'Country Name', 'Region', 'Satate', 'District', 'Suburb',
                    'City'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "countryId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "region"},
                        {data: "state"},
                        {data: "district"},
                        {data: "suburb"},
                        {data: "city"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function ssData(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/ssd';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Id',
                    'SsdId', 'SalespersonsId', 'Salespersons Name', 'Customers Id', 'Customers Name',
                    'Distributors Id', 'Distributors Name', 'Wholesalers Id', 'Wholesalers Name', 'Call Records Id',
                    'Sku Number', 'Sku Name', 'Packing Size', 'Qty Cases', 'Qty Pcs',
                    'Free Cases', 'Free Pcs', 'Total Price', 'Invoice Number', 'Method',
                    'Ssd Month', 'Ssd Year', 'Ssd Date', 'Date Created', 'Delivery Date',
                    'Remarks', 'New Sku', 'Status', 'Country Id', 'Country Name'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "id",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "id"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersons"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2DistributorsId"},
                        {data: "armstrong2DistributorsName"},
                        {data: "armstrong2WholesalersId"},
                        {data: "armstrong2WholesalersName"},
                        {data: "armstrong2CallRecordsId"},
                        {data: "skuNumber"},
                        {data: "skuName"},
                        {data: "pakingSize"},
                        {data: "qtyCases"},
                        {data: "qtyPcs"},
                        {data: "freeQtyCases"},
                        {data: "freeQtyPcs"},
                        {
                            data: "totalPrice",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "invoiceNumber"},
                        {data: "sMethod"},
                        {data: "ssdMonth"},
                        {data: "ssdYear"},
                        {
                            data: "ssdDate",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "deliveryDate",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "remarks"},
                        {data: "newSku"},
                        {data: "status"},
                        {data: "countryId"},
                        {data: "countryName"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataDistributors(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/distributors';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            country: countryId,
            getall: getall,
            spId: sp,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_distributors_id',
                    'armstrong_2_distributors_id', 'name', 'groups_id', 'groups_name', 'asst_manager',
                    'armstrong_2_salespersons_id', 'salespersons_name', 'ssd_id', 'ssd_name', 'street_address',
                    'city', 'postal_code', 'country_id', 'country_name', 'phone',
                    'phone_2', 'email', 'suburb', 'region', 'province',
                    'fax', 'update_info', 'log_ids', 'active', 'country_channels_id',
                    'country_channels_name', 'country_sub_channels_id', 'country_sub_channels_name', 'business_types_id', 'business_types_name',
                    'assigned_distributor_1', 'assigned_distributor_1_name', 'assigned_distributor_2', 'assigned_distributor_2_name', 'contact_details',
                    'date_created', 'last_updated'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1DistributorsId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1DistributorsId"},
                        {data: "armstrong2DistributorsId"},
                        {data: "name"},
                        {data: "groupsId"},
                        {data: "groupsName"},
                        {data: "asstManager"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "ssdId"},
                        {data: "sapCustName"},
                        {data: "streetAddress"},
                        {data: "city"},
                        {data: "postalCode"},
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "phone"},
                        {data: "phone2"},
                        {data: "email"},
                        {data: "suburb"},
                        {data: "region"},
                        {data: "province"},
                        {data: "fax"},
                        {data: "updateInfo"},
                        {data: "logIds"},
                        {data: "sActive"},
                        {data: "countryChannelsId"},
                        {data: "countryChannelsName"},
                        {data: "countrySubChannelsId"},
                        {data: "countrySubChannelsName"},
                        {data: "businessTypesId"},
                        {data: "businessTypesName"},
                        {data: "assignedDistributor1"},
                        {data: "assignedDistributor1Name"},
                        {data: "assignedDistributor2"},
                        {data: "assignedDistributor2Name"},
                        {data: "contactDetails"},
                        {data: "stDateCreated"},
                        {data: "stLastUpdated"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataWholesalers(urlGetReport, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/wholesalers';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            country: countryId,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_wholesalers_id',
                    'armstrong_2_wholesalers_id', 'name', 'armstrong_2_distributors_id', 'distributors_Name', 'armstrong_2_salespersons_id',
                    'salespersons_name', 'street_address', 'class_code', 'class', 'district',
                    'city', 'postal_code', 'region', 'phone', 'phone_2',
                    'email', 'fax', 'country_id', 'country_name', 'update_info',
                    'country_channels_id', 'country_channels_name', 'country_sub_channels_id', 'country_sub_channels_name', 'business_types_id',
                    'business_types_name', 'assigned_distributor_1', 'assigned_distributor_1_name', 'assigned_distributor_2', 'assigned_distributor_2_name',
                    'contact_details', 'active', 'date_created', 'last_updated', "ssd_id", "ssd_name"
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1WholesalersId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1WholesalersId"},
                        {data: "armstrong2WholesalersId"},
                        {data: "name"},
                        {data: "armstrong2DistributorsId"},
                        {data: "armstrong2DistributorsName"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersonsName"},
                        {data: "streetAddress"},
                        {data: "classCode"},
                        {data: "class1"},
                        {data: "district"},
                        {data: "city"},
                        {data: "postalCode"},
                        {data: "region"},
                        {data: "phone"},
                        {data: "phone2"},
                        {data: "email"},
                        {data: "fax"},
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "updateInfo"},
                        {data: "countryChannelsId"},
                        {data: "countryChannelsName"},
                        {data: "countrySubChannelsId"},
                        {data: "countrySubChannelsName"},
                        {data: "businessTypesId"},
                        {data: "businessTypesName"},
                        {data: "assignedDistributor1"},
                        {data: "assignedDistributor1Name"},
                        {data: "assignedDistributor2"},
                        {data: "assignedDistributor2Name"},
                        {data: "contactDetails"},
                        {data: "sActive"},
                        {data: "stDateCreated"},
                        {data: "stLastUpdated"},
                        {data: "ssdId"},
                        {data: "sapCustName"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataCustomersReprotAll(urlGetReport, sp, fromdate, todate, countryId, getall, fromdate, todate, app_type, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/data_customers';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            from_date: fromdate,
            to_date: todate,
            app_type: appType
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_customers_id',
                    'armstrong_2_customers_id', 'armstrong_2_customers_name', 'armstrong_2_salespersons_id', 'salespersons', 'ssd_id',
                    'ssd_id_2', 'sap_cust_name', 'sap_cust_name_2', 'supplier_type', 'supplier_id',
                    'street_address', 'postal_code', 'phone', 'phone_two', 'email',
                    'email_two', 'fax', 'web_url', 'acct_opened', 'global_channels_id',
                    'global_channels_name', 'channel_id', 'channel_name', 'channel_weightage', 'sub_channel',
                    'sub_channel_name', 'otm', 'country_id', 'country_name', 'cuisine_channels_id',
                    'region', 'state', 'district', 'suburb', 'key_acct',
                    'business_type', 'business_type_name', 'kosher_type', 'primary_supplier_id', 'primary_supplier_name',
                    'secondary_supplier_id', 'secondary_supplier_name', 'trading_days_per_yr', 'trading_wks_per_yr', 'turnover',
                    'capacity', 'no_of_outlets', 'convenience_lvl', 'meals_per_day', 'selling_price',
                    'budget_price', 'food_cost', 'best_time_to_call', 'notes', 'latitude',
                    'longitude', 'date_created', 'last_updated', 'display_name', 'registered_name',
                    'opening_hours', 'active', 'update_info', 'operator_channels', 'city',
                    'ufs_share', 'customers_types_id', 'customers_types_name', 'serving_types_id', 'serving_types_name',
                    'channel_groups_id', 'channel_groups_name', 'cuisine_groups_id', 'cuisine_groups_name', 'approved',
                    'approved_message', 'approved_compare', 'status'
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1CustomersId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1CustomersId"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersons"},
                        {data: "ssdId"},
                        {data: "ssdId2"},
                        {data: "sapCustName"},
                        {data: "sapCustName2"},
                        {data: "supplierType"},
                        {data: "supplierId"},
                        {data: "streetAddress"},
                        {data: "postalCode"},
                        {data: "phone"},
                        {data: "phoneTwo"},
                        {data: "email"},
                        {data: "emailTwo"},
                        {data: "fax"},
                        {data: "webUrl"},
                        {
                            data: "acctOpened",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "globalChannelsId"},
                        {data: "globalChannelsName"},
                        {data: "channel"},
                        {data: "channelName"},
                        {
                            data: "channelWeightage",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "subChannel"},
                        {data: "subChannelsName"},
                        {data: "otm"},
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "cuisineChannelsId"},
                        {data: "region"},
                        {data: "state"},
                        {data: "district"},
                        {data: "suburb"},
                        {data: "keyAcct"},
                        {data: "businessType"},
                        {data: "businessTypeName"},
                        {data: "kosherType"},
                        {data: "primarySupplier"},
                        {data: "primarySupplierName"},
                        {data: "secondarySupplier"},
                        {data: "secondarySupplierName"},
                        {data: "tradingDaysPerYr"},
                        {data: "tradingWksPerYr"},
                        {data: "turnover"},
                        {data: "capacity"},
                        {data: "noOfOutlets"},
                        {data: "convenienceLvl"},
                        {data: "mealsPerDay"},
                        {data: "sellingPrice"},
                        {data: "budgetPrice"},
                        {data: "foodCost"},
                        {data: "bestTimeToCall"},
                        {data: "notes"},
                        {data: "latitude"},
                        {data: "longitude"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "lastUpdated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "displayName"},
                        {data: "registeredName"},
                        {data: "openingHours"},
                        {data: "active"},
                        {data: "updateInfo"},
                        {data: "operatorChannels"},
                        {data: "city"},
                        {
                            data: "ufsShare",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "customersTypesId"},
                        {data: "customersTypesName"},
                        {data: "servingTypesId"},
                        {data: "servingTypesName"},
                        {data: "channelGroupsId"},
                        {data: "channelGroupsName"},
                        {data: "cuisineGroupsId"},
                        {data: "cuisineGroupsName"},
                        {data: "approved"},
                        {data: "approvedMessage"},
                        {data: "approvedCompare"},
                        {data: "status"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function dataCustomersReprotTw(urlGetReport, sp, countryId, getall, fromdate, todate, app_type, isMultiCountry, regional) {
    var urlget = urlGetReport + 'masterdata/data_customers';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        timeout: 0,
        url: urlget,
        data: {
            spId: sp,
            country: countryId,
            getall: getall,
            from_date: fromdate,
            to_date: todate,
            app_type: appType
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['armstrong_1_customers_id',
                    'armstrong_2_customers_id', 'armstrong_2_customers_name', 'armstrong_2_salespersons_id', 'salespersons', 'ssd_id',
                    'ssd_id_2', 'sap_cust_name', 'sap_cust_name_2', 'supplier_type', 'supplier_id',
                    'street_address', 'postal_code', 'phone', 'phone_two', 'email',
                    'email_two', 'fax', 'web_url', 'acct_opened', 'global_channels_id',
                    'global_channels_name', 'channel_id', 'channel_name', 'channel_weightage', 'sub_channel',
                    'sub_channel_name', 'otm', 'country_id', 'country_name', 'cuisine_channels_id',
                    'region', 'state', 'district', 'suburb', 'key_acct',
                    'business_type', 'business_type_name', 'kosher_type', 'primary_supplier_id', 'primary_supplier_name',
                    'secondary_supplier_id', 'secondary_supplier_name', 'trading_days_per_yr', 'trading_wks_per_yr', 'turnover',
                    'capacity', 'no_of_outlets', 'convenience_lvl', 'meals_per_day', 'selling_price',
                    'budget_price', 'food_cost', 'best_time_to_call', 'notes', 'latitude',
                    'longitude', 'date_created', 'last_updated', 'display_name', 'registered_name',
                    'opening_hours', 'active', 'update_info', 'operator_channels', 'city',
                    'ufs_share', 'customers_types_id', 'customers_types_name', 'serving_types_id', 'serving_types_name',
                    'channel_groups_id', 'channel_groups_name', 'cuisine_groups_id', 'cuisine_groups_name', 'approved',
                    'approved_message', 'approved_compare', 'contacts_id', 'contacts_name', 'status'

                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    html += '<th>' + arr[i] + '</th>';
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong1CustomersId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong1CustomersId"},
                        {data: "armstrong2CustomersId"},
                        {data: "armstrong2CustomersName"},
                        {data: "armstrong2SalespersonsId"},
                        {data: "salespersons"},
                        {data: "ssdId"},
                        {data: "ssdId2"},
                        {data: "sapCustName"},
                        {data: "sapCustName2"},
                        {data: "supplierType"},
                        {data: "supplierId"},
                        {data: "streetAddress"},
                        {data: "postalCode"},
                        {data: "phone"},
                        {data: "phoneTwo"},
                        {data: "email"},
                        {data: "emailTwo"},
                        {data: "fax"},
                        {data: "webUrl"},
                        {
                            data: "acctOpened",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "globalChannelsId"},
                        {data: "globalChannelsName"},
                        {data: "channel"},
                        {data: "channelName"},
                        {
                            data: "channelWeightage",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "subChannel"},
                        {data: "subChannelsName"},
                        {data: "otm"},
                        {data: "countryId"},
                        {data: "countryName"},
                        {data: "cuisineChannelsId"},
                        {data: "region"},
                        {data: "state"},
                        {data: "district"},
                        {data: "suburb"},
                        {data: "keyAcct"},
                        {data: "businessType"},
                        {data: "businessTypeName"},
                        {data: "kosherType"},
                        {data: "primarySupplier"},
                        {data: "primarySupplierName"},
                        {data: "secondarySupplier"},
                        {data: "secondarySupplierName"},
                        {data: "tradingDaysPerYr"},
                        {data: "tradingWksPerYr"},
                        {data: "turnover"},
                        {data: "capacity"},
                        {data: "noOfOutlets"},
                        {data: "convenienceLvl"},
                        {data: "mealsPerDay"},
                        {data: "sellingPrice"},
                        {data: "budgetPrice"},
                        {data: "foodCost"},
                        {data: "bestTimeToCall"},
                        {data: "notes"},
                        {data: "latitude"},
                        {data: "longitude"},
                        {
                            data: "dateCreated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {
                            data: "lastUpdated",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    var d = new Date(data);
                                    return formatDate(d, 'yyyy-MM-dd hh:mm:ss');
                                }
                                return data;
                            }
                        },
                        {data: "displayName"},
                        {data: "registeredName"},
                        {data: "openingHours"},
                        {data: "active"},
                        {data: "updateInfo"},
                        {data: "operatorChannels"},
                        {data: "city"},
                        {
                            data: "ufsShare",
                            render: function (data, type, row) {
                                if (isNotBlank(data) && (type === 'display' || type === 'filter')) {
                                    return data.toFixed(2);
                                }
                                return data;
                            }
                        },
                        {data: "customersTypesId"},
                        {data: "customersTypesName"},
                        {data: "servingTypesId"},
                        {data: "servingTypesName"},
                        {data: "channelGroupsId"},
                        {data: "channelGroupsName"},
                        {data: "cuisineGroupsId"},
                        {data: "cuisineGroupsName"},
                        {data: "approved"},
                        {data: "approvedMessage"},
                        {data: "approvedCompare"},
                        {data: "contactsId"},
                        {data: "contactsName"},
                        {data: "status"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

function showError() {
    //$('#reporting-view').html('<html><body><p style="font-size: 25px;font-weight: bolder; color: red;">The system have error. Please try later.</p></body></html>');
}
function isNotBlank(data) {
    return (data !== undefined) && (data !== null);
}

function generateParamHaveBetweenDate(sp, fromdate, todate, countryId, getall) {
    var dataSend = '';
    dataSend += 'from_date=' + fromdate;
    dataSend += '&to_date=' + todate;
    dataSend += '&country=' + countryId;
    dataSend += '&getall=' + getall;
    dataSend += '&' + generateParamSpId(sp);
    return dataSend;
}

function generateParamWitchMonthYear(sp, month, year, countryId, getall) {
    var dataSend = '';
    dataSend += 'year=' + year;
    dataSend += '&month=' + month;
    dataSend += '&country=' + countryId;
    dataSend += '&getall=' + getall;
    dataSend += '&' + generateParamSpId(sp);
    return dataSend;
}

function generateParamNotTime(sp, countryId, getall) {
    var dataSend = '';
    dataSend += 'country=' + countryId;
    dataSend += '&getall=' + getall;
    dataSend += '&' + generateParamSpId(sp);
    return dataSend;
}

function generateParamSpId(sp) {
    var dataSend = '';
    if ($.isArray(sp)) {
        for (var i = 0; i < sp.length; i++) {
            if (i == 0) {
                dataSend += 'spId=' + sp[i];
            } else {
                dataSend += '&spId=' + sp[i];
            }

        }
        ;
    } else {
        dataSend += 'spId=' + sp;
    }
    return dataSend;
}
