$(document).on('change', 'select[name="armstrong_2_related_mother_id"]', function(){
    var data = {type: $(this).data('type'), 'mother_id': $(this).val()};
    $.ajax({
        url: base_url()+'ami/chains/getChainByCus/',
        type: 'post',
        dataType: 'html',
        data: data,
        success: function(html)
        {
            if(html != '')
            {
                var optionchoose = $('select[name="armstrong_2_chains_id"] option[value="'+html+'"]');
                if(optionchoose.length > 0)
                {
                    $('select[name="armstrong_2_chains_id"] option[selected="selected"]').removeAttr('selected');
                    optionchoose.attr('selected', 'selected');
                    return false;
                }
            }
        }
    });
});
$(document).on('change', 'select[name="armstrong_2_chains_id"]', function(){
    var data = {type: $(this).data('type'), 'armstrong_2_chains_id': $(this).val()};
    $.ajax({
        url: base_url()+'ami/chains/getCusByChain/',
        type: 'post',
        dataType: 'json',
        data: data,
        success: function(json)
        {
            if(json)
            {
                for(x in json)
                {
                    if(json[x]['armstrong_2_customers_id'] != '')
                    {
                        var optionchoose = $('select[name="armstrong_2_related_mother_id"] option[value="'+json[x]['armstrong_2_customers_id']+'"]');
                        if(optionchoose.length > 0)
                        {
                            $('select[name="armstrong_2_chains_id"] option[selected="selected"]').removeAttr('selected');
                            optionchoose.attr('selected', 'selected');
                            return false;
                        }
                    }
                }
            }
        }
    });
});