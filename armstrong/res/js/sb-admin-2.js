$(function() {

    $('#side-menu').metisMenu();

    // console.log($(window).height());

    var menuHeight = $(window).height() - $('#sidebar-search').height() - $('.navbar').height();

    $('#side-menu').slimScroll({
        height: menuHeight
    });  

    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    // tooltip
    $("[data-toggle=tooltip], .Tooltip").tooltip();

    // popover
    $("[data-toggle=popover]").popover();
    $(document).on('click', '.popover-title .close', function (e) {
        var $target = $(e.target),
            $popover = $target.closest('.popover').prev();
        $popover && $popover.popover('hide');
    });

    // ajax modal
    $(document).on('click', '[data-toggle="ajaxModal"]', function (e) {
        e.preventDefault();
        $('#ajaxModal').remove();

        var $this = $(this),
            $remote = $this.data('remote') || $this.attr('href'),
            options = $this.data('options') || {},
            $modal = $('<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="ajaxModalLabel" aria-hidden="true"><div id="ajaxModalContent"></div></div>');

        $this.blur();
        $('body').append($modal);
        $modal.modal(options);
        $('#ajaxModalContent').load($remote);
    });

    if ($.fn.scrollToFixed)
    {
        $('#sidebar').scrollToFixed({
            minWidth: 768
        });

        $('.sticky').scrollToFixed({
            preFixed: function() { 
                if ($(this).hasClass('sticky-h1')) {
                    $(this).find('h1').addClass('text-primary'); 
                }
            },
            postFixed: function() { 
                if ($(this).hasClass('sticky-h1')) {
                    $(this).find('h1').removeClass('text-primary'); 
                }
            }
        });
    }


    $(document).on('click', '.clone-btn', function(e) {
        e.preventDefault();
        var $this = $(this),
            base = $this.data('clone') || false,
            baseCount = $this.data('count') || false
            maxCount = $this.data('maxcount') || false;         

        if (!base)
        {
            return;
        }

        if (maxCount)
        {
            if ($(document).find(base).length >= maxCount) {
                return;
            }
        }

        var $base = $(base).first(),
            $clone = $base.clone(),
            $new = $clone.clone(),
            nextCounter = $(document).find(base).length;//$base.length;

        $new.find('input[name]').each(function()
        {
            var $this = $(this);
            $this.attr('name', $this.attr('name').replace(/\[(\d+)\]\[/, '[' + nextCounter + ']['));
            $this.val('');
        });

        $new.find('select[name]').each(function()
        {
            var $this = $(this);
            $this.attr('name', $this.attr('name').replace(/\[(\d+)\]\[/, '[' + nextCounter + ']['));
            $this.val('');
        });

        // $new.find('.base-count').each(function()
        // {
        //     var $this = $(this),
        //         newCounter = parseInt($(document).find('.base-count').last().html());

        //     $this.html('').html(nextCounter + 1);
        // });

        $new.find('.remove-clone-btn').show();

        // $new.find('.clone-remove').empty().remove();
        $new.find('.clone-remove').each(function() {
            $(this).empty().remove();
        });       

        $new.insertAfter($(document).find(base).last());

        if (baseCount)
        {
            $(document).find(baseCount).each(function(index, item) {
                $(this).html('').html(index + 1);
            });
        }
    });

    $(document).on('click', '.remove-clone-btn', function(e) {
        e.preventDefault();
        var $this = $(this),
            base = $this.data('clone') || false;            

        if (!base)
        {
            return;
        }

        length = $(document).find(base).length;

        if (length <= 1)
        {
            return;
        }

        var $base = $this.closest(base);

        $base.find('.clone-move').each(function() {
            var $this = $(this),
                name = $this.data('name') || false,
                target = $this.data('target') || false;

            if (target)
            {
                if (name)
                {
                    $this.attr('name', name);
                }

                $(target).append($this);
            }
        });

        // $base.empty().remove();
        $base.html('').hide();
    });

    $(document).on('change', '.AutoInputTrigger', function(e) {
        e.preventDefault();

        var $this = $(this),
            url = $this.data('url') || false;

        if ($this.is('input'))
        {
            val = $this.val();
        }
        else
        {
            val = $this.find(":selected").val();
        }

        if (url)
        {
            $('input.AutoInput').val('');

            $.ajax({
                url: url,
                data: {id: val},
                dateType: 'json',
                success: function(res) {
                    $.each(res, function(key, value) {
                        // console.log(key);
                        var $input = $('input[name=' + key + ']');
                        if ($input.length && $input.hasClass('AutoInput'))
                        {
                            $input.val(value);
                        }
                    });
                }
            });
        }
    });


    CheckAll($('input.CheckAll, a.CheckAll, label.CheckAll'));

    $('form.submit-confirm').on('click', '.btn-sm.btn-danger', function(e) {
        e.preventDefault();
        var $this = $(this);
        bootbox.confirm("Please confirm deletion?", function(result) {
            if (result) {                
                $this.closest('form').submit();
            }
        });
    });
});

function CheckAll($control)
{
    $control.each(function() 
    {
        var $this = $(this);

        if ($this.is(':checkbox'))
        {
            var $target = $this.data('target') ? $($this.data('target')) : false,
                $description = $this.data('description') ? $($this.data('description')).hide() : false;

            if (!$target || !$target.length)
            {
                $target = $this.closest('form');
            }

            var getCheckBoxes = function()
            {
                var $checkboxes,
                    filter = $this.data('filter');

                $checkboxes = filter
                    ? $target.find(filter).filter('input:checkbox')
                    : $target.find('input:checkbox');        

                return $checkboxes;
            };

            var setSelectAllState = function()
            {
                var $checkboxes = getCheckBoxes(),
                    allSelected = $checkboxes.length > 0;

                $checkboxes.each(function() {
                    if ($(this).is($this))
                    {
                        return true;
                    }

                    if (!$(this).prop('checked'))
                    {
                        allSelected = false;
                        return false;
                    }
                });                

                $this.prop('checked', allSelected);                
            };
            setSelectAllState();

            var toggleAllRunning = false;

            $target.on('click', 'input:checkbox', function(e)
            {
                if (toggleAllRunning)
                {
                    return;
                }

                var $target = $(e.target);
                if ($target.is($this))
                {
                    return;
                }

                if ($this.data('filter'))
                {
                    if (!$target.closest($this.data('filter')).length)
                    {
                        return;
                    }
                }                

                setSelectAllState();
            });

            $target.on('change', 'input:checkbox', function(e)
            {
                if ($description && $description.length)
                {                    
                    var checked = false,
                        $checkboxes = getCheckBoxes();

                    $checkboxes.each(function() 
                    {
                        if ($(this).is(':checked'))
                        {
                            checked = true;
                        }
                    });

                    if (checked)
                    {
                        $description.show().removeClass('hide');
                    }
                    else
                    {
                        $description.hide().addClass('hide');
                    }
                }
            });

            $this.click(function(e)
            {
                if (toggleAllRunning)
                {
                    return;
                }

                toggleAllRunning = true;
                getCheckBoxes().prop('checked', e.target.checked).triggerHandler('click');
                toggleAllRunning = false;

                if ($description && $description.length)
                {
                    if ($(this).is(':checked'))
                    {
                        $description.show().removeClass('hide');
                    }
                    else
                    {
                        $description.hide().addClass('hide');
                    }
                }
            });
        }
        else
        {
            $this.click(function(e)
            {
                var target = $this.data('target');

                if (target)
                {
                    console.log($this)
                    $(target).prop('checked', true);                
                }
            });
        }
    });
}
