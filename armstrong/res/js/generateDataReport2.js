function getAssesmentReport(urlGetReport, fromdate, todate, active, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/assessment_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            active: active,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Salesperson ID', 'Salesperson Name', 'Research analysis and planning', 'Openning', 'Listening and Questioning',
                    'Matching and Presenting',
                    'Handling Objection', 'Colosing', 'Follow up and measurement', 'Notes in research analysis Planning',
                    'Principles in RAP',
                    'Principles in RAP 1',
                    'Principles in RAP 2',
                    'Principles in RAP 3',
                    'Principles in RAP 4',
                    'Principles in RAP 5',
                    'Principles in RAP 6',
                    'Principles in RAP 7',
                    'Principles in RAP 8',
                    'Principles in RAP 9',
                    'Principles in RAP 10',
                    'Principles in RAP 11',
                    'Principles in RAP 12',
                    'Principles in RAP 13',
                    'Principles in RAP 14',
                    'Principles in RAP 56',
                    'Principles in RAP 57',
                    'Principles in RAP 58',
                    'Principles in RAP 59',
                    'Principles in RAP 60',
                    'Principles in RAP 61',
                    'Principles in RAP 62',
                    'Principles in RAP 63',
                    'Principles in RAP 64',
                    'Principles in RAP 65',
                    'Principles in RAP 66',
                    'Principles in RAP 67',
                    'Principles in RAP 68',
                    'Principles in RAP 69',
                    'Principles in RAP 114',
                    'Principles in RAP 115',
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    if(arr[i] == 'Notes in Reaserch analysis Planning')
                    {
                        html += '<th class="longtext" style="width: 300px">' + arr[i] + '</th>';
                    }
                    else
                    {
                        html += '<th>' + arr[i] + '</th>';
                    }
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonId"},
                        {data: "armstrong2SalespersonName"},
                        {data: "researchAnalysisAndPlanning"},
                        {data: "openning"},
                        {data: "listeningAndQuestioning"},
                        {data: "matchingAndPresenting"},
                        {data: "handlingObjection"},
                        {data: "closing"},
                        {data: "followUpAndMeasurement"},
                        {data: "notesInResearchAnalysisPlanning"},
                        {data: "principlesInRAP"},
                        {data: "principlesInRAP1"},
                        {data: "principlesInRAP2"},
                        {data: "principlesInRAP3"},
                        {data: "principlesInRAP4"},
                        {data: "principlesInRAP5"},
                        {data: "principlesInRAP6"},
                        {data: "principlesInRAP7"},
                        {data: "principlesInRAP8"},
                        {data: "principlesInRAP9"},
                        {data: "principlesInRAP10"},
                        {data: "principlesInRAP11"},
                        {data: "principlesInRAP12"},
                        {data: "principlesInRAP13"},
                        {data: "principlesInRAP14"},
                        {data: "principlesInRAP56"},
                        {data: "principlesInRAP57"},
                        {data: "principlesInRAP58"},
                        {data: "principlesInRAP59"},
                        {data: "principlesInRAP60"},
                        {data: "principlesInRAP61"},
                        {data: "principlesInRAP62"},
                        {data: "principlesInRAP63"},
                        {data: "principlesInRAP64"},
                        {data: "principlesInRAP65"},
                        {data: "principlesInRAP66"},
                        {data: "principlesInRAP67"},
                        {data: "principlesInRAP68"},
                        {data: "principlesInRAP69"},
                        {data: "principlesInRAP114"},
                        {data: "principlesInRAP115"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}

//getOutOfTradeReport
function getOutOfTradeReport(urlGetReport, fromdate, todate, sp, countryId, getall, isMultiCountry, regional) {
    var urlget = urlGetReport + 'transactiondata/out_of_trade_report';
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry
        },
        success: function (result) {
            alert('wait java team');
            return false;
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                var arr = ['Salesperson ID', 'Salesperson Name', 'Research analysis and planning', 'Openning', 'Listening and Questioning',
                    'Matching and Presenting',
                    'Handling Objection', 'Colosing', 'Follow up and measurement', 'Notes in Reaserch analysis Planning',
                    'Principles in RAP',
                    'Principles in RAP 1',
                    'Principles in RAP 2',
                    'Principles in RAP 3',
                    'Principles in RAP 4',
                    'Principles in RAP 5',
                    'Principles in RAP 6',
                    'Principles in RAP 7',
                    'Principles in RAP 8',
                    'Principles in RAP 9',
                    'Principles in RAP 10',
                    'Principles in RAP 11',
                    'Principles in RAP 12',
                    'Principles in RAP 13',
                    'Principles in RAP 14',
                    'Principles in RAP 56',
                    'Principles in RAP 57',
                    'Principles in RAP 58',
                    'Principles in RAP 59',
                    'Principles in RAP 60',
                    'Principles in RAP 61',
                    'Principles in RAP 62',
                    'Principles in RAP 63',
                    'Principles in RAP 64',
                    'Principles in RAP 65',
                    'Principles in RAP 66',
                    'Principles in RAP 67',
                    'Principles in RAP 68',
                    'Principles in RAP 69',
                    'Principles in RAP 114',
                    'Principles in RAP 115',
                ];
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                for (var i = 0; i < arr.length; i++) {
                    if(arr[i] == 'Notes in Reaserch analysis Planning')
                    {
                        html += '<th class="longtext" style="width: 300px">' + arr[i] + '</th>';
                    }
                    else
                    {
                        html += '<th>' + arr[i] + '</th>';
                    }
                }
                ;
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: [
                        {
                            data: "armstrong2SalespersonId",
                            render: function (data, type, row) {
                                if (type === 'display' || type === 'filter') {
                                    return i++;
                                }
                                return data;
                            }
                        },
                        {data: "armstrong2SalespersonId"},
                        {data: "armstrong2SalespersonName"},
                        {data: "researchAnalysisAndPlanning"},
                        {data: "openning"},
                        {data: "listeningAndQuestioning"},
                        {data: "matchingAndPresenting"},
                        {data: "handlingObjection"},
                        {data: "closing"},
                        {data: "followUpAndMeasurement"},
                        {data: "notesInResearchAnalysisPlanning"},
                        {data: "principlesInRAP"},
                        {data: "principlesInRAP1"},
                        {data: "principlesInRAP2"},
                        {data: "principlesInRAP3"},
                        {data: "principlesInRAP4"},
                        {data: "principlesInRAP5"},
                        {data: "principlesInRAP6"},
                        {data: "principlesInRAP7"},
                        {data: "principlesInRAP8"},
                        {data: "principlesInRAP9"},
                        {data: "principlesInRAP10"},
                        {data: "principlesInRAP11"},
                        {data: "principlesInRAP12"},
                        {data: "principlesInRAP13"},
                        {data: "principlesInRAP14"},
                        {data: "principlesInRAP56"},
                        {data: "principlesInRAP57"},
                        {data: "principlesInRAP58"},
                        {data: "principlesInRAP59"},
                        {data: "principlesInRAP60"},
                        {data: "principlesInRAP61"},
                        {data: "principlesInRAP62"},
                        {data: "principlesInRAP63"},
                        {data: "principlesInRAP64"},
                        {data: "principlesInRAP65"},
                        {data: "principlesInRAP66"},
                        {data: "principlesInRAP67"},
                        {data: "principlesInRAP68"},
                        {data: "principlesInRAP69"},
                        {data: "principlesInRAP114"},
                        {data: "principlesInRAP115"}
                    ],
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError();
            }
        },
        error: function (request, status, error) {
            showError();
        }
    });
}
function empty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof this == "undefined":
            return true;
        default:
            return false;
    }
}
function getNewReport(urlGetReport, mode, sp, sku, customer_channel, fromdate, todate, countryId, getall, dateStartMonth, dateEndMonth, month, year, number_sku, app_type, active, channel, otm, isGripCustomer, isMultiCountry, regional, report_type, note_type ,type = null,sr_manage)
{
    var urlget = urlGetReport + 'transactiondata/'+mode;

    if(mode == 'rich_media_demonstration'){
        urlget = urlGetReport + 'summaryreports/rich_media_demostration_report';
    }
    if(mode == 'transaction_rich_media_demonstration'){
        urlget = urlGetReport + 'transactiondata/rich_media_demostration_report';
    }
    var dataprams = {
        spId: sp,
        from_date: fromdate,
        to_date: todate,
        app_type: app_type,
        customer_channel: customer_channel,
        country: countryId,
        getall: getall,
        is_multi_country: isMultiCountry
    };
    if(mode == 'rich_media_demonstration' || mode == 'transaction_rich_media_demonstration')
    {
        dataprams = {
            month: month,
            year: year,
            spId: sp,
            country: countryId,
            getall: getall,
            is_multi_country: isMultiCountry,
            active : active,
            sr_manage : sr_manage,
        };
    }
    if(mode == 'summary_out_of_trade_report'){
        urlget = urlGetReport + 'summaryreports/out_of_trade_report';
        dataprams = {
            spId: sp,
            from_date: fromdate,
            to_date: todate,
            app_type: app_type,
            customer_channel: customer_channel,
            country: countryId,
            getall: getall,
            app_type : app_type,
            is_multi_country: isMultiCountry
        };
        console.log(dataprams)
    }
    if(mode == 'ttl_report')
    {
        urlget = urlGetReport + 'transactiondata/ttl_report';
        dataprams = {
            month: month,
            year: year,
            country: countryId,
            getall: getall,
        };
    }
    if(mode == 'bo_file')
    {
        urlget = urlGetReport + 'transactiondata/bo_file';
        dataprams = {
            month: month,
            year: year,
            country: countryId,
            getall: getall,
        };
    }
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: urlget,
        data: dataprams,
        success: function (result) {
            if (result.codeError === undefined || result.codeError === null) {
                var html = '';
                html += '<table class="table table-report display" id="example" >';
                html += '<thead><tr><th>#</th>';
                var columnsinit = [];
                if(!(result.listTitle === null)){
                    for (var i = 0; i < result.listTitle.length; i++) {
                        var tempinit = result.listTitle[i].split(':');
                        if (tempinit.length >= 2) {
                            var key = tempinit[0].replace('"', "");
                            var name = tempinit[1].replace('"', "");
                            var c = {};
                            html += '<th class="' + key + '">' + name + '</th>';
                            c.data = key;
                            if (i == 0) {
                                var id = {
                                    data: key,
                                    render: function (data, type, row) {
                                        if (type === 'display' || type === 'filter') {
                                            return i++;
                                        }
                                        return data;
                                    }
                                };
                                columnsinit.push(id);
                            }
                            columnsinit.push(c);
                        }
                    }

                }
                else
                {
                    html += 'Data empty';
                }
                html += '</tr></thead>';
                html += '</table>';
                $('#reporting-view').html(html);
                var i = 1;
                var t = $('#example').dataTable({
                    data: result.listData,
                    columns: columnsinit,
                    fnDrawCallback: function (oSettings) {
                        if (oSettings.bSorted || oSettings.bFiltered) {
                            for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                                $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                            }
                        }
                    },
                    aoColumnDefs: [
                        {"bSortable": false, "aTargets": [0]}
                    ],
                    lengthMenu: [[100, 1000, -1], [100, 1000, "All"]]
                });
            } else {
                showError(result);
            }
        },
        error: function (request, status, error) {
            showError(result);
        }
    });
}