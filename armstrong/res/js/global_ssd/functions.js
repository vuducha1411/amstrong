// unix time generator
function UNIXTIME(){
	return Math.round((new Date()).getTime() / 1000);
}

var FORM_CONFIG_DISABLED = {
	required : false,
	disabled : true
}

var FORM_CONFIG_ENABLED = {
	required : true,
	disabled : false
}
var scrollXDivFull = '<div class="col-md-12 scrollXDivFull scroll-x"></div>';
var colCount = null;
var systemColumns = 1;
var editIcon = '<i class="fa fa-pencil"></i>';
var cancelIcon = '<i class="fa fa-close"></i>';
var htmlOverlay = '<div id="myNav" class="overlay"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><div class="overlay-content"><h3>Loading.. <i class="fa fa-spin fa-circle-o-notch"></i></h3></div></div>';

function is_object( obj ) { return (!!obj) && (obj.constructor === Object); }
function is_array( arr ) { return (!!arr) && (arr.constructor === Array); }

function enable_field(field, enable = true) {
	field.prop( (enable && field.find('option').length > 1) ? FORM_CONFIG_ENABLED : FORM_CONFIG_DISABLED );
}
function popField(form, data) {
	if( form.length == 1 && data.length > 0 ) {
		data.forEach(function(val){
			val.data.forEach(function(inVal){
				display = inVal.name + ' (' + val.index + ')';
				optionValue = val.index + ':' + inVal.id;
				form.append('<option value="'+optionValue+'">'+display+'</option>');
			});
		})

		form.find('option[value=""]').remove();
		form.find('option[value="0"]').remove();

		if( form.find('option').length <= 0 ) {
			enable_field(form, false);
			form.prepend('<option selected value="">No data found</option>');
		} else {
			form.prepend('<option selected value="">Please select from list</option>');
			enable_field(form);
		}		
	}
}

function isValidValue( formVal ) {
	switch(formVal) {
		case 0:
		case '0':
		case undefined:
		case null:
		case '':
		case ' ':
		case undefined:
			return false;
			break;
		default:
			return true;
			break;
	}
}

function getColumns(tableBase) {
	allCols = tableBase.find('thead > tr').find('th');
	return {
		count : allCols.length,
		obj : allCols
	};
}

function generateRow(columnObj) {
	htmlGen = '<tr class="datarow">';

	$.each(columnObj, function(index, obj) {
		if( !isSystemObj(obj) ) {	
			inputName = obj.textContent.toLowerCase();
			inputName = inputName.replace(/ /g, '_');
			htmlGen += '<td class="dyna-inpt"><input placeholder="Input '+obj.textContent+'" type="text" name="'+inputName+'[]" class="edited form-control"></td>';
		} else if ($(obj).hasClass("in-chkbx")) {
            htmlGen += '<td class="dyna-inpt"><input type="checkbox" class="input-chckbx"></td>';
		}
	});

	for (var i = systemColumns - 1; i >= 0; i--) {
		htmlGen += '<td><button type="button" class="del-row btn btn-danger btn-xs" title="Delete row"><i class="fa fa-close"></i></button></td>';
	}

	htmlGen += '</tr>';
	return htmlGen;
}

function isSystemObj(obj) {
	sysClass = obj.className.split(' ');
	return (sysClass.indexOf('system') >= 0 || sysClass.indexOf('no-input') >= 0);
	// if ( sysClass.indexOf('system') >= 0 || sysClass.indexOf('no-input') >= 0 ) {
	// 	return true;
	// } else {
	// 	return false;
	// }
	// return obj.className.split(' ').indexOf("system") >= 0 ? true : false;
}

function refreshRowDelete() {
	btnDeleteRow = $("button.del-row");
	btnDeleteRow.off("click");
	btnDeleteRow.click(function(){
		if(confirm("Are you sure you want to delete row?")) {
            $(this).closest('tr').remove();
            $("input[type=checkbox].input-chckbx").trigger("change");
		}
	});
}

function retrieveValue( source, destination ) {
	$.each(source, function(index, obj){
		if($(obj).val() == '' || $(obj).val() == null) {
			destination.eq(index).val($(obj).text());
		} else if ($(obj).text() == '' || $(obj).val() == null) {
			destination.eq(index).empty().text($(obj).val());
		}
	});
}

function MAPPING_TABLE_MODIFY () {
	var btnAddRow = $("#add_row");
	var btnSaveRows = $("#save_row");
	var btnEditRow = $("button.edit-row");
	var btnSaveRows = $("#save_row");
	var mapTblBase = $("#tbl_map"); 
	var mapTblHead = $("#tbl_map > thead > tr"); 
	var mapTblBody = $("table#tbl_map > tbody");
	var chbIsDraft = $("#is_draft_chb");
	var inputChkboxes = $("input[type=checkbox].input-chckbx");
	var masterChkbox = $("#masterCheck");

	masterChkbox.click(function () {
        // $("p#selectCount").empty();
		allCurrentBoxes = $("input[type=checkbox].input-chckbx");
		if($(this).is(":checked")) {
			allCurrentBoxes.prop("checked", true);
            // $("p#selectCount").append(selected + " row(s) selected");
		} else {
            allCurrentBoxes.prop("checked", false);
		}
        allCurrentBoxes.trigger("change");
    });

    // inputChkboxes.change(function (e) {
    inputCheckboxChange = function () {
    	selected = $("input[type=checkbox].input-chckbx:checked").length;
    	if($(this).is(':checked')) {
			$(this).closest('tr').addClass('highlight');
		} else {
            $(this).closest('tr').removeClass('highlight');
		}
        $("p#selectCount").empty();
    	if (selected > 0) {
    		$("#bulk_delete_row").prop("disabled", false);
            $("p#selectCount").append(selected + " row(s) selected");
        } else {
            $("#bulk_delete_row").prop("disabled", true);
		}

		if (selected === 0) {
            masterChkbox.prop("checked", false);
		} else if(selected !== $("input[type=checkbox].input-chckbx").length) {
    		masterChkbox.prop("indeterminate", true);
		} else {
            masterChkbox.prop("checked", true);
		}
		// console.log($("input[type=checkbox].input-chckbx:checked").length);
    };

    inputChkboxes.change(inputCheckboxChange);

    $("#bulk_delete_row").click(function(){
    	selectedRows = $("input[type=checkbox].input-chckbx:checked");
		if (confirm("Are you sure you want to delete " + selectedRows.length + " row(s)?")) {
			selectedRows.each(function(index, object){
				$(object).closest('tr').remove();
			});
		}
	});
	
	colCount = getColumns(mapTblBase).count - systemColumns;

	chbIsDraft.click(function() {
		$("#is_draft").val( $(this).is(":checked") ? 1 : 0 );
	})

	btnAddRow.click(function () {
		mapTblBody.append(generateRow(getColumns(mapTblBase).obj));
		refreshRowDelete();
        inputChkboxes.off("change");
        $("input[type=checkbox].input-chckbx").change(inputCheckboxChange);
		mapTblBody.find('tr:last-child').find('input[type=text]').first().focus();
	});

	btnSaveRows.click(function() {
		var hasError = false;
		if( mapTblBase.find('input[type="text"]').length > 0 ) {
			if(confirm("This will finalize your changes. Do you want to proceed?")) {
                $("input[type=text].existing:visible").each(function (ind, obj) {
                    $(obj).val($(obj).next().text());
                });
                finalJson = [];
                mapTblBody.find('tr').each(function(ind, obj){
                    var collective = {};
                    $(obj).find('input[type="text"]').each(function(inInd, inObj){
                        jsonKey = $(inObj).attr('name').replace('[]', '').toUpperCase();
                        jsonVal = $(inObj).val();
                        collective[jsonKey] = jsonVal;
                    });
                    finalJson.push(collective);
                });
                finalJson = JSON.stringify(finalJson);
                // proceedToSave = true;

                if (finalJson === $("#rawMapJson").val()) {
                	alert("No changes have been made.");
				} else {
                    $('input[type="text"].form-control').remove();
                    $("#form_doAddNewMap").append(
                        $("<input>")
                            .attr('type', 'hidden')
                            .attr('name', "finalJson")
                            .val(finalJson)
                    ).submit();
				}
			}
			/*
			// script to check empty and invalid values
			$.each($("tr.datarow"), function(index, item){
				$.each($(item).find('td.dyna-inpt'), function(tdIndx, tdItm) {
					if(!hasError && $(tdItm).find('input[type="text"]').val().trim() == '') {
						$(tdItm).find('input[type="text"]').focus();
						hasError = true;
						return false;
					}
				})
			})
			*/

			// if( !hasError ) {
			// } else {
			// 	alert("PLEASE FILL OUT ALL FIELDS BEFORE SAVING ENTRY")
			// }
		} else {
			alert("There are no items to save");
		}
	});

	btnEditRow.click(function(){
		var currEditBtn = $(this);
		// var currentValues =
		var receiverTr = $(this).closest('tr');
		var trashBtn = receiverTr.find('button.del-row');
		var saveEditBtn = receiverTr.find('button.saveEdit');

		if( $(this).hasClass("btn-danger") ) {
			$(this).empty().append(editIcon).removeClass('btn-danger').addClass("btn-warning").attr('title', 'Edit row');
			retrieveValue(receiverTr.find('span.def-val'), receiverTr.find('input[type="text"]'));
			saveEditBtn.hide();
			saveEditBtn.off("click");
		} else if ($(this).hasClass("btn-warning")) {
			$(this).empty().append(cancelIcon).removeClass("btn-warning").addClass('btn-danger').attr('title', 'Cancel Edit');
			saveEditBtn.show();
			saveEditBtn.click(function(){
				retrieveValue(receiverTr.find('input[type="text"]'), receiverTr.find('span.def-val'));
				currEditBtn.empty().append(editIcon).removeClass('btn-danger').addClass("btn-warning");
				saveEditBtn.hide();
				saveEditBtn.off("click");
				trashBtn.toggle();
				receiverTr.find('input[type="text"]').removeClass("edited").addClass("edited").toggle();
				receiverTr.find('span.def-val').toggle();
			});
		} else {
			alert("SCRIPT ERROR. PLEASE CONTACT THE ADMINISTRATORS");
			location.reload();
		}

		trashBtn.toggle();
		receiverTr.find('input[type="text"]').toggle();
		receiverTr.find('span.def-val').toggle();
	});

	refreshRowDelete();
}

function populateTable(data) {
	if(data.mapData.length > 0) {
		$.each(data.mapData, function(index, item) {
			// build thead content
			titleStr = item["name"] + '   <a class="btn btn-xs btn-warning" href="' + base_url() +'ami/global_ssd/modDraftMapping/'+ item["map_id"] +'"><i class="fa fa-edit"></i> Edit</a>';
			// titleStr = '<a href="' + base_url() +'ami/global_ssd/modDraftMapping/'+ item["map_id"] +'">'+ item["name"] +'</a>';
			// titleStr += ( item["is_draft"] ) ? "<small>(Draft)</small>" : "";
			data.tblTitle.empty().append(titleStr);
			theadContent = "<thead><tr>";
			$.each(item["columns"].split(":"), function(colInd, colItm) {
				theadContent += "<th>" + colItm + "</th>";
			});
			theadContent += "</tr></thead>";

			tbodyContent = "<tbody>";
			if(item["value"] != null) {
				// build tbody content
				$.each(item["value"], function(contentInd, contentItem) {
					tbodyContent += "<tr>";
					$.each(item["columns"].split(":"), function(colInd, colItm) {
						tbodyContent += "<td>" + contentItem[colItm] + "</td>";
					});
					tbodyContent += "</tr>";
				});
			} else {
				tbodyContent += '<tr><td colspan="'+item["columns"].split(":").length+'"><i>No data found.</i></td></tr>';
			}
			tbodyContent += "</tbody>";

			data.table.empty().append(theadContent + tbodyContent);
			data.table.focus();
			return false;
		});
	} else {
		data.tblTitle.empty();
		alert("No mapping found. Please refresh browser and try again.");
	}
}

function MAPPING_TABLE_INTERFACE() {
	var inputSeach = $("#inputSearch");
	var ddCpdtg = document.getElementById("dropdown_cpdtg");
	var ddCcustg = document.getElementById("dropdown_ccustg");
	var ddSelectPd = document.getElementById("select_PDT");
	var ddSelectCust = document.getElementById("select_CUST");

	if(ddCpdtg != null &&ddCpdtg.length == 1) {
		$.ajax({ // get PRODUCT MAPPING maps
			url : base_url() + 'ami/global_ssd/getCPDTGmap',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				popField($("#dropdown_cpdtg"), [
						{ 
							index : "DT", 
							data : data["distributors"]
						},
						{ 
							index : "WHS", 
							data : data["wholesalers"]
						},
						{ 
							index : "CUST", 
							data : data["customers"]
						}
					]);
				return false;
			}
		});
	}

	if(ddCcustg != null && ddCcustg.length == 1) {
		$.ajax({ // get PRODUCT MAPPING maps
			url : base_url() + 'ami/global_ssd/getCCUSTGmap',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				popField($("#dropdown_ccustg"), [
						{ 
							index : "DT", 
							data : data["distributors"]
						},
						{ 
							index : "WHS", 
							data : data["wholesalers"]
						},
						{ 
							index : "CUST", 
							data : data["customers"]
						}
					]);
				return false;
			}
		});
	}

	if(ddSelectPd != null && ddSelectPd.length == 1) {
		$.ajax({ // get roles without PRODUCT MAPPING maps
			url : base_url() + 'ami/global_ssd/getCPDTGmapWithout',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				popField($("#select_PDT"), [
						{ 
							index : "DT", 
							data : data["distributors"]
						},
						{ 
							index : "WHS", 
							data : data["wholesalers"]
						},
						{ 
							index : "CUST", 
							data : data["customers"]
						}
					]);
				return false;
			}
		});
	}

	if(ddSelectCust != null && ddSelectCust.length == 1) {
		$.ajax({ // get roles without PRODUCT MAPPING maps
			url : base_url() + 'ami/global_ssd/getCCUSTGmapWithout',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				popField($("#select_CUST"), [
						{ 
							index : "DT", 
							data : data["distributors"]
						},
						{ 
							index : "WHS", 
							data : data["wholesalers"]
						},
						{ 
							index : "CUST", 
							data : data["customers"]
						}
					]);
				return false;
			}
		});
	}

	$('#entity').change(function(){
		$("div.no-show.form-group").hide();
		selected = $(this).find('option:selected');
		$(".entity-columns").hide();
		$("#columns_"+selected.val()).show();
		switch(selected.val()) {
			case '1' :
				$("#selectDiv_PDT").show();
				enable_field($("#selectDiv_PDT select"));
				break;
			case '2' :
				$("#selectDiv_CUST").show();
				enable_field($("#selectDiv_CUST select"));
				break;
			default:
				$(".add-newselect").hide();
				enable_field($(".add-newselect select"), false);
				break;
		}
		return false;
	});

	$("#btnInitMappingCreate").click(function() {
		$("#errorMsg").empty();
		err = [];
		objEntity = $(".obj-entity:visible");
		mapEntity = $("#entity");
		form = $("form#addNewMappingDraft");
		form.empty();
		if( !isValidValue(mapEntity.val()) ) {
			err.push("Please select a valid map entity");
		}
		if( !isValidValue(objEntity.val()) ) {
			err.push("Please select a DT/WHS/CUSTOMER before proceeding");
		}

		if( err.length == 0 ) {
			form.append('<input type="hidden" name="map_entity_id" value="'+mapEntity.val()+'" />');
			form.append('<input type="hidden" name="role_entity_id" value="'+objEntity.val()+'" />');
			form.submit();
		} else {
			err.forEach(function(item) {
				$("#errorMsg").append('<li>'+item+'</li>');
			});
		}
		return false;
	});

	$(".dynaViewMap").click(function(){
		dropDown = $($(this).data("target"));
		btnTrigger = $(this).closest('div').find('button');

		if(isValidValue( dropDown.val() )) {
			$('div.mapview-selection select').not($(this).data('target')).val('');
			toggleFunctionality([
				inputSeach,
				btnTrigger
			], FORM_CONFIG_DISABLED);

			$("#map_table").empty();
			$("#map_title").empty().append("Loading mapping. Please wait..");

			$.ajax({
				url : base_url() + 'ami/global_ssd/getMap/' + dropDown.val() + '/' + $(this).data('entity') ,
				type : 'GET',
				dataType : 'json',
				success : function(data) {
					populateTable({
						mapData : data,
						table : $("#map_table"),
						tblTitle : $("#map_title") 
					});

					toggleFunctionality([
						btnTrigger,
						inputSeach
					], FORM_CONFIG_ENABLED);

					$('.sortable').off();
                    $('.sortable th').off();
                    sorttable.init();

				}, error: function() {
					$("#map_title").empty();
					toggleFunctionality(btnTrigger, FORM_CONFIG_ENABLED);
					alert("Map Load Failed. Please Contact Administrator.");
					location.reload();
				}
			});
		} else {
			alert("Please select a map from the list");
		}
		return false;
	})

	$(".dynaEditMap").click(function () {
		mapVal = $($(this).data('target')).val();
		if( mapVal ) {
            editHref = $(this).data('href') + '/' + mapVal + '/' + $(this).data('map');
			$("*").off();
			console.log(editHref);
            location.href = editHref;
		} else {
			alert("Please select a map from the list");
		}
    });
}

function toggleFunctionality( form, config ) {
	if ( is_object(form) || is_array(form) ) {
		$.each(form, function(index, obj) {
			toggleFunctionality(obj, config);
		})
	} else {
		form.prop(config);	
	}
}

function openOverlay( message = 'Loading data. Please wait..' ) { 
	message += '  <i class="fa fa-spin fa-circle-o-notch"></i>';
	$("#overlay-message").empty().append(message);
	$(".load-overlay").width("100%");
}

function closeOverlay() { $(".load-overlay").width("0%") }

function GLOBAL_SSD_MASTER(siteUrl) {
	// $.fn.dataTable.ext.errMode = false;
	$.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
		console.log(message);
		// closeOverlay();
	};
	openOverlay("Loading SSD data. Please wait..");
	var dateOfTransactionsFilter = $("#filterDateOfTrans");
	var rawSsdRoute = siteUrl;
	var ssdRoute = encodeURI(rawSsdRoute);
	var exportData = {
		acknowledge : {
			exportOptions : {
				format: {
					body : function (data, row, column, node) {
						if(column === 0) {
							return $(data).hasClass("acknowledged") ? "Acknowledged" : ""
						} else {
							return data;
						}
                    }
				}
			}
		}	
	};
	var dtConfiguration = {
		"ajax" : {
			"url" : ssdRoute,
			"dataSrc" : ""
		},
		"dom": 'lBfrtip',
		// "buttons": ['excel'],
		buttons: [
			$.extend(true, {}, exportData.acknowledge, {
				extend: 'excelHtml5',
				text: '<i class="fa fa-downoad"></i>  Export',
				filename : function() {
					console.log($("#dotTo_datepicker").val() !== "");
					if( $("#dotTo_datepicker").val() !== "" && $("#dotFrom_datepicker").val() !== "" ) {
                        if( $("#dotTo_datepicker").val() === $("#dotFrom_datepicker").val() ) {
                            dataDate = "_" + $("#dotTo_datepicker").val();
                        } else {
                            dataDate = "_" + $("#dotFrom_datepicker").val() + ' - ' + $("#dotTo_datepicker").val()
                        }
					} else {
						dataDate = "_ALL";
					}
					return "Global_SSD_Export" + dataDate;
				}
			})
			// {
			// 	extend: 'excelHtml5',
			// 	text: '<i class="fa fa-downoad"></i>  Export',
			// }
		],
		"columns" : [
			{
				"data" : "id",
				'targets': 0,
				'checkboxes': {
					'selectRow': true
				},
				"render" : function(data, type, full, meta){
					if(full.acknowledged == 1 || full.acknowledged == "1") {
						return '<i class="fa fa-check-circle acknowledged" style="color : #4caf50;"></i>';
						// return '<input type="checkbox" value="'+data+'" class="dt-checkboxes" checked>';
					} else {
						return '<input type="checkbox" value="'+data+'" class="dt-checkboxes" >';
					}
				}
			},
			{ "data" : "id" },
			{ "data" : "DISTRIBUTOR" },
			{ "data" : "DT_CUSTOMER_CODE" },
			{ "data" : "DT_CUSTOMER_NAME" },
			{ "data" : "CUSTOMERS" },
			{ "data" : "GROUPING_CUSTOMER" },
			{ "data" : "TEAM" },
			{ "data" : "SALESMAN" },
			{ "data" : "CHANNEL" },
			{ "data" : "SALESMAN_CODE" },
			{ "data" : "WHOLESALE_SUB_CHANNEL" },
			{ "data" : "GOLD_PLUS" },
			{ "data" : "CPU" },
			{ "data" : "OEM" },
			{ "data" : "DT_PRODUCT_CODE" },
			{ "data" : "DT_PRODUCT_NAME" },
			{ "data" : "PRODUCT" },
			{ "data" : "PRODUCT_CODE" },
			{ "data" : "PRODUCT_GROUPING" },
			{ "data" : "CATEGORY" },
			{ "data" : "MARKET" },
			{ "data" : "HURRICANE_10" },
			{ "data" : "TOP_SKUS" },
			{ "data" : "BRAND" },
			{ "data" : "YEAR" },
			{ "data" : "MONTH" },
			{ "data" : "DATE_OF_TRANSACTION" },
			{ "data" : "SUM_OF_TOTAL_PURCHASE_PRODUCT" },
			{ "data" : "SUM_OF_TOTAL_QTY_PURCHASE" },
			{ "data" : "UOM" }
		],
		"order": [[ 1, 'asc' ]],
		"initComplete" : function(){
			$("#tbl_global_ssd").wrap(scrollXDivFull);
			closeOverlay();
			$("#form_acknowledge button[type=submit]").attr('disabled', false);
		}
	};
	var globalSSDTable = $('#tbl_global_ssd').removeAttr("width").DataTable(dtConfiguration);
	// globalSSDTable.buttons().container().appendTo('li#ssdExportBtn');

    dateOfTransactionsFilter.click(function(event){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $("#form_acknowledge button[type=submit]").attr('disabled', true);
        openOverlay("Loading SSD data. Please wait..");
        if( isValidValue($("#dotFrom_datepicker").val()) && isValidValue($("#dotTo_datepicker").val()) ) {
            dtConfiguration["ajax"]["url"] = encodeURI(rawSsdRoute + "?f=" + $("#dotFrom_datepicker").val() + "&t=" + $("#dotTo_datepicker").val()  + "&a=" + $("#acknowledged").is(":checked"));
            // closeOverlay();
        } else {
            dtConfiguration["ajax"]["url"] = encodeURI(rawSsdRoute + "?a=" + $("#acknowledged").is(":checked"));
        }
        setTimeout(function(){
            globalSSDTable.destroy();
            globalSSDTable = $('#tbl_global_ssd').removeAttr("width").DataTable(dtConfiguration);
            globalSSDTable.buttons().container().appendTo('li#ssdExportBtn');
        }, 100);
    });

    dateOfTransactionsFilter.trigger("click");

	$('#form_acknowledge').on('submit', function(e){
        var params = globalSSDTable.$('input[type="checkbox"].dt-checkboxes:checked', {"page" : "all"});
		if( params.length > 0 && confirm("All "+params.length+" selected SSD data will be acknowledged. Do you want to proceed?") ) {
			var form = $(this);
			form.empty();
			// Iterate over all form elements
			$.each(params, function(ind, obj){
				form.append(
					$("<input>")
						.attr('type', 'hidden')
						.attr('name', "ssdData[]")
						.val($(obj).val())
				);
			});
			if( isValidValue($('#dotFrom_datepicker').val()) && isValidValue($('#dotFrom_datepicker').val()) ) {
				form.append($('<input>').attr({ type : 'hidden', name : 'from' }).val($('#dotFrom_datepicker').val()));
				form.append($('<input>').attr({ type : 'hidden', name : 'to' }).val($('#dotTo_datepicker').val()));
			}
		} else if (params.length <= 0) {
        	alert("No SSD data selected. Please try again.");
			$(this).find('input[type=hidden]').remove();
            e.preventDefault();
		} else {
            e.preventDefault();
		}
	});
}

function GLOBAL_SSD_ACK ( siteUrl ) {
    // $.fn.dataTable.ext.errMode = false;
    $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
        console.log(message);
        // closeOverlay();
    };
    openOverlay("Loading SSD data. Please wait..");
    var rawSsdRoute = siteUrl;
    var ssdRoute = encodeURI(rawSsdRoute);
    var dtConfiguration = {
        "ajax" : {
            "url" : ssdRoute,
            "dataSrc" : ""
        },
        "dom": 'lBfrtip',
        // "buttons": ['excel'],
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-downoad"></i>  Export',
                filename : function() {
                    console.log($("#dotTo_datepicker").val() !== "");
                    if( $("#dotTo_datepicker").val() !== "" && $("#dotFrom_datepicker").val() !== "" ) {
                        if( $("#dotTo_datepicker").val() === $("#dotFrom_datepicker").val() ) {
                            dataDate = "_" + $("#dotTo_datepicker").val();
                        } else {
                            dataDate = "_" + $("#dotFrom_datepicker").val() + ' - ' + $("#dotTo_datepicker").val()
                        }
                    } else {
                        dataDate = "_ALL";
                    }
                    return "Global_SSD_Export" + dataDate;
                }
            }
        ],
        "columns" : [
            { "data" : "id" },
            { "data" : "DISTRIBUTOR" },
            { "data" : "DT_CUSTOMER_CODE" },
            { "data" : "DT_CUSTOMER_NAME" },
            { "data" : "CUSTOMERS" },
            { "data" : "GROUPING_CUSTOMER" },
            { "data" : "TEAM" },
            { "data" : "SALESMAN" },
            { "data" : "CHANNEL" },
            { "data" : "SALESMAN_CODE" },
            { "data" : "WHOLESALE_SUB_CHANNEL" },
            { "data" : "GOLD_PLUS" },
            { "data" : "CPU" },
            { "data" : "OEM" },
            { "data" : "DT_PRODUCT_CODE" },
            { "data" : "DT_PRODUCT_NAME" },
            { "data" : "PRODUCT" },
            { "data" : "PRODUCT_CODE" },
            { "data" : "PRODUCT_GROUPING" },
            { "data" : "CATEGORY" },
            { "data" : "MARKET" },
            { "data" : "HURRICANE_10" },
            { "data" : "TOP_SKUS" },
            { "data" : "BRAND" },
            { "data" : "YEAR" },
            { "data" : "MONTH" },
            { "data" : "DATE_OF_TRANSACTION" },
            { "data" : "SUM_OF_TOTAL_PURCHASE_PRODUCT" },
            { "data" : "SUM_OF_TOTAL_QTY_PURCHASE" },
            { "data" : "UOM" }
        ],
        "order": [[ 1, 'asc' ]],
        "initComplete" : function(){
            $("#tbl_global_ssd").wrap(scrollXDivFull);
            closeOverlay();
        }
    };
    var globalSSDTable = $('#tbl_global_ssd').removeAttr("width").DataTable(dtConfiguration);
    globalSSDTable.buttons().container().appendTo('li#ssdExportBtn');
    $("#filterDateOfTrans").click(function(event){
        openOverlay("Loading SSD data. Please wait..");
        if( isValidValue($("#dotFrom_datepicker").val()) && isValidValue($("#dotTo_datepicker").val()) ) {
            dtConfiguration["ajax"]["url"] = encodeURI(rawSsdRoute + "&f=" + $("#dotFrom_datepicker").val() + "&t=" + $("#dotTo_datepicker").val());
        } else {
            dtConfiguration["ajax"]["url"] = encodeURI(rawSsdRoute);
        }
        setTimeout(function(){
            globalSSDTable.destroy();
            globalSSDTable = $('#tbl_global_ssd').removeAttr("width").DataTable(dtConfiguration);
            globalSSDTable.buttons().container().appendTo('li#ssdExportBtn');
        }, 200);
    });
}

////////////
// COMMON //
////////////
$(document).ready(function(){
	var datePicker = {
		today : $(".datepicker-today")
	};
	// overlay click event-handler
	$(".overlay").click(false);

	// table search
	$(".search-table").keyup(function(event){
		if( event.keyCode == 13 || $(this).val() == '' ) {
			var filter = $(this).val().toUpperCase();
			var allTr = $($(this).data("target")).find('tbody > tr');
			$.each(allTr, function(index, item){
				var occurence = false;
				$.each($(item).find('td'), function(tdIndex, tdItem) {
					if ($(tdItem).text().toUpperCase().indexOf(filter) > -1) {
						if(filter == '') {
							$(tdItem).removeClass("found");
						} else {
							$(tdItem).addClass("found");
						}
						$(item).show();
						occurence = true;
					} else {
						$(tdItem).removeClass("found");
					}
				});
				if( !occurence ) {
					$(item).hide();
				}
			});
		}
	});

	// export table
	$(".export-table").click(function(argument) {
		if($($(this).data('target')).find('tbody').length > 0) {
            console.log("Exporting table ID : " + $(this).data('target'));
            $($(this).data('target')).table2excel({
                exclude: "tr:hidden",
                name: $(this).data('sheetname'),
                filename: $(this).data('filename') + "_" + UNIXTIME() //do not include extension
            });
		} else {
			alert("Please view a map from the list before export");
		}
	});

	// export table
	$(".export-table-complete").click(function(argument) {
        if($($(this).data('target')).find('tbody').length > 0) {
            console.log("Exporting table ID : " + $(this).data('target'));
            $($(this).data('target')).table2excel({
                name: $(this).data('sheetname'),
                filename: $(this).data('filename') + "_" + UNIXTIME() //do not include extension
            });
        } else {
            alert("Please view a map from the list before export");
		}
	});

	$('input[type="text"].datepicker').datepicker({		
		format : 'yyyy-mm-dd',
	}).on('changeDate', function(e){
		$($(this).data("target")).val($(this).val());
		$($(this).data("target")).datepicker("setStartDate", $(this).val() );
	});

	if(!isValidValue(datePicker.today.val())) {
        $(".datepicker-today").datepicker("setDate", "0");
	}

	$("span.input-group-addon").click(function () {
		$($(this).data('caltarget')).focus();
    });

	setTimeout(function(){
        $("div.alert").fadeOut('fast');
	}, 10000);

	$(".close").click(function(){
		$(this).closest('div').fadeOut('fast');
	});
});