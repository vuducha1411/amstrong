$('#Generate').click(function(e) {
    xhr = null;
    var $this = $(this),
        $selector = $('#SalespersonsSelector'),
        $startDate = $('#InputStartDate'),
        $endDate = $('#InputEndDate'),
        $container = $($this.data('container')),
        url = $this.data('url') || false
        draft = $this.data('draft');

    if (xhr) {
        xhr.abort();
    }

    if (!xhr && url && $startDate.val() && $endDate.val() && $selector.val()) {
        
        $container.html('').empty().html('<div class="col-sm-6 col-sm-offset-3"><i class="fa fa-spinner fa-spin"></i> Loading...</div>');

        $.ajax({
            url: url,
            type: 'POST',
            data: { id: $selector.val(), start_date: $startDate.val(), end_date: $endDate.val() , draft: draft },
            success: function(res) {
                try {
                    var resJson = JSON.parse(res);
                    if (resJson._session_expried) {
                        window.location.reload();
                        return;
                    }
                    
                } catch(e) {}


                $container.html('').empty();
                if (res) {
                    $container.append(res);
                    CheckAll($('input.CheckAll'));
                }

                xhr = null;
            }
        });
    }
});

$('#InputDateRange').daterangepicker({
    format: 'YYYY-MM-DD'
}).on('apply.daterangepicker', function(ev, picker) {
    $('#InputStartDate').val(picker.startDate.format('YYYY-MM-DD'));
    $('#InputEndDate').val(picker.endDate.format('YYYY-MM-DD'));
});