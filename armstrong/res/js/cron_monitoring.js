$(document).ready(

	function(){
		
	    $('#cronDataTable').dataTable({
	        "processing": true,
	        "ajax": {
	            "url": current_url()+"/getCronList",
	            "type": "GET",
            	"dataSrc":""
	        },
	        'aoColumns': [
	        	{mData: "id"},
	            {mData: "name"},
	            {mData: "desc"},
	            {mData: "schedule"},
	            {mData: "last_updated"}
	            
	        ],
	        'order': [[1, 'desc']],
	        'aoColumnDefs': [{
	            'aTargets': ['nosort']
	        }],
	        "iDisplayLength": 10
	    });


	}
);