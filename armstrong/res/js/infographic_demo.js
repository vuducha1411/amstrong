$(document).ready(function () {

    //draw human chart
    drawHumanChart(9, 30, "human-chart");
    $("#txt1").text("9 SALES LEADERS");
    $("#txt2").text("30 SALES PERSONNELS");

    //piechart
    drawPiechart("calldata-chart", 0);
    //growth chart
    drawGrowthChart(0, "growthchart-index1");
    $("#growthchart-textct1").text("0%");
    drawGrowthChart(0, "growthchart-index2");
    $("#growthchart-textct2").text("0%");
    drawGrowthChart(0, "growthchart-index3");
    $("#growthchart-textct3").text("0%");
    drawGrowthChart(0, "growthchart-index4");
    $("#growthchart-textct4").text("0%");

    // customer type - line chart
    var maxWidth = 357;
    var maxValue = 0;
    var maxTfo = 0;
    var width = 0;

    var rect1 = document.getElementById("linechart1");
    var polygon1 = document.getElementById("polygon1");
    var textrect = document.getElementById("textrect");
    width = calculateWidth(maxWidth, maxTfo, 0)
    drawLineChart(rect1, polygon1, textrect, width);

    var rect2 = document.getElementById("linechart2");
    var polygon2 = document.getElementById("polygon2");
    var textrect = document.getElementById("textrect2");
    width = calculateWidth(maxWidth, maxValue, 0)
    drawLineChart(rect2, polygon2, textrect2, width);

    var rect3 = document.getElementById("linechart3");
    var polygon3 = document.getElementById("polygon3");
    var textrect = document.getElementById("textrect3");
    width = calculateWidth(maxWidth, maxTfo, 0)
    drawLineChart(rect3, polygon3, textrect3, width);

    var rect4 = document.getElementById("linechart4");
    var polygon4 = document.getElementById("polygon4");
    var textrect = document.getElementById("textrect4");
    width = calculateWidth(maxWidth, maxValue, 0)
    drawLineChart(rect4, polygon4, textrect4, width);

    var rect5 = document.getElementById("linechart5");
    var polygon5 = document.getElementById("polygon5");
    var textrect = document.getElementById("textrect5");
    width = calculateWidth(maxWidth, maxTfo, 0)
    drawLineChart(rect5, polygon5, textrect5, width);

    var rect6 = document.getElementById("linechart6");
    var polygon6 = document.getElementById("polygon6");
    var textrect = document.getElementById("textrect6");
    width = calculateWidth(maxWidth, maxValue, 0)
    drawLineChart(rect6, polygon6, textrect6, width);

    var rect7 = document.getElementById("linechart7");
    var polygon7 = document.getElementById("polygon7");
    var textrect = document.getElementById("textrect7");
    width = calculateWidth(maxWidth, maxTfo, 0)
    drawLineChart(rect7, polygon7, textrect7, width);

    var rect8 = document.getElementById("linechart8");
    var polygon8 = document.getElementById("polygon8");
    var textrect = document.getElementById("textrect8");
    width = calculateWidth(maxWidth, maxValue, 0)
    drawLineChart(rect8, polygon8, textrect8, width);

});

function drawHumanChart(redhumans, greenhumans, chartid) {
    var greenHumanImage = "http://dev.ufs-armstrong.com/armstrong/res/img/infographic/green_human.png",
        redHumanImage = "http://dev.ufs-armstrong.com/armstrong/res/img/infographic/red_human.png",
        allHuman = redhumans + greenhumans;
    var svgDocument = document.getElementById(chartid);

    //var svgWidth = svgDocument.clientWidth,
    //svgHeight = svgDocument.clientHeight,
    //rowCount = 0;
    var svgWidth = $("#" + chartid).width(),
        svgHeight = $("#" + chartid).height(),
        rowCount = 0;

    if (allHuman < 5) rowCount = 1;
    else if (allHuman < 15) rowCount = 2;
    else if (allHuman < 34) rowCount = 3;
    else if (allHuman < 100) rowCount = 4;
    else rowCount = 5;

    var humanHeight;
    if (rowCount > 1) humanHeight = (svgHeight - (rowCount - 1) * 5) / rowCount;
    else humanHeight = svgHeight;

    var humanWidth = 68 / 176 * humanHeight,
        humanPerRow = Math.floor(svgWidth / (humanWidth + 2)),
        humanInLastRow = allHuman - (rowCount - 1) * humanPerRow,
        rowInRed = Math.floor(redhumans / humanPerRow),
        redHumanInLastRow = redhumans - (rowInRed) * humanPerRow;

    for (var i = 0; i < rowCount; i++) {
        for (var j = 0; j < humanPerRow; j++) {
            var currentHumanColor = redHumanImage;
            if ((i == rowInRed && j >= redHumanInLastRow) || i > rowInRed) {
                currentHumanColor = greenHumanImage;
            }
            if (j >= allHuman - (i * humanPerRow)) break;

            var svgimg = document.createElementNS('http://www.w3.org/2000/svg', 'image');
            svgimg.setAttribute('height', humanHeight);
            svgimg.setAttribute('width', humanWidth);
            svgimg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', currentHumanColor);
            svgimg.setAttribute('x', humanWidth * j + 2 * j);
            svgimg.setAttribute('y', humanHeight * i + 2 * i);
            svgDocument.appendChild(svgimg);
        }
    }
}


//function draw line chart
function drawLineChart(rect, polygol, text, width) {
    rect.setAttribute("width", width + 0.55);
    polygol.setAttribute("x", width + 10);
    text.setAttribute("x", width - 90);
}

//function draw growth chart
function drawGrowthChart(percent, chartid) {
    var svgns = "http://www.w3.org/2000/svg";
    var growthChartColumnHeight = [30, 37.5, 45, 52.5, 60, 67.5, 75, 82.5, 90, 97.5];
    var growthChartColumnWidth = 11.5;
    var growthChartHorizonPosition = [0, 15.5, 30.5, 46, 61.5, 77, 92, 107.5, 123, 139];
    var chartHeight = 106;
    var mainColor = "rgb(142, 198, 65)";
    var grayColor = "rgb(189, 190, 192)";

    var svgDocument = document.getElementById(chartid);
    var percentPerTen = percent / 10;

    for (i = 0; i < 10; i++) {
        var currentColumnWidth = growthChartColumnWidth;
        var currentColumnColor = mainColor;
        if (i <= percentPerTen && percentPerTen < i + 1) {
            currentColumnWidth = (percent % 10) / 10 * growthChartColumnWidth;
        } else if (i > percentPerTen) {
            currentColumnColor = grayColor;
        }

        var shape = document.createElementNS(svgns, "rect");
        shape.setAttributeNS(null, "x", growthChartHorizonPosition[i]);
        shape.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
        shape.setAttributeNS(null, "width", currentColumnWidth);
        shape.setAttributeNS(null, "height", growthChartColumnHeight[i]);
        shape.setAttributeNS(null, "fill", currentColumnColor);

        var shape1 = null;
        if (currentColumnWidth < growthChartColumnWidth) {
            shape1 = document.createElementNS(svgns, "rect");
            shape1.setAttributeNS(null, "x", growthChartHorizonPosition[i] + currentColumnWidth - 0.5);
            shape1.setAttributeNS(null, "y", chartHeight - growthChartColumnHeight[i]);
            shape1.setAttributeNS(null, "width", growthChartColumnWidth - currentColumnWidth);
            shape1.setAttributeNS(null, "height", growthChartColumnHeight[i]);
            shape1.setAttributeNS(null, "fill", grayColor);
        }

        svgDocument.appendChild(shape);
        if (shape1 != null) svgDocument.appendChild(shape1);
    }
}

function drawPiechart(chartid, percent) {
    var chart = document.getElementById(chartid);
    var abtext1 = document.getElementById("abtext1");
    var abtext2 = document.getElementById("abtext2");
    var textpercent = document.getElementById("piechart-percent");

    abtext2_newy = 150 + 105 * Math.cos(Math.PI * (1 - percent / 100)) - abtext1.offsetHeight / 2;
    abtext2_newx = 90 + 105 * Math.sin(Math.PI * (1 - percent / 100)) - abtext1.offsetWidth / 2;
    abtext1_newy = 150 - 105 * Math.cos(Math.PI * (1 - percent / 100)) - abtext2.offsetHeight / 2;
    abtext1_newx = 90 - 105 * Math.sin(Math.PI * (1 - percent / 100)) - abtext2.offsetWidth / 2;

//		abtext1.style.marginTop = abtext1_newy;
//		abtext1.style.marginLeft = abtext1_newx;
//
//		abtext2.style.marginTop = abtext2_newy;
//		abtext2.style.marginLeft = abtext2_newx;

    $('#abtext1').css({
        marginTop: abtext1_newy,
        marginLeft: abtext1_newx
    });
    $('#abtext2').css({
        marginTop: abtext2_newy,
        marginLeft: abtext2_newx
    })

    // chart.setAttribute("data-text", percent + "%");
    textpercent.innerHTML = percent + "%";
    chart.setAttribute("data-percent", percent);

    $('#' + chartid).circliful();
}

function calculateWidth(maxWidth, maxValue, value) {
    var width = 0;
    width = (value / maxValue) * maxWidth;

    return width;
}