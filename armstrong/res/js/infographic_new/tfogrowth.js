$(document).ready(function(){
	var ctx = document.getElementById("existBar"); /*$("#existBar");*/
	var ctx1 = $("#newBar");

	var existBar = new Chart(ctx, {
    	type: 'horizontalBar',
    	data: {
        	labels: ["Existing SKU"],
        	datasets: [{
	            fill: true,
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            data: [550] 
        	},{
        		fill: true,
	            backgroundColor: 'rgb(255, 139, 133)',
	            borderColor: 'rgb(255, 139, 133)',
	            fontColor: '#FFF',
	            borderWidth: 2,
	            data: [550] 
        	}]
    	},
    	options: {
    		maintainAspectRatio: true,
    		labels: {
    			display: true
    		},
        	responsive: true,
	        legend: { 
	            display: false, 
	            position: 'right'
	        },
        	scales: {
        		xAxes: [{
	        		display: false,
	        		barThickness: 1,
	        		gridlines: {
	        			display: false,
	                    color: 'rgb(255,255,255)'
	        		}
        		}],
        		yAxes: [{
	        		ticks : {
	        			max: 1000,
	        			fontFamily: 'avenirMed',
	                    fontsize: '8pt',
	                    fontColor: '#327782'
	        		},
	        		gridlines: {
	        			display: false,
	                    color: 'rgb(255,255,255)'
	        		}
        		}]
        	},
        	animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;
		          	//ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		          	ctx.textAlign = 'left';
		          	ctx.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		console.log(model);
		              		ctx.fillText("$"+label, left + 15, model.y + 8);
		            	}
		          	});               
		        }
		    }
    	}
    });

	/*new bar*/
	var newBar = new Chart(ctx1, {
    	type: 'horizontalBar',
    	data: {
        	labels: ["New SKU"],
        	datasets: [{
	            fill: true,
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            data: [450] 
        	},{
        		fill: true,
	            backgroundColor: 'rgb(255, 139, 133)',
	            borderColor: 'rgb(255, 139, 133)',
	            fontColor: '#FFF',
	            borderWidth: 1,
	            data: [380] 
        	}]
    	},
    	options: {
    		labels: {
    			display: true
    		},
    		elements: {
	            rectangle: {
	                borderWidth: 2,
	                borderColor: '#4BD5E2',
	                borderSkipped: 'left'
	            }
        	},
        	responsive: true,
	        legend: { 
	            display: false, 
	            position: 'right'
	        },
        	scales: {
        		xAxes: [{
	        		display: false,
	        		ticks : {
	        			min: 0,
	        			max: 1000,
	        			fontFamily: 'avenirMed',
	                    fontsize: '8pt',
	                    fontColor: '#327782'
	        		},
	        		barThickness: 1,
	        		gridlines: {
	        			display: true,
	                    color: 'rgb(255,255,255)'
	        		}
        		}],
        		yAxes: [{
	        		ticks : {
	        			fontFamily: 'avenirMed',
	                    fontsize: '8pt',
	                    fontColor: '#327782'
	        		},
	        		gridlines: {
	        			display: true,
	                    color: 'rgb(255,255,255)'
	        		}
        		}]
        	},
        	animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;
		          	//ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		          	ctx.textAlign = 'left';
		          	ctx.textBaseline = 'bottom';
		          	ctx1.textAlign = 'left';
		          	ctx1.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		console.log(model);
		              		ctx.fillText("$"+label, left + 15, model.y + 8);
		            	}
		          	});               
		        }
		    }
    	}
    });
});