$(document).ready(function(){
	var ctx = document.getElementById("call-criteria");
	var callCriteria = new Chart(ctx, {
    	type: 'horizontalBar',
    	data: {
        	labels: ["Sampling", "Pantry Check", "TFO", "Rich Media Demo", "Competitor Info", " Personal Objective"],
	        datasets: [{
	            fill: true,
	            backgroundColor: '#4BD5E2',
	            borderColor: '#4BD5E2',
	            fontColor: '#FFF',
	            borderWidth: 2,
	            data: [350, 430, 640, 830, 900, 920] 
	        }]
	    },
    	options: {
    		events: false,
    		showToolTips: false,
	    	labels: {
	    		display: false
	    	},
	    	/*elements: {
	            rectangle: {
	                borderWidth: 2,
	                borderColor: '#4BD5E2',
	                borderSkipped: 'left'
	            }
	        },*/
	        responsive: true,
	        maintainAspectRation: false,
	        legend: { 
	            display: false, 
	            position: 'right'
	        },
	        scales: {
	        	xAxes: [{
	        		display: false,
	        		ticks : {
	        			min: 0,
	        			max: 1000,
	        			stepSize: 500,
	        			fontFamily: 'avenirMed',
	                    fontsize: '8pt',
	                    fontColor: '#327782'
	        		},
	        		gridlines: {
	        			display: true,
	                    color: '#F1F2F2'
	        		}
	        	}],
	        	yAxes: [{
	        		display: true,
	        		gridlines: {
	        			display: true,
	                    color: '#F1F2F2'
	        		},
	        		ticks : {
	        			fontFamily: 'avenirMed',
	                    fontsize: '8pt',
	                    fontColor: '#327782'
	        		}
	        	}]
	        },
	        animation: {
		        onComplete: function () {
		          	var ctx = this.chart.ctx;
		          	//ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		          	ctx.textAlign = 'left';
		          	ctx.textBaseline = 'bottom';

		          	this.data.datasets.forEach(function (dataset) {
		            	for (var i = 0; i < dataset.data.length; i++) {
		              		var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		                  	left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
		              		ctx.fillStyle = '#327782'; // label color
		              		var label = dataset.data[i]; //model.label
		              		console.log(model);
		              		ctx.fillText(label+"/1000 ", left + 15, model.y + 8);
		              		ctx.fillText("("+label*0.10+"%)", left + 70, model.y + 8);
		            	}
		          	});               
		        }
		    }
		}
    })
});