
     var chart;

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        //alert(testData.length);

        var arr = new Array([]);
        arr[0] =  ['Countries', 'Countries', 'Fulfilled all Criterias', {'role': 'style'},{type: 'string', role: 'annotation'}];
        //arr[1] = [testData[0].countryCode,  165,      938];

                
        for(var i=0;i<(chartData.length);i++){
          arr[i+1] = [chartData[i].countryCode , parseInt(chartData[i].activeCustomerCount), parseInt(chartData[i].fiveCriteriaPercentage)/100, 
          'point { size: 5; fill-color: #333333; }', parseInt(chartData[i].fiveCriteriaPercentage) + '%' ];
         
        }
        
         var data = google.visualization.arrayToDataTable(arr);

//Id.substring(1, chartData[i].countryId.length-1)

    var options = {
      title : 'Regional Customer Profiling'  ,
      vAxes: {0:{viewWindowMode:'explicit',
                      viewWindow:{
                                 
                                  min:0
                                  },
                                  title: 'Customer Count'
                                  },
              1:{format:"#%"
                } 
              },
      hAxis: {title: 'Countries',
              slantedText: true, 
              slantedTextAngle: 45},
      pointSize: 5,
      seriesType: 'bars',
      series: {1: {targetAxisIndex:1,
            type: 'line'}},
      width: 1000,
      height: 500,
      bar: {groupWidth: 45},
      fontSize: 13
    };

    chart = new google.visualization.ComboChart(document.getElementById('g_chart'));
    chart.draw(data, options);
  }
    

    function drawChart() {
        // Some raw data (not necessarily accurate)
        //alert(testData.length);
        var arr = new Array([]);
        arr[0] =  ['Time Period', 'Synced within 0-2 Days', {'role': 'annotation'}, {type: 'string', 'role': 'tooltip'}, 'Synced after 3 Days', {'role': 'annotation'}, {type: 'string', 'role': 'tooltip'}];
        //arr[1] = [testData[0].countryCode,  165,      938];

                
        for(var i=0;i<(chartData.length);i++){
          arr[i+1] = [chartData[i].countryCode, parseInt(chartData[i].zeroTwoDaysCount), parseInt(chartData[i].zeroTwoDaysCount), chartData[i].zeroTwoDaysCountSRNames + '\nTotal: ', parseInt(chartData[i].threeFiveDaysCount), parseInt(chartData[i].threeFiveDaysCount), chartData[i].threeFiveDaysCountSRNames + '\nTotal: '];
         
        }
        
        var data = google.visualization.arrayToDataTable(arr);
 
//Id.substring(1, chartData[i].countryId.length-1)

        var options_fullStacked = {
            title : 'Regional Sync Report'  ,
            width: 1000,
            height: 500,
            bar: {groupWidth: '60%'},
            colors: ['#009933', '#cc0000'],
            legend: { position: 'top', maxLines: 3 },
            isStacked: 'percent',
            vAxis: {
              title: 'Country',
              minValue: 0
            },
            hAxis: {
              title: 'Calls Synced',
              minValue: 0
            },
            annotations: {
              textStyle: {
                fontSize: 11,
                color: '#000000',
                //auraColor: '#ffffff'
              }
            },
            tooltip: { textStyle: { fontSize: 11.5 } }
        };

          chart = new google.visualization.ColumnChart(document.getElementById('g_chart'));
          chart.draw(data, options_fullStacked);
    }