/**
 * Created by hung on 09/04/2015.
 */
var total;
var view_type;
function generateHtmlReport(data, view) {
    var html = '',
        arr_title;
    view_type = view;
    if (view == 0 || view == 1) { // 0:SKU; 1:Channel
        arr_title = total = data[0];
        var table_name = (view == 0) ? 'SKU Report' : 'Channel Report';
        html += '<div class="block_table"><div class="table-title"><span>' + table_name + '</span></div>';
        html += '<div class="table-wrapper"><table class="table table-channel display" id="example" >';
        html += '<thead>';
        arr_title = getTitle(arr_title);
        html += generateEmptyTd(Object.keys(arr_title).length);
        html += '<tr>';
        html += '<th class="spacer"></th>';
        html += '<th class="border-right" >';
        html += '<a href="#self" class="asc" data-key="skuName">';
        html += (view == 0) ? 'Sku Name' : 'Channel';
        html += '<i class="fa fa-sort-up"></i></a></th>';
        $.each(arr_title, function (k, l) {
            if (l !== null) {
                html += '<th >';
                html += '<a href="#self" class="sort" data-key="' + k + '">' + l + '<i class="fa fa-sort"></i></a></th>';
            }
        });
        //for (var l = 0; l < arr_title.length; l++) {
        //    if ($.inArray(l, [0, 4, 5]) != -1) {
        //        html += '<th class="border-right">';
        //    } else {
        //        html += '<th>';
        //    }
        //    if(l == 0){
        //        html += '<a class="asc">' + arr_title[l] + '<i class="fa fa-sort-down"></i></a></th>';
        //    }else{
        //        html +=  '<a class="">' + arr_title[l] + '<i class="fa fa-sort"></i></a></th>';
        //    }
        //
        //}
        html += '<th class="spacer"></th>';
        html += '</tr></thead>';
        html += '<tbody>';
        var count = 0;
        var total_count = 0;
        total = removeNullVariables(total);
        if (total.skuName == ".Total") {
            html += generateEmptyTd(Object.keys(arr_title).length);
            html += '<tr style="background-color: #fff; color: #fff">';
            html += '<td class="spacer"></td>';
            html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
            $.each(total, function (k, v) {
                if (k !== 'skuName' && k !== 'child') {
                    total_count++;
                    if (total_count == Object.keys(total).length - 2) {
                        html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(v, 0) + '</div><div>' + getSplitValue(v, 1) + '</div></td>';
                    } else {
                        html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(v, 0) + '</div><div>' + getSplitValue(v, 1) + '</div></td>';
                    }
                }
            });
            html += '<td class="spacer"></td>';
            html += '</tr>';
            html += generateEmptyTd(Object.keys(arr_title).length);
        }

        $.each(data, function (k, l) {
            //if(i % per_page === 0 ){
            //    j++;
            //}
            //if(j === current_page){
            //    html += '<tr data-paging="'+(j)+'">';
            //}else{
            //    html += '<tr data-paging="'+(j)+'" class="hidden">';
            //}
            if (view == 0) {
                var child_sku = l.child;
                html += generateEmptyTd(Object.keys(arr_title).length);
                html += '<tr>';
                html += '<td class="spacer"></td>';
                html += '<td style="background-color: #E6E7E8;">' + l.skuName + '</td>';
                total_count = 0;
                $.each(arr_title, function (key, val) {
                    html += '<td style="background-color: #E6E7E8;"><div>' + getSplitValue(l[key], 0) + '</div><div>' + getSplitValue(l[key], 1) + '</div></td>';
                });
                html += '<td class="spacer"></td>';
                html += '</tr>';

                if (l.skuName != "unassigned") {
                    html += generateEmptyTd(Object.keys(arr_title).length);
                }
                if (child_sku.length > 0) {
                    for (var j = 0; j < child_sku.length; j++) {
                        html += '<tr>';
                        html += '<td class="spacer"></td>';
                        html += '<td class="border-right" style="background-color: #D1D3D4">' + (j + 1) + '. ' + child_sku[j].skuName + '</td>';
                        $.each(arr_title, function (key, val) {
                            html += '<td style="background-color: #D1D3D4"><div>' + getSplitValue(child_sku[j][key], 0) + '</div><div>' + getSplitValue(child_sku[j][key], 1) + '</div></td>';
                        });
                        html += '<td class="spacer"></td>';
                        html += '</tr>';
                    }
                }
            } else {
                if (l.skuName !== ".Total") {
                    count++;

                    html += '<tr>';
                    html += '<td class="spacer"></td>';
                    //html += '<td class="border-right">' + count + '. ' + val + '</td>';
                    $.each(removeNullVariables(l), function (key, val) {
                        if (key != 'child') {
                            if (key == 'skuName') {
                                html += '<td class="border-right">' + count + '. ' + val + '</td>';
                            } else {
                                html += '<td><div>' + getSplitValue(l[key], 0) + '</div><div>' + getSplitValue(l[key], 1) + '</div></td>';
                            }
                        }
                    });
                    html += '<td class="spacer"></td>';
                    html += '</tr>';
                }
            }
        });
        html += generateEmptyTd(Object.keys(arr_title).length);
        html += '</tbody>';
        html += '</table></div></div>';
    } else if (view == 3) { // Chanel/SKU
        arr_title = total = data[0].child;
        html += '<div class="block_table"><div class="table-title"><span>Channel/SKU Report</span></div>';
        html += '<div class="table-wrapper table-responsive"><table class="table table-channel-sku display" id="example" >';
        html += '<thead>';
        html += generateEmptyTd(arr_title.length);
        html += '<tr>';
        html += '<th class="spacer"></th>';
        html += '<th class="border-right"><a href="#self" class="asc" data-key="channel-name">Channel<i class="fa fa-sort-up"></i></a></th>';
        for (var l = 0; l < arr_title.length; l++) {
            html += '<th><a href="#self" data-key="' + l + '" class="sort">' + arr_title[l].name + '<i class="fa fa-sort"></i></a></th>';
        }
        html += '<th class="spacer"></th>';
        html += '</tr></thead>';
        html += '<tbody>';
        html += generateEmptyTd(arr_title.length);
        html += '<tr style="background-color: #fff; color: #fff">';
        html += '<td class="spacer"></td>';
        html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
        for (var h = 0; h < total.length; h++) {
            if (h == total.length - 1) {
                html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(total[h].value, 0) + '</div><div>' + getSplitValue(total[h].value, 1) + '</div></td>';
            } else {
                html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(total[h].value, 0) + '</div><div>' + getSplitValue(total[h].value, 1) + '</div></td>';
            }
        }
        html += '<td class="spacer"></td>';
        html += '</tr>';
        html += generateEmptyTd(arr_title.length);
        var count = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].key != '.Total') {
                var child = data[i].child;
                count++;
                html += '<tr>';
                html += '<td class="spacer"></td>';
                html += '<td class="border-right">' + count + '. ' + data[i].name + '</td>';
                for (var j = 0; j < child.length; j++) {
                    html += '<td><div>' + getSplitValue(child[j].value, 0) + '</div><div>' + getSplitValue(child[j].value, 1) + '</div></td>';
                }
                html += '<td class="spacer"></td>';
                html += '</tr>';
            }
        }

        html += generateEmptyTd(arr_title.length);
        html += '</tbody>';
        html += '</table></div></div>';
    } else if (view == 5 || view == 6) { // 5: SR/SKU; 6: SR/Channel
        var table_title = (view == 5) ? 'SR/SKU Report' : 'SR/Channel Report';
        arr_title = total = data[0].values;
        html += '<div class="block_table"><div class="table-title"><span>' + table_title + '</span></div>';
        html += '<div class="table-wrapper table-responsive"><table class="table table-sr-sku display" id="example" >';
        html += '<thead>';
        html += generateEmptyTd(arr_title.length);
        html += '</tr>';
        html += '<th class="spacer"></th>';
        html += '<th class="border-right"><a href="#self" class="asc" data-key="channel-name">Salesperson<i class="fa fa-sort-up"></i></a></th>';
        for (var l = 0; l < arr_title.length; l++) {
            html += '<th><a href="#self" data-key="' + l + '" class="sort">' + arr_title[l].name + '<i class="fa fa-sort"></i></a></th>';
        }
        html += '<th class="spacer"></th>';
        html += '</tr></thead>';
        html += '<tbody>';
        html += generateEmptyTd(arr_title.length);
        html += '<tr style="background-color: #fff; color: #fff">';
        html += '<td class="spacer"></td>';
        html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
        for (var k = 0; k < arr_title.length; k++) {
            if (k == total.length - 1) {
                html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(total[k].value, 0) + '</div><div>' + getSplitValue(total[k].value, 1) + '</div></td>';
            } else {
                html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(total[k].value, 0) + '</div><div>' + getSplitValue(total[k].value, 1) + '</div></td>';
            }
        }
        html += '<td class="spacer"></td>';
        html += '</tr>';
        for (var i = 0; i < data.length; i++) {
            if (data[i].key != ".Total") {
                var salesperson = data[i].child;
                html += generateEmptyTd(arr_title.length);
                html += '<tr>';
                html += '<td class="spacer"></td>';
                html += '<td style="background-color: #E6E7E8;">' + data[i].name + '</td>';
                for (var h = 0; h < arr_title.length; h++) {
                    html += '<td style="background-color: #E6E7E8;"><div>' + getSplitValue(data[i].values[h].value, 0) + '</div><div>' + getSplitValue(data[i].values[h].value, 1) + '</div></td>';
                }
                html += '<td class="spacer"></td>';
                html += '</tr>';
                if (data[i].key != "unassigned") {
                    html += generateEmptyTd(arr_title.length);
                }
                if (salesperson.length > 0) {
                    for (var j = 0; j < salesperson.length; j++) {
                        var child = salesperson[j].child;
                        html += '<tr>';
                        html += '<td class="spacer"></td>';
                        html += '<td class="border-right" style="background-color: #D1D3D4">' + salesperson[j].name + '</td>';
                        for (var k = 0; k < child.length; k++) {
                            html += '<td style="background-color: #D1D3D4"><div>' + getSplitValue(child[k].value, 0) + '</div><div>' + getSplitValue(child[k].value, 1) + '</div></td>';
                        }
                        html += '<td class="spacer"></td>';
                        html += '</tr>';
                    }
                }
            }
        }
        html += generateEmptyTd(arr_title.length);
        html += '</tbody>';
        html += '</table></div></div>';
    }
    return html;
}

function generateGripGrabHtmlReport(data) {
    var html = '',
        header_title = data[0];
    var count = 0;
    html += '<div class="block_table"><div class="table-title"><span>Grip/Grab Report</span></div>';
    html += '<div class="table-wrapper table-responsive"><table class="table table-grip-grab display" id="example" >';
    html += '<thead>';
    header_title = getGripGrabTitle(header_title);
    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '<tr>';
    html += '<th class="spacer"></th>';
    html += '<th class="border-right"><a href="javascript:;" data-key="armstrong2SalespersonsId" class="sort">SR ID <i class="fa fa-sort"></i></a></th>';
    html += '<th><a href="javascript:;" class="asc" data-key="salespersonsName">SR Name<i class="fa fa-sort-up"></i></a></th>';
    $.each(header_title, function (k, v) {
        if (k != 'salespersonsName' && k != 'armstrong2SalespersonsId') {

            if (k !== 'armstrong2SalespersonsId') {
                html += '<th><a href="javascript:;" data-key="' + k + '" class="sort">' + v + '<i class="fa fa-sort"></i></a></th>';
            } else {
                html += '<th class="border-right"><a href="javascript:;" class="asc" data-key="' + k + '">' + v + '<i class="fa fa-sort-up"></i></a></th>';
            }
        }

    });
    html += '<th class="spacer"></th>';
    html += '</tr></thead>';
    html += '<tbody>';
    html += generateEmptyTd(Object.keys(header_title).length - 1);

    var count = 0;
    for (var i = 0; i < data.length; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(data[i], function (key, val) {

            if (key != 'salespersonsName' && key != 'armstrong2SalespersonsId') {
                $.each(header_title, function (k, value) {

                    if (key == k) {
                        if (key == 'armstrong2SalespersonsId') {
                            html += '<td class="border-right">' + count + '. ' + val + '</td>';
                        } else {
                            html += '<td>' + val + '</td>';
                        }
                    }
                });
            } else if (key == 'salespersonsName') {
                html += '<td class="border-right">' + data[i].armstrong2SalespersonsId + '</td>';
                html += '<td>' + count + '. ' + data[i].salespersonsName + '</td>';


            }
        });
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }

    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '</tbody>';
    html += '</table></div></div>';
    return html;
}

function generateRichMediaHtmlReport(data) {
    var html = '',
        header_title = data[0];
    html += '<div class="block_table"><div class="table-title"><span>Rich Media Report</span></div>';
    html += '<div class="table-wrapper table-responsive"><table class="table table-rich-media display" id="example" >';
    html += '<thead>';
    header_title = getRichMediaTitle(header_title);
    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '<tr>';
    html += '<th class="spacer"></th>';
    $.each(header_title, function (k, v) {
        if (k !== 'skuName') {
            html += '<th><a href="javascript:;" data-key="' + k + '" class="sort">' + v + '<i class="fa fa-sort"></i></a></th>';
        } else {
            html += '<th class="border-right"><a href="javascript:;" class="asc" data-key="' + k + '">' + v + '<i class="fa fa-sort-up"></i></a></th>';
        }
    });
    html += '<th class="spacer"></th>';
    html += '</tr></thead>';
    html += '<tbody>';
    html += generateEmptyTd(Object.keys(header_title).length - 1);

    var count = 0;
    for (var i = 0; i < data.length; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(data[i], function (key, val) {
            $.each(header_title, function (k, value) {
                if (key == k) {
                    if (key == 'skuName') {
                        html += '<td class="border-right">' + count + '. ' + val + '</td>';
                    } else {
                        html += '<td>' + val + '</td>';
                    }
                }
            });
        });
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }

    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '</tbody>';
    html += '</table></div></div>';
    return html;
}

function generatePromotionActivityHtmlReport(data) {
    var html = '',
        header_title = data[0];
    html += '<div class="block_table"><div class="table-title"><span>Promotion Activity Report</span></div>';
    html += '<div class="table-wrapper table-responsive"><table class="table table-promotion-activity display" id="example" >';
    html += '<thead>';
    header_title = getPromotionActivityTitle(header_title);
    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '<tr>';
    html += '<th class="spacer"></th>';
    $.each(header_title, function (k, v) {
        if (k !== 'armstrong2SalespersonsId') {
            html += '<th><a href="javascript:;" data-key="' + k + '" class="sort">' + v + '<i class="fa fa-sort"></i></a></th>';
        } else {
            html += '<th class="border-right"><a href="javascript:;" class="asc" data-key="' + k + '">' + v + '<i class="fa fa-sort-up"></i></a></th>';
        }
    });
    html += '<th></th>';
    html += '<th class="spacer"></th>';
    html += '</tr></thead>';
    html += '<tbody>';
    html += generateEmptyTd(Object.keys(header_title).length - 1);

    var count = 0;
    for (var i = 0; i < data.length; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(data[i], function (key, val) {
            $.each(header_title, function (k, value) {
                if (key == k) {
                    if (key == 'armstrong2SalespersonsId') {
                        html += '<td class="border-right activity-id" data-id="' + data[i].id + '">' + count + '. ' + val + '</td>';
                    } else if (key == 'participate') {
                        if (val == true) {
                            html += '<td><span class="checkbox-checked"></span></td>';
                        } else {
                            html += '<td><span class="checkbox-unchecked"></span></td>';
                        }
                    } else {
                        html += '<td>' + val + '</td>';
                    }
                }
            });
        });
        if (data[i].participate == true) {
            html += '<td><button type="button" class="btn-primary pdf-generate">PDF</td>';
        } else {
            html += '<td></td>';
        }
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }

    html += generateEmptyTd(Object.keys(header_title).length - 1);
    html += '</tbody>';
    html += '</table></div></div>';
    return html;
}

function generateEmptyTd(total) {
    var html = '<tr class="spacer">';
    html += '<td></td>';
    for (var i = 0; i <= total; i++) {
        if (total[i] !== null) {
            html += '<td></td>';
        }
    }
    html += '<td></td>';
    html += '</tr>';
    return html;
}

function pickColor(arr_color) {
    if (colorFlag == 0) {
        colorFlag = 1;
        return arr_color[0];
    } else if (colorFlag == 1) {
        colorFlag = 0;
        return arr_color[1];
    }
}

function removeNullVariables(obj) {
    for (var i in obj) {
        if (obj[i] === null || obj[i] === undefined) {
            delete obj[i];
        }
    }
    return obj;
}

function getTitle(arr_title) {
    var label = {};
    $.each(arr_title, function (k, l) {
        if (l != null) {
            switch (k) {
                case 'penetrationRolling3Month':
                    label['penetrationRolling3Month'] = 'Penetration % (rolling 3 months)';
                    break;
                case 'penetrationRolling6Month':
                    label['penetrationRolling6Month'] = 'Penetration % (rolling 6 months)';
                    break;
                case 'penetrationRolling12Month':
                    label['penetrationRolling12Month'] = 'Penetration % (rolling 12 months)';
                    break;
                case 'penetrationJanToJun':
                    label['penetrationJanToJun'] = 'Penetration % (Jan-Jun) last year';
                    break;
                case 'penetrationJulToDec':
                    label['penetrationJulToDec'] = 'Penetration % (Jul-Dec) last year)';
                    break;
                case 'penetration1Year':
                    label['penetration1Year'] = '1 year Penetration last year';
                    break;
            }
        }
    });
    return label;
}

function getGripGrabTitle(arr_title) {
    var label = {};
    $.each(arr_title, function (k, l) {
        if (l != null) {
            switch (k) {
                case 'salesLeaderName':
                    label['salesLeaderName'] = 'SR Manager Name';
                    break;
                case 'salespersonsManagerId':
                    label['salespersonsManagerId'] = 'SR Manager ID';
                    break;
                case 'salespersonsName':
                    label['salespersonsName'] = 'SR Name';
                    break;
                case 'armstrong2SalespersonsId':
                    label['armstrong2SalespersonsId'] = 'SR ID';
                    break;
                case 'newGrip':
                    label['newGrip'] = 'New Grip';
                    break;
                case 'totalLostGrip':
                    label['totalLostGrip'] = 'Lost Grip';
                    break;
                case 'totalGrip':
                    label['totalGrip'] = 'Total Grip';
                    break;
                case 'newGrab':
                    label['newGrab'] = 'New Grab';
                    break;
                case 'totalLostGrap':
                    label['totalLostGrap'] = 'Lost Grab';
                    break;
                case 'totalGrab':
                    label['totalGrab'] = 'Total Grab';
                    break;
                case 'averageGrabPerGrip':
                    label['averageGrabPerGrip'] = 'Average Grab Per Grip';
                    break;
                case 'alias':
                    label['alias'] = 'Alias';
                    break;
                case 'territory':
                    label['territory'] = 'Territory';
                case 'srStatus':
                    label['srStatus'] = 'Sr Status';
                    break;
            }
        }
    });
    return label;
}

function getRichMediaTitle(arr_title) {
    var label = {};
    $.each(arr_title, function (k, l) {
        if (l != null) {
            switch (k) {
                case 'skuName':
                    label['skuName'] = 'Sku Name';
                    break;
                case 'skuNumber':
                    label['skuNumber'] = 'Sku Number';
                    break;
                case 'listed':
                    label['listed'] = 'Listed';
                    break;
                case 'productImage':
                    label['productImage'] = 'Product Image';
                    break;
                case 'sellingStories':
                    label['sellingStories'] = 'Selling Stories';
                    break;
                case 'recipeTag':
                    label['recipeTag'] = 'Recipe tab';
                    break;
                case 'video':
                    label['video'] = 'Video';
                    break;
                case 'certification':
                    label['certification'] = 'Certification';
                    break;
                case 'productSpecification':
                    label['productSpecification'] = 'Product Specification';
                    break;
            }
        }
    });
    return label;
}

function getPromotionActivityTitle(arr_title) {
    var label = {};
    $.each(arr_title, function (k, l) {
        if (l == null) l = '';
        switch (k) {
            case 'armstrong2SalespersonsId':
                label['armstrong2SalespersonsId'] = 'Salesperson ID';
                break;
            case 'armstrong2SalespersonsName':
                label['armstrong2SalespersonsName'] = 'Armstrong 2 Salesperson Name';
                break;
            case 'armstrong2CustomersName':
                label['armstrong2CustomersName'] = 'Customer Name';
                break;
            case 'contactName':
                label['contactName'] = 'Contact Name';
                break;
            case 'phoneNumber':
                label['phoneNumber'] = 'Phone Number';
                break;
            case 'email':
                label['email'] = 'Email';
                break;
            case 'channel':
                label['channel'] = 'Channel';
                break;
            case 'dateOrder':
                label['dateOrder'] = 'Order Date';
                break;
            case 'lastVisitDate':
                label['lastVisitDate'] = 'Last Visit Date';
                break;
            case 'participate':
                label['participate'] = 'Participate';
            case 'srStatus':
                label['srStatus'] = 'Sr Status';
                break;
        }
    });
    return label;
}

// Penetration Report Channel sorting
$(document).on('click', '#reporting-view .table-channel tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        tbody = $('.table tbody'),
        view = $('select[name="view_by"]').val(),
        sortTag = $('#reporting-view .table tr th a i');
    html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        if (view_type == 0) {
            for (var p = 0; p < responJson.length; p++) {
                child = responJson[p].child;
                child.sort(function (a, b) {
                    var a1 = (key == 'skuName') ? a[key] : parseFloat(a[key].split('%')[0]);
                    var b1 = (key == 'skuName') ? b[key] : parseFloat(b[key].split('%')[0]);
                    if (a1 == b1) return 0;
                    return a1 > b1 ? 1 : -1;
                });
                responJson[p].child = child;
            }
        } else if (view_type == 1) {
            responJson.sort(function (a, b) {
                var a1 = (key == 'skuName') ? a[key] : (a[key] !== null) ? parseFloat(a[key].split('%')[0]) : null;
                var b1 = (key == 'skuName') ? b[key] : (b[key] !== null) ? parseFloat(b[key].split('%')[0]) : null;
                if (a1 == b1) return 0;
                return a1 > b1 ? 1 : -1;
            });
        }

        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        if (view_type == 0) {
            for (var p = 0; p < responJson.length; p++) {
                child = responJson[p].child;
                child.sort(function (a, b) {
                    var a1 = (key == 'skuName') ? a[key] : parseFloat(a[key].split('%')[0]);
                    var b1 = (key == 'skuName') ? b[key] : parseFloat(b[key].split('%')[0]);
                    if (a1 == b1) return 0;
                    return a1 < b1 ? 1 : -1;
                });
                responJson[p].child = child;
            }
        } else if (view_type == 1) {
            responJson.sort(function (a, b) {
                var a1 = (key == 'skuName') ? a[key] : (a[key] !== null) ? parseFloat(a[key].split('%')[0]) : null;
                var b1 = (key == 'skuName') ? b[key] : (b[key] !== null) ? parseFloat(b[key].split('%')[0]) : null;
                if (a1 == b1) return 0;
                return a1 < b1 ? 1 : -1;
            });
        }

        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = total;
    arr_title = getTitle(arr_title);
    var count = 0;
    var total_count = 0;
    if (total.skuName == ".Total") {
        html += generateEmptyTd(Object.keys(arr_title).length);
        html += '<tr style="background-color: #fff; color: #fff">';
        html += '<td class="spacer"></td>';
        html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
        $.each(total, function (k, v) {
            total_count++;
            if (k !== 'skuName' && k !== 'child') {
                if (total_count == Object.keys(total).length - 1) {
                    html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(v, 0) + '</div><div>' + getSplitValue(v, 1) + '</div></td>';
                } else {
                    html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(v, 0) + '</div><div>' + getSplitValue(v, 1) + '</div></td>';
                }
            }
        });
        html += '<td class="spacer"></td>';
        html += '</tr>';
        html += generateEmptyTd(Object.keys(arr_title).length);
    }

    $.each(responJson, function (k, l) {
        if (view_type == 0) {
            var child_sku = l.child;
            html += generateEmptyTd(Object.keys(arr_title).length);
            html += '<tr>';
            html += '<td class="spacer"></td>';
            html += '<td style="background-color: #E6E7E8;">' + l.skuName + '</td>';
            total_count = 0;

            $.each(arr_title, function (key, val) {
                html += '<td style="background-color: #E6E7E8;"><div>' + getSplitValue(l[key], 0) + '</div><div>' + getSplitValue(l[key], 1) + '</div></td>';
            });
            html += '<td class="spacer"></td>';
            html += '</tr>';

            if (l.skuName != "unassigned") {
                html += generateEmptyTd(Object.keys(arr_title).length);
            }
            if (child_sku.length > 0) {
                for (var j = 0; j < child_sku.length; j++) {
                    html += '<tr>';
                    html += '<td class="spacer"></td>';
                    html += '<td class="border-right" style="background-color: #D1D3D4">' + (j + 1) + '. ' + child_sku[j].skuName + '</td>';
                    $.each(arr_title, function (key, val) {
                        html += '<td style="background-color: #D1D3D4"><div>' + getSplitValue(child_sku[j][key], 0) + '</div><div>' + getSplitValue(child_sku[j][key], 1) + '</div></td>';
                    });
                    html += '<td class="spacer"></td>';
                    html += '</tr>';
                }
            }
        } else if (view_type == 1) {
            if (l.skuName !== ".Total") {
                count++;

                html += '<tr>';
                html += '<td class="spacer"></td>';
                $.each(removeNullVariables(l), function (key, val) {
                    if (key != 'child') {
                        if (key == 'skuName') {
                            html += '<td class="border-right">' + count + '. ' + val + '</td>';
                        } else {
                            html += '<td><div>' + getSplitValue(l[key], 0) + '</div><div>' + getSplitValue(l[key], 1) + '</div></td>';
                        }
                    }
                });
                html += '<td class="spacer"></td>';
                html += '</tr>';
            }
        }

    });
    html += generateEmptyTd(Object.keys(arr_title).length);
    tbody.html(html);
});

// Penetration Report Channel SKU sorting
$(document).on('click', '#reporting-view .table-channel-sku tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        tbody = $('.table tbody'),
        sortTag = $('#reporting-view .table tr th a i'),
        html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        responJson.sort(function (a, b) {
            var a1 = (key == 'channel-name') ? a.name : parseFloat(a.child[key].value.split('%')[0]);
            var b1 = (key == 'channel-name') ? b.name : parseFloat(b.child[key].value.split('%')[0]);
            if (a1 == b1) return 0;
            return a1 > b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        responJson.sort(function (a, b) {
            var a1 = (key == 'channel-name') ? a.name : parseFloat(a.child[key].value.split('%')[0]);
            var b1 = (key == 'channel-name') ? b.name : parseFloat(b.child[key].value.split('%')[0]);
            if (a1 == b1) return 0;
            return a1 < b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = responJson[0].child;
    html += generateEmptyTd(arr_title.length);
    html += '<tr style="background-color: #fff; color: #fff">';
    html += '<td class="spacer"></td>';
    html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
    for (var h = 0; h < total.length; h++) {
        if (h == total.length - 1) {
            html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(total[h].value, 0) + '</div><div>' + getSplitValue(total[h].value, 1) + '</div></td>';
        } else {
            html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(total[h].value, 0) + '</div><div>' + getSplitValue(total[h].value, 1) + '</div></td>';
        }
    }
    html += '<td class="spacer"></td>';
    html += '</tr>';
    html += generateEmptyTd(arr_title.length);
    var count = 0;
    for (var i = 0; i < total_data; i++) {
        if (responJson[i].key != ".Total") {
            var child = responJson[i].child;
            count++;
            html += '<tr>';
            html += '<td class="spacer"></td>';
            html += '<td class="border-right">' + count + '. ' + responJson[i].name + '</td>';
            for (var j = 0; j < child.length; j++) {
                html += '<td><div>' + getSplitValue(child[j].value, 0) + '</div><div>' + getSplitValue(child[j].value, 1) + '</div></td>';
            }
            html += '<td class="spacer"></td>';
            html += '</tr>';
        }
    }
    html += generateEmptyTd(arr_title.length);
    tbody.html(html);
});

// Penetration Report SR SKU sorting
$(document).on('click', '#reporting-view .table-sr-sku tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        child,
        tbody = $('.table tbody'),
        sortTag = $('#reporting-view .table tr th a i'),
        html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        for (var p = 0; p < responJson.length; p++) {
            child = responJson[p].child;
            child.sort(function (a, b) {
                var a1 = (key == 'channel-name') ? a.name : parseFloat(a.child[key].value.split('%')[0]);
                var b1 = (key == 'channel-name') ? b.name : parseFloat(b.child[key].value.split('%')[0]);
                if (a1 == b1) return 0;
                return a1 > b1 ? 1 : -1;
            });
            responJson[p].child = child;
        }
        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        for (var p = 0; p < responJson.length; p++) {
            child = responJson[p].child;
            child.sort(function (a, b) {
                var a1 = (key == 'channel-name') ? a.name : parseFloat(a.child[key].value.split('%')[0]);
                var b1 = (key == 'channel-name') ? b.name : parseFloat(b.child[key].value.split('%')[0]);
                if (a1 == b1) return 0;
                return a1 < b1 ? 1 : -1;
            });
            responJson[p].child = child;
        }
        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = responJson[0].values;
    html += generateEmptyTd(arr_title.length);
    html += '<tr style="background-color: #fff; color: #fff">';
    html += '<td class="spacer"></td>';
    html += '<td class="group round-start" style="background-color: #3abfa8">Total</td>';
    for (var k = 0; k < arr_title.length; k++) {
        if (k == total.length - 1) {
            html += '<td class="group round-end" style="background-color: #3abfa8"><div>' + getSplitValue(total[k].value, 0) + '</div><div>' + getSplitValue(total[k].value, 1) + '</div></td>';
        } else {
            html += '<td class="group" style="background-color: #3abfa8"><div>' + getSplitValue(total[k].value, 0) + '</div><div>' + getSplitValue(total[k].value, 1) + '</div></td>';
        }
    }
    html += '<td class="spacer"></td>';
    html += '</tr>';
    //html += generateEmptyTd(arr_title.length);
    for (var i = 0, j = 0; i < total_data; i++) {
        if (responJson[i].key != ".Total") {
            var salesperson = responJson[i].child;
            html += generateEmptyTd(arr_title.length);
            html += '<tr>';
            html += '<td class="spacer"></td>';
            html += '<td style="background-color: #E6E7E8;">' + responJson[i].name + '</td>';
            for (var h = 0; h < arr_title.length; h++) {
                html += '<td style="background-color: #E6E7E8;"><div>' + getSplitValue(responJson[i].values[h].value, 0) + '</div><div>' + getSplitValue(responJson[i].values[h].value, 1) + '</div></td>';

            }
            html += '<td class="spacer"></td>';
            html += '</tr>';
            if (responJson[i].key != "unassigned") {
                html += generateEmptyTd(arr_title.length);
            }
            if (salesperson.length > 0) {
                for (var j = 0; j < salesperson.length; j++) {
                    var child = salesperson[j].child;
                    html += '<tr>';
                    html += '<td class="spacer"></td>';
                    html += '<td class="border-right" style="background-color: #D1D3D4">' + salesperson[j].name + '</td>';
                    for (var k = 0; k < child.length; k++) {
                        html += '<td style="background-color: #D1D3D4"><div>' + getSplitValue(child[k].value, 0) + '</div><div>' + getSplitValue(child[k].value, 1) + '</div></td>';
                    }
                    html += '<td class="spacer"></td>';
                    html += '</tr>';
                }
            }
        }
    }
    html += generateEmptyTd(arr_title.length);
    tbody.html(html);
});

// Grip Grab report sorting
$(document).on('click', '#reporting-view .table-grip-grab tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        tbody = $('.table tbody'),
        sortTag = $('#reporting-view .table tr th a i'),
        html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 > b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 < b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = responJson[0];
    arr_title = getGripGrabTitle(arr_title);
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    var count = 0;
    for (var i = 0; i < total_data; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(responJson[i], function (key, val) {
            $.each(arr_title, function (k, value) {
                if (key == k) {
                    if (key == 'armstrong2SalespersonsId') {
                        html += '<td class="border-right">' + count + ' . ' + val + '</td>';
                    } else {
                        html += '<td>' + val + '</td>';
                    }
                }
            });
        });
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    tbody.html(html);
});

// Rich Media report sorting
$(document).on('click', '#reporting-view .table-rich-media tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        tbody = $('.table tbody'),
        sortTag = $('#reporting-view .table tr th a i'),
        html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 > b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 < b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = responJson[0];
    arr_title = getRichMediaTitle(arr_title);
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    var count = 0;
    for (var i = 0; i < responJson.length; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(responJson[i], function (key, val) {
            $.each(arr_title, function (k, value) {
                if (key == k) {
                    if (key == 'skuName') {
                        html += '<td class="border-right">' + count + '. ' + val + '</td>';
                    } else {
                        html += '<td>' + val + '</td>';
                    }
                }
            });
        });
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    tbody.html(html);
});

// Promotion Activity report sorting
$(document).on('click', '#reporting-view .table-promotion-activity tr th a', function () {
    var $this = $(this),
        cr_class = $this.attr('class'),
        fa_tag = $this.find('i'),
        key = $this.data('key'),
        tbody = $('.table tbody'),
        sortTag = $('#reporting-view .table tr th a i'),
        html = '';
    if (cr_class == 'sort' || cr_class == 'desc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 > b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('asc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-up">');
    } else if (cr_class == 'asc') {
        responJson.sort(function (a, b) {
            var a1 = a[key];
            var b1 = b[key];
            if (a1 == b1) return 0;
            return a1 < b1 ? 1 : -1;
        });
        $this.removeClass(cr_class);
        $this.addClass('desc');
        sortTag.removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
        fa_tag.remove();
        $this.append('<i class="fa fa-sort-down">');
    }
    var total_data = responJson.length;
    var arr_title = responJson[0];
    arr_title = getPromotionActivityTitle(arr_title);
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    var count = 0;
    for (var i = 0; i < total_data; i++) {
        count++;
        html += '<tr>';
        html += '<td class="spacer"></td>';
        $.each(responJson[i], function (key, val) {
            $.each(arr_title, function (k, value) {
                if (key == k) {
                    if (key == 'armstrong2SalespersonsId') {
                        html += '<td class="border-right activity-id" data-id="' + responJson[i].id + '">' + count + '. ' + val + '</td>';
                    } else if (key == 'participate') {
                        if (val == true) {
                            html += '<td><span class="checkbox-checked"></span></td>';
                        } else {
                            html += '<td><span class="checkbox-unchecked"></span></td>';
                        }
                    } else {
                        html += '<td>' + val + '</td>';
                    }
                }
            });
        });
        if (responJson[i].participate == true) {
            html += '<td><button type="button" class="btn-primary pdf-generate">PDF</td>';
        } else {
            html += '<td></td>';
        }
        html += '<td class="spacer"></td>';
        html += '</tr>';
    }
    html += generateEmptyTd(Object.keys(arr_title).length - 1);
    tbody.html(html);
});

$(document).on('click', '.pagination li', function () {
    var page = $(this).data('page'),
        item = null,
        current_page = $('.pagination li.active').data('page'),
        last_page = $('.pagination li:eq(-2)').data('page');
    if (page === 'prev') {
        if (current_page > 1) {
            item = current_page - 1;
        } else {
            item = current_page;
        }
        $('.pagination li.active').removeClass('active');
        $('.pagination li[data-page="' + item + '"]').addClass('active');
    } else if (page === 'next') {
        if (current_page < last_page) {
            item = current_page + 1;
        } else {
            item = current_page;
        }
        $('.pagination li.active').removeClass('active');
        $('.pagination li[data-page="' + item + '"]').addClass('active');
    } else {
        item = page;
        $('.pagination li.active').removeClass('active');
        $(this).addClass('active');
    }
    $('table tbody tr[data-paging]').each(function () {
        var paging = $(this).data('paging');
        if (paging === item) {
            $(this).removeClass('hidden');
        } else {
            $(this).addClass('hidden');
        }
    });
});