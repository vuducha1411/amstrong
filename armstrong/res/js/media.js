$(function() {
    $(document).on('click', '.mediathumb', function (e) {

        var galleryClass = $('#ModalMediaGallery').data('class'),
            $html = $('<div class="col-md-4"><div class="thumbnail">' + $(this).parent().html() + '</div></div>'),
            $modalView = $html.find('.modal-view'),
            $form = $('#uploader_' + galleryClass).closest('form');
        var app_type_html = '<div class="app_type" data-id="' +$(this).data('id')+ '">' +
                            '<ul class="list-inline" style="margin: 0 10px;">' +
                            '<input type="checkbox" name="app_pull" value="0" checked> <span style="margin-right: 15px">Pull </span>' +
                            '<input type="checkbox" name="app_push" value="1" checked> Push' +
                            '</ul></div>';
        $html.find('.filename').remove();
        $html.find('img').css({height:"138px"});
        if(galleryClass == 'brochure'){
            $html.find('.thumbnail').append(app_type_html);
        }
        // if (galleryClass === 'image') 
        // {
        //     //$('#uploader_' + galleryClass + ' .uploaded').html('');
        //     $('#uploader_' + galleryClass + ' .delete-media').trigger('click');
        //     $('#uploader_' + galleryClass + ' .btn-upload').hide();
        //     $modalView.attr('data-src', $modalView.attr('href')).removeAttr('target');
        //     $modalView.attr('href', '#ModalView').attr('data-toggle', 'modal');
        // }

        $html.find('.mediathumb').removeClass('mediathumb');
        $('#uploader_' + galleryClass + ' .cancelUpload').trigger('click');
        $('#uploader_' + galleryClass + ' .uploaded').append($html);
        $html.find('.caption ul a.delete').removeClass('delete-media-gallery');
        $html.find('.caption ul a.delete').addClass('delete-media');
        //$('.delete-media-hidden').show();
        // $('#uploader_' + galleryClass).find('.delete-image, .conf-delete').replaceWith('<button type="button" class="remove-image btn btn-inverse btn-mini delete"><i class="icon-remove icon-white"></i> Remove</button>');

        // if (galleryClass === 'image') $form.find('input:hidden.temp_change').remove();

        $form.append('<input type="hidden" class="temp_change" name="media_ref_change[]" value="' + $(this).data('id') + '" />');
        $('#modalClose').trigger('click');
    });

    $(document).on('click', 'a.modal-view', function (e) {

        if ($(this).attr('target') == '_blank')
        {
            return;
        }

        e.preventDefault();

        var $this = $(this),
            $target = $($this.attr('href'));

        $target.find('.modal-body').empty().html('<img src="' + $this.attr('data-src') + '" class="img-responsive">');
    });

    $(document).on('click', 'a.delete-media', function (e) {
        e.preventDefault();

        var $this = $(this);
        mediaClass = $this.data('class'),
            $form = $this.closest('form');

        if (mediaClass == 'image')
        {
            $('#uploader_' + mediaClass + ' .btn-upload').removeClass('hide').show();
        }

        $this.closest('.thumbnail').parent().remove();
        $form.append('<input type="hidden" class="temp_unlink" name="media_ref_unlink[]" value="' + $this.data('id') + '" />');
    });

    $(document).on('click', 'a.delete-media-gallery', function(){
        var media_url = $(this).prev().attr('href'),
            media_id = $(this).closest('.thumbnail').find('.mediathumb').data('id');
        if(media_id == undefined){
            media_id = $(this).closest('tr').find('.mediathumb').data('id');
        }
        var item_wrapElement = $('.uploaded').find('a[data-id="'+media_id+'"]').closest('.col-md-4');
        var thumbElement = $('.thumbnail .mediathumb[data-id="'+media_id+'"]').closest('.col-md-4');
        var list_view_item = $('.mediathumb[data-id="'+media_id+'"]').closest('tr');
        $.ajax({
            type: 'POST',
            url: base_url() + 'ami/media/deleteMedia',
            data: {
                id: media_id,
                media_url: media_url
            },
            success: function(rs){
                if(rs.success == 1){
                    $('.errors').remove();
                    $('.success').remove();
                    $('#message').append('<div class="success" style="padding: 0 20px; color: #449D44"><p>This media has been deleted</p></div>');
                    list_view_item.remove();
                    thumbElement.remove();
                    item_wrapElement.remove();
                }else{
                    $('.success').remove();
                    $('.errors').remove();

                    $('#message').append('<div class="errors" style="padding: 0 20px; color: #FF1F00"><p>This media has been used</p></div>');

                }
            }
        });
    });

    $(document).on('click', '.app_type input[type="checkbox"]', function(){
        var $form = $('#uploader_brochure').closest('form'),
            media_id = $(this).closest('.app_type').data('id'),
            check_box_pull = $(this).closest('ul').find('input[name="app_pull"]').prop('checked'),
            check_box_push = $(this).closest('ul').find('input[name="app_push"]').prop('checked'),
            app_type = 2;

        if(check_box_pull && !check_box_push){
            app_type = 0;
        }else if(!check_box_pull && check_box_push){
            app_type = 1;
        }else if(!check_box_pull && !check_box_push){
            alert('Please check at least 1 option');
            $(this).closest('ul').find('input[name="app_pull"]').prop('checked', true);
            $(this).closest('ul').find('input[name="app_push"]').prop('checked', true);
        }
        $form.append('<input type="hidden" class="temp_unlink" name="brochure_app_type[' + media_id + ']" value="' + app_type + '" />');
    });
});