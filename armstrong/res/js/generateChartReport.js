function generateHtmlChart(dataJson, type) {
    var numberInColum = 6;
    var data = reformatData(dataJson, type);
    var total_data = data.dataColum.length;
    var totalSubData = data.dataColum[0].length;
    var colorChart = ['#3bbfa8', '#da1c5c', '#eda43b', '#2d89ef', '#1e7145', '#2b5797', '#da532c', '#1e7145','#F3B200','#77B900','#2572EB','#AD103C','#632F00','#B01E00','#C1004F','#7200AC','#4617B4','#006AC1','#008287','#199900','#00C13F','#FF981D','#FF2E12','#FF1D77','#AA40FF','#1FAEFF','#56C5FF','#00D8CC','#91D100','#E1B700','#FF76BC','#00A3A3','#FE7C22'];
    for(var c = 0; c < 1000; c++){
        colorChart.push(getRandomColor());
    }

    html = '<div id="chart"><div class="chart_colum"><div class="chart-title"><span>Chart Data</span></div><div class="c_wrapper">';
    var total_sub_col = totalSubData == 1 ? 2 : totalSubData;
    var c_container_width = (total_sub_col * 20 + 30) * total_data;
    var svg_width = total_sub_col * 20 + 30;
    // TODO check with label title
    var labelTitle = 50;
    if(totalSubData > numberInColum){
        labelTitle = (Math.ceil(totalSubData / numberInColum) * 40) + 10;
    }
    html += '<div class="c_container" style="width:' + c_container_width + 'px">';
    for (var i = 0; i < total_data; i++) {
        var list_value = [];
        html += '<svg width="' + svg_width + '" height="430">';
        var step = 0;
        for (var j = 0; j < totalSubData; j++) {
            if(data.dataColum[i][j] == null) data.dataColum[i][j] = '0% 0';
            var sub_col = data.dataColum[i][j].split('%');
            var sub_col_h = getHeightColum(sub_col['0']);
            list_value.push(sub_col_h.value);
            html += '<rect x="' + step + '" y="' + sub_col_h.y + '" rx="5" width="17" height="' + sub_col_h.value + '" style="fill:' + colorChart[j] + '" />';
            step = step + 17;
        }
        var position = getMaxValue(list_value);
        var step = 15;
        var rect_x = 0;
        var rect_y = 0;
        if (totalSubData > 3) {
            rect_x = (totalSubData - 3) * 20 / 2;
            if(totalSubData > 10){
                rect_x = (totalSubData - 3 - (Math.ceil(totalSubData / 10))) * 18 / 2;
            }
            rect_y = (totalSubData - 3) * 15;
            if(totalSubData > (numberInColum - 2)){
                rect_y = (numberInColum - 2) * 15;
            }

        }
        html += '<rect x="' + rect_x + '" y="' + (position.position_y - rect_y) + '" rx="10" width="'+ labelTitle +'" height="' + (55 + rect_y) + '" style="fill:#414143" />';
        for (var j = 0; j < totalSubData; j++) {
            if(j % numberInColum == 0){
                step = 15;
            }
            var sub_col = data.dataColum[i][j].split('%');
            html += '<text x="' + (rect_x + (Math.floor(j / numberInColum) * 40) + 10) + '" y="' + (position.position_y + step - rect_y) + '" fill="' + colorChart[j] + '">' + sub_col['0'] + '%</text>';
            step = step + 15;
        }
        html += '</svg>';
    }

    if(totalSubData == 1){
        var li_width = totalSubData * 51;
        var li_class = 'one_column_with';
    }else{
        var li_width = totalSubData * 20;
        var li_class = '';
    }

    html += '<ul class="title_chart_colum" style="width:' + c_container_width + 'px">';
    for (var i = 0; i < total_data; i++) {
        html += '<li class="' + li_class + '" style="width:' + li_width + 'px">' + data.titleColum[i] + '</li>';
    }
    html += '</ul>';
    html += '</div></div></div>';
    html += '<div class="c_note"><svg width="100%" height="'+ (totalSubData * 31) +'px">';
    var step_one = 10;
    var step_two = 25;
    for (var j = 0; j < totalSubData; j++) {
        html += '<rect x="40" y="' + step_one + '" rx="5" width="20" height="20" style="fill:' + colorChart[j] + '" />' +
            '<text x="80" y="' + step_two + '" fill="#404042">' + data.noteChart[j] + '</text>';
        step_one = step_one + 30;
        step_two = step_two + 30;
    }
    html += '</svg></div></div>';
    return html;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function reformatData(dataJson, type) {
    var data_result = '';
    switch (type) {
        case 0:
            data_result = dataSku(dataJson);
            break;
        case 1:
            data_result = dataSku(dataJson);
            break;
        case 3:
            data_result = dataChannelSku(dataJson);
            break;
        case 5:
            data_result = dataSrSku(dataJson);
            break;
        default:
            alert('Failed!'); return false;
    }
    return data_result;
}

function dataSku(listData) {

    listData.noteChart = [];
    var titleColum = [];
    var dataColum = [];
    for (var i = 0; i < listData.length; i++) {
        titleColum.push(listData[i].skuName);
        var sub_data = [];
        if(listData[i].penetrationRolling3Month != null ){
            sub_data.push(listData[i].penetrationRolling3Month);
            listData.noteChart.push('Penetration % (rolling 3 months)');
        }
        if(listData[i].penetrationRolling6Month != null ){
            sub_data.push(listData[i].penetrationRolling6Month);
            listData.noteChart.push('Penetration % (rolling 6 months)');
        }
        if(listData[i].penetrationRolling12Month != null ){
            sub_data.push(listData[i].penetrationRolling12Month);
            listData.noteChart.push('Penetration % (rolling 12 months)');
        }
        if(listData[i].penetrationJanToJun != null ){
            sub_data.push(listData[i].penetrationJanToJun);
            listData.noteChart.push('Penetration % (Jan-Jun) last year');
        }
        if(listData[i].penetrationJulToDec != null ){
            sub_data.push(listData[i].penetrationJulToDec);
            listData.noteChart.push('Penetration % (Jul-Dec) last year');
        }
        if(listData[i].penetration1Year != null ){
            sub_data.push(listData[i].penetration1Year);
            listData.noteChart.push('1 year Penetration last year');
        }
        dataColum.push(sub_data);
    }
    listData.titleColum = titleColum;
    listData.dataColum = dataColum;
    return listData;
}

function dataChannelSku(listData) {
    var data = listData;
    var totalRecord = listData.length;
    // listData[0]: .Total, listData[1]: .Unassign  => ignored
    var totalChild = 0;

    for(var x=0; x<listData.length; x++){
        if(listData[x].child.length > 0){
            totalChild = listData[x].child.length;
            break;
        }
    }
    var noteChart = [];
    var titleColum = [];
    var dataColum = [];

    for(var i=0; i< totalChild; i++){
        noteChart.push(listData[x].child[i].name);
    }
    listData.noteChart = noteChart;

    for (var i = 0; i < data.length; i++) {
        if(listData[i].name != '.Total'){
            titleColum.push(listData[i].name);
            var sub_data = [];

            for(var j=0; j<totalChild; j++){
                sub_data.push(listData[i].child[j].value);
            }
            dataColum.push(sub_data);
        }
    }
    listData.titleColum = titleColum;
    listData.dataColum = dataColum;

    return listData;
}

function dataSrSku(listData) {
    var data = listData;
    var totalRecord = listData.length;
    // listData[0]: .Total, listData[1]: .Unassign  => ignored
    //var totalChild = listData[2].child[0].child.length;
    var totalChild = 0;
    for(var x=2; x<listData.length; x++){
        if(listData[x].child.length > 0){
            totalChild = listData[x].child[0].child.length;
            break;
        }
    }
    var noteChart = [];
    var titleColum = [];
    var dataColum = [];

    // TODO get noteChart
    for(var i=0; i< totalChild; i++){
        noteChart.push(listData[x].child[0].child[i].name);
    }
    listData.noteChart = noteChart;

    var data_sale = [];
    for (var i = 0; i < data.length; i++) {
        if(listData[i].name != '.Total'){
            if(listData[i].child.length > 0){
                data_sale.push(listData[i].child);
            }
        }
    }
    for (var i = 0; i < data.length; i++) {
        if(listData[i].name != '.Total'){
            if(listData[i].child.length > 0){
                var data_child = listData[i].child;
                for(var k = 0; k< data_child.length; k++){
                    titleColum.push(data_child[k].name);
                    var data_sub_child = data_child[k];
                    var sub_data = [];
                    for(var j=0; j<totalChild; j++){
                        sub_data.push(data_sub_child.child[j].value);
                    }
                    dataColum.push(sub_data);
                }
            }
        }
    }
    listData.titleColum = titleColum;
    listData.dataColum = dataColum;
    return listData;
}