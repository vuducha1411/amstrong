var flagprocess = true;
var runprocess = false;
var oldprocess = 0;
function showProgress(){
    if(flagprocess){
        $.ajax({
            url: current_url()+'/getprogress',
            type: 'GET',
            dataType: 'json',
            success: function(json){
                if(typeof json._session_expried != 'undefined')
                {
                    alert('You must login again');
                }
                else
                {
                    if(json.hightrow && json.rowcurrent)
                    {
                        var progress = parseInt(json.rowcurrent)*100/parseInt(json.hightrow);
                        if(oldprocess < progress)
                        {
                            oldprocess = progress;
                            if(progress >= 100)
                            {
                                progress = 100;
                            }
                            $('#ImportModal .progress .progress-bar').animate({width: progress+'%'}, 100, 'linear');
                        }
                    }
                    setTimeout(function(){
                        showProgress();
                    }, 500);
                }
            }
        });
    }
}
var options = {
    runtimes: 'html5,flash,silverlight,html4',
    browse_button: 'ImportPickfiles',
    url: $('#ImportPickfiles').data('url'),
    multi_selection: false,
    unique_names: true,

    filters: {
        max_file_size: '3mb',
        mime_types: [
            {'title': 'Excel', 'extensions': 'csv,xls,xlsx'}
        ]
    },

    // Flash settings
    flash_swf_url: base_url() + 'js/plupload/Moxie.swf',

    // Silverlight settings
    silverlight_xap_url: base_url() + 'js/plupload/Moxie.xap',

    // multipart_params : $.extend(params, {'uniqueId' : uniqueId}),

    init: {
        FilesAdded: function (up, files) {
            up.refresh(); // Reposition Flash/Silverlight
            up.start();
            var progress = '<div class="progress">'
                + '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="300" style="width: 1%;">'
                + '<span class="sr-only">&nbsp;</span>'
                + '</div>'
                + '</div>';

            $('#ImportModal').find('.modal-body').html('').html(progress);

            $('#ImportModalTrigger').trigger('click');
        },

        UploadProgress: function (up, file) {
           // $('#ImportModal .progress .progress-bar').animate({width: file.percent + '%'}, 100, 'linear');
           // $('#ImportModal .progress .progress-bar').animate({width: '0%'}, 100, 'linear');
            if (file.percent == 100) {
                $('#ImportModal').find('.modal-footer-message').html('').html('Importing...');
            }
            console.log('call UploadProgress');
            flagprocess = true;
            if(!runprocess){
                $('#ImportModal .progress .progress-bar').animate({width: '0%'}, 100, 'linear');
                runprocess = true;
                oldprocess = 0;
                showProgress();
            }
        },

        FileUploaded: function (up, file, info) {
            var response = JSON.parse(info.response);
            runprocess = false;
            if (response.success) {
                if (response.type == 'ssd') {
                    $html = '';
                    if(response.nb_success > 0)
                        $html += '<span style="color: green;" >Success: '+response.nb_success+' records</span><br>';
                    if(response.nb_error > 0)
                        $html += '<span style="color: red;" >Errors: '+response.nb_error+' records</span><br>';
                 //   $html = '-- Max filesize 3MB (csv, xls, xlsx) --<br />';
                    if(typeof response.file_error !=  "undefined")
                    {
                        $html += 'Please check format Excel row or format colum and check again. <br />';
                        $html += 'Please download your <a href="http://' + response.file_error + '">error file</a> and compare with <a href="http://ufs-armstrong.com/armstrong-v3/res/up/ssdExcelTemplate/ssd_excel_template.xlsx">SSD Excel Template</a>';
                    }
                    $('#ImportModal').find('.modal-body').html('').html($html);
                    if(response.nb_error > 0){
                        $('#ImportModal').find('.modal-body').css('color', 'red');
                    }
                   // $('.modal-footer-message').html('<span class="text-success">Successfully Imported</span>');
                    flagprocess = false;
                } else {
                    $('#ImportModal').find('.modal-footer-message').html('').html('<span class="text-success">Successfully Imported</span>');
                    flagprocess = false;
                    setTimeout(function () {
                        location.reload();
                    }, 3000); // 1.5 sec
                }
            } else {
                if (response.type !== "undefined") {
                    if (response.type == 'ssd') {
                        $html = '-- Max filesize 3MB (csv, xls, xlsx) --<br />';
                        $html += 'Error occurred, please check format Excel file or format colum and try again import. <br />';
                        $html += 'You can compare with <a href="http://ufs-armstrong.com/armstrong-v3/res/up/ssdExcelTemplate/ssd_excel_template.xlsx">SSD Excel Template</a>';

                        $('#ImportModal').find('.modal-body').html('').html($html);
                        $('#ImportModal').find('.modal-body').css('color', 'red');
                        $('.modal-footer-message').html('');
                    } else {
                        $('#ImportModal').find('.modal-body').html('').html('Error occured, please try again later');
                    }
                } else {
                    $('#ImportModal').find('.modal-body').html('').html('Error occured, please try again later');
                }
            }
        }
    }
}

var importUploader = new plupload.Uploader(options);
importUploader.init();