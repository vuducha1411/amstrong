$(document).ready(function(){
    $('#sapTable').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url()+"ami/shipment/get_sap_invoice/",
            "type": "POST",
            "data": function (d) {
                d.year = $("#year").val(),
                d.month = $("#month").val()
            }
        },
        'aoColumns': [
            {mData: "armstrong_2_distributors_id"},
            {mData: "armstrong_2_distributors_name"},
            {mData: "sku_number","width" : "10%"},
            {mData: "sku_name"},
            {mData: "invoice_qty","width" : "10%"},
            {mData: "invoice_value","width" : "10%"},
            {mData: "invoice_date"}
        ],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        "iDisplayLength": 10
    });

    $("#generate_tbl").on("click", function(){
        $('#sapTable').DataTable().ajax.reload();
    })
});