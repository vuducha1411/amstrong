$(document).ready(function () {
    $('#gitTable').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url()+"ami/shipment/get_git/",
            "type": "POST",
            "data": function(d){
                d.year = $("#year").val();
                d.month = $("#month").val();
            }
        },
        'aoColumns': [
            {mData: "armstrong_2_distributors_id"},
            {mData: "armstrong_2_distributors_name"},
            {mData: "sku_number","width" : "10%"},
            {mData: "sku_name"},
            {mData: "git"},
            {mData: "invoice_value", "bSearchable": false,"width" : "10%"},
            {mData: "date_created", "bSearchable": false,"width" : "15%"}
        ],
//        'order': [[5, 'desc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        "iDisplayLength": 10
    });

    $("#generate_tbl").on("click", function(){
        $('#gitTable').DataTable().ajax.reload();
    })
});