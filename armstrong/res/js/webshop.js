/**
 * Created by OPSOLUTIONS PH INC on 8/11/2017.
 */
$(document).ready(function () {
    $("#loading").hide();
    $('#wbDataTables').dataTable({
        "processing": true,
        "ajax": {
            "url": current_url()+"/get_webshop",
            "type": "GET"
        },
        'aoColumns': [
            {mData: "armstrong_2_webshop_id"},
            {mData: "webshop_order_id"},
            {mData: "armstrong_2_salespersons_id"},
            {mData: "armstrong_2_customers_name"},
            // {mData: "armstrong_2_call_records_id"},
            {mData: "total"},
            {mData: "date_created", "bSearchable": false},
            {mData: "edit_del"}
        ],
        'order': [[5, 'desc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        "iDisplayLength": 10
    });

    /* table for raw data */
    $('#wbRawDataTables').dataTable({
        "processing": true,
        "ajax": {
            "url": current_url()+"/get_raw_data",
            "type": "GET"
        },
        'columns': [
            {data: "order_id", 'bSearchable' : true},
            {data: "ufsClientNumber"},
            {data: "businessName"},
            {data: "contact_name"},
            {data: "orderTotalPrice"},
            {data: "order_date", "bSearchable": false},
            {data: "date_created", "bSearchable": false},
            {data: "edit_del"}
        ],
        'order': [[5, 'desc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        "iDisplayLength": 10
    });

    /* untagged customers */
    $('#wbUntaggedDt').dataTable({
        "processing": true,
        "ajax": {
            "url": current_url()+"/get_webshop_untagged",
            "type": "GET"
        },
        'aoColumns': [
            {mData: "order_id"},
            {mData: "customer_id"},
            {mData: "businessName"},
            {mData: "contact_name"},
            {mData: "orderTotalPrice"},
            {mData: "order_date", "bSearchable": false},
            {mData: "date_created", "bSearchable": false},
            {mData: "edit_del"}
        ],
        'order': [[5, 'desc']],
        'aoColumnDefs': [{
            'aTargets': ['nosort']
        }],
        "iDisplayLength": 10
    });

    /*$("#tagModal").on('hide.bs.modal', function(e){
        $("#productList").remove();
    });*/

    /* Refresh when selected */
    $('#armstrong_2_salesperson').on('change', function() {
        var id = $("#armstrong_2_salesperson").val();
        $("#armstrong_2_salespersons_id").val(id);
        $("#armstrong_2_customers_id").val("");
        $("#armstrong_2_customer")
            .val("")
            .autocomplete( "search", "" );
    });

    /* CUSTOMERS autocomplete */
    $('#armstrong_2_customer').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: base_url()+"ami/customers/tokenfield",
                dataType: "json",
                data: {
                    term : request.term,
                    action: 'webshop',
                    salespersons : $('#armstrong_2_salespersons_id').val()
                },
                success: function(data) {
                    if (data.length > 0) {
                        response(data);
                        document.getElementById('armstrong_2_customer').style.borderColor = '#ccc';
                        /*document.getElementById('addPotential').innerHTML = "";*/
                    }
                    else {
                        document.getElementById('armstrong_2_customer').style.borderColor = '#ff0000';
                        /*document.getElementById('addPotential').innerHTML = "<p onclick='showAddPotentialCustomer()' style='cursor:pointer'>" +
                            "<a>Customer doesnt exist. Add to Potential Customer<a/></p>";*/
                    }
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $('#armstrong_2_customers_id').val(ui.item.value);
        }
    }).on('focus', function() {
        this.select();
    });

    /* WHOLESALERS autocomplete */
    $('#armstrong_2_wholesaler').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: base_url()+"ami/wholesalers/tokenfield",
                dataType: "json",
                data: {
                    term : request.term,
                    action: 'webshop',
                    salespersons : $('#armstrong_2_salespersons_id').val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $('#armstrong_2_wholesalers_id').val(ui.item.value);

            $('#armstrong_2_distributor').val('');
            $('#armstrong_2_distributors_id').val('');
        }
    }).on('focus', function() {
        this.select();
    });

    /* DISTRIBUTORS autocomplete*/
    $('#armstrong_2_distributor').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: base_url()+"ami/distributors/tokenfield",
                dataType: "json",
                data: {
                    term : request.term,
                    action: 'webshop',
                    salespersons : $('#armstrong_2_salespersons_id').val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $('#armstrong_2_distributors_id').val(ui.item.value);
            $('#armstrong_2_wholesaler').val('');
            $('#armstrong_2_wholesalers_id').val('');
        }
    }).on('focus', function() {
        this.select();
    });

    /* potential customers */
    /*$('#armstrong_1_customers').change(function(){
        alert("its working");
    });
    $('#armstrong_1_customers').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: base_url()+"ami/potential_customers/tokenfield",
                dataType: "json",
                data: {
                    term : request.term,
                    action: 'webshop',
                    salespersons : $('#armstrong_2_salespersons_id').val()
                },
                success: function(data) {
                    console.log(data);
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $('#armstrong_1_customers_id').val(ui.item.value);
            $('#armstrong_2_customers_name').val(ui.item.name);
            $('#armstrong_2_customers_id').val('');
        }
    }).on('focus', function() {
        this.select();
    });*/

    /* autocompute */
    /*$("#armstrong_product_quantity").change(function(){
        $total = $(this).val() * $("#armstrong_product_price").val();
        $("#armstrong_total_price").val($total);
    });*/

    /* datepicker*/
    $('#webshop-daterange .input-daterange').datepicker({
        todayBtn: "linked",
        todayHighlight: true,
        // startDate: "2017-11-01",
        format: "yyyy-mm-dd"
    });
    /* hide fields */
    showFields('potential');
});

/*var dateFormat = "yy-mm-dd",
    from = $("#date_from").datepicker({
        todayBtn: 'linked',
        'todayHighlight': true,
        /!*changeMonth: true,
        numberOfMonths: 1,*!/
        format: "yyyy-mm-dd"
    })
        .on("change", function () {
            to.datepicker("option", "minDate", getDate(this));
        }),
    to = $("#date_to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        format: "yyyy-mm-dd"
    })
        .on("change", function () {
            from.datepicker("option", "maxDate", getDate(this));
        });

function getDate( element ) {
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }
    return date;
}*/

var timer;
var session;
function downloadWebshop(sessionId){
    $("#loading").show();
    session = sessionId;
    $.ajax({
        url: current_url()+"/download",
        type: 'POST',
        data: {
            'date_from': $("#date_from").val(),
            'date_to': $("#date_to").val(),
            'session': sessionId
        },
        success: function(result){
            if(result != 1){
                var res = JSON.parse(result);
                showError(res);
            }
        }
    });

    timer = setInterval(refreshProgress, 5000);
}

function refreshProgress(){
    $("#btnDownload").attr('disabled',true);
    $.ajax({
        url: current_url()+"/load_checker",
        type: 'POST',
        data: {
            'session': session
        },
        success: function(result){
            var data = JSON.parse(result);
            $("#message").html(data.message);

            if(data.percent == 100){
                window.clearInterval(timer);
                timer = window.setInterval(completeProgress, 1000);
            }
        }
    });
}

function completeProgress(){
    $("#message").html("Download Complete");
    window.clearInterval(timer);
    $("#btnDownload").attr('disabled',false);
    $("#successModal").modal("show");

    /* refresh table */
    $('#wbDataTables').DataTable().ajax.reload();
    $('#wbRawDataTables').DataTable().ajax.reload();
}

function showError(data){
    $("#message").html("");
    window.clearInterval(timer);
    $("#btnDownload").attr('disabled',false);
    $("#warningModal .modal-title").text(data.error);
    $("#warningModal .modal-body").html(data.reason);
    $("#warningModal").modal("show");
}

function hideModal(){
    $(".modal").modal("hide");
    $("#message").html("");
    $("#loading").hide();
}

function showFields(type){
    if(type == 'armstrong'){
        $("#armstrong").show();
        $("#potential").hide();
        $("#potential_fields").show();
        $("#potential_id").attr('disabled', true);
        $("#potential_name").attr('disabled', true);
        $("#armstrong_fields").hide();

    } else {
        $("#potential").show();
        $("#armstrong").hide();
        $("#potential_fields").hide();
        $("#potential_id").attr('disabled', false);
        $("#potential_name").attr('disabled', false);
        $("#armstrong_fields").show();
    }
}

/*
function showAddPotentialCustomer(){
    $("#customer").hide();
    $("#armstrong_2_customers_id").val("");
    document.getElementById('potential').innerHTML = "" +
        "<div class='col-md-12 form-group'>" +
            "<div class='form-group'>" +
                "<label for='potential_customers' class='control-label col-md-3'>Potential Customer ID</label> " +
                "<div class='col-md-6 no-parsley'>" +
                    "<input type='text' value='' class='form-control' data-parsley-required='true' id='armstrong_1_customers'>" +
                    "<input type='hidden' name='potential[armstrong_1_customers_id]' value='' class='form-control' data-parsley-required='true' id='armstrong_1_customers_id'>" +
                "</div>" +
            "</div>" +
        "</div>" +
        "<div class='col-md-12 form-group'>" +
            "<div class='form-group'>" +
                "<label for='potential_customers_name' class='control-label col-md-3'>Potential Customer Name</label> " +
                "<div class='col-md-6 no-parsley'>" +
                    "<input type='text' name='potential[armstrong_2_customers_name]' value='' class='form-control' data-parsley-required='true' id='armstrong_2_customers_name'>" +
                    "<div onclick='showAddCustomer()' style='cursor:pointer'><a>Show Customer Field</a></div>" +
                "</div>" +
            "</div>" +
        "</div>";


    $('#armstrong_1_customers').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: base_url()+"ami/potential_customers/tokenfield",
                dataType: "json",
                data: {
                    term : request.term,
                    action: 'webshop',
                    salespersons : $('#armstrong_2_salespersons_id').val()
                },
                success: function(data) {
                    console.log(data);
                    response(data);
                }
            });
        },
        delay: 100,
        select: function( event, ui ) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $('#armstrong_1_customers_id').val(ui.item.value);
            $('#armstrong_2_customers_name').val(ui.item.name);
            $('#armstrong_2_customers_id').val('');
        }
    }).on('focus', function() {
        this.select();
    });
}

function showAddCustomer(){
    $("#customer").show();
    document.getElementById('potential').innerHTML = "";
}*/
