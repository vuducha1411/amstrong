function createUploader($uploader) {
    var $filelist = $uploader.find('.filelist'),
        $uploaded = $uploader.find('.uploaded'),
        $uploadAction = $uploader.find('.upload-actions'),
        uniqueId = $('input[name="uniqueId"]').val() || false,
        params = $uploader.data('params') || {},
        appTypeHtml = '';
    if ($uploader.attr('id') == 'brochureUpload') {
        appTypeHtml = '<div class="app_type">' +
            '<ul class="list-inline" style="margin: 0 10px;">' +
            '<input type="checkbox" name="app_pull" value="0" checked>' +
            '<span style="margin-right: 15px">Pull </span>' +
            '<input type="checkbox" name="app_push" value="1" checked>Push' +
            '</ul> </div>';
    }

    var options = {
        runtimes: 'html5,flash,silverlight,html4',
        browse_button: $uploader.data('button') || 'pickfiles',
        container: $uploader.data('container') || 'upload-container',
        url: $uploader.data('url'),
        multi_selection: $uploader.data('multi') || false,
        resize: $uploader.data('resize') || {},

        filters: {
            max_file_size: $uploader.data('maxfilesize') || '5mb',
            mime_types: $uploader.data('mimetypes') || []
        },

        // Flash settings
        flash_swf_url: base_url() + 'js/plupload/Moxie.swf',

        // Silverlight settings
        silverlight_xap_url: base_url() + 'js/plupload/Moxie.xap',

        multipart_params: $.extend(params, {'uniqueId': uniqueId}),

        init: {
            PostInit: function () {
            },

            FilesAdded: function (up, files) {
                $filelist.find('.alert-file button.close').trigger('click'); //limit uploading to 1
                $uploaded.html('');
                $uploadAction.hide();
                $.each(files, function (i, file) {
                    $filelist.append(
                        '<div id="' + file.id + '" class="alert alert-file">' +
                        '<span class="filename hide">' + file.name + ' (' + plupload.formatSize(file.size) + ') </span>' +
                        '<button type="button" class="close cancelUpload">&times;</button>' + appTypeHtml +
                            '<div class="progress progress-striped"><div class="progress-bar" style="width: 1%;"></div></div></div>');
                    $filelist.on('click', '#' + file.id + ' button.cancelUpload', function () {
                        var mediaId = $(this).attr('data-id') || false;
                        if (mediaId) {
                            $.ajax({
                                dataType: 'json',
                                type: 'POST',
                                url: base_url() + 'ami/media/delete/' + mediaId,
                                data: {},
                                success: function (result) {
                                    //console.log(result);
                                    if (result.success) {
                                        up.removeFile(file);
                                        $('#' + file.id).remove();
                                        $uploadAction.show();
                                    }
                                    else {
                                        $('#' + file.id).append('<span class="text-danger">' + result.message + '</span>');
                                    }
                                }
                            });
                        }
                    });
                });
                up.refresh(); // Reposition Flash/Silverlight
                up.start();
            },

            UploadProgress: function (up, file) {
                //if(!$('#' + file.id + ' .progress').hasClass('progress-striped')){
                $('#' + file.id + ' .progress').addClass('active');
                $('#' + file.id + ' button.cancelUpload').hide();
                //}
                $('#' + file.id + ' .progress .progress-bar').animate({width: file.percent + '%'}, 100, 'linear');
            },

            Error: function (up, err) {
                $filelist.append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    'Error: ' + err.code + ', Message: ' + err.message +
                    (err.file ? ', File: ' + err.file.name : '') +
                    "</div>"
                );
                up.refresh(); // Reposition Flash/Silverlight
            },

            FileUploaded: function (up, file, info) {
                var response = JSON.parse(info.response);
                $('#' + file.id + ' .progress .progress-bar').animate({width: '100%'}, 100, 'linear');
                $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').fadeOut();
                $('#' + file.id + ' .filename').removeClass('hide').show();
                $('#' + file.id + ' button.cancelUpload').attr('data-id', response.id).show();
                $('#' + file.id + ' .app_type').attr('data-id', response.id).show();
                $('form').append('<input type="hidden" class="temp_change" name="media_ref_change[]" value="' + response.id + '" />');
            }
        }
    };

    if (!uniqueId) {
        alert('Cannot find uploader ID');
        return;
    }

    if ($uploader.length) {
        var uploader = new plupload.Uploader(options);
        uploader.init();
        //console.log(uploader);
    }
}