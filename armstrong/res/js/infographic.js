$(function() {
    // tooltip
    $("[data-toggle=tooltip], .Tooltip").tooltip();

    // popover
    $("[data-toggle=popover]").popover();
    $(document).on('click', '.popover-title .close', function (e) {
        var $target = $(e.target),
            $popover = $target.closest('.popover').prev();
        $popover && $popover.popover('hide');
    });

    // ajax modal
    $(document).on('click', '[data-toggle="ajaxModal"]', function (e) {
        e.preventDefault();
        $('#ajaxModal').remove();

        var $this = $(this),
            $remote = $this.data('remote') || $this.attr('href'),
            options = $this.data('options') || {},
            $modal = $('<div class="modal" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="ajaxModalLabel" aria-hidden="true">' +
                '<div id="ajaxModalContent">' +
                '</div>' +
            '</div>');

        $('body').append($modal);
        $modal.modal(options);
        $('#ajaxModalContent').load($remote);
    });
});
function removeElement(class_name,class_add){
	if(class_add.length > 0){
		$( class_name + ' .inner_title').addClass('title_info');
		$( class_name + ' .inner_title').addClass(class_add);
	}
	elem = $(class_name);		
	elem.replaceWith(elem.html());
	/*hotfix*/
	elem = $('.a_average');		
	elem.replaceWith(elem.html());						
	$('.inner_average').css('color','#337ab7');
}