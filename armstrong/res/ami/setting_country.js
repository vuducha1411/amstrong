/**
 * Created by Erika Ann Liongco on 7/13/2017.
 */
$(document).ready(function(){

});

function saveRecord(){

    var value = $("#country_settings").serialize();
    $.ajax({
        url: BASE_URL+'ami/setting_country/save',
        type: 'POST',
        data: value,
        success: function(res){
            location.reload();
        }
    });
}
